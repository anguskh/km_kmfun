<?php
/**
 * Check controller folder file count
 * 檢查controller目錄有幾隻功能程式 並比對menu的呈現
 */

$ignore = array(
	'common/dashboard',
	'common/startup',
	'common/login',
	'common/logout',
	'common/forgotten',
	'common/reset',			
	'common/footer',
	'common/header',
	'error/not_found',
	'error/permission'
);

// Make path into an array
$path = array('/Applications/MAMP/htdocs/kmfun/admin/controller/*') ;

// While the path array is still populated keep looping through
while (count($path) != 0) {
	$next = array_shift($path);

	foreach (glob($next) as $file) {
		// If directory add to path array
		if (is_dir($file)) {
			$path[] = $file . '/*';
		}

		// Add the file to the files to be deleted array
		if (is_file($file)) {
			$files[] = $file;
		}
	}
}
foreach ($files as $icnt => $pathStr) {
	echo "{$pathStr}<br>" ;
}


/**
 * add by Angus 2015.06.18 2017.02.28
 * pre( $user_group_info, __METHOD__, __FILE__) ;
 */
function pre($val, $className="", $fileName="", $toLogFile = FALSE, $Parameters = "")
{
	if (DEBUG_MOD) {
		if ( $toLogFile) {
			$tmpStr = trim($fileName)		? "file Name : {$fileName}\n"		: "" ;
			$tmpStr .= trim($className)		? "class Name : {$className}\n"		: "" ;
			$tmpStr .= trim($Parameters)	? "Parameters : {$Parameters}\n"	: "" ;
			$tmpStr .= print_r( $val, TRUE);
			save_log( $tmpStr) ;
		} else {
			echo '<pre>';
			echo trim($fileName)	? "file Name : {$fileName}\n"		: "" ;
			echo trim($className)	? "class Name : {$className}\n"		: "" ;
			echo trim($Parameters)	? "Parameters : {$Parameters}\n"	: "" ;
			print_r( $val);
			// var_dump( $val);
			echo '</pre><p>';
		}
	}
}
