<?php
/**
 * 每個月跑一次 刪除上個月的qrcode資料
 */
error_reporting(E_ALL) ;
ini_set("display_errors","On") ;
date_default_timezone_set("Asia/Taipei") ;
mb_internal_encoding("UTF-8");
header("Content-Type:text/html; charset=utf-8");


include_once('../inc/common_inc.php') ;

$dbObj = new database( DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE, '3306') ;

$SQLCmd = "SELECT * FROM oc_qrcode WHERE effective_date <= DATE_SUB(CURDATE(), INTERVAL 1 MONTH)" ;
$orderCarArr = $dbObj->query( $SQLCmd)->rows ;

$nowTime = date( 'Y-m-d H:i:s') ;
echo "執行時間 {$nowTime} -------------------------------------------------------------------\n";
echo "{$SQLCmd}\n";
echo "移除清單 : \n";
foreach ($orderCarArr as $iCnt => $tmpRow) {
	echo join( ",", $tmpRow)."\n" ;
}

$SQLCmd = "DELETE FROM oc_qrcode WHERE effective_date <= DATE_SUB(CURDATE(), INTERVAL 1 MONTH)" ;
echo "{$SQLCmd}\n";
// $dbObj->query( $SQLCmd)->rows ;