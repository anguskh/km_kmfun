<!doctype html>
<html lang="zh-TW">
	<head>
		<meta charset="utf-8">
		<title>NoPaper金豐租車</title>
		<meta name="keywords" content="金門租車, 金豐租車, 金門旅遊, 金門自由行" />
		<meta http-equiv="expires" content="0">
		<meta name="description" content="" />
		<meta name="designer" content="Jeff & Angus" />
		<meta name="viewport" content="width=device-width">
		<link rel="shortcut icon" href="1429819240.ico" />
		<link href="kmfun/view/css/bootstrap.css" rel="stylesheet"/>
		<link href="kmfun/view/css/daterangepicker.css" rel="stylesheet"/>
		<script src="kmfun/view/js/jquery-3.1.1.min.js"></script>
		<script src="kmfun/view/js/bootstrap.js"></script>
        <script src="kmfun/view/js/moment.js"></script><!--datetimepicker-->
		<script type="text/javascript">
			$(function () {
				$('[data-toggle="tooltip"]').tooltip()
			})
		</script>
		<script>
			$(function(){
				var theWindow = $(window);
				var windowW = theWindow.width();
				var windowH = theWindow.height();
				mainFunction();
				$(window).resize(function() {
					mainFunction();
				});
				function mainFunction(){
					var picW = $(".index_news ul li .newsbox .pimg img").width();
					var pic2W = $(".index_vehicle ul li img").width();
					$(".index_news ul li .newsbox .pimg").width(picW);
					$(".index_news ul li .newsbox .pimg").height(picW*0.625);
					if (windowW > 991) {
						$(".index_vehicle ul li img").height(pic2W);
						$(".index_vehicle ul li .mask").width(pic2W);
						$(".index_vehicle ul li .mask").height(pic2W);
					}
				};
			});
		</script>
		<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
		<![endif]-->
<?php if ( !strpos( HTTP_SERVER, "localhost") ) : ?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K42BQN2');</script>
<!-- End Google Tag Manager -->
<?php endif ; ?>
	</head>
	<body id="<?=$html_body_id?>">
<?php if ( !strpos( HTTP_SERVER, "localhost") ) : ?>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K42BQN2"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php endif ; ?>
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container">
		        <div class="navbar-header">
		        	<a href="tel:082371888" class="glyphicon glyphicon-earphone tel"></a>
		        	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
		        		<span class="sr-only">Toggle navigation</span>
		        		<span class="icon-bar"></span>
		        		<span class="icon-bar"></span>
		        		<span class="icon-bar"></span>
		        	</button>
		        	<a class="navbar-brand" href="./">金豐租車</a>
		        </div>
		        <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="<?=$urlStrAboutUs?>">關於金豐</a></li>
                        <li><a href="?route=information/carinfo">車型介紹</a></li>
                        <li><a href="<?=$urlStrRentInfo?>">租賃事宜</a></li>
                        <li><a href="?route=order/ordersearch">訂單查詢</a></li>
                        <li><a href="?route=site/customer">會員中心</a></li>
                        <li><a href="<?=$urlStrPacketRule?>">行李接送規則</a></li>
                        <!--
                        <li><a href="news.html">優惠活動</a></li>
                        <li><a href="?route=information/bedinfo">民宿介紹</a></li>
                        <li><a href="food.html">在地美食</a></li>
                        <li><a href="attraction.html">旅遊景點</a></li>
                    	-->
                    </ul>

<?php if ( !empty( $user_id)) : ?>
                    <span class="logout"><i class="glyphicon glyphicon-user"></i><?=$user_id?><a href="?route=site/customer/logout">登出</a></span>
<?php endif ; ?>
					<ul class="nav navbar-nav navbar-right">
						<li><a href="tel:082371888" class="glyphicon glyphicon-earphone tel"><span>082-371888</span></a></li>
					</ul>
				</div><!--/.nav-collapse -->
			</div>
		</nav>