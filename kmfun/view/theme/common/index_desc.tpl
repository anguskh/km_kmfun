	    <div class="wrapper" <?php if ( $qstrlen == 32) :?> hidden <?php endif ;?>>
            <div class="fixed-icon">
                <!--<a href="http://13.114.175.239/KMFun/html/mobile/self_shipping_departure.html" class="package-icon"><span class="glyphicon glyphicon-briefcase"></span>回程行李接送</a>-->
                <a href="https://line.me/R/ti/p/%40kmfun" class="line-icon" target="_blank">加入好友</a>
            </div>
	    	<!--<div class="index_news">
		    	<p class="title">
					<span>SPECIAL FOR YOU</span><br/> 
		    		優惠活動
		    	</p>
				<ul>
					<li>
						<a href=""></a>
						<p class="pimg"><img src="image/news.jpg" alt=""></p>
						<p class="ptxt">
							<span class="date">2017-06-07 ~ 2017-07-07</span>
							<span class="ntitle">【限量贈品倒數計時】Byebye-Chuchu掛勾最後加碼</span>
						</p>
						<span class="triangle"></span>
						<span class="arrow"></span>
					</li>
					<li>
						<a href=""></a>
						<p class="pimg"><img src="image/news.jpg" alt=""></p>
						<p class="ptxt">
							<span class="date">2017-06-07 ~ 2017-07-07</span>
							<span class="ntitle">【限量贈品倒數計時】Byebye-Chuchu掛勾最後加碼</span>
						</p>
						<span class="triangle"></span>
						<span class="arrow"></span>
					</li>
					<li>
						<a href=""></a>
						<p class="pimg"><img src="image/news.jpg" alt=""></p>
						<p class="ptxt">
							<span class="date">2017-06-07 ~ 2017-07-07</span>
							<span class="ntitle">【限量贈品倒數計時】Byebye-Chuchu掛勾最後加碼</span>
						</p>
						<span class="triangle"></span>
						<span class="arrow"></span>
					</li>
				</ul>
			</div><!--index_news-->
            <div class="index_about">
                <!--<img src="images/service.png" alt="">-->
                <div class="container-fluid">
                    <div class="row">
                        <div class="imgbox col-md-5 col-sm-6">
                            <img src="kmfun/view/images/index_about.jpg" alt="">
                        </div>
                        <div class="txtbox col-md-7 col-sm-6">
                            <p class="title">
                                <span>OUR SERVICE</span><br/>
                                我們的服務
                            </p>
                            金豐租車公司成立於2015年，我們是一家嶄新公司，擁有堅強的服務團隊及全新車款，以「服務第一、顧客至上」，決心打造「金門租車第一品牌」為目標，希望藉由每一次服務，成就高品質企業。
                            <p class="tag"><span>短期租車</span><span>長期租車</span><span>單程接送</span></p>
                        </div>
                    </div>
                </div>
            </div><!--index_about-->
            <div class="index_vehicle">
                <p class="title">
                    <span>CAR TYPE</span><br/>
                    車型介紹
                </p>
                <div style="text-align:center;">
                    <ul style="float:none; display: inline-block;">
                        <li class="col-md-2 col-xs-6">
                            <img src="kmfun/view/images/type_1.png" alt="">
                            <a href="?route=information/carinfo&id=3" class="mask">汽車<span>1~5人</span></a>
                        </li>
                        <li class="col-md-2 col-xs-6">
                            <img src="kmfun/view/images/type_2.png" alt="">
                            <a href="?route=information/carinfo&id=4" class="mask">進口車<span>1~9人</span></a>
                        </li>
                        <li class="col-md-2 col-xs-6">
                            <img src="kmfun/view/images/type_3.png" alt="">
                            <a href="?route=information/carinfo&id=3" class="mask">客貨車<span>1~8人</span></a>
                        </li>
                        <li class="col-md-2 col-xs-6">
                            <img src="kmfun/view/images/type_4.png" alt="">
                            <a href="?route=information/carinfo&id=2" class="mask">機車<span>1~2人</span></a>
                        </li>
                        <!--
                        <li class="col-md-2 col-xs-6">
                            <img src="kmfun/view/images/type_5.png" alt="">
                            <a href="?route=information/carinfo&id=6" class="mask">自行車<span>1~9人</span></a>
                        </li>
                        -->
                        <li class="col-md-2 col-xs-6">
                            <img src="kmfun/view/images/type_6.png" alt="">
                            <a href="?route=information/carinfo&id=7" class="mask">自動車<span>1人</span></a>
                        </li>
                    </ul>
                </div>
            </div>
	    </div><!--wrapper-->