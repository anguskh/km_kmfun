    <footer>
        <div class="info container">
            <ul class="store">
                <li class="col-sm-12">所有營業據點，全年無休</li>
                <li class="col-sm-4 col-xs-12"><!--加上 class="col-lg-4 col-xs-12"-->
                    <p class="location">金城總店</p><br/>
                    <a href="https://goo.gl/maps/2X93X7oJvPk" class="guide" target="_blank"><img src="kmfun/view/images/map_1.png"></a><br/>
                    金門縣金寧鄉伯玉路一段359-1號<br/>
                    營業時間： 08:30-18:30
                </li>
                <li class="col-sm-4 col-xs-12"><!--加上 class="col-lg-4 col-xs-12"-->
                    <p class="location">金湖店</p><br/>
                    <a href="https://goo.gl/maps/qA16Czw4CGu" class="guide" target="_blank"><img src="kmfun/view/images/map_2.png"></a></a><br/>
                    金門縣金湖鎮太湖路二段201號<br/>
                    營業時間： 08:30-17:30
                </li>
                <li class="col-sm-4 col-xs-12"><!--加上 class="col-lg-4 col-xs-12"-->
                    <p class="location">機場服務檯</p><br/>
                    <a href="https://goo.gl/maps/Be7WRgu4EZH2" class="guide" target="_blank"><img src="kmfun/view/images/map_3.png"></a></a></br/>
                    金門尚義機場<br/>
                    營業時間： 08:00-18:30
                </li>
            </ul>
            <ul class="contact">
                <li class="col-md-4 col-xs-12">
                    聯絡電話<a href="tel:082371888" class="representative">082-371888</a><br/>
                    傳真號碼<a href="#">082-372333</a><br/>
                    E-mail<a href="mailto:kmfun999@gmail.com">kmfun999@gmail.com</a>
                </li>
                <li class="col-md-4 col-xs-12">
                    <ul class="qr row">
                        <li class="col-xs-6">
                            <img src="kmfun/view/images/qr_1.jpg" alt=""><br/>
                            Line ID <b>@kmfun</b>
                        </li>
                        <li class="col-xs-6">
                            <img src="kmfun/view/images/qr_2.jpg" alt=""><br/>
                            WeChat ID <b>kmfun000</b>
                        </li>
                    </ul>
                </li>
            </ul>
        </div><!--info-->
        <p class="copyright">
            2017 © KmFun金豐租車 版權所有
        </p>
    </footer>
</html>