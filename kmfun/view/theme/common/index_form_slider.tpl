<script src="kmfun/view/js/daterangepicker.js"></script>
<header>
			<div id="myCarousel" class="carousel slide <?php if ( $qstrlen == 32) :?> hidden <?php endif ;?>" data-ride="carousel">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#myCarousel" data-slide-to="1"></li>
					<li data-target="#myCarousel" data-slide-to="2"></li>
					<li data-target="#myCarousel" data-slide-to="3"></li>
				</ol>
				<!-- Wrapper for slides -->
				<div class="carousel-inner">
					<?php foreach ($sliderArr as $iCnt => $rowInfo) : ?>
					<div class="item <?php echo ($iCnt == 0) ? "active" : "" ; ?>" style="background: url(image/<?=$rowInfo['image']?>) top center no-repeat;">
						<a href="<?=$rowInfo['href']?>" title="<?=$rowInfo['name']?>"></a>
					</div>
					<?php endforeach ; ?>
<!-- 					<div class="item active" style="background: url(kmfun/view/images/banner.jpg) top center no-repeat;">
						<a href="#" ></a>
					</div>
 -->			  	</div>
				<!-- Left and right controls -->
				<a class="left carousel-control" href="#myCarousel" data-slide="prev">
			    	<span class="glyphicon glyphicon-chevron-left"></span>
			    	<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#myCarousel" data-slide="next">
			    	<span class="glyphicon glyphicon-chevron-right"></span>
			    	<span class="sr-only">Next</span>
				</a>
			</div>
			<div class="order-type" <?php if ( $qstrlen == 32) :?>style="padding-top: 6rem;"<?php endif ;?>>
				<ul class="nav nav-tabs" >
				    <li class="active"><a data-toggle="tab" href="#menu1"><span class="icon-car"></span>租車</a></li>
				</ul>
				<div class="tab-content container-fluid">
				    <div id="menu1" class="tab-pane fade in active">
						<form action="">
							<div class="col">
								<div class="box box1">
									<label for="">取車地點：</label>
									<div class="form-group">
										<select class="form-control" id="rent_station1" name="rent_station1">
											<option value=""> 請選擇 </option>
<?php foreach( $rentStation as $tmpStation) : ?>
										<option value="<?=$tmpStation['idx']?>" <?php echo (($rent_station==$tmpStation['idx'])?"selected":"");?>><?=$tmpStation['opt_name']?></option>
<?php endforeach ; ?>
										</select>
									</div>

						        </div>
						        <div class="box box2">
									<label for="">取車日期：</label>
									<div class="form-group">
										<input type="text" id="sdate" name="rent_date_start" value="<?php echo (($qid_flag=='Y'||$qid_flag=='YY')?$showDate1:'');?>" <?php echo (($qid_flag=='Y'||$qid_flag=='YY')?'readonly':'');?>/>
										<span class="datepicker">
						            		<span class="glyphicon glyphicon-calendar"></span>
						                </span>
<?php
										if($qid_flag == "N"){
?>
										<script type="text/javascript">
											$(function() {
												console.log('取車日期 Init');
												var d1 = "" ;

												d1 = new Date((new Date()).valueOf() + 2000*3600*24) ;
												$('input[name="rent_date_start"]').daterangepicker({
											        locale: {
												    	format: 'YYYY-MM-DD'
												    },
												    singleDatePicker: true,
												    minDate: d1
											    });
											});


										</script>
<?php
										}
?>
						            </div>
							    </div>
						        <div class="box box3">
						            <label for="">取車時間：</label>
									<div class="form-group">
										<select class="form-control" id="rent_time_start" name="rent_time_start">
<?php foreach( $rentTimes as $tmpTime) : ?>
									        <option <?php echo (($qid_time==$tmpTime)?'selected':'');?>><?=$tmpTime?></option>
<?php endforeach ; ?>
									    </select>
									</div>
							    </div>
						    </div>
						    <div class="col">
								<div class="box box4">
									<label for="">還車地點：</label>
									<div class="form-group">
										<select class="form-control" id="rent_station2" name="rent_station2">
											<option value=""> 請選擇 </option>
<?php foreach( $rentStation as $tmpStation) : ?>
										<option value="<?=$tmpStation['idx']?>"><?=$tmpStation['opt_name']?></option>
<?php endforeach ; ?>
									    </select>
									</div>
								</div>
								<div class="box box5">
									<label for="">還車日期：</label>
									<div class="form-group">
										<input type="text" id="edate" name="rent_date_end" value="" />
										<span class="datepicker">
						            		<span class="glyphicon glyphicon-calendar"></span>
						                </span>
										<script type="text/javascript">
											$(function() {
												d1 = new Date((new Date()).valueOf() + 2000*3600*24) ;
												<?php
													if($qid_flag=='Y'||$qid_flag=='YY'){
												?>
														d1 = '<?php echo $showDate1?>';
												<?php
													}
												?>
												$('input[name="rent_date_end"]').daterangepicker({
											        locale: {
												    	format: 'YYYY-MM-DD'
												    },
												    singleDatePicker: true,
												    minDate: d1
											    });
											});

										</script>
						            </div>
							    </div>
								<div class="box box6">
						            <label for="">還車時間：</label>
									<div class="form-group">
										<select class="form-control" id="rent_time_end" name="rent_time_end">
<?php foreach( $rentTimes as $tmpTime) : ?>
									        <option <?php echo (($qid_time==$tmpTime)?'selected':'');?>><?=$tmpTime?></option>
<?php endforeach ; ?>
									    </select>
									</div>

								</div>
							</div>
							<div class="col final">
								<div class="box box1">
									<label for="">訂車數量</label>
									<div class="form-group">
										<select class="form-control num" id="b_car_num" name="b_car_num">
									        <option>1</option>
<!-- 									        <option>2</option>
									        <option>3</option>
									        <option>4</option>
									        <option>5</option>
									        <option>6</option>
									        <option>7</option>
									        <option>8</option>
									        <option>9</option>
 -->									    </select>
									</div>
								</div>
								<div class="box box2">
									<label for="">成人</label>
									<div class="form-group">
										<select class="form-control num" id="adults" name="adults">
									        <option value="0">0人</option>
									        <option value="1">1人</option>
									        <option value="2">2人</option>
									        <option value="3">3人</option>
									        <option value="4">4人</option>
									        <option value="5">5人</option>
									        <option value="6">6人</option>
									        <option value="7">7人</option>
									        <option value="8">8人</option>
									        <option value="9">9人</option>
									    </select>
									</div>
								</div>
								<div class="box box2">
									<label for="">幼兒<span data-toggle="tooltip" data-placement="top" title data-original-title="1 ~ 4歲" class="tip">?</span></label>
									<div class="form-group">
										<select class="form-control num" id="children" name="children">
									        <option value="0">0人</option>
									        <option value="1">1人</option>
									        <option value="2">2人</option>
									        <option value="3">3人</option>
									    </select>
									</div>
								</div>
								<div class="box box3">
									<label for="">嬰兒<span data-toggle="tooltip" data-placement="top" title data-original-title="0 ~ 1歲" class="tip">?</span></label>
									<div class="form-group">
										<select class="form-control num" id="babies" name="babies">
									        <option value="0">0人</option>
									        <option value="1">1人</option>
									        <option value="2">2人</option>
									        <option value="3">3人</option>
									    </select>
									</div>
								</div>
								<div class="box box5">
									<label for="">優惠代碼</label>
									<div class="form-group">
										<input type="text" name="coupon" />
									</div>
								</div>
								<div class="box box4">
									<button type="button" id="bt_rental" onclick="">立即選車</button>
								</div>
							</div>
						</form>
				    </div>
				</div>
			</div>

</header>
<script type="text/javascript"><!--

$("select[name*='rent_station1']").change( function () {
	var html = "" ;
	if ( $(this).val() == 16) {
		html = '<option value=""> --請選擇-- </option><option>08:30</option><option>09:00</option><option>09:30</option><option>10:00</option><option>10:30</option><option>11:00</option><option>11:30</option><option>12:00</option><option>12:30</option><option>13:00</option><option>13:30</option><option>14:00</option><option>14:30</option><option>15:00</option><option>15:30</option><option>16:00</option><option>16:30</option><option>17:00</option><option>17:30</option><option>18:00</option>';
	} else if ( $(this).val() == 17) {
		html = '<option value=""> --請選擇-- </option><option>08:30</option><option>09:00</option><option>09:30</option><option>10:00</option><option>10:30</option><option>11:00</option><option>11:30</option><option>12:00</option><option>12:30</option><option>13:00</option><option>13:30</option><option>14:00</option><option>14:30</option><option>15:00</option><option>15:30</option><option>16:00</option><option>16:30</option><option>17:00</option>';
	} else if ( $(this).val() == 18) {
		html = '<option value=""> --請選擇-- </option><option>08:00</option><option>08:30</option><option>09:00</option><option>09:30</option><option>10:00</option><option>10:30</option><option>11:00</option><option>11:30</option><option>12:00</option><option>12:30</option><option>13:00</option><option>13:30</option><option>14:00</option><option>14:30</option><option>15:00</option><option>15:30</option><option>16:00</option><option>16:30</option><option>17:00</option><option>17:30</option><option>18:00</option>';
	} else {
		html = '<option value=""> --請選擇-- </option><option>08:30</option><option>09:00</option><option>09:30</option><option>10:00</option><option>10:30</option><option>11:00</option><option>11:30</option><option>12:00</option><option>12:30</option><option>13:00</option><option>13:30</option><option>14:00</option><option>14:30</option><option>15:00</option><option>15:30</option><option>16:00</option><option>16:30</option><option>17:00</option><option>17:30</option><option>18:00</option>';
	}
	$('select[name=\'rent_time_start\']').html(html);

	//檢查gid_time是否不為空 不為空要指定時間
	var gid_time = "<?php echo $qid_time?>";

	if(gid_time != ""){
		//$('select[name=\'rent_time_start\']').find('option[value=\''+gid_time+'\']').prop("selected");
		//$("#rent_time_start option[value='15:30']").attr("selected", true);
		$("#rent_time_start option").attr("selected", false);
		//console.log($("#rent_time_start"));
		$("#rent_time_start").each(function() {
			$('option', this).each(function () {
				if(gid_time==$(this).text()){
					$(this).attr('selected', 'selected')
				}
			});
		});
	}

});
$("select[name*='rent_station2']").change( function () {
	var html = "" ;
	if ( $(this).val() == 16) {
		html = '<option value=""> --請選擇-- </option><option>08:30</option><option>09:00</option><option>09:30</option><option>10:00</option><option>10:30</option><option>11:00</option><option>11:30</option><option>12:00</option><option>12:30</option><option>13:00</option><option>13:30</option><option>14:00</option><option>14:30</option><option>15:00</option><option>15:30</option><option>16:00</option><option>16:30</option><option>17:00</option><option>17:30</option><option>18:00</option>';
	} else if ( $(this).val() == 17) {
		html = '<option value=""> --請選擇-- </option><option>08:30</option><option>09:00</option><option>09:30</option><option>10:00</option><option>10:30</option><option>11:00</option><option>11:30</option><option>12:00</option><option>12:30</option><option>13:00</option><option>13:30</option><option>14:00</option><option>14:30</option><option>15:00</option><option>15:30</option><option>16:00</option><option>16:30</option><option>17:00</option>';
	} else if ( $(this).val() == 18) {
		html = '<option value=""> --請選擇-- </option><option>08:00</option><option>08:30</option><option>09:00</option><option>09:30</option><option>10:00</option><option>10:30</option><option>11:00</option><option>11:30</option><option>12:00</option><option>12:30</option><option>13:00</option><option>13:30</option><option>14:00</option><option>14:30</option><option>15:00</option><option>15:30</option><option>16:00</option><option>16:30</option><option>17:00</option><option>17:30</option><option>18:00</option>';
	} else {
		html = '<option value=""> --請選擇-- </option><option>08:30</option><option>09:00</option><option>09:30</option><option>10:00</option><option>10:30</option><option>11:00</option><option>11:30</option><option>12:00</option><option>12:30</option><option>13:00</option><option>13:30</option><option>14:00</option><option>14:30</option><option>15:00</option><option>15:30</option><option>16:00</option><option>16:30</option><option>17:00</option><option>17:30</option><option>18:00</option>';
	}
	$('select[name=\'rent_time_end\']').html(html);


	//檢查gid_time是否不為空 不為空要指定時間
	var gid_time = "<?php echo $qid_time?>";

	if(gid_time != ""){
		//$('select[name=\'rent_time_start\']').find('option[value=\''+gid_time+'\']').prop("selected");
		//$("#rent_time_start option[value='15:30']").attr("selected", true);
		$("#rent_time_end option").attr("selected", false);
		//console.log($("#rent_time_start"));
		$("#rent_time_end").each(function() {
			$('option', this).each(function () {
				if(gid_time==$(this).text()){
					$(this).attr('selected', 'selected')
				}
			});
		});
	}

});

$('#bt_rental').on('click', function () {
	var coupon = $('input[name=\'coupon\']').val() ;
	console.log( "coupon code : " + coupon) ;
	var qid = '<?php echo $qid;?>';
	var d1 = $('#sdate').val() ;
	var d2 = $('#edate').val() ;
	var t1 = $('select[name=\'rent_time_start\'] option:selected').val() ;
	var t2 = $('select[name=\'rent_time_end\'] option:selected').val() ;

	var s1 = $('select[name=\'rent_station1\'] option:selected').val() ;
	var s2 = $('select[name=\'rent_station2\'] option:selected').val() ;

	var b_car_num	= $('select[name=\'b_car_num\'] option:selected').val() ;
	var adults		= $('select[name=\'adults\'] option:selected').val() ;
	var children	= $('select[name=\'children\'] option:selected').val() ;
	var babies		= $('select[name=\'babies\'] option:selected').val() ;

	var msg = "" ;
	var sDate = d1+'T'+t1;
	var eDate = d2+'T'+t2;

	console.log( new Date());
	if ( s1 == "") { msg += "取車地點未選定\n" ; }
	if ( s2 == "") { msg += "還車地點未選定\n" ; }
	if ( sDate == eDate) { msg += "還車日期不能等於取車日期\n" ; }
	if ( checkDate( sDate)) { msg += "取車日期不能小於今天\n" ; }
	if ( checkDate( sDate, eDate)) { msg += "還車日期不能小於取車日期\n" ; }
	if ( adults < b_car_num) { msg += "請選擇旅遊人數\n" ; }

	var nowDate = Date.parse((new Date()).toDateString()) ;
	var nowDateTime = Date.parse((new Date()).toDateString()+' '+(new Date()).toTimeString());
	if ( (Date.parse(d1) > nowDate) || (Data.parse(d1+' 07:30') > nowDateTime)) {
	} else {
    	msg += "抱歉~~ 無法租訂當日車輛" ;
	}


	if ( msg != "") { alert( msg) ; return false ; }

	var oneDayHour = checkOneDay ( sDate, eDate) ;

	if ( oneDayHour < 24) {
		var msg = "本公司最短租期為一天(24H). 未滿一天以天計算" ;
		if ( confirm(msg)) {
			location.href = "?route=order/rental&token=<?=$token?>&s1="+s1+"&s2="+s2+"&d1="+d1+"&d2="+d2+"&t1="+t1+"&t2="+t2+"&bcn="+b_car_num+"&adults="+adults+"&children="+children+"&babies="+babies+"&coupon="+coupon+"&qid="+qid ;
		}
	} else {
		location.href = "?route=order/rental&token=<?=$token?>&s1="+s1+"&s2="+s2+"&d1="+d1+"&d2="+d2+"&t1="+t1+"&t2="+t2+"&bcn="+b_car_num+"&adults="+adults+"&children="+children+"&babies="+babies+"&coupon="+coupon+"&qid="+qid ;
	}
}) ;

function checkOneDay( sDate, eDate) {
	var currDate = "" ;
	var txtDate = "" ;
	if ( eDate == undefined) {
		currDate = Date.parse((new Date()).toDateString()+' '+(new Date()).toTimeString());
		txtDate = Date.parse(sDate);
	} else {
		currDate = Date.parse(sDate);
		txtDate = Date.parse(eDate);
	}
	var ONE_HOUR = 1000 * 60 * 60 ;
	return  (txtDate - currDate) / ONE_HOUR ;
}

function checkDate( sDate, eDate) {
	var currDate = "" ;
	var txtDate = "" ;
	if ( eDate == undefined) {
		currDate = Date.parse((new Date()).toDateString()+' '+(new Date()).toTimeString());
		txtDate = Date.parse(sDate);
	} else {
		currDate = Date.parse(sDate);
		txtDate = Date.parse(eDate);
	}
	// if ( currDate == txtDate) return true ;

	if (currDate <= txtDate) {
		return false ;
	} else {
		return true ;
	}
}

$( document ).ready(function() {
	Date.prototype.addDays = function(days) {
	  this.setDate(this.getDate() + days);
	  return this;
	}
	$('#sdate').change(function(event) {
		var sDate = new Date( $(this).val()) ;
		var eDate = sDate.addDays(1).toISOString() ;

		eDate = eDate.split("T") ;
		console.log(eDate) ;
		$('#edate').val( eDate[0]) ;
	});
});




--></script>