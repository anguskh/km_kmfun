<?php echo $header; ?>
		<!-- <link href="kmfun/view/css/bootstrap.css" rel="stylesheet"/>
		<link href="kmfun/view/css/daterangepicker.css" rel="stylesheet"/>
		<script src="kmfun/view/js/jquery-3.1.1.min.js"></script>
		<script src="kmfun/view/js/bootstrap.js"></script>
        <script src="kmfun/view/js/moment.js"></script>
    -->
<header></header>
<div class="wrapper container">
	<div class="container-fluid">
		<div class="container-fluid title bnb">會員中心</div>
		<div class="memberbox wide">
			<div class="container">
			<?php if ($error_warning) { ?>
				<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
					<button type="button" class="close" data-dismiss="alert">&times;</button>
				</div>
			<?php } ?>
			<?php if ($success) { ?>
				<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
					<button type="button" class="close" data-dismiss="alert">&times;</button>
				</div>
			<?php } ?>
			</div>
			<form action="?route=site/customer/saveInfo" method="post" id="form1">
			<table class="status_info">
				<tr>
					<th>帳號</th>
					<td>
						<span class="" ><?=$infoCustometr['pid']?></span>
						<input type="hidden" name="customer_id" value="<?=$infoCustometr['customer_id']?>">
					</td>
				</tr>
				<tr>
					<th>密碼</th>
					<td>
						<button class="pwdIN" type="button" id="btEditPW" >修改密碼</button>
						<input  class="pwdOUT" type="password" name="password" placeholder="請輸入密碼" hidden>
						<input  class="pwdOUT" type="password" name="passwordconfirm" placeholder="請再輸入密碼" onKeyUp="check()" hidden>
						<button class="pwdOUT" type="button" id="btSendPW" hidden>修改密碼</button>
						<button class="pwdOUT" type="button" id="btCancelPW" hidden>取消</button><br>
						<span id="msg" style="color: red;"></span>
					</td>
				</tr>
				<tr>
					<th>姓名</th>
					<td>
						<span class="custInLine" <?=$hiddenInfo?>><?=$infoCustometr['name']?></span>
						<input class="custOutLine" type="text" name="customer[name]" value="<?=$infoCustometr['name']?>" <?=$hiddenEdit?>>
					</td>
				</tr>
				<tr>
					<th>電話</th>
					<td>
						<span class="custInLine" <?=$hiddenInfo?>><?=$infoCustometr['telephone']?></span>
						<input class="custOutLine" type="text" name="customer[tel]" value="<?=$infoCustometr['telephone']?>" <?=$hiddenEdit?>>
					</td>
				</tr>
				<tr>
					<th>email</th>
					<td>
						<span class="custInLine" <?=$hiddenInfo?>><?=$infoCustometr['email']?></span>
						<input class="custOutLine" type="text" name="customer[email]" value="<?=$infoCustometr['email']?>" <?=$hiddenEdit?>>
					</td>
				</tr>
				<tr>
					<th>地址</th>
					<td>
						<input type="hidden" name="address_id" value="<?=$infoAddress['address_id']?>">
						<span class="custInLine" <?=$hiddenInfo?>><?=$infoAddress['city']?> <?=$infoAddress['postcode-zh']?> <?=$infoAddress['address_1']?></span>
						<select class="custOutLine" name="zipcode1" id="zipcode1" <?=$hiddenEdit?>>
							<option value="">請選擇</option>
						<?php foreach ( $retZip as $iCnt => $tmpOpt) :
								$zipSeled = ( $tmpOpt['zip_code'] == $selZipCode1) ? "selected" : "" ;
						?>
							<option value="<?=$tmpOpt['zip_code']?>" <?=$zipSeled?>><?=$tmpOpt['zip_name']?></option>
						<?php endforeach ;?>
						</select>
						<select class="custOutLine" name="zipcode2" id="zipcode2" <?=$hiddenEdit?>>
							<option value="">請選擇</option>
						<?php
							if( isset( $retZip2)) {
								foreach( $retZip2 as $zip_code => $zip_name) {
									$zipSeled = ( $zip_code == $selZipCode2) ? "selected" : "" ;
									echo "<option value=\"{$zip_code}\" {$zipSeled}>{$zip_name}</option>" ;
								}
							}
						?>
						</select>
						<input class="custOutLine" type="text" name="address" value="<?=$infoAddress['address_2']?>" <?=$hiddenEdit?>>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<button type="button" id="btInLine" <?=$hiddenInfo?>>修改</button>
						<button type="submit" form="form1" id="btSave" <?=$hiddenEdit?>>儲存</button>
						<button type="button" id="btOutLine" <?=$hiddenEdit?>>取消</button>
					</td>
				</tr>
			</table>
			</form>

			<div class="table-responsive">
				<table class="table">
					<tr>
						<?php foreach ($orderColName as $colKey => $colName) : ?>
							<th><?=$colName?></th>
						<?php endforeach ; ?>
					</tr>
					<?php if ( is_array( $orderList)) : ?>
						<?php foreach ($orderList as $iCnt => $row) : ?>
							<tr>
								<?php foreach ($orderColName as $colKey => $colName) : ?>
									<td><?=$row[$colKey]?></td>
								<?php endforeach ; ?>
							</tr>
						<?php endforeach ; ?>
					<?php else : ?>
					<tr>
						<td colspan="<?=count($orderColName)?>">目前沒有訂單
						</td>
					</tr>
					<?php endif ; ?>
				</table>
			</div>
		</div>

		<script type="text/javascript">
			var pwCheckOK = false ;
			$('#btInLine').click( function () {
				$(this).hide() ;
				$('.custInLine').hide() ;
				$('.custOutLine').show() ;
				$('#btOutLine').show() ;
				$('#btSave').show() ;
			});
			$('#btOutLine').click( function () {
				$(this).hide() ;
				$('.custInLine').show() ;
				$('.custOutLine').hide() ;
				$('#btInLine').show() ;
				$('#btSave').hide() ;
			});
			$('#btEditPW').click( function () {
				$('.pwdOUT').show() ;
				$('#btEditPW').hide() ;
			}) ;
			$('#btCancelPW').click( function () {
				$('.pwdOUT').hide() ;
				$('#btEditPW').show() ;
			}) ;

			function check() {
				var pw1 = $("input[name='password']").val()
				var pw2 = $("input[name='passwordconfirm']").val()
				var msg = "" ;
				console.log( pw1);
				console.log( pw2);
				if ( pw1 != pw2) {
					msg = "密碼不同" ;
					$('#msg').html( msg) ;
				} else {
					$('#msg').html( "") ;
					pwCheckOK = true ;
				}
			}

			$('#btSendPW').click( function () {
				if ( !pwCheckOK) {
					console.log( "未完成");
				} else {
					$('#form1').submit() ;
				}
			});


// 非同步
$("select[name='zipcode1']").change( function () {
	console.log("select[name='zipcode1']") ;
	var selVal = $(this).val() ;
	console.log(selVal) ;
	$('#zipcode2').find('option:not(:first)').remove() ;
	$.ajax({
		url : '?route=order/rental/getSubZip',
		type : 'post',
		data : {zipMain : selVal},
		dataType : 'json',
		success : function (resp) {
			for (var i = 0; i < resp.length; i++) {
				$("#zipcode2").append('<option value=' + resp[i].zip_code + '>' + resp[i].zip_code + ' ' + resp[i].zip_name + '</option>') ;
			}
		}
	});
});
		</script>
	</div>
</div>
<?php echo $footer; ?>