<?php echo $header; ?>
<header></header>
<div class="wrapper container">
	<div class="container-fluid">
		<div class="container-fluid title bnb">會員中心</div>
			<form action="?route=site/customer/joinus" method="post" id="form1">
				<div class="memberbox wide">
					<p class="stitle">加入會員</p>
					<?php if ($error_warning) { ?>
						<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
							<button type="button" class="close" data-dismiss="alert">&times;</button>
						</div>
					<?php } ?>
					<?php if ( isset( $success)) { ?>
						<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
							<button type="button" class="close" data-dismiss="alert">&times;</button>
						</div>
					<?php } ?>
					<p>
						<label>帳號</label>
						<input type="text" name="customer[pid]" placeholder="身分證(護照)號碼" value="" onKeyUp="up(this)">
					</p>
					<p>
						<label>姓名</label>
						<input type="text" name="customer[name]" placeholder="姓名" value="">
					</p>
					<p>
						<label>電話</label>
						<input type="text" name="customer[tel]" placeholder="請填寫手機號碼" value="">
					</p>
					<p>
						<label>e-mail</label>
						<input type="text" name="customer[email]" placeholder="電子郵件" value="">
					</p>
					<p>
						<label>地址</label>
						<select name="zipcode1" id="zipcode1">
							<option value="">請選擇</option>
							<?php foreach ( $retZip as $iCnt => $tmpOpt) :?>
							<option value="<?=$tmpOpt['zip_code']?>" ><?=$tmpOpt['zip_name']?></option>
							<?php endforeach ;?>
						</select>
						<select name="zipcode2" id="zipcode2">
							<option value="">請選擇</option>
						</select>
						<input type="text" name="address" value="">
					</p>
					<p>
						<label>密碼</label>
						<input type="text" name="password" placeholder="請輸入密碼" value="">
					</p>
					<p>
						<label>確認密碼</label>
						<input type="text" name="passwordconfirm" placeholder="請再輸入密碼" value="">
					</p>
					<p>
						<button type="button" id="btSave" >加入會員</button>
						<button type="button" id="cancel">取消</button>
					</p>
				</div>
		</form>
		<script type="text/javascript">
			function up(d){
				d.value=d.value.toUpperCase();
			}

			$('#btSave').click( function () {
				var msg = "" ;
				var isEmpty = true ;
				if ( $("input[name='customer[pid]']").val() == "" ) {
					msg += "帳號 未填寫\n" ;
				}
				if ( $("input[name='customer[name]']").val() == "" ) {
					msg += "姓名 未填寫\n" ;
				}
				if ( $("input[name='customer[tel]']").val() == "" ) {
					msg += "電話 未填寫\n" ;
				}
				if ( $("input[name='customer[email]']").val() == "" ) {
					msg += "e-mail 未填寫\n" ;
				}
				console.log($("select[name='zipcode1']").val());
				if ( $("select[name='zipcode1']").val() == ""
						|| $("select[name='zipcode2']").val() == ""
						|| $("input[name='customer[address]']").val() == "") {
					msg += "地址 未填寫\n" ;
				}
				if ( $("input[name='password']").val() == ""
						|| $("input[name='passwordconfirm']").val() == "") {
					msg += "密碼 未填寫完全\n" ;
				}
				if ( $("input[name='password']").val() != $("input[name='passwordconfirm']").val()) {
					msg += "密碼 未填寫錯誤\n" ;
				}

				if ( msg != "") {
					alert( msg) ;
					return false ;
				}
				$('#form1').submit() ;
			}) ;

// 非同步
$("select[name='zipcode1']").change( function () {
	console.log("select[name='zipcode1']") ;
	var selVal = $(this).val() ;
	console.log(selVal) ;
	$('#zipcode2').find('option:not(:first)').remove() ;
	$.ajax({
		url : '?route=order/rental/getSubZip',
		type : 'post',
		data : {zipMain : selVal},
		dataType : 'json',
		success : function (resp) {
			for (var i = 0; i < resp.length; i++) {
				$("#zipcode2").append('<option value=' + resp[i].zip_code + '>' + resp[i].zip_code + ' ' + resp[i].zip_name + '</option>') ;
			}
		}
	});
});
		</script>
	</div>
</div>
<?php echo $footer; ?>