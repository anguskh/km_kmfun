<?php echo $header; ?>
<header></header>
<div class="wrapper container">
	<div class="container-fluid">
		<div class="container-fluid title bnb">會員中心</div>
			<form action="?route=site/customer/forget" method="post">
			<?php if ($error_warning) { ?>
				<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
					<button type="button" class="close" data-dismiss="alert">&times;</button>
				</div>
			<?php } ?>
			<?php if ($success) { ?>
				<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
					<button type="button" class="close" data-dismiss="alert">&times;</button>
				</div>
			<?php } ?>

				<div class="memberbox">
					<p class="stitle">忘記密碼</p>
					<p>
						<label>帳號</label>
						<input type="text" name="customer_id" value="a125432447" placeholder="請使用 身分證(護照)" onKeyUp="up(this)">
					</p>
					<p>
						<label>e-mail</label>
						<input type="text" name="email" value="angus.kh@gmail.com" placeholder="電子郵件">
					</p>
					<p><input type="submit" value="送出"></p>
				</div>
			</form>
		<script type="text/javascript">
			function up(d){
				d.value=d.value.toUpperCase();
			}
		</script>
	</div>
</div>
<?php echo $footer; ?>