<?php echo $header; ?>
		<header></header>
	    <div class="wrapper container">
	    	<div class="container-fluid step">
	    		<span class="<?=$stepFlag[0]?>">1</span>選擇車輛<span class="glyphicon glyphicon-menu-right"></span>
	    		<span class="<?=$stepFlag[1]?>">2</span>額外服務<span class="glyphicon glyphicon-menu-right"></span>
	    		<span class="<?=$stepFlag[2]?>">3</span>駕駛人資料<span class="glyphicon glyphicon-menu-right"></span>
	    		<span class="<?=$stepFlag[3]?>">4</span>完成訂車
	    	</div>
	    	<div class="container-fluid rental-info">
		    	<ul>
		    		<li class="col-md-4 col-sm-6">
						<label for="">取車</label><br/>
						<?=$rentInfo['startDate']?>
						<span class="glyphicon glyphicon-chevron-right to"></span>
						<span class="glyphicon glyphicon-chevron-down to"></span>
		    		</li>
		    		<li class="col-md-4 col-sm-6">
						<label for="">還車</label><br/>
						<?=$rentInfo['endDate']?>
		    		</li>
		    		<li class="col-md-4 col-sm-12">
		    			<ul>
		    				<li>訂車數量<span class="num"><?=$rentInfo['b_car_num']?></span></li>
		    				<li>成人<span class="num"><?=$rentInfo['adults']?></span></li>
		    				<li>0~1歲<span class="num"><?=$rentInfo['babies']?></span></li>
		    				<li>1~4歲<span class="num"><?=$rentInfo['children']?></span></li>
		    			</ul>
		    		</li>
		    	</ul>
		    </div>
	    	<div class="vehicle-list container-fluid">
	    		<div class="tab-content">
					<table cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td class="col-lg-4 col-sm-3 col-xs-12 white">
								<a href="#"><img src="<?=$rentInfo['carSeed']['thumb']?>" alt=""></a>
							</td>
							<td class="col-lg-5 col-sm-6 col-xs-12 white">
								<a href=""><?=$rentInfo['carSeed']['car_seed']?></a><br/>
								<?php
								// dump( $rentInfo['carSeed']) ;
								$carMode = ( $rentInfo['carSeed']['idx'] == 18) ? "手排" : "自排" ;
								?>
								<?=$rentInfo['carSeed']['car_door']?>門 | <?=$carMode?> | 乘坐 <?=$rentInfo['carSeed']['car_peopele']?>人
							</td>
						</tr>
					</table>
				</div>
			</div>
			<form action="">
				<div class="container-fluid additional">
					<ul>
						<li><p class="stitle">額外服務</p></li>
						<?php if ( $car_model == 7 ) : ?>
							<input type="hidden" id="mobilemoto" value="0">
							<input type="hidden" id="babieschair1" value="0">
							<input type="hidden" id="babieschair2" value="0">
							<input type="hidden" id="babieschair3" value="0">
							<input type="hidden" id="childrenchair" value="0">
							<input type="hidden" id="gps" value="0">
						<?php elseif ( $car_model == 2 || $car_model == 6 ) :
							// $car_model == 2 機車
							// $car_model == 6 自行車
						?>
						<!-- mark by kmfun 2018.10.07 -->
						<!--<li>
							<div class="pimg col-sm-3"><img src="kmfun/view/images/mobile_moto.jpg" alt=""></div>
							<div class="ptxt col-sm-9">
								<p>手機導航架<span>$50 / 日均</span></p>
								機車用手機導航架<br/><br/><br/>
								我要租用
								<select name="" id="mobilemoto">
									<?php for( $i = 0; $i <= $optionCnt+1 ; $i++ ) : ?>
									<option value="<?=$i?>"><?=$i?></option>
									<?php endfor ; ?>
								</select>
								個
								<input type="hidden" id="babieschair1" value="0">
								<input type="hidden" id="babieschair2" value="0">
								<input type="hidden" id="babieschair3" value="0">
								<input type="hidden" id="childrenchair" value="0">
								<input type="hidden" id="gps" value="0">
							</div>
							<p style="color:red">圖片僅供參考，實物以本公司為準</p>
						</li>-->

						<?php else : ?>
						<?php if ( !isset( $fittingRelay[0]) || $fittingRelay[0]) : ?>
						<li>
							<div class="pimg col-sm-3"><img src="kmfun/view/images/additional_11.jpg" alt=""></div>
							<div class="ptxt col-sm-9">
								<p>兒童安全座椅〔二歲以下〕<span>以日計價(非24小時計算)：100元/日</span></p>
								年齡逾０歲至二歲以下之幼童，應坐於車輛後座之嬰兒用座椅。<br/><br/><br/>
								我要租用
								<input type="hidden" id="mobilemoto" value="0">
								<select name="" id="babieschair1">
									<?php for( $i = 0; $i <= $optionCnt+1 ; $i++ ) : ?>
									<option value="<?=$i?>"><?=$i?></option>
									<?php endfor ; ?>
								</select>
								個
							</div>
							<p style="color:red">圖片僅供參考，實物以本公司為準</p>
						</li>
						<?php else : ?>
						<input type="hidden" id="babieschair1" value="0">
						<?php endif ; ?>
						<?php if ( !isset( $fittingRelay[1]) || $fittingRelay[1]) : ?>
						<li>
							<div class="pimg col-sm-3"><img src="kmfun/view/images/additional_1.jpg" alt=""></div>
							<div class="ptxt col-sm-9">
								<p>兒童安全座椅〔二歲以上〕<span>以日計價(非24小時計算)：100元/日</span></p>
								年齡逾二歲至四歲以下且體重在十公斤以上至十八公斤以下之幼童，應坐於車輛後座之幼童用座椅; 年齡四歲以下之幼童如因體型特殊顯無法依規定使用者，得選用適當之安全椅。<br/><br/><br/>
								我要租用
								<select name="" id="babieschair2">
									<?php for( $i = 0; $i <= $optionCnt+1 ; $i++ ) : ?>
									<option value="<?=$i?>"><?=$i?></option>
									<?php endfor ; ?>
								</select>
								個
							</div>
							<p style="color:red">圖片僅供參考，實物以本公司為準</p>
						</li>
						<?php else : ?>
						<input type="hidden" id="babieschair2" value="0">
						<?php endif ; ?>
						<?php if ( !isset( $fittingRelay[2]) || $fittingRelay[2]) : ?>
						<li>
							<div class="pimg col-sm-3"><img src="kmfun/view/images/additional_12.jpg" alt=""></div>
							<div class="ptxt col-sm-9">
								<p>兒童安全座椅〔增高墊〕<span>以日計價(非24小時計算)：100元/日</span></p>
								年齡逾四歲以上之幼童，如因身高等特殊需求之使用者，得選用適當之增高墊。<br/><br/><br/>
								我要租用
								<select name="" id="babieschair3">
									<?php for( $i = 0; $i <= $optionCnt+1 ; $i++ ) : ?>
									<option value="<?=$i?>"><?=$i?></option>
									<?php endfor ; ?>
								</select>
								個
							</div>
							<p style="color:red">圖片僅供參考，實物以本公司為準</p>
						</li>
						<?php else : ?>
						<input type="hidden" id="babieschair3" value="0">
						<?php endif ; ?>
						<?php if ( !isset( $fittingRelay[3]) || $fittingRelay[3]) : ?>
						<li>
							<div class="pimg col-sm-3"><img src="kmfun/view/images/additional_2.jpg" alt=""></div>
							<div class="ptxt col-sm-9">
								<p>嬰兒推車<span>以日計價(非24小時計算)：100元/日</span></p>
								<select name="" id="childrenchair">
									<?php for( $i = 0; $i <= $optionCnt+1 ; $i++ ) : ?>
									<option value="<?=$i?>"><?=$i?></option>
									<?php endfor ; ?>
								</select>
								個
							</div>
							<p style="color:red">圖片僅供參考，實物以本公司為準</p>
						</li>
						<?php else : ?>
						<input type="hidden" id="childrenchair" value="0">
						<?php endif ; ?>
						<li>
							<div class="pimg col-sm-3"><img src="kmfun/view/images/additional_3.jpg" alt=""></div>
							<div class="ptxt col-sm-9">
								<p>GPS衛星導航系統<span>$0 / 日均</span></p>
								<span class="text-danger">2020.10.01起租用導航費用調整為100元/日，費用將於辦理取車手續時收取。</span><br/><br/>
								我要租用
								<select name="" id="gps">
									<?php for( $i = 0; $i <= $optionCnt ; $i++ ) : ?>
									<option value="<?=$i?>"><?=$i?></option>
									<?php endfor ; ?>
								</select>
								個
							</div>
							<p style="color:red">圖片僅供參考，實物以本公司為準</p>
						</li>
						<?php endif ; ?>
					</ul>
					<div class="button-line">
						<button class="clear" type="button" onclick="javascript:history.back();">上一步</button><button class="send bt_rental" type="button" >下一步</button>
					</div>
				</div>
			</form>
	    </div><!--wrapper-->
<input type="hidden" id="qid" value="<?=$rentInfo['qid']?>">
<input type="hidden" id="s1" value="<?=$rentInfo['s1']?>">
<input type="hidden" id="s2" value="<?=$rentInfo['s2']?>">
<input type="hidden" id="d1" value="<?=$rentInfo['d1']?>">
<input type="hidden" id="d2" value="<?=$rentInfo['d2']?>">
<input type="hidden" id="t1" value="<?=$rentInfo['t1']?>">
<input type="hidden" id="t2" value="<?=$rentInfo['t2']?>">

<input type="hidden" id="adults" value="<?=$rentInfo['adults']?>">
<input type="hidden" id="children" value="<?=$rentInfo['children']?>">
<input type="hidden" id="babies" value="<?=$rentInfo['babies']?>">
<input type="hidden" id="carIdx" value="<?=$rentInfo['carIdx']?>">

<script type="text/javascript"><!--
$( '.bt_rental').on('click', function () {
	var carIdx = $('#carIdx').val() ;
	var s1 = $('#s1').val() ;
	var s2 = $('#s2').val() ;
	var d1 = $('#d1').val() ;
	var d2 = $('#d2').val() ;
	var t1 = $('#t1').val() ;
	var t2 = $('#t2').val() ;
	var qid = $('#qid').val() ;

	var adults = $('#adults').val() ;
	var children = $('#children').val() ;
	var babies = $('#babies').val() ;

	var mm  = $('#mobilemoto').val() ;
	var bc  = $('#babieschair1').val() ;
	var bc1 = $('#babieschair2').val() ;
	var bc2 = $('#babieschair3').val() ;
	var cc  = $('#childrenchair').val() ;
	var gps = $('#gps').val() ;
	console.log( $('#babieschair1').val());
	console.log( $('#babieschair2').val());
	console.log( $('#babieschair3').val());

	location.href = "?route=order/rental/step3&token=<?=$token?>&s1="+s1+"&s2="+s2+"&d1="+d1+"&d2="+d2+"&t1="+t1+"&t2="+t2+"&adults="+adults+"&children="+children+"&babies="+babies+"&carIdx="+carIdx+"&bc="+bc+"&bc1="+bc1+"&bc2="+bc2+"&cc="+cc+"&gp="+gps+"&mm="+mm+"&qid="+qid ;
});
--></script>
<?php echo $footer; ?>