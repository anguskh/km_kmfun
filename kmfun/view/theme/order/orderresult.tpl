<?php echo $header; ?>
<header></header>
	<div class="wrapper container">
		<div class="container-fluid rental-info">
			<p class="welcome">訂單編號<span><?=$orderInfo["agreement_no"]?></span></p>
	    	<ul>
	    		<li class="col-md-4 col-sm-6">
					<label for="">取車</label><br/>
					<?=$orderInfo["rent_date_out"]?>
					<span class="glyphicon glyphicon-chevron-right to"></span>
					<span class="glyphicon glyphicon-chevron-down to"></span>
	    		</li>
	    		<li class="col-md-4 col-sm-6">
					<label for="">還車</label><br/>
					<?=$orderInfo["rent_date_in_plan"]?>
	    		</li>
	    		<li class="col-md-4 col-sm-12">
	    			<ul>
	    				<li>訂車數量<span class="num"><?=$orderInfo["total_car"]?></span></li>
	    				<li>成人<span class="num"><?=$orderInfo["adult"]?></span></li>
	    				<li>0~1歲<span class="num"><?=$orderInfo["baby"]?></span></li>
	    				<li>1~4歲<span class="num"><?=$orderInfo["children"]?></span></li>
	    			</ul>
	    		</li>
	    	</ul>
	    </div>
    	<div class="vehicle-list container-fluid">
    		<div class="tab-content">
				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td class="col-lg-4 col-sm-3 col-xs-12">
							<a href="#"><img src="<?=$carSeed["thumb"]?>" alt=""></a>
						</td>
						<td class="col-lg-5 col-sm-6 col-xs-12">
							<a href=""><?=$carSeed["car_seed"]?></a><br/>
							<?=$carSeed["car_door"]?>門 | <?=$carsSeed["at_mt"]?> | 乘坐<?=$carSeed["car_peopele"]?>人
						</td>
						<td class="col-lg-3 col-sm-3 col-xs-12">
							<ul>
								<li>
									<!--<span class="glyphicon glyphicon-ok"></span> 訂車數量 <span class="num"><?=$orderInfo["total_car"]?></span> 台<br/>-->
									<span class="glyphicon glyphicon-ok"></span> 0~1歲幼童座椅 <span class="num"><?=$orderInfo["baby_chair"]?></span> 個<br/>
									<span class="glyphicon glyphicon-ok"></span> 1~4歲幼童座椅 <span class="num"><?=$orderInfo["baby_chair1"]?></span> 個<br/>
									<span class="glyphicon glyphicon-ok"></span> GPS衛星導航系統
								</li>
							</ul>
						</td>
					</tr>
					<!--
					<tr>
						<td colspan="3">
							<p class="package-title"><span class="glyphicon glyphicon-briefcase"></span>行李託運預約</p>
							<ul class="package-order">
								<li>
									<label for="">件數：</label>
									5
								</li>
								<li>
									<label for="">船 / 航班時間：</label>
									2017-12-13 11:30
						        </li>
								<li>
									<label for="">收行李時間：</label>
									2017-12-13 08:30
								</li>
								<li>
									<label for="">行李放置地點：</label>
									山外店
								</li>
								<li>
									<label for="">住宿民宿：</label>
									金門縣金湖鎮太湖路二段207號
								</li>
							</ul>
						</td>
					</tr>
					-->
					<tr>
						<td colspan="3">
							<?php
								if ($orderInfo["discountPercentage"] < 1) {
							?>
							<ul class="totalprice">
								<li>
									<b class="price">早鳥優惠 <?=$orderInfo["discountPercentage"] * 10?> 折</b>
								</li>
							</ul>
							<?php
								}
							?>

							<ul class="totalprice">
								<li>
									<b class="price">車價（共 <?=$orderInfo["total_car"]?> 台，每台借車時間為 <?=$orderInfo["rentDay"]?> 天又 <?=$orderInfo["rentHour"]?> 小時），共 <?=$orderInfo["car_price2Str"]?> 元</b>
								</li>
							</ul>
							<ul class="totalprice">
								<li>
									<b class="price">配件合計 <?=$orderInfo["car_price3Str"]?> 元</b>
								</li>
							</ul>
							<ul class="totalprice">
								<li>
									<b class="price">總價（含稅）<?=$orderInfo["totalStr"]?> 元</b>
								</li>
							</ul>
						</td>
					</tr>
				</table>
				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td colspan="3">
							<ul class="totalprice">
								<li>
									<b class="text-danger"><?=$orderInfo["RtnMsg"]?></b>
								</li>
							</ul>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="renter-info container-fluid">
			<div class="renter-box">
				<table cellspacing="0" cellpadding="0" border="0" class="renter finished">
					<tr>
						<th colspan="4" class="subtitle">承租人資料</th>
					</tr>
					<tr>
						<th>承租人姓名<span>Renter</span></th>
						<td><?=$orderInfo["rent_user_name"]?></td>
						<th>身分證字號／護照號碼<span>ID / Passwaord No.</span></th>
						<td><?=$orderInfo["rent_user_id"]?></td>
					</tr>
					<tr>
						<th>出生年月日<span>Date of Birth</span></th>
						<td><?=$orderInfo["rent_user_born"]?></td>
						<th>手機<span>Mobile</span></th>
						<td><?=$orderInfo["rent_user_tel"]?></td>
					</tr>
					<tr>
						<th colspan="4" class="subtitle">取車人資料</th>
					</tr>
					<tr>
						<th>取車人姓名<span>Renter</span></th>
						<td><?=$orderInfo["get_user_name"]?></td>
						<th>身分證字號／護照號碼<span>ID / Passwaord No.</span></th>
						<td><?=$orderInfo["get_user_id"]?></td>
					</tr>
					<tr>
						<th>出生年月日<span>Date of Birth</span></th>
						<td><?=$orderInfo["get_user_born"]?></td>
						<th>手機<span>Mobile</span></th>
						<td><?=$orderInfo["get_user_tel"]?></td>
					</tr>
					<tr>
						<th colspan="4" class="subtitle">其他通訊資料</th>
					</tr>
					<tr>
						<th>聯絡地址<span>Address</span></th>
						<td colspan="3"><?=$orderInfo["rent_user_address"]?></td>
					</tr>
					<tr>
						<th>公司名稱<span>Company</span></th>
						<td><?=$orderInfo["rent_company_name"]?></td>
						<th>公司統編<span>VAT No.</span></th>
						<td><?=$orderInfo["rent_company_no"]?></td>
					</tr>
					<tr>
						<th>緊急連絡人<span>Emergency Contact</span></th>
						<td><?=$orderInfo["urgent_man"]?></td>
						<th>緊急連絡人電話<span>Emergency Tel</span></th>
						<td><?=$orderInfo["urgent_mobile"]?></td>
					</tr>
				</table>
			</div>
		</div>
		<div class="button-line">

		</div>
	</div>
<?php echo $footer; ?>