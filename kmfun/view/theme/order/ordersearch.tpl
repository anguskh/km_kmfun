<?php echo $header; ?>
<header></header>
	<div class="wrapper container">
		<div class="container-fluid order-search">
            <div class="container-fluid title bnb">訂單查詢</div>
			<table cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td colspan="3">
						<ul class="totalprice">
							<li><b class="text-danger"><?=$RtnMsg?></b></li>
						</ul>
					</td>
				</tr>
			</table>

            <form action="">
                <div class="order-search-box">
                    <p>
                        <label>承租人手機號碼</label>
                        <input type="text" id="mobile">
                    </p>
<!--                    <p>
                        <label>承租人生日</label>
                        <input type="text" name="datepicker" id="bdate" value="" />
                        <script type="text/javascript">
                            $(function() {
                                $('input[name="datepicker"]').daterangepicker({
                                	locale: {
								    	format: 'YYYY-MM-DD'
								    },
                                    singleDatePicker: true,
                                    showDropdowns: true
                                });
                            });
                        </script>
                    </p>-->
                    <p>
                        <label>訂單編號</label>
                        <input type="text" id="order_sn">
                    </p>
                </div>
			    <div class="button-line">
                    <button class="search-btn" type="button" id="bt_search" onclick="">送出查詢</button>
                </div>
            </form>
	    </div>
	</div>
<script type="text/javascript"><!--
$('#bt_search').on('click', function () {
	var m1 = $('#mobile').val() ;
	var o1 = $('#order_sn').val() ;
	var msg = "" ;

	if ( m1 == "") { msg += "承租人手機號碼未填寫\n" ; }
	if ( o1 == "") { msg += "訂單編號未填寫\n" ; }
	if ( msg != "") { alert( msg) ; return false ; }

	location.href = "?route=order/ordersearch/result&m1="+m1+"&o1="+o1 ;
}) ;
--></script>
<?php echo $footer; ?>