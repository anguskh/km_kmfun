<?php echo $header; ?>
		<header></header>
	    <div class="wrapper container">
	    	<div class="container-fluid step">
	    		<span class="<?=$stepFlag[0]?>">1</span>選擇車輛<span class="glyphicon glyphicon-menu-right"></span>
	    		<span class="<?=$stepFlag[1]?>">2</span>額外服務<span class="glyphicon glyphicon-menu-right"></span>
	    		<span class="<?=$stepFlag[2]?>">3</span>駕駛人資料<span class="glyphicon glyphicon-menu-right"></span>
	    		<span class="<?=$stepFlag[3]?>">4</span>完成訂車
	    	</div>
	    	<div class="container-fluid rental-info">
		    	<ul>
		    		<li class="col-md-4 col-sm-6">
						<label for="">取車</label><br/>
						<?=$rentInfo['startDate']?>
						<span class="glyphicon glyphicon-chevron-right to"></span>
						<span class="glyphicon glyphicon-chevron-down to"></span>
		    		</li>
		    		<li class="col-md-4 col-sm-6">
						<label for="">還車</label><br/>
						<?=$rentInfo['endDate']?>
		    		</li>
		    		<li class="col-md-4 col-sm-12">
		    			<ul>
		    				<li>訂車數量<span class="num"><?=$rentInfo['b_car_num']?></span></li>
		    				<li>成人<span class="num"><?=$rentInfo['adults']?></span></li>
		    				<li>0~1歲<span class="num"><?=$rentInfo['babies']?></span></li>
		    				<li>1~4歲<span class="num"><?=$rentInfo['children']?></span></li>
		    			</ul>
		    		</li>
		    	</ul>
		    </div>

	    	<div class="vehicle-list container-fluid">
	    		<ul class="nav nav-tabs">
				    <li class="col-sm-2 col-xs-4 active">
				    	<a data-toggle="tab" href="#menu00">
				    		<img src="image/catalog/car_model/type_7.jpg" alt="全部車系">全部車系<br>&nbsp;</a>
				    </li>
	    		<?php foreach( $carModels as $cnt => $carModel) :
	    				if ( isset($carSeeds[$carModel['idx']])) {
	    					$nowStr = "目前有車" ;
	    				} else {
	    					$nowStr = "<b class=\"fully-booked\">訂車額滿</b>" ;
	    				}
	    		?>
					<!-- <li class="col-sm-2 col-xs-4 <?=( $cnt==0) ? 'active' : ''?> "> -->
					<li class="col-sm-2 col-xs-4">
				    	<a data-toggle="tab" href="#menu<?=$carModel['idx']?>">
				    		<img src="<?=$carModel['opt_short_desc']?>" alt="<?=$carModel['opt_name']?>"><?=$carModel['opt_name']?><br><?=$nowStr?></a>
				    </li>
				<?php endforeach ; ?>
				</ul>
				<div class="tab-content">
<div id="menu00" class="tab-pane fade in active">
<?php
	foreach( $carModels as $cnt => $carModel) :
		if ( isset( $carSeeds[$carModel['idx']])) :
			foreach( $carSeeds[$carModel['idx']] as $iCnt => $carSeed) :
				$carMode = ( $carSeed['idx'] == 18) ? "手排" : "自排" ;
?>
	<table cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td class="col-lg-3 col-sm-3 col-xs-12 white">
				<a href="#"><img src="<?=$carSeed['thumb']?>" alt=""></a>
				<span>圖片僅供參考</span>
			</td>
			<td class="col-lg-5 col-sm-6 col-xs-12">
				<a href=""><?=$carSeed['car_seed']?></a><br/>
<?php
//	( isset($carSeed['car_seed'])) ? "{$carSeed['car_door']}門 | " : '' ;
//	( isset($carSeed['car_peopele'])) ? echo "乘坐 {$carSeed['car_peopele']}人" : '' ;


?>
				<?=$carSeed['car_door']?>門 | <?=$carMode?> | 乘坐 <?=$carSeed['car_peopele']?>人<br>
				<?php if ( $carSeed['car_model'] == 7) : ?>
				<span style="display:block; margin: 10px 0 0 0; padding: 3px 0; text-align: center; color:#cc0000; background:#ffffcc;">依台灣現行法令，電動自行車無法雙載</span>
				<?php endif ; ?>

			</td>
			<td class="col-lg-3 col-sm-3 col-xs-12">
					單價（含稅）<br/>
					<b class="price"><?=$carSeed['car_unit_price']?>元 / 天</b><br/><br/>
					<span>總價（共計 <?=$rentInfo['rentDay']?> 天又 <?=$rentInfo['rentHour']?> 小時）</span><br/>
					<?php if ( $rentInfo['DiscountPercentage'] < 1) : ?>
					<span>早鳥優惠 <?=str_replace('0', '', $rentInfo['DiscountPercentage'] * 100)?> 折</span><br/>
					<?php endif ; ?>
					<b class="total-price"><?=$carSeed['car_price2']?>元</b>
 			</td>
		</tr>
		<tr>
			<td colspan="3">
				<ul>
					<li class="col-sm-6">
						*訂車前，請詳閱<a href="#">服務條款</a><br/>
						*燃油政策：<b>由承租人自備，還車時油量不得少於出車時油量</b>
                        <br>【注意事項】
                        <ol>
                            <li>本公司訂車系統如租還車時間一樣，可同時下訂同款同型車輛，如租/還車及車款不同請分開下單；</li>
                            <li>訂單如為多筆車輛，本合約以租車人為主要合約人，同行候需提供合格駕駛執照作為核對之依據，車輛如發生任何毀損情形本公司仍以租車人作為合約執行對象，如無法接受，請分開單訂車，避免不必要之糾紛。</li>
                        </ol>
					</li>
					<li class="col-sm-6">
						<!-- <?php if ($carModel['idx'] != 7 && $carModel['idx'] != 2 && $carModel['idx'] != 6) : ?>
						我們提供以下服務：<br/>
						<span class="glyphicon glyphicon-ok"></span> 強制汽車責任險<br/>
						<span class="glyphicon glyphicon-ok"></span> GPS行車記錄器<br/>
						<?php endif ; ?>-->
						<a class="btn bt_rental" href="#" car_idx="<?=$carSeed['idx']?>" p2="<?=$carSeed['car_price2']?>">立即訂車</a>
					</li>
				</ul>
			</td>
		</tr>
	</table>


<?php
			endforeach ;
		endif ;
	endforeach ;
?>
</div>

<?php foreach( $carModels as $cnt => $carModel) : ?>
<!-- <div id="menu<?=$carModel['idx']?>" class="tab-pane fade <?=( $cnt==0) ? 'in active' : ''?> "> -->
<div id="menu<?=$carModel['idx']?>" class="tab-pane fade">
	<?php if ( isset($carSeeds[$carModel['idx']])) : ?>
	<?php foreach( $carSeeds[$carModel['idx']] as $iCnt => $carSeed) :
		$carMode = ( $carSeed['idx'] == 18) ? "手排" : "自排" ;
	?>
	<table cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td class="col-lg-3 col-sm-3 col-xs-12 white">
				<a href="#"><img src="<?=$carSeed['thumb']?>" alt=""></a>
				<span>圖片僅供參考</span>
			</td>
			<td class="col-lg-5 col-sm-6 col-xs-12">
				<a href=""><?=$carSeed['car_seed']?></a><br/>
<?php
//	( isset($carSeed['car_seed'])) ? "{$carSeed['car_door']}門 | " : '' ;
//	( isset($carSeed['car_peopele'])) ? echo "乘坐 {$carSeed['car_peopele']}人" : '' ;
?>
				<?=$carSeed['car_door']?>門 | <?=$carMode?> | 乘坐 <?=$carSeed['car_peopele']?>人<br>
				<?php if ( $carSeed['car_model'] == 7) : ?>
				<span style="display:block; margin: 10px 0 0 0; padding: 3px 0; text-align: center; color:#cc0000; background:#ffffcc;">依台灣現行法令，電動自行車無法雙載</span>
				<?php endif ; ?>
			</td>
			<td class="col-lg-3 col-sm-3 col-xs-12">
				單價（含稅）<br/>
				<b class="price"><?=$carSeed['car_unit_price']?>元 / 天</b><br/><br/>
				<span>總價（共計<?=$rentInfo['rentDay']?>天又<?=$rentInfo['rentHour']?>小時）</span><br/>
				<?php if ( $rentInfo['DiscountPercentage'] < 1) : ?>
				<span>早鳥優惠 <?=$rentInfo['DiscountPercentage'] * 100?>折</span><br/>
				<?php endif ; ?>

				<b class="total-price"><?=$carSeed['car_price2']?>元</b>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<ul>
					<li class="col-sm-6">
						*訂車前，請詳閱<a href="#">服務條款</a><br/>
						*燃油政策：<b>由承租人自備，還車時油量不得少於出車時油量</b>
					</li>
					<li class="col-sm-6">
						<!-- <?php if ($carModel['idx'] != 7 && $carModel['idx'] != 2 && $carModel['idx'] != 6) : ?>
						我們提供以下服務：<br/>
						<span class="glyphicon glyphicon-ok"></span> 強制汽車責任險<br/>
						<span class="glyphicon glyphicon-ok"></span> GPS行車記錄器<br/>
						<?php endif ; ?>-->
						<a class="btn bt_rental" href="#" car_idx="<?=$carSeed['idx']?>" p2="<?=$carSeed['car_price2']?>" >立即訂車</a>
					</li>
				</ul>
			</td>
		</tr>
	</table>
	<?php endforeach ; ?>
	<?php else : ?>
						<div class="fully-booked-info alert alert-danger">
							<span class="glyphicon glyphicon-warning-sign"></span>目前車輛額滿中，如有任何租車需求可洽客服082-371888轉1線，或加Line@ID：kmfun，由1對1線上客服聯絡，謝謝您！<br/>
							Sorry, fully booked! If you have any problem, please e-mail to us kmfun999@gmail.com, thank you.</div>
	<?php endif ; ?>
</div>
<?php endforeach ; ?>
				    </div>
				</div>
			</div>
	    </div><!--wrapper-->

<input type="hidden" id="qid" value="<?=$rentInfo['qid']?>">
<input type="hidden" id="s1" value="<?=$rentInfo['s1']?>">
<input type="hidden" id="s2" value="<?=$rentInfo['s2']?>">
<input type="hidden" id="d1" value="<?=$rentInfo['d1']?>">
<input type="hidden" id="d2" value="<?=$rentInfo['d2']?>">
<input type="hidden" id="t1" value="<?=$rentInfo['t1']?>">
<input type="hidden" id="t2" value="<?=$rentInfo['t2']?>">

<input type="hidden" id="adults" value="<?=$rentInfo['adults']?>">
<input type="hidden" id="children" value="<?=$rentInfo['children']?>">
<input type="hidden" id="babies" value="<?=$rentInfo['babies']?>">
<input type="hidden" id="coupon" value="<?=$rentInfo['coupon']?>">
<script type="text/javascript"><!--
$( '.bt_rental').on('click', function () {
	var carIdx = $(this).attr('car_idx') ;
	var s1 = $('#s1').val() ;
	var s2 = $('#s2').val() ;
	var d1 = $('#d1').val() ;
	var d2 = $('#d2').val() ;
	var t1 = $('#t1').val() ;
	var t2 = $('#t2').val() ;
	var qid = $('#qid').val() ;

	var adults   = $('#adults').val() ;
	var children = $('#children').val() ;
	var babies   = $('#babies').val() ;
	var p2       = $(this).attr('p2') ;

	location.href = "?route=order/rental/step2&token=<?=$token?>&s1="+s1+"&s2="+s2+"&d1="+d1+"&d2="+d2+"&t1="+t1+"&t2="+t2+"&adults="+adults+"&children="+children+"&babies="+babies+"&carIdx="+carIdx+"&p2="+p2+"&qid="+qid;
});
--></script>
<?php echo $footer; ?>