<?php echo $header; ?>
		<header></header>
		<script src="kmfun/view/js/daterangepicker2.js?<?=time()?>"></script>
		<div class="wrapper container">
			<div class="container-fluid step">
	    		<span class="<?=$stepFlag[0]?>">1</span>選擇車輛<span class="glyphicon glyphicon-menu-right"></span>
	    		<span class="<?=$stepFlag[1]?>">2</span>額外服務<span class="glyphicon glyphicon-menu-right"></span>
	    		<span class="<?=$stepFlag[2]?>">3</span>駕駛人資料<span class="glyphicon glyphicon-menu-right"></span>
	    		<span class="<?=$stepFlag[3]?>">4</span>完成訂車
	    	</div>
			<div class="container-fluid rental-info">
		    	<ul>
		    		<li class="col-md-4 col-sm-6">
						<label for="">取車</label><br/>
						<?=$rentInfo['startDate']?>
						<span class="glyphicon glyphicon-chevron-right to"></span>
						<span class="glyphicon glyphicon-chevron-down to"></span>
		    		</li>
		    		<li class="col-md-4 col-sm-6">
						<label for="">還車</label><br/>
						<?=$rentInfo['endDate']?>
		    		</li>
		    		<li class="col-md-4 col-sm-12">
		    			<ul>
		    				<li>訂車數量<span class="num"><?=$rentInfo['b_car_num']?></span></li>
		    				<li>成人<span class="num"><?=$rentInfo['adults']?></span></li>
		    				<li>0~1歲<span class="num"><?=$rentInfo['babies']?></span></li>
		    				<li>1~4歲<span class="num"><?=$rentInfo['children']?></span></li>
		    			</ul>
		    		</li>
		    	</ul>
		    </div>
	    	<div class="vehicle-list container-fluid">
	    		<div class="tab-content">
					<table cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td class="col-lg-4 col-sm-3 col-xs-12">
								<a href="#"><img src="<?=$rentInfo['carSeed']['thumb']?>" alt=""></a>
							</td>
							<td class="col-lg-4 col-sm-3 col-xs-12">
								<a href=""><?=$rentInfo['carSeed']['car_seed']?></a><br/>
								<?php $carMode = ( $rentInfo['carSeed']['idx'] == 18) ? "手排" : "自排" ; ?>
								<?=$rentInfo['carSeed']['car_door']?>門 | <?=$carMode?>  | 乘坐<?=$rentInfo['carSeed']['car_peopele']?>人
							</td>
							<td class="col-lg-6 col-sm-1 col-xs-12">
								<ul>
									<li>
										<?php if($rentInfo['bc'] > 0) { ?>
											<span class="glyphicon glyphicon-ok"></span> 兒童座椅〔一歲以下〕 <span class="num"><?=$rentInfo['bc']?></span> 個，合計 <span class="num"><?=$rentInfo['bc']*100*$rentInfo['useDay']?></span> 元<br/>
										<?php } ?>
										<?php if($rentInfo['bc1'] > 0) { ?>
											<span class="glyphicon glyphicon-ok"></span> 兒童座椅〔一歲以上〕 <span class="num"><?=$rentInfo['bc1']?></span> 個，合計 <span class="num"><?=$rentInfo['bc1']*100*$rentInfo['useDay']?></span> 元<br/>
										<?php } ?>
										<?php if($rentInfo['bc2'] > 0) { ?>
											<span class="glyphicon glyphicon-ok"></span> 兒童座椅〔增高墊〕 <span class="num"><?=$rentInfo['bc2']?></span> 個，合計 <span class="num"><?=$rentInfo['bc2']*100*$rentInfo['useDay']?></span> 元<br/>
										<?php } ?>
										<?php if($rentInfo['cc'] > 0) { ?>
											<span class="glyphicon glyphicon-ok"></span> 嬰兒推車 <span class="num"><?=$rentInfo['cc']?></span> 個，合計 <span class="num"><?=$rentInfo['cc']*100*$rentInfo['useDay']?></span> 元<br/>
										<?php } ?>
										<?php if($rentInfo['gps'] > 0) { ?>
											<span class="glyphicon glyphicon-ok"></span> GPS衛星導航系統 <span class="num"><?=$rentInfo['gps']?></span> 個，合計 <span class="num"><?=$rentInfo['gps']*0*$rentInfo['useDay']?></span> 元<br/>
										<?php } ?>
										<?php if($rentInfo['mm'] > 0) { ?>
											<span class="glyphicon glyphicon-ok"></span> 機車導航架 <span class="num"><?=$rentInfo['mm']?></span> 個，合計 <span class="num"><?=$rentInfo['mm']*50*$rentInfo['useDay']?></span> 元<br/>
										<?php } ?>
									</li>
								</ul>
							</td>
						</tr>
						<tr>
							<td colspan="3">
								<ul class="totalprice">
									<?php if ( $rentInfo['DiscountPercentage'] < 1) : ?>
									<li>早鳥優惠 <?=$rentInfo['DiscountPercentage'] * 100?>折</li>
									<?php endif ; ?>
									<li>
										車價（共<?=$rentInfo['b_car_num']?>台，每台借車時間為<?=$rentInfo['rentDay']?>天又<?=$rentInfo['rentHour']?>小時），共 <b class="price"><?=$rentInfo['car_price2Str']?>元</b>
									</li>
									<li>
										配件合計 <b class="price"><?=$rentInfo['car_price3Str']?>元</b>
									</li>
									<li>
										總價（含稅）<b class="price"><?=$rentInfo['totalStr']?>元</b>
									</li>
								</ul>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="container">
			<?php if ($error_warning) { ?>
				<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
					<button type="button" class="close" data-dismiss="alert">&times;</button>
				</div>
			<?php } ?>
			</div>
			<form action="?<?=$actStr?>" method="post" id="target">
				<div class="renter-info container-fluid">
					<div class="renter-box">
						<p class="notice">警語：承租人或連絡人若個資不符，本公司有權拒絕出車，請據實填寫資料。<font style="color:red;">國旅卡不適用本服務</font></p>
						<table cellspacing="0" cellpadding="0" border="0" class="renter">
							<tr>
								<th colspan="4" class="subtitle">承租人資料</th>
							</tr>
							
							<tr>
								<th><b>*</b> 承租人姓名<span>Renter</span></th>
								<td><input type="text" name="input_rent_name" value="<?=$rent_name?>"></td>
								<th><b>*</b> 身份別<span>Identity</span></th>
								<td>
									<input type="radio" class="checkinfo" name="input_identity" value="1" <?php echo (($identity==1 || $identity == '')?'checked':'')?>><span class="checkinfo">國內身分證</span>
									<!-- input type="radio" class="checkinfo" name="input_identity" value="2"><span class="checkinfo">中港澳</span -->
									<input type="radio" class="checkinfo" name="input_identity" value="3" <?php echo (($identity==3)?'checked':'')?>><span class="checkinfo" >其它</span>
								</td>

							</tr>
							<tr>
								<th><b>*</b> 出生年月日<span>Date of Birth</span></th>
								<td>
 									<input type="text" name="input_bornday" value="<?=$bornday?>" class="birth">
									<span class="datepicker">
						            	<span class="glyphicon glyphicon-calendar"></span>
						            </span>
									<script type="text/javascript">
										$(function() {
										    $('input[name="input_bornday"]').daterangepicker({
										        locale: {
											    	format: 'YYYY-MM-DD'
											    },
												singleDatePicker: true,
												showDropdowns: true,
										    });
										});
									</script><!--20170719 新增showDropdowns: true-->
								</td>
								<th><b>*</b> 身分證字號／護照號碼<span>ID / Passwaord No.</span></th>
								<td><input type="text" name="input_rent_user_id" value="<?=$rent_user_id?>"></td>

							</tr>
							<tr>
								<th><b>*</b> E-Mail</th>
								<td><input type="text" name="input_rent_email" value="<?=$rent_email?>"></td>
								<th><b>*</b> 手機<span>Mobile</span></th>
								<td><input type="text" name="input_mobile" value="<?=$mobile?>"></td>

							</tr>
							<!--
							<tr>
								<th colspan="4" class="subtitle">取車人資料</th>
							</tr>
							<tr>
								<?php $checked = ( $syncrent == "on") ? "checked" : "" ;?>
								<td colspan="2"><input type="checkbox" class="checkinfo" name="syncrent" <?=$checked?>><span class="checkinfo">同承租人資料</span></td>
								<th><b>*</b> 身份別<span>Identity</span></th>
								<td>
									<input type="radio" class="checkinfo" name="input_get_identity" value="1" <?php echo (($get_identity==1 || $get_identity == '')?'checked':'')?>><span class="checkinfo">國內身分證</span>
							-->
									<!-- input type="radio" class="checkinfo" name="input_get_identity" value="2"><span class="checkinfo">中港澳</span> -->
							<!--
									<input type="radio" class="checkinfo" name="input_get_identity" value="3" <?php echo (($get_identity==3)?'checked':'')?>><span class="checkinfo" >其它</span>
								</td>
							</tr>
							<tr>
								<th><b>*</b> 取車人姓名<span>Renter</span></th>
								<td><input type="text" name="input_get_name" value="<?=$get_name?>"></td>
								<th><b>*</b> 身分證字號／護照號碼<span>ID / Passwaord No.</span></th>
								<td><input type="text" name="input_get_user_id" value="<?=$get_user_id?>"></td>
							</tr>
							<tr>
								<th><b>*</b> 出生年月日<span>Date of Birth</span></th>
								<td>
									<input type="text" name="input_get_bornday" value="<?=$get_bornday?>" class="birth" />
									<span class="datepicker">
						            	<span class="glyphicon glyphicon-calendar"></span>
						            </span>
									<script type="text/javascript">
										$(function() {
										    $('input[name="input_get_bornday"]').daterangepicker({
										        locale: {
											    	format: 'YYYY-MM-DD'
											    },
												singleDatePicker: true,
												showDropdowns: true,
										    });
										});
									</script>
							-->
									<!--20170719 新增showDropdowns: true-->
							<!--
								</td>
								<th><b>*</b> 手機<span>Mobile</span></th>
								<td><input type="text" name="input_get_mobile" value="<?=$get_mobile?>"></td>
							</tr>
							-->
							<tr>
								<th colspan="4" class="subtitle">其他通訊資料</th>

							</tr>
							<tr>
								<th><b>*</b> 通訊地址<span>Address</span></th>
								<td colspan="3">
									<select name="input_rent_zipcode1" id="input_rent_zipcode1">
										<option value="">請選擇</option>
<?php foreach ( $retZip as $iCnt => $tmpOpt) :
		$zipSeled = ( $tmpOpt['zip_code'] == $rent_zipcode1) ? "selected" : "" ;
?>
	<option value="<?=$tmpOpt['zip_code']?>" <?=$zipSeled?>><?=$tmpOpt['zip_name']?></option>
<?php endforeach ;?>
									</select>
									<select name="input_rent_zipcode2" id="input_rent_zipcode2">
										<option value="">請選擇</option>
<?php
	if( isset( $retZip2)) {
		foreach( $retZip2 as $iCnt => $tmpOpt) {
			$zipSeled = ( $tmpOpt['zip_code'] == $rent_zipcode2) ? "selected" : "" ;
			echo "<option value=\"{$tmpOpt['zip_code']}\" {$zipSeled}>{$tmpOpt['zip_name']}</option>" ;
		}
	}
?>

									</select>
									<input type="text" class="long" name="input_rent_address" value="<?=$rent_address?>"/><br/>
									<input type="checkbox" class="checkinfo" id="external" /><span class="checkinfo">其他（國外）</span>
								</td>
							</tr>
							<tr>
								<th>公司名稱<span>Company</span></th>
								<td><input type="text" name="input_company" value="<?=$company?>"></td>
								<th>公司統編<span>VAT No.</span></th>
								<td>
									<input type="text" onfocus="if(this.value=='如需開立統編請填寫')this.value='';" onblur="if(this.value=='')this.value='如需開立統編請填寫'" name="input_company_no" value="<?=$company_no?>">
								</td>
							</tr>
							<tr>
								<th><b>*</b> 緊急連絡人<span>Emergency Contact</span></th>
								<td><input type="text" name="input_urgent_man" value="<?=$urgent_man?>"></td>
								<th><b>*</b> 緊急連絡人電話<span>Emergency Tel</span></th>
								<td><input type="text" name="input_urgent_mobile" value="<?=$urgent_mobile?>"></td>
							</tr>
						</table>
						<table cellspacing="0" cellpadding="0" border="0" class="renter">
							<tr>
								<th rowspan="2">航/船班資訊</th>
								<td rowspan="2">
									<select name="air_port">
										<option value="">請選擇</option>
<?php foreach ( $airListArr as $iCnt => $tmpOpt) :
		// $airSeled = ( $tmpOpt['zip_code'] == $rent_zipcode1) ? "selected" : "" ;
?>
	<option value="<?=$tmpOpt['idx']?>" ><?=$tmpOpt['opt_short_desc']?></option>
<?php endforeach ;?>
										<option value="xx">自取</option>
									</select>
								</td>
								<th>航班日期</th>
								<td>
 									<input type="text" name="air_date" value="" class="birth">
									<span class="datepicker">
						            	<span class="glyphicon glyphicon-calendar"></span>
						            </span>
									<script type="text/javascript">
										$(function() {
											var d1 = new Date() ;
										    $('input[name="air_date"]').daterangepicker({
										        locale: {
											    	format: 'YYYY-MM-DD'
											    },
												singleDatePicker: true,
												showDropdowns: true,
												minDate: d1
										    });
										});
									</script><!--20170719 新增showDropdowns: true-->
								</td>
							</tr>
							</tr>
								<th>起飛/航時間</th>
								<td>
									<select name="air_hour">
										<?php for( $i = 0; $i < 24; $i++) :
											$val = str_pad($i, 2, '0', STR_PAD_LEFT) ;
										?>
										<option value="<?=$val?>"><?=$val?></option>
										<?php endfor ; ?>
									</select> :
									<select name="air_minute">
										<?php for( $i = 0; $i < 12; $i++) :
											$val = str_pad($i * 5, 2, '0', STR_PAD_LEFT) ;
										?>
										<option value="<?=$val?>"><?=$val?></option>
										<?php endfor ; ?>
									</select>
								</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="renter-info container-fluid strongtxt">
					<input type="checkbox" name="ckboxConfirm" value='yes' class="checkinfo">同意（我已詳細閱讀<a href="?route=information/page&token=A2DDDBF2-82AA-C541-F603-64B8ED6CE4D7" target="_blank">租賃契約</a>，並接受上述內容）
				</div>
				<div class="button-line">
					<input type="hidden" name="ecpay_choose_payment" value="Credit">
					<input type="hidden" name="qid" value="<?=$rentInfo['qid']?>">
					<button class="clear" onclick="javascript:history.back();">上一步</button>
					<button type="submit" type="button" class="send disable" disabled >確認送出</button>
				</div>
			</form>
		</div>
<script type="text/javascript"><!--
$( document ).ready(function() {
	$( "input[name='input_bornday']").change( function () {
		var nowTime = Date.parse(new Date().toDateString()) ;
		var birthday = Date.parse( $( "input[name='input_bornday']").val()) ;
		if ( (nowTime - birthday) < 631123200000) {
			alert( "請注意 未滿20歲 不能租車") ;
		}
	}) ;
	$( "input[name='input_get_bornday']").change( function () {
		var nowTime = Date.parse(new Date().toDateString()) ;
		var birthday = Date.parse( $( "input[name='input_get_bornday']").val()) ;
		if ( (nowTime - birthday) < 631123200000) {
			alert( "請注意 未滿20歲 不能租車") ;
		}
	}) ;

	if ( $("select[name='air_port']").val() == "xx") {
		$("input[name ='air_date']").attr('disabled', 'disabled') ;
		$("input[name ='air_hour']").attr('disabled', 'disabled') ;
		$("input[name ='air_minute']").attr('disabled', 'disabled') ;
	}

	$("#target").submit(function( event ) {
		var msg = "取消交易須酌收營業損失\n\n➤ 於預定取車日當日或不通知取消，不退費。\n➤ 於預定取車日前7 日內取消（不含取車當日），收取總費用的18%。\n➤ 於預定取車日前8 日前取消（不含取車當日），可全額退費。\n（但仍收取100元人工作業處理費用）" ;
		return confirm(msg) ;
	});

});

$("select[name='air_port']").change( function () {
	var selVal = $(this).val() ;
	if ( selVal == "xx") {
		$("input[name ='air_date']").attr('disabled', 'disabled') ;
		$("input[name ='air_hour']").attr('disabled', 'disabled') ;
		$("input[name ='air_minute']").attr('disabled', 'disabled') ;
	} else {
		$("input[name ='air_date']").removeAttr('disabled') ;
		$("input[name ='air_hour']").removeAttr('disabled') ;
		$("input[name ='air_minute']").removeAttr('disabled') ;
	}
}) ;



$("input[name='ckboxConfirm']").click( function (){
	if ($(this).prop("checked")) {
		$('button[type=submit]').removeClass('disable') ;
		$('button[type=submit]').attr("disabled", false) ;
	} else {
		$('button[type=submit]').addClass('disable') ;
		$('button[type=submit]').attr("disabled", true) ;
	}
}) ;


// 非同步取回區碼
$("select[name='input_rent_zipcode1']").change( function () {
	var selVal = $(this).val() ;
	$('#input_rent_zipcode2').find('option:not(:first)').remove() ;
	console.log(selVal) ;
	$.ajax({
		url : '?route=order/rental/getSubZip',
		type : 'post',
		data : {zipMain : selVal},
		dataType : 'json',
		success : function (resp) {
			for (var i = 0; i < resp.length; i++) {
				$("#input_rent_zipcode2").append('<option value=' + resp[i].zip_code + '>' + resp[i].zip_code + ' ' + resp[i].zip_name + '</option>') ;
			}
		}
	});
});

$('#external').click( function () {
	if ($(this).is(":checked")) {
		$('#input_rent_zipcode1').find('option:not(:first)').remove() ;
		$('#input_rent_zipcode2').find('option:not(:first)').remove() ;
	} else {
		$.ajax({
			url : '?route=order/rental/getSubZip',
			type : 'post',
			data : {zipMain : ''},
			dataType : 'json',
			success : function (resp) {
				for (var i = 0; i < resp.length; i++) {
					$("#input_rent_zipcode1").append('<option value=' + resp[i].zip_code + '>' + resp[i].zip_name + '</option>') ;
				}
			}
		});
	}
}) ;
--></script>
<?php echo $footer; ?>