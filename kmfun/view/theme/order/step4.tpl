<?php echo $header; ?>
		<header></header>
		<div class="wrapper container">
			<div class="container-fluid step">
	    		<span class="<?=$stepFlag[0]?>">1</span>選擇車輛<span class="glyphicon glyphicon-menu-right"></span>
	    		<span class="<?=$stepFlag[1]?>">2</span>額外服務<span class="glyphicon glyphicon-menu-right"></span>
	    		<span class="<?=$stepFlag[2]?>">3</span>駕駛人資料<span class="glyphicon glyphicon-menu-right"></span>
	    		<span class="<?=$stepFlag[3]?>">4</span>完成訂車
	    	</div>
			<div class="container-fluid rental-info">
				<?php if ( isset($error_warning)) { ?>
					<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
						<button type="button" class="close" data-dismiss="alert">&times;</button>
					</div>
				<?php } ?>
				<?php if ( isset($success)) { ?>
				<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
					<button type="button" class="close" data-dismiss="alert">&times;</button>
				</div>
				<?php } ?>
				<?php if ( isset($rentInfo['agreement_no'])) : ?>
				<!--<p class="welcome">感謝您的預約！<br/>訂單編號為<span><?=$rentInfo['agreement_no']?></span></p>-->
				<?php endif ; ?>
		    	<ul>
		    		<li class="col-md-4 col-sm-6">
						<label for="">取車</label><br/>
						<?=$rentInfo['startDate']?>
						<span class="glyphicon glyphicon-chevron-right to"></span>
						<span class="glyphicon glyphicon-chevron-down to"></span>
		    		</li>
		    		<li class="col-md-4 col-sm-6">
						<label for="">還車</label><br/>
						<?=$rentInfo['endDate']?>
		    		</li>
		    		<li class="col-md-4 col-sm-12">
		    			<ul>
		    				<li>訂車數量<span class="num"><?=$rentInfo['b_car_num']?></span></li>
		    				<li>成人<span class="num"><?=$rentInfo['adults']?></span></li>
		    				<li>0~1歲<span class="num"><?=$rentInfo['babies']?></span></li>
		    				<li>1~4歲<span class="num"><?=$rentInfo['children']?></span></li>
		    			</ul>
		    		</li>
		    	</ul>
		    </div>
	    	<div class="vehicle-list container-fluid">
	    		<div class="tab-content">
					<table cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td class="col-lg-4 col-sm-3 col-xs-12">
								<a href="#"><img src="<?=$rentInfo['carSeed']['thumb']?>" alt=""></a>
							</td>
							<td class="col-lg-4 col-sm-3 col-xs-12">
								<a href=""><?=$rentInfo['carSeed']['car_seed']?></a><br/>
								<?php $carMode = ( $rentInfo['carSeed']['idx'] == 18) ? "手排" : "自排" ; ?>
								<?=$rentInfo['carSeed']['car_door']?>門 | <?=$carMode?> | 乘坐<?=$rentInfo['carSeed']['car_peopele']?>人
							</td>
							<td class="col-lg-6 col-sm-1 col-xs-12">
								<ul>
									<li>
										<?php if($rentInfo['bc'] > 0) { ?>
											<span class="glyphicon glyphicon-ok"></span> 兒童座椅〔一歲以下〕 <span class="num"><?=$rentInfo['bc']?></span> 個，合計 <span class="num"><?=$rentInfo['bc']*100*$rentInfo['useDay']?></span> 元<br/>
										<?php } ?>
										<?php if($rentInfo['bc1'] > 0) { ?>
											<span class="glyphicon glyphicon-ok"></span> 兒童座椅〔一歲以上〕 <span class="num"><?=$rentInfo['bc1']?></span> 個，合計 <span class="num"><?=$rentInfo['bc1']*100*$rentInfo['useDay']?></span> 元<br/>
										<?php } ?>
										<?php if($rentInfo['bc2'] > 0) { ?>
											<span class="glyphicon glyphicon-ok"></span> 兒童座椅〔增高墊〕 <span class="num"><?=$rentInfo['bc2']?></span> 個，合計 <span class="num"><?=$rentInfo['bc2']*100*$rentInfo['useDay']?></span> 元<br/>
										<?php } ?>
										<?php if($rentInfo['cc'] > 0) { ?>
											<span class="glyphicon glyphicon-ok"></span> 嬰兒推車 <span class="num"><?=$rentInfo['cc']?></span> 個，合計 <span class="num"><?=$rentInfo['cc']*100*$rentInfo['useDay']?></span> 元<br/>
										<?php } ?>
										<?php if($rentInfo['gps'] > 0) { ?>
											<span class="glyphicon glyphicon-ok"></span> GPS衛星導航系統 <span class="num"><?=$rentInfo['gps']?></span> 個，合計 <span class="num"><?=$rentInfo['gps']*0*$rentInfo['useDay']?></span> 元<br/>
										<?php } ?>
										<?php if($rentInfo['mm'] > 0) { ?>
											<span class="glyphicon glyphicon-ok"></span> 手機導航架 <span class="num"><?=$rentInfo['mm']?></span> 個，合計 <span class="num"><?=$rentInfo['mm']*50*$rentInfo['useDay']?></span> 元<br/>
										<?php } ?>
									</li>
								</ul>
							</td>
						</tr>
						<tr>
							<td colspan="3">
								<ul class="totalprice">
									<li>
										車價（共<?=$rentInfo['b_car_num']?>台，每台借車時間為<?=$rentInfo['rentDay']?>天又<?=$rentInfo['rentHour']?>小時），共 <b class="price"><?=$rentInfo['car_price2']?>元</b>
									</li>
									<li>
										配件合計 <b class="price"><?=$rentInfo['car_price3']?>元</b>
									</li>
									<li>
										總價（含稅）<b class="price"><?=$rentInfo['total']?>元</b>
									</li>
								</ul>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<form action="">
				<div class="renter-info container-fluid">
					<div class="renter-box">
						<table cellspacing="0" cellpadding="0" border="0" class="renter finished">
							<tr>
								<th colspan="4" class="subtitle">承租人資料</th>
							</tr>
							<tr>
								<th>承租人姓名<span>Renter</span></th>
								<td><?=$rentInfo['input_rent_name']?></td>
								<th>身分證字號／護照號碼<span>ID / Passwaord No.</span></th>
								<td><?=$rentInfo['input_rent_user_id']?></td>
							</tr>
							<tr>
								<th>出生年月日<span>Date of Birth</span></th>
								<td><?=$rentInfo['input_bornday']?></td>
								<th>手機<span>Mobile</span></th>
								<td><?=$rentInfo['input_mobile']?></td>
							</tr>
							<tr>
								<th>E-Mail</th>
								<td><?=$rentInfo['input_rent_email']?></td>
							</tr>
							<tr>
								<th colspan="4" class="subtitle">取車人資料</th>
							</tr>
							<tr>
								<th>承租人姓名<span>Renter</span></th>
								<td><?=$rentInfo['input_get_name']?></td>
								<th>身分證字號／護照號碼<span>ID / Passwaord No.</span></th>
								<td><?=$rentInfo['input_get_user_id']?></td>
							</tr>
							<tr>
								<th>出生年月日<span>Date of Birth</span></th>
								<td><?=$rentInfo['input_get_bornday']?></td>
								<th>手機<span>Mobile</span></th>
								<td><?=$rentInfo['input_get_mobile']?></td>
							</tr>
							<tr>
								<th colspan="4" class="subtitle">其他通訊資料</th>
							</tr>
							<tr>
								<th>聯絡地址<span>Address</span></th>
								<td colspan="3"><?=$rentInfo['input_rent_address']?></td>
							</tr>
							<tr>
								<th>公司名稱<span>Company</span></th>
								<td><?=$rentInfo['input_company']?></td>
								<th>公司統編<span>VAT No.</span></th>
								<td><?=$rentInfo['input_company_no']?></td>
							</tr>
							<tr>
								<th>緊急連絡人<span>Emergency Contact</span></th>
								<td><?=$rentInfo['input_urgent_man']?></td>
								<th>緊急連絡人電話<span>Emergency Tel</span></th>
								<td><?=$rentInfo['input_urgent_mobile']?></td>
							</tr>
						</table>
						<table cellspacing="0" cellpadding="0" border="0" class="renter">
							<tr>
								<th>航/船班資訊</th>
								<td><?=$rentInfo['flight_point']?></td>
								<th>航班日期</th>
								<td><?=$rentInfo['flight_sDate']?></td>
							</tr>
						</table>
					</div>
				</div>
				<div class="button-line">
					<button class="send" type="button" onclick="javascript:location.href='./'">回到首頁</button>
				</div>
			</form>
		</div>

<script type="text/javascript"><!--
--></script>
<?php echo $footer; ?>