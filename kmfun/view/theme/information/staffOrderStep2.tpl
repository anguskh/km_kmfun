<!doctype html>
<html lang="zh-TW">
	<head>
		<meta charset="utf-8">
		<title>金豐線上庫存即時查詢</title>
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<meta name="designer" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

		<link href="kmfun/view/css/bootstrap.css?<?=$cssTime?>" rel="stylesheet"/>
		<script src="kmfun/view/js/jquery-2.1.1.min.js?<?=$cssTime?>"></script>
		<script src="kmfun/view/js/bootstrap.js?<?=$cssTime?>"></script>
        <script src="kmfun/view/js/moment.js?<?=$cssTime?>"></script>

<!--datetimepicker-->
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<!--datetimepicker-->
<!--autocomplete-->
<script src="kmfun/view/js/jquery-ui/jquery-ui.min.js?<?=$cssTime?>"></script>
<link rel="stylesheet" type="text/css" href="kmfun/view/js/jquery-ui/jquery-ui.min.css" />
<!--autocomplete-->


		<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
		<![endif]-->
	</head>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-K42BQN2');</script>
    <!-- End Google Tag Manager -->
</head>
<body id="store">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K42BQN2"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="?route=order/ordersearch/stock">車輛即時查詢</a>
            </div>
        </div>
    </nav>
    <header id="header" class="navbar navbar-static-top"></header>
    <div id="container">
	    <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i>金豐 作業人員 <font color=red>填寫客戶資訊</font></h3>
            </div>
            <?php if ($error_warning) { ?>
                <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>
            <?php } ?>
            <div class="container-fluid">
                <div id="content">
                    <div class="row">
                        <form action="?route=order/staff/step3" id="form1" method="post" onsubmit="return funcSubmit();">
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-order-type"><b>*</b>承租人姓名 :</label>
							<div class="col-sm-10 dt">
								<input type="text" name="rent_name">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-order-type"><b>*</b>身分證字號／護照號碼 :</label>
							<div class="col-sm-10 dt">
								<input type="text" name="rent_id" onKeyUp="up(this)">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-order-type"><b>*</b>出生年月日 :</label>
							<div class="col-sm-10 dt">
								<select name="rent_bron_y">
									<?php for( $i=$sYear; $i > 1930 ; $i-- ) : ?>
										<option value="<?=$i?>"><?=$i?></option>
									<?php endfor ;?>
								</select>
								<select name="rent_bron_m">
									<?php for( $i=1; $i < 13 ; $i++ ) : ?>
										<option value="<?=$i?>"><?=$i?></option>
									<?php endfor ;?>
								</select>
								<select name="rent_bron_d">
									<?php for( $i=1; $i < 32 ; $i++ ) : ?>
										<option value="<?=$i?>"><?=$i?></option>
									<?php endfor ;?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-order-type"><b>*</b>手機 :</label>
							<div class="col-sm-10 dt">
								<input type="text" name="rent_mobile">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-order-type"><b>*</b>E-Mail :</label>
							<div class="col-sm-10 dt">
								<input type="text" name="rent_mail">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-order-type">同承租人 :</label>
							<div class="col-sm-10 dt">
								<input type="checkbox" name="sync">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-order-type"><b>*</b>取車人姓名 :</label>
							<div class="col-sm-10 dt">
								<input type="text" name="get_name">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-order-type"><b>*</b>身分證字號／護照號碼 :</label>
							<div class="col-sm-10 dt">
								<input type="text" name="get_id" onKeyUp="up(this)">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-order-type"><b>*</b>出生年月日 :</label>
							<div class="col-sm-10 dt">
								<select name="get_bron_y">
									<?php for( $i=$sYear; $i > 1930 ; $i-- ) : ?>
										<option value="<?=$i?>"><?=$i?></option>
									<?php endfor ;?>
								</select>
								<select name="get_bron_m">
									<?php for( $i=1; $i < 13 ; $i++ ) : ?>
										<option value="<?=$i?>"><?=$i?></option>
									<?php endfor ;?>
								</select>
								<select name="get_bron_d">
									<?php for( $i=1; $i < 32 ; $i++ ) : ?>
										<option value="<?=$i?>"><?=$i?></option>
									<?php endfor ;?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-order-type"><b>*</b>手機 :</label>
							<div class="col-sm-10 dt">
								<input type="text" name="get_mobile">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-order-type"><b>*</b>E-Mail :</label>
							<div class="col-sm-10 dt">
								<input type="text" name="get_mail">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-order-type">通訊地址 :</label>
							<div class="col-sm-10 dt">
								<select name="rent_zipcode1" id="rent_zipcode1">
										<option value="">請選擇</option>
<?php foreach ( $retZip as $iCnt => $tmpOpt) :
		$zipSeled = ( $tmpOpt['zip_code'] == $rent_zipcode1) ? "selected" : "" ;
?>
	<option value="<?=$tmpOpt['zip_code']?>" <?=$zipSeled?>><?=$tmpOpt['zip_name']?></option>
<?php endforeach ;?>
								</select>
								<select name="rent_zipcode2" id="rent_zipcode2">
									<option value="">請選擇</option>
								</select>
								<input type="text" class="long" name="rent_address" value=""/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-order-type">公司名稱 :</label>
							<div class="col-sm-10 dt">
								<input type="text" name="rent_company">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-order-type">公司統編 :</label>
							<div class="col-sm-10 dt">
								<input type="text" name="rent_company_no">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-order-type">緊急連絡人 :</label>
							<div class="col-sm-10 dt">
								<input type="text" name="urgent_name">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-order-type">緊急連絡人電話 :</label>
							<div class="col-sm-10 dt">
								<input type="text" name="urgent_mobile">
							</div>
						</div>
 						<div class="form-group btn">
							<div class="col-sm-12">
								<button type="submit" id="btSubmit" name="next" class="btn btn-success">下一步</button>
							</div>
						</div>
                        </form>
                    </div>
                </div>
            </div>
	    </div>
    </div>
    <script type="text/javascript">
// 轉大寫
function up(d){
	d.value=d.value.toUpperCase();
}

// 非同步取回區碼
$("select[name='rent_zipcode1']").change( function () {
	var selVal = $(this).val() ;
	$('#rent_zipcode2').find('option:not(:first)').remove() ;
	console.log(selVal) ;
	$.ajax({
		url : '?route=order/rental/getSubZip',
		type : 'post',
		data : {zipMain : selVal},
		dataType : 'json',
		success : function (resp) {
			for (var i = 0; i < resp.length; i++) {
				$("#rent_zipcode2").append('<option value=' + resp[i].zip_code + '>' + resp[i].zip_code + ' ' + resp[i].zip_name + '</option>') ;
			}
		}
	});
});
$("input[name='sync']").click(function(event) {
	if ($(this).is(":checked")) {
		$( "input[name$='get_name']" ).val( $( "input[name='rent_name']").val()) ;
		$( "input[name$='get_id']" ).val( $( "input[name='rent_id']").val()) ;
		$( "input[name$='get_mobile']" ).val( $( "input[name='rent_mobile']").val()) ;
		$( "input[name$='get_mail']" ).val( $( "input[name='rent_mail']").val()) ;
		// $( "select[name='get_bron_y']")[$( "select[name='rent_bron_y'] option:selected").index()].attr("selectedIndex", ) ;
		$( "select[name='get_bron_y']").prop('selectedIndex', $( "select[name='rent_bron_y'] option:selected").index());
		$( "select[name='get_bron_m']").prop('selectedIndex', $( "select[name='rent_bron_m'] option:selected").index());
		$( "select[name='get_bron_d']").prop('selectedIndex', $( "select[name='rent_bron_d'] option:selected").index());
	} else {
		$( "input[name$='get_name']" ).val( "") ;
		$( "input[name$='get_id']" ).val( "") ;
		$( "input[name$='get_mobile']" ).val( "") ;
		$( "input[name$='get_mail']" ).val( "") ;
		$( "select[name='get_bron_y']").prop('selectedIndex', 0);
		$( "select[name='get_bron_m']").prop('selectedIndex', 0);
		$( "select[name='get_bron_d']").prop('selectedIndex', 0);
	}
});
function funcSubmit() {
	console.log( 'funcSubmit');
	$('#btSubmit').attr("disabled", true) ; // 確保不會再按第二次
	var msg = "" ;
	var rent_name       = $("input[name='rent_name']").val() ;
	var rent_id         = $("input[name='rent_id']").val() ;
	// var rent_bron    = $("input[name='rent_bron']").val() ;
	var rent_mobile     = $("input[name='rent_mobile']").val() ;
	var rent_mail       = $("input[name='rent_mail']").val() ;
	var get_name        = $("input[name='get_name']").val() ;
	var get_id          = $("input[name='get_id']").val() ;
	// var get_bron     = $("input[name='get_bron']").val() ;
	var get_mobile      = $("input[name='get_mobile']").val() ;
	var get_mail        = $("input[name='get_mail']").val() ;
	// var rent_address = $("input[name='rent_address']").val() ;

	if ( !rent_name) {
		msg += "承租人姓名 未填寫\n" ;
	}
	if ( !rent_id) {
		msg += "承租人身分證字號 未填寫\n" ;
	}
	console.log( checkID( rent_id));
	if ( !checkID( rent_id)) {
		msg += "承租人身分證字號"+rent_id+" 錯誤\n" ;
	}
	if ( !rent_mobile) {
		msg += "承租人電話 未填寫" ;
	}
	if ( !rent_mail) {
		msg += "承租人email 未填寫" ;
	}
	if ( !get_name) {
		msg += "" ;
	}
	if ( !get_id) {
		msg += "" ;
	}

	if ( msg) {
		// 未完成 請重新填寫
		$('#btSubmit').attr("disabled", false) ;
		alert( msg) ;
		return false ;
	}
	return true ;
}

// 檢查身份証是否正確
function checkID( id ) {
	tab = "ABCDEFGHJKLMNPQRSTUVXYWZIO"
	A1 = new Array (1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3 );
	A2 = new Array (0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5 );
	Mx = new Array (9,8,7,6,5,4,3,2,1,1);

	if ( id.length != 10 ) return false;
	i = tab.indexOf( id.charAt(0) );
	if ( i == -1 ) return false;
	sum = A1[i] + A2[i]*9;

	for ( i=1; i<10; i++ ) {
		v = parseInt( id.charAt(i) );
		if ( isNaN(v) ) return false;
		sum = sum + v * Mx[i];
	}
	if ( sum % 10 != 0 ) return false;
	return true;
}

   </script>
    <footer style="background: none; color: #777; text-align: center;">2018 © KmFun金豐租車 版權所有</footer>
</body></html>
