<?php echo $header; ?>
<header></header>
	    <div class="wrapper container">
	    	<div class="container-fluid title about">車型介紹</div>
	    	<div class="vehicle-list container-fluid page">
	    		<ul class="nav nav-tabs">
					<?php $strActive = isset( $activeShell) && $activeShell == ''  ? 'active' : "" ; ?>
	    			<li class="col-sm-2 col-xs-4 <?=$strActive?>">
				    	<a data-toggle="tab" href="#menu1">
				    		<img src="image/carinfo/type_7.jpg" alt="">
				    		全部車款
				    	</a>
				    </li>
				<?php foreach( $carModels as $cnt => $carModel) : ?>
					<?php $strActive = isset( $activeShell) && $carModel['idx'] == $activeShell ? 'active' : "" ; ?>
					<!-- <li class="col-sm-2 col-xs-4 <?=( $cnt==0) ? 'active' : ''?> "> -->
					<li class="col-sm-2 col-xs-4 <?=$strActive?>" >
				    	<a data-toggle="tab" href="#menu<?=$carModel['idx']?>">
				    		<img src="<?=$carModel['opt_short_desc']?>" alt="<?=$carModel['opt_name']?>"><?=$carModel['opt_name']?></a>
				    </li>
				<?php endforeach ; ?>

				</ul>
				<?php $strActive = isset( $activeShell) && $activeShell == ''  ? 'in active' : "" ; ?>
				<div class="tab-content">
				    <div id="menu1" class="tab-pane fade <?=$strActive?>">
			    	<?php
					foreach( $carSeeds as $iCnt => $carSeed) :
						foreach ($carSeed['car_seed'] as $key => $value) :
							if ( $value['idx'] != 3) { // 為了不顯示 東庚 電動車
							$carMode = ( $value['idx'] == 18) ? "手排" : "自排" ;
					?>
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="col-lg-3 col-sm-3 col-xs-12 white">
									<a href="#"><img src="<?=$value['thumb']?>" alt=""></a>
									<span>圖片僅供參考</span>
								</td>

								<td class="col-lg-6 col-sm-7 col-xs-12 line">
									<a href=""><?=$value['car_seed']?></a><br/>
									<?php if( $value['car_door'] > 0 && $value['car_peopele'] > 0){ ?>
									<?=$value['car_door']?>門 | <?=$carMode?> | 乘坐<?=$value['car_peopele']?>人<br/><br/>
									<?php }else{ ?>
									&nbsp;<br/><br/>
									<?php } ?>
									<span>單價（含稅）</span><b class="price"><?=$value['car_price']?>元 / 天</b><br>
									<?php if ( $value['car_model'] == 7) : ?>
									<span style="display:block; margin: 10px 0 0 0; padding: 3px 0; text-align: center; color:#cc0000; background:#ffffcc;">依台灣現行法令，電動自行車無法雙載</span>
									<?php endif ; ?>
								</td>
								<td class="col-lg-3 col-sm-2 col-xs-12">
									<a href="?route=information/cardetail&sn=<?=$value['idx']?>" class="btn">詳細介紹</a>
								</td>
							</tr>
						</table>
					<?php
							}
						endforeach ;
					endforeach ;
					?>
				    </div>

				<?php
				foreach( $carModels as $cnt => $carModel) :
					$strActive = isset( $activeShell) && $activeShell == $carModel['idx']  ? 'in active' : "" ;
				?>
				    <div id="menu<?=$carModel['idx']?>" class="tab-pane fade <?=$strActive?>">
					<?php
					foreach( $carSeeds as $iCnt => $carSeed) :
						if ($carSeed['car_model'] == $carModel['idx']) :
						foreach ($carSeed['car_seed'] as $key => $value) :
							if ( $value['idx'] != 3) { // 為了不顯示 東庚 電動車
							$carMode = ( $value['idx'] == 18) ? "手排" : "自排" ;
					?>
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td class="col-lg-3 col-sm-3 col-xs-12 white">
									<a href="#"><img src="<?=$value['thumb']?>" alt=""></a>
									<span>圖片僅供參考</span>
								</td>
								<td class="col-lg-6 col-sm-7 col-xs-12 line">
									<a href=""><?=$value['car_seed']?></a><br/>
									<?php if( $value['car_door'] > 0 && $value['car_peopele'] > 0){ ?>
									<?=$value['car_door']?>門 | <?=$carMode?> | 乘坐<?=$value['car_peopele']?>人<br/><br/>
									<?php }else{ ?>
									&nbsp;<br/><br/>
									<?php } ?>
									<span>單價（含稅）</span><b class="price"><?=$value['car_price']?>元 / 天</b><br>
									<?php if ( $value['car_model'] == 7) : ?>
									<span style="display:block; margin: 10px 0 0 0; padding: 3px 0; text-align: center; color:#cc0000; background:#ffffcc;">依台灣現行法令，電動自行車無法雙載</span>
									<?php endif ; ?>
								</td>
								<td class="col-lg-3 col-sm-2 col-xs-12">
									<a href="?route=information/cardetail&sn=<?=$value['idx']?>" class="btn">詳細介紹</a>
								</td>
							</tr>
						</table>
					<?php
							}
						endforeach;
						endif;
					endforeach;?>
				    </div>
				<?php
				endforeach ;
				?>

				</div>
			</div>
	    </div><!--wrapper-->
<?php echo $footer; ?>