<!doctype html>
<html lang="zh-TW">
	<head>
		<meta charset="utf-8">
		<title>金豐線上庫存即時查詢</title>
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<meta name="designer" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

		<link href="kmfun/view/css/bootstrap.css?<?=$cssTime?>" rel="stylesheet"/>
		<script src="kmfun/view/js/jquery-3.1.1.min.js?<?=$cssTime?>"></script>
		<script src="kmfun/view/js/bootstrap.js?<?=$cssTime?>"></script>
        <script src="kmfun/view/js/moment.js?<?=$cssTime?>"></script><!--datetimepicker-->
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
		<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
		<![endif]-->
	</head>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-K42BQN2');</script>
    <!-- End Google Tag Manager -->
</head>
<body id="store">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K42BQN2"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="/">車輛即時查詢</a>
            </div>
        </div>
    </nav>
    <header id="header" class="navbar navbar-static-top"></header>
    <div id="container">
	    <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i>金豐 工作人員登入</h3>
            </div>
            <?php if ($error_warning) { ?>
                <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>
            <?php } ?>
            <div class="container-fluid">
                <div id="content">
                    <div class="row">
                        <form action="?route=order/ordersearch/stock" id="form1" method="post">
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="sel-order-type">帳號 :</label>
                            <div class="col-sm-10 dt">
                                <input type="text" id="account" name="account" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="sel-order-type">密碼 :</label>
                            <div class="col-sm-10 dt">
                                <input type="password" id="password" name="password" class="form-control">
                            </div>
                        </div>
                        <div class="form-group btn">
                            <div class="col-sm-12">
                                <button type="button" name="login" class="btn btn-success">登入</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
	    </div>
    </div>
    <script type="text/javascript">
        $("button[name='login']").click( function () {
            var account  = $("input[name='account']").val() ;
            var password = $("input[name='password']").val() ;
            console.log(account) ;
            console.log(password) ;

			if ( account != "" && password != "") {
				$('#form1').submit() ;
			} else {
				alert("帳號密碼未填寫") ;
				return false ;
			}

        }) ;
    </script>
    <footer style="background: none; color: #777; text-align: center;">2018 © KmFun金豐租車 版權所有</footer>
</body></html>
