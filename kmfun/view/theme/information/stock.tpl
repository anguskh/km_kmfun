<!doctype html>
<html lang="zh-TW">
	<head>
		<meta charset="utf-8">
		<title>金豐線上庫存即時查詢</title>
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<meta name="designer" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

		<link href="kmfun/view/css/bootstrap.css?<?=$cssTime?>" rel="stylesheet"/>
		<script src="kmfun/view/js/jquery-3.1.1.min.js?<?=$cssTime?>"></script>
		<script src="kmfun/view/js/bootstrap.js?<?=$cssTime?>"></script>
        <script src="kmfun/view/js/moment.js?<?=$cssTime?>"></script><!--datetimepicker-->
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
		<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
		<![endif]-->
	</head>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-K42BQN2');</script>
    <!-- End Google Tag Manager -->
</head>
<body id="store">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K42BQN2"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="?route=order/ordersearch/stock">車輛即時查詢</a>
            </div>
        </div>
    </nav>
    <header id="header" class="navbar navbar-static-top"></header>
    <div id="container">
	    <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i>金豐車輛即時查詢</h3>
            </div>
            <div class="container-fluid">
                <div id="content">
                    <div class="row">
                        <form>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="sel-order-type">開始時間 :</label>
                            <div class="col-sm-10 dt">
                                <p class="dtbox">
                                    <input type="text" id="sDate" name="sDate" class="form-control" value="">
                                    <span class="datepicker">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </p>
                                <script type="text/javascript">
                                	var d1 = new Date() ;
                                    $(function() {
										$('input[name="sDate"]').daterangepicker({
											locale: {
												format: 'YYYY-MM-DD'
											},
											singleDatePicker: true,
											minDate: d1
										});
                                    });
                                </script>
                                <select name="sTime">
                                <?php foreach ($timeArr as $timeRange) : ?>
                                <option value="<?=$timeRange?>"><?=$timeRange?></option>
                                <?php endforeach ; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="sel-order-type">結束時間 :</label>
                            <div class="col-sm-10 dt">
                                <p class="dtbox">
                                    <input type="text" id="eDate" name="eDate" class="form-control" value="">
                                    <span class="datepicker">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </p>
                                <script type="text/javascript">
                                    $(function() {
										$('input[name="eDate"]').daterangepicker({
											locale: {
												format: 'YYYY-MM-DD'
											},
											singleDatePicker: true,
											minDate: d1
										});
                                    });
                                </script>
                                <select name="eTime">
									<?php foreach ($timeArr as $timeRange) : ?>
									<option value="<?=$timeRange?>" selected><?=$timeRange?></option>
									<?php endforeach ; ?>
                                </select>
                            </div>
                        </div>
                        <!-- mark by Angus 2018.08.26
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="sel-order-type">使用車種 :</label>
                            <div class="col-sm-10">
                                <select name="car_model" class="form-control">
                                    <option value="">-- 請選擇 --</option>
                                        <option value="3">汽車</option>
                                        <option value="4">進口車</option>
                                        <option value="2">機車</option>
                                        <option value="7">電動自行車</option>
                                        <option value="6">自行車</option>
                                    </select>
                            </div>
                        </div>
                        -->
                        <div class="form-group btn">
                            <div class="col-sm-12">
                                <button type="button" name="search" class="btn btn-success">查詢</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <p class="searchresult_title">查詢結果：</p>
            <div id="showArea">
            </div>
            <form action="?route=order/staff" id="form1" method="post">
                <input type="hidden" name="step2">
            </form>
	    </div>
    </div>
    <script type="text/javascript">
    $('button[name=\'search\']').click( function () {
        console.log( "search button") ;
        var sDate = $('#sDate').val() ;
        var eDate = $('#eDate').val() ;
        var sTime = $('select[name=\'sTime\']').val() ;
        var eTime = $('select[name=\'eTime\']').val() ;
        var carModel = 0;

        var msg = "" ;
        console.log( "sDate : " + sDate + "\n") ;
        console.log( "eDate : " + eDate + "\n") ;
        console.log( "sTime : " + sTime + "\n") ;
        console.log( "eTime : " + eTime + "\n") ;
        console.log( "carModel : " + carModel + "\n") ;

        var d1 = new Date( sDate + ' ' + sTime) ;
        var d2 = new Date( eDate + ' ' + eTime) ;
        if ( d2 < d1) {
            msg = "結束日期不得小於開始日期" ;
        }

        if ( msg == "") {
            runSearch( sDate, eDate, sTime, eTime, carModel) ;
        } else {
            alert( msg) ;
        }


    });

    function runSearch( sDate, eDate, sTime, eTime, carModel) {

        $.ajax({
            url : '?route=order/ordersearch/searchCar',
            type : 'post',
            data : {
                sDate : sDate,
                eDate : eDate,
                sTime : sTime,
                eTime : eTime,
                carModel : carModel,
            },
            dataType: 'html',
            success: function(html) {
                console.log( html) ;
                $('#showArea').html( html) ;
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                console.log( thrownError);
                console.log( ajaxOptions);
                console.log( xhr);
            }
        });
    }
    </script>
    <footer style="background: none; color: #777; text-align: center;">2018 © KmFun金豐租車 版權所有</footer>
</body></html>
