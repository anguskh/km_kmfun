<?php echo $header; ?>
<header></header>
	    <div class="wrapper container">
	    	<div class="container-fluid title about">車型介紹</div>
	    	<div class="vehicle-list container-fluid page">
				<div class="container-fluid additional">
					<ul>
						<li><p class="stitle"><?=$carSeed['car_seed']?></p></li>
						<li>
							<div class="pimg col-sm-5">
								<img src="image/<?=$carSeed['image']?>" alt="">
								<p><span>單價（含稅）</span><b class="price"><?=$carSeed['car_price']?>元 / 天</b></p>
								<?php if ( $carSeed['car_model'] == 7) : ?>
								<span style="display:block; margin: 10px 0 0 0; padding: 3px 0; text-align: center; color:#cc0000; background:#ffffcc;">依台灣現行法令，電動自行車無法雙載</span>
								<?php endif ; ?>
							</div>
							<div class="ptxt col-sm-7">
								<p class="car_info_title">基本信息</p>
								<table cellpadding="0" cellspacing="0" border="0" class="car_info">
									<tr>
										<th><span></span>年份</th>
										<td>：<?=$carSeed['car_year']?>年</td>
									</tr>
									<tr>
										<th><span></span>座位數/車門數</th>
										<td>：<?=$carSeed['car_peopele']?> / <?=$carSeed['car_door']?></td>
									</tr>
									<tr>
										<th><span></span>汽油種類</th>
										<td>：<?=$carSeed['gas_type']?></td>
									</tr>
									<tr>
										<th><span></span>排氣量</th>
										<td>：<?=$carSeed['displacement']?></td>
									</tr>
									<tr>
										<th><span></span>變速系統</th>
										<td>：<?=$carSeed['at_mt']?></td>
									</tr>
									<tr>
										<th><span></span>倒車雷達</th>
										<td>：<?=$carSeed['back_radar']?></td>
									</tr>
									<tr>
										<th><span></span>廣播FM/AM</th>
										<td>：<?=$carSeed['radio']?></td>
									</tr>
									<tr>
										<th><span></span>CD/MP3</th>
										<td>：<?=$carSeed['cd_player']?> / <?=$carSeed['mp3']?></td>
									</tr>
									<tr>
										<th><span></span>安全氣襄位置</th>
										<td>：<?=$carSeed['airbag_pos']?></td>
									</tr>
								</table>
								<p class="car_info_title">租車配置信息</p>
								<table cellpadding="0" cellspacing="0" border="0" class="car_info">
									<tr>
										<th><span></span>USB充電器</th>
										<td>：<?=$carSeed['usb_charge']?></td>
									</tr>
									<tr>
										<th><span></span>衛星導航</th>
										<td>：<?=$carSeed['gps']?></td>
									</tr>
									<tr>
										<th><span></span>行車記錄器</th>
										<td>：<?=$carSeed['driving_recorder']?></td>
									</tr>
								</table>
							</div>
						</li>
					</ul>
					<div class="button-line">
						<button class="clear" type="button" onclick="javascript:history.back()">回上一頁</button>
					</div>
				</div>
			</div>
	    </div><!--wrapper-->
<?php echo $footer; ?>