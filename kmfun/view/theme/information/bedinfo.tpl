<?php echo $header; ?>
<header>
<script src="kmfun/view/js/masonry.pkgd.min.js"></script>
<script>
	$(function(){
		$('.masonry').masonry({
			  itemSelector: '.item'
			});
	});
</script>
</header>
	    <div class="wrapper container">
	    	<div class="container-fluid title bnb">民宿介紹</div>
	    	<div class="container-fluid bnbbox">
	    		<div class="row masonry">
	    			<?php
		    			foreach ($bedList as $key => $row_data) {
		    				// 圖片設定
						if ( !empty($row_data['imageIntro']))
							$imageIntroArr = unserialize($row_data['imageIntro'] ) ;
						else {
							$imageIntroArr = array() ;
						}
						$imageIntro =  "image/".reset($imageIntroArr);
	    			?>
					<div class="col-md-3 item">
						<div class="info">
							<a href="?route=information/bedinfo/detail&sn=<?=$row_data['idx']?>"><img src="<?=$imageIntro?>" alt="" class="img-responsive"></a>
							<a href="?route=information/bedinfo/detail&sn=<?=$row_data['idx']?>" class="bnb-name"><?=$row_data["name"]?></a><br/>
							<?=$row_data["pos"].$row_data["pos1"]?><br/>
							<span class="bnb-distance"><?=$row_data["reg_no"]?></span>
						</div>
					</div>
				<?php
					}
				?>
				</div><!--masonry-->
			</div><!--bnbbox-->
	    
	    </div><!--wrapper-->
<?php echo $footer; ?>