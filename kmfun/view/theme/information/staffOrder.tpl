<!doctype html>
<html lang="zh-TW">
	<head>
		<meta charset="utf-8">
		<title>金豐線上庫存即時查詢</title>
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<meta name="designer" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

		<link href="kmfun/view/css/bootstrap.css?<?=$cssTime?>" rel="stylesheet"/>
		<script src="kmfun/view/js/jquery-2.1.1.min.js?<?=$cssTime?>"></script>
		<script src="kmfun/view/js/bootstrap.js?<?=$cssTime?>"></script>
        <script src="kmfun/view/js/moment.js?<?=$cssTime?>"></script>

<!--datetimepicker-->
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<!--datetimepicker-->
<!--autocomplete-->
<script src="kmfun/view/js/jquery-ui/jquery-ui.min.js?<?=$cssTime?>"></script>
<link rel="stylesheet" type="text/css" href="kmfun/view/js/jquery-ui/jquery-ui.min.css" />
<!--autocomplete-->


		<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
		<![endif]-->
	</head>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-K42BQN2');</script>
    <!-- End Google Tag Manager -->
</head>
<body id="store">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K42BQN2"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="?route=order/ordersearch/stock">車輛即時查詢</a>
            </div>
        </div>
    </nav>
    <header id="header" class="navbar navbar-static-top"></header>
    <div id="container">
	    <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i>金豐 作業人員</h3>
            </div>
            <?php if ($error_warning) { ?>
                <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>
            <?php } ?>
            <div class="container-fluid">
                <div id="content">
                    <div class="row">
                        <form action="?route=order/staff/step2" id="form1" method="post" onsubmit="return funcSubmit();">
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-pay-type">付款類型 :</label>
							<div class="col-sm-10 dt">
								<select name="pay_type">
									<option value="" >請選擇</option>
									<option value="1" >信用卡</option>
									<option value="2" >現金</option>
									<option value="3" >匯款</option>
									<option value="4" >應收</option>
								</select>
							</div>
						</div>
						<!--
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-charge-method">收費方式 :</label>
							<div class="col-sm-10 dt">
								<select name="charge_method">
									<option value="" >請選擇</option>
									<option value="1" >未收款</option>
									<option value="2" >應收</option>
									<option value="3" >已收</option>
								</select>
							</div>
						</div>
						-->
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-order-type">民宿/業者 :</label>
							<div class="col-sm-10 dt">
								<input type="text" name="minsu_name" id="minsu_name" class="form-control">
								<input type="hidden" name="minsu_id" id="minsu_id" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-order-type">取車地點 :</label>
							<div class="col-sm-10 dt">
								<select name="sStation">
									<option value="" >請選擇</option>
									<?php foreach( $rentStation as $iCnt => $row) : ?>
									<option value="<?=$row['idx']?>" ><?=$row['opt_name']?></option>
									<?php endforeach ; ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-order-type">還車地點 :</label>
							<div class="col-sm-10 dt">
								<select name="eStation">
									<option value="" >請選擇</option>
									<?php foreach( $rentStation as $iCnt => $row) : ?>
									<option value="<?=$row['idx']?>" ><?=$row['opt_name']?></option>
									<?php endforeach ; ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-order-type">取車時間 :</label>
							<div class="col-sm-10 dt">
								<input type="text" name="sd" value="<?=$infoArr['sd']?> <?=$infoArr['st']?>" class="form-control" readonly>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-order-type">還車時間 :</label>
							<div class="col-sm-10 dt">
								<input type="text" name="ed" value="<?=$infoArr['ed']?> <?=$infoArr['et']?>" class="form-control" readonly>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-order-type">使用車輛 :</label>
							<div class="col-sm-10 dt">
								<input type="text" name="car_name" value="<?=$rentInfo['car_name']?>" class="form-control" readonly>
								<input type="hidden" name="car_type" value="<?=$rentInfo['car_model']?>" class="form-control">
								<input type="hidden" name="car_model" value="<?=$infoArr['modId']?>" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-order-type">車輛數量 :</label>
							<div class="col-sm-10 dt">
								<select name="useCnt" class="calculatingCarPrices">
									<?php for( $i=1; $i <= 4 ; $i++) : ?>
									<option value="<?=$i?>"><?=$i?></option>
									<?php endfor ; ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-order-type">配件:</label>
							<div class="col-sm-10 dt">
								<label class="col-sm-2 control-label" for="baby_chair1">兒童安全座椅〔一歲以上〕</label>
								<select name="baby_chair1" class="calculatingCarPrices">
									<?php for( $i = 0; $i <= 4 ; $i++ ) : ?>
									<option value="<?=$i?>"><?=$i?></option>
									<?php endfor ; ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-order-type"></label>
							<div class="col-sm-10 dt">
								<label class="col-sm-2 control-label" for="baby_chair2">兒童安全座椅〔增高墊〕</label>
								<select name="baby_chair2" class="calculatingCarPrices">
									<?php for( $i = 0; $i <= 4 ; $i++ ) : ?>
									<option value="<?=$i?>"><?=$i?></option>
									<?php endfor ; ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-order-type"></label>
							<div class="col-sm-10 dt">
								<label class="col-sm-2 control-label" for="children_chair">嬰兒推車</label>
								<select name="children_chair" class="calculatingCarPrices">
									<?php for( $i = 0; $i <= 4 ; $i++ ) : ?>
									<option value="<?=$i?>"><?=$i?></option>
									<?php endfor ; ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-order-type"></label>
							<div class="col-sm-10 dt">
								<label class="col-sm-2 control-label" for="gps">GPS衛星導航系統</label>
								<select name="gps" class="calculatingCarPrices">
									<?php for( $i = 0; $i <= 4 ; $i++ ) : ?>
									<option value="<?=$i?>"><?=$i?></option>
									<?php endfor ; ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-order-type"></label>
							<div class="col-sm-10 dt">
								<label class="col-sm-2 control-label" for="mobile_moto">機車導航架</label>
								<select name="mobile_moto" class="calculatingCarPrices">
									<?php for( $i = 0; $i <= 4 ; $i++ ) : ?>
									<option value="<?=$i?>"><?=$i?></option>
									<?php endfor ; ?>
								</select>
							</div>
						</div>


						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-order-type">租車費用 :</label>
							<div class="col-sm-10 dt">
								<?=$rentInfo['rentDay']?>天 <?=$rentInfo['rentHour']?>小時
								費用單價 <?=$rentInfo['car_price']?>元
								<?php
									if ( $rentInfo['DiscountPercentage'] < 1) {
										echo "優惠折數 " . $rentInfo['DiscountPercentage'] * 100 . "折"  ;
									}
								?>
								<br>
								<input type="text" name="price" value="<?=$rentInfo['car_price_show']?>" class="form-control">
								<input type="hidden" name="rentDay" value="<?=$rentInfo['rentDay']?>" class="form-control">
								<input type="hidden" name="rentHour" value="<?=$rentInfo['rentHour']?>" class="form-control">
								<input type="hidden" name="discount" value="<?=$rentInfo['DiscountPercentage']?>" class="form-control">
								<input type="hidden" name="price_basic" value="<?=$rentInfo['car_price_show']?>" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-order-type">SPDC :</label>
							<div class="col-sm-10 dt">
								<input type="text" name="spdc_price" value="0" class="form-control">
							</div>
						</div>
 						<div class="form-group btn">
							<div class="col-sm-12">
								<button type="submit" name="next" class="btn btn-success">下一步</button>
							</div>
						</div>
                        </form>
                    </div>
                </div>
            </div>
	    </div>
    </div>
    <script type="text/javascript">
function funcSubmit() {
	console.log( 'funcSubmit');
	var msg = "" ;
	var sStation      = $("select[name='sStation']").val() ;
	var eStation      = $("select[name='eStation']").val() ;
	var pay_type      = $("select[name='pay_type']").val() ;

	if ( !pay_type) {
		msg += "付款方式 未填寫\n" ;
	}
	if ( !sStation) {
		msg += "取車地點 未填寫\n" ;
	}
	if ( !eStation) {
		msg += "還車地點 未填寫\n" ;
	}

	if ( msg) {
		alert( msg) ;
		return false ;
	}
	return true ;
}
// mark by Angus 2020.03.26
// 車輛費用即時計算 by Angus 2020.03.24
// $("select[name='useCnt']").change( function () {
// 	var total = 0 ;
// 	var carCnt = $(this).val() ;
// 	var price = $("input[name='price_basic']").val() ;


// 	$("input[name='price']").val( price * carCnt) ;
//  }) ;

/**
 * [calculatingCarPrices 計算車價]
 * @Another Angus
 * @date    2020-03-26
 */
$('.calculatingCarPrices').change( function () {
	calculatingCarPrices() ;
}) ;

$("input[name='spdc_price']").keyup(function(event) {
	console.log("input[name='spdc_price']") ;
	var spdcMoney = $(this).val() ;
	console.log( spdcMoney);
	calculatingCarPrices() ;
});

function calculatingCarPrices() {
	var total  = 0 ;
	var carCnt = $("select[name='useCnt']").val() ; // 車子數量
	var price  = $("input[name='price_basic']").val() ; // 基本車價
	var rentDay  = $("input[name='rentDay']").val() ; // 天數
	var rentHour  = $("input[name='rentHour']").val() ; // 小時
	if(parseInt(rentHour)>0){
		rentDay++;
	}
//	var bc1         = $("select[name='babieschair1']").val() ; // 兒童安全座椅〔一歲以下〕
	var bc2         = $("select[name='baby_chair1']").val() ;	// 兒童安全座椅〔一歲以上〕
	var bc3         = $("select[name='baby_chair2']").val() ;	// 兒童安全座椅〔增高墊〕
	var bc4         = $("select[name='children_chair']").val() ;// 嬰兒推車
	var gps         = $("select[name='gps']").val() ;			// gps
	var mobile_moto = $("select[name='mobile_moto']").val() ;	// 機車導航架
	var spdcMoney   = $("input[name='spdc_price']").val() ;		// 業務折扣
	// console.log( bc1 +' '+ bc2 +' '+ bc3 );

	total = (price * carCnt) + ( parseInt(bc2) + parseInt(bc3)+ parseInt(bc4)+ parseInt(gps)) * 100  - spdcMoney ;
	total += parseInt(mobile_moto) * 100;
	
	if(rentDay >1 ){
		total += parseInt(mobile_moto) * 50 * (parseInt(rentDay)-1);
	}
	$("input[name='price']").val( total) ;
}

// 客戶名稱自動帶入
$('input[name=\'minsu_name\']').autocomplete({
	'source': function(request, response) {
		var request = $('input[name=\'minsu_name\']').val() ;
		// console.log( "request :" + request) ;
		// console.log( "response :" + response) ;
		// console.log( '?route=order/staff/getAjaxMinsu&filter_name=' + encodeURIComponent(name));
		$.ajax({
			url: '?route=order/staff/getAjaxMinsu&filterName=' + encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				console.log( "json : " + JSON.stringify(json)) ;
				response($.map(json, function(item) {
					console.log( "for : " + JSON.stringify(item));
					return {
						value: item['code'],
						label: item['name']
					}
				}));
			}
		});
	},
	'select': function(event, opt) {
		event.preventDefault();
		console.log( "sel : " + opt.item.label) ;
		// debugger;
		$('input[name=\'minsu_name\']').val( opt.item.label) ;
		$('input[name=\'minsu_id\']').val( opt.item.value) ;
		// debugger;
		// $('#minsu_name').val( opt.item.label) ;
		// $('#minsu_id').val( opt.item.value) ;
	},
	focus: function(event, opt) {
        event.preventDefault();
        $(this).val(opt.item.label);
    }

});
   </script>
    <footer style="background: none; color: #777; text-align: center;">2018 © KmFun金豐租車 版權所有</footer>
</body></html>
