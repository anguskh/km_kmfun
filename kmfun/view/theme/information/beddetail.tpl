<?php echo $header; ?>
<header>
	<link href="kmfun/view/css/swiper.min.css" rel="stylesheet"/>
	<link href="kmfun/view/css/lightbox.css" rel="stylesheet"/>
	<script src="kmfun/view/js/lightbox.js"></script>
</header>
	    <div class="wrapper container">
	    	<div class="container-fluid bnbbox">
	    		<p class="location"><?=$bedInfo["pos"]?> &gt; <?=$bedInfo["pos1"]?></p>
	    		<h1><?=$bedInfo["name"]?>　<a href="tel:<?=$bedInfo["tel"]?>" class="phone"><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span><?=$bedInfo["tel"]?></a>　<a href="<?=$bedInfo["url"]?>" class="website"><span class="glyphicon glyphicon-home" aria-hidden="true"></span><?=$bedInfo["url"]?></a></h1>
	    		<ul class="feature">
    			<?php
    				$service = explode(";", $bedInfo["service"]);
    				$cht_ary = array(
    					"children"=>"歡迎孩子",
    					"pet"=>"歡迎寵物",
	    				"no_smoking"=>"禁止吸煙",
	    				"eng"=>"英語服務",
	    				"breakfast"=>"提供早餐",
	    				"wifi"=>"提供WiFi",
	    				"parking"=>"提供停車",
	    				"washing"=>"洗衣機",
	    				"bike"=>"單車"
	    			);
	    			if ( is_array( $service)) {
	    			foreach ($service as $key => $value) {
    			?>
					<li class="icon_<?=$value?>"><?=$cht_ary[$value]?></li>
					<!--<li class="icon_pet">歡迎寵物</li>
					<li class="icon_no_smoking">禁止吸煙</li>
					<li class="icon_eng">英語服務</li>
					<li class="icon_breakfast">提供早餐</li>
					<li class="icon_wifi">提供WiFi</li>
					<li class="icon_parking">提供停車</li>
					<li class="icon_washing">洗衣機</li>
					<li class="icon_bike">單車</li>-->
					<?php
						}
					}
					?>
	    		</ul>
				<ul class="row imglist">

			<?php
				$len = count($bedInfo["imageIntroArr"]);
				$i = 0;
				foreach ($bedInfo["imageIntroArr"] as $key => $image) {
					if (!empty($image)) {
						$image = "image/".$image;
						if ($i == 0) {
			?>
					<li class="col-lg-4 col-md-6">
						<a href="<?=$image?>" data-toggle="lightbox" data-gallery="gallery"><img src="<?=$image?>" class="img-responsive" alt=""></a>
					</li>
			<?php
						}else{
			?>
					<li class="col-lg-2 col-md-3">
						<a href="<?=$image?>" data-toggle="lightbox" data-gallery="gallery"><img src="<?=$image?>" class="img-responsive" alt=""></a>
					</li>
			<?php
						}
					}
					$i++;
				}
			?>
				</ul>
			</div>
	    	</div><!--wrapper-->
	    	<div class="swiper-container">
		    <div class="swiper-wrapper">
		<?php
			foreach ($bedInfo["imageIntroArr"] as $key => $image) {
				$image = "image/".$image;
		?>
		    	<div class="swiper-slide"><img src="<?=$image?>"></div>
		<?php
			}
		?>
		    </div>
		    <!-- Add Arrows -->
		    <div class="swiper-button-next"></div>
		    <div class="swiper-button-prev"></div>
		</div>
		<div class="wrapper container">
	    	<div class="container-fluid bnbbox">
				<div class="txtinfo">
					<?$bedInfo['room_type']?>
				</div>
				<div class="roomtype">
					<ul class="title">
						<li class="col-lg-6 col-md-6 col-sm-6">房型</li>
						<li class="col-lg-3 col-md-3 col-sm-6">最多人數</li>
						<li class="col-lg-3 col-md-3 col-sm-6">每晚最低價</li>
					</ul>
				<?php
					foreach ($roomdata as $key => $room_row) {
				?>
					<ul>
						<li class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
							<img src="image/<?=$room_row['image']?>" alt="">
							<span class="room-name"><?=$room_row['name']?></span>
							<div class="facility">
							<?php
								$ary_device = explode(";", $room_row["device"]);
								$drvice_cht = array("wifi"=>"免費wifi","bath"=>"獨立衛浴");
								foreach ($ary_device as $key => $device) {
							?>
								<span><?=$drvice_cht[$device]?></span>
							<?php
								}
							?>
							</div>
						</li>
						<li class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
							<?=$room_row['per_cnt']?>人<br/>
							<?php  if($room_row['extra_per_cnt']>0){?>
								<span class="small">可再加<?=$room_row['extra_per_cnt']?>人 ，需加價</span>
							<?php }?>
						</li>
						<li class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
							<?=$room_row['base_price']?>元 <span class="small"> /房</span>
						</li>
					</ul>
				<?php }?>
				</div>
				<div class="moreinfo">
					<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#location">位置與交通</a></li>
						<li><a data-toggle="tab" href="#facility">旅宿設備</a></li>
						<li><a data-toggle="tab" href="#rule">入住時間與住房規則</a></li>
						<li><a data-toggle="tab" href="#service">旅宿服務</a></li>
						<li><a data-toggle="tab" href="#cancellation">退訂政策</a></li>
					</ul>
					<div class="tab-content">
						<div id="location" class="tab-pane fade in active">
							<h3>位置與交通</h3>
							<p><iframe src="http://maps.google.com.tw/maps?f=q&hl=zh-TW&geocode=&q=<?=$bedInfo['address']?>&output=embed" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></p>
						</div>
						<div id="facility" class="tab-pane fade">
							<h3>旅宿設備</h3>
							<p><?=$bedInfo["device_text"]?></p>
						</div>
						<div id="service" class="tab-pane fade">
							<h3>旅宿服務</h3>
							<p><?=$bedInfo["service_text"]?></p>
						</div>
						<div id="rule" class="tab-pane fade">
							<h3>入住時間與住房規則</h3>
							<p><?=$bedInfo["rule_text"]?></p>
						</div>
						<div id="cancellation" class="tab-pane fade">
							<h3>退訂政策</h3>
							<p><?=$bedInfo["cancel_text"]?></p>
						</div>
					</div>
				</div>
			</div><!--bnbbox-->
	    </div>
<script src="kmfun/view/js/swiper.min.js"></script>
<script>
  	var swiper = new Swiper('.swiper-container', {
  		navigation: {
      		nextEl: '.swiper-button-next',
      		prevEl: '.swiper-button-prev',
  		},
  	});
  	$(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox();
        });
</script>
<?php echo $footer; ?>