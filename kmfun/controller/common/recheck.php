<?php
/**
 * 2018.06.29 處理
 * http://localhost/kmfun/?route=common/recheck
 */
class ControllerCommonRecheck extends Controller {
	private $infoArr ; // 基礎資訊
	private $objLog ;
	private $notifyToken = "6d68rctKSJnz5IasZb9jXmWuJOBFVoOgBCuOIdigl3x" ; // 主機狀態通知
	private $notifyUrl   = 'https://notify-api.line.me/api/notify -H "Authorization: Bearer %s" -d "message=%s"' ;

	public function index() {
		$toDay = date( 'Y_m_d') ;
		$this->objLog = new log( "cronReSendMail_{$toDay}.log") ;
		$this->load->model('common/option') ;
		$this->load->model('common/cars') ;
		$this->load->model('common/zip') ;
		$this->load->model('common/order') ;
		$this->load->model('common/cronjob') ;

		$this->infoArr['stationArr']  = $this->model_common_option->getOptionItemsArr4Index( '15') ;	// 取車地點
		$this->infoArr['carModels']   = $this->model_common_option->getOptionItemsArr4Index( '1') ;		// 車種
		$this->infoArr['carSeedInfo'] = $this->model_common_cars->getCarSeedInfo() ; 					// 車型
		$this->infoArr['zip']         = $this->model_common_zip->getAllZipCode() ; 						// 郵遞區號
		// dump( $this->infoArr) ;

		/**
		 * 檢查是否有完成的訂單
		 */
		$SQLCmd     = $this->model_common_cronjob->getOrderListCmd( 5) ;
			$this->objLog->write( $SQLCmd) ;
		// 取得已完成綠界的訂單
		$runListArr = $this->model_common_cronjob->getOrderList( 5) ;
			$this->objLog->write( "本次5分整理需補寄mail&簡訊 : " . count( $runListArr) . "筆") ;
		// 組合查詢的訂單
		$orderIdArr = array() ;
		foreach ($runListArr as $iCnt => $row) {
			$orderIdArr[] = $row['order_id'] ;
			$runListArrp[$row['order_id']] = $row ;
		}
		$this->objLog->write( print_r( $orderIdArr, true)) ;

		// 檢查是否有清單  無清單  就停本次作業
		if ( !empty( $orderIdArr)) {
			$SQLCmd           = $this->model_common_cronjob->check_tbEcpayReturnCmd( $orderIdArr) ;
			$this->objLog->write( $SQLCmd) ;
			$tbEcpayReturnArr = $this->model_common_cronjob->check_tbEcpayReturn( $orderIdArr) ;
			$this->objLog->write( print_r( $tbEcpayReturnArr, true)) ;

			$sendOrderArr = array() ;
			foreach ($orderIdArr as $tb_orderID) {
				if ( isset($tbEcpayReturnArr[$tb_orderID]) && $tbEcpayReturnArr[$tb_orderID]['RtnCode'] == '1') {
					// echo " $tb_orderID 要發信<br>" ;
					$this->sendMailMsg( $runListArrp[$tb_orderID]) ;
					$this->sendSMS( $runListArrp[$tb_orderID]) ;
					$sendOrderArr[] = $runListArrp[$tb_orderID]['agreement_no'] ;
				} else {
					echo " $tb_orderID 尚未回傳<br>" ;
					if ( !isset($tbEcpayReturnArr[$tb_orderID])) {
						$this->objLog->write( "$tb_orderID 尚未回傳" ) ;
						// $this->sendLineNotify( "$tb_orderID 尚未回傳") ;
					}
				}
			}

			$this->objLog->write( "sendOrderArr") ;
			$this->objLog->write( print_r( $sendOrderArr, true)) ;
			if ( !empty( $sendOrderArr) && !empty(trim( join( ',', $sendOrderArr))) ) {
				$LineMsg = "本次5分檢查 : " . join( ',', $sendOrderArr) ;
				$this->sendLineNotify( $LineMsg) ;
			}
		} else {
			$this->objLog->write( "5分鐘檢查 作業完成~~~~~") ;
		}

		// --------------------------------------------------------------------------------------------
		// 取出綠界已成單 未發簡訊的筆數  checkout != '',
		$SQLCmd     = $this->model_common_cronjob->getOrderListCmd( 60) ;
			$this->objLog->write( $SQLCmd) ;
		// 取得已完成綠界的訂單
		$runListArr = $this->model_common_cronjob->getOrderList( 60) ;
			$this->objLog->write( "本次整理需補寄mail&簡訊 : " . count( $runListArr) . "筆") ;
		$orderIdArr = array() ;
        foreach ($runListArr as $iCnt => $row) {
        	$orderIdArr[] = $row['order_id'] ;
        	$runListArrp[$row['order_id']] = $row ;
        }
        // dump( $runListArrp) ;
		$this->objLog->write( print_r( $orderIdArr, true)) ;

		if ( !empty( $orderIdArr)) {
			$SQLCmd           = $this->model_common_cronjob->check_tbEcpayReturnCmd( $orderIdArr) ;
			$this->objLog->write( $SQLCmd) ;
			$tbEcpayReturnArr = $this->model_common_cronjob->check_tbEcpayReturn( $orderIdArr) ;
			$this->objLog->write( print_r( $tbEcpayReturnArr, true)) ;

			foreach ($orderIdArr as $tb_orderID) {
				if ( isset($tbEcpayReturnArr[$tb_orderID]) && $tbEcpayReturnArr[$tb_orderID]['RtnCode'] == '1') {
					// echo " $tb_orderID 要發信<br>" ;
					$this->sendMailMsg( $runListArrp[$tb_orderID]) ;
					$this->sendSMS( $runListArrp[$tb_orderID]) ;
				} else {
					echo " $tb_orderID 不用發信<br>" ;
					$this->model_common_cronjob->clean_tbScheduling( $runListArrp[$tb_orderID]['agreement_no']) ;
					if ( !isset($tbEcpayReturnArr[$tb_orderID])) {
						$this->objLog->write( "$tb_orderID 查無綠界回傳 clean_tbScheduling") ;
						// $this->sendLineNotify( "{$runListArrp[$tb_orderID]['agreement_no']} 查無綠界回傳 clean_tbScheduling") ;
					} else {
						$this->objLog->write( "$tb_orderID {$tbEcpayReturnArr[$tb_orderID]['RtnCode']}|{$tbEcpayReturnArr[$tb_orderID]['RtnMsg']}  clean_tbScheduling") ;
						$this->sendLineNotify( "{$runListArrp[$tb_orderID]['agreement_no']} {$tbEcpayReturnArr[$tb_orderID]['RtnCode']}|{$tbEcpayReturnArr[$tb_orderID]['RtnMsg']}  clean_tbScheduling") ;
					}
				}
			}
			$this->objLog->write( "作業完成~~~~~") ;
		} else {
			$this->objLog->write( "作業完成~~~~~") ;
		}
	}

	/**
	 * [sendLineNotify 排程檢查通知]
	 * @param   string     $msgStr [description]
	 * @return  [type]             [description]
	 * @Another Angus
	 * @date    2019-09-24
	 */
	public function sendLineNotify ( $msgStr = "") {
		/**
		 * [$notifyToken Line notify 設定值]
		 * @var string
		 */
		$msgStr = "\n[Func] 排程作業recheck.php\n" . $msgStr ;
		$notifyUrlStr = sprintf( $this->notifyUrl, $this->notifyToken, $msgStr) ;
		exec( "curl {$notifyUrlStr}") ;
	}


	/**
	 * [buileSendMailMsg description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2018-03-20
	 */
	protected function buileSendMailMsgContent( $data = array()) {
		$car_modelNo = 	$this->infoArr['carSeedInfo'][$data['car_model']]['car_model'] ;
		$address     = 	isset( $this->infoArr['zip'][$data['rent_zip1']]) ? $this->infoArr['zip'][$data['rent_zip1']] : "" ;
		$address     .= isset( $this->infoArr['zip'][$data['rent_zip2']]) ? $this->infoArr['zip'][$data['rent_zip2']] : "" ;
		$address     = 	preg_replace('/\s(?=)/', '', $address);
		$address     .= $data['rent_user_address'] ;

		// 租車費用文字說明調整 add by Angus 2018.07.05 ---------------------------------------------------------------
		// 車輛單價
		$car_price = $this->infoArr['carSeedInfo'][$data['car_model']]['car_price'] ;
		// 早鳥折數 於2020.07.07 調整可以輸出兩個值 百分比及天數
		list($data['DiscountPercentage'], $data['DiscountDays']) = $this->model_common_order->getDiscountPercentage_days( $data['rent_date_out'], $data['create_date']) ;
		// 調整log
		$lineMsg = "cronjob {$data['agreement_no']}\n本次計算折數:{$data['DiscountPercentage']}\n下單時間:{$data['create_date']}|租車時間:{$data['rent_date_out']}\n提前租車天數:{$data['DiscountDays']}" ;
		if ( !empty($data['sale_off'])) {
			$data['DiscountPercentage'] = $data['sale_off'] ;
			$lineMsg .= "本次使用折數是DB:{$data['sale_off']}\n" ;
		}

		// 使用天數
		$second1 = strtotime( $data['rent_date_in_plan']) - strtotime( $data['rent_date_out']) ;
		$data['rentDay'] 		= floor( $second1 / 86400) ;	// 24hr * 60分 * 60秒
		$data['rentHour'] 		= ( $second1 % 86400) / 3600 ;	//
		$lineMsg .= "本次租車天數 {$data['rentDay']}天{$data['rentHour']}Hr" ;

		$this->sendLineNotify( $lineMsg) ;
		$this->objLog->write( $lineMsg) ;
		// 產生費用
		if ( $data['rentDay'] == 0 ) {
			// $data['car_price2'] = $car_price * $data['total_car'] * $data['DiscountPercentage'] ;
			$data['car_price2'] = $car_price * $data['total_car'] ;
		} else if ( $data['rentDay']  >= 1 && $data['rentHour'] <= 6) {
			// $data['car_price2'] = ( $car_price * $data['rentDay'] * $data['DiscountPercentage'] + $car_price * 0.1 * ceil( $data['rentHour']) ) * $data['total_car'] ;
			$data['car_price2'] = ( $car_price * $data['rentDay'] + $car_price * 0.1 * ceil( $data['rentHour']) ) * $data['total_car'] ;
		} else { //if ( $rentInfo['rentDay']  > 1 && $rentInfo['rentHour'] > 6) {
			// $data['car_price2'] = $car_price * ( $data['rentDay'] + 1) * $data['DiscountPercentage'] * $data['total_car'] ;
			$data['car_price2'] = $car_price * ( $data['rentDay'] + 1) * $data['total_car'] ;
		}
		$data['car_price2Str'] = number_format( $data['car_price2']) ;

		// 配件使用天數
		$useDay = round( ( strtotime( $data['rent_date_in_plan']) - strtotime( $data['rent_date_out']))/3600/24) + 1 ;
		// bc => 兒童安全座椅〔一歲以下〕, bc1 => 兒童安全座椅〔一歲以上〕, bc2 => 兒童安全座椅〔增高墊〕
		if($data['baby_chair'] > 0 || $data['baby_chair1'] > 0 || $data['baby_chair2'] > 0 ||
			$data['children_chair'] > 0 || $data['gps'] > 0 || $data['mobile_moto'] > 0) {
			$data['car_price3'] = ($data['baby_chair'] * 100 * $useDay) +
									($data['baby_chair1'] * 100 * $useDay) +
									($data['baby_chair2'] * 100 * $useDay) +
									($data['children_chair'] * 100 * $useDay) +
									($data['gps'] * 0 * $useDay) +
									($data['mobile_moto'] * 50 * $useDay) ;
			$data['car_price3Str'] = number_format( $data['car_price3']) ;

		} else {
			$data['car_price3Str'] = 0 ;
		}
		$data['totalStr'] = number_format( $data['total']) ;

		$rentPriceStr = "\n" ;
		if ( $data['DiscountPercentage'] < 1) {
			$rentPriceStr .= "         早鳥優惠 ".($data['DiscountPercentage'] * 10)." 折\n" ;
		}
		$rentPriceStr .= "         車價（共{$data['total_car']}台，每台借車時間為 {$data['rentDay']}天又{$data['rentHour']}小時），共 {$data['car_price2Str']}元\n" ;
		$rentPriceStr .= "         配件合計 {$data['car_price3Str']}元\n" ;
		$rentPriceStr .= "         總價（含稅）{$data['totalStr']}元\n" ;
		// 租車費用文字說明調整 add by Angus 2018.07.05 End ---------------------------------------------------------------
		// 航班 add by Angus 2018.07.21 ---------------------------------------------------------------------------------
		$isAirInfo = $this->model_common_order->getFilght( $data['agreement_no']) ;
		// dump( $isAirInfo) ;
		if ( isset( $isAirInfo['order_no'])) {
			$airportArr = $this->model_common_option->getOptionArrUseParent( array(134, 131)) ; // 航/船班資訊
			// dump( $airportArr) ;
			foreach ($airportArr as $iCnt => $tmpRow) {
				if ( $tmpRow['idx'] == $isAirInfo['airport'] || $tmpRow['idx'] == $isAirInfo['pier']) {
					$airInfoStr = $tmpRow['opt_short_desc'] . " " . $isAirInfo['s_date'] ;
					break ;
				}
			}
		} else {
			$airInfoStr = "自取" ;
		}

		// 航班 add by Angus 2018.07.21 End -----------------------------------------------------------------------------

		// 配件
		$tmpOther = "" ;
		if($data['baby_chair'] > 0) {
			$tmpOther .= "嬰兒安全座椅 x {$data['baby_chair']}" ;
		}
		if($data['baby_chair1'] > 0) {
			if($tmpOther != "")
				$tmpOther .= "\r\n　　&nbsp;&nbsp;&nbsp;";
			$tmpOther .= "兒童安全座椅 x {$data['baby_chair1']}" ;
		}
		if($data['baby_chair2'] > 0) {
			if($tmpOther != "")
				$tmpOther .= "\r\n　　&nbsp;&nbsp;&nbsp;";
			$tmpOther .= "兒童增高墊 x {$data['baby_chair2']}" ;
		}
		if($data['children_chair'] > 0) {
			if($tmpOther != "")
				$tmpOther .= "\r\n　　&nbsp;&nbsp;&nbsp;";
			$tmpOther .= "嬰兒推車 x {$data['children_chair']}" ;
		}
		if($data['gps'] > 0) {
			if($tmpOther != "")
				$tmpOther .= "\r\n　　&nbsp;&nbsp;&nbsp;";
			$tmpOther .= "GPS x {$data['gps']}" ;
		}
		if($data['mobile_moto'] > 0) {
			if($tmpOther != "")
				$tmpOther .= "\r\n　　&nbsp;&nbsp;&nbsp;";
			$tmpOther .= "機車導航架 x {$data['mobile_moto']}" ;
		}
		if($tmpOther == "")
			$tmpOther .= "無";

		$pattern = "
＃＃＃＃＃＃＃＃＃＃＃
＃　車輛行事曆資訊　＃
＃＃＃＃＃＃＃＃＃＃＃
車種 : %s
車型 : %s
訂車數量 : %s
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
【租車資訊】
訂單編號 : %s
訂單類別 : 網路訂單
民宿(業者) :
取車地點 : %s
還車地點 : %s
取車時間 : %s
還車時間 : %s
租車費用 : %s
備註 :
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
【客戶資料】
承租人姓名 : %s
身分證字號／護照號碼 : %s
出生年月日 : %s
手機 : %s
取車人姓名 : %s
身分證字號／護照號碼 : %s
出生年月日 : %s
手機 : %s
航班時間 : %s
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
通訊地址 : %s
公司名稱 : %s
公司統編 : %s
緊急連絡人 : %s
緊急連絡人電話 : %s
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
配件 : %s
" ;
		return sprintf( $pattern,
						$this->infoArr['carModels'][$car_modelNo]['opt_name'],	// 車種
						$this->infoArr['carSeedInfo'][$data['car_model']]['car_seed'],	// 車型
						$data['total_car'],				// 訂車數量
						$data['agreement_no'],			// 訂單編號
						$this->infoArr['stationArr'][$data['s_station']]['opt_name'],	// 取車地點
						$this->infoArr['stationArr'][$data['e_station']]['opt_name'],	// 還車地點
						$data['rent_date_out'],			// 取車時間
						$data['rent_date_in_plan'],		// 還車時間
						$rentPriceStr,					// 租車費用
						$data['rent_user_name'],		// 承租人姓名
						$data['rent_user_id'],			// 身分證字號／護照號碼
						$data['rent_user_born'],		// 出生年月日
						$data['rent_user_tel'],			// 手機
						$data['get_user_name'],			// 取車人姓名
						$data['get_user_id'],			// 身分證字號／護照號碼
						$data['get_user_born'],			// 出生年月日
						$data['get_user_tel'],			// 手機
						$airInfoStr,
						$address,						// 通訊地址
						$data['rent_company_name'],		// 公司名稱
						$data['rent_company_no'],		// 公司統編
						$data['urgent_man'],			// 緊急連絡人
						$data['urgent_mobile'],			// 緊急連絡人電話
						$tmpOther
		) ;
	}

	/**
	 * [sendMailMsg description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2018-03-20
	 */
	protected function sendMailMsg( $data = array()) {
		// dump( $data) ;
		$mail = new Mail();

		$mail->protocol      = $this->config->get('config_mail_protocol') ;
		$mail->parameter     = $this->config->get('config_mail_parameter') ;
		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname') ;
		$mail->smtp_username = $this->config->get('config_mail_smtp_username') ;
		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8') ;
		$mail->smtp_port     = $this->config->get('config_mail_smtp_port') ;
		$mail->smtp_timeout  = $this->config->get('config_mail_smtp_timeout') ;

		$subject = "訂單通知 訂單編號:{$data['agreement_no']}" ;
		$message = $this->buileSendMailMsgContent( $data) ;
		// dump( $message) ;
		// $this->objLog->write( $message) ;

		$mail->setTo( $data['rent_user_mail']) ;
		// $mail->setTo( 'kh0813@gmail.com') ;
		$mail->setFrom($this->config->get('config_email')) ;
		$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) ;
		$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8')) ;
		$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8')) ;

		try {
			$mail->send() ;
		} catch( Exception $e) {
			 $this->objLog->write( 'Error exception: ', $e->getMessage()) ;
			 $this->objLog->write( "{$data['order_id']} | {$data['agreement_no']} | {$data['rent_user_mail']}") ;
		}

		$sendMailArr = array(
			"kmfun005@gmail.com",
			"kmfun999@gmail.com"
		) ;
		if ( WEBSITE_TYPE != 'dev') {
			$mail->setTo( $sendMailArr) ;
			$mail->send();
		}

		$sendMailArr = array(
			"kh0813@gmail.com",
			"jaff.tw@gmail.com"
		) ;
		$mail->setSubject(html_entity_decode("cronJob : ".$subject, ENT_QUOTES, 'UTF-8')) ;
		$mail->setTo( $sendMailArr) ;
		$mail->send();
		$this->objLog->write( "{$data['order_id']} - 發送mail 訂單編號 : {$data['agreement_no']} | mail : {$data['rent_user_mail']}") ;
	}

	/**
	 * [buileSendSmsMsgContent description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2018-04-12
	 */
	protected function buileSendSmsMsgContent( $data = array()) {
		$pattern = "您好！您的租車訂單編號%s付款金額：%s元已收到，車輛已預留，將於出發前一天會再與您聯繫。提醒您，如取消交易須酌收營業損失費用！如有任何疑問請加入Line：@kmfun諮詢或撥082-371888，金豐租車感謝您!(系統發送，請勿回傳)" ;

		return sprintf( $pattern, $data['agreement_no'], number_format( $data['total'])) ;
	}

	/**
	 * [sendSMS description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2018-04-12
	 */
	protected function sendSMS( $data = array()) {
		$this->load->model('common/order') ;
    // [batchID] => 7aadd35d-c4cf-4c67-9c34-58d3205f5acb
    // [credit] => 3973.00

		$sms        = new SMSHttp() ;
		$userID     = "0965109222" ;	//發送帳號
		$password   = "0965109222" ;	//發送密碼
		$subject    = "金豐租車網路訂車" ;	//簡訊主旨，主旨不會隨著簡訊內容發送出去。用以註記本次發送之用途。可傳入空字串。
		$content    = $this->buileSendSmsMsgContent( $data) ;	//簡訊內容
		$sendMobile = $data['rent_user_tel'] ;
		// $sendMobile = "0937025519" ;
		$sendTime   = "";

		if( $sms->getCredit( "0965109222", "0965109222") && $sms->credit > 0) {
			if ( $sms->sendSMS( $userID, $password, $subject, $content, $sendMobile, $sendTime)) {
				$this->model_common_order->updateSmsStatus(
					array( "agreement_no" => $data['agreement_no'], "batchID" => $sms->batchID, "updateUser"=> "CronJobSMS")
				) ;
				$this->objLog->write( "{$data['order_id']} - 發送簡訊 訂單編號 : {$data['agreement_no']} | 電話號碼 : {$data['rent_user_tel']}") ;
			}
		}
	}

	/**
	 * [isPhone 驗證台灣手機號碼]
	 * @param   [type]     $str [description]
	 * @return  boolean         [description]
	 * @Another Angus
	 * @date    2018-04-12
	 */
	protected function isPhone($str) {
	    if (preg_match("/^09[0-9]{2}-[0-9]{3}-[0-9]{3}$/", $str)) {
	        return true;    // 09xx-xxx-xxx
	    } else if(preg_match("/^09[0-9]{2}-[0-9]{6}$/", $str)) {
	        return true;    // 09xx-xxxxxx
	    } else if(preg_match("/^09[0-9]{8}$/", $str)) {
	        return true;    // 09xxxxxxxx
	    } else {
	        return false;
	    }
	}
}