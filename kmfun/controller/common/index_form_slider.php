<?php
class ControllerCommonIndexFormSlider extends Controller {
	public function index() {
		$data = array() ;
		$data['qstrlen'] = isset( $this->request->get['qid']) ? strlen($this->request->get['qid']) : "" ;
		$this->load->model('common/option') ;

		$data['rentStation'] = $this->model_common_option->getOptionItemsArr( '15') ;
		// dump( $data['rentStation']) ;
		$data['rentTimes'] = $this->getRentTime() ;

		// add by Angus 2018.04.11
		$sliderArr = $this->model_common_option->getIndexKV_Slider() ;
		foreach ($sliderArr as $cnt => $tmpRow) {
			if( !empty( $tmpRow['page_desc'])) {
				$sliderArr[$cnt]['href'] = $this->url->link('information/page', 'token=' . $tmpRow['manufacturer_id'], 'SSL') ;
			} else {
				$sliderArr[$cnt]['href'] = "#" ;
			}
		}
		$data['sliderArr'] = $sliderArr ;
		$data['showDate1']  = date("Y-m-d", strtotime('+1 day')) ;
		$data['showDate2']  = date("Y-m-d", strtotime('+2 day')) ;
		//檢查有沒有qid(qrcode)$this->request->get['qid'];
		//有的話是快速
		$qid_arr = array();
		$qid_flag = "N";
		$qid_time = "";
		$qid = "";
		$data["rent_station"] = "";
		if(isset($this->request->get['qid']) && $this->request->get['qid'] != ''){
			//檢查qid是否是這三個 (固定的)
			if($this->request->get['qid'] == "3469FE736EF31BB7" || $this->request->get['qid'] == "E1E3C87AFF11D26A" || $this->request->get['qid'] == "7518FAD75806FC49" ){
				// 3469FE736EF31BB7 金城16  E1E3C87AFF11D26A 金胡17 7518FAD75806FC49尚義18
				$rent_arr = array("3469FE736EF31BB7"=>"16", "E1E3C87AFF11D26A"=>"17", "7518FAD75806FC49"=>"18");
				$data["rent_station"] = $rent_arr[$this->request->get['qid']];
				$qid_flag = "YY";
				//取車日=現在時間後30分 以整數為單位
				$min = date("H:i",mktime(date("H"), date("i")+30, 0, 0 , 0, 0));
				$tmp_array= array(
					"07:30",
					"08:00",
					"08:30",
					"09:00",
					"09:30",
					"10:00",
					"10:30",
					"11:00",
					"11:30",
					"12:00",
					"12:30",
					"13:00",
					"13:30",
					"14:00",
					"14:30",
					"15:00",
					"15:30",
					"16:00",
					"16:30",
					"17:00",
					"17:30",
					"18:00",
					"18:30",
				) ;
				foreach($tmp_array as $arr){
					if($qid_time == "" && ($arr >= $min)){
						$qid_time = $arr;
					}
				}
			}else{
				//檢查qid是否還可使用
				$rs_qid = $this->model_common_option->checkQid($this->request->get['qid']) ;
				$qid_flag = $rs_qid[0];
				$qid_arr = $rs_qid[1];
				$qid_time = $rs_qid[2];
			}


			if($qid_flag == "Y" || $qid_flag == "YY" ){
				$data['showDate1']  = date("Y-m-d") ;
				$data['showDate2']  = date("Y-m-d") ;
				//$data['showDate1']  = date("Y-m-25") ;
				//$data['showDate2']  = date("Y-m-25") ;
				$qid = $this->request->get['qid'];
			}
		}
		$data['qid_arr'] = $qid_arr ;
		$data['qid_flag'] = $qid_flag;
		$data['qid_time']  = $qid_time;
		$data['qid']  = $qid;

		$data['token'] = $this->session->data['tokenf'] ;
		$data['bt_rental_url'] = $this->url->link('order/rental', 'token=' . $this->session->data['tokenf'], 'SSL') ;
		// dump( $this->session) ;


		return $this->load->view('common/index_form_slider', $data) ;
	}

	protected function getRentTime() {
		return array(
				"08:00",
				"08:30",
				"09:00",
				"09:30",
				"10:00",
				"10:30",
				"11:00",
				"11:30",
				"12:00",
				"12:30",
				"13:00",
				"13:30",
				"14:00",
				"14:30",
				"15:00",
				"15:30",
				"16:00",
				"16:30",
				"17:00",
				"17:30",
				"18:00",
				"18:30",
			) ;
	}
}
