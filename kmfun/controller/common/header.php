<?php
class ControllerCommonHeader extends Controller {
	public function index() {

		$data = array() ;
		// dump( $this->session->data) ;

		if ( isset( $this->session->data['customer_id'])) {
			$data['user_id'] = $this->session->data['customer_id'] ;
		} else {
			$data['user_id'] = "" ;
		}

		// if ( $this->request->get['route'] == "common/home" || !isset( $this->request->get['route'])){
		if ( !isset( $this->request->get['route']) || ($this->request->get['route'] == "common/home")) {
			$data['html_body_id'] = "index" ;
		} else if($this->request->get['route'] == "information/bedinfo/detail"){
			$data['html_body_id'] = "bnb2" ;
		}else {
			$data['html_body_id'] = "order" ;
		}

		// 關於金豐 A01022BE-A6E7-E68A-6482-69B18B5F2AAC
		$data['urlStrAboutUs'] = $this->url->link('information/page', 'token=A01022BE-A6E7-E68A-6482-69B18B5F2AAC') ;

		// 租貸事宜 A2DDDBF2-82AA-C541-F603-64B8ED6CE4D7
		$data['urlStrRentInfo'] = $this->url->link('information/page', 'token=A2DDDBF2-82AA-C541-F603-64B8ED6CE4D7') ;

		// 行李接送規則 7EE3BA44-AC03-069F-EFF5-07E29CE55B25
		$data['urlStrPacketRule'] = $this->url->link('information/page', 'token=7EE3BA44-AC03-069F-EFF5-07E29CE55B25') ;

		// // 車型介紹 629E9C84-F27C-F462-FE98-0CBC9A13DDF8
		// $data['urlStrCarInfo'] = $this->url->link('information/page', 'token=629E9C84-F27C-F462-FE98-0CBC9A13DDF8') ;

		return $this->load->view('common/header', $data);
	}
}
