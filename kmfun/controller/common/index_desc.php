<?php
class ControllerCommonIndexDesc extends Controller {
	public function index() {
		$data = array() ;
		$data['qstrlen'] = isset( $this->request->get['qid']) ? strlen($this->request->get['qid']) : "" ;
		return $this->load->view('common/index_desc', $data);
	}
}
