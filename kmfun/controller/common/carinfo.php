<?php
/**
 * 2018.07.11 add by Angus
 * 檢查目前網路訂單可提供的車種及數量
 * http://localhost/kmfun/?route=common/carinfo&sd=2018-07-12 10:30:00&ed=2018-07-14 13:00:00&cn=1
 */
class ControllerCommonCarinfo extends Controller {
	public function index() {
		echo "檢查車輛狀況" ;
		$this->load->model('common/cars') ;

		$sDate     = $this->request->get['sd'] ;
		$eDate     = $this->request->get['ed'] ;
		$b_car_num = $this->request->get['cn'] ;


		$data['carSeeds'] = $this->model_common_cars->getCarsSeedArr(
				array( $sDate, $eDate , $b_car_num)) ;
		dump( $data['carSeeds']) ;
	}
}