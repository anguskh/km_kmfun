<?php
/**
 * 測試用
 * https://kmfun.tw/?route=common/mail
 */
class ControllerCommonMail extends Controller {
	public function index() {
		// dump( $this->config) ;

		$mail = new Mail();

		$mail->protocol      = $this->config->get('config_mail_protocol');
		$mail->parameter     = $this->config->get('config_mail_parameter');
		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
		// $mail->smtp_hostname = "ssl://smtp.gmail.com" ;
		$mail->smtp_username = $this->config->get('config_mail_smtp_username');
		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
		// $mail->smtp_username = "noreply.kmfun@gmail.com" ;
		// $mail->smtp_password = html_entity_decode( "shmayyeyxkssxgny", ENT_QUOTES, 'UTF-8') ; ;
		// $mail->smtp_port     = 465;
		$mail->smtp_port     = $this->config->get('config_mail_smtp_port');
		$mail->smtp_timeout  = $this->config->get('config_mail_smtp_timeout');

		$subject = "金豐租車有限公司{HTTP_SERVER}" ;
		$message = "恭喜你租車成功" ;

		// array("angus.kh@gmail.com","jaff.tw@gmail.com")
		$mail->setTo( array("angus.kh@gmail.com"));
		// $mail->setFrom($this->config->get('config_email'));
		$mail->setFrom( "no-reply@kmfun.com.tw");
		$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
		$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
		$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
		dump( $mail) ;
		$mail->send();

		// $mail->setTo("jaff.tw@gmail.com");
		// dump( $mail) ;
		// $mail->send();


	}
}
