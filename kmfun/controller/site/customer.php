<?php
/**
 * [aaa description]
 * @Another Angus
 * @date    2018-12-19
 */
// http://localhost/kmfun_Nopaper/?route=site/customer
// http://nopaper.kmfun.com.tw/?route=site/customer
// 2. 官網會員中心
//  防止老人家沒email
//  帳號：身分證(護照)
//  密碼：行動電話
//  功能：俢改密碼、基本資料、歷史訂單、忘記密碼
// curl https://notify-api.line.me/api/notify -H "Authorization: Bearer cvIhwJRFQsdyNaa9D4RLsj62JesAIHGELOiQcIDPfUq" -d "message=金豐祖車測試訊息"
class ControllerSiteCustomer extends Controller {
	private $error = array();
	private $data  = array();

	/**
	 * [isLocalhost description]
	 * @return  boolean    [description]
	 * @Another Angus
	 * @date    2018-12-21
	 */
	private function isLocalhost() {
		if (SERVER_ADDRESS != "localhost") {
			$this->data = $this->preparation() ;
		} else {
			$this->data['header'] = "" ; $this->data['footer'] = "" ;
		}
	}

	/**
	 * [index 首頁]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-12-19
	 */
	public function index() {

		// dump( $this->session->data) ;
		if ( !isset( $this->session->data['info']) || empty( $this->session->data['info'])) {
			$this->login() ;
		} else {
			$this->showStatus() ;
		}
	}

	/**
	 * [login 登入頁]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-12-19
	 */
	public function login() {
		$this->isLocalhost() ;

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateLoginForm()) {
			$this->response->redirect($this->url->link('site/customer', '', true)) ;
		}
		$this->data['warning'] = $this->error ;
		if ( isset( $this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'] ;
		}
		unset( $this->session->data['success']) ;
		// dump( $this->error) ;
		$this->response->setOutput($this->load->view('customer/login', $this->data)) ;
	}

	/**
	 * [forget 忘記密碼]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-12-19
	 */
	public function forget() {
		$this->isLocalhost() ;

		$this->data['success'] = "" ;
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForgetForm()) {
			$this->data['success'] = "己將密碼寄出" ;
			// $this->response->redirect($this->url->link('site/customer/forget', '', true)) ;
		}

		$this->data['error_warning'] = !empty($this->error) ? $this->error['warning'] : "" ;
		$this->response->setOutput($this->load->view('customer/forget', $this->data)) ;
	}

	/**
	 * [login 登出]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-12-19
	 */
	public function logout() {
		unset( $this->session->data['info']) ;
		unset( $this->session->data['customer_id']) ;
		$this->response->redirect($this->url->link('site/customer', '', true)) ;
	}

	/**
	 * [saveInfo description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-12-21
	 */
	public function saveInfo() {
		$this->isLocalhost() ;
		$this->load->model('common/customer') ;

		// 修改密碼 add by Angus 2018.12.26
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateChangePW_Form()) {
			$editArr['customer_id'] = $this->request->post['customer_id'] ;
			$editArr['password']    = $this->request->post['password'] ;

			$this->model_common_customer->setPassword( $editArr) ;
			$this->session->data['success'] = "修改完成";
			// exit() ;
			$this->response->redirect($this->url->link('site/customer', '', true)) ;
		}

		// 修改基本資料 add by Angus 2018.12.24
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateInfoSaveForm()) {
			// dump( $this->request->post) ;
			$editArr['customer_id'] = $this->request->post['customer_id'] ;
			$editArr['address_id']  = $this->request->post['address_id'] ;
			$editArr['name']        = $this->request->post['customer']['name'] ;
			$editArr['tel']         = $this->request->post['customer']['tel'] ;
			$editArr['email']       = $this->request->post['customer']['email'] ;
			$editArr['zipCode1']    = $this->request->post['zipcode1'] ;
			$editArr['zipCode2']    = $this->request->post['zipcode2'] ;
			$editArr['address']     = $this->request->post['address'] ;

			$this->model_common_customer->setCustomerInfo( $editArr) ;
			$this->session->data['success'] = "修改完成";

			$this->response->redirect($this->url->link('site/customer', '', true)) ;
		}
		$this->showStatus( true) ;
	}

	/**
	 * [showStatus 會員狀態]
	 * @param   boolean    $editFlag [description]
	 * @return  [type]               [description]
	 * @Another Angus
	 * @date    2018-12-21
	 */
	private function showStatus( $editFlag = false) {
		$this->isLocalhost() ;
		$showStatus = $editFlag ? true : false ;

		$this->load->model('common/customer') ;
		$this->load->model('common/option') ;
		$this->load->model('common/cars') ;
		$this->load->model('common/zip') ;

		$customer_id           = $this->session->data['info']['customer_id'] ;	// 流水號
		$rentUserId            = $this->session->data['info']['user_id'] ;		// 身份証字號
		$maskPID			   = $this->MaskString( $this->session->data['info']['user_id']) ;

		// 車站資訊
		$rentStation           = $this->model_common_option->getOptionItemsArr4Index( '15') ;
		// 訂單類別
		$orderType             = $this->model_common_option->getOptionItemsArr4Index( '8') ;
		// 車型資訊
		$carModel              = $this->model_common_cars->getCarSeedInfo() ;
		// 地址 城市
		$this->data['retZip']  = $this->model_common_zip->getZipCode() ;
		// dump( $this->data['retZip']) ;

		// 個人資料
		// dump( $this->session->data['info']) ;
		// $rowAddr = $this->model_common_customer->getCustomerAddress( 64) ;
		// dump( $infoAddress) ;

		$infoCustometr['customer_id'] = $customer_id ;
		if ( $showStatus) { // 修改中
			$this->data['hiddenInfo'] = "hidden" ;
			$this->data['hiddenEdit'] = "" ;

			$customerInfo               = $this->request->post['customer'] ;
			$infoCustometr['pid']       = $maskPID ;
			$infoCustometr['name']      = $customerInfo['name'] ;
			$infoCustometr['email']     = $customerInfo['email'] ;
			$infoCustometr['telephone'] = $customerInfo['tel'] ;
			// $infoCustometr['password']    = $this->request->post['customer']['name'] ;
			// $infoCustometr['create_date'] = $this->session->data['info']['date_added'] ;

			$this->data['selZipCode1']    = $this->request->post['zipcode1'] ;
			$this->data['selZipCode2']    = $this->request->post['zipcode2'] ;
			$infoAddress['address_2']     = $this->request->post['address'] ;	// 東坑路中和巷48弄19號
			$this->data['retZip2']        = $this->model_common_zip->getOptionZipCode( $this->request->post['zipcode1']) ;

			$rowAddr = $this->model_common_customer->getCustomerAddress( $customer_id) ;
			// $rowAddr = $this->model_common_customer->getCustomerAddress( 64) ;

			list( $selZipCode1, $retZip2) = $this->model_common_zip->zipCodeReverseLookup( $rowAddr['postcode']) ;
			$infoAddress['address_id']    = isset( $rowAddr['address_id'])	? $rowAddr['address_id'] : "" ;
			$infoAddress['address_1']     = isset( $rowAddr['address_1'])	? $rowAddr['address_1'] : "" ;
			$infoAddress['city']          = isset( $rowAddr['city'])		? $rowAddr['city'] : "" ;
			$infoAddress['postcode-zh']   = isset( $retZip2[$rowAddr['postcode']])	? $retZip2[$rowAddr['postcode']] : "" ;

		} else { // 沒修改
			$this->data['hiddenInfo'] = "" ;
			$this->data['hiddenEdit'] = "hidden" ;

			$infoCustometr['pid']         = $maskPID ;
			$infoCustometr['name']        = $this->session->data['info']['firstname'] ;
			$infoCustometr['email']       = $this->session->data['info']['email'] ;
			$infoCustometr['telephone']   = $this->session->data['info']['telephone'] ;
			$infoCustometr['password']    = $this->session->data['info']['password'] ;
			$infoCustometr['create_date'] = $this->session->data['info']['date_added'] ;

			$rowAddr = $this->model_common_customer->getCustomerAddress( $customer_id) ;
			// $rowAddr = $this->model_common_customer->getCustomerAddress( 64) ;

			$infoAddress['address_id']    = isset( $rowAddr['address_id'])	? $rowAddr['address_id'] : "" ;
			$infoAddress['name']          = isset( $rowAddr['firstname'])	? $rowAddr['firstname'] : "" ;
			$infoAddress['address_1']     = isset( $rowAddr['address_1'])	? $rowAddr['address_1'] : "" ;	// 東坑路中和巷48弄19號
			$infoAddress['address_2']     = isset( $rowAddr['address_1'])	? $rowAddr['address_1'] : "" ;	// 東坑路中和巷48弄19號
			$infoAddress['city']          = isset( $rowAddr['city'])		? $rowAddr['city'] : "" ;		// 臺中市
			$infoAddress['postcode']      = isset( $rowAddr['postcode'])	? $rowAddr['postcode'] : "" ;	// 423

			list( $selZipCode1, $retZip2) = $this->model_common_zip->zipCodeReverseLookup( $rowAddr['postcode']) ;
			$this->data['selZipCode1']    = $selZipCode1 ;
			$this->data['selZipCode2']    = $infoAddress['postcode'] ;
			$this->data['retZip2']        = $retZip2 ;
			$infoAddress['postcode-zh']   = isset( $retZip2[$rowAddr['postcode']])	? $retZip2[$rowAddr['postcode']] : "" ;	// 423
		}

		$this->data['infoCustometr'] = $infoCustometr ;
		$this->data['infoAddress']   = $infoAddress ;
		// 歷史資料
		$this->data['orderColName'] = $this->getOrderColName() ;
		$orderList = $this->model_common_customer->getOrderList( $rentUserId) ;
		foreach ($orderList as $iCnt => $row) {
			$orderList[$iCnt]['order_type'] = $orderType[$row['order_type']]['opt_name'] ;
			$orderList[$iCnt]['s_station']  = $rentStation[$row['s_station']]['opt_name'] ;
			$orderList[$iCnt]['e_station']  = $rentStation[$row['e_station']]['opt_name'] ;
			$orderList[$iCnt]['car_model']  = $carModel[$row['car_model']]['car_seed'] ;
		}
		$this->data['orderList'] = $orderList ;
		// $this->data['orderList'] = array() ;


		$this->data['error_warning'] = ( !empty($this->error)) ? join( "<br>", $this->error) : "" ;
		$this->data['success']       = isset( $this->session->data['success']) ? $this->session->data['success'] : "" ;
		unset( $this->session->data['success']) ;

		$this->response->setOutput($this->load->view('customer/customer_status', $this->data)) ;
	}

	/**
	 * [joinus description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-12-20
	 */
	public function joinus() {
		$this->isLocalhost() ;
		$this->load->model('common/zip') ;

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateJoinUsForm()) {
			$insArr['customer_group_id'] = 1 ;
			$insArr['user_id']           = $this->request->post['customer']['pid'] ;
			$insArr['firstname']         = $this->request->post['customer']['name'] ;
			$insArr['lastname']          = "" ;
			$insArr['email']             = $this->request->post['customer']['email'] ;
			$insArr['telephone']         = $this->request->post['customer']['tel'] ;
			$insArr['password']          = $this->request->post['password'] ;

			$insArr['zipcode1']          = $this->request->post['zipcode1'] ;
			$insArr['zipcode2']          = $this->request->post['zipcode2'] ;
			$insArr['address']           = $this->request->post['address'] ;


			$this->model_common_customer->addCustomer( $insArr) ;
			$this->session->data['success'] = "新增完成" ;
			$this->response->redirect($this->url->link('site/customer', '', true)) ;
		}
		$this->data['error_warning'] = ( !empty($this->error)) ? join( "<br>", $this->error) : "" ;
		$this->data['retZip']  = $this->model_common_zip->getZipCode() ;

		$this->response->setOutput($this->load->view('customer/joinus', $this->data)) ;
	}

	/**
	 * [validateChangePW_Form description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-12-26
	 */
	private function validateChangePW_Form() {
		if ( !empty($this->request->post['password']) && !empty($this->request->post['passwordconfirm'])) {
			return true ;
		}
		return false ;
	}

	/**
	 * [validateInfoSaveForm description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-12-21
	 */
	private function validateInfoSaveForm() {
		if ( !is_numeric( $this->request->post['customer_id']) || !is_numeric( $this->request->post['address_id'])) {
			$this->error['warning'] = "資料錯誤 請連絡系統管理員" ;
			return $this->error ;
		}

		if ( empty( $this->request->post['customer']['name'])) {
			$this->error['name'] = "會員姓名未填寫" ;
		}

		if ( empty( $this->request->post['customer']['tel'])) {
			$this->error['tel'] = "會員電話未填寫" ;
		}
		if ( empty( $this->request->post['customer']['email'])) {
			$this->error['tel'] = "會員電子郵件未填寫" ;
		}
		if ( empty( $this->request->post['zipcode1']) || empty( $this->request->post['zipcode2'])
				|| empty( $this->request->post['address'])) {
			$this->error['zipCode'] = "地址未填寫清楚" ;
		}

		return !$this->error ;
	}

	/**
	 * [validateLoginForm 登入檢查]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-12-19
	 */
	private function validateLoginForm() {
		$this->load->model('common/customer') ;

		if ( empty($this->request->post['customer_id'])) {
			$this->error['customer_id'] = "帳號未填寫" ;
		}
		if ( empty($this->request->post['password'])) {
			$this->error['password'] = "密碼未填寫" ;
		}

		if ( empty($this->error)) {
			if ( $retArr = $this->model_common_customer->loginCkeck( $this->request->post)) {
				$this->session->data['info'] = $retArr ;
				$this->session->data['customer_id'] = $this->MaskString($retArr['user_id']) ;
			} else {
				$this->error['warning'] = "帳號密碼錯誤" ;
			}
		}
		// dump( $this->error) ;
		return !$this->error;
	}

	/**
	 * [validateForgetForm description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-12-20
	 * https://www.w3schools.com/php/php_form_url_email.asp
	 */
	private function validateForgetForm() {
		$this->load->model('common/customer') ;

		// dump( $this->request->post) ;
		if (!filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
			$this->error['email'] = "email 格式不正確";
		}

		if ( empty($this->error)) {
			if ( $retArr = $this->model_common_customer->forgetCkeck( $this->request->post)) {
				$this->sendForgetMailMsg( $retArr) ;
			} else {
				$this->error['warning'] = "查無此帳號" ;
			}
		}

		// dump( $this->error) ;
		return !$this->error;
	}

	/**
	 * [validateJoinUsForm description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-01-01
	 */
	private function validateJoinUsForm() {
		// dump( $this->request->post) ;
		$this->load->model('common/customer') ;
		$isExist = $this->model_common_customer->getCustomerInfo( $this->request->post['customer']['pid']) ;
		// dump( $isExist) ;
		if ( $isExist > 0 ) {
			// dump( $isExist) ;
			$this->error['account'] = "帳號已存在 請試忘記密碼" ;
		}
		if (!filter_var($this->request->post['customer']['email'], FILTER_VALIDATE_EMAIL)) {
			$this->error['email'] = "email 格式不正確";
		}
		return !$this->error ;
	}

	/**
	 * [sendForgetMailMsg 忘記密碼重送mail]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2018-12-20
	 */
	private function sendForgetMailMsg( $data = array()) {
		$mail = new Mail();
		$sendTo = !empty( $data['email']) ? $data['email'] : "noreply.kmfun@gmail.com" ;

		$mail->protocol      = $this->config->get('config_mail_protocol') ;
		$mail->parameter     = $this->config->get('config_mail_parameter') ;
		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname') ;
		$mail->smtp_username = $this->config->get('config_mail_smtp_username') ;
		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8') ;
		$mail->smtp_port     = $this->config->get('config_mail_smtp_port') ;
		$mail->smtp_timeout  = $this->config->get('config_mail_smtp_timeout') ;

		// $subject = "金豐租車訂單成功通知信" ;
		$subject = "金豐租車 忘記密碼" ;
		$message = $this->buildForgetMailMsgContent( $data) ;

		$mail->setTo( $sendTo) ;
		$mail->setFrom($this->config->get('config_email')) ;
		$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) ;
		$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8')) ;
		$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8')) ;
		$mail->send();

		$sendMailArr = array(
			"kmfun005@gmail.com",
			"kmfun999@gmail.com"
		) ;
		if ( WEBSITE_TYPE != 'dev') {
			$mail->setTo( $sendMailArr) ;
			$mail->send();
		}

		$sendMailArr = array(
			"kh0813@gmail.com",
			"jaff.tw@gmail.com"
		) ;
		$mail->setTo( $sendMailArr) ;
		$mail->send();
	}

	/**
	 * [buildForgetMailMsgContent 忘記密碼 內容]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2018-12-20
	 */
	private function buildForgetMailMsgContent ( $data = array()) {
		$firstname = $data['firstname'] ;
		$lastname = $data['lastname'] ;
		$password = !empty($data['password']) ? $data['password'] : $data['telephone'] ;

		$pattern = "親愛的 %s %s 您好\n您的密碼是 : %s" ;

		return sprintf( $pattern, $firstname, $lastname, $password) ;
	}

	/**
	 * [preparation description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-12-20
	 */
	protected function preparation() {
		$data['footer'] = $this->load->controller('common/footer') ;
		$data['header'] = $this->load->controller('common/header') ;

		return $data ;
	}

	/**
	 * [getOrderColName description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-12-20
	 */
	protected function getOrderColName() {
		return array(
				"order_type"        => '訂單類別',
				"agreement_no"      => '訂單編號',
				"car_model"         => '車型',
				//"car_no"            => '車號',
				"rent_date_out"     => '取車日期',
				"rent_date_in_plan" => '還車日期',
				"s_station"         => '取車地點',
				"e_station"         => '還車地點',
				"total"             => '費用',
				"create_date"       => '訂單日期',
			) ;
	}

	/**
	 * [test_input description]
	 * @param   [type]     $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2018-12-20
	 */
	private function test_input($data) {
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}

	/**
	 * [MaskString 身份証 遮罩 / 字串遮罩]
	 * @param   [type]     $s       [description]
	 * @param   integer    $masknum [description]
	 * @Another Angus
	 * @date    2018-12-23
	 */
	private function MaskString($s, $masknum=6){
		$len= strlen($s);
		if($masknum<0) $masknum = $len + $masknum;
		if($len<3)return $s;
		elseif( $len< $masknum+1)return substr( $s, 0,1). str_repeat('*',$len-2). substr( $s, -1);
		$right=  ($len-$masknum)>>1;
		$left= $len- $right- $masknum;
		return substr( $s, 0,$left). str_repeat('*',$len-$right-$left). substr( $s, -$right);
	}


/*
           [customer_id] => 1459
            [customer_group_id] => 1
            [store_id] => 0
            [language_id] => 0
            [user_id] => A122843359
            [firstname] => 劉塗生
            [lastname] =>
            [email] => sherryueng@yahoo.com.tw
            [telephone] => 09332019820912230264
            [fax] =>
            [password] =>
            [salt] =>
            [cart] =>
            [wishlist] =>
            [newsletter] => 0
            [address_id] => 0
            [custom_field] =>
            [ip] =>
            [status] => 1
            [approved] => 1
            [safe] => 0
            [token] =>
            [code] =>
            [date_added] => 2018-12-04 18:16:47
 */


}

