<?php
/**
 * http://kmfun.tw/?route=order/ordersearch/stock
 * http://localhost/kmfun_Nopaper/?route=order/ordersearch/stock
 */
class Controllerorderorderpdf extends Controller {
	private $error = array() ;
	private $staff = array() ;

	public function index() {
		$data = array() ;

		$this->load->model('common/order') ;
		$this->load->model('common/cars') ;

		if(!is_null($this->request->get['o1']) && !is_null($this->request->get['m1'])){
			$rowInfo = $this->model_common_order->getOrderForCheckOrder( $this->request->get) ;
			$data['rentInfo'] = $rowInfo ;
			if ( !$rowInfo) {
				echo "查無訂單，請電洽 082-371888 與客服確認訂單！" ;
			}else{
				if ( !empty( $rowInfo) ) {
					$CarSeedSN = $this->model_common_cars->getCarInfoColor( " a.idx='{$rowInfo['car_sn']}' and a.car_no = '{$rowInfo['car_no']}'" ) ;
					$data['carSeed'] = $CarSeedSN;
				}
				$data['flightInfo'] = $this->model_common_cars->getFlightInfo($rowInfo['agreement_no']) ;
				//tb_car_condition 承租人出車簽名
				$mainPath = DIR_SIGN_UPLOAD ;
				$rsarr = $this->model_common_cars->getCarCondition($rowInfo['agreement_no']);
				$data["img_sign"] = "";
				$data["img_sign_in"] = "";
				$data["create_man"] = $rsarr[2];
				$data["return_man"] = $rsarr[3];
				$data["create_date"] = $rsarr[4];
				$data["return_date"] = $rsarr[5];
				if($rsarr[0] != ''){
					//取車
					$data["img_sign"] = '<img src="'.$mainPath.$rsarr[0].'" style="height:40px">';
				}
				if($rsarr[1] != ''){
					//還車
					$data["img_sign_in"] = '<img src="'.$mainPath.$rsarr[1].'" style="height:40px">';
				}

				// 使用天數
				$second1 = strtotime( $rowInfo['rent_date_in_plan']) - strtotime( $rowInfo['rent_date_out']) ;
				$data['rentInfo']['rentDay'] 	= floor( $second1 / 86400) ;	// 24hr * 60分 * 60秒
				$data['rentInfo']['rentHour'] 	= ( $second1 % 86400) / 3600 ;	//

				// 早鳥折數
				$data['rentInfo']['discountPercentage'] = $this->model_common_order->getDiscountPercentage( $data['rentInfo']['rent_date_out'], $data['rentInfo']['create_date']) ;

				// 產生費用
				if ( $data['rentInfo']['rentDay'] == 0 ) {
					$data['rentInfo']['car_price2'] = $data['carSeed']['car_price'] * $data['rentInfo']['discountPercentage'] * $data['rentInfo']['total_car'] ;

				} else if ( $data['rentInfo']['rentDay']  >= 1 && $data['rentInfo']['rentHour'] <= 6) {
					$data['rentInfo']['car_price2'] = ($data['carSeed']['car_price'] * $data['rentInfo']['rentDay'] * $data['rentInfo']['discountPercentage'] + $data['carSeed']['car_price'] * 0.1 * ceil( $data['rentInfo']['rentHour']) ) * $data['rentInfo']['total_car'] ;
				} else { //if ( $rentInfo['rentDay']  > 1 && $rentInfo['rentHour'] > 6) {
					$data['rentInfo']['car_price2'] = $data['carSeed']['car_price'] * ( $data['rentInfo']['rentDay'] + 1) * $data['rentInfo']['discountPercentage'] * $data['rentInfo']['total_car'] ;
				}
				$data['rentInfo']['car_price2Str'] = number_format( $data['rentInfo']['car_price2']) ;
				// 配件使用天數
				$useDay = round( ( strtotime( $data['rentInfo']['rent_date_in_plan']) - strtotime( $data['rentInfo']['rent_date_out']))/3600/24) + 1 ;
				// bc => 兒童安全座椅〔一歲以下〕, bc1 => 兒童安全座椅〔一歲以上〕, bc2 => 兒童安全座椅〔增高墊〕
				if($data['rentInfo']['baby_chair'] > 0 || $data['rentInfo']['baby_chair1'] > 0 || $data['rentInfo']['baby_chair2'] > 0 ||
					$data['rentInfo']['children_chair'] > 0 || $data['rentInfo']['gps'] > 0 || $data['rentInfo']['mobile_moto'] > 0) {
					$data['rentInfo']['car_price3'] = ($data['rentInfo']['baby_chair'] * 100 * $useDay) +
											($data['rentInfo']['baby_chair1'] * 100 * $useDay) +
											($data['rentInfo']['baby_chair2'] * 100 * $useDay) +
											($data['rentInfo']['children_chair'] * 100 * $useDay) +
											($data['rentInfo']['gps'] * 0 * $useDay) +
											($data['rentInfo']['mobile_moto'] * 50 * $useDay) ;
					$data['rentInfo']['baby_chair1_price'] = $data['rentInfo']['baby_chair1'] * 100 * $useDay;
					$data['rentInfo']['children_chair_price'] = $data['rentInfo']['children_chair'] * 100 * $useDay;
					$data['rentInfo']['car_price3Str'] = number_format( $data['rentInfo']['car_price3']) ;

				} else {
					$data['rentInfo']['car_price3']    = 0 ;
					$data['rentInfo']['car_price3Str'] = 0 ;
					$data['rentInfo']['baby_chair1_price'] = 0;
					$data['rentInfo']['children_chair_price'] = 0;

				}
				$data['rentInfo']['useDay'] = $useDay;
				$data['rentInfo']['totalStr'] = number_format( $data['rentInfo']['car_price2'] + $data['rentInfo']['car_price3']) ;


						// 訂單結帳狀態
				$rowInfo['RtnMsg'] = "此筆訂單尚未完成信用卡授權，請電洽 082-371888 與客服確認訂單！";
				if($rowInfo['checkout'] != "" && $rowInfo['order_status'] != 'x') {
					$EcRet = $this->model_common_order->checkEcReturnCode($rowInfo['order_id']) ;
					if($EcRet['RtnCode'] == 1)
						$rowInfo['RtnMsg'] = $EcRet['RtnMsg'];
				}

				$this->response->setOutput($this->load->view('order/ordercart', $data)) ;
			}
		}else{
			echo "查無訂單，請電洽 082-371888 與客服確認訂單！" ;
		}
	}
}
