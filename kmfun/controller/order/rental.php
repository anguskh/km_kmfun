<?php
class ControllerOrderRental extends Controller {
	private $error = array();

	public function index() {
		$data = $this->preparation() ;
		$rentInfo = array() ;

		$this->load->model('common/option') ;
		$this->load->model('common/cars') ;
		$this->load->model('common/order') ;
		$this->load->model('tool/image') ;


		$data['stepFlag'] = array( "ok", "no", "no", "no") ;
		$officeArr = [
						'16'=>['t1'=>'08:30', 't2'=>'18:00'],
						'17'=>['t1'=>'08:00', 't2'=>'17:00'],
						'18'=>['t1'=>'08:30', 't2'=>'18:00'],
						 ] ;
		// dump( $officeArr) ;
		// dump( [$this->request->get['s1'], $this->request->get['s2'], $this->request->get['t1'], $this->request->get['t2']]) ;
		// 檢查租車時間是否有超過
		$sRentTime = strtotime($this->request->get['d1'] .' '.$this->request->get['t1']) ;
		$eRentTime = strtotime($this->request->get['d2'] .' '.$this->request->get['t2']) ;
		$scsTime   = strtotime($this->request->get['d1'] .' '.$officeArr[$this->request->get['s1']]['t1']) ;
		$ecsTime   = strtotime($this->request->get['d1'] .' '.$officeArr[$this->request->get['s1']]['t2']) ;
		$sceTime   = strtotime($this->request->get['d2'] .' '.$officeArr[$this->request->get['s2']]['t1']) ;
		$eceTime   = strtotime($this->request->get['d2'] .' '.$officeArr[$this->request->get['s2']]['t2']) ;

		if ( $scsTime > $sRentTime) $this->request->get['t1'] = $officeArr[$this->request->get['s1']]['t1'] ;
		if ( $ecsTime < $sRentTime) $this->request->get['t1'] = $officeArr[$this->request->get['s1']]['t2'] ;
		if ( $sceTime > $eRentTime) $this->request->get['t2'] = $officeArr[$this->request->get['s2']]['t1'] ;
		if ( $eceTime < $eRentTime) $this->request->get['t2'] = $officeArr[$this->request->get['s2']]['t2'] ;

		// dump( [$this->request->get['s1'], $this->request->get['s2'], $this->request->get['t1'], $this->request->get['t2']]) ;

		list( $year, $month, $day)	= explode('-', $this->request->get['d1']) ;
		$rentInfo['startDate']		= "{$year}年{$month}月{$day}日 {$this->request->get['t1']}" ;
		$rentInfo['startDateYMD']	= $this->request->get['d1'] . " " . $this->request->get['t1'] ;
		list( $year, $month, $day)	= explode('-', $this->request->get['d2']) ;
		$rentInfo['endDate']		= "{$year}年{$month}月{$day}日 {$this->request->get['t2']}" ;
		$rentInfo['endDateYMD']		= $this->request->get['d2'] . " " . $this->request->get['t2'] ;
		$second1 = floor( ( strtotime( $rentInfo['endDateYMD']) - strtotime( $rentInfo['startDateYMD']))) ;
		$rentInfo['rentDay'] 		= floor( $second1 / 86400) ;	// 24hr * 60分 * 60秒
		$rentInfo['rentHour'] 		= ( $second1 % 86400) / 3600 ;	//
		$rentInfo['qid'] 			= isset( $this->request->get['qid']) ? $this->request->get['qid'] : "" ;	// qrcode_id


		$rentInfo['s1'] = $this->request->get['s1'] ;
		$rentInfo['s2'] = $this->request->get['s2'] ;
		$rentInfo['d1'] = $this->request->get['d1'] ;
		$rentInfo['d2'] = $this->request->get['d2'] ;
		$rentInfo['t1'] = $this->request->get['t1'] ;
		$rentInfo['t2'] = $this->request->get['t2'] ;
		// DiscountDay 計算 早鳥折數
		$rentInfo['DiscountPercentage'] = $this->model_common_order->getDiscountPercentage( $rentInfo['d1']) ;

		$rentInfo['b_car_num'] = ($this->request->get['bcn'] > 1) ? 1 : $this->request->get['bcn'];
		$rentInfo['adults']    = $this->request->get['adults'] ;
		$rentInfo['children']  = $this->request->get['children'] ;
		$rentInfo['babies']    = $this->request->get['babies'] ;
		$rentInfo['coupon']    = $this->request->get['coupon'] ;

		$data['token'] = $this->session->data['tokenf'] ;

		$this->session->data['rentInfo'] = serialize( $rentInfo) ;
		$data['rentInfo'] = $rentInfo ;

		$data['rentStations'] = $this->model_common_option->getOptionItemsArr( '15') ;
		$data['carModels'] = $this->model_common_option->getOptionItemsArr( '1') ;
		// dump( $data['carModels']) ;

		$data['carSeeds'] = $this->model_common_cars->getCarsSeedArr( array( $rentInfo['startDateYMD'], $rentInfo['endDateYMD'], $rentInfo['b_car_num'])) ;

		foreach ($data['carSeeds'] as $key => $carSeeds) {
			foreach ($carSeeds as $iCnt => $tmpRow) {

				// 費用 -------------------------------------------------------------------------------------------------
				$tmpRow['car_unit_price'] = number_format( $tmpRow['car_price']) ;

				if ( $rentInfo['rentDay'] == 0 ) {
					$tmpRow['car_price2'] = number_format( $tmpRow['car_price'] * $rentInfo['b_car_num'] * $rentInfo['DiscountPercentage']) ;
				} else if ( $rentInfo['rentDay']  >= 1 && $rentInfo['rentHour'] <= 6) {
					$tmpRow['car_price2'] = number_format(
							( $tmpRow['car_price'] * $rentInfo['rentDay'] * $rentInfo['DiscountPercentage'] + $tmpRow['car_price'] * 0.1 * ceil( $rentInfo['rentHour']) ) * $rentInfo['b_car_num']
							) ;
				} else { //if ( $rentInfo['rentDay']  > 1 && $rentInfo['rentHour'] > 6) {
					$tmpRow['car_price2'] = number_format( $tmpRow['car_price'] * ( $rentInfo['rentDay'] + 1) * $rentInfo['DiscountPercentage'] * $rentInfo['b_car_num']) ;
				}
				// 費用 -------------------------------------------------------------------------------------------------

				// 車子圖片
				if ( $tmpRow['image'] && is_file(DIR_IMAGE . $tmpRow['image'])) {
					$tmpRow['thumb'] = $this->model_tool_image->resize($tmpRow['image'], 230, 134) ;
				} else {
					$tmpRow['thumb'] = $this->model_tool_image->resize('no_image.png', 230, 134);
				}

				$data['carSeeds'][$key][$iCnt] = $tmpRow ;
			}
		}
		// dump( $data['carModels']) ;
		// dump( $data['carSeeds']) ;
		// dump( $this->session->data['rentInfo']) ;
		// dump( $rentInfo) ;

		$this->response->setOutput($this->load->view('order/step1', $data)) ;
	}

	public function step2() {

		$data = $this->preparation() ;
		$data['stepFlag'] = array( "ok", "ok", "no", "no") ;
		$rentInfo = unserialize( $this->session->data['rentInfo']) ;

		// 2019.07.08 星期一14:53 金豐租車-陳昱筑 您好要請您幫忙鎖配件7/19-7/21(安全座椅及增高墊 ) by Angus 2019.07.17
		// 2019.08.13 星期二16:23 金豐租車-張月馨 09/13-09/15的嬰兒推車可以鎖起來嗎? by Angus 2019.08.13
		// 2019.09.24 星期二13:52 金豐租車-郭心怡 麻煩協助鎖上唷 10/18-10/20 一歲以下安元座椅已預約額滿 by Angus 2019.09.30
		// 2019.11.21 星期四09:34 金豐租車-郭心怡 麻煩協助鎖12/7-12/8 推車2台 已接滿了 by Angus 2019.11.21
		// 2019.11.28 星期四09:01 金豐租車-王淑怡 麻煩協助鎖01/27-01/31 增高墊 已接滿了
		// 2020.01.14 星期二10:41 金豐租車-郭心怡 2/28 -3/1 增高墊已額滿 請協助鎖庫存
		// 2020.01.20 星期一12:36 金豐租車-王淑怡 2/28 -3/1 嬰兒推車已額滿 請協助鎖庫存
		// 2020.01.22 星期三10:12 金豐租車-王淑怡 1/30-02/01 嬰兒推車已額滿 請協助鎖庫存 01/26安全座椅已額滿 請協助鎖庫存
		// 2020.11.26 11:26 金豐租車-郭心怡 1/24-1/26 推車已額滿囉
		// 2020.11.26 16:39 金豐租車-陳恩平 11/28~11/29兒童安全座椅也已經預約額滿了，麻煩協助上鎖，謝謝
		// 2021.02.12 星期五14:25 金豐租車-郭心怡 2/15-2/17 兒童安全座椅 及嬰兒安全座椅 已額滿 @Angus 陳冠欣 @周哲弘
		// 10/7~10/11的安全椅也麻煩幫忙鎖一下
		//
		// 0 兒童安全座椅〔一歲以下〕
		// 1 兒童安全座椅〔一歲以上〕
		// 2 兒童安全座椅〔增高墊〕
		// 3 嬰兒推車
		$fittingTime = array(
			"0" => array( array( strtotime( '2020-01-26 00:00:00'), strtotime( '2020-01-27 00:00:00')),
						  array( strtotime( '2020-09-30 00:00:00'), strtotime( '2020-10-03 00:00:00')),
						  array( strtotime( '2020-10-07 00:00:00'), strtotime( '2020-10-12 00:00:00')),
						  array( strtotime( '2020-11-28 00:00:00'), strtotime( '2020-11-30 00:00:00')),
				          array( strtotime( '2021-02-15 00:00:00'), strtotime( '2021-02-18 00:00:00')),
						),
			"1" => array( array( strtotime( '2020-01-26 00:00:00'), strtotime( '2020-01-27 00:00:00')),
						  array( strtotime( '2020-09-30 00:00:00'), strtotime( '2020-10-03 00:00:00')),
						  array( strtotime( '2020-10-07 00:00:00'), strtotime( '2020-10-12 00:00:00')),
						  array( strtotime( '2020-11-28 00:00:00'), strtotime( '2020-11-30 00:00:00')),
				          array( strtotime( '2021-02-15 00:00:00'), strtotime( '2021-02-18 00:00:00')),
						),
			"2" => array( array( strtotime( '2020-08-26 00:00:00'), strtotime( '2020-08-30 00:00:00')),
						  array( strtotime( '2020-09-30 00:00:00'), strtotime( '2020-10-06 00:00:00')),
						  array( strtotime( '2020-10-07 00:00:00'), strtotime( '2020-10-12 00:00:00')),
						),
			"3" => array( array( strtotime( '2020-01-30 00:00:00'), strtotime( '2020-02-01 00:00:00')),
						  array( strtotime( '2020-02-28 00:00:00'), strtotime( '2020-03-02 00:00:00')),
						  array( strtotime( '2021-01-24 00:00:00'), strtotime( '2021-01-27 00:00:00')),
						),
		) ;

		$data['fittingRelay'] = $this->checkFittingRelay( $rentInfo, $fittingTime) ;
		// dump( $data['fittingRelay']) ;

		$rentInfo['carIdx'] = $this->request->get['carIdx'] ;
		$data['token']      = $this->session->data['tokenf'] ;

		$this->load->model('common/cars') ;
		$carSeed = $this->model_common_cars->getCarSeed( $this->request->get['carIdx']) ;
		// dump( $carSeed) ;
		$data['car_model'] = $carSeed['car_model'] ;
		$data['optionCnt'] = $rentInfo['b_car_num'] ;
		$this->load->model('tool/image') ;
		if ($carSeed['image'] && is_file(DIR_IMAGE . $carSeed['image'])) {
			$carSeed['thumb'] = $this->model_tool_image->resize($carSeed['image'], 230, 134) ;
		} else {
			$carSeed['thumb'] = $this->model_tool_image->resize('no_image.png', 230, 134);
		}

		$this->load->model('common/option') ;
		$data['carModels'] = $this->model_common_option->getOptionItemsArr( '1') ;
		foreach ($data['carModels'] as $key => $carModels) {
			if($carModels['idx'] == $carSeed['car_model']) {
				$rentInfo['car_model_name'] = $carModels['opt_name'] ;
			}
		}

		// 下面這段好像用不到
		// $startdate = strtotime( $rentInfo['startDateYMD']) ;
		// $enddate   = strtotime( $rentInfo['endDateYMD']) ;
		// $rentDays  = ceil( ( $enddate-$startdate) / 3600 / 24) ;
		// $rentInfo['rentDays'] = $rentDays ;
		// 計算租車費用
		$rentInfo['car_price1'] = number_format( $carSeed['car_price']) ;
		if ( $rentInfo['rentDay'] == 0 ) {
			$rentInfo['car_price2'] = $carSeed['car_price'] * $rentInfo['b_car_num'] * $rentInfo['DiscountPercentage'] ;
		} else if ( $rentInfo['rentDay']  >= 1 && $rentInfo['rentHour'] <= 6) {
			$rentInfo['car_price2'] = ( $carSeed['car_price'] * $rentInfo['rentDay'] * $rentInfo['DiscountPercentage'] + $carSeed['car_price'] * 0.1 * ceil( $rentInfo['rentHour']) ) * $rentInfo['b_car_num'] ;
		} else { //if ( $rentInfo['rentDay']  > 1 && $rentInfo['rentHour'] > 6) {
			$rentInfo['car_price2'] = $carSeed['car_price'] * ( $rentInfo['rentDay'] + 1) * $rentInfo['DiscountPercentage'] * $rentInfo['b_car_num'] ;
		}


		$rentInfo['carSeed'] = $carSeed ;
		$this->session->data['rentInfo'] = serialize( $rentInfo) ;
		$data['rentInfo'] = $rentInfo ;
		// dump( $rentInfo) ;
		$this->response->setOutput($this->load->view('order/step2', $data)) ;
	}

	public function step3() {
		$this->load->model('common/order') ;
		$this->load->model('common/cars') ;
		$this->load->model('common/option') ;

		// 計算配件金額
		$rentInfo = unserialize( $this->session->data['rentInfo']) ;
		$rentInfo['car_price3'] = 0 ;
		$rentInfo['car_price2Str'] = number_format( $rentInfo['car_price2']) ;

		$useDay = round( (strtotime($rentInfo['d2']) - strtotime($rentInfo['d1']))/3600/24) + 1 ;
		// $useDay = $rentInfo['rentDay'] + (($rentInfo['rentHour'] > 0) ? 1 : 0) ;
		$rentInfo['useDay'] = $useDay ;
		// bc => 兒童安全座椅〔一歲以下〕, bc1 => 兒童安全座椅〔一歲以上〕, bc2 => 兒童安全座椅〔增高墊〕
		if($this->request->get['bc'] > 0 || $this->request->get['bc1'] > 0 || $this->request->get['bc2'] > 0 ||
			$this->request->get['cc'] > 0 || $this->request->get['gp'] > 0 || $this->request->get['mm'] > 0) {
			$rentInfo['car_price3'] = ($this->request->get['bc'] * 100 * $useDay) +
									($this->request->get['bc1'] * 100 * $useDay) +
									($this->request->get['bc2'] * 100 * $useDay) +
									($this->request->get['cc'] * 100 * $useDay) +
									($this->request->get['gp'] * 0 * $useDay) +
									($this->request->get['mm'] * 50 * $useDay) ;
			$rentInfo['total'] = $rentInfo['car_price2'] + $rentInfo['car_price3'] ;

			$rentInfo['car_price3Str'] = number_format( $rentInfo['car_price3']);
			$rentInfo['totalStr'] = number_format( $rentInfo['total']);
		} else {
			$rentInfo['car_price3Str'] = 0 ;
			$rentInfo['total']         = $rentInfo['car_price2'] ;
			$rentInfo['totalStr']      = number_format( $rentInfo['total']) ;
		}
		$this->session->data['rentInfo'] = serialize($rentInfo) ;

		// dump( $rentInfo) ;
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			//先檢查qid是否有效
			$qid_flag = "";
			if (isset($rentInfo['qid']) && $rentInfo['qid']!='') {
				if($rentInfo['qid'] == "3469FE736EF31BB7" || $rentInfo['qid'] == "E1E3C87AFF11D26A" || $rentInfo['qid'] == "7518FAD75806FC49" ){
					// 3469FE736EF31BB7 金城16  E1E3C87AFF11D26A 金胡17 7518FAD75806FC49尚義18
					$rent_arr = array("3469FE736EF31BB7"=>"16", "E1E3C87AFF11D26A"=>"17", "7518FAD75806FC49"=>"18");
					$data["rent_station"] = $rent_arr[$rentInfo['qid']];
					$qid_flag = "YY";
				}else{
					//檢查qid是否還可使用
					$rs_qid   = $this->model_common_option->checkQid($rentInfo['qid']) ;
					$qid_flag = $rs_qid[0] ;
					$qid_arr  = $rs_qid[1] ;
					$qid_time = $rs_qid[2] ;

					if ($qid_flag != "Y") {
						$qid_flag = "N";
					}
				}
			}

			if( $qid_flag == "N") {
				//qrcode有誤 顯示qrcode不能使用 離開
				echo"<script> alert(\"qrcode逾時，請洽管理員\"); /*location.href = '".HTTP_SERVER."';*/</script>";
			} else {
				$rentInfo = unserialize( $this->session->data['rentInfo']) ;
				// dump( $rentInfo) ;

				//檢查qid_flag = "Y"
				if($qid_flag == "Y" || $qid_flag == "YY" ){
					$insData['qrcode']        = $qid_arr["qrcode"];
					$insData['charge_method'] = 1;
					$insData['order_type']    = 140 ; // qrcode訂車
					if($qid_flag == "YY"){
						$insData['order_type']    = 147 ; // 前台業務qrcode訂車
					}
				} else {
					$insData['order_type']    = 9 ; // 網路訂單
				}

				$insData['agreement_no']      = $this->newAgreementNo() ;
				$insData['car_model']         = $rentInfo['carIdx'] ;
				$insData['s_station']         = $rentInfo['s1'] ;
				$insData['e_station']         = $rentInfo['s2'] ;
				$insData['rent_date_out']     = $rentInfo['d1'] ." ".$rentInfo['t1'] ;
				$insData['rent_date_in_plan'] = $rentInfo['d2'] ." ".$rentInfo['t2'] ;
				$insData['sday']              = date("j", strtotime( $rentInfo['d1'])) ;
				$insData['eday']              = date("j", strtotime( $rentInfo['d2'])) ;

				$insData['baby_chair']        = $rentInfo['bc'] ;
				$insData['baby_chair1']       = $rentInfo['bc1'] ;
				$insData['baby_chair2']       = $rentInfo['bc2'] ;
				$insData['children_chair']    = $rentInfo['cc'] ;
				$insData['gps']               = $rentInfo['gps'] ;
				$insData['mobile_moto']       = $rentInfo['mm'] ;

				$insData['adult']             = $rentInfo['adults'] ;
				$insData['baby']              = $rentInfo['babies'] ;
				$insData['children']          = $rentInfo['children'] ;
				$insData['total_car']         = $rentInfo['b_car_num'] ;

				// 早鳥優惠折數 by Angus 2020.07.04
				$insData['sale_off']          = $rentInfo['DiscountPercentage'] ;
				// 總價 by Justice 2017.1.18
				$insData['total']             = $rentInfo['total'] ;

				// 取得from post 承租人資料
				$insData['rent_user_name']    = $this->request->post['input_rent_name'] ;
				$insData['rent_identity']     = $this->request->post['input_identity'] ;
				$insData['rent_user_id']      = $this->request->post['input_rent_user_id'] ;
				$insData['rent_user_born']    = $this->request->post['input_bornday'] ;
				$insData['rent_user_tel']     = $this->request->post['input_mobile'] ;
				$insData['rent_user_mail']    = $this->request->post['input_rent_email'] ;
				$insData['coupon']            = $rentInfo['coupon'] ;

				// 取得from post 取車人資料
				$insData['get_user_name']     = $this->request->post['input_get_name'] ;
				$insData['get_identity']      = $this->request->post['input_get_identity'] ;
				$insData['get_user_id']       = $this->request->post['input_get_user_id'] ;
				$insData['get_user_born']     = $this->request->post['input_get_bornday'] ;
				$insData['get_user_tel']      = $this->request->post['input_get_mobile'] ;

				// 其他通訊資料
				$insData['rent_zip1']         = $this->request->post['input_rent_zipcode1'] ;
				$insData['rent_zip2']         = $this->request->post['input_rent_zipcode2'] ;
				$insData['rent_user_address'] = $this->request->post['input_rent_address'] ;

				$insData['rent_company_name'] = $this->request->post['input_company'] ;

				if ( $this->request->post['input_company_no'] != "如需開立統編請填寫") {
					$insData['rent_company_no'] = $this->request->post['input_company_no'] ;
				} else {
					$insData['rent_company_no'] = "" ;
				}
				$insData['urgent_man']			= $this->request->post['input_urgent_man'] ;
				$insData['urgent_mobile']		= $this->request->post['input_urgent_mobile'] ;

				$carInfoFilter = array(
						"car_seed"		=> $rentInfo['carIdx'],
						"start_date"	=> $insData['rent_date_out'],
						"end_date"		=> $insData['rent_date_in_plan'],
						"car_limit"		=> $rentInfo['b_car_num'],
					) ;
				$carCntInfo = $this->model_common_cars->getCarInfo( $carInfoFilter) ;
				// dump( $carCntInfo) ;
				$insData['carCntInfo'] = serialize( $carCntInfo) ;

				$insData['car_sn'] = $carCntInfo[0]['idx'] ;
				$insData['car_no'] = $carCntInfo[0]['car_no'] ;

				// 檢查航班資訊
				if ( !empty( $this->request->post['air_port']) && $this->request->post['air_port'] != "xx") {
					$airportArr = $this->model_common_option->getOptionArrUseParent( array(134, 131)) ; // 航/船班資訊
					// dump( $airportArr) ;
					foreach ($airportArr as $iCnt => $tmpRow) {
						if ( $tmpRow['idx'] == $this->request->post['air_port']) {
							if ( $tmpRow['parent'] == 134) {
								$insData['trans_type'] = 1 ;
								$insData['airport']    = $this->request->post['air_port'] ;
								$insData['pier']       = "" ;
							} else {
								$insData['trans_type'] = 2 ;
								$insData['airport']    = "" ;
								$insData['pier']       = $this->request->post['air_port'] ;
							}
							$insData['flight_sDate'] = $this->request->post['air_date'] . " " . $this->request->post['air_hour'] . ":" . $this->request->post['air_minute'] ;
							$rentInfo['flight_point'] = $tmpRow['opt_short_desc'] ;
							$rentInfo['flight_sDate'] = $insData['flight_sDate'] ;
							break ;
						}
					}
				} else {
					$insData['trans_type']    = "" ;
					$rentInfo['flight_point'] = "自取" ;
					$rentInfo['flight_sDate'] = "" ;
				}
				// dump( $insData) ;
				// exit();
				// to do 車輛最後哩程


				// 寫入資料庫
				// 檢查是否有加入會員 add by Angus 2018.04.14
				$checkInfo = $this->model_common_order->checkCustomerByPid( $insData['rent_user_id']) ;
				if ( empty( $checkInfo) ) {
					$this->model_common_order->newCustomer( $insData) ;
				}

				$this->model_common_order->newOrder( $insData, $qid_flag) ;
				foreach ($carCntInfo as $iCnt => $carInfo) {
					$insData['car_sn'] = $carInfo['idx'] ;
					$insData['car_no'] = $carInfo['car_no'] ;
					$this->model_common_order->newScheduling( $insData) ; // 航班也在這裡 add by Angus 2018.07.21
				}

				// exit() ;
				$rentInfo['car_no'] = $insData['car_no'] ;
				$rentInfo['agreement_no']= $insData['agreement_no'] ;
				// 承租人資料
				$rentInfo['input_rent_name']		= $this->request->post['input_rent_name'] ;
				$rentInfo['input_identity']		= $this->request->post['input_identity'] ;
				$rentInfo['input_rent_user_id']		= $this->request->post['input_rent_user_id'] ;
				$rentInfo['input_bornday']			= $this->request->post['input_bornday'] ;
				$rentInfo['input_mobile']			= $this->request->post['input_mobile'] ;
				$rentInfo['input_rent_email']		= $this->request->post['input_rent_email'] ;
				// 取車人資料
				$rentInfo['input_get_name']			= $this->request->post['input_get_name'] ;
				$rentInfo['input_get_identity']		= $this->request->post['input_get_identity'] ;
				$rentInfo['input_get_user_id']		= $this->request->post['input_get_user_id'] ;
				$rentInfo['input_get_bornday']		= $this->request->post['input_get_bornday'] ;
				$rentInfo['input_get_mobile']		= $this->request->post['input_get_mobile'] ;
				// 其他通訊資料
				$this->load->model('common/zip') ;
				$allZipArr = $this->model_common_zip->getAllZipCode() ;

				$rentInfo['input_rent_address']		= $allZipArr[$insData['rent_zip1']].$allZipArr[$insData['rent_zip2']].$this->request->post['input_rent_address'] ;
				$rentInfo['input_company']			= $this->request->post['input_company'] ;
				$rentInfo['input_company_no']		= $this->request->post['input_company_no'] ;

				$rentInfo['input_urgent_man']		= $this->request->post['input_urgent_man'] ;
				$rentInfo['input_urgent_mobile']	= $this->request->post['input_urgent_mobile'] ;

				// dump( $rentInfo) ;
				$this->session->data['rentInfo'] = serialize( $rentInfo) ;
				$this->session->data['agreement_no'] = $insData['agreement_no']	;
				$this->session->data['qid_flag'] = $qid_flag;

				//送出至綠界 by Justice 2017.1.18
				if ( DEBUG_MOD || $qid_flag=="Y" || $qid_flag=="YY") {
					$this->response->redirect($this->url->link('order/rental/step4', 'token=' . $this->session->data['tokenf'], true));
				} else {
					$this->response->redirect($this->url->link('extension/payment/ecpay/redirect', 'token=' . $this->session->data['tokenf'], true));
				}
			}

		}

		$this->getForm() ;
	}

	public function step4() {
		$this->load->model('common/order') ;
		$objLog = new log( 'ecpay4End.log') ;


		$data = $this->preparation() ;
		$data['stepFlag'] = array( "ok", "ok", "ok", "ok") ;
		if ( isset( $this->session->data['rentInfo'])) {
			$objLog->write( $this->session->data) ;
			$rentInfo         = unserialize( $this->session->data['rentInfo']) ;
			// dump( $rentInfo) ;
			// dump( $this->session->data) ;
			$data['rentInfo'] = $rentInfo ;
			if($this->session->data['qid_flag']=="Y" || $this->session->data['qid_flag']=="YY"){
				//查order_sn 寫入session
				$tmp_order = $this->model_common_order->getOrder($rentInfo['agreement_no']);
				$this->session->data['order_sn'] = $tmp_order['order_id'];
			}

			$checkSQLCmd = $this->model_common_order->checkEcReturnCodeCmd( $this->session->data['order_sn']) ;
			$objLog->write( $checkSQLCmd) ;
			$ecRtn = $this->model_common_order->checkEcReturnCode( $this->session->data['order_sn']) ;
			$objLog->write( $ecRtn) ;
			if ( DEBUG_MOD || $this->session->data['qid_flag']=="Y" || $this->session->data['qid_flag']=="YY") {
				$ecRtn['RtnCode'] = 1 ;

			}
			if ( $ecRtn && $ecRtn['RtnCode'] <= 1 ) { // 訂單成立
				/*
					filter_var( $data['rentInfo']['input_rent_email'], FILTER_VALIDATE_EMAIL) return true;
					filter_var(variable, filter, options) 檢查mail格式是否正確
					add by Angus 2018.03.20
				 */
				if ( !empty( $data['rentInfo']['input_rent_email']) && filter_var( $data['rentInfo']['input_rent_email'], FILTER_VALIDATE_EMAIL)) {
					$this->sendMailMsg( $rentInfo) ;
				}

				if ( !empty( $data['rentInfo']['input_mobile']) && $this->isPhone( $data['rentInfo']['input_mobile'])) {
					if ( !DEBUG_MOD) {
						$this->sendSMS( $rentInfo) ;
					}
				}

				$data['success'] = "租車資訊已受理 感謝您的預約！<br/>訂單編號為<span color=\"#ffd633\"> {$rentInfo['agreement_no']}</span>" ;
				$objLog->write( $data['success']) ;
			} else {
				// 訂單失敗 不寄mail 不發簡訊
				// 刪除 tb_scheduling 資料
				// $this->model_common_order->delTb_scheduling( $this->session->data['agreement_no']) ;
				if ( !$ecRtn) {
					$data['error_warning'] = "本交易尚未接收刷卡回傳，請等候簡訊通知 有任何疑問請撥打 082-371888轉1" ;
				} else {
					$this->model_common_order->delTb_scheduling( $this->session->data['agreement_no']) ;
					$data['error_warning'] = "{$ecRtn['RtnCode']} | {$ecRtn['RtnMsg']} 本交易未成功，請重新建立訂單 有任何疑問請撥打 082-371888轉1" ;
				}
				$objLog->write( $data['error_warning']) ;
			}


			if ( !DEBUG_MOD) {
				unset( $this->session->data['rentInfo']) ;
				unset( $this->session->data['order_sn']) ;
				unset( $this->session->data['agreement_no']) ;
				unset( $this->session->data['success']) ;
			}
			// exit() ;
			// 完成訂購作業
			$objLog->write( "顯示前台頁面") ;
			$this->response->setOutput($this->load->view('order/step4', $data)) ;
		} else {
			// 導回首頁
			$objLog->write( "按了重新整理") ;
			$this->response->redirect($this->config->get('site_base'));
		}
		// dump( $rentInfo) ;
	}

	protected function getForm() {
		// dump( $this->request->post) ;
		$data['stepFlag'] = array( "ok", "ok", "ok", "no") ;

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'] ;
		} else {
			$data['error_warning'] = '' ;
		}

		$this->load->model('common/option') ;
		$stationArr = $this->model_common_option->getCarStation( array( $this->request->get['s1'], $this->request->get['s2'])) ;
		$airPortArr = $this->model_common_option->getOptionArrUseParent( array(134, 131)) ; // 航/船班資訊
		$data['airListArr'] = $airPortArr ;
		// dump( $airPortArr) ;


		$data['rentInfo']                = unserialize( $this->session->data['rentInfo']) ;
		$data['rentInfo']['bc']          = $this->setDataInfo( 'get', '', 'bc') ;
		$data['rentInfo']['bc1']         = $this->setDataInfo( 'get', '', 'bc1') ;
		$data['rentInfo']['bc2']         = $this->setDataInfo( 'get', '', 'bc2') ;
		$data['rentInfo']['cc']          = $this->setDataInfo( 'get', '', 'cc') ;
		$data['rentInfo']['gps']         = $this->setDataInfo( 'get', '', 'gp') ;
		$data['rentInfo']['mm']          = $this->setDataInfo( 'get', '', 'mm') ;
		$data['rentInfo']['qid']          = $this->setDataInfo( 'get', '', 'qid') ;
		$data['rentInfo']['station']     = $stationArr ;
		$this->session->data['rentInfo'] = serialize( $data['rentInfo']) ;
		// dump( $data['rentInfo']) ;

		$data['token'] = $this->session->data['tokenf'] ;

		$this->load->model('common/zip') ;
		$data['retZip'] = $this->model_common_zip->getZipCode() ;

		// 取得from post 承租人資料
		$data['identity']     = $this->setDataInfo( 'post', 'input', 'identity') ;
		$data['rent_name']     = $this->setDataInfo( 'post', 'input', 'rent_name') ;
		$data['rent_user_id']  = $this->setDataInfo( 'post', 'input', 'rent_user_id') ;
		$data['bornday']       = $this->setDataInfo( 'post', 'input', 'bornday') ;
		$data['mobile']        = $this->setDataInfo( 'post', 'input', 'mobile') ;
		$data['rent_email']    = $this->setDataInfo( 'post', 'input', 'rent_email') ;

		// 取得from post 取車人資料
		$data['get_identity']     = $this->setDataInfo( 'post', 'input', 'get_identity') ;
		$data['syncrent']      = $this->setDataInfo( 'post', '', 'syncrent') ;
		$data['get_name']      = $this->setDataInfo( 'post', 'input', 'get_name') ;
		$data['get_user_id']   = $this->setDataInfo( 'post', 'input', 'get_user_id') ;
		$data['get_bornday']   = $this->setDataInfo( 'post', 'input', 'get_bornday') ;
		$data['get_mobile']    = $this->setDataInfo( 'post', 'input', 'get_mobile') ;

		// 其他通訊資料
		$data['rent_zipcode1'] = $this->setDataInfo( 'post', 'input', 'rent_zipcode1') ;
		$data['rent_zipcode2'] = $this->setDataInfo( 'post', 'input', 'rent_zipcode2') ;
		if ( $data['rent_zipcode2'] != '') {
			$data['retZip2'] = $this->model_common_zip->getZipCode( $data['rent_zipcode1']) ;
		}
		$data['rent_address']	= $this->setDataInfo( 'post', 'input', 'rent_address') ;

		$data['company']		= $this->setDataInfo( 'post', 'input', 'company') ;

		if ( isset( $this->request->post['input_company_no']) && $this->request->post['input_company_no'] != "如需開立統編請填寫") {
			$data['company_no'] = $this->request->post['input_company_no'] ;
		} else {
			$data['company_no'] = "如需開立統編請填寫" ;
		}
		$data['urgent_man']		= $this->setDataInfo( 'post', 'input', 'urgent_man') ;
		$data['urgent_mobile']	= $this->setDataInfo( 'post', 'input', 'urgent_mobile') ;

		// 航班
		$data['air_port']		= $this->setDataInfo( 'post', '', 'air_port') ;
		$data['air_date']		= $this->setDataInfo( 'post', '', 'air_date') ;
		$data['air_hour']		= $this->setDataInfo( 'post', '', 'air_hour') ;
		$data['air_minute']		= $this->setDataInfo( 'post', '', 'air_minute') ;


		$data['actStr'] = $this->request->server['QUERY_STRING'] ;

		$data['footer'] = $this->load->controller('common/footer') ;
		$data['header'] = $this->load->controller('common/header') ;
		// dump(
		// 	array( $this->request->post['air_port'],
		// 			$this->request->post['air_date'],
		// 			$this->request->post['air_hour'],
		// 			$this->request->post['air_minute'],
		// 	)
		// ) ;

		$this->response->setOutput($this->load->view('order/step3', $data)) ;
	}


	protected function validateForm() {
		if ( $this->request->post['ckboxConfirm'] != 'yes') {
			$this->error['ckboxConfirm'] = "<span class=\"glyphicon glyphicon-info-sign\"></span>尚未閱讀租賃契約" ;
		}
		if (utf8_strlen($this->request->post['input_rent_name']) == 0) {
			$this->error['input_rent_name'] = "<span class=\"glyphicon glyphicon-info-sign\"></span>承租人姓名不可為空白" ;
		}

		if (utf8_strlen($this->request->post['input_rent_user_id']) == 0) {
			$this->error['input_rent_user_id'] = "<span class=\"glyphicon glyphicon-info-sign\"></span>身分證字號／護照號碼不可為空白" ;
		}else{
			if($this->request->post['input_identity'] == 1){
				//國內身分證
				$r_check_id = $this->check_id_card($this->request->post['input_rent_user_id']);
				if($r_check_id != 'Y'){
					$this->error['input_rent_user_id'] = "<span class=\"glyphicon glyphicon-info-sign\"></span>身分證字號／護照號碼 ".$r_check_id ;
				}
			}/*else if($this->request->post['input_identity'] == 2){
				//香港身份證
				$r_check_id = $this->checkHKIdCard($this->request->post['input_rent_user_id']);
				if($r_check_id['status']=='N'){
					$this->error['input_rent_user_id'] = "<span class=\"glyphicon glyphicon-info-sign\"></span>身分證字號／護照號碼 有誤";
				}

				echo $r_check_id['status'];

			}*/
		}

		// 檢查 是否滿20歲
		$today = strtotime( date( 'Y-m-d')) ;
		$rentBornDay = strtotime( $this->request->post['input_bornday']) ;

		if ( ( $today - $rentBornDay) < 631152000) {
			$this->error['input_bornday'] = "<span class=\"glyphicon glyphicon-info-sign\"></span>未滿20歲 不能租車" ;
		}

		if (utf8_strlen($this->request->post['input_mobile']) == 0) {
			$this->error['input_mobile'] = "<span class=\"glyphicon glyphicon-info-sign\"></span>手機不可為空白" ;
		}

		if (utf8_strlen($this->request->post['input_rent_email']) == 0) {
			$this->error['input_rent_email'] = "<span class=\"glyphicon glyphicon-info-sign\"></span>email不可為空白" ;
		}

		if (!filter_var( $this->request->post['input_rent_email'], FILTER_VALIDATE_EMAIL)) {
		    $this->error['input_rent_email'] = "<span class=\"glyphicon glyphicon-info-sign\"></span>email格式錯誤" ;
		}
		// if (utf8_strlen($this->request->post['input_rent_address']) == 0) {
		// 	$this->error['input_rent_address'] = "<span class=\"glyphicon glyphicon-info-sign\"></span>聯絡地址不可為空白" ;
		// }

		// 取車人 =========================================================================================================
		// if (utf8_strlen($this->request->post['input_get_name']) == 0) {
		// 	$this->error['input_get_name'] = "<span class=\"glyphicon glyphicon-info-sign\"></span>取車人 姓名不可為空白" ;
		// }
		// if (utf8_strlen($this->request->post['input_get_user_id']) == 0) {
		// 	$this->error['input_get_user_id'] = "<span class=\"glyphicon glyphicon-info-sign\"></span>取車人 身分證字號／護照號碼不可為空白" ;
		// }
		// // 檢查 是否滿20歲
		// $rentBornDay = strtotime( $this->request->post['input_get_bornday']) ;
		// if ( ( $today - $rentBornDay) < 631152000) {
		// 	$this->error['input_get_bornday'] = "<span class=\"glyphicon glyphicon-info-sign\"></span>取車人 未滿20歲 不能取車" ;
		// }
		// if (utf8_strlen($this->request->post['input_get_mobile']) == 0) {
		// 	$this->error['input_get_mobile'] = "<span class=\"glyphicon glyphicon-info-sign\"></span>取車人 手機不可為空白" ;
		// }

		if (utf8_strlen($this->request->post['input_rent_address']) == 0) {
			$this->error['input_rent_address'] = "<span class=\"glyphicon glyphicon-info-sign\"></span>通訊地址 不可為空白" ;
		}

		if (utf8_strlen($this->request->post['input_urgent_man']) == 0) {
			$this->error['input_urgent_man'] = "<span class=\"glyphicon glyphicon-info-sign\"></span>緊急連絡人 不可為空白" ;
		}

		if (utf8_strlen($this->request->post['input_urgent_mobile']) == 0) {
			$this->error['input_urgent_mobile'] = "<span class=\"glyphicon glyphicon-info-sign\"></span>緊急連絡人電話 不可為空白" ;
		}

		// todo 這裡會有問題
		// 檢查訂車數量是否足夠 =============================================================================
		$rentInfo = unserialize( $this->session->data['rentInfo']) ;
		// dump( $rentInfo) ;
		$carInfoFilter = array(
				"car_seed"		=> $rentInfo['carIdx'],
				"start_date"	=> $rentInfo['startDateYMD'],
				"end_date"		=> $rentInfo['endDateYMD'],
				"car_limit"		=> $rentInfo['b_car_num'],
			) ;
		$carCntInfo = $this->model_common_cars->getCarInfo( $carInfoFilter) ;
		if ( count($carCntInfo) == 0 ) {
			$this->error['input_get_mobile'] = "<span class=\"glyphicon glyphicon-info-sign\"></span>您所選擇的車種 目前已訂車額滿 請重新選擇" ;
		}

		if ( $this->error) {
			$msgStr = "" ;
			foreach ($this->error as $key => $tmpMsg) {
				$msgStr .= "{$tmpMsg}<br>" ;
			}
			$this->error['warning'] = "<span class=\"glyphicon glyphicon-warning-sign\"></span>必填資料不完全" . "<br>{$msgStr}";
		}

		return !$this->error;
	}

	protected function newAgreementNo() {
		$todayNo = date( 'Ymd') ;
		return $this->model_common_order->newAgreementNo( $todayNo) ;
	}

	protected function preparation() {
		$data['footer'] = $this->load->controller('common/footer') ;
		$data['header'] = $this->load->controller('common/header') ;

		return $data ;
	}

	// zip code for Ajax
	public function getSubZip() {
		// dump( $_POST['zipMain']) ;

		// exit() ;
		$json = array();

		if (isset($this->request->post['zipMain'])) {
			$this->load->model('common/zip');

			$results = $this->model_common_zip->getZipCode( $this->request->post['zipMain']) ;

			foreach ($results as $result) {
				$json[] = array(
					'zip_code'	=> $result['zip_code'],
					// 'zip_name'	=> strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
					'zip_name'	=> strip_tags(html_entity_decode($result['zip_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		// $sort_order = array();

		// foreach ($json as $key => $value) {
		// 	$sort_order[$key] = $value['name'];
		// }

		// array_multisort($sort_order, SORT_ASC, $json) ;
		// dump( $json) ;

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	/**
	 * [buileSendMailMsg description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2018-03-20
	 */
	protected function buileSendMailMsgContent( $data = array()) {
		// 配件
		$tmpOther = "" ;
		if($data['bc'] > 0) {
			$tmpOther .= "嬰兒安全座椅 x {$data['bc']}" ;
		}
		if($data['bc1'] > 0) {
			if($tmpOther != "")
				$tmpOther .= "\r\n　　&nbsp;&nbsp;&nbsp;";
			$tmpOther .= "兒童安全座椅 x {$data['bc1']}" ;
		}
		if($data['bc2'] > 0) {
			if($tmpOther != "")
				$tmpOther .= "\r\n　　&nbsp;&nbsp;&nbsp;";
			$tmpOther .= "兒童增高墊 x {$data['bc2']}" ;
		}
		if($data['cc'] > 0) {
			if($tmpOther != "")
				$tmpOther .= "\r\n　　&nbsp;&nbsp;&nbsp;";
			$tmpOther .= "嬰兒推車 x {$data['cc']}" ;
		}
		if($data['gps'] > 0) {
			if($tmpOther != "")
				$tmpOther .= "\r\n　　&nbsp;&nbsp;&nbsp;";
			$tmpOther .= "GPS x {$data['gps']}" ;
		}
		if($data['mm'] > 0) {
			if($tmpOther != "")
				$tmpOther .= "\r\n　　&nbsp;&nbsp;&nbsp;";
			$tmpOther .= "機車導航架 x {$data['mm']}" ;
		}
		if($tmpOther == "")
			$tmpOther .= "無";

		// 租車費用文字說明調整 add by Angus 2018.07.05 ---------------------------------------------------------------
		$rentPriceStr = "\n" ;
		if ( $data['DiscountPercentage'] < 1) {
			$rentPriceStr .= "         早鳥優惠 ".($data['DiscountPercentage'] * 10)." 折\n" ;
		}
		$rentPriceStr .= "         車價（共{$data['b_car_num']}台，每台借車時間為 {$data['rentDay']}天又{$data['rentHour']}小時），共 {$data['car_price2Str']}元\n" ;
		$rentPriceStr .= "         配件合計 {$data['car_price3Str']}元\n" ;
		$rentPriceStr .= "         總價（含稅）{$data['totalStr']}元\n" ;

		$pattern = "
＃＃＃＃＃＃＃＃＃＃＃
＃　車輛行事曆資訊　＃
＃＃＃＃＃＃＃＃＃＃＃
車種 : %s
車型 : %s
訂車數量 : %s
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
【租車資訊】
訂單編號 : %s
訂單類別 : 網路訂單
民宿(業者) :
取車地點 : %s
還車地點 : %s
取車時間 : %s
還車時間 : %s
租車費用 : %s
備註 :
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
【客戶資料】
承租人姓名 : %s
身分證字號／護照號碼 : %s
出生年月日 : %s
手機 : %s
取車人姓名 : %s
身分證字號／護照號碼 : %s
出生年月日 : %s
手機 : %s
航班時間 : %s
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
通訊地址 : %s
公司名稱 : %s
公司統編 : %s
緊急連絡人 : %s
緊急連絡人電話 : %s
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
配件 : %s
" ;
		return sprintf( $pattern,
						$data['car_model_name'],	// 車種
						$data['carSeed']['car_seed'],	// 車型
						$data['b_car_num'],				// 訂車數量
						$data['agreement_no'],			// 訂單編號
						$data['station'][$data['s1']]['opt_desc'],	// 取車地點
						$data['station'][$data['s2']]['opt_desc'],	// 還車地點
						$data['startDateYMD'],			// 取車時間
						$data['endDateYMD'],			// 還車時間
						$rentPriceStr,					// 租車費用
						$data['input_rent_name'],		// 承租人姓名
						$data['input_rent_user_id'],	// 身分證字號／護照號碼
						$data['input_bornday'],			// 出生年月日
						$data['input_mobile'],			// 手機
						$data['input_get_name'],		// 取車人姓名
						$data['input_get_user_id'],		// 身分證字號／護照號碼
						$data['input_get_bornday'],		// 出生年月日
						$data['input_get_mobile'],		// 手機
						$data['flight_point'] ." ". $data['flight_sDate'],
						$data['input_rent_address'],	// 通訊地址
						$data['input_company'],			// 公司名稱
						$data['input_company_no'],		// 公司統編
						$data['input_urgent_man'],		// 緊急連絡人
						$data['input_urgent_mobile'],	// 緊急連絡人電話
						$tmpOther
		) ;
	}

	/**
	 * [sendMailMsg description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2018-03-20
	 */
	protected function sendMailMsg( $data = array()) {
		// dump( $data) ;
		$mail = new Mail();

		$mail->protocol      = $this->config->get('config_mail_protocol') ;
		$mail->parameter     = $this->config->get('config_mail_parameter') ;
		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname') ;
		$mail->smtp_username = $this->config->get('config_mail_smtp_username') ;
		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8') ;
		$mail->smtp_port     = $this->config->get('config_mail_smtp_port') ;
		$mail->smtp_timeout  = $this->config->get('config_mail_smtp_timeout') ;

		// $subject = "金豐租車訂單成功通知信" ;
		$subject = "訂單通知 訂單編號:{$data['agreement_no']}" ;
		$message = $this->buileSendMailMsgContent( $data) ;

		$mail->setTo( $data['input_rent_email']) ;
		$mail->setFrom($this->config->get('config_email')) ;
		$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) ;
		$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8')) ;
		$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8')) ;
		$mail->send();

		$sendMailArr = array(
			"kmfun005@gmail.com",
			"kmfun999@gmail.com"
		) ;
		if ( WEBSITE_TYPE != 'dev') {
			$mail->setTo( $sendMailArr) ;
			$mail->send();
		}

		$sendMailArr = array(
			"kh0813@gmail.com",
			"jaff.tw@gmail.com"
		) ;
		$mail->setTo( $sendMailArr) ;
		$mail->send();
	}

	/**
	 * [buileSendSmsMsgContent description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2018-04-12
	 */
	protected function buileSendSmsMsgContent( $data = array()) {
		$pattern = "您好！您的租車訂單編號%s付款金額：%s元已收到，車輛已預留，將於出發前一天會再與您聯繫。提醒您，如取消交易須酌收營業損失費用！如有任何疑問請加入Line：@kmfun諮詢或撥082-371888，金豐租車感謝您!(系統發送，請勿回傳)" ;

		return sprintf( $pattern, $data['agreement_no'], number_format( $data['total'])) ;
	}

	/**
	 * [sendSMS description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2018-04-12
	 */
	protected function sendSMS( $data = array()) {
		$this->load->model('common/order') ;
    // [batchID] => 7aadd35d-c4cf-4c67-9c34-58d3205f5acb
    // [credit] => 3973.00

		$sms        = new SMSHttp() ;
		$userID     = "0965109222" ;	//發送帳號
		$password   = "0965109222" ;	//發送密碼
		$subject    = "金豐租車網路訂車" ;	//簡訊主旨，主旨不會隨著簡訊內容發送出去。用以註記本次發送之用途。可傳入空字串。
		$content    = $this->buileSendSmsMsgContent( $data) ;	//簡訊內容
		$sendMobile = $data['input_mobile'] ;
		// $sendMobile = "0935659310" ;
		$sendTime   = "";

		if ( WEBSITE_TYPE != 'dev') {
			if( $sms->getCredit( "0965109222", "0965109222") && $sms->credit > 0) {
				if ( $sms->sendSMS( $userID, $password, $subject, $content, $sendMobile, $sendTime)) {
					$this->model_common_order->updateSmsStatus(
						array( "agreement_no" => $data['agreement_no'], "batchID" => $sms->batchID, "updateUser"=> "Step4SendSMS")
					) ;
					// dump( $data['agreement_no']) ;
				}
			}
		}
	}

	/**
	 * [isPhone 驗證台灣手機號碼]
	 * @param   [type]     $str [description]
	 * @return  boolean         [description]
	 * @Another Angus
	 * @date    2018-04-12
	 */
	protected function isPhone($str) {
	    if (preg_match("/^09[0-9]{2}-[0-9]{3}-[0-9]{3}$/", $str)) {
	        return true;    // 09xx-xxx-xxx
	    } else if(preg_match("/^09[0-9]{2}-[0-9]{6}$/", $str)) {
	        return true;    // 09xx-xxxxxx
	    } else if(preg_match("/^09[0-9]{8}$/", $str)) {
	        return true;    // 09xxxxxxxx
	    } else {
	        return false;
	    }
	}

	//驗證身份證字號
	protected function check_id_card($cardid){
		$err ='';
		//先將字母數字存成陣列
		$alphabet =['A'=>'10','B'=>'11','C'=>'12','D'=>'13','E'=>'14','F'=>'15','G'=>'16','H'=>'17','I'=>'34',
					'J'=>'18','K'=>'19','L'=>'20','M'=>'21','N'=>'22','O'=>'35','P'=>'23','Q'=>'24','R'=>'25',
					'S'=>'26','T'=>'27','U'=>'28','V'=>'29','W'=>'32','X'=>'30','Y'=>'31','Z'=>'33'];
		//檢查字元長度
		if(strlen($cardid) !=10){$err = '1';}//長度不對

		//驗證英文字母正確性
		$alpha = substr($cardid,0,1);//英文字母
		$alpha = strtoupper($alpha);//若輸入英文字母為小寫則轉大寫
		if(!preg_match("/[A-Za-z]/",$alpha)){
			$err = '2';}else{
				//計算字母總和
				$nx = $alphabet[$alpha];
				$ns = $nx[0]+$nx[1]*9;//十位數+個位數x9
			}

		//驗證男女性別
		$gender = substr($cardid,1,1);//取性別位置
		if($gender !='1' && $gender !='2'){$err = '3';}//驗證性別

		//N2x8+N3x7+N4x6+N5x5+N6x4+N7x3+N8x2+N9+N10
		if($err ==''){
			$i = 8;
			$j = 1;
			$ms =0;
			//先算 N2x8 + N3x7 + N4x6 + N5x5 + N6x4 + N7x3 + N8x2
			while($i >= 2){
				$mx = substr($cardid,$j,1);//由第j筆每次取一個數字
				$my = $mx * $i;//N*$i
				$ms = $ms + $my;//ms為加總
				$j+=1;
				$i--;
			}
			//最後再加上 N9 及 N10
			$ms = $ms + substr($cardid,8,1) + substr($cardid,9,1);
			//最後驗證除10
			$total = $ns + $ms;//上方的英文數字總和 + N2~N10總和
			if( ($total%10) !=0){$err = '4';}
		}
		//錯誤訊息返回
		switch($err){
			case '1':$msg = '字元數錯誤';break;
			case '2':$msg = '英文字母錯誤';break;
			case '3':$msg = '性別錯誤';break;
			case '4':$msg = '驗證失敗';break;
			default:$msg = 'Y';break;
		}

		return $msg;
	}

	//香港身份证驗證
    public function checkHKIdCard($number){
		//香港身份证首字母对应的数字
		$letterNum = array(
					'A'=>1,
					'B'=>2,
					'C'=>3,
					'D'=>4,
					'E'=>5,
					'F'=>6,
					'G'=>7,
					'H'=>8,
					'I'=>9,
					'J'=>10,
					'K'=>11,
					'L'=>12,
					'M'=>13,
					'N'=>14,
					'O'=>15,
					'P'=>16,
					'Q'=>17,
					'R'=>18,
					'S'=>19,
					'T'=>20,
					'U'=>21,
					'V'=>22,
					'W'=>23,
					'X'=>24,
					'Y'=>25,
					'Z'=>26,
					);


    	//将中文括号全部替换成英文括号
		if (strpos($number,"（") || strpos($number,"）")){
			$number = str_replace('（', '(', $number);
			$number = str_replace('）', ')', $number);
		}
		if(strlen($number)!=10){
		    return array('status'=>'N');
		}
    	$one = substr($number,0,1);//取首字母
    	$two = substr($number,1,1);//取第二位
    	$three = substr($number,2,1);//取第三位
    	$four = substr($number,3,1);//取第四位
    	$five = substr($number,4,1);//取第五位
    	$six = substr($number,5,1);//取第六位
    	$seven = substr($number,6,1);//取第七位
    	$checkCode = substr($number,-2,1);//取括号内的校验码
    	$sixNum = substr($number, 1,6);//取中间六位数字
    	if (preg_match('/^[A-Z]+$/', $one)){//第一位要是大写字母
    		if ( is_numeric($sixNum)){//中间六位要是数字
    			$sum = $this->letterNum[$one]*8+$two*7+$three*6+$four*5+$five*4+$six*3+$seven*2;
    			$residue = $sum%11;
    			if ($residue==1){
    				$checkIdCode='A';
    			}
    			elseif($residue==0){
    				$checkIdCode=0;
    			}
    			else{
    				$checkIdCode=11-$residue;
    			}
    			if ($checkCode==$checkIdCode){
    				return array('status'=>'Y','sfz_id'=>$number);
    			}else{
    				return array('status'=>'N');
    			}
    		}
    		else{
    			return array('status'=>'N');
    		}
    	}
    	else{
    		return array('status'=>'N');
    	}
    }

    /**
     * [checkFittingRelay 配件開關]
     * @param   array      $data [description]
     * 兒童安全座椅〔一歲以下〕
	 * 兒童安全座椅〔一歲以上〕
	 * 兒童安全座椅〔增高墊〕
	 * 嬰兒推車
     * @return  array      $retArr [description]
     * @Another Angus
     * @date    2019-11-28
     */
    protected function checkFittingRelay( $rentInfo = array(), $data = array()) {
    	$retArr = array() ;
    	// dump( array($rentInfo['startDateYMD'], $rentInfo['endDateYMD'])) ;
    	foreach ($data as $i => $rows) {
    		// dump( $rows) ;
    		foreach ($rows as $j => $row) {
				if ( isset( $row[0]) && ( !isset($retArr[$i]) || $retArr[$i] == true) ) {
					if ( strtotime( $rentInfo['startDateYMD']) < $row['0'] && strtotime( $rentInfo['endDateYMD']) < $row['0']) {
						$retArr[$i] = true ; // 可租
					} else if ( strtotime( $rentInfo['startDateYMD']) > $row['1'] && strtotime( $rentInfo['endDateYMD']) > $row['1']) {
						$retArr[$i] = true ; // 可租
					} else {
						$retArr[$i] = false ; // 不可租
					}
				}
    		}
    	}
    	return $retArr ;
    }


}
