<?php
/**
 * http://kmfun.tw/?route=order/ordersearch/stock
 * http://localhost/kmfun_Nopaper/?route=order/ordersearch/stock
 */
class ControllerOrderOrdersearch extends Controller {
	private $error = array() ;
	private $staff = array() ;

	public function index() {
		$data = array() ;

		$this->session->data['tokenf'] = $this->session->session_id ;
		if ( isset( $this->session->data['RtnMsg'])) {
			$RtnMsg = $this->session->data['RtnMsg'] ;
			unset( $this->session->data['RtnMsg']) ;
		} else {
			$RtnMsg = "" ;
		}
		$data['RtnMsg'] = $RtnMsg ;

		$data['footer'] = $this->load->controller('common/footer') ;
		$data['header'] = $this->load->controller('common/header') ;

		$this->response->setOutput($this->load->view('order/ordersearch', $data)) ;
	}

	/**
	 * [result description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-09-07
	 */
	public function result() {
		$data = array() ;
		$this->session->data['tokenf'] = $this->session->session_id ;
		$data['token'] = $this->session->data['tokenf'] ;

		$this->load->model('common/order') ;
		$this->load->model('common/cars') ;
		$data['orderInfo'] = $this->model_common_order->getOrderForCheckOrder( $this->request->get) ;
		// dump( $data['orderInfo']) ;

		if ( !$data['orderInfo']) {
			$this->session->data['RtnMsg'] = "查無訂單，請電洽 082-371888 與客服確認訂單！" ;
			$this->response->redirect($this->url->link('order/ordersearch', '', true));
			exit() ;
		}

		if ( !empty( $data['orderInfo']) ) {
			$CarSeedSN = $this->model_common_cars->getCarSeedSN($data['orderInfo']['car_sn']) ;
			// dump($CarSeedSN) ;
			$data['carSeed'] = $this->model_common_cars->getCarSeed($CarSeedSN) ;
			// dump($carSeed) ;
		}

		$this->load->model('tool/image');
		if (isset($data['carSeed']["image"]) && is_file(DIR_IMAGE . $data['carSeed']["image"])) {
			$data['carSeed']['thumb'] = $this->model_tool_image->resize($data['carSeed']["image"], 230, 134);
		} else {
			$data['carSeed']['thumb'] = $this->model_tool_image->resize('no_image.png', 230, 134);
		}

		// 早鳥折數
		// dump( array( $data['orderInfo']['rent_date_out'], $data['orderInfo']['create_date'])) ;
		$data['orderInfo']['discountPercentage'] = $this->model_common_order->getDiscountPercentage( $data['orderInfo']['rent_date_out'], $data['orderInfo']['create_date']) ;
		// dump( $data['orderInfo']['discountPercentage']) ;

		// 使用天數
		$second1 = strtotime( $data['orderInfo']['rent_date_in_plan']) - strtotime( $data['orderInfo']['rent_date_out']) ;
		$data['orderInfo']['rentDay'] 	= floor( $second1 / 86400) ;	// 24hr * 60分 * 60秒
		$data['orderInfo']['rentHour'] 	= ( $second1 % 86400) / 3600 ;	//

		// 產生費用
		if ( $data['orderInfo']['rentDay'] == 0 ) {
			$data['orderInfo']['car_price2'] = $data['carSeed']['car_price'] * $data['orderInfo']['discountPercentage'] * $data['orderInfo']['total_car'] ;

		} else if ( $data['orderInfo']['rentDay']  >= 1 && $data['orderInfo']['rentHour'] <= 6) {
			$data['orderInfo']['car_price2'] = ($data['carSeed']['car_price'] * $data['orderInfo']['rentDay'] * $data['orderInfo']['discountPercentage'] + $data['carSeed']['car_price'] * 0.1 * ceil( $data['orderInfo']['rentHour']) ) * $data['orderInfo']['total_car'] ;
		} else { //if ( $rentInfo['rentDay']  > 1 && $rentInfo['rentHour'] > 6) {
			$data['orderInfo']['car_price2'] = $data['carSeed']['car_price'] * ( $data['orderInfo']['rentDay'] + 1) * $data['orderInfo']['discountPercentage'] * $data['orderInfo']['total_car'] ;
		}
		$data['orderInfo']['car_price2Str'] = number_format( $data['orderInfo']['car_price2']) ;

		// 配件使用天數
		$useDay = round( ( strtotime( $data['orderInfo']['rent_date_in_plan']) - strtotime( $data['orderInfo']['rent_date_out']))/3600/24) + 1 ;
		// bc => 兒童安全座椅〔一歲以下〕, bc1 => 兒童安全座椅〔一歲以上〕, bc2 => 兒童安全座椅〔增高墊〕
		if($data['orderInfo']['baby_chair'] > 0 || $data['orderInfo']['baby_chair1'] > 0 || $data['orderInfo']['baby_chair2'] > 0 ||
			$data['orderInfo']['children_chair'] > 0 || $data['orderInfo']['gps'] > 0 || $data['orderInfo']['mobile_moto'] > 0) {
			$data['orderInfo']['car_price3'] = ($data['orderInfo']['baby_chair'] * 100 * $useDay) +
									($data['orderInfo']['baby_chair1'] * 100 * $useDay) +
									($data['orderInfo']['baby_chair2'] * 100 * $useDay) +
									($data['orderInfo']['children_chair'] * 100 * $useDay) +
									($data['orderInfo']['gps'] * 0 * $useDay) +
									($data['orderInfo']['mobile_moto'] * 50 * $useDay) ;
			$data['orderInfo']['car_price3Str'] = number_format( $data['orderInfo']['car_price3']) ;

		} else {
			$data['orderInfo']['car_price3']    = 0 ;
			$data['orderInfo']['car_price3Str'] = 0 ;
		}

		// 總價（含稅）
		$data['orderInfo']['totalStr'] = number_format( $data['orderInfo']['car_price2'] + $data['orderInfo']['car_price3']) ;

		// 訂單結帳狀態
		$data['orderInfo']['RtnMsg'] = "此筆訂單尚未完成信用卡授權，請電洽 082-371888 與客服確認訂單！";
		if($data['orderInfo']['checkout'] != "" && $data['orderInfo']['order_status'] != 'x') {
			$EcRet = $this->model_common_order->checkEcReturnCode($data['orderInfo']['order_id']) ;
			if($EcRet['RtnCode'] == 1)
				$data['orderInfo']['RtnMsg'] = $EcRet['RtnMsg'];
		}

		//dump($data['orderInfo']);

		$data['header'] = $this->load->controller('common/header') ;
		$data['footer'] = $this->load->controller('common/footer') ;

		$this->response->setOutput($this->load->view('order/orderresult', $data)) ;
	}

	/**
	 * [stock description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-01-04
	 * http://localhost/kmfun_Nopaper/?route=order/ordersearch/stock/
	 */
	public function stock() {
		// dump( $this->session->data) ;
		// 檢查是否登入 add by Angus 2019.01.04
		$logger = new Log('Instant_Inquiry.log') ;
		$data['cssTime'] = time() ;

		$remoteAddr = $this->request->server['REMOTE_ADDR'] ;
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateLoginForm()) {

			$this->session->data['staff']         = $this->staff ;
			$this->session->data['token']         = $this->session->session_id ;
			$this->session->data['user_id']       = $this->staff['user_id'] ;
			$this->session->data['user_name']     = $this->staff['username'] ;
			$this->session->data['user_group_id'] = $this->staff['user_group_id'] ;

			$logger->write( $remoteAddr . "|登入完成 金豐車輛即時查詢|order/ordersearch/stock|");
			$this->response->redirect($this->url->link('order/ordersearch/stock', 'token='.$this->session->data['token'], true)) ;
		}

		if ( !isset( $this->session->data['staff'])) {
			$data['error_warning'] = ( !empty($this->error)) ? join( "<br>", $this->error) : "" ;
			$this->response->setOutput($this->load->view('information/staffLogin', $data)) ;
			$account  = isset( $this->request->post['account']) ? $this->request->post['account'] : "" ;
			$password = isset( $this->request->post['password']) ? $this->request->post['password'] : "" ;
			$logger->write( $remoteAddr . "|登入失敗 金豐車輛即時查詢|order/ordersearch/stock|acc:{$account}|pw:{$password}");
		} else {
			$this->load->model('common/option') ;
			$logger->write( $remoteAddr . "|使用 金豐車輛即時查詢|order/ordersearch/stock|acc:{$this->session->data['staff']['username']}");
			$data['carModelSel'] = $this->model_common_option->getOptionItemsArr( 1) ;
			$data['timeArr']     = $this->model_common_option->getRentTime() ;

			$this->response->setOutput($this->load->view('information/stock', $data)) ;
		}
	}


	public function searchCar() {
//     [sDate] => 2018-08-24
//     [eDate] => 2018-08-26
//     [sTime] => 07:30
//     [eTime] => 07:30
//     [carModel] => 3

// <p>汽車</p>
// TOYOTA SEINTA<span>2</span><br/>
// 三菱 得利卡<span>5</span><br/>
// Nissan TIIDA<span>1</span><br/>
// HONDA FIT<span>1</span><br/>
// Toyota Vios 2017<span>3</span><br/>
// Toyota VIOS<span>1</span>
// <p>進口車</p>
// Volkswagen 福斯 T6<span>1</span><br/>
// Volkswagen 福斯 T5<span>1</span>
// <p>機車</p>
// New Mii 110<span>9</span><br/>
// 東庚 電動車<span>1</span><br/>
// GT125<span>42</span>
// <p>電動自行車</p>
// 中華Bobe EM-25<span>10</span>
// <p>自行車</p>
// 折疊式自行車<span>5</span><br/>
// 折疊式協力車<span>1</span>
		$startDateYMD = $this->request->post['sDate'] . " " . $this->request->post['sTime'] ;
		$endDateYMD   = $this->request->post['eDate'] . " " . $this->request->post['eTime'] ;

		$logger = new Log('Instant_Inquiry.log') ;
		$logger->write( $this->request->server['REMOTE_ADDR'] . "|查詢可用車輛|acc:{$this->session->data['staff']['username']}|S:{$startDateYMD}|E:{$endDateYMD}" );

		$this->load->model('common/cars') ;
		$carCntArr = $this->model_common_cars->getCarCntByModel(
			array(	$startDateYMD,
					$endDateYMD,
					$this->request->post['carModel'])
		) ;
		// dump( $carCntArr) ;
		$carModel = $this->model_common_cars->getCarModel() ;
		// dump( $carModel) ;
		$mainArr = array() ;
		$subArr = array() ;
		$carNameArr = array() ;
		foreach ($carModel as $iCnt => $tmp) {
			$mainArr[$tmp['idx']] = $tmp['opt_name'] ;
			$subArr[$tmp['idx']][] = $tmp['car_id'] ;
			$carNameArr[$tmp['car_id']] = $tmp['car_name'] ;
		}

		$retHtml = "" ;
		$carCntStr = "" ;

		// dump( $mainArr) ;
		$logStr = "" ;
		foreach ($mainArr as $mIdx => $mTmp) {
			$carCntStr = "" ;
			$logStr    .= "{$mTmp}:" ;
			foreach ($subArr[$mIdx] as $sCnt => $carModelId) {
				if ( isset( $carCntArr[$carModelId])) {
					// $carCntStr .= "<a href=\"?route=order/staff\">{$carNameArr[$carModelId]}</a><span>$carCntArr[$carModelId]</span><br>" ;
					$carCntStr .= "<a href=\"#\" class=\"carSel\" modId=\"{$carModelId}\">{$carNameArr[$carModelId]}</a><span>$carCntArr[$carModelId]</span><br>" ;
					$logStr .= "{$carNameArr[$carModelId]}-{$carCntArr[$carModelId]}/" ;
				}
			}
			$logStr .= ";" ;
			if ( !empty($carCntStr)) {
				$retHtml .= "<p>{$mTmp}</p>{$carCntStr}" ;
			}
		}
		$logger->write( $this->request->server['REMOTE_ADDR'] . "|查詢可用車輛|acc:{$this->session->data['staff']['username']}|{$logStr}" );
$retHtml .= <<<HTML
<script type="text/javascript">
$('.carSel').click( function () {
	var modId = $(this).attr('modId') ;
	var sd = $('input[name=\'sDate\']').val() ;
	var ed = $('input[name=\'eDate\']').val() ;
	var st = $('select[name=\'sTime\']').val() ;
	var et = $('select[name=\'eTime\']').val() ;
	// location.replace("?route=order/staff&step2="+JSON.stringify({ sd, ed, st, et, modId })) ;
	$('input[name=\'step2\']').val( JSON.stringify({ sd, ed, st, et, modId })) ;
	$('#form1').submit() ;
});
</script>'
HTML
;
		echo $retHtml ;
	}

	/**
	 * [validateLoginForm description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-01-04
	 */
	private function validateLoginForm() {
		$this->load->model('common/staff') ;
		$selArr['acc'] = isset( $this->request->post['account']) ? $this->request->post['account'] : "" ;
		$selArr['pwd'] = isset( $this->request->post['password']) ? $this->request->post['password'] : "" ;

		$this->staff = $this->model_common_staff->checkStaff( $selArr) ;

		// dump( $this->staff) ;
		if ( empty( $this->staff )) {
			$this->error['warning'] = "帳號密碼錯誤 請連絡系統管理員" ;
		}

		return !$this->error ;
	}
}
