<?php
/**
 *	?route=order/ordersearch/stock/
 *	http://localhost/kmfun_Nopaper/?route=order/staff/
 *	http://localhost/kmfun/?route=order/ordersearch/stock/
 *	http://nopaper.kmfun.com.tw/?route=order/ordersearch/stock/
 *	https://www.qr-code-generator.com/
 */
class ControllerOrderStaff extends Controller {
	private $data       = array() ;
	private $logger     = "" ;
	private $remoteAddr = "" ;
	private $logMsg		= "" ;
	private $mailMsg 	= "" ;

	public function __construct( $registry) {
		parent::__construct($registry) ;
		$this->logger     = new Log('Instant_Inquiry.log') ;
		$this->remoteAddr = $this->request->server['REMOTE_ADDR'] ;
	}

	/**
	 * [index description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-01-10
	 */
	public function index() {
		$logHeader = "|輸入訂單內容|order/staff|" ;

		$this->load->model('common/cars') ;
		$this->load->model('common/order') ;
		$this->load->model('common/option') ;

		$this->data['cssTime'] = time() ;
		$this->data['error_warning'] = "" ;
		// dump( $this->session->data) ;
		// dump( $this->request->get) ;
		// dump( $this->request->post['step2']) ;
		if ( !isset( $this->session->data['staff'])) {
			$this->logger->write( $this->remoteAddr . "{$logHeader}未登入系統" );
			$this->response->redirect($this->url->link('order/ordersearch/stock', '', true)) ;
			exit() ;
		} else if ( $this->request->server['REQUEST_METHOD'] == 'GET') {
			$this->logger->write( $this->remoteAddr . "{$logHeader}未登入系統" );
			$this->response->redirect($this->url->link('order/ordersearch/stock', '', true)) ;
			exit() ;
		} else {
			$logHeader .= "acc:{$this->session->data['staff']['username']}|" ;
		}


		// [jsonStr] => {"sd":"2019-01-10","ed":"2019-01-10","st":"07:30","et":"07:30","modId":"18"}
		list( $t, $jsonStr) = explode("=", urldecode( file_get_contents('php://input'))) ;
		$infoArr = json_decode ( trim($jsonStr), true) ;
		$this->data['infoArr'] = $infoArr ;

		$startDateYMD = $infoArr['sd'] . " " . $infoArr['st'] ;
		$endDateYMD   = $infoArr['ed'] . " " . $infoArr['et'] ;

		$this->logMsg = "modId:{$infoArr['modId']};S:{$startDateYMD};|E:{$endDateYMD}" ;
		$this->logger->write( $this->remoteAddr . "{$logHeader}{$this->logMsg}" );

		// 取得可租數量
		$carCntArr = $this->model_common_cars->getCarCntByModel(
			array(	$startDateYMD,
					$endDateYMD,
					$infoArr['modId'])
		) ;
		$this->data['carCnt'] = $carCntArr[$infoArr['modId']] ;
		// 取回車型資訊 carName
		$carModel    = $this->model_common_cars->getCarSeed( $infoArr['modId']) ;
		$carModelArr = $this->model_common_option->getOptionItemsArr4Index( '1') ;

		// dump( $payTypeArr) ;
		// dump( $carModelArr[$carModel['car_model']]['opt_desc']) ;
		$rentInfo['car_model'] = $carModelArr[$carModel['car_model']]['opt_desc'] ;
		$rentInfo['car_name']  = $carModel['car_seed'] ;
		$rentInfo['car_price'] = $carModel['car_price'] ;

		// 計算此次費用
		$second1 = floor( ( strtotime( $endDateYMD) - strtotime( $startDateYMD))) ;
		$rentInfo['rentDay']  = floor( $second1 / 86400) ;	// 24hr * 60分 * 60秒
		$rentInfo['rentHour'] = ( $second1 % 86400) / 3600 ;	//
		$rentInfo['DiscountPercentage'] = $this->model_common_order->getDiscountPercentage( $startDateYMD) ;
		if ( $rentInfo['rentDay'] == 0 ) {
			$rentInfo['car_price_show'] = $rentInfo['car_price'] * $rentInfo['DiscountPercentage'] ;
		} else if ( $rentInfo['rentDay']  >= 1 && $rentInfo['rentHour'] <= 6) {
			$rentInfo['car_price_show'] = ( $rentInfo['car_price'] * $rentInfo['rentDay'] * $rentInfo['DiscountPercentage'] + $rentInfo['car_price'] * 0.1 * ceil( $rentInfo['rentHour']) ) ;
		} else { //if ( $rentInfo['rentDay']  > 1 && $rentInfo['rentHour'] > 6) {
			$rentInfo['car_price_show'] = $rentInfo['car_price'] * ( $rentInfo['rentDay'] + 1) * $rentInfo['DiscountPercentage'] ;
		}

		// dump( $rentInfo) ;
		$this->data['rentInfo'] = $rentInfo ;

		// 取得訂單類別 parent=8
		$this->data['orderTypeArr'] = $this->model_common_option->getOptionItemsArr4Index( '8') ;
		// 取得取車地點 parent=15
		$this->data['rentStation'] = $this->model_common_option->getOptionItemsArr( '15') ;


		$this->response->setOutput($this->load->view('information/staffOrder', $this->data)) ;
	}

	/**
	 * [step2 description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-07-08
	 */
	public function step2() {
		$logHeader = "|輸入訂單內容|order/staff/step2|" ;

		// dump( $this->request->post) ;
		// dump( $this->session->data) ;
		if ( !isset( $this->session->data['staff'])) {
			$this->logger->write( $this->remoteAddr . "{$logHeader}未登入系統" );
			$this->response->redirect($this->url->link('order/ordersearch/stock', '', true)) ;
			exit() ;
		} else {
			$logHeader .= "acc:{$this->session->data['staff']['username']}|" ;
		}
		$this->data['cssTime'] = time() ;
		$this->data['error_warning'] = "" ;

		/**
		 * orderType 訂單類別(來源) add by Angus 2020.03.28 增加中
		 * 146 前台業務訂車
		 * 付款類型payType		1:信用卡,2:現金,3:匯款
		 * 收費方式chargeMethod	1:未收款,2:應收,3:已收
		 */
		// $rentInfo['orderType']    = $this->request->post['order_type'] ; // 訂單類別/付費類型 add by Angus 2020.03.26
		$rentInfo['orderType']     = 146 ;                              // 訂單類別/付費類型 新增訂單類型146[前台業務訂車] add by Angus 2020.03.26
		$rentInfo['pay_type']      = $this->request->post['pay_type'] ; // 訂單類別/付費類型 add by Angus 2020.03.26
		// $rentInfo['charge_method'] = $this->request->post['charge_method'] ;

		$rentInfo['startDateYMD']  = $this->request->post['sd'] ;
		$rentInfo['endDateYMD']    = $this->request->post['ed'] ;
		$rentInfo['sStation']      = $this->request->post['sStation'] ;
		$rentInfo['eStation']      = $this->request->post['eStation'] ;

		$rentInfo['minsuName']     = explode("-", $this->request->post['minsu_name'])[1] ;
		$rentInfo['minsuID']       = $this->request->post['minsu_id'] ;

		$rentInfo['carType']       = $this->request->post['car_type'] ;
		$rentInfo['carModel']      = $this->request->post['car_model'] ;
		$rentInfo['car_name']      = $this->request->post['car_name'] ;
		$rentInfo['carCnt']        = $this->request->post['useCnt'] ;

		$rentInfo['rentDay']       = $this->request->post['rentDay'] ;
		$rentInfo['rentHour']      = $this->request->post['rentHour'] ;
		$rentInfo['discount']      = $this->request->post['discount'] ;

		//配件
		$rentInfo['baby_chair']        = "" ;
		$rentInfo['baby_chair1']       = intval( $this->request->post['baby_chair1']) ;
		$rentInfo['baby_chair2']       = intval( $this->request->post['baby_chair2']) ;
		$rentInfo['children_chair']    = intval( $this->request->post['children_chair']) ;
		$rentInfo['gps']               = intval( $this->request->post['gps']) ;
		$rentInfo['mobile_moto']       = intval( $this->request->post['mobile_moto']) ;


		$rentInfo['price']         = intval( $this->request->post['price']) ;		// 折扣後價格
		$rentInfo['priceBasic']    = intval( $this->request->post['price_basic']) ;	// 車價 未含配件
		$rentInfo['spdc_price']    = intval( $this->request->post['spdc_price']) ;	// 業務折扣費用 add by Angus 2020.03.26


		// add by Angus 將陣列轉String
		// $this->logMsg = implode( ";", array_map(
		// function ($v, $k) { return sprintf("%s='%s'", $k, $v); },
		// 	$rentInfo,
		// 	array_keys($rentInfo)
		// )) ;

		// dump( $rentInfo) ;
		// exit() ;

		$this->logMsg = urldecode( http_build_query( $rentInfo, '', ';')) ;
		$this->logger->write( $this->remoteAddr . "{$logHeader}{$this->logMsg}" ) ;

		$this->session->data['rent2'] = $rentInfo ;

		$this->data['sYear'] = date( 'Y') - 18 ;

		$this->load->model('common/zip') ;
		$this->data['retZip'] = $this->model_common_zip->getZipCode() ;


		$this->response->setOutput($this->load->view('information/staffOrderStep2', $this->data)) ;
	}

	/**
	 * [step3 description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-07-08
	 */
	public function step3() {

		// dump( $this->request->post) ;
		// dump( $this->session->data['rent2']) ;
		$logHeader = "|輸入訂單內容|order/staff/step3|" ;
		if ( !isset( $this->session->data['staff']) || !isset( $this->session->data['rent2'])) {
			$this->logger->write( $this->remoteAddr . "{$logHeader}未登入系統" );
			$this->response->redirect($this->url->link('order/ordersearch/stock', '', true)) ;
			exit() ;
		} else {
			$logHeader .= "acc:{$this->session->data['staff']['username']}|" ;
		}

		$this->load->model('common/order') ;
		$this->load->model('common/cars') ;
		$this->load->model('common/option') ;
		$stationArr = $this->model_common_option->getOptionItemsArr4Index( '15') ;


		$insData['agreement_no']      = $this->newAgreementNo() ;
		$insData['order_type']        = $this->session->data['rent2']['orderType'] ; // 工作人員
		$insData['minsuID']           = $this->session->data['rent2']['minsuID'] ;   // 民宿/業者
		$insData['minsuName']         = $this->session->data['rent2']['minsuName'] ; // 民宿/業者
		$insData['car_type']          = $this->session->data['rent2']['carType'] ;
		$insData['car_model']         = $this->session->data['rent2']['carModel'] ;
		$insData['car_name']          = $this->session->data['rent2']['car_name'] ;
		$insData['s_station']         = $this->session->data['rent2']['sStation'] ;
		$insData['e_station']         = $this->session->data['rent2']['eStation'] ;
		$insData['s_station_desc']    = $stationArr[$insData['s_station']]['opt_desc'] ;
		$insData['e_station_desc']    = $stationArr[$insData['e_station']]['opt_desc'] ;
		$insData['rent_date_out']     = $this->session->data['rent2']['startDateYMD'] ;
		$insData['rent_date_in_plan'] = $this->session->data['rent2']['endDateYMD'] ;
		$insData['sday']              = date("j", strtotime( $this->session->data['rent2']['startDateYMD'])) ;
		$insData['eday']              = date("j", strtotime( $this->session->data['rent2']['endDateYMD'])) ;
		$insData['rentDay']           = $this->session->data['rent2']['rentDay'] ;
		$insData['rentHour']          = $this->session->data['rent2']['rentHour'] ;
		$insData['discount']          = $this->session->data['rent2']['discount'] ;

		// 配件
		$space = "" ;
		$insData['baby_chair']        = $space ;
		$insData['baby_chair1']       = $this->session->data['rent2']['baby_chair1'] ; ;
		$insData['baby_chair2']       = $this->session->data['rent2']['baby_chair2'] ; ;
		$insData['children_chair']    = $this->session->data['rent2']['children_chair'] ; ;
		$insData['gps']               = $this->session->data['rent2']['gps'] ; ;
		$insData['mobile_moto']       = $this->session->data['rent2']['mobile_moto'] ; ;
		$insData['parts']             = ( $insData['baby_chair1'] + $insData['baby_chair2'] + $insData['children_chair'] + $insData['gps'] + $insData['mobile_moto']) * 100 ; // 配件總價 add by Angus 2020.03.24

		$insData['adult']             = $space ;
		$insData['baby']              = $space ;
		$insData['children']          = $space ;
		$insData['total_car']         = $this->session->data['rent2']['carCnt'] ;


		$insData['total']             = $this->session->data['rent2']['price'] ;      // 折扣後價格
		$insData['priceBasic']        = $this->session->data['rent2']['priceBasic'] ; // 全部車價 不含配件
		$insData['spdc_price']        = $this->session->data['rent2']['spdc_price'] ; // 業務折扣費用 add by Angus 2020.03.26
		$insData['pay_type']          = $this->session->data['rent2']['pay_type'] ;
		// $insData['charge_method']     = $this->session->data['rent2']['charge_method'] ;

		// 取得from post 承租人資料
		$rent_bron = $this->request->post['rent_bron_y'] . "-" . $this->request->post['rent_bron_m'] . "-" . $this->request->post['rent_bron_d'] ;
		$insData['rent_user_name']    = $this->request->post['rent_name'] ;
		$insData['rent_identity']     = '1' ;
		$insData['rent_user_id']      = $this->request->post['rent_id'] ;
		$insData['rent_user_born']    = $rent_bron ;
		$insData['rent_user_tel']     = $this->request->post['rent_mobile'] ;
		$insData['rent_user_mail']    = $this->request->post['rent_mail'] ;
		$insData['coupon']            = $space ;

		// 取得from post 取車人資料
		$get_bron  = $this->request->post['get_bron_y'] . "-" . $this->request->post['get_bron_m'] . "-" . $this->request->post['get_bron_d'] ;
		$insData['get_user_name']     = $this->request->post['get_name'] ;
		$insData['get_user_id']       = $this->request->post['get_id'] ;
		$insData['get_user_born']     = $get_bron ;
		$insData['get_user_tel']      = $this->request->post['get_mobile'] ;

		// 其他通訊資料
		$insData['rent_zip1']         = $this->request->post['rent_zipcode1'] ;
		$insData['rent_zip2']         = $this->request->post['rent_zipcode2'] ;
		$insData['rent_user_address'] = $this->request->post['rent_address'] ;

		$insData['rent_company_name'] = $this->request->post['rent_company'] ;
		$insData['rent_company_no']   = $this->request->post['rent_company_no'] ;
		$insData['urgent_man']        = $this->request->post['urgent_name'] ;
		$insData['urgent_mobile']     = $this->request->post['urgent_mobile'] ;

		$carInfoFilter = array(
				"car_seed"		=> $this->session->data['rent2']['carModel'],
				"start_date"	=> $insData['rent_date_out'],
				"end_date"		=> $insData['rent_date_in_plan'],
				"car_limit"		=> $insData['total_car'],
			) ;
		$carCntInfo = $this->model_common_cars->getCarInfo( $carInfoFilter) ;
		// dump( $carCntInfo) ;
		$insData['carCntInfo'] = serialize( $carCntInfo) ;

		$insData['car_sn'] = $carCntInfo[0]['idx'] ;
		$insData['car_no'] = $carCntInfo[0]['car_no'] ;
		// dump( $insData) ;
		// exit() ;

		// 檢查是否有加入會員 add by Angus 2019.01.17
		$checkInfo = $this->model_common_order->checkCustomerByPid( $insData['get_user_id']) ;
		if ( empty( $checkInfo) ) {
			$this->model_common_order->newCustomer( $insData) ;
			$this->logger->write( $this->remoteAddr . "{$logHeader}加入新會員|" ) ;
		}

		$this->logMsg = urldecode( http_build_query( $insData, '',';')) ;
		$this->logger->write( $this->remoteAddr . "{$logHeader}{$this->logMsg}" ) ;
		// unset( $this->session->data['rent2']) ;

		$orderSN = $this->model_common_order->newOrder( $insData) ;
		// dump( $orderSN) ;
		// todo
		$this->model_common_order->newScheduling( $insData) ;

		// foreach ($carCntInfo as $iCnt => $carInfo) {
		// 	$insData['car_sn'] = $carInfo['idx'] ;
		// 	$insData['car_no'] = $carInfo['car_no'] ;
		// 	$this->model_common_order->newScheduling( $insData) ; // 航班也在這裡 add by Angus 2018.07.21
		// }

		if ( !empty( $insData['rent_user_mail']) && filter_var( $insData['rent_user_mail'], FILTER_VALIDATE_EMAIL)) {
			$this->sendMailMsg( $insData) ;
		}

		$this->data['orderInfo']     = $insData ;
		$this->data['mailMsg']       = $this->mailMsg ;
		$this->data['token']         = $this->session->data['token'] ;
		$this->data['orderSN']       = $orderSN ;
		$this->data['error_warning'] = "" ;

		if ( date( 'Y-m-d') == substr( $insData['rent_date_out'], 0, 10)) {
			$this->data['todayGetCar'] = true ;
		} else {
			$this->data['todayGetCar'] = false ;
		}

		// 追加開發情況
		if ( SERVER_ADDRESS == "localhost") {
			$this->data['Backstage'] = "admin" ;
		} else {
			$this->data['Backstage'] = "nimda" ;
		}

		// http://kmfun.tw/?route=order/ordersearch/result&m1=0922715660&o1=201907170046
		$this->response->setOutput($this->load->view('information/staffOrderView', $this->data)) ;
	}

	/**
	 * [getAjaxMinsu description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-01-06
	 * <!--autocomplete-->
	 * 要用jquery-2.1.1.min.js 這個版本才會動作
	 * <script src="kmfun/view/js/jquery-2.1.1.min.js?<?=$cssTime?>"></script>
	 * <script src="kmfun/view/js/jquery-ui/jquery-ui.min.js?<?=$cssTime?>"></script>
	 * <link rel="stylesheet" type="text/css" href="kmfun/view/js/jquery-ui/jquery-ui.min.css" />
	 * <!--autocomplete-->
	 * http://localhost/kmfun_Nopaper/?route=order/staff/getAjaxMinsu&filter_name=
	 */
	public function getAjaxMinsu() {
		// dump( $this->request->get) ;
		$json = array() ;
		if (isset($this->request->get['filterName'])) {
			$filter_name = $this->request->get['filterName'];
		} else {
			$filter_name = '';
		}

		$filter_data = array(
			'filter_name'  => $filter_name,
			'start'        => 0,
			'limit'        => 5
		);

		$this->load->model('common/staff') ;
		$results = $this->model_common_staff->getMinsuFromAjax($filter_data);

		foreach ($results as $result) {
			$json[] = array(
				'code' => strip_tags(html_entity_decode($result['idx'], ENT_QUOTES, 'UTF-8')),
				"name" => strip_tags(html_entity_decode($result['data_type'] . "-" . $result['name'], ENT_QUOTES, 'UTF-8'))
			);
		}

		$this->response->addHeader('Content-Type: application/json') ;
		$this->response->setOutput(json_encode($json)) ;
	}

	/**
	 * [buileSendMailMsg description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2018-03-20
	 */
	protected function buileSendMailMsgContent( $data = array()) {
		// 配件
		$tmpOther = "" ;
		if($data['baby_chair'] > 0) {
			$tmpOther .= "嬰兒安全座椅 x {$data['baby_chair']}" ;
		}
		if($data['baby_chair1'] > 0) {
			if($tmpOther != "")
				$tmpOther .= "\r\n　　&nbsp;&nbsp;&nbsp;";
			$tmpOther .= "兒童安全座椅 x {$data['baby_chair1']}" ;
		}
		if($data['baby_chair2'] > 0) {
			if($tmpOther != "")
				$tmpOther .= "\r\n　　&nbsp;&nbsp;&nbsp;";
			$tmpOther .= "兒童增高墊 x {$data['baby_chair2']}" ;
		}
		if($data['children_chair'] > 0) {
			if($tmpOther != "")
				$tmpOther .= "\r\n　　&nbsp;&nbsp;&nbsp;";
			$tmpOther .= "嬰兒推車 x {$data['children_chair']}" ;
		}
		if($data['gps'] > 0) {
			if($tmpOther != "")
				$tmpOther .= "\r\n　　&nbsp;&nbsp;&nbsp;";
			$tmpOther .= "GPS x {$data['gps']}" ;
		}
		if($data['mobile_moto'] > 0) {
			if($tmpOther != "")
				$tmpOther .= "\r\n　　&nbsp;&nbsp;&nbsp;";
			$tmpOther .= "機車導航架 x {$data['mobile_moto']}" ;
		}
		if($tmpOther == "")
			$tmpOther .= "無";

		// 租車費用文字說明調整 add by Angus 2018.07.05 ---------------------------------------------------------------
		$rentPriceStr = "\n" ;
		if ( $data['discount'] < 1) {
			$rentPriceStr .= "         早鳥優惠 ".($data['discount'] * 10)." 折\n" ;
		}
		$data['car_price2Str'] = number_format( $data['total']) ;
		$data['car_price3Str'] = $data['parts'] ;
		$rentPriceStr .= "         車價（共{$data['total_car']}台，每台借車時間為 {$data['rentDay']}天又{$data['rentHour']}小時），共 ". number_format($data['priceBasic'])."元\n" ;
		$rentPriceStr .= "         配件合計 {$data['car_price3Str']}元\n" ;
		$rentPriceStr .= "         總價（含稅）" . number_format($data['total'] + $data['spdc_price'])." 元\n" ;
		$rentPriceStr .= "         業務折扣 -".number_format($data['spdc_price'])."元\n" ;
		$rentPriceStr .= "         應收總價（含稅）{$data['car_price2Str']}元\n" ;

		$pattern = "
＃＃＃＃＃＃＃＃＃＃＃
＃　車輛行事曆資訊　＃
＃＃＃＃＃＃＃＃＃＃＃
車種 : %s
車型 : %s
訂車數量 : %s
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
【租車資訊】
訂單編號 : %s
訂單類別 : 現場訂車
民宿(業者) : %s
取車地點 : %s
還車地點 : %s
取車時間 : %s
還車時間 : %s
租車費用 : %s
備註 :
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
【客戶資料】
承租人姓名 : %s
身分證字號／護照號碼 : %s
出生年月日 : %s
手機 : %s
取車人姓名 : %s
身分證字號／護照號碼 : %s
出生年月日 : %s
手機 : %s
航班時間 : %s
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
通訊地址 : %s
公司名稱 : %s
公司統編 : %s
緊急連絡人 : %s
緊急連絡人電話 : %s
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
配件 : %s
" ;
		return sprintf( $pattern,
						$data['car_type'],	// 車種
						$data['car_name'],	// 車型
						$data['total_car'],				// 訂車數量
						$data['agreement_no'],			// 訂單編號
						$data['minsuName'],             // 民宿/業者
						$data['s_station_desc'],		// 取車地點
						$data['e_station_desc'],		// 還車地點
						$data['rent_date_out'],			// 取車時間
						$data['rent_date_in_plan'],		// 還車時間
						$rentPriceStr,					// 租車費用
						$data['rent_user_name'],		// 承租人姓名
						$data['rent_user_id'],			// 身分證字號／護照號碼
						$data['rent_user_born'],		// 出生年月日
						$data['rent_user_tel'],			// 手機
						$data['get_user_name'],			// 取車人姓名
						$data['get_user_id'],			// 身分證字號／護照號碼
						$data['get_user_born'],		    // 出生年月日
						$data['get_user_tel'],		    // 手機
						" ",							// 航班時間
						$data['rent_user_address'],	// 通訊地址
						$data['rent_company_name'],			// 公司名稱
						$data['rent_company_no'],		// 公司統編
						$data['urgent_man'],		// 緊急連絡人
						$data['urgent_mobile'],	// 緊急連絡人電話
						$tmpOther
		) ;
	}
// 訂單類別 : %s
// 民宿(業者) :%s
	/**
	 * [sendMailMsg description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-07-14
	 */
	protected function sendMailMsg( $data = array()) {
		// dump( $data) ;
		$mail = new Mail();

		$mail->protocol      = $this->config->get('config_mail_protocol') ;
		$mail->parameter     = $this->config->get('config_mail_parameter') ;
		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname') ;
		$mail->smtp_username = $this->config->get('config_mail_smtp_username') ;
		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8') ;
		$mail->smtp_port     = $this->config->get('config_mail_smtp_port') ;
		$mail->smtp_timeout  = $this->config->get('config_mail_smtp_timeout') ;

		// $subject = "金豐租車訂單成功通知信" ;
		$subject = "NoPaper訂單通知 訂單編號:{$data['agreement_no']}" ;
		$this->mailMsg = $this->buileSendMailMsgContent( $data) ;

		$mail->setTo( $data['rent_user_mail']) ;
		$mail->setFrom($this->config->get('config_email')) ;
		$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) ;
		$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8')) ;
		$mail->setText(html_entity_decode($this->mailMsg, ENT_QUOTES, 'UTF-8')) ;
		$mail->send();

		$sendMailArr = array(
			"kmfun005@gmail.com",
			"kmfun999@gmail.com"
		) ;
		if ( WEBSITE_TYPE != 'dev') {
			$mail->setTo( $sendMailArr) ;
			$mail->send();
		}

		$sendMailArr = array(
			"kh0813@gmail.com",
			"jaff.tw@gmail.com"
		) ;
		$mail->setTo( $sendMailArr) ;
		$mail->send();
	}

	/**
	 * [newAgreementNo description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-01-16
	 */
	protected function newAgreementNo() {
		$todayNo = date( 'Ymd') ;
		return $this->model_common_order->newAgreementNo( $todayNo) ;
	}

}
