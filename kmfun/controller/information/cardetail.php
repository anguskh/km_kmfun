<?php
class ControllerInformationCardetail extends Controller {
	public function index() {
		$data = array() ;
		// $this->document->setTitle($this->config->get('config_meta_title'));
		// $this->document->setDescription($this->config->get('config_meta_description'));
		// $this->document->setKeywords($this->config->get('config_meta_keyword'));

		// if (isset($this->request->get['route'])) {
		// 	$this->document->addLink($this->config->get('config_url'), 'canonical');
		// }

		// $data['column_left'] = $this->load->controller('common/column_left');
		// $data['column_right'] = $this->load->controller('common/column_right');
		// $data['content_top'] = $this->load->controller('common/content_top');
		// $data['content_bottom'] = $this->load->controller('common/content_bottom');
		$carIdx = $this->request->get['sn'];
		// dump( $this->session) ;
		$this->session->data['tokenf'] = $this->session->session_id ;

		$this->load->model('common/cars') ;
		$data['carSeed'] = $this->model_common_cars->getCarSeed($carIdx);

		$data['footer'] = $this->load->controller('common/footer') ;
		$data['header'] = $this->load->controller('common/header') ;

		$this->response->setOutput($this->load->view('information/cardetail', $data)) ;
	}
}
