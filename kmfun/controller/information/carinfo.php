<?php
class ControllerInformationCarinfo extends Controller {
	public function index() {
		$data = array() ;
		// $this->document->setTitle($this->config->get('config_meta_title'));
		// $this->document->setDescription($this->config->get('config_meta_description'));
		// $this->document->setKeywords($this->config->get('config_meta_keyword'));

		// if (isset($this->request->get['route'])) {
		// 	$this->document->addLink($this->config->get('config_url'), 'canonical');
		// }

		// $data['column_left'] = $this->load->controller('common/column_left');
		// $data['column_right'] = $this->load->controller('common/column_right');
		// $data['content_top'] = $this->load->controller('common/content_top');
		// $data['content_bottom'] = $this->load->controller('common/content_bottom');

		// dump( $this->session) ;
		$this->session->data['tokenf'] = $this->session->session_id ;

		$this->load->model('common/option') ;
		$data['carModels'] = $this->model_common_option->getCarOptionItemsArrCount('1') ;
		// dump( $data['carModels']) ;

		$this->load->model('common/cars') ;
		$data['carSeeds'] = $this->model_common_cars->getCarSeedAll() ;


		$this->load->model('tool/image') ;
		foreach ($data['carSeeds'] as $iCnt => $carSeed) {
			foreach ($carSeed['car_seed'] as $key => $value) {
				// 車子圖片
				if ( $value['image'] && is_file(DIR_IMAGE . $value['image'])) {
					$data['carSeeds'][$iCnt]['car_seed'][$key]['thumb'] = $this->model_tool_image->resize($value['image'], 230, 134) ;
				} else {
					$data['carSeeds'][$iCnt]['car_seed'][$key]['thumb'] = $this->model_tool_image->resize('no_image.png', 230, 134);
				}
			}
		}
		$data['activeShell'] = isset( $this->request->get['id']) ? $this->request->get['id'] : "" ;

		$data['footer'] = $this->load->controller('common/footer') ;
		$data['header'] = $this->load->controller('common/header') ;

		$this->response->setOutput($this->load->view('information/carinfo', $data)) ;
	}
}
