<?php
class ControllerInformationBedinfo extends Controller {
	public function index() {
		$data = array() ;

		$this->session->data['tokenf'] = $this->session->session_id ;

		$this->load->model('common/bed') ;
		$data['bedList'] = $this->model_common_bed->getBedList() ;

		$data['footer'] = $this->load->controller('common/footer') ;
		$data['header'] = $this->load->controller('common/header') ;

		$this->response->setOutput($this->load->view('information/bedinfo', $data)) ;
	}
	public function detail() {
		$data = array() ;
		$bedIdx = $this->request->get['sn'];
		$this->session->data['tokenf'] = $this->session->session_id ;

		$this->load->model('common/bed') ;
		$data['bedInfo'] = $this->model_common_bed->getBedInfo($bedIdx) ;
		$data['roomdata'] = $this->model_common_bed->getRoomData($bedIdx) ;

		// 圖片設定
		if ( !empty($data['bedInfo']))
			$imageIntroArr = unserialize( $data['bedInfo']['imageIntro'] ) ;
		else {
			$imageIntroArr = array() ;
		}
		$data['bedInfo']['imageIntroArr']=$imageIntroArr;
		$data['footer'] = $this->load->controller('common/footer') ;
		$data['header'] = $this->load->controller('common/header') ;

		$this->response->setOutput($this->load->view('information/beddetail', $data)) ;
	}
}
