<?php
class ControllerInformationPage extends Controller {
	public function index() {
		$this->load->model('common/page') ;
		if ( !isset($this->request->get['token'])) {
			$this->response->redirect(HTTP_SERVER) ;
		}

		$guidStr = $this->request->get['token'] ;
		// dump( $this->request->get) ;

		$information_info = $this->model_common_page->getInformation($guidStr) ;
		if ( !isset( $information_info['description'])) {
			$information_info = $this->model_common_page->getSliderPageInfo($guidStr) ;
			// dump( $information_info) ;
			if ( !isset( $information_info['page_desc'])) $this->response->redirect(HTTP_SERVER) ;
			$information_info['description'] = $information_info['page_desc'] ;
		}

		// dump( $information_info) ;
		// $this->document->setDescription($information_info[0]['description']) ;
		// dump( $this->document) ;
		//
		$data['information_info'] = html_entity_decode($information_info['description'], ENT_QUOTES, 'UTF-8');

		$data['footer'] = $this->load->controller('common/footer') ;
		$data['header'] = $this->load->controller('common/header') ;

		$this->response->setOutput($this->load->view('common/page', $data)) ;
	}
}