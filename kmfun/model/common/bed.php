<?php
class ModelCommonBed extends Model {

	public function getBedList() {
		$SQLCmd = "SELECT idx,name,pos,pos1,reg_no,imageIntro FROM tb_minshuku " ;
		$query = $this->db->query( $SQLCmd) ;
		return $query->rows ;
	}

	public function getBedInfo($bedIdx="") {
		if ($bedIdx!="") {
			$SQLCmd = "SELECT * FROM tb_minshuku WHERE idx = '{$bedIdx}' " ;
			$query = $this->db->query( $SQLCmd) ;
			return $query->rows[0] ;
		}else{
			return false;
		}
	}

	function getRoomData($bedIdx){
		$SQLCmd = "SELECT * FROM tbl_room_type WHERE src_idx = '{$bedIdx}' ";
 		$query = $this->db->query( $SQLCmd) ;
 		return $query->rows ;
	}
	
}