<?php
class ModelCommonOption extends Model {
	public function getOptionItemsArr( $parentIdx = 0) {
		$SQLCmd = "SELECT * FROM sys_option WHERE parent = '{$parentIdx}' AND status = '1' ORDER BY opt_seq ASC" ;
		$query = $this->db->query( $SQLCmd) ;

		return $query->rows ;
	}

	public function getOptionItemsArr4Index( $parentIdx = 0) {
		$SQLCmd = "SELECT * FROM sys_option WHERE parent = '{$parentIdx}' AND status = '1' ORDER BY opt_seq ASC" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;

		$retArr = array() ;
		foreach ($query->rows as $iCnt => $row) {
			$retArr[$row['idx']] = $row ;
		}

		return $retArr ;
	}

	/**
	 * [getOptionArrUseParent description]
	 * @param   array      $parArr [description]
	 * @return  [type]             [description]
	 * @Another Angus
	 * @date    2018-07-18
	 */
	public function getOptionArrUseParent( $parArr = array()) {

		$retArr = array() ;
		foreach ($parArr as $i => $parentID) {
			$tmp = $this->getOptionItemsArr( $parentID) ;
			$retArr = array_merge( $retArr, $tmp) ;
		}
		return $retArr ;
	}

	public function getCarStation( $stationIdxArr = array()) {
		if ( !empty( $stationIdxArr[1]) )
			$inStr = join(',', $stationIdxArr) ;
		else
			$inStr = $stationIdxArr[0] ;

		$SQLCmd = "SELECT * FROM sys_option WHERE parent = '15' AND status = '1' AND idx in ({$inStr}) ORDER BY opt_seq ASC" ;
		$query = $this->db->query( $SQLCmd) ;

		$retArr = array() ;
		foreach ($query->rows as $tmpRow) {
			$retArr[$tmpRow['idx']] = $tmpRow ;
		}

		return $retArr ;
	}

	public function getCarOptionItemsArrCount( $parentIdx = 0) {
		$SQLCmd = "
SELECT so.*, COUNT(so.opt_seq) car_cnt FROM sys_option so
	LEFT JOIN sys_car_model scm ON so.idx = scm.car_model
	LEFT JOIN sys_cars sc  ON scm.idx = sc.car_seed
	WHERE so.parent =1 AND so.status=1 AND scm.status=1
		GROUP BY so.opt_seq
		ORDER BY so.opt_seq ASC" ;

		// $SQLCmd  = "SELECT so.*, COUNT(so.opt_seq) car_cnt FROM sys_cars sc
		// 		LEFT JOIN sys_car_model scm ON scm.idx = sc.car_seed
		// 		LEFT JOIN sys_option so ON so.parent = '{$parentIdx}' AND so.status = '1' AND so.idx = scm.car_model
		// 		GROUP BY so.opt_seq
		// 		ORDER BY so.opt_seq ASC";
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;

		return $query->rows ;
	}

	/**
	 * [getIndexKV_Slider description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-04-11
	 */
	public function getIndexKV_Slider() {
		$SQLCmd = "SELECT * FROM oc_manufacturer WHERE status='1' ORDER BY sort_order ASC" ;

		$query = $this->db->query( $SQLCmd) ;
		return $query->rows ;
	}

	public function getRentTime() {
		return array(
				"07:30",
				"08:00",
				"08:30",
				"09:00",
				"09:30",
				"10:00",
				"10:30",
				"11:00",
				"11:30",
				"12:00",
				"12:30",
				"13:00",
				"13:30",
				"14:00",
				"14:30",
				"15:00",
				"15:30",
				"16:00",
				"16:30",
				"17:00",
				"17:30",
				"18:00",
				"18:30",
			) ;
	}

	public function checkQid($qid=''){
		$SQLCmd = "SELECT * FROM oc_qrcode WHERE status='1' and qrcode='{$qid}'" ;
		$query = $this->db->query( $SQLCmd) ;
		$now_date = date("Y-m-d H:i:s");
		$tmp_time = "";
		$qid_flag = 'N';
		if($query->num_rows == 1){
			//檢查是否在時間內
			$retArr = array() ;
			foreach ($query->rows[0] as $key=>$tmpRow) {
				$retArr[$key] = $tmpRow ;
			}
			if($retArr['flag'] == 'N' && ($now_date >= $retArr['start_date'] && $now_date <= $retArr['effective_date'])){
				$qid_flag = 'Y';

				//取車日=現在時間後30分 以整數為單位
				$min = date("H:i",mktime(date("H"), date("i")+30, 0, 0 , 0, 0));
				$tmp_array= array(
					"07:30",
					"08:00",
					"08:30",
					"09:00",
					"09:30",
					"10:00",
					"10:30",
					"11:00",
					"11:30",
					"12:00",
					"12:30",
					"13:00",
					"13:30",
					"14:00",
					"14:30",
					"15:00",
					"15:30",
					"16:00",
					"16:30",
					"17:00",
					"17:30",
					"18:00",
					"18:30",
				) ;


				foreach($tmp_array as $arr){
					if($tmp_time == "" && ($arr >= $min)){
						$tmp_time = $arr;
					}
				}
			}
		}

		$return = array();
		$return[0] = $qid_flag;
		$return[1] = $retArr;
		$return[2] = $tmp_time;
		return $return ;

	}
}