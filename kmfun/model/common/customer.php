<?php
/**
 * [ModelCommonCustomer 會員中心]
 * @Another Angus
 * @date    2018-12-19
 */
class ModelCommonCustomer extends Model {

	/**
	 * [getCustomerAddress description]
	 * @param   string     $customer_id [description]
	 * @return  [type]                  [description]
	 * @Another Angus
	 * @date    2018-12-21
	 */
	public function getCustomerAddress( $customer_id = "") {
		$customer_id = $this->db->escape( $customer_id) ;
		$SQLCmd = "SELECT * FROM oc_address WHERE customer_id='{$customer_id}'" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		return $query->row ;
	}

	/**
	 * [getCustomerInfo description]
	 * @param   [type]     $user_id [description]
	 * @return  [type]              [description]
	 * @Another Angus
	 * @date    2019-01-01
	 */
	public function getCustomerInfo( $user_id) {
		$user_id = $this->db->escape( $user_id) ;
		$SQLCmd = "SELECT * FROM oc_customer WHERE user_id='{$user_id}'" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		// dump( $query) ;
		return $query->num_rows ;
	}

	/**
	 * [setCustomerInfo description]
	 * @param   array      $editArr [description]
	 * @Another Angus
	 * @date    2018-12-23
	 */
	public function setCustomerInfo(  $editArr = array()) {
		foreach ($editArr as $keyName => $value) {
			$editArr[$keyName] = $this->db->escape( $value) ;
		}

		$SQLCmd = "UPDATE oc_customer SET firstname='{$editArr['name']}',
				email='{$editArr['email']}', telephone='{$editArr['tel']}'
			WHERE customer_id={$editArr['customer_id']}" ;
		$this->db->query( $SQLCmd) ;

		$SQLCmd = "SELECT zip_name FROM sys_zip WHERE zip_code=8" ;
		$query = $this->db->query( $SQLCmd) ;

		$SQLCmd = "UPDATE oc_address SET city='{$query->row['zip_name']}', postcode='{$editArr['zipCode2']}',
				address_1='{$editArr['address']}' WHERE address_id={$editArr['address_id']}" ;
		$query = $this->db->query( $SQLCmd) ;
	}

	/**
	 * [setPassword description]
	 * @param   array      $editArr [description]
	 * @Another Angus
	 * @date    2018-12-26
	 */
	public function setPassword( $editArr = array()) {
		foreach ($editArr as $keyName => $value) {
			$editArr[$keyName] = $this->db->escape( $value) ;
		}
		$SQLCmd = "UPDATE oc_customer SET password='{$editArr['password']}'
			WHERE customer_id={$editArr['customer_id']}" ;
		$this->db->query( $SQLCmd) ;
	}

	/**
	 * [getOrderList description]
	 * @param   string     $rent_user_id [description]
	 * @return  [type]                   [description]
	 * @Another Angus
	 * @date    2018-12-20
	 */
	public function getOrderList( $rent_user_id = "") {
		$SQLCmd = "SELECT * FROM tb_order WHERE rent_user_id='{$rent_user_id}' AND order_status is NULL ORDER BY create_date DESC" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		return $query->rows ;
	}

	/**
	 * [loginCkeck description]
	 * @param   array      $selData [description]
	 * @return  [type]              [description]
	 * @Another Angus
	 * @date    2018-12-19
	 */
	public function loginCkeck( $selData = array()) {
		$user_id = $this->db->escape( $selData['customer_id']) ;
		$telephone = $this->db->escape( $selData['password']) ;

		$SQLCmd = "SELECT * FROM oc_customer WHERE user_id='{$user_id}' AND password='{$telephone}'" ;
		$query = $this->db->query( $SQLCmd) ;
		if ( $query->num_rows == 0) {
			$SQLCmd = "SELECT * FROM oc_customer WHERE user_id='{$user_id}' AND telephone='{$telephone}'" ;
			// dump( $SQLCmd) ;
			$query = $this->db->query( $SQLCmd) ;
		}
		return $query->row ;
	}

	/**
	 * [forgetCkeck description]
	 * @param   array      $selData [description]
	 * @return  [type]              [description]
	 * @Another Angus
	 * @date    2018-12-20
	 */
	public function forgetCkeck( $selData = array()) {
		$user_id = $this->db->escape( $selData['customer_id']) ;
		$email   = $this->db->escape( $selData['email']) ;

		$SQLCmd = " SELECT * FROM oc_customer WHERE user_id='{$user_id}' AND email='{$email}'" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		return $query->row ;
	}


	/**
	 * [addCustomer description]
	 * @param   array      $insArr [description]
	 * @Another Angus
	 * @date    2019-01-02
	 */
	public function addCustomer( $data = array()) {

		$SQLCmd = "INSERT INTO " . DB_PREFIX . "customer SET customer_group_id = '" . (int)$data['customer_group_id'] . "', user_id='" . $this->db->escape($data['user_id']) . "', password='" . $this->db->escape($data['password']) . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', status = '1', approved = '1', safe = '0', date_added = NOW()" ;
		$this->db->query( $SQLCmd) ;

		$customer_id = $this->db->getLastId();

		$cityName = "";
		if($data['zipcode1'] != ""){
			$SQLCmd   = "SELECT zip_name FROM sys_zip WHERE zip_code='{$data['zipcode1']}'" ;
			$query    = $this->db->query( $SQLCmd) ;
			$cityName = $query->row['zip_name'] ;
		}

		$SQLCmd = "INSERT INTO " . DB_PREFIX . "address SET customer_id = '" . (int)$customer_id . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', address_1 = '" . $this->db->escape($data['address']) . "', city = '{$cityName}', postcode = '" . $this->db->escape($data['zipcode2']) . "'" ;
		// dump( $SQLCmd) ;
		$this->db->query( $SQLCmd) ;

	}




















}