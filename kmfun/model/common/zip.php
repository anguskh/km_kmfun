<?php
class ModelCommonZip extends Model {
	public function getZipCode( $zipMain = "") {
		// dump( $zipMain) ;
		$SQLCmd = "SELECT zip_name, zip_code FROM sys_zip WHERE 1 " ;

		if ( $zipMain == "all") {
			$whereStr = "" ;
		} else if ( !empty( $zipMain)) {
			$whereStr = " AND parent_id={$zipMain}" ;
		} else {
			$whereStr = " AND parent_id=0" ;
		}
		$SQLCmd .= $whereStr ;

		$query = $this->db->query( $SQLCmd) ;
		return $query->rows ;
	}

	/**
	 * [getZipCode description]
	 * @param   string     $zipMain [description]
	 * @return  [type]              [description]
	 * @Another Angus
	 * @date    2018-12-21
	 */
	public function getOptionZipCode( $zipMain = "") {
		// dump( $zipMain) ;
		$SQLCmd = "SELECT zip_name, zip_code FROM sys_zip WHERE 1 " ;

		if ( $zipMain == "all") {
			$whereStr = "" ;
		} else if ( !empty( $zipMain)) {
			$whereStr = " AND parent_id={$zipMain}" ;
		} else {
			$whereStr = " AND parent_id=0" ;
		}
		$SQLCmd .= $whereStr ;

		$query = $this->db->query( $SQLCmd) ;
		$retArr = array() ;
		foreach ($query->rows as $iCnt => $tmpVal) {
			$retArr[ $tmpVal['zip_code']] = $tmpVal['zip_name'] ;
		}
		return $retArr ;
	}

	public function getAllZipCode () {
		$SQLCmd = "SELECT zip_name, zip_code FROM sys_zip WHERE 1 " ;
		$query = $this->db->query( $SQLCmd) ;

		$retArr = array() ;
		foreach ($query->rows as $iCnt => $tmpVal) {
			$retArr[ $tmpVal['zip_code']] = $tmpVal['zip_name'] ;
		}
		return $retArr ;
	}

	/**
	 * [zipCodeReverseLookup description]
	 * @param   string     $zipCode [description]
	 * @return  [type]              [description]
	 * @Another Angus
	 * @date    2018-12-21
	 */
	public function zipCodeReverseLookup( $zipCode = "") {
		$zipCode = $this->db->escape( $zipCode) ;
		if ( empty($zipCode)) {
			return array("","") ;
		}
		$SQLCmd = "SELECT parent_id FROM sys_zip WHERE zip_code={$zipCode}" ;
		$query = $this->db->query( $SQLCmd) ;
		if ( $query->num_rows == 0) {
			return array("","") ;
		} else {
			$parent_id = $query->row['parent_id'] ;
		}

		$SQLCmd = "SELECT zip_name, zip_code FROM sys_zip WHERE parent_id={$parent_id}" ;
		$query = $this->db->query( $SQLCmd) ;
		if ( $query->num_rows == 0) {
			return array("","") ;
		}
		$retArr = array() ;
		foreach ($query->rows as $iCnt => $row) {
			$retArr[$row['zip_code']] = $row['zip_name'] ;
		}
		return array( $parent_id, $retArr) ;
	}
}

