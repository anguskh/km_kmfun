<?php
class ModelCommonOrder extends Model {

	public function newAgreementNo( $todayNo) {
		$newAgreementNo = "" ;

		$SQLCmd = "SELECT * FROM tb_order WHERE agreement_no like '{$todayNo}%' order by agreement_no desc limit 1" ;
		$query = $this->db->query( $SQLCmd) ;
		if ( isset( $query->rows[0])) {
			$atLastNo = (int)str_replace($todayNo, "", $query->rows[0]['agreement_no']) + 1 ;
			$newAgreementNo = $todayNo.str_pad($atLastNo, 4,'0', STR_PAD_LEFT) ;
		} else {
			$atLastNo = 1 ;
			$newAgreementNo = $todayNo.str_pad($atLastNo, 4,'0', STR_PAD_LEFT) ;
		}

		return $newAgreementNo ;
	}

	/**
	 * [newOrder description]
	 * @param   [type]     $insData  [description]
	 * @param   string     $qid_flag [沒說明是什麼 那裡用到的??]
	 * @return  [type]               [description]
	 * @Another Angus??
	 * @date    2020-04-04
	 */
	public function newOrder( $insData, $qid_flag="") {
		// dump( $insData) ;
		$now = date('Y-m-d H:i:s') ;
		// add by Angus 2019.07.14 原本寫死webSite 加入前台工作人員也可以開單
		$createUser    = isset( $this->session->data['staff']) ? $this->session->data['staff']['username'] : "webSite" ;
		$minsuID       = isset( $insData['minsuID'])       ? $insData['minsuID'] : "" ;
		$pay_type      = isset( $insData['pay_type'])      ? $this->db->escape( $insData['pay_type']) : "" ;		// 付款類型 add by Angus 2020.03.30
		$charge_method = isset( $insData['charge_method']) ? $this->db->escape( $insData['charge_method']) : "" ;	// 收費方式 add by Angus 2020.03.30
		$spdc          = isset( $insData['spdc_price'])    ? $this->db->escape( $insData['spdc_price']) : "" ;		// 業務折扣 add by Angus 2020.03.30

		// QRCode 增加的判斷
		$QRCodeSqlStr = "" ;
		if ( $qid_flag == "Y" || $qid_flag == "YY") {
			$QRCodeSqlStr = "qrcode='{$this->db->escape($insData['qrcode'])}', charge_method='{$this->db->escape($insData['charge_method'])}'," ;
		}

		$SQLCmd = "INSERT INTO tb_order SET
			agreement_no      ='{$insData['agreement_no']}',
			car_model         ='{$this->db->escape($insData['car_model'])}',
			order_type        ='{$insData['order_type']}',
			pay_type          ='{$pay_type}',

			minsu_idx         ='{$minsuID}',
			car_sn            ='{$insData['car_sn']}',
			car_no            ='{$insData['car_no']}',
			s_station         ='{$this->db->escape($insData['s_station'])}',
			e_station         ='{$this->db->escape($insData['e_station'])}',
			coupon            ='{$this->db->escape($insData['coupon'])}',
			rent_date_out     ='{$this->db->escape($insData['rent_date_out'])}',
			rent_date_in_plan ='{$this->db->escape($insData['rent_date_in_plan'])}',

			rent_user_name    ='{$this->db->escape($insData['rent_user_name'])}',
			rent_identity     ='{$this->db->escape($insData['rent_identity'])}',
			rent_user_id      ='{$this->db->escape($insData['rent_user_id'])}',
			rent_user_born    ='{$this->db->escape($insData['rent_user_born'])}',
			rent_user_tel     ='{$this->db->escape($insData['rent_user_tel'])}',
			rent_user_mail    ='{$this->db->escape($insData['rent_user_mail'])}',

			get_user_name     ='{$this->db->escape($insData['rent_user_name'])}',
			get_identity      ='{$this->db->escape($insData['rent_identity'])}',
			get_user_id       ='{$this->db->escape($insData['rent_user_id'])}',
			get_user_born     ='{$this->db->escape($insData['rent_user_born'])}',
			get_user_tel      ='{$this->db->escape($insData['rent_user_tel'])}',

			rent_zip1         ='{$this->db->escape($insData['rent_zip1'])}',
			rent_zip2         ='{$this->db->escape($insData['rent_zip2'])}',
			rent_user_address ='{$this->db->escape($insData['rent_user_address'])}',
			rent_company_name ='{$this->db->escape($insData['rent_company_name'])}',
			rent_company_no   ='{$this->db->escape($insData['rent_company_no'])}',
			urgent_man        ='{$this->db->escape($insData['urgent_man'])}',
			urgent_mobile     ='{$this->db->escape($insData['urgent_mobile'])}',

			baby_chair        ='{$this->db->escape($insData['baby_chair'])}',
			baby_chair1       ='{$this->db->escape($insData['baby_chair1'])}',
			baby_chair2       ='{$this->db->escape($insData['baby_chair2'])}',
			children_chair    ='{$this->db->escape($insData['children_chair'])}',
			gps               ='{$this->db->escape($insData['gps'])}',
			mobile_moto       ='{$this->db->escape($insData['mobile_moto'])}',
			adult             ='{$this->db->escape($insData['adult'])}',
			baby              ='{$this->db->escape($insData['baby'])}',
			children          ='{$this->db->escape($insData['children'])}',
			total_car         ='{$this->db->escape($insData['total_car'])}',
			other_car         ='{$this->db->escape($insData['carCntInfo'])}',
			{$QRCodeSqlStr}
			sale_off          ='{$this->db->escape($insData['sale_off'])}',
			total             ='{$this->db->escape($insData['total'])}',
			spdc              ='{$spdc}',

			createUser        ='{$createUser}',
			create_date       ='{$now}' " ;

		//另外要把qrcode狀態變成Y
		$qrcodeStr = isset( $insData['qrcode']) ? $insData['qrcode'] : "" ;
		$sql_qrcode = "UPDATE oc_qrcode SET flag = 'Y', agreement_no='{$insData['agreement_no']}'
			WHERE qrcode='{$qrcodeStr}'";
		$query = $this->db->query( $SQLCmd) ;
		// dump( $SQLCmd) ;
		// exit() ;
		if($query && $qid_flag == "Y"){
			$this->db->query( $sql_qrcode) ;
		}

		return $this->db->getLastId() ;
	}

	public function newScheduling( $insData) {
		$now = date('Y-m-d H:i:s') ;
		// add by Angus 2019.07.14 原本寫死webSite 加入前台工作人員也可以開單
		$createUser = isset( $this->session->data['staff']) ? $this->session->data['staff']['username'] : "webSite" ;
		$memo = ($createUser == "webSite") ? "網頁訂單" : "現場訂車 業務手動key單" ;

		$SQLCmd = "INSERT INTO tb_scheduling SET
			car_seed    ='{$this->db->escape($insData['car_model'])}',
			car_idx     ='{$insData['car_sn']}',
			order_type  ='{$insData['order_type']}',

			s_station   ='{$this->db->escape($insData['s_station'])}',
			e_station   ='{$this->db->escape($insData['e_station'])}',

			start_date  ='{$this->db->escape($insData['rent_date_out'])}',
			end_date    ='{$this->db->escape($insData['rent_date_in_plan'])}',
			sday        ='{$insData['sday']}', eday='{$insData['eday']}',
			cust_name   ='{$this->db->escape($insData['rent_user_name'])}',
			order_no    ='{$insData['agreement_no']}',
			cust_tel    ='{$this->db->escape($insData['rent_user_tel'])}',

			take_name   ='{$this->db->escape($insData['rent_user_name'])}',
			take_mobile ='{$this->db->escape($insData['rent_user_tel'])}',

			reg_user    ='{$createUser}',
			reg_date    ='{$now}',
			status      ='1',
			memo        ='{$memo}'" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;

		// 航班資訊
		if ( !empty( $insData['trans_type'])) {
			$SQLCmd = "INSERT INTO tb_flight SET order_no='{$insData['agreement_no']}',
				trans_type='{$insData['trans_type']}',
				airport='{$insData['airport']}',
				pier='{$insData['pier']}',
				s_date='{$insData['flight_sDate']}'
			 " ;
			 // dump( $SQLCmd) ;
			 $query = $this->db->query( $SQLCmd) ;
		}
	}

	/**
	 * [getOrder description]
	 * @param   array      $filterArr [description]
	 * @return  [type]                [description]
	 * @Another Angus
	 * @date    2018-09-07
	 */
	public function getOrder( $agreement_no){
		$SQLCmd = "	SELECT * FROM tb_order
						WHERE agreement_no = '{$agreement_no}'" ;
		$query = $this->db->query( $SQLCmd) ;

		if ( isset( $query->rows[0])) {
			return $query->rows[0];
		} else {
			return false;
		}
	}

	/**
	 * [getOrder description]
	 * @param   array      $filterArr [description]
	 * @return  [type]                [description]
	 * @Another Angus
	 * @date    2018-09-07
	 */
	public function getOrderForCheckOrder( $filterArr = array()){
		foreach ($filterArr as $keyName => $value) {
			$filterArr[$keyName] = $this->db->escape( $value) ;
		}
		$SQLCmd = "	SELECT * FROM tb_order
						WHERE agreement_no = '{$filterArr['o1']}' AND rent_user_tel = '{$filterArr['m1']}'" ;
		$query = $this->db->query( $SQLCmd) ;

		if ( isset( $query->rows[0])) {
			return $query->rows[0];
		} else {
			return false;
		}
	}

	/**
	 * [getDiscountPercentage 回傳優惠百分比]
	 * @param   [type]     $rentStartDate [租車日期]
	 * @return  [type]                    [description]
	 * @Another Angus
	 * @date    2018-04-07
	 * @date    2020-07-07 調整為共用程序給不同function使用 這裡只需回傳百分比
	 *                     程式提至 function countCostPercentage
	 */
	public function getDiscountPercentage ( $rentStartDate = "", $create_date = "") {
		list($percent, $discountDay) = $this->countCostPercentage( $rentStartDate, $create_date) ;
		return $percent;
	}

	/**
	 * [getDiscountPercentage_days 回傳優惠百分比及天數]
	 * @param   [type]     $rentStartDate [租車日期]
	 * @return  [type]                    [description]
	 * @Another Angus
	 * @date    2020-07-07 調整為共用程序給不同function使用 回傳百分比及天數
	 */
	public function getDiscountPercentage_days ( $rentStartDate = "", $create_date = "") {
		list($percent, $discountDay) = $this->countCostPercentage( $rentStartDate, $create_date) ;
		return [$percent, $discountDay] ;
	}

	/**
	 * [countCostPercentage 回傳優惠百分比及天數]
	 * @param   [type]     $rentStartDate [租車日期]
	 * @return  [type]                    [description]
	 * @Another Angus
	 * @date    2020-07-07
	 */
	public function countCostPercentage( $rentStartDate = "", $create_date = "") {
		if ( !empty( $create_date)) {
			$create_date = substr($create_date, 0, 10);
			$today = strtotime( $create_date) ;
		} else {
			$today = strtotime( date( 'Y-m-d')) ;
		}

		$discountDay = ( strtotime( date('Y-m-d', strtotime( $rentStartDate . " - 0 day")) ) - $today) / 86400 ;

		if ( $discountDay >= 60) {
			$percent = 0.75 ;
		} else if ( $discountDay < 60 && $discountDay >= 45) {
			$percent = 0.85 ;
		} else if ( $discountDay < 45 && $discountDay >= 30) {
			$percent = 0.9 ;
		} else {
			$percent = 1 ;
		}
		return [$percent, $discountDay] ;
	}

	public function checkout_update ($order_id=''){
		$objLog = new log( 'ecpay3OrderReturnModel.log') ;
		$objLog->write( $this->request->get) ;
		$objLog->write( $this->request->post) ;
		$objLog->write( $this->session->data) ;


		$TradeStatus = $this->request->post['TradeStatus'];

		$SQLCmd = "	UPDATE tb_order SET checkout = '{$TradeStatus}'
					WHERE order_id = {$order_id}" ;
		if ($order_id!='') {
			$this->db->query($SQLCmd) ;
			return ture;
		}else{
			return false;
		}
	}

	/**
	 * [checkCustomerByPid 檢查顧客資料是否存在]
	 * @param   string     $customerPid [description]
	 * @return  [type]                  [description]
	 * @Another Angus
	 * @date    2018-04-14
	 */
	public function checkCustomerByPid( $customerPid = "" ) {
		$SQLCmd = "SELECT user_id FROM oc_customer WHERE upper(user_id)='{$customerPid}'" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;

		return $query->rows;
	}

	/**
	 * [checkEcReturnCode 取回交易狀態]
	 * @param   string     $orderId [description]
	 * @return  [type]              [description]
	 * @Another Angus
	 * @date    2018-05-11
	 */
	public function checkEcReturnCodeCmd( $orderId = "") {
		$SQLCmd = "SELECT RtnCode, RtnMsg FROM tb_ecpay_return WHERE orderID ='{$orderId}'" ;
		return $SQLCmd ;
	}

	public function checkEcReturnCode( $orderId = "") {
		$SQLCmd = "SELECT RtnCode, RtnMsg FROM tb_ecpay_return WHERE orderID ='{$orderId}'" ;
		$query = $this->db->query( $SQLCmd) ;
		// dump( $query) ;
		if ( isset( $query->rows[0])) {
			return $query->rows[0];
		} else {
			return false;
		}
	}

	public function newCustomer( $data = array()) {
		$now = date('Y-m-d H:i:s') ;
		// 新增客戶檔 oc_customer
		$data['email'] = isset( $data['rent_user_mail']) ? $data['rent_user_mail'] : "" ;
		$SQLCmd = "INSERT INTO oc_customer SET customer_group_id=1, store_id=0, language_id=0, status=1, approved=1,
			user_id='{$data['rent_user_id']}',
			firstname='{$data['rent_user_name']}',
			telephone='{$data['rent_user_tel']}',
			email='{$data['email']}',
			date_added='{$now}'" ;
		$query = $this->db->query( $SQLCmd) ;

		// 取回最後新增的customer_id
		$SQLCmd      = "SELECT LAST_INSERT_ID() customer_id" ;
		$query       = $this->db->query( $SQLCmd) ;
		$customer_id = $query->row['customer_id'] ;
		// $customer_id = $this->db->getLastId(); 原生寫法

		// 存客戶地址資訊
		$cityName = "";
		$postCode = ( !empty( $data['rent_zip2'])) ? $data['rent_zip2'] : $data['rent_zip1'] ;
		if($data['rent_zip1'] != ""){
			$SQLCmd   = "SELECT zip_name FROM sys_zip WHERE zip_code='{$data['rent_zip1']}'" ;
			$query    = $this->db->query( $SQLCmd) ;
			$cityName = $query->row['zip_name'] ;
		}

		$SQLCmd = "INSERT INTO oc_address SET customer_id='{$customer_id}',
			pid='{$data['rent_user_id']}',
			firstname='{$data['rent_user_name']}',
			address_1='{$data['rent_user_address']}',
			postcode='{$postCode}',
			city='$cityName' " ;
		$query    = $this->db->query( $SQLCmd) ;
	}

	/**
	 * [updateSmsStatus description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2018-04-13
	 */
	public function updateSmsStatus( $data = array()) {
		$now = date('Y-m-d H:i:s') ;
		// $data['batchID'] = "7aadd35d-c4cf-4c67-9c34-58d3205f5acb" ;
		$SQLCmd = "UPDATE tb_order SET batchID='{$data['batchID']}', updateUser='{$data['updateUser']}', updateDate='{$now}'
			where agreement_no='{$data['agreement_no']}'" ;
		$query = $this->db->query( $SQLCmd) ;
	}

	/**
	 * [delTb_scheduling description]
	 * @param   string     $order_no [description]
	 * @return  [type]               [description]
	 * @Another Angus
	 * @date    2018-05-12
	 */
	public function delTb_scheduling( $order_no = "") {
		$now = date('Y-m-d H:i:s') ;
		// $SQLCmd = "DELETE FROM tb_scheduling WHERE order_no='{$order_no}'" ;
		$SQLCmd = "UPDATE tb_scheduling set status=2, updateUser='Step4Kill', updateTime='{$now}' WHERE order_no='{$order_no}'" ;
		$query = $this->db->query( $SQLCmd) ;

		$SQLCmd = "UPDATE tb_order SET order_status='x', updateUser='Step4Kill', updateDate='{$now}' WHERE agreement_no='{$order_no}'" ;
		$query = $this->db->query( $SQLCmd) ;
	}

	public function getFilght( $order_no = "") {
		$SQLCmd = "select * from tb_flight where order_no='{$order_no}'" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		return $query->row;
	}

}