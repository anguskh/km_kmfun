<?php
class ModelCommonCronjob extends Model {
	/**
	 * [getOrderListCmd description]
	 * @param   integer    $checkTime [傳入 5-60 分鐘]
	 * @return  [type]                [description]
	 * @Another Angus
	 * @date    2019-09-23
	 */
	public function getOrderListCmd( $checkTime = 5) {
		$now = date( 'Y-m-d H:i:s') ;
		$SQLCmd = "SELECT * FROM tb_order WHERE order_type=9 AND order_status IS NULL
			AND createUser='webSite' AND create_date > '2018-06-25' AND batchID IS NULL
			AND create_date < DATE_SUB('{$now}', INTERVAL {$checkTime} MINUTE)
			ORDER BY agreement_no ASC" ;
		return $SQLCmd ;
	}

	/**
	 * [getOrderList description]
	 * @param   integer    $checkTime [傳入 5-60 分鐘]
	 * @return  [type]                [description]
	 * @Another Angus
	 * @date    2019-09-23
	 */
	public function getOrderList( $checkTime = 5) {
		$now = date( 'Y-m-d H:i:s') ;
		$SQLCmd = "SELECT * FROM tb_order WHERE order_type=9 AND order_status IS NULL
			AND createUser='webSite' AND create_date > '2018-06-25' AND batchID IS NULL
			AND create_date < DATE_SUB('{$now}', INTERVAL {$checkTime} MINUTE)
			ORDER BY agreement_no ASC" ;
		if ( WEBSITE_TYPE == "dev") {
			dump( $SQLCmd) ;
			// exit() ;
		}

		$query = $this->db->query( $SQLCmd) ;
		return $query->rows ;
	}

	public function check_tbEcpayReturnCmd( $orderIDArr = array()) {
		$inStr  = join(',', $orderIDArr) ;
		$SQLCmd = "SELECT * FROM tb_ecpay_return WHERE orderID in ({$inStr}) AND RtnCode='1'" ;
		return $SQLCmd ;
	}

	public function check_tbEcpayReturn( $orderIDArr = array()) {
		$inStr  = join(',', $orderIDArr) ;
		$retArr = array() ;
		if ( $inStr != '') {
			$SQLCmd = "SELECT * FROM tb_ecpay_return WHERE orderID in ({$inStr}) AND RtnCode='1'" ;
			$query = $this->db->query( $SQLCmd) ;
			$retArr = array() ;
			foreach ($query->rows as $iCnt => $row) {
				$retArr[$row['orderID']] = $row ;
			}
		}

		return $retArr ;
	}

	public function clean_tbScheduling( $order_no = "") {
		$now = date('Y-m-d H:i:s') ;
		// $SQLCmd = "DELETE FROM tb_scheduling WHERE order_no='{$order_no}'" ;
		$SQLCmd = "UPDATE tb_scheduling set status=2, updateUser='cronJobKill', updateTime='{$now}'  WHERE order_no='{$order_no}'" ;
		$query = $this->db->query( $SQLCmd) ;

		$SQLCmd = "UPDATE tb_order SET order_status='x', updateUser='cronJobKill', updateDate='{$now}' WHERE agreement_no='{$order_no}'" ;
		$query = $this->db->query( $SQLCmd) ;
	}
}
