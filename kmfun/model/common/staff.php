<?php
/**
 * [ModelCommonCustomer 會員中心]
 * @Another Angus
 * @date    2018-12-19
 */
class ModelCommonStaff extends Model {

	/**
	 * [checkStaff description]
	 * @param   array      $selArr [description]
	 * @return  [type]             [description]
	 * @Another Angus
	 * @date    2019-01-04
	 */
	public function checkStaff( $selArr = array()) {
		foreach ($selArr as $key => $value) {
			$selArr[$key] = $this->db->escape( $value) ;
		}
		if ( !empty( $selArr['acc']) && !empty( $selArr['pwd'])) {
			$SQLCmd = "SELECT*FROM oc_user WHERE username='{$selArr['acc']}' AND (PASSWORD=SHA1(CONCAT(salt,SHA1(CONCAT(salt,SHA1('{$selArr['pwd']}')))))) AND STATUS='1'" ;
			$query = $this->db->query( $SQLCmd) ;

			return $query->row ;
		}
		return array() ;
	}

	/**
	 * [getMinsuFromAjax description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2019-01-06
	 */
	public function getMinsuFromAjax ( $data = array()) {
		$whereStr = "" ;
		if ( isset( $data['filter_name']) && !empty( $data['filter_name'])) {
			$whereStr = "AND name like '%{$data['filter_name']}%'" ;
		}

		if ( !empty( $whereStr)) {
			$SQLCmd = "SELECT idx, name,
						CASE data_type
							WHEN '1' THEN '民宿'
							WHEN'2' THEN '業者'
						END AS data_type FROM tb_minshuku WHERE status = 1 {$whereStr} ORDER BY data_type ASC, name ASC" ;
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$SQLCmd .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}
			$query = $this->db->query( $SQLCmd) ;
			return $query->rows ;
		} else {
			return array() ;
		}
	}

}