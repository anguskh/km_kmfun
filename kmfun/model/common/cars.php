<?php
class ModelCommonCars extends Model {

	public function getCarsSeedArr( $rentDateArr = array()) {
		$notInCarIdx = array() ;
		// 開始時間在區間內的車
		$SQLCmd = "SELECT car_idx FROM tb_scheduling WHERE '{$rentDateArr[0]}' BETWEEN start_date AND end_date AND status=1" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		foreach ($query->rows as $key => $tmpArr) {
			$notInCarIdx[] = $tmpArr['car_idx'] ;
		}
		// 結束時間在區間內的車
		$SQLCmd = "SELECT car_idx FROM tb_scheduling WHERE '{$rentDateArr[1]}' BETWEEN start_date AND end_date AND status=1" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		foreach ($query->rows as $key => $tmpArr) {
			$notInCarIdx[] = $tmpArr['car_idx'] ;
		}

		// 因應出車整理需作業時間 調整租車預留兩小時的整理時間
		// add by Angus 2018.08.21
		// add by Angus 2018.12.10
		$rentStartTime = date('Y-m-d H:i',strtotime('-2 hour',strtotime($rentDateArr[0])));
		$rentEndTime   = date('Y-m-d H:i',strtotime('+2 hour',strtotime($rentDateArr[1])));
		// check 車輛在租車開始期間 是否有被租用
		$SQLCmd = "SELECT car_idx FROM tb_scheduling WHERE start_date BETWEEN '{$rentStartTime}' and '{$rentEndTime}' AND status=1" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		foreach ($query->rows as $key => $tmpArr) {
			$notInCarIdx[] = $tmpArr['car_idx'] ;
		}
		// check 車輛在租車結束期間 是否有被租用
		$SQLCmd = "SELECT car_idx FROM tb_scheduling WHERE end_date BETWEEN '{$rentStartTime}' and '{$rentEndTime}' AND status=1" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		foreach ($query->rows as $key => $tmpArr) {
			$notInCarIdx[] = $tmpArr['car_idx'] ;
		}

		$notInCarIdx = array_unique( $notInCarIdx) ;
		$notInStr = join( ',', $notInCarIdx) ;
		// dump( $notInStr) ;
		$sqlInStr = ( !empty($notInStr)) ? "AND idx NOT IN ( {$notInStr})" : "" ;

		$SQLCmd = "SELECT * FROM sys_car_model WHERE
					idx IN ( SELECT car_seed FROM sys_cars WHERE 1=1 {$sqlInStr} AND status = '1'
						GROUP by car_seed HAVING COUNT(car_seed) >= {$rentDateArr[2]}
					)" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;

		$retArr = array() ;
		foreach ($query->rows as $key => $tmpArr) {
			$retArr[ $tmpArr['car_model']][] = $tmpArr ;
		}

		return $retArr ;
	}

	public function getCarInfo( $filterData = array() ) {
		// dump( $filterData) ;
		$notInCarIdx = array() ;
		// 因應出車整理需作業時間 調整租車預留兩小時的整理時間
		// add by Angus 2019.01.05
		$filterData['start_date'] = date('Y-m-d H:i',strtotime('-2 hour',strtotime($filterData['start_date'])));
		$filterData['end_date']   = date('Y-m-d H:i',strtotime('+2 hour',strtotime($filterData['end_date'])));
		// 開始時間在區間內的車
		$SQLCmd = "SELECT car_idx FROM tb_scheduling WHERE car_seed={$filterData['car_seed']}
			AND '{$filterData['start_date']}' BETWEEN start_date AND end_date AND status=1" ;
		$query = $this->db->query( $SQLCmd) ;
		foreach ($query->rows as $key => $tmpArr) {
			$notInCarIdx[] = $tmpArr['car_idx'] ;
		}
		// 結束時間在區間內的車
		$SQLCmd = "SELECT car_idx FROM tb_scheduling WHERE car_seed={$filterData['car_seed']}
			AND '{$filterData['end_date']}' BETWEEN start_date AND end_date AND status=1" ;
		$query = $this->db->query( $SQLCmd) ;
		foreach ($query->rows as $key => $tmpArr) {
			$notInCarIdx[] = $tmpArr['car_idx'] ;
		}
		// check 車輛在租車開始期間 是否有被租用
		$SQLCmd = "SELECT car_idx FROM tb_scheduling WHERE car_seed={$filterData['car_seed']}
			AND start_date BETWEEN '{$filterData['start_date']}' and '{$filterData['end_date']}' AND status=1" ;
		$query = $this->db->query( $SQLCmd) ;
		foreach ($query->rows as $key => $tmpArr) {
			$notInCarIdx[] = $tmpArr['car_idx'] ;
		}
		// check 車輛在租車結束期間 是否有被租用
		$SQLCmd = "SELECT car_idx FROM tb_scheduling WHERE car_seed={$filterData['car_seed']}
			AND end_date BETWEEN '{$filterData['start_date']}' and '{$filterData['end_date']}' AND status=1" ;
		$query = $this->db->query( $SQLCmd) ;
		foreach ($query->rows as $key => $tmpArr) {
			$notInCarIdx[] = $tmpArr['car_idx'] ;
		}

		$notInCarIdx = array_unique( $notInCarIdx) ;
		$notInStr = join( ',', $notInCarIdx) ;

		$sqlInStr = ( !empty($notInStr)) ? "AND idx NOT IN ( {$notInStr})" : "" ;

		$SQLCmd = "SELECT * FROM sys_cars WHERE car_seed = {$filterData['car_seed']} {$sqlInStr} AND status = '1' LIMIT {$filterData['car_limit']}" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;

		return $query->rows ;
	}

	public function getCarSeedSN( $carSN = "") {
		$whereStr = !empty( $carSN) ? " WHERE idx={$carSN}" : "" ;

		$SQLCmd = "SELECT * FROM sys_cars {$whereStr}" ;
		$query = $this->db->query( $SQLCmd) ;
		// dump( $SQLCmd) ;
		return $query->rows[0]['car_seed'] ;
	}

	public function getCarSeed( $carIdx = "") {
		$whereStr = !empty( $carIdx) ? " WHERE idx={$carIdx}" : "" ;

		$SQLCmd = "SELECT * FROM sys_car_model {$whereStr}" ;
		$query = $this->db->query( $SQLCmd) ;
		// dump( $SQLCmd) ;
		return $query->rows[0] ;
	}

	public function getCarSeedAll() {
		$SQLCmd = "SELECT * FROM sys_option WHERE parent = '1' AND status = '1' ORDER BY opt_seq ASC" ;
		$rs_option = $this->db->query($SQLCmd)->rows;
		$rs = array();

		foreach ($rs_option as $key => $value) {
			$car_model = $value['idx'];
			$SQLCmd = "SELECT * FROM sys_car_model WHERE car_model = '{$car_model}' AND status='1'" ;
			// dump( $SQLCmd) ;
			$rs_cars = $this->db->query($SQLCmd)->rows;
			$rs[$key]['car_seed'] = $rs_cars;
			$rs[$key]['car_model'] = $car_model;
		}

		return $rs ;
	}

	public function getCarSeedInfo() {
		$SQLCmd = "SELECT * FROM sys_car_model" ;
		$tmpArr = $this->db->query($SQLCmd)->rows ;
		foreach ($tmpArr as $iCnt => $row) {
			$retArr[$row['idx']] = $row ;
		}

		return $retArr ;
	}

	public function getCarSeedName( $carIdx) {
		$whereStr = !empty( $carIdx) ? " WHERE idx={$carIdx}" : "" ;
		$SQLCmd = "SELECT * FROM sys_car_model {$whereStr}" ;
		$query = $this->db->query( $SQLCmd) ;
		return $query->rows ;
	}

	public function getCarModel() {
		$SQLCmd = "SELECT so.idx, so.opt_name, scm.idx AS car_id, scm.car_seed AS car_name
			FROM sys_option so LEFT JOIN sys_car_model scm ON so.idx=scm.car_model
			WHERE so.parent=1 AND so.status=1 AND scm.status=1
			ORDER BY so.opt_seq ASC" ;
			// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		return $query->rows ;
	}

	/**
	 * [getCarCntByModel description]
	 * @param   array      $filterData [description]
	 * @return  [type]                 [description]
	 * @Another Angus
	 * @date    2018-08-25
	 */
	public function getCarCntByModel( $filterData = array()) {
		// dump( $filterData) ;
		$notInStr = $this->getUsedCars( $filterData) ;
		// dump( $notInStr) ;
		if ( $filterData['2'] != 0) {
			$filterStr = " AND car_seed={$filterData['2']}" ;
		} else {
			$filterStr = "" ;
		}
		$SQLCmd = "SELECT car_seed, count(car_seed) cnt FROM sys_cars
			WHERE idx NOT IN ({$notInStr}) AND status = '1' {$filterStr} group by car_seed" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		$retArr = array() ;
		foreach ($query->rows as $key => $tmpArr) {
			$retArr[$tmpArr['car_seed']] = $tmpArr['cnt'] ;
		}

		return $retArr ;
	}

	/**
	 * [getUsedCars description]
	 * @param   array      $filterData [description]
	 * @return  [type]                 [description]
	 * @Another Angus
	 * @date    2018-08-25
	 */
	public function getUsedCars( $filterData = array()) {
		$notInCarIdx = array() ;
		// 因應出車整理需作業時間 調整租車預留兩小時的整理時間 2019.01.06
		$filterData[0] = date('Y-m-d H:i',strtotime('-2 hour',strtotime($filterData[0])));
		$filterData[1] = date('Y-m-d H:i',strtotime('+2 hour',strtotime($filterData[1])));
		// 開始時間在區間內的車
		$SQLCmd = "SELECT car_idx FROM tb_scheduling WHERE '{$filterData[0]}' BETWEEN start_date AND end_date AND status=1" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		foreach ($query->rows as $key => $tmpArr) {
			$notInCarIdx[] = $tmpArr['car_idx'] ;
		}
		// 結束時間在區間內的車
		$SQLCmd = "SELECT car_idx FROM tb_scheduling WHERE '{$filterData[1]}' BETWEEN start_date AND end_date AND status=1" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		foreach ($query->rows as $key => $tmpArr) {
			$notInCarIdx[] = $tmpArr['car_idx'] ;
		}

		// check 車輛在租車開始期間 是否有被租用
		$SQLCmd = "SELECT car_idx FROM tb_scheduling WHERE start_date BETWEEN '{$filterData[0]}' and '{$filterData[1]}' AND status=1" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		foreach ($query->rows as $key => $tmpArr) {
			$notInCarIdx[] = $tmpArr['car_idx'] ;
		}
		// check 車輛在租車結束期間 是否有被租用
		$SQLCmd = "SELECT car_idx FROM tb_scheduling WHERE end_date BETWEEN '{$filterData[0]}' and '{$filterData[1]}' AND status=1" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		foreach ($query->rows as $key => $tmpArr) {
			$notInCarIdx[] = $tmpArr['car_idx'] ;
		}

		$notInCarIdx = array_unique( $notInCarIdx) ;
		$notInStr = join( ',', $notInCarIdx) ;
		return $notInStr ;
	}

	//檢查car_no的顏色
	public function getCarNoColor($whereStr = "") {
		$SQLCmd = "SELECT idx FROM sys_cars WHERE 1=1 {$whereStr}" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		return $query->rows ;
	}

	public function getCarInfoColor( $whereStr) {
		$SQLCmd = "SELECT a.idx, b.idx AS car_seed_idx, b.car_model, b.car_seed, a.car_no, b.car_price, a.car_color FROM sys_cars a LEFT JOIN sys_car_model b ON a.car_seed=b.idx
				WHERE {$whereStr} ORDER BY car_no ASC" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		return $query->row ;
	}
	public function getCarCondition( $a_no) {
		$SQLCmd = "SELECT tc.sign_out, tc.sign_in, tc.create_man, tc.return_man,				concat(ou.lastname,ou.firstname ) as create_name, 
		concat(ou2.lastname,ou2.firstname ) as return_name, create_date, return_date
					from tb_car_condition tc
					left join oc_user ou ON ou.username = tc.create_man
					left join oc_user ou2 ON ou2.username = tc.return_man
				WHERE tc.agreement_no = '{$a_no}' limit 1" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		
		$sign_out = "";
		$sign_in = "";
		$create_man = "";
		$return_man = "";
		$create_date = "";
		$return_date = "";
		foreach ($query->rows as $key => $tmpArr) {
			$sign_out = $tmpArr['sign_out'];
			$sign_in = $tmpArr['sign_in'];
			$create_man = (($tmpArr['create_name']!='')?$tmpArr['create_name']:'');
			$return_man = (($tmpArr['return_name']!='')?$tmpArr['return_name']:'');
			$create_date = $tmpArr['create_date'];
			$return_date = $tmpArr['return_date'];
		}
		$return_arr = array();
		$return_arr[0] = $sign_out;
		$return_arr[1] = $sign_in;
		$return_arr[2] = $create_man;
		$return_arr[3] = $return_man;
		$return_arr[4] = $create_date;
		$return_arr[5] = $return_date;
		return $return_arr;
	}
	public function getFlightInfo( $order_no = "") {

		$SQLCmd = "SELECT tf.*, so.opt_name airline_name FROM tb_flight tf
					LEFT JOIN sys_option so ON (so.idx = tf.airline AND so.parent = '125') OR (so.idx = tf.pier AND so.parent = '131')
					WHERE order_no='{$order_no}'" ;
 		$query = $this->db->query( $SQLCmd) ;
 		// dump( $SQLCmd) ;
 		return $query->row ;
	}

}