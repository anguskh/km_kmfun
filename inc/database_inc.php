<?php
class database {
	private $connection;

	public function __construct($hostname, $username, $password, $database, $port = '3306') {
		$this->connection = new \mysqli($hostname, $username, $password, $database, $port);

		if ($this->connection->connect_error) {
			throw new \Exception('Error: ' . $this->connection->error . '<br />Error No: ' . $this->connection->errno);
		}

		$this->connection->set_charset("utf8");
		$this->connection->query("SET SQL_MODE = ''");
	}
    
    public function getData($sql){
        $result = $this->query($sql);
        if(!count($result->row)){
            return false;
        }
        return $result->row;
    }
    
    public function getArrayData($sql){
        $result = $this->query($sql);
        if(!count($result->rows)){
            return false;
        }
        return $result->rows;
    }
    
    public function getNumRows($sql){
        $result = $this->query($sql);
        if(!isset($result->num_rows)){
            return 0;
        }
        return $result->num_rows;
    }
    
    public function insertDB($sql){
        $result = $this->query($sql);
        if($result!==false){
            return $this->getLastId();
        }
        
        return false;
    }
    
    public function updateDB($sql){
        $result = $this->query($sql);
        if($result!==false){
            return true;
        }
        
        return false;
    }

	public function query($sql) {
		$query = $this->connection->query($sql);

		if (!$this->connection->errno) {
			if ($query instanceof \mysqli_result) {
				$data = array();

				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}

				$result = new \stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;

				$query->close();

				return $result;
			} else {
				return true;
			}
		} else {
			throw new \Exception('Error: ' . $this->connection->error  . '<br />Error No: ' . $this->connection->errno . '<br />' . $sql);
		}
	}

	public function escape($value) {
		return $this->connection->real_escape_string($value);
	}
	
	public function countAffected() {
		return $this->connection->affected_rows;
	}

	public function getLastId() {
		return $this->connection->insert_id;
	}
	
	public function connected() {
		return $this->connection->ping();
	}
	
	public function __destruct() {
		$this->connection->close();
	}
}