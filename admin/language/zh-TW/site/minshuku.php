<?php
// Heading 標題
$_['heading_title']     = '民宿管理';

// Text 文字
$_['text_list']         = '民宿清單';
$_['text_add']          = '新增功能';
$_['text_edit']         = '編輯功能';
$_['text_copy']         = '複製功能';
$_['text_delete']       = '刪除功能';

$_['text_success']      = '成功：您已經修改民宿資訊！';

// Column 欄位
$_['column_del_func']		= '刪除功能' ;
$_['column_new_func_desc']	= '功能名稱' ;
$_['column_new_func_path']	= '功能位置' ;
$_['column_new_func']		= '類別名稱' ;

$_['column_map']			= '地圖';
$_['column_action']			= '管理';


// Entry 條目
$_['entry_name']            = '民宿名稱';
$_['entry_pos1']            = '地區';
$_['entry_room_type']       = '房型';
$_['entry_address']         = '地址';
$_['entry_title']			= '頁面標題：';
$_['entry_description']		= '頁面內容：';
$_['entry_store']			= '商店：';
$_['entry_meta_title'] 		= 'Meta 標籤標題：';
$_['entry_meta_keyword'] 	= 'Meta 標籤關鍵字：';
$_['entry_meta_description'] = 'Meta 標籤說明：';
$_['entry_status']			= '頁面狀態：';
$_['entry_online_date']		= '上線日期：';


// Help
$_['help_code']         = '啟用購物禮券所需代碼。';

// Error 錯誤訊息
$_['error_warning']			= '警告：資料未正確輸入！';
$_['error_permission']		= '警告：您沒有權限更改民宿資訊頁面！';
$_['error_name']			= '民宿名稱 : 資料未正確輸入！';
$_['error_pos']				= '縣別 : 資料未正確輸入！';
$_['error_pos1']			= '地區 : 資料未正確輸入！';
$_['error_room_type']		= '房型 : 資料未正確輸入！';
$_['error_room_cnt']		= '房間數 : 資料未正確輸入！';
$_['error_price']			= '價位 : 資料未正確輸入！';
$_['error_tel']				= '電話 : 資料未正確輸入！';
$_['error_email']			= 'email : 資料未正確輸入！';
