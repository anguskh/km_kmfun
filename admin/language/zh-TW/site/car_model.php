<?php
// Heading 標題
$_['heading_title']     = '車種管理';

// Text 文字
$_['text_list']         = '車種清單';
$_['text_add']          = '新增車種';
$_['text_edit']         = '編輯車種';
$_['text_copy']         = '複製車種';
$_['text_delete']       = '刪除車種';

$_['text_success']      = '成功：您已經修改車種資訊！';

// Column 欄位
$_['column_del_func']		= '刪除功能' ;
$_['column_new_func_desc']	= '功能名稱' ;
$_['column_new_func_path']	= '功能位置' ;
$_['column_new_func']		= '類別名稱' ;

$_['column_action']			= '管理';


// Entry 條目
// $_['entry_title']			= '頁面標題：';
$_['entry_status']			= '車種狀態：';


// Help
$_['help_code']         = '啟用購物禮券所需代碼。';

// Error 錯誤訊息
$_['error_warning']			= '警告：資料未正確輸入！';
$_['error_permission']		= '警告：您沒有權限更改車種管理資訊頁面！';
$_['error_title']			= '頁面標題必須在3至64個字之間！';
$_['error_description']		= '頁面內容長度不得少於3個字！';
$_['error_meta_title']		= 'Meta 標題必須在3至255個字之間！';

$_['error_car_seed']		= '車型為必選欄位';
$_['error_car_seed_1']		= '車型名稱重複';
$_['error_car_model']		= '車種為必選欄位';
$_['error_car_year']		= '年份必需是數字';
$_['error_car_price']		= '金額必需是數字';

// $_['error_account']		= '警告：此頁面不能被刪除，因為它是目前為預設的商店會員條款！';
// $_['error_checkout']		= '警告：此頁面不能被刪除，因為它是目前為預設的商店結帳條款！';
// $_['error_affiliate']   	= '警告：此頁面不能被刪除，因為它是目前為預設的商店推薦條款！';
// $_['error_return']		= '警告：此頁面不能被刪除，因為它是目前為預設的商店退換條款！';
// $_['error_store']		= '警告：此頁面不能被刪除，因為目前已有 %s 家商店使用中！';
