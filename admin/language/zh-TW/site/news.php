<?php
// Heading 標題
$_['heading_title']     = '最新消息管理';

// Text 文字
$_['text_list']         = '最新消息清單';
$_['text_add']          = '新增功能';
$_['text_edit']         = '編輯功能';
$_['text_copy']         = '複製功能';
$_['text_delete']       = '刪除功能';

$_['text_success']      = '成功：您已經修改消息區資訊！';

// Column 欄位
$_['column_action']			= '管理';
$_['column_title']			= '頁面標題';
$_['column_status']			= '頁面狀態';
$_['column_online_date']	= '上線日期';
$_['column_c_date']			= '建立日期';


// Entry 條目
$_['entry_title']			= '頁面標題：';
$_['entry_description']		= '頁面內容：';
$_['entry_store']			= '商店：';
$_['entry_meta_title'] 		= 'Meta 標籤標題：';
$_['entry_meta_keyword'] 	= 'Meta 標籤關鍵字：';
$_['entry_meta_description'] = 'Meta 標籤說明：';
$_['entry_status']			= '頁面狀態：';
$_['entry_online_date']		= '上架日期：';


// Help
$_['help_code']         = '啟用購物禮券所需代碼。';

// Error 錯誤訊息
$_['error_warning']			= '警告：資料未正確輸入！';
$_['error_permission']		= '警告：您沒有權限更改商店資訊頁面！';
$_['error_title']			= '頁面標題必須在3至64個字之間！';
$_['error_description']		= '頁面內容長度不得少於3個字！';
$_['error_online_date']		= '上線日期未填寫';
$_['error_meta_title']		= 'Meta 標題必須在3至255個字之間！';
// $_['error_account']		= '警告：此頁面不能被刪除，因為它是目前為預設的商店會員條款！';
// $_['error_checkout']		= '警告：此頁面不能被刪除，因為它是目前為預設的商店結帳條款！';
// $_['error_affiliate']   	= '警告：此頁面不能被刪除，因為它是目前為預設的商店推薦條款！';
// $_['error_return']		= '警告：此頁面不能被刪除，因為它是目前為預設的商店退換條款！';
// $_['error_store']		= '警告：此頁面不能被刪除，因為目前已有 %s 家商店使用中！';
