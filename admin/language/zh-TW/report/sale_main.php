<?php
// Heading
$_['heading_title']			= '主單';
$_['heading_title_today']   = '每日結帳人員';

// Text
$_['text_list']         = '銷售主單';
$_['text_year']         = '年';
$_['text_month']        = '月';
$_['text_week']         = '週';
$_['text_day']          = '日';
$_['text_all_status']   = '所有狀態';

// Column 主單欄位名稱
$_['column_order_type']        = '項目';
$_['column_car_no']            = '車號';
$_['column_car_model']         = '車型';
$_['column_car_type']          = '種類';
$_['column_rent_user_name']    = '承租人';
$_['column_rent_user_id']      = '身份証';
$_['column_rent_company_name'] = '公司';
$_['column_rent_company_no']   = '統編';
$_['column_rent_user_born']    = '出生日期';
$_['column_rent_user_tel']     = '電話';
$_['column_agreement_no']      = '訂單編號';
$_['column_rent_user_id']      = '身份証';

$_['column_s_station']         = '出車地點';
$_['column_e_station']         = '還車地點';

$_['column_rent_out_date']     = '預計出車日期';
$_['column_rent_out_time']     = '預計出車時間';
$_['column_rent_in_date']      = '預計還車日期';
$_['column_rent_in_time']      = '預計還車時間';
$_['column_real_out_date']     = '實際出車日期';
$_['column_real_out_time']     = '實際出車時間';
$_['column_real_in_date']      = '實際還車日期';
$_['column_real_in_time']      = '實際還車時間';
$_['column_total']             = '金額';		// 現收費用
$_['column_minsu']             = '業者名稱'; // 退佣單位
$_['column_create_man']        = '租車業務';
$_['column_return_man']        = '還車業務';
$_['column_income']            = '公司淨收入';
$_['column_total_day']         = '天數';
$_['column_members']           = '人數';

// 2020.04.07 調整成月報表欄位
$_['column_pay_type']      = '付款方式';
$_['column_plug_cost']     = '配件費用';

$_['column_lose_type']     = '回程付款方式';
$_['column_oil_cost']      = '油費';
$_['column_overTime_cost'] = '逾時費用';
$_['column_business_lose'] = '營業損失';
$_['column_car_lose']      = '車體損失';
$_['column_other_lose']    = '其他損失';
$_['column_spdc_cost']     = '業務折扣';
$_['column_memo']          = '備註';




// Entry
$_['entry_date_start']  = '開始日期';
$_['entry_date_end']    = '結束日期';
$_['entry_group']       = '顯示分組';
$_['entry_status']      = '訂單狀態';
