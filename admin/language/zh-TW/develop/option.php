<?php
// Heading 標題
$_['heading_title']     = '功能產生器';

// Text 文字
$_['text_list']         = '功能清單';
$_['text_add']          = '新增功能';
$_['text_edit']         = '編輯功能';
$_['text_copy']         = '複製功能';

$_['text_sent']         = '成功： 購物禮券郵件已發送！';
$_['text_success']      = '成功：您已經修改購物禮券！';

// Column 欄位
$_['column_del_func']		= '刪除功能' ;
$_['column_new_func_desc']	= '功能名稱' ;
$_['column_new_func_path']	= '功能位置' ;
$_['column_new_func']		= '類別名稱' ;


$_['column_name']       = '購物禮券名稱';
$_['column_code']       = '購物禮券代碼';
$_['column_from']       = '發送人';
$_['column_to']         = '收券人';
$_['column_theme']      = '主題';
$_['column_amount']     = '金額';
$_['column_status']     = '狀態';
$_['column_order_id']   = '訂單ID';
$_['column_customer']   = '客戶';
$_['column_date_added'] = '新增日期';
$_['column_action']     = '管理';

// Entry 條目
$_['entry_del_func']		= '刪除功能路徑';
$_['entry_new_func_desc']   = '功能名稱';
$_['entry_new_func_path']   = '功能位置';
$_['entry_new_func']		= '類別名稱';



$_['entry_code']        = '購物禮券代碼';
$_['entry_from_name']   = '發送人';
$_['entry_from_email']  = '發送人 E-Mail';
$_['entry_to_name']     = '收券人';
$_['entry_to_email']    = '收券人 E-Mail';
$_['entry_theme']       = '主題';
$_['entry_message']     = '內容';
$_['entry_amount']      = '金額';
$_['entry_status']      = '狀態';

// Help
$_['help_code']         = '啟用購物禮券所需代碼。';

// Error 錯誤訊息
$_['error_exists']      = '警告：功能名稱已經使用！';

$_['error_selection']   = '警告：您沒有選擇購物禮券！';
$_['error_permission']  = '警告：您沒有權限使用該功能！';
$_['error_exists']      = '警告：購物禮券代碼已經使用！';
$_['error_code']        = '代碼必須在 3 至 10 個字之間！';
$_['error_to_name']     = '收券人名稱必須在 1 至 64 個字之間！';
$_['error_from_name']   = '您的名稱必須在 1 至 64 個字之間！';
$_['error_email']       = '無效的郵件地址！';
$_['error_amount']      = '總計金額必須大於等於1！';
$_['error_order']       = '警告：此購物禮券不能被刪除，因為它是屬於<a href="%s">訂單</a>的一部分！';
