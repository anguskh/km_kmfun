<?php
// Heading
$_['heading_title']         = '自動新增資料';

// Text
$_['text_success']          = '成功: 您已新增資料!';
$_['text_list']             = '新增清單';//

// Column
$_['column_field']          = '欄位名稱';//
$_['column_type']           = '欄位屬性';//
$_['column_customize']      = '自訂參數';

// Entry
$_['entry_tables']          = '資料表';

// Help

// Error
$_['error_warning']         = '警告: 請確實檢查填寫內容!';
$_['error_permission']      = '警告: 您沒有權限修改客戶資料!';
$_['error_exists']          = '警告: E-Mail 信箱已註冊過!';
$_['error_firstname']       = '名字長度至少要1到32個字!';
$_['error_lastname']        = '姓氏長度至少要1到32個字!';
$_['error_email']           = '請輸入正確的 E-Mail 信箱!';
$_['error_telephone']       = '電話長度至少要1到32個字!';
$_['error_password']        = '密碼長度至少要4到20個字!';
$_['error_confirm']         = '密碼和確認密碼不相符!';
$_['error_address_1']       = '地址長度至少要3到128個字!';
$_['error_city']            = '鄉鎮市區長度至少要2到128個字!';
$_['error_postcode']        = '郵遞區號長度至少要2到10個字!';
$_['error_country']         = '請選擇國家!';
$_['error_zone']            = '請選擇縣市!';
$_['error_custom_field']    = '%s 必須輸入!';
$_['error_custom_field_validate']  = '%s 內容不正確!';
$_['error_quantity']        = '警告: 請輸入數字!';