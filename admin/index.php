<?php
error_reporting(E_ALL) ;
ini_set('display_errors','On') ;
date_default_timezone_set("Asia/Taipei"); //設定台北時間
// Version
define('VERSION', '2.3.0.2');

// 記錄網頁執行路徑 add by Angus 2017.03.23
$logFileName = "/var/www/html/kmfun/system/storage/logs/db.log" ;
// $logFileName = "D:/xampp/htdocs/kmfun/system/storage/logs/db.log" ;
$fp = fopen($logFileName , "a" ) ;
	$nowTime = date("Y/m/d H:i:s");
	fwrite($fp,"{$nowTime} | -----{$_SERVER["REQUEST_URI"]}----- \r\n") ;
fclose( $fp) ;

// Configuration
if (is_file('config.php')) {
	require_once('config.php');
}

// Install
if (!defined('DIR_APPLICATION')) {
	header('Location: ../install/');
	exit;
}

// Startup
// /Applications/MAMP/htdocs/oc2302/system/startup.php
require_once(DIR_SYSTEM . 'startup.php');

start('admin');