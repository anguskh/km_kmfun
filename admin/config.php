<?php
define('SERVER_ADDRESS', $_SERVER['HTTP_HOST']) ;
define('PROJECT_SITE', 'kmfun') ;

// HTTP
define('HTTP_SERVER',	'http://'.SERVER_ADDRESS.'/'.PROJECT_SITE.'/admin/');
define('HTTP_CATALOG',	'http://'.SERVER_ADDRESS.'/'.PROJECT_SITE.'/');

// HTTPS
define('HTTPS_SERVER',	'http://'.SERVER_ADDRESS.'/'.PROJECT_SITE.'/admin/');
define('HTTPS_CATALOG',	'http://'.SERVER_ADDRESS.'/'.PROJECT_SITE.'/');

// Default Folder add by Angus 2017.02.28
define('DIR_INITIAL',	'/var/www/html/'.PROJECT_SITE) ;

// DIR
define('DIR_APPLICATION',	DIR_INITIAL.'/admin/');
define('DIR_SYSTEM',		DIR_INITIAL.'/system/');
define('DIR_IMAGE',			DIR_INITIAL.'/image/');
define('DIR_LANGUAGE',		DIR_INITIAL.'/admin/language/');
define('DIR_TEMPLATE',		DIR_INITIAL.'/admin/view/template/');
define('DIR_CONFIG',		DIR_INITIAL.'/system/config/');
define('DIR_CACHE',			DIR_INITIAL.'/system/storage/cache/');
define('DIR_DOWNLOAD',		DIR_INITIAL.'/system/storage/download/');
define('DIR_LOGS',			DIR_INITIAL.'/system/storage/logs/');
define('DIR_MODIFICATION',	DIR_INITIAL.'/system/storage/modification/');
define('DIR_UPLOAD',		DIR_INITIAL.'/system/storage/upload/');
define('DIR_CATALOG',		DIR_INITIAL.'/catalog/');

// DB Angus Home
// define('DB_DRIVER',		'mysqli');
// define('DB_HOSTNAME',	'anguskh.com');
// define('DB_USERNAME',	'kmfun');
// define('DB_PASSWORD',	'kmfun123');
// define('DB_DATABASE',	'kmfun_dev');
// define('DB_PORT',		'35678');
// define('DB_PREFIX',		'oc_');

// Pro Server
// define('DB_DRIVER',		'mysqli');
// define('DB_HOSTNAME',	'210.65.47.82');
// define('DB_USERNAME',   'kmfun_prod');
// define('DB_PASSWORD',   'kiZsEmrzMAX5Tdjk');
// define('DB_DATABASE',   'kmfun');
// define('DB_PORT',		'3306');
// define('DB_PREFIX',		'oc_');

// Dev Server
define('DB_DRIVER',		'mysqli');
define('DB_HOSTNAME',	'210.65.47.82');
define('DB_USERNAME',	'devkmfun');
define('DB_PASSWORD',	'develop4!kmfun');
define('DB_DATABASE',	'kmfun_dev');
define('DB_PORT',		'3306');
define('DB_PREFIX',		'oc_');


// Debug Mod add by Angus 2017.03.05
define('DEBUG_MOD', true) ;
// Debug Mod add by Angus 2017.03.20
define('DB_DEBUG_MOD', true) ;