<?php
class ModelSaleOrder extends Model {
	/**
	 * [deleteOrder description]
	 * @param   [type]     $order_id [description]
	 * @return  [type]               [description]
	 * @Another Angus
	 * @date    2018-01-23
	 */
	public function deleteOrder( $order_id) {
		$now = date('Y-m-d H:i:s') ;
		$SQLCmd = "INSERT INTO tb_order_bak SELECT * FROM tb_order WHERE order_id='{$order_id}'" ;
		$this->db->query( $SQLCmd) ;

		// $this->db->query( "UPDATE tb_order SET order_status = null WHERE order_id='{$order_id}'") ;
		$this->db->query( "UPDATE tb_order SET order_status='x', updateUser='{$this->session->data['user_name']}', updateDate='{$now}' WHERE order_id='{$order_id}'") ;
		$ret = $this->db->query( "SELECT agreement_no, other_car, car_sn FROM tb_order WHERE order_id='{$order_id}'") ;
		$inCars   = array() ;
		$inCars[] = $ret->row['car_sn'] ;
		if ( !empty($ret->row['other_car'])) {
			// dump( array( $ret->row['agreement_no'], unserialize( $ret->row['other_car']))) ;
			$carsArr = unserialize( $ret->row['other_car']) ;
			foreach ($carsArr as $iCnt => $carInfo) {
				$inCars[] = $carInfo['idx'] ;
			}
		}
		$inCarStr = join( ',', $inCars) ;
		$SQLCmd = "UPDATE tb_scheduling SET status=2, updateUser='{$this->session->data['user_name']}', updateTime='{$now}'  WHERE order_no='{$ret->row['agreement_no']}' AND car_idx in ({$inCarStr})" ;
		// $SQLCmd = "UPDATE tb_scheduling SET status=1 WHERE order_no='{$ret->row['agreement_no']}' AND car_idx in ({$inCarStr})" ;
		$this->db->query( $SQLCmd) ;
	}

	// rework by Angus 2017.07.08
	public function getOrder($order_id) {
		$SQLCmd = "SELECT * FROM tb_order WHERE order_id = '{$this->db->escape($order_id)}' ORDER BY create_date DESC" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query($SQLCmd);

		return $query->row;
	}

	/**
	 * [getOrderIDUseAgreement_no 用訂單編號取回訂單流水號] 用於車輛調度表 功能
	 * @param   [type]     $agreement_no [訂單編號]
	 * @return  [type]                   [訂單流水號]
	 * @Another Angus
	 * @date    2017-08-05
	 */
	public function getOrderIDUseAgreement_no( $agreement_no) {
		$SQLCmd = "SELECT order_id FROM tb_order WHERE agreement_no = '{$this->db->escape($agreement_no)}' ORDER BY create_date DESC" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query($SQLCmd);

		return $query->row;
	}

	/**
	 * [getOrderInfoByAgreement_no 用訂單編號取回訂單資訊]
	 * @param   string     $agreement_no [description]
	 * @return  [type]                   [description]
	 * @Another Angus
	 * @date    2017-12-12
	 */
	public function getOrderInfoByAgreement_no( $agreement_no = "") {
		$SQLCmd = "SELECT * FROM tb_order WHERE agreement_no = '{$this->db->escape($agreement_no)}' ORDER BY create_date DESC" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query($SQLCmd);

		return $query->row;
	}

	// rework by Angus 2017.07.02
	public function getOrders($data = array()) {
		// dump( $data) ;
		$SQLCmd = "SELECT * FROM tb_order" ;

		$SQLCmd .= " WHERE 1=1" ;

		// if ( !isset( $status)) {
		// 	$SQLCmd .= " AND order_status IS NULL" ;
		// }

		if (!empty($data['filter_order_id'])) {
			if ( strpos( $data['filter_order_id'], ',') > 0) {
				$tmp = explode(',', $data['filter_order_id']) ;
				foreach ($tmp as $key => &$value) {
					$value = $this->db->escape( $value) ;
				}
				$joinStr = "'".join( "','", $tmp)."'" ;
				$SQLCmd .= " AND agreement_no IN ({$joinStr})" ;
			} else {
				$SQLCmd .= " AND agreement_no like '{$this->db->escape( $data['filter_order_id'])}%'" ;
			}
		}
		if (!empty($data['filter_order_type'])) {
			$SQLCmd .= " AND order_type = '{$this->db->escape( $data['filter_order_type'])}'" ;
		}
		if (!empty($data['filter_order_status'])) {
			if ( $data['filter_order_status'] == 'x') {
				$SQLCmd .= " AND order_status = '{$this->db->escape( $data['filter_order_status'])}'" ;
			} else if ( $data['filter_order_status'] == "success"){
				$SQLCmd .= " AND (order_status IS NULL OR order_status IN (1,2))" ;
			}
		}
		if (!empty($data['filter_customer'])) {
			$SQLCmd .= " AND rent_user_name like '%{$this->db->escape( $data['filter_customer'])}%'" ;
		}
		if (!empty($data['filter_mobile'])) {
			$SQLCmd .= " AND rent_user_tel like '%{$this->db->escape( $data['filter_mobile'])}%'" ;
		}
		if (!empty($data['filter_station'])) {
			$SQLCmd .= " AND s_station = '{$this->db->escape( $data['filter_station'])}'" ;
		}
		if (!empty($data['filter_date_out'])) {
			$SQLCmd .= " AND DATE_FORMAT(rent_date_out,'%Y-%m-%d') = '{$this->db->escape( $data['filter_date_out'])}'" ;
		}
		if (!empty($data['filter_date_out2'])) {
			$SQLCmd .= " AND DATE_FORMAT(rent_date_in_plan,'%Y-%m-%d') = '{$this->db->escape( $data['filter_date_out2'])}'" ;
		}

		if (!empty($data['filter_date_tout'])) {
			$SQLCmd .= " AND DATE_FORMAT(rent_date_out_true,'%Y-%m-%d') = '{$this->db->escape( $data['filter_date_tout'])}'" ;
		}
		if (!empty($data['filter_date_tout2'])) {
			$SQLCmd .= " AND DATE_FORMAT(rent_date_in,'%Y-%m-%d') = '{$this->db->escape( $data['filter_date_tout2'])}'" ;
		}


		// make sort
		$sort_data = array(
			'order_type',
			'agreement_no',
			'checkout',
			'rent_user_name',
			'rent_user_tel',
			'car_model',
			'car_no',
			'rent_date_out',
			'rent_date_in_plan',
			'rent_date_out_true',
			'rent_date_in',
			's_station',
			'e_station',
			'total',
			'create_date',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$SQLCmd .= " ORDER BY " . $data['sort'];
		} else {
			$SQLCmd .= " ORDER BY create_date" ;
		}
		// make order key
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$SQLCmd .= " DESC";
		} else {
			$SQLCmd .= " ASC";
		}
		// make limit range
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$SQLCmd .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		// dump( $SQLCmd) ;
		$query = $this->db->query($SQLCmd);
		return $query->rows;
	}

	// add by Angus 2017-12-23
	/**
	 * [getOrdersForScheduling description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2017-12-23
	 */
	public function getOrdersFromScheduling( $data = array()) {
		$inStr = '\''. join('\',\'', $data) . '\'' ;
		$SQLCmd = "SELECT agreement_no, car_sn, s_station, e_station, other_car, baby_chair, baby_chair1, baby_chair2, children_chair, gps, mobile_moto, adult, baby, children, total_car FROM tb_order WHERE agreement_no IN ({$inStr})" ;
		$query = $this->db->query($SQLCmd);

		$nodeArr = array() ;
		foreach ($query->rows as $iCnt => $node) {
			$nodeArr[$node['agreement_no']] = $node ;
		}

		return $nodeArr;
	}

	public function getOrderProducts($order_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

		return $query->rows;
	}

	public function getOrderOptions($order_id, $order_product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_option WHERE order_id = '" . (int)$order_id . "' AND order_product_id = '" . (int)$order_product_id . "'");

		return $query->rows;
	}

	public function getOrderVouchers($order_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_voucher WHERE order_id = '" . (int)$order_id . "'");

		return $query->rows;
	}

	public function getOrderVoucherByVoucherId($voucher_id) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_voucher` WHERE voucher_id = '" . (int)$voucher_id . "'");

		return $query->row;
	}

	public function getOrderTotals($order_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_total WHERE order_id = '" . (int)$order_id . "' ORDER BY sort_order");

		return $query->rows;
	}

	// add by Angus 2017.07.08
	public function newAgreementNo( $todayNo) {
		$newAgreementNo = "" ;

		$SQLCmd = "SELECT * FROM tb_order WHERE agreement_no like '{$todayNo}%' order by agreement_no desc limit 1" ;
		$query = $this->db->query( $SQLCmd) ;
		if ( isset( $query->rows[0])) {
			$atLastNo = (int)str_replace($todayNo, "", $query->rows[0]['agreement_no']) + 1 ;
			$newAgreementNo = $todayNo.str_pad($atLastNo, 4,'0', STR_PAD_LEFT) ;
		} else {
			$atLastNo = 1 ;
			$newAgreementNo = $todayNo.str_pad($atLastNo, 4,'0', STR_PAD_LEFT) ;
		}

		return $newAgreementNo ;
	}

	// add by Angus 2017.07.08
	public function addInformation( $data = array()) {
		$now = date('Y-m-d H:i:s') ;

		foreach ($data as $key => $value) {
			if ( $key != "other_car")
				$data[$key] = $this->db->escape( $value) ;
		}

		$todayNo = date( 'Ymd') ;
		// 123 保養車輛
		// 124 自行鎖車
		if ( ($data['order_type'] != '124' && $data['order_type'] != '123')) {
			$data['agreement_no'] = $this->newAgreementNo( $todayNo) ;
			$SQLCmd = "INSERT INTO tb_order SET
				agreement_no      = '{$data['agreement_no']}',
				rent_user_name    = '{$data['rent_user_name']}',
				rent_user_id      = '{$data['rent_user_id']}',
				rent_identity     = '{$data['rent_identity']}',
				rent_user_born    = '{$data['rent_user_born']}',
				rent_user_tel     = '{$data['rent_user_tel']}',
				rent_user_mail    = '{$data['rent_user_mail']}',

				rent_zip1         = '{$data['rent_zip1']}',
				rent_zip2         = '{$data['rent_zip2']}',
				rent_user_address = '{$data['rent_user_address']}',

				get_user_name     = '{$data['get_user_name']}',
				get_identity      = '{$data['get_identity']}',
				get_user_id       = '{$data['get_user_id']}',
				get_user_born     = '{$data['get_user_born']}',
				get_user_tel      = '{$data['get_user_tel']}',

				rent_company_name = '{$data['rent_company_name']}',
				rent_company_no   = '{$data['rent_company_no']}',
				urgent_man        = '{$data['urgent_man']}',
				urgent_mobile     = '{$data['urgent_mobile']}',

				order_type        = '{$data['order_type']}',
				minsu_idx         = '{$data['minsu_idx']}',
				charge_method     = '{$data['charge_method']}',
				pay_type          = '{$data['pay_type']}',
				spdc              = '{$data['spdc']}',
				pickdiff          = '{$data['pickdiff']}',
				s_station         = '{$data['s_station']}',
				e_station         = '{$data['e_station']}',
				rent_date_out     = '{$data['rent_date_out_full']}',
				rent_date_in_plan = '{$data['rent_date_in_plan_full']}',

				adult             = '{$data['adult']}',
				children          = '{$data['children']}',
				baby              = '{$data['baby']}',

				baby_chair        = '{$data['baby_chair']}',
				baby_chair1       = '{$data['baby_chair1']}',
				baby_chair2       = '{$data['baby_chair2']}',
				children_chair    = '{$data['children_chair']}',
				gps               = '{$data['gps']}',
				mobile_moto       = '{$data['mobile_moto']}',

				oil_cost      = '{$data['oil_cost']}',
				overTime_cost = '{$data['overTime_cost']}',
				business_lose = '{$data['business_lose']}',
				car_lose      = '{$data['car_lose']}',
				other_lose    = '{$data['other_lose']}',
				lose_type     = '{$data['lose_type']}',

				car_model         = '{$data['car_model']}',
				car_sn            = '{$data['car_sn']}',
				car_no            = '{$data['car_no']}',
				other_car         = '{$data['other_car']}',

				total             = '{$data['total']}',

				total_car         = '{$data['total_car']}',

				create_date       = '{$now}',
				createUser        = '{$this->session->data['user_name']}'" ;

			// dump( $SQLCmd) ;
			$this->db->query( $SQLCmd) ;
		} else {
			$data['agreement_no'] = "" ;
		}

		list($y, $m, $sd) = explode('-', $this->db->escape($data['rent_date_out']));
		list($y, $m, $ed) = explode('-', $this->db->escape($data['rent_date_in_plan']));
		$carsArr = unserialize( $data['other_car']) ;
		// dump( $carsArr) ;
		// exit() ;
		foreach ($carsArr as $iCnt => $tmpCar) {
			$SQLCmd = "INSERT INTO tb_scheduling SET
				car_seed = '{$tmpCar['car_seed']}',
				car_idx = '{$tmpCar['idx']}',
				order_type = '{$data['order_type']}', order_no = '{$data['agreement_no']}',
				s_station = '{$data['s_station']}', e_station = '{$data['e_station']}',
				start_date = '{$data['rent_date_out_full']}', end_date = '{$data['rent_date_in_plan_full']}',
				sday = '{$sd}', eday = '{$ed}',
				cust_name = '{$data['rent_user_name']}',
				cust_tel = '{$data['rent_user_tel']}', memo = '{$data['area_memo']}',
				take_name = '{$data['get_user_name']}', take_mobile = '{$data['get_user_tel']}',
				reg_user = '{$this->session->data['user_name']}', reg_date='{$now}'";
			// dump( $SQLCmd) ;
			$this->db->query( $SQLCmd) ;
		}

		return($data['agreement_no']);
	}

	// add by Angus 2017.07.08
	/**
	 * [editInformation description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2018-02-11
	 */
	public function editInformation( $data = array(), $types = "") {
		$now = date('Y-m-d H:i:s') ;
		// add by Angus 2019.07.30
		$SQLCmd = "INSERT INTO tb_order_bak SELECT * FROM tb_order WHERE order_id='{$data['order_id']}'" ;
		$this->db->query( $SQLCmd) ;

		// dump( $data) ;
		foreach ($data as $key => $value) {
			if ( $key != "other_car")
				$data[$key] = $this->db->escape( $value) ;
		}
		if($types == "pickup"){
			$SQLCmd = "UPDATE tb_order SET
				rent_user_name    = '{$data['rent_user_name']}',
				rent_user_id      = '{$data['rent_user_id']}',
				rent_user_born    = '{$data['rent_user_born']}',
				rent_user_tel     = '{$data['rent_user_tel']}',

				rent_user_address = '{$data['rent_user_address']}',

				rent_date_out     = '{$data['rent_date_out_full']}',
				rent_date_in_plan = '{$data['rent_date_in_plan_full']}',
				rent_date_out_true = now(),

				get_user_name     = '{$data['get_user_name']}',
				get_user_id       = '{$data['get_user_id']}',
				get_user_born     = '{$data['get_user_born']}',
				get_user_tel      = '{$data['get_user_tel']}',

				baby_chair1       = '{$data['baby_chair1']}',
				baby_chair2       = '{$data['baby_chair2']}',
				children_chair    = '{$data['children_chair']}',
				gps               = '{$data['gps']}',
				mobile_moto       = '{$data['mobile_moto']}',

				umbrella			= '{$data['umbrella']}',
				driving_recorder    = '{$data['driving_recorder']}',
				mobile_car			= '{$data['mobile_car']}',
				helmet              = '{$data['helmet']}',
				raincoat			= '{$data['raincoat']}',
				car_safety			= '{$data['car_safety']}',
				moto_safety			= '{$data['moto_safety']}',

				car_model         = '{$data['car_model']}',
				car_sn            = '{$data['car_sn']}',
				car_no            = '{$data['car_no']}',
				other_car         = '{$data['other_car']}',

				rent_company_name = '{$data['rent_company_name']}',
				rent_company_no   = '{$data['rent_company_no']}',
				urgent_man        = '{$data['urgent_man']}',
				urgent_mobile     = '{$data['urgent_mobile']}',

				pickdiff          = '{$data['pickdiff']}',

				s_station         = '{$data['s_station']}',
				e_station         = '{$data['e_station']}',

				pay_type          = '{$data['pay_type']}',

				overTime_cost     = '{$data['overTime_cost']}',
				other_lose     = '{$data['other_lose']}',
				rent_miles_out     = '{$data['rent_miles_out']}',
				rent_miles_in     = '{$data['rent_miles_in']}',

				updateDate        = '{$now}',
				updateUser = '{$this->session->data['user_name']}'
			WHERE order_id='{$data['order_id']}'" ;
		}else{
			$SQLCmd = "UPDATE tb_order SET
				rent_user_name    = '{$data['rent_user_name']}',
				rent_identity     = '{$data['rent_identity']}',
				rent_user_id      = '{$data['rent_user_id']}',
				rent_user_born    = '{$data['rent_user_born']}',
				rent_user_tel     = '{$data['rent_user_tel']}',
				rent_user_mail    = '{$data['rent_user_mail']}',

				rent_zip1         = '{$data['rent_zip1']}',
				rent_zip2         = '{$data['rent_zip2']}',
				rent_user_address = '{$data['rent_user_address']}',

				get_user_name     = '{$data['get_user_name']}',
				get_user_id       = '{$data['get_user_id']}',
				get_user_born     = '{$data['get_user_born']}',
				get_user_tel      = '{$data['get_user_tel']}',

				rent_company_name = '{$data['rent_company_name']}',
				rent_company_no   = '{$data['rent_company_no']}',
				urgent_man        = '{$data['urgent_man']}',
				urgent_mobile     = '{$data['urgent_mobile']}',

				order_type        = '{$data['order_type']}',
				minsu_idx         = '{$data['minsu_idx']}',
				pay_type          = '{$data['pay_type']}',
				spdc              = '{$data['spdc']}',
				pickdiff          = '{$data['pickdiff']}',
				s_station         = '{$data['s_station']}',
				e_station         = '{$data['e_station']}',
				rent_date_out     = '{$data['rent_date_out_full']}',
				rent_date_in_plan = '{$data['rent_date_in_plan_full']}',

				oil_cost          = '{$data['oil_cost']}',
				overTime_cost     = '{$data['overTime_cost']}',
				other_lose        = '{$data['other_lose']}',
				business_lose     = '{$data['business_lose']}',
				car_lose          = '{$data['car_lose']}',
				lose_type         = '{$data['lose_type']}',

				adult             = '{$data['adult']}',
				children          = '{$data['children']}',
				baby              = '{$data['baby']}',

				baby_chair        = '{$data['baby_chair']}',
				baby_chair1       = '{$data['baby_chair1']}',
				baby_chair2       = '{$data['baby_chair2']}',
				children_chair    = '{$data['children_chair']}',
				gps               = '{$data['gps']}',
				mobile_moto       = '{$data['mobile_moto']}',

				car_model         = '{$data['car_model']}',
				car_sn            = '{$data['car_sn']}',
				car_no            = '{$data['car_no']}',
				other_car         = '{$data['other_car']}',

				total             = '{$data['total']}',

				total_car         = '{$data['total_car']}',

				updateDate        = '{$now}',
				updateUser = '{$this->session->data['user_name']}'
			WHERE order_id='{$data['order_id']}'" ;


		}
		$this->db->query( $SQLCmd) ;
		// dump( $data['other_car']) ;
		// dump( $SQLCmd) ;
		//exit() ;
		// add by Angus  2018.07.01
		// 訂單狀態是刪除的  就不再更新tb_scheduling
		$SQLCmd = "SELECT order_status FROM tb_order WHERE order_id='{$data['order_id']}'" ;
		$retOrderStatus = $this->db->query( $SQLCmd) ;

		if ( $retOrderStatus->row['order_status'] != 'x') {
			list($y, $m, $sd) = explode('-', $this->db->escape($data['rent_date_out']));
			list($y, $m, $ed) = explode('-', $this->db->escape($data['rent_date_in_plan']));
			$SQLCmd = "DELETE FROM tb_scheduling WHERE order_no='{$data['agreement_no']}' " ;
			$this->db->query( $SQLCmd) ;
			$carsArr = unserialize( $data['other_car']) ;
			foreach ($carsArr as $iCnt => $tmpCar) {
				$SQLCmd = "INSERT INTO tb_scheduling SET
					car_seed = '{$tmpCar['car_seed']}',
					car_idx = '{$tmpCar['idx']}',
					order_type = '{$data['order_type']}', order_no = '{$data['agreement_no']}',
					s_station = '{$data['s_station']}', e_station = '{$data['e_station']}',
					start_date = '{$data['rent_date_out_full']}', end_date = '{$data['rent_date_in_plan_full']}',
					sday = '{$sd}', eday = '{$ed}',
					cust_name = '{$data['rent_user_name']}',
					cust_tel = '{$data['rent_user_tel']}', memo = '{$data['area_memo']}',
					take_name = '{$data['get_user_name']}', take_mobile = '{$data['get_user_tel']}',
					reg_user = '{$this->session->data['user_name']}', reg_date='{$now}'";
				// dump( $SQLCmd) ;
				$this->db->query( $SQLCmd) ;
			}
			// exit() ;
		} else {
			if($types == "pickup"){
				//要回寫備註
				$SQLCmd = "UPDATE tb_scheduling SET status=2, memo = '{$data['area_memo']}' WHERE order_no='{$data['agreement_no']}' " ;
			}else{
				$SQLCmd = "UPDATE tb_scheduling SET status=2 WHERE order_no='{$data['agreement_no']}' " ;
			}
			$this->db->query( $SQLCmd) ;
		}

	}

	// add by Angus 2017.07.05
	public function getCustomers($data = array()) {
		$SQLCmd = "SELECT DISTINCT rent_user_name FROM tb_order
			WHERE 1=1 " ;

		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "rent_user_name like '%{$this->db->escape( $data['filter_name'])}%'" ;
		}

		if ($implode) {
			$SQLCmd .= " AND " . implode(" AND ", $implode);
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$SQLCmd .= " ORDER BY create_date DESC LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		// dump( $SQLCmd) ;

		$query = $this->db->query($SQLCmd);

		return $query->rows;
	}

	/**
	 * [getTotalOrdersForOther description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2018-01-29
	 */
	public function getTotalOrdersForOther( $data = array()) {
		$SQLCmd = "SELECT COUNT(*) AS total FROM tb_order" ;
		$SQLCmd .= " WHERE 1=1 AND (order_status != 'x' || order_status IS NULL)" ;


		if (!empty($data['filter_date_added'])) {
			$SQLCmd .= " AND DATE(create_date) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
		}

		// dump( array( $data, $SQLCmd)) ;
		$query = $this->db->query( $SQLCmd);
		return $query->row['total'];
	}

	// rework by Angus 2017.07.02
	public function getTotalOrders($data = array()) {
		$SQLCmd = "SELECT COUNT(*) AS total FROM tb_order" ;
		$SQLCmd .= " WHERE 1=1 " ;

		// if ( !isset( $status)) {
		// 	$SQLCmd .= " AND order_status IS NULL" ;
		// }

		if (!empty($data['filter_order_id'])) {
			if ( strpos( $data['filter_order_id'], ',') > 0) {
				$tmp = explode(',', $data['filter_order_id']) ;
				foreach ($tmp as $key => &$value) {
					$value = $this->db->escape( $value) ;
				}
				$joinStr = "'".join( "','", $tmp)."'" ;
				$SQLCmd .= " AND agreement_no IN ({$joinStr})" ;
			} else {
				$SQLCmd .= " AND agreement_no like '{$this->db->escape( $data['filter_order_id'])}%'" ;
			}
		}
		if (!empty($data['filter_order_type'])) {
			$SQLCmd .= " AND order_type = '{$this->db->escape( $data['filter_order_type'])}'" ;
		}
		if (!empty($data['filter_order_status'])) {
			if ( $data['filter_order_status'] == 'x') {
				$SQLCmd .= " AND order_status = '{$this->db->escape( $data['filter_order_status'])}'" ;
			} else if ( $data['filter_order_status'] == "success"){
				$SQLCmd .= " AND order_status IS NULL" ;
			}
		}
		if (!empty($data['filter_customer'])) {
			$SQLCmd .= " AND rent_user_name like '%{$this->db->escape( $data['filter_customer'])}%'" ;
		}
		if (!empty($data['filter_mobile'])) {
			$SQLCmd .= " AND rent_user_tel like '%{$this->db->escape( $data['filter_mobile'])}%'" ;
		}
		if (!empty($data['filter_station'])) {
			$SQLCmd .= " AND s_station = '{$this->db->escape( $data['filter_station'])}'" ;
		}
		if (!empty($data['filter_date_out'])) {
			$SQLCmd .= " AND DATE_FORMAT(rent_date_out,'%Y-%m-%d') = '{$this->db->escape( $data['filter_date_out'])}'" ;
		}

		// dump( array( $data, $SQLCmd)) ;
		$query = $this->db->query( $SQLCmd);

		return $query->row['total'];
	}

	/**
	 * [dashboardTodayOrderCnt 資訊總覽] 一次取兩天的量 edit by Angus 2018.09.02
	 * @param   string     $station_name [description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-03-05
	 */
	public function dashboardTodayOrderCnt( $station_name = "", $today = "") {
		$todayAdd1 = date('Y-m-d', strtotime($today. ' + 1 day')) ;

		$colNameArr = array('s_station', 'e_station') ;
		$retArr     = array() ;
		if ( !empty( $station_name) && in_array($station_name, $colNameArr)) {
			$whereStr = "" ;
			switch ( $station_name) {
				case 's_station':
					$whereStr .= " AND order_status is NULL" ;
					$dateColName   = "rent_date_out" ;
					break;
				case 'e_station':
					$whereStr .= " AND order_status = 1" ; // add by Angus 2018.08.20 等無紙化上線再調回來
					//$whereStr .= " AND order_status is NULL" ;
					$dateColName   = "rent_date_in_plan" ;
					break;
			}
			//$whereStr .= " AND order_status is NULL" ;
			$whereStr .= " AND DATE_FORMAT({$dateColName}, '%Y-%m-%d') IN ('{$today}', '{$todayAdd1}')" ;

			$SQLCmd = "SELECT date_format({$dateColName}, '%Y-%m-%d') as useDate, {$station_name} as station_id, COUNT({$station_name}) cnt FROM tb_order
				WHERE  IF(( `order_type`='140' or  `order_type`='147' ) ,`charge_method` =3 and pay_type != '' or pay_type != null, '1=1') {$whereStr}
			 GROUP BY date_format({$dateColName}, '%Y-%m-%d'), {$station_name}" ;
			// dump( $SQLCmd) ;
			$query = $this->db->query( $SQLCmd) ;

			// dump( $query->rows) ;
			foreach ($query->rows as $i => $tmpRow) {

				if ( $tmpRow['useDate'] == $today) {
					$retArr[0][$tmpRow['station_id']] = $tmpRow['cnt'] ;
				} else {
					$retArr[1][$tmpRow['station_id']] = $tmpRow['cnt'] ;
				}
			}
			// dump( $retArr) ;
		}

		return $retArr ;
	}

	/**
	 * [dashboardTodayOrder description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2018-03-07
	 */
	public function dashboardTodayOrder( $data = array() ) {
		// dump( $data) ;
		$whereStr = "" ;
		$whereStr .= " AND {$data['fStation']} = {$data['fStationVal']} " ;
		switch ( $data['fStation']) {
			case 's_station':
				$whereStr .= " AND order_status is NULL" ;
				$whereStr .= " AND ( rent_date_out BETWEEN '{$data['fToday']} 00:00' AND '{$data['fToday']} 23:59')" ;
				break;
			case 'e_station':
				$whereStr .= " AND order_status = 1" ; // add by Angus 2018.08.20 等無紙化上線再調回來
				// $whereStr .= " AND order_status is NULL" ;
				$whereStr .= " AND ( rent_date_in_plan BETWEEN '{$data['fToday']} 00:00' AND '{$data['fToday']} 23:59')" ;
				break;
		}

		$SQLCmd = "SELECT * FROM tb_order WHERE IF((`order_type`='140' or `order_type`='147'),`charge_method` =3 and pay_type != '' or pay_type != null, '1=1') {$whereStr} ORDER BY {$data['sort']} {$data['order']}" ;
		// dump( array( $data, $SQLCmd)) ;
		$query = $this->db->query($SQLCmd) ;
		return $query->rows ;
	}


	public function getTotalOrdersByStoreId($store_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE store_id = '" . (int)$store_id . "'");

		return $query->row['total'];
	}

	public function getTotalOrdersByOrderStatusId($order_status_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE order_status_id = '" . (int)$order_status_id . "' AND order_status_id > '0'");

		return $query->row['total'];
	}

	public function getTotalOrdersByProcessingStatus() {
		$implode = array();

		$order_statuses = $this->config->get('config_processing_status');

		foreach ($order_statuses as $order_status_id) {
			$implode[] = "order_status_id = '" . (int)$order_status_id . "'";
		}

		if ($implode) {
			$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE " . implode(" OR ", $implode));

			return $query->row['total'];
		} else {
			return 0;
		}
	}

	public function getTotalOrdersByCompleteStatus() {
		$implode = array();

		$order_statuses = $this->config->get('config_complete_status');

		foreach ($order_statuses as $order_status_id) {
			$implode[] = "order_status_id = '" . (int)$order_status_id . "'";
		}

		if ($implode) {
			$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE " . implode(" OR ", $implode) . "");

			return $query->row['total'];
		} else {
			return 0;
		}
	}

	public function getTotalOrdersByLanguageId($language_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE language_id = '" . (int)$language_id . "' AND order_status_id > '0'");

		return $query->row['total'];
	}

	public function getTotalOrdersByCurrencyId($currency_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE currency_id = '" . (int)$currency_id . "' AND order_status_id > '0'");

		return $query->row['total'];
	}

	public function createInvoiceNo($order_id) {
		$order_info = $this->getOrder($order_id);

		if ($order_info && !$order_info['invoice_no']) {
			$query = $this->db->query("SELECT MAX(invoice_no) AS invoice_no FROM `" . DB_PREFIX . "order` WHERE invoice_prefix = '" . $this->db->escape($order_info['invoice_prefix']) . "'");

			if ($query->row['invoice_no']) {
				$invoice_no = $query->row['invoice_no'] + 1;
			} else {
				$invoice_no = 1;
			}

			$this->db->query("UPDATE `" . DB_PREFIX . "order` SET invoice_no = '" . (int)$invoice_no . "', invoice_prefix = '" . $this->db->escape($order_info['invoice_prefix']) . "' WHERE order_id = '" . (int)$order_id . "'");

			return $order_info['invoice_prefix'] . $invoice_no;
		}
	}

	public function getOrderHistories($order_id, $start = 0, $limit = 10) {
		if ($start < 0) {
			$start = 0;
		}

		if ($limit < 1) {
			$limit = 10;
		}

		$query = $this->db->query("SELECT oh.date_added, os.name AS status, oh.comment, oh.notify FROM " . DB_PREFIX . "order_history oh LEFT JOIN " . DB_PREFIX . "order_status os ON oh.order_status_id = os.order_status_id WHERE oh.order_id = '" . (int)$order_id . "' AND os.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY oh.date_added ASC LIMIT " . (int)$start . "," . (int)$limit);

		return $query->rows;
	}

	public function getTotalOrderHistories($order_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "order_history WHERE order_id = '" . (int)$order_id . "'");

		return $query->row['total'];
	}

	public function getTotalOrderHistoriesByOrderStatusId($order_status_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "order_history WHERE order_status_id = '" . (int)$order_status_id . "'");

		return $query->row['total'];
	}

	public function getEmailsByProductsOrdered($products, $start, $end) {
		$implode = array();

		foreach ($products as $product_id) {
			$implode[] = "op.product_id = '" . (int)$product_id . "'";
		}

		$query = $this->db->query("SELECT DISTINCT email FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "order_product op ON (o.order_id = op.order_id) WHERE (" . implode(" OR ", $implode) . ") AND o.order_status_id <> '0' LIMIT " . (int)$start . "," . (int)$end);

		return $query->rows;
	}

	public function getTotalEmailsByProductsOrdered($products) {
		$implode = array();

		foreach ($products as $product_id) {
			$implode[] = "op.product_id = '" . (int)$product_id . "'";
		}

		$query = $this->db->query("SELECT DISTINCT email FROM `" . DB_PREFIX . "order` o LEFT JOIN " . DB_PREFIX . "order_product op ON (o.order_id = op.order_id) WHERE (" . implode(" OR ", $implode) . ") AND o.order_status_id <> '0'");

		return $query->row['email'];
	}
}
