<?php
class ModelSaleQrcode extends Model {

	public function updateIn( $order_id, $other_car) {
		$now = date('Y-m-d H:i:s') ;
		$SQLCmd = "INSERT INTO tb_order_bak SELECT * FROM tb_order WHERE order_id='{$order_id}'" ;
		$this->db->query( $SQLCmd) ;

		$SQLCmd = "UPDATE tb_order SET other_car='{$other_car}', updateUser='{$this->session->data['user_name']}', updateDate='{$now}' WHERE order_id = '{$order_id}'" ;
		$this->db->query( $SQLCmd) ;
	}

	public function delUseIdx ( $useIdx) {
		$SQLCmd = "UPDATE oc_qrcode SET status='x' WHERE idx = '" . (int)$useIdx . "'" ;
		// dump( $SQLCmd) ;
		$this->db->query( $SQLCmd) ;
	}

	public function getColumns() {
		$columns = array() ;
		$SQLCmd = "DESC oc_qrcode" ;
		$query = $this->db->query( $SQLCmd) ;

		foreach ( $query->rows as $result) {
			$columns[] = $result['Field'];
		}

		return $columns ;
	}

	/**
	 * [getInformations description]
	 * @param   [type]     $useIdx [description]
	 * @return  [type]             [description]
	 * @Another Angus
	 * @date    2018-05-26
	 */
	public function getInformations( $useIdx) {
		$SQLCmd = "SELECT * FROM oc_qrcode WHERE idx = '" . (int)$useIdx . "'" ;
 		$query = $this->db->query( $SQLCmd) ;

 		return $query->rows ;
	}

	public function getInformation( $useIdx) {
		$SQLCmd = "SELECT * FROM oc_qrcode WHERE idx = '" . (int)$useIdx . "'" ;
 		$query = $this->db->query( $SQLCmd) ;

 		return $query->row ;
	}

	public function addInformation ( $data) {
		date_default_timezone_set('Asia/Taipei');
		//自動生成qrcode YmdHis+ok_min+remark (mod16)
		$qrcode = $data['input_start_date'].$data['input_ok_min'].$data['input_remark'];
		$qrcode = md5($qrcode);
		//設定結束時間
		$effective_date = date('Y-m-d H:i:s' , mktime(date("H"),date("i")+$data['input_ok_min'],date("s"),date("m"),date("d"),date("Y")));
		//$min = substr($effective_date,14,2);



		// dump( $data) ;
		$SQLCmd = "INSERT INTO oc_qrcode SET
			qrcode = '" . $this->db->escape($qrcode) . "',
			remark = '". $this->db->escape( $data['input_remark'])."',
			start_date = '" . $this->db->escape($data['input_start_date']) . "',
			effective_date = '" . $this->db->escape($effective_date) . "',
			ok_min = '" . $this->db->escape($data['input_ok_min']) . "',
			status = '" . $this->db->escape($data['sel_status']) . "'" ;
		// dump( $SQLCmd) ;
		$this->db->query( $SQLCmd) ;
	}

	public function editInformation ( $useIdx, $data) {
		$SQLCmd = "UPDATE oc_qrcode SET
			remark = '". $this->db->escape( $data['input_remark'])."',
			ok_min = '" . $this->db->escape($data['input_ok_min']) . "',
			status = '" . $this->db->escape($data['sel_status']) . "'
		 	WHERE idx = '" . (int)$useIdx . "'" ;
		 // dump( $SQLCmd) ;
		$this->db->query( $SQLCmd) ;
	}

	public function getTotalQrcode( $data = array()) {
		$SQLCmd = "SELECT COUNT(*) AS total FROM oc_qrcode" ;
		$SQLCmd .= " WHERE status != 'x'" ;
		$SQLCmd .= " and effective_date >= NOW()" ;
		$query = $this->db->query( $SQLCmd) ;

		return $query->row['total'] ;
	}

	public function getQrcodes( $data = array()) {
		$SQLCmd = "SELECT * FROM oc_qrcode " ;
		$SQLCmd .= " WHERE status != 'x'" ;
		$SQLCmd .= " and effective_date >= NOW()" ;
		//$SQLCmd .= " ORDER BY {$data['sort']} {$data['order']}" ;

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$SQLCmd .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query( $SQLCmd) ;

		return $query->rows ;
	}

	/**
	 * [getCarNoForList description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2018-05-30
	 */
	public function getCarNoForList( $data = array()) {
		// dump( $data) ;
		$inStr = join( ',', $data) ;
		$SQLCmd = "SELECT * FROM oc_qrcode WHERE idx IN ({$inStr}) AND status != 'x'" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		return $query->rows ;
	}


	/**
	 * [getCarNoForAax description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2018-02-14
	 */
	public function getCarNoForAjax( $data = array()) {
		$whereStr = "" ;
		if ( isset( $data['car_seed']) && !empty( $data['car_seed'])) {
			$whereStr = "AND car_seed = '{$data['car_seed']}'" ;
		}
		if ( isset( $data['car_no']) && !empty( $data['car_no'])) {
			$whereStr .= "AND car_no LIKE '%{$data['car_no']}%'" ;
		}

		if ( !empty( $whereStr)) {
			$SQLCmd = "SELECT idx, car_no FROM oc_qrcode WHERE 1=1 {$whereStr} AND status != 'x'" ;
			// dump( $SQLCmd) ;
			$query = $this->db->query( $SQLCmd) ;
			return $query->rows ;
		} else {
			return array() ;
		}
	}
	public function getImageSrc($qrcode){
		//HTTP_CATALOG+?qid=$qrcode
		header("Content-Type: image/png");
		$data = HTTP_CATALOG."?qid=".$qrcode;
		$params['data'] = $data;
		$params['level'] = 'H';
		$params['size'] = 10;
		$params['savename'] = DIR_IMAGE.'qrcode/'.$qrcode.'.png';
		$this->ciqrcode->generate($params);
	 }




}