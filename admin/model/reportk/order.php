<?php
/**
 *
 */
class ModelReportkOrder extends Model {

	/**
	 * [getLevel_1_report description]
	 * @param   array      $filter [description]
	 * @return  [type]             [description]
	 * @Another Angus
	 * @date    2019-01-11
	 */
	public function getLevel_1_report( $filter = array()) {
		$whereStr = " AND order_type = " . $this->db->escape( $filter['ot']) ;

		$SQLCmd = "SELECT strY, strM, sum( total ) total, count( strD ) cnt FROM
				(
				SELECT
					date_format( create_date, '%Y' ) strY,
					date_format( create_date, '%m' ) strM,
					date_format( create_date, '%d' ) strD,
					total
				FROM tb_order WHERE order_status IS NULL {$whereStr}
				) a
			GROUP BY strY, strM
			ORDER BY strY DESC, strM DESC" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query($SQLCmd);
		return $query->rows ;
	}

	/**
	 * [getLevel_2_report description]
	 * @param   array      $filter [description]
	 * @return  [type]             [description]
	 * @Another Angus
	 * @date    2019-01-12
	 */
	public function getLevel_2_report( $filter = array()) {
		$whereStr = " AND order_type = " . $this->db->escape( $filter['ot']) ;
		$whereStr .= " AND date_format( create_date, '%Y' ) = " . $this->db->escape( $filter['yy']) ;
		$whereStr .= " AND date_format( create_date, '%m' ) = " . $this->db->escape( $filter['mm']) ;

		$SQLCmd = "SELECT strY, strM, strD, sum( total ) total, count( strD ) cnt FROM
				(
				SELECT
					date_format( create_date, '%Y' ) strY,
					date_format( create_date, '%m' ) strM,
					date_format( create_date, '%d' ) strD,
					total
				FROM tb_order WHERE order_status IS NULL {$whereStr}
				) a
			GROUP BY strY, strM, strD
			ORDER BY strY DESC, strM DESC" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query($SQLCmd);
		return $query->rows ;
	}

	/**
	 * [getTotal description]
	 * @param   array      $filter [description]
	 * @return  [type]             [description]
	 * @Another Angus
	 * @date    2020-02-20
	 */
	public function getTotal( $filter = array()) {
		$column = array();
		//
		if ( isset( $filter['rdi']) && $filter['rdi'] != "") {
//			$column[] = "DATE_FORMAT(rent_date_in, '%Y-%m-%d') =".$this->db->escape($filter['ept']);
			$column[] = "DATE_FORMAT(create_date, '%Y-%m-%d') ='".$this->db->escape($filter['rdi'])."'";
		} else {
			if ( isset($filter['fds']) && $filter['fds'] != "") {
				$column[] = "create_date > '".$this->db->escape($filter['fds'])."'";
			}
			if ( isset($filter['fde']) && $filter['fde'] != "") {
				$column[] = "create_date < '".$this->db->escape($filter['fde'])." 23:59:59'";
			}
		}

		$SQLCmd = "SELECT COUNT(*) AS total FROM tb_order WHERE ( order_status IS NULL OR order_status IN ('1', '2')) " ;
		$SQLCmd .= " AND " . implode(" AND ",$column);
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		return $query->row['total'];
	}

	/**
	 * [getList description]
	 * @param   array      $filter [description]
	 * @return  [type]             [description]
	 * @Another Angus
	 * @date    2020-02-20
	 */
	public function getList( $filter = array()) {
		$sort_data = array() ;
		$column = array();

		// 以還車時間查詢
		if ( isset( $filter['rdi']) && $filter['rdi'] != "") {
			$column[] = "rent_date_in > '".$this->db->escape($filter['rdi'])." 00:00:00'";
			if ( isset($filter['station']) && !empty( $filter['station'])) {
				$column[] = "e_station =".$this->db->escape($filter['station']) ;
			}
			if ( isset($filter['filter_man']) && !empty( $filter['filter_man'])) {
				$column[] = "out_man ='".$filter['filter_man']."'" ;
			}
		}
		if ( isset( $filter['rde']) && $filter['rde'] != "") {
			$column[] = "rent_date_in < '".$this->db->escape($filter['rde'])." 23:59:59'";
		}


		if ( isset($filter['fds']) && $filter['fds'] != "") {
			$column[] = "rent_date_out_true > '".$this->db->escape($filter['fds'])." 00:00:00'";
			if ( isset($filter['station']) && !empty( $filter['station'])) {
				$column[] = "s_station =".$this->db->escape($filter['station']) ;
			}
			if ( isset($filter['filter_man']) && !empty( $filter['filter_man'])) {
				$column[] = "in_man ='".$filter['filter_man']."'" ;
			}
		}
		if ( isset($filter['fde']) && $filter['fde'] != "") {
			$column[] = "rent_date_out_true < '".$this->db->escape($filter['fde'])." 23:59:59'";
		}


		if ( isset($filter['spt']) && !empty( $filter['spt'])) {
			$column[] = "s_station =".$this->db->escape($filter['spt']);
		}
		if ( isset($filter['ept']) && !empty( $filter['ept'])) {
			$column[] = "e_station =".$this->db->escape($filter['ept']);
		}

		// $SQLCmd = "SELECT tbo.order_type, tbo.lose_type, tbo.car_no, tbo.car_model, tbo.rent_user_name, tbo.rent_user_id,
		// 		tbo.rent_company_name, tbo.rent_company_no,
		// 		tbo.rent_user_born, tbo.rent_user_tel, tbo.agreement_no,
		// 		DATE_FORMAT(tbo.rent_date_out,'%Y-%m-%d') as rent_out_date, DATE_FORMAT(tbo.rent_date_out,'%H-%i') as rent_out_time,
		// 		DATE_FORMAT(tbo.rent_date_in_plan,'%Y-%m-%d') as rent_in_date, DATE_FORMAT(tbo.rent_date_in_plan,'%H-%i') as rent_in_time,
		// 		datediff( DATE_FORMAT(tbo.rent_date_in_plan,'%Y-%m-%d'), DATE_FORMAT(tbo.rent_date_out,'%Y-%m-%d')) +1 as total_day,
		// 		tbo.total, tbo.minsu_idx as minsu, ( tbo.adult + tbo.baby + tbo.children) as members,
		// 		tbcc.create_man, tbcc.return_man
		// 	FROM tb_order tbo LEFT JOIN tb_car_condition tbcc ON tbo.agreement_no=tbcc.agreement_no
		// 	WHERE ( tbo.order_status IS NULL OR tbo.order_status IN ('1', '2')) \n" ;
		$SQLCmd = "SELECT tbo.* ,
				datediff( DATE_FORMAT(tbo.rent_date_in_plan,'%Y-%m-%d'), DATE_FORMAT(tbo.rent_date_out,'%Y-%m-%d')) +1 as total_day,
				( tbo.adult + tbo.baby + tbo.children) as members,
				tbcc.create_man , tbcc.return_man
			FROM tb_order as tbo left join tb_car_condition as tbcc on tbo.agreement_no = tbcc.agreement_no
			WHERE ( tbo.order_status IS NULL OR tbo.order_status IN ('1', '2')) \n" ;
		$SQLCmd .= " AND " . implode(" AND ",$column);

		// 預設排序欄位 -----------------------------------------------------------------------
		$sort_data = array(
			"battery_id",
		);
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$SQLCmd .= " ORDER BY " . $data['sort'];
			if (( $data['order'] == 'DESC')) {
				$SQLCmd .= " DESC";
			} else {
				$SQLCmd .= " ASC";
			}
		} else {
			// 預設排序條件
			$SQLCmd .= " ORDER BY tbo.agreement_no ASC" ;
		}

		// // 分頁部份 -------------------------------------------------------------------------
		if (isset($filter['start']) || isset($filter['limit'])) {
			if ($filter['start'] < 0) {
				$filter['start'] = 0;
			}

			if ($filter['limit'] < 1) {
				$filter['limit'] = 20;
			}
			// $SQLCmd .= " LIMIT " . (int)$filter['start'] . "," . (int)$filter['limit'];
		}

		// dump( $filter) ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		return $query->rows;
	}

	/**
	 * [getOrderTotal description]
	 * @param   array      $filter [description]
	 * @return  [type]             [description]
	 * @Another Angus
	 * @date    2020-02-23
	 */
	public function getOrderTotal( $filter = array()) {
		$column = array();

		if ( isset($filter['fds']) && $filter['fds'] != "") {
			$column[] = "create_date > '".$this->db->escape($filter['fds'])."'";
		}
		if ( isset($filter['fde']) && $filter['fde'] != "") {
			$column[] = "create_date < '".$this->db->escape($filter['fde'])." 23:59:59'";
		}
		$SQLCmd = "SELECT order_type, COUNT(order_type) cnt, SUM(total) sum_total FROM tb_order
				WHERE ( order_status IS NULL OR order_status IN ('1', '2'))" ;
		$SQLCmd .= " AND " . implode(" AND ",$column);
		$SQLCmd .= " GROUP BY order_type" ;

		$query = $this->db->query( $SQLCmd) ;
		return $query->rows;
	}

	/**
	 * [getMinsuList description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2020-02-21
	 */
	public function getMinsuList() {
		$retArr = array() ;
		$SQLCmd = "SELECT idx, name FROM tb_minshuku" ;
		$query = $this->db->query( $SQLCmd) ;

		foreach ($query->rows as $iCnt => $row) {
			$retArr[$row['idx']] = $row['name'] ;
		}
		return $retArr ;
	}

	/**
	 * [getCarModelType description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2020-02-21
	 */
	public function getCarModelType() {
		$retArr = array() ;
		$SQLCmd = "SELECT scm.idx, scm.car_seed, scm.car_model, so.opt_name
				FROM sys_car_model scm LEFT JOIN sys_option so ON scm.car_model = so.idx" ;
		$query = $this->db->query( $SQLCmd) ;

		foreach ($query->rows as $iCnt => $row) {
			$retArr[$row['idx']] = $row ;
		}
		return $retArr ;
	}

	/**
	 * [getConditionName description]
	 * @param   string     $agreement_no [description]
	 * @return  [type]                   [description]
	 * @Another Angus
	 * @date    2020-04-14
	 */
	public function getConditionName( $agreement_no = "") {
		if ( $agreement_no == "") {
			return [];
		} else {
			$SQLCmd = "select * from tb_car_condition where agreement_no='{$agreement_no}'" ;
			$query = $this->db->query( $SQLCmd) ;

			return [@$query->row['create_man'], @$query->row['return_man']] ;
		}
	}

	/**
	 * [getUserArr description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2020-07-21
	 */
	public function getUserArr() {
		$retArr = [] ;
		$SQLCmd = "SELECT username, lastname, firstname FROM oc_user WHERE user_group_id != 1" ;
		$query = $this->db->query( $SQLCmd) ;
		foreach ($query->rows as $iCnt => $row) {
			$retArr[$row['username']] = $row['username']."-".$row['lastname'].$row['firstname'] ;
		}
		return $retArr ;
	}
}
