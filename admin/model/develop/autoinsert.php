<?php
class ModelDevelopAutoinsert extends Model {
	public function getTables()	{
		$sql = 'SHOW TABLES' ;

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getDescription($table) {
		$sql = 'DESC ' . $table;

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function insertRandomData($table, $insertDatas) {
		$sql = 'INSERT ' . $table;

		$field = '';

		foreach ($insertDatas as $key => $insertData) {
			$values[] = implode("','", $insertData);

			if (!$field) {
				foreach ($insertData as $column => $data) {
					$fieldArray[] = $column;
				}

				$field = ' (' .implode(',', $fieldArray) . ')';
			}
		}

		$sql .= $field . " VALUES ('" . implode("'), ('", $values) . "')";

		$this->db->query($sql);
	}
}
