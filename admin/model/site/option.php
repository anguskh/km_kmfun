<?php
class ModelSiteOption extends Model {

	/**
	 * [addInformation description]
	 * @param   Array      $data [description]
	 * @Another Angus
	 * @date    2017-10-12
	 */
	public function addInformation ( Array $data) {
		$SQLCmd = "INSERT INTO sys_option SET
			opt_name = '" . $this->db->escape($data['opt_name']) . "', opt_desc = '" . $this->db->escape($data['opt_desc']) . "',
			opt_short_desc = '" . $this->db->escape($data['opt_short_desc']) . "', opt_seq = '" . $this->db->escape($data['opt_seq']) . "',
		 	status = '" . $this->db->escape($data['status']) . "', parent = '" . $this->db->escape($data['parent']) . "'";
		$this->db->query( $SQLCmd) ;
	}

	/**
	 * [editInformation description]
	 * @param   integer     $useIdx [description]
	 * @param   Array       $data   [description]
	 * @return  Array               [description]
	 * @Another Angus
	 * @date    2017-10-12
	 */
	public function editInformation ( $useIdx, Array $data) {
		$SQLCmd = "UPDATE sys_option SET
			opt_name = '" . $this->db->escape($data['opt_name']) . "', opt_desc = '" . $this->db->escape($data['opt_desc']) . "',
			opt_short_desc = '" . $this->db->escape($data['opt_short_desc']) . "', opt_seq = '" . $this->db->escape($data['opt_seq']) . "',
		 	status = '" . $this->db->escape($data['status']) . "' WHERE idx = '" . (int)$useIdx . "'" ;
		$this->db->query( $SQLCmd) ;
	}

	/**
	 * [getOptionItemsArr description]
	 * @param   integer    $parentIdx 父層編號
	 * @param   boolean    $mod       是否要全取回來, 預設只取啟用選項, 在處理列表顯示就要取全部的選項
	 * @return  [type]                回傳id
	 * @Another Angus
	 * @date    2017-11-10
	 */
	public function getOptionItemsArr( $parentIdx = 0, $mod = false) {

		$SQLCmd = "SELECT idx, opt_desc  FROM sys_option WHERE parent = '{$parentIdx}' %s ORDER BY opt_seq ASC" ;
		$SQLCmd = ( $mod) ? sprintf( $SQLCmd, '' ) : sprintf( $SQLCmd, "AND status = '1'") ;
		// dump( $SQLCmd) ;

		$query = $this->db->query( $SQLCmd) ;

		$retArr = array() ;
		foreach ($query->rows as $key => $tmpArr) {
			$retArr[ $tmpArr['idx']] = $tmpArr['opt_desc'] ;
		}
		return $retArr ;
	}

	/**
	 * [getOptionNameArr 取得sys_option.opt_name]
	 * @param   integer    $parentIdx [description]
	 * @param   boolean    $mod       [description]
	 * @return  [type]                [description]
	 * @Another Angus
	 * @date    2017-12-23
	 */
	public function getOptionNameArr( $parentIdx = 0, $mod = false) {

		$SQLCmd = "SELECT idx, opt_name  FROM sys_option WHERE parent = '{$parentIdx}' %s ORDER BY opt_seq ASC" ;
		$SQLCmd = ( $mod) ? sprintf( $SQLCmd, '' ) : sprintf( $SQLCmd, "AND status = '1'") ;
		// dump( $SQLCmd) ;

		$query = $this->db->query( $SQLCmd) ;

		$retArr = array() ;
		foreach ($query->rows as $key => $tmpArr) {
			$retArr[ $tmpArr['idx']] = $tmpArr['opt_name'] ;
		}
		return $retArr ;
	}
	/**
	 * [getOptionItemsAllColArr description] idx, opt_name, opt_desc, opt_short_desc
	 * @param   integer    $parentIdx [description]
	 * @param   boolean    $mod       是否要全取回來, 預設只取啟用選項, 在處理列表顯示就要取全部的選項
	 * @return  [array]                [description]
	 * @Another Angus
	 * @date    2017-11-12
	 */
	public function getOptionItemsAllColArr( $parentIdx = 0, $mod = false) {

		$SQLCmd = "SELECT idx, opt_name, opt_desc, opt_short_desc FROM sys_option WHERE parent = '{$parentIdx}' %s ORDER BY opt_seq ASC" ;
		$SQLCmd = ( $mod) ? sprintf( $SQLCmd, '' ) : sprintf( $SQLCmd, "AND status = '1'");
		// dump( $SQLCmd) ;

		$query = $this->db->query( $SQLCmd) ;

		return $query->rows ;
	}

	/**
	 * [getOptionItems description]
	 * @param   integer  $parentIdx  [description]
	 * @return  [type]               [description]
	 */
	public function getOptionItems( $parentIdx = 0) {
		if ( $parentIdx == 0) {
			$SQLCmd = "SELECT * FROM sys_option WHERE parent='{$parentIdx}' ORDER BY idx ASC" ;
		} else {
			$SQLCmd = "SELECT * FROM sys_option WHERE parent='{$parentIdx}' ORDER BY status ASC, opt_seq ASC" ;
		}

		$query = $this->db->query( $SQLCmd) ;

		return $query->rows ;
	}


	/**
	 * [getTotalCnt description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2017-10-12
	 */
	public function getTotalCnt() {
		$SQLCmd = "SELECT COUNT(*) AS total FROM sys_option" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;

		return $query->row['total'] ;
	}

	/**
	 * [getLists description]
	 * @param  array  $data [description]
	 * @return [type]       [description]
	 */
	public function getLists( $data = array()) {
		$SQLCmd = "SELECT * FROM sys_option" ;

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$SQLCmd .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query( $SQLCmd) ;

		return $query->rows ;
	}

	/**
	 * [getRentTime description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-02-08
	 */
	public function getRentTime() {
		return array(
				// "07:00",
				"07:30",
				"08:00",
				"08:30",
				"09:00",
				"09:30",
				"10:00",
				"10:30",
				"11:00",
				"11:30",
				"12:00",
				"12:30",
				"13:00",
				"13:30",
				"14:00",
				"14:30",
				"15:00",
				"15:30",
				"16:00",
				"16:30",
				"17:00",
				"17:30",
				"18:00",
				"18:30",
				// "19:00",
				// "19:30",
				// "20:00",
			) ;
	}

	/**
	 * [getOptionArrUseParent description]
	 * @param   array      $parArr [description]
	 * @return  [type]             [description]
	 * @Another Angus
	 * @date    2018-07-18
	 */
	public function getOptionArrUseParent( $parArr = array()) {

		$retArr = array() ;
		foreach ($parArr as $i => $parentID) {
			$tmp = $this->getOptionItemsAllColArr( $parentID) ;
			$retArr = array_merge( $retArr, $tmp) ;
		}
		return $retArr ;
	}

	public function getUserList($user_group_id, $now_username = ""){
		$SQLCmd = "SELECT user_id, username, lastname, firstname FROM oc_user WHERE (user_group_id in ({$user_group_id}) or username= '{$now_username}' ) and status = 1" ;
		$query = $this->db->query( $SQLCmd) ;
		return $query->rows ;
	}


}