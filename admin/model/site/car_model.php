<?php
class ModelSiteCarModel extends Model {
	/**
	 * 取 車輛類型 汽車, 進口車, 貨車, 機車, 自動車, 自行車
	 * @param   boolean    $showAll [顯示所有車種 預設不顯示停用之車種]
	 * @return  [type]              [description]
	 * @Another Angus
	 * @date    2017-11-12
	 * @date    2018-10-08
	 */
	public function getCarModelArr( $showAll = false) {
		$statusStr = "" ;
		if ( !$showAll) {
			$statusStr = "AND status = '1'" ;
		}

		$SQLCmd = "SELECT idx, opt_desc FROM sys_option WHERE parent = '1' {$statusStr} ORDER BY  opt_seq ASC" ;
		$query = $this->db->query( $SQLCmd) ;

		$retArr = array() ;
		foreach ($query->rows as $key => $tmpArr) {
			$retArr[ $tmpArr['idx']] = $tmpArr['opt_desc'] ;
		}
		return $retArr ;
	}

	public function getColumns() {
		$columns = array() ;
		$SQLCmd = "DESC sys_car_model" ;
		$query = $this->db->query( $SQLCmd) ;

		foreach ( $query->rows as $result) {
			$columns[] = $result['Field'];
		}

		return $columns ;
	}

	/**
	 * [getInformation description]
	 * @param   [type]     $useIdx [description]
	 * @return  [type]             [description]
	 * @Another Angus
	 * @date    2017-11-12
	 */
	public function getInformation ( $useIdx) {
		$SQLCmd = "SELECT * FROM sys_car_model WHERE idx = '" . (int)$useIdx . "'" ;
 		$query = $this->db->query( $SQLCmd) ;

 		return $query->row ;
	}

	/**
	 * 用車型名稱查詢 用在檢查新增/修改車種基本資料時的比對功能
	 * @param   [text]     $useIdx [車種名稱 : TOYOTA, HONDA, BMW]
	 * @return  [type]             [description]
	 * @Another Angus
	 * @date    2017-11-12
	 */
	public function getInformationUseSeed ( $useIdx) {
		$useIdx = $this->db->escape( $useIdx) ;
		$SQLCmd = "SELECT * FROM sys_car_model WHERE car_seed = '" . $useIdx . "'" ;
 		$query = $this->db->query( $SQLCmd) ;

 		return $query->row ;
	}

	public function addInformation ( $data) {
		foreach ($data as $key => $value) {
			$data[$key] = $this->db->escape( $value) ;
		}
		$imageIntro = serialize( array(
			$data['imageIntro1'], $data['imageIntro2'], $data['imageIntro3']
			)) ;

		$SQLCmd = "INSERT INTO sys_car_model SET
			car_seed = '{$data['input_car_seed']}', car_model = '{$data['sel_car_model']}', car_year = '{$data['input_car_year']}',
			car_price = '{$data['input_car_price']}', car_peopele = '{$data['input_car_peopele']}', car_door = '{$data['input_car_door']}',
			gas_class = '{$data['input_gas_class']}', gas_type = '{$data['input_gas_type']}', displacement = '{$data['input_displacement']}',
			at_mt = '{$data['input_at_mt']}', back_radar = '{$data['input_back_radar']}', cd_player = '{$data['input_cd_player']}',
			mp3 = '{$data['input_mp3']}', radio = '{$data['input_radio']}', usb_charge = '{$data['input_usb_charge']}',
			gps = '{$data['input_gps']}', driving_recorder = '{$data['input_driving_recorder']}',
			airbag = '{$data['input_airbag']}', airbag_pos = '{$data['input_airbag_pos']}',
			image = '{$data['image']}', imageIntro = '{$imageIntro}'
		" ;
		// dump( $SQLCmd) ;
		$this->db->query( $SQLCmd) ;
	}

	public function editInformation ( $useIdx, $data) {
		foreach ($data as $key => $value) {
			$data[$key] = $this->db->escape( $value) ;
		}
		$imageIntro = serialize( array(
			$data['imageIntro1'], $data['imageIntro2'], $data['imageIntro3']
			)) ;

		$SQLCmd = "UPDATE sys_car_model SET
			car_seed = '{$data['input_car_seed']}', car_model = '{$data['sel_car_model']}', car_year = '{$data['input_car_year']}',
			car_price = '{$data['input_car_price']}', car_peopele = '{$data['input_car_peopele']}', car_door = '{$data['input_car_door']}',
			gas_class = '{$data['input_gas_class']}', gas_type = '{$data['input_gas_type']}', displacement = '{$data['input_displacement']}',
			at_mt = '{$data['input_at_mt']}', back_radar = '{$data['input_back_radar']}', cd_player = '{$data['input_cd_player']}',
			mp3 = '{$data['input_mp3']}', radio = '{$data['input_radio']}', usb_charge = '{$data['input_usb_charge']}',
			gps = '{$data['input_gps']}', driving_recorder = '{$data['input_driving_recorder']}',
			airbag = '{$data['input_airbag']}', airbag_pos = '{$data['input_airbag_pos']}',
			image = '{$data['image']}', imageIntro = '{$imageIntro}' WHERE idx='{$useIdx}'" ;

		$this->db->query( $SQLCmd) ;
	}

	public function delUseIdx ( $useIdx) {
		$this->db->query("DELETE FROM sys_car_model WHERE idx = '" . (int)$useIdx . "'") ;

		// $this->cache->delete('information');
	}

	/**
	 * 取 車型名稱
	 * @return  [array]     回傳 車型ID, 車型名稱
	 * @Another Angus
	 * @date    2017-11-12
	 */
	public function getCarSeedOption() {
		$SQLCmd = "SELECT idx, car_seed FROM sys_car_model order by car_model asc, car_seed asc" ;
		$query = $this->db->query( $SQLCmd) ;

		$arrCarSeedOption = array() ;
		foreach ($query->rows as $key => $value) {
			$arrCarSeedOption[$value['idx']] = $value['car_seed'] ;
		}
		return $arrCarSeedOption ;
	}

	public function getCarSeedOptionSearch() {
		$SQLCmd = "SELECT idx, car_seed, car_model FROM sys_car_model order by car_model asc, car_seed asc" ;
		$query = $this->db->query( $SQLCmd) ;

		$carModelDesc = $this->getCarModelArr() ;
		$sortModel = array() ;
		$carModelArr = array() ;
		foreach ($query->rows as $key => $value) {
			$sortModel[$value['car_model']][] = $value['idx'] ;
			$carModelArr[$value['idx']] = $value['car_seed'] ;
		}
		return array( $sortModel, $carModelArr) ;
	}

	public function getCarsForCarSeed( $useIdxArr) {
// 查詢車號及車型的SQLCmd
// select sc.*, scm.car_seed car_seed_name
// from sys_cars sc left join sys_car_model scm on sc.car_seed=scm.idx where sc.status='2'
		// dump( $useIdxArr) ;
		$inStr = join( ',', $useIdxArr) ;
		if ( !empty( $inStr)) {
			// $SQLCmd = "SELECT a.idx, b.car_seed, a.car_no from sys_cars a left join sys_car_model b on a.car_seed=b.idx
			// 			WHERE a.car_seed IN ({$inStr}) AND a.status='1' ORDER BY car_seed ASC, car_no ASC" ;
			// $SQLCmd = "SELECT a.idx, b.car_seed, a.car_no from sys_cars a left join sys_car_model b on a.car_seed=b.idx
			// 			WHERE a.car_seed IN ({$inStr}) ORDER BY car_seed ASC, car_no ASC" ;
			$SQLCmd = "SELECT sc.idx, scm.car_seed, sc.car_no from sys_cars sc left join sys_car_model scm on sc.car_seed=scm.idx
						WHERE sc.car_seed IN ({$inStr}) AND sc.status in ( 1,2) ORDER BY sc.car_seed ASC, sc.car_no ASC" ;
			// dump( $SQLCmd) ;
			$query = $this->db->query( $SQLCmd) ;
			$arrCars = array() ;
			foreach ($query->rows as $key => $value) {
				$arrCars[$value['idx']] = array( $value['car_seed'],$value['car_no'], $value['idx']) ;
			}
		} else {
			$arrCars = array() ;
		}

		return $arrCars ;
	}

	public function getCarInfo( $useIdx) {
		$SQLCmd = "SELECT a.idx, b.idx AS car_seed_idx, b.car_model, b.car_seed, a.car_no, b.car_price, a.car_color FROM sys_cars a LEFT JOIN sys_car_model b ON a.car_seed=b.idx
				WHERE  a.idx='{$useIdx}' ORDER BY car_no ASC" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		$query->row['car_model_name'] = $this->getCarModelArr()[$query->row['car_model']] ;

		return $query->row ;
	}

	/**
	 * [getTotalCnt description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2017-11-28
	 */
	public function getTotalCnt( $data = array()) {
		$SQLCmd = "SELECT COUNT(*) AS total FROM sys_car_model" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;

		return $query->row['total'] ;
	}

	/**
	 * [getLists description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2017-11-28
	 */
	public function getLists( $data = array()) {
		$SQLCmd = "SELECT * FROM sys_car_model " ;
		$SQLCmd .= " ORDER BY {$data['sort']} {$data['order']}" ;

		// 分頁
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$SQLCmd .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;

		return $query->rows ;
	}

	public function getCarModelLists( $useIdx) {
		$inStr = join( ',', $useIdx) ;

		$SQLCmd = "SELECT idx, car_seed, car_model FROM sys_car_model WHERE idx in ({$inStr})" ;
		$query = $this->db->query( $SQLCmd) ;
		return $query->rows ;
	}

	public function jsonCarModelLists( $useIdx) {
		$inStr = join( ',', $useIdx) ;

		$SQLCmd = "SELECT idx, car_seed FROM sys_car_model WHERE car_model in ({$inStr})" ;
		$query = $this->db->query( $SQLCmd) ;
		return $query->rows ;
	}

	public function jsonCarModelList( $useIdx) {

		$SQLCmd = "SELECT idx, car_seed FROM sys_car_model WHERE car_model={$useIdx}" ;
		$query = $this->db->query( $SQLCmd) ;
		return $query->rows ;
	}
}