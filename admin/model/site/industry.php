<?php
class ModelSiteIndustry extends Model {

	/**
	 * [delUseIdx 刪除]
	 * @param   [type]     $useIdx [description]
	 * @return  [type]             [description]
	 * @Another Angus
	 * @date    2018-05-01
	 */
	public function delUseIdx ( $useIdx) {
		$SQLCmd = "UPDATE tb_minshuku SET status=3, editer = '" . $this->user->getUserName() . "', u_date = now()
			WHERE idx = '" . (int)$useIdx . "'" ;
		$this->db->query( $SQLCmd) ;

		$this->cache->delete('information') ;
	}

	/**
	 * [getColumns 列表欄位]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-05-01
	 */
	public function getColumns() {
		$columns = array() ;
		// $SQLCmd = "DESC sys_news" ;
		// $query = $this->db->query( $SQLCmd) ;

		// foreach ( $query->rows as $result) {
		// 	$columns[] = $result['Field'];
		// } 業者名稱、連絡人姓名、連絡人電話、連絡人地址
		$columns = array(
			"name"     => "業者名稱",
			"man_name" => "連絡人姓名",
			"tel"      => "連絡人電話",
			"email"    => "連絡人Mail",
			"address"  => "連絡人地址",
		) ;



		return $columns ;
	}

	/**
	 * [getInformation 表單]
	 * @param   [type]     $useIdx [description]
	 * @return  [type]             [description]
	 * @Another Angus
	 * @date    2018-05-01
	 */
	public function getInformation( $useIdx) {
		$SQLCmd = "SELECT * FROM tb_minshuku WHERE idx = '" . (int)$useIdx . "'" ;
 		$query = $this->db->query( $SQLCmd) ;

 		return $query->row ;
	}

	/**
	 * [addInformation 新增]
	 * @param   [type]     $data [description]
	 * @Another Angus
	 * @date    2018-05-01
	 */
	public function addInformation ( $data) {
		// dump( $this->session) ;
 		$SQLCmd = "INSERT INTO tb_minshuku SET
			name = '" . $this->db->escape($data['input_name']) . "', man_name = '" . $this->db->escape($data['input_man_name']) . "',
			tel = '" . $this->db->escape($data['input_tel']) . "', email = '" . $this->db->escape($data['input_mail']) . "',
			address = '" . $this->db->escape($data['input_address']) . "',
			creater = '" . $this->user->getUserName() . "', c_date = now(),
			status = '" . $this->db->escape($data['sel_status']) . "', data_type = 2" ;
		// dump( $SQLCmd) ;
		$this->db->query( $SQLCmd) ;
	}

	/**
	 * [editInformation 存檔]
	 * @param   [type]     $useIdx [description]
	 * @param   [type]     $data   [description]
	 * @return  [type]             [description]
	 * @Another Angus
	 * @date    2018-05-01
	 */
	public function editInformation ( $useIdx, $data) {
		$SQLCmd = "UPDATE tb_minshuku SET
			name = '" . $this->db->escape($data['input_name']) . "', man_name = '" . $this->db->escape($data['input_man_name']) . "',
			tel = '" . $this->db->escape($data['input_tel']) . "', email = '" . $this->db->escape($data['input_mail']) . "',
			address = '" . $this->db->escape($data['input_address']) . "',
			editer = '" . $this->user->getUserName() . "', u_date = now(),
		 	status = '" . $this->db->escape($data['sel_status']) . "' WHERE idx = '" . (int)$useIdx . "'" ;
		$this->db->query( $SQLCmd) ;
	}

	/**
	 * [getTotalCnt 業者數量] data_type = 2
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-05-01
	 */
	public function getTotalCnt( $data = array()) {
		$SQLCmd = "SELECT COUNT(*) AS total FROM tb_minshuku WHERE data_type = 2 AND status != 3" ;
		$implode = array();
		if (!empty($data['filter_name'])) {
			$implode[] = "name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		if (!empty($data['filter_man_name'])) {
			$implode[] = "man_name LIKE '%" . $this->db->escape($data['filter_man_name']) . "%'";
		}
		if (!empty($data['filter_tel'])) {
			$implode[] = "tel LIKE '%" . $this->db->escape($data['filter_tel']) . "%'";
		}
		if (!empty($data['filter_address'])) {
			$implode[] = "address LIKE '%" . $this->db->escape($data['filter_address']) . "%'";
		}
		if ($implode) {
			$SQLCmd .= " AND " . implode(" AND ", $implode);
		}

		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;

		return $query->row['total'] ;
	}

	/**
	 * [getLists 業者列表] data_type = 2
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2018-04-30
	 */
	public function getLists( $data = array()) {
		$sort_data = array(
			'name',
			'man_name',
			'tel',
			'email',
			'address',
		);

		$SQLCmd = "SELECT * FROM tb_minshuku WHERE data_type = 2 AND status != 3" ;

		$implode = array();
		if (!empty($data['filter_name'])) {
			$implode[] = "name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}
		if (!empty($data['filter_man_name'])) {
			$implode[] = "man_name LIKE '%" . $this->db->escape($data['filter_man_name']) . "%'";
		}
		if (!empty($data['filter_tel'])) {
			$implode[] = "tel LIKE '%" . $this->db->escape($data['filter_tel']) . "%'";
		}
		if (!empty($data['filter_address'])) {
			$implode[] = "address LIKE '%" . $this->db->escape($data['filter_address']) . "%'";
		}
		if ($implode) {
			$SQLCmd .= " AND " . implode(" AND ", $implode);
		}


		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$SQLCmd .= " ORDER BY " . $data['sort'];
		} else {
			$SQLCmd .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$SQLCmd .= " DESC";
		} else {
			$SQLCmd .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$SQLCmd .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;

		return $query->rows ;
	}
}