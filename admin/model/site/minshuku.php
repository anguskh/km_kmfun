<?php
class ModelSiteMinshuku extends Model {

	/**
	 * [delUseIdx 刪除]
	 * @param   [type]     $useIdx [description]
	 * @return  [type]             [description]
	 * @Another Angus
	 * @date    2018-05-01
	 */
	public function delUseIdx ( $useIdx) {
		$SQLCmd = "UPDATE tb_minshuku SET status=3, editer = '" . $this->user->getUserName() . "', u_date = now()
			WHERE idx = '" . (int)$useIdx . "'" ;
		$this->db->query( $SQLCmd) ;
	}

	public function getColumns() {
		$columns = array() ;
		$SQLCmd = "DESC tb_minshuku" ;
		$query = $this->db->query( $SQLCmd) ;

		foreach ( $query->rows as $result) {
			$columns[] = $result['Field'];
		}

		return $columns ;
	}

	public function getInformation( $useIdx) {
		$SQLCmd = "SELECT * FROM tb_minshuku WHERE idx = '" . (int)$useIdx . "'" ;
 		$query = $this->db->query( $SQLCmd) ;

 		return $query->row ;
	}

	public function addInformation ( $data) {
		$ary_service = $data['service'];
		$service = implode(";",$ary_service);

		$imageIntro = serialize( array(
			$data['imageIntro1'], $data['imageIntro2'], $data['imageIntro3'],$data['imageIntro4'],
			$data['imageIntro5'], $data['imageIntro6'], $data['imageIntro7'], $data['imageIntro8']
		)) ;

		$SQLCmd = "INSERT INTO tb_minshuku SET
			name = '{$this->db->escape( $data['input_name'])}', start = '{$this->db->escape( $data['input_start'])}',
			pos = '{$this->db->escape( $data['input_pos'])}', pos1 = '{$this->db->escape( $data['input_pos1'])}',
			room_type = '{$this->db->escape( $data['input_room_type'])}', room_cnt = '{$this->db->escape( $data['input_room_cnt'])}',
			price = '{$this->db->escape( $data['input_price'])}', tel = '{$this->db->escape( $data['input_tel'])}', fax = '{$this->db->escape( $data['input_fax'])}',
			email = '{$this->db->escape( $data['input_email'])}', url = '{$this->db->escape( $data['input_url'])}',
			post_id = '{$this->db->escape( $data['input_post_id'])}', address = '{$this->db->escape( $data['input_address'])}',
			km_type = '{$this->db->escape( $data['input_km_type'])}', reg_no = '{$this->db->escape( $data['input_reg_no'])}',
			approved_date = '{$this->db->escape( $data['input_approved_date'])}', google_map = '{$this->db->escape( $data['input_google_map'])}',
			service = '{$this->db->escape($service)}' , imageIntro = '{$imageIntro}',
			device_text = '{$this->db->escape( $data['input_device_text'])}', rule_text = '{$this->db->escape( $data['input_rule_text'])}',
			service_text = '{$this->db->escape( $data['input_service_text'])}', cancel_text = '{$this->db->escape( $data['input_cancel_text'])}',
			creater = '" . $this->user->getUserName() . "', c_date = now(),
			status = '" . $this->db->escape($data['sel_status']) . "', data_type = 1" ;
		// dump( $SQLCmd) ;
		$this->db->query( $SQLCmd) ;
		$useIdx = mysql_insert_id();
		$this->process_room($useIdx, $data);
	}

	public function editInformation ( $useIdx, $data) {

		$ary_service = $data['service'];
		$service = implode(";",$ary_service);

		$imageIntro = serialize( array(
			$data['imageIntro1'], $data['imageIntro2'], $data['imageIntro3'],$data['imageIntro4'],
			$data['imageIntro5'], $data['imageIntro6'], $data['imageIntro7'], $data['imageIntro8']
		)) ;

		$SQLCmd = "UPDATE tb_minshuku SET
			name = '{$this->db->escape( $data['input_name'])}', start = '{$this->db->escape( $data['input_start'])}',
			pos = '{$this->db->escape( $data['input_pos'])}', pos1 = '{$this->db->escape( $data['input_pos1'])}',
			room_type = '{$this->db->escape( $data['input_room_type'])}', room_cnt = '{$this->db->escape( $data['input_room_cnt'])}',
			price = '{$this->db->escape( $data['input_price'])}', tel = '{$this->db->escape( $data['input_tel'])}', fax = '{$this->db->escape( $data['input_fax'])}',
			email = '{$this->db->escape( $data['input_email'])}', url = '{$this->db->escape( $data['input_url'])}',
			post_id = '{$this->db->escape( $data['input_post_id'])}', address = '{$this->db->escape( $data['input_address'])}',
			km_type = '{$this->db->escape( $data['input_km_type'])}', reg_no = '{$this->db->escape( $data['input_reg_no'])}',
			approved_date = '{$this->db->escape( $data['input_approved_date'])}', google_map = '{$this->db->escape( $data['input_google_map'])}',
			service = '{$this->db->escape($service)}' , imageIntro = '{$imageIntro}',
			device_text = '{$this->db->escape( $data['input_device_text'])}', rule_text = '{$this->db->escape( $data['input_rule_text'])}',
			service_text = '{$this->db->escape( $data['input_service_text'])}', cancel_text = '{$this->db->escape( $data['input_cancel_text'])}',
			editer = '" . $this->user->getUserName() . "', u_date = now(),
		 	status = '" . $this->db->escape($data['sel_status']) . "'
				WHERE idx = '" . (int)$useIdx . "'" ;
		// echo($SQLCmd);
		// exit();
		$this->db->query( $SQLCmd) ;
		$this->process_room($useIdx, $data);
	}

	public function process_room($useIdx, $data)
	{
		// dump($data);
		// exit();
		$SQLCmd = "DELETE FROM tbl_room_type WHERE src_idx = '{$useIdx}'";
		$this->db->query( $SQLCmd) ;
		foreach ($data['room']['name'] as $key => $name) {
			$ary_device = array();
			if (isset($data['room']["wifi"][$key]) && $data['room']["wifi"][$key] =="1") {
				$ary_device[]="wifi";
			}
			if (isset($data['room']["bath"][$key]) && $data['room']["bath"][$key] =="1") {
				$ary_device[]="bath";
			}
			$device = implode(";",$ary_device);
			$SQLCmd = "INSERT tbl_room_type SET
				name = '{$this->db->escape( $name)}',
				base_price = '{$this->db->escape($data['room']['base_price'][$key])}',
				per_cnt = '{$this->db->escape($data['room']['per_cnt'][$key])}',
				extra_per_cnt = '{$this->db->escape($data['room']['extra_per_cnt'][$key])}',
				image = '{$this->db->escape($data['room']['image'][$key])}',
				device = '{$device}',
				src_idx = '{$useIdx}'";
			echo($SQLCmd );
			echo("</br>");
			$this->db->query( $SQLCmd) ;
		}
		// exit();
	}

	public function getRoomData($useIdx){
		$SQLCmd = "SELECT * FROM tbl_room_type WHERE src_idx = '{$useIdx}' ";
 		$query = $this->db->query( $SQLCmd) ;
 		return $query->rows ;
	}

	public function getTotalCnt($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM tb_minshuku WHERE data_type = 1 AND status != 3 ";

		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_pos1'])) {
			$implode[] = "pos1 LIKE '%" . $this->db->escape($data['filter_pos1']) . "%'";
		}

		if (!empty($data['filter_room_type'])) {
			$implode[] = "room_type LIKE '%" . $this->db->escape($data['filter_room_type']) . "%'";
		}

		if (!empty($data['filter_address'])) {
			$implode[] = "address LIKE '%" . $this->db->escape($data['filter_address']) . "%'";
		}

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
		// dump($sql) ;
		$query = $this->db->query($sql) ;

		return $query->row['total'] ;
	}

	public function getLists($data = array()) {
		$sql = "SELECT * FROM tb_minshuku WHERE data_type = 1 AND status != 3 ";

		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_pos1'])) {
			$implode[] = "pos1 LIKE '%" . $this->db->escape($data['filter_pos1']) . "%'";
		}

		if (!empty($data['filter_room_type'])) {
			$implode[] = "room_type LIKE '%" . $this->db->escape($data['filter_room_type']) . "%'";
		}

		if (!empty($data['filter_address'])) {
			$implode[] = "address LIKE '%" . $this->db->escape($data['filter_address']) . "%'";
		}

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}

		$sort_data = array(
			"name",
			"pos",
			"pos1",
			"room_type",
			"room_cnt",
			"price",
			"tel",
			"address"
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		// dump($sql) ;
		$query = $this->db->query($sql);

		return $query->rows ;
	}

	/**
	 * [getListFromAjax 非同步取民宿名單]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2018-01-10
	 */
	public function getListFromAjax ( $data = array()) {
		$whereStr = "" ;
		if ( isset( $data['name']) && !empty( $data['name'])) {
			$whereStr = "AND name like '%{$data['name']}%'" ;
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$SQLCmd .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		if ( !empty( $whereStr)) {
			$SQLCmd = "SELECT idx, name FROM tb_minshuku WHERE status != 3 {$whereStr}" ;
			// dump( $SQLCmd) ;
			$query = $this->db->query( $SQLCmd) ;
			return $query->rows ;
		} else {
			return array() ;
		}
	}

	/**
	 * [checkMinsuName description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2018-01-21
	 */
	public function checkMinsuName( $data = array()) {
		if ( isset($data['name'])) {
			$whereStr = "AND name = '{$data['name']}'" ;
			$SQLCmd = "SELECT idx, name FROM tb_minshuku WHERE 1=1 {$whereStr}" ;
			$query = $this->db->query( $SQLCmd) ;
			return $query->rows ;
		} else {
			return array() ;
		}
	}
}