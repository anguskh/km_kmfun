<?php
class ModelSitePickup extends Model {

	/**
	 * [getTotalCnt 取得今天以內的訂單數量]
	 * @param  array  $data filter_data 過瀘條件
	 * @return [type]       [description]
	 */
	public function getTotalCnt( $data = array()) {

		$SQLCmd = "SELECT COUNT(*) AS total FROM tb_order WHERE 1=1 " ;
		if ( isset( $data['dateRange']) && is_array( $data['dateRange'])) {
			$SQLCmd .= " AND rent_date_out > '{$data['dateRange'][0]}' AND rent_date_out < '{$data['dateRange'][1]}'" ;
		}
		if ( isset( $data['order_status']) && !is_numeric( $data['order_status'])) {
			$SQLCmd .= " AND order_status {$data['order_status']}" ;
		} else if ( is_numeric( $data['order_status'])) {
			$SQLCmd .= " AND order_status ='{$data['order_status']}'" ;
		}

		if (!empty($data['filter_order_id'])) {
			$SQLCmd .= " AND agreement_no like '{$this->db->escape( $data['filter_order_id'])}%'" ;
		}
		if (!empty($data['filter_order_type'])) {
			$SQLCmd .= " AND order_type = '{$this->db->escape( $data['filter_order_type'])}'" ;
		}
		if (!empty($data['filter_order_status'])) {
			if ( $data['filter_order_status'] == 'x') {
				$SQLCmd .= " AND order_status = '{$this->db->escape( $data['filter_order_status'])}'" ;
			} else if ( $data['filter_order_status'] == "success"){
				$SQLCmd .= " AND order_status IS NULL" ;
			}
		}
		if (!empty($data['filter_customer'])) {
			$SQLCmd .= " AND rent_user_name like '%{$this->db->escape( $data['filter_customer'])}%'" ;
		}
		if (!empty($data['filter_mobile'])) {
			$SQLCmd .= " AND rent_user_tel like '%{$this->db->escape( $data['filter_mobile'])}%'" ;
		}
		if (!empty($data['filter_station'])) {
			$SQLCmd .= " AND s_station = '{$this->db->escape( $data['filter_station'])}'" ;
		}
		if (!empty($data['filter_date_out'])) {
			$SQLCmd .= " AND DATE_FORMAT(rent_date_out,'%Y-%m-%d') = '{$this->db->escape( $data['filter_date_out'])}'" ;
		}

		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;

		return $query->row['total'] ;
	}

	/**
	 * [getLists 取得今天以內的訂單數量]
	 * @param  array  $data filter_data 過瀘條件
	 * @return [type]       [description]
	 */
	public function getLists( $data = array()) {
		// dump( $data) ;
		$whereArr = array() ;
		$SQLCmd  = "SELECT * FROM tb_order WHERE 1=1 " ;
		$SQLCmd2 = "SELECT * FROM tb_order WHERE " ;
		if ( isset( $data['dateRange']) && is_array( $data['dateRange'])) {
			$SQLCmd .= " AND rent_date_out > '{$data['dateRange'][0]}' AND rent_date_out < '{$data['dateRange'][1]}'" ;
			$whereArr[] = " ( rent_date_out > '{$data['dateRange'][0]}' AND rent_date_out < '{$data['dateRange'][1]}')" ;
		}
		if ( isset( $data['order_status']) && !is_numeric( $data['order_status'])) {
			// dump( "in") ;
			$SQLCmd .= " AND order_status {$data['order_status']}" ;
			$whereArr[] = " order_status {$data['order_status']}" ;
		} else if ( is_numeric( $data['order_status'])) {
			$SQLCmd .= " AND order_status ='{$data['order_status']}'" ;
			$whereArr[] .= " order_status ='{$data['order_status']}'" ;
		}
		if (!empty($data['filter_order_id'])) {
			$SQLCmd .= " AND agreement_no like '{$this->db->escape( $data['filter_order_id'])}%'" ;
		}
		if (!empty($data['filter_order_type'])) {
			$SQLCmd .= " AND order_type = '{$this->db->escape( $data['filter_order_type'])}'" ;
		}
		if (!empty($data['filter_order_status'])) {
			if ( $data['filter_order_status'] == 'x') {
				$SQLCmd .= " AND order_status = '{$this->db->escape( $data['filter_order_status'])}'" ;
			} else if ( $data['filter_order_status'] == "success"){
				$SQLCmd .= " AND order_status IS NULL" ;
			}
		}
		if (!empty($data['filter_customer'])) {
			$SQLCmd .= " AND rent_user_name like '%{$this->db->escape( $data['filter_customer'])}%'" ;
		}
		if (!empty($data['filter_mobile'])) {
			$SQLCmd .= " AND rent_user_tel like '%{$this->db->escape( $data['filter_mobile'])}%'" ;
		}
		if (!empty($data['filter_station'])) {
			$SQLCmd .= " AND s_station = '{$this->db->escape( $data['filter_station'])}'" ;
		}
		if (!empty($data['filter_date_out'])) {
			$SQLCmd .= " AND DATE_FORMAT(rent_date_out,'%Y-%m-%d') = '{$this->db->escape( $data['filter_date_out'])}'" ;
		}


		// dump( $whereArr) ;

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$SQLCmd .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;

		return $query->rows ;
	}

	/**
	 * [getInformation 取得訂單內容]
	 * @param  [type] $useIdx [description]
	 * @return [type]         [description]
	 */
	public function getInformation( $useIdx) {
		$SQLCmd = "SELECT tor.*, toc.image1, toc.image2, toc.image3, toc.image4, toc.add_name, toc.add_time, toc.check_name
					FROM tb_order tor
					left join tb_order_carcheck toc ON toc.agreement_no = tor.agreement_no
					WHERE tor.order_id = '" . (int)$useIdx . "' " ;
 		$query = $this->db->query( $SQLCmd) ;
 		// dump( $SQLCmd) ;
 		return $query->row ;
	}

	/**
	 * [getCarConditionInfo 取得車輛狀態]
	 * @param   [type]     $car_sn [description]
	 * @return  [type]             [description]
	 * @Another Angus
	 * @date    2017-11-13
	 */
	public function getCarConditionInfo( $car_sn) {
		$SQLCmd = "SELECT * FROM tb_car_condition WHERE car_sn={$car_sn} ORDER BY agreement_no DESC " ;
		$query = $this->db->query( $SQLCmd) ;
		// dump( $SQLCmd) ;
		// dump( $query->rows) ;
		// dump( $query->row) ;
		return $query->row ;
	}

	/**
	 * [addInformation 新增交車車輛狀態]
	 * @param   int        $useIdx              [description]
	 * @param   array      $data                [description]
	 * @param   array      $carConditionInfoArr [description]
	 * @Another Angus
	 * @date    2017-11-15
	 */
	public function addInformation( $useIdx, $data = array(), $carConditionInfoArr = array()) {
		header("Content-Type: text/html;charset=utf-8");
		$now = date('Y-m-d H:i:s') ;

		// dump( $data) ;
		// dump( $carConditionInfoArr) ;
		// dump( $this->session);
		$user_name = $this->session->data['user_name'] ;
		//寫入車輛檢核圖片
		//处理图片 2020.07.24 先隱藏此圖
		/*$img_array = array('image1','image2','image3','image4');
		for($i=0; $i<count($img_array); $i++){
			if(isset($_FILES[$img_array[$i]]['tmp_name']) && $_FILES[$img_array[$i]]['tmp_name']!=''){
				$new_file_name = $_FILES[$img_array[$i]]["name"];
				$path = 'checkcar/';
				$n_path = $data['agreement_no'].'/';
				if (!file_exists(DIR_IMAGE . $path)) {
					@mkdir(DIR_IMAGE . $path, 0777);
				}

				if (!file_exists(DIR_IMAGE . $path.$n_path)) {
					@mkdir(DIR_IMAGE . $path.$n_path, 0777);
				}

				$filepath = DIR_IMAGE.$path.$n_path;
				$new_filepath = $filepath.$new_file_name;
				if($_FILES[$img_array[$i]]["tmp_name"]!='')
				{
					move_uploaded_file($_FILES[$img_array[$i]]["tmp_name"] , $new_filepath ) ;
				}
				$data[$img_array[$i]] = $new_file_name;
			}else{
				$data[$img_array[$i]] = "";
			}
		}*/

		/*$SQLCmd = "INSERT INTO tb_order_carcheck SET  agreement_no = '{$data['agreement_no']}',
					image1 = '{$data['image1']}',
					image2 = '{$data['image2']}',
					image3 = '{$data['image3']}',
					image4 = '{$data['image4']}',
					add_name = '{$data['add_name']}',
					check_name = '{$data['check_name']}',
					add_time = '{$data['add_time']}',
					create_date = now(), create_man = '{$user_name}'" ;*/
		$SQLCmd = "INSERT INTO tb_order_carcheck SET  agreement_no = '{$data['agreement_no']}',
					add_name = '{$data['add_name']}',
					check_name = '{$data['check_name']}',
					add_time = '{$data['add_time']}',
					create_date = now(), create_man = '{$user_name}'" ;

		// dump( $SQLCmd) ;
		$this->db->query( $SQLCmd) ;

		foreach ($data['car_sn'] as $iCnt => $car_sn) {
			$jsonConditionInfo = json_encode( $carConditionInfoArr[$car_sn]) ;
			$SQLCmd = "INSERT INTO tb_car_condition SET  agreement_no = '{$data['agreement_no']}', car_sn = '{$car_sn}', car_no = '{$data['car_no'][$iCnt]}', condition_before = '{$jsonConditionInfo}', create_date = now(), create_man = '{$user_name}', sign_out = '{$data['signature'][$iCnt]}' " ;
			// dump( $SQLCmd) ;
			$this->db->query( $SQLCmd) ;
		}

		// 變更訂單狀態 todo  暫時用1 代替
		$SQLCmd = "INSERT INTO tb_order_bak SELECT * FROM tb_order WHERE agreement_no = '{$data['agreement_no']}'" ;
		$this->db->query( $SQLCmd) ;

		$SQLCmd = " UPDATE tb_order SET order_status = '1', updateUser='{$this->session->data['user_name']}', updateDate='{$now}' WHERE agreement_no = '{$data['agreement_no']}'" ;
		// dump( $SQLCmd) ;
		$this->db->query( $SQLCmd) ;
	}

	public function updateInformation ( $useIdx, $data = array(), $carConditionInfoArr = array()) {
		// dump( $data) ;
		// dump( $carConditionInfoArr) ;
		// exit();
		$user_name = $this->session->data['user_name'] ;
		foreach ($data['car_sn'] as $iCnt => $car_sn) {
			$jsonConditionInfo = json_encode( $carConditionInfoArr[$car_sn]) ;
			$SQLCmd = "UPDATE tb_car_condition SET condition_after = '{$jsonConditionInfo}', return_date = now(), return_man = '{$user_name}', sign_in = '{$data['signature'][$iCnt]}' WHERE agreement_no = '{$data['agreement_no']}' AND car_sn = '{$car_sn}'" ;
			$this->db->query( $SQLCmd) ;
		}
		//還車時也要更新 check_name與add_name
		$SQLCmd = " UPDATE tb_order_carcheck SET check_name = '{$data['check_name']}',add_name = '{$data['add_name']}' WHERE agreement_no = '{$data['agreement_no']}'" ;
		// dump( $SQLCmd) ;
		$this->db->query( $SQLCmd) ;

		// 變更訂單狀態 todo  暫時用1 代替
		$SQLCmd = "INSERT INTO tb_order_bak SELECT * FROM tb_order WHERE agreement_no = '{$data['agreement_no']}'" ;
		$this->db->query( $SQLCmd) ;

		$up_edit = "";
		if($data['now_pickup'] == 'N'){
			//要更新 還車地點、營業損失、費用支付方式
			$up_edit .= ", e_station = '{$data['e_station']}'";
			$up_edit .= ", business_lose = '{$data['business_lose']}'";
			$up_edit .= ", car_lose = '{$data['car_lose']}'";
			$up_edit .= ", lose_type = '{$data['lose_type']}'";
			$up_edit .= ", overTime_cost = '{$data['overTime_cost']}'";
			$up_edit .= ", other_lose = '{$data['other_lose']}'";
			$up_edit .= ", rent_miles_in = '{$data['rent_miles_in']}'";
			$up_edit .= ", oil_cost = '{$data['oil_cost']}'";

			//要回寫備註
			$SQLCmd_memo = "UPDATE tb_scheduling SET memo = '{$data['area_memo']}' WHERE order_no='{$data['agreement_no']}' " ;
			$this->db->query( $SQLCmd_memo) ;

		}
		$up_edit .= ", pay_type = '{$data['pay_type']}', rent_date_in = now(), in_man='{$user_name}'";
		$SQLCmd = " UPDATE tb_order SET order_status = '2' {$up_edit} , updateUser='{$this->session->data['user_name']}', updateDate='{$now}' WHERE agreement_no = '{$data['agreement_no']}'" ;
		// dump( $SQLCmd) ;
		$this->db->query( $SQLCmd) ;
	}

	/**
	 * [getCarConditionInfoPickup 還車時，取回交車時的狀態]
	 * @param   string     $car_sn       車輛流水號
	 * @param   string     $agreement_no 訂單編號
	 * @return  [type]                   [description]
	 * @Another Angus
	 * @date    2017-11-22
	 */
	public function getCarConditionInfoPickup( $car_sn="", $agreement_no="") {
		$SQLCmd = "SELECT * FROM tb_car_condition WHERE car_sn={$car_sn} AND agreement_no='{$agreement_no}'" ;
		$query = $this->db->query( $SQLCmd) ;
		// dump( $SQLCmd) ;
		// dump( $query->row) ;
		return $query->row ;
	}

	/**
	 * [checkIsOrder 檢查訂單是否存在]
	 * @param   [string]     $order_id     [description]
	 * @param   [string]     $agreement_no [description]
	 * @return  [type]                     [description]
	 * @Another Angus
	 * @date    2017-11-15
	 */
	public function checkIsOrder( $order_id, $agreement_no) {
		$SQLCmd = "SELECT * FROM tb_order WHERE order_id='%s' AND agreement_no='%s'" ;
		$SQLCmd = sprintf( $SQLCmd, $order_id, $agreement_no) ;
		$query = $this->db->query( $SQLCmd) ;

		if ( isset( $query->row['order_id'])) return true ;
		else return false ;
	}
}