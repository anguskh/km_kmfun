<?php
class ModelSiteNews extends Model {

	public function delUseIdx ( $useIdx) {
		$this->db->query("DELETE FROM sys_news WHERE idx = '" . (int)$useIdx . "'") ;

		// $this->cache->delete('information');
	}

	public function getColumns() {
		$columns = array() ;
		$SQLCmd = "DESC sys_news" ;
		$query = $this->db->query( $SQLCmd) ;

		foreach ( $query->rows as $result) {
			$columns[] = $result['Field'];
		}

		return $columns ;
	}

	public function getInformation( $useIdx) {
		$SQLCmd = "SELECT * FROM sys_news WHERE idx = '" . (int)$useIdx . "'" ;
 		$query = $this->db->query( $SQLCmd) ;

 		return $query->row ;
	}

	public function addInformation ( $data) {
		// dump( $this->session) ;
		$SQLCmd = "INSERT INTO sys_news SET
			title = '" . $this->db->escape($data['input_title']) . "', description = '" . $this->db->escape($data['area_description']) . "',
			creater = '" . $this->user->getUserName() . "', c_date = now(), online_date = '" . $this->db->escape($data['input_online_date']) . "',
			status = '" . $this->db->escape($data['sel_status']) . "'" ;
		// dump( $SQLCmd) ;
		$this->db->query( $SQLCmd) ;
	}

	public function editInformation ( $useIdx, $data) {
		$SQLCmd = "UPDATE sys_news SET
			title = '" . $this->db->escape($data['input_title']) . "', description = '" . $this->db->escape($data['area_description']) . "',
			editer = '" . $this->user->getUserName() . "', u_date = now(), online_date = '" . $this->db->escape($data['input_online_date']) . "',
		 	status = '" . $this->db->escape($data['sel_status']) . "' WHERE idx = '" . (int)$useIdx . "'" ;
		$this->db->query( $SQLCmd) ;
	}

	public function getTotalCnt() {
		$SQLCmd = "SELECT COUNT(*) AS total FROM sys_news" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;

		return $query->row['total'] ;
	}

	public function getLists( $data = array()) {
		$SQLCmd = "SELECT * FROM sys_news " ;

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$SQLCmd .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query( $SQLCmd) ;

		return $query->rows ;
	}
}