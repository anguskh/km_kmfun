<?php
class ModelSiteZip extends Model {

	public function getZipStr( $zipcode = "") {
		$SQLCmd = "SELECT zip_name, zip_code FROM sys_zip WHERE zip_code='{$zipcode}' " ;
		$query = $this->db->query( $SQLCmd) ;
		return $query->row ;
	}

	/**
	 * [getZipCode description]
	 * @param   string     $zipMain [區碼 未設定:台北市,新北市 有設定:士林區,信義區]
	 * @return  [type]              [description]
	 * @Another Angus
	 * @date    2017-12-13
	 */
	public function getZipCode( $zipMain = "") {
		// dump( $zipMain) ;
		$SQLCmd = "SELECT zip_name, zip_code FROM sys_zip WHERE 1 " ;

		if ( $zipMain == "all") {
			$whereStr = "" ;
		} else if ( !empty( $zipMain)) {
			$whereStr = " AND parent_id={$zipMain}" ;
		} else {
			$whereStr = " AND parent_id=0" ;
		}
		$SQLCmd .= $whereStr ;

		$query = $this->db->query( $SQLCmd) ;
		return $query->rows ;
	}

	public function getAllZipCode () {
		$SQLCmd = "SELECT zip_name, zip_code FROM sys_zip WHERE 1 " ;
		$query = $this->db->query( $SQLCmd) ;

		$retArr = array() ;
		foreach ($query->rows as $iCnt => $tmpVal) {
			$retArr[ $tmpVal['zip_code']] = $tmpVal['zip_name'] ;
		}
		return $retArr ;
	}
}

