<?php
class ModelSiteCars extends Model {

	public function updateIn( $order_id, $other_car) {
		$now = date('Y-m-d H:i:s') ;
		$SQLCmd = "INSERT INTO tb_order_bak SELECT * FROM tb_order WHERE order_id='{$order_id}'" ;
		$this->db->query( $SQLCmd) ;

		$SQLCmd = "UPDATE tb_order SET other_car='{$other_car}', updateUser='{$this->session->data['user_name']}', updateDate='{$now}' WHERE order_id = '{$order_id}'" ;
		$this->db->query( $SQLCmd) ;
	}

	public function delUseIdx ( $useIdx) {
		$SQLCmd = "UPDATE sys_cars SET status='x' WHERE idx = '" . (int)$useIdx . "'" ;
		// dump( $SQLCmd) ;
		$this->db->query( $SQLCmd) ;
	}

	public function getColumns() {
		$columns = array() ;
		$SQLCmd = "DESC sys_cars" ;
		$query = $this->db->query( $SQLCmd) ;

		foreach ( $query->rows as $result) {
			$columns[] = $result['Field'];
		}

		return $columns ;
	}

	/**
	 * [getInformations description]
	 * @param   [type]     $useIdx [description]
	 * @return  [type]             [description]
	 * @Another Angus
	 * @date    2018-05-26
	 */
	public function getInformations( $useIdx) {
		$SQLCmd = "SELECT * FROM sys_cars WHERE idx = '" . (int)$useIdx . "'" ;
 		$query = $this->db->query( $SQLCmd) ;

 		return $query->rows ;
	}

	public function getInformation( $useIdx) {
		$SQLCmd = "SELECT * FROM sys_cars WHERE idx = '" . (int)$useIdx . "'" ;
 		$query = $this->db->query( $SQLCmd) ;

 		return $query->row ;
	}

	public function addInformation ( $data) {
		// dump( $data) ;
		$SQLCmd = "INSERT INTO sys_cars SET
			car_seed = '" . $this->db->escape($data['sel_car_seed']) . "',
			car_no = '". $this->db->escape( $data['input_car_no'])."',
			car_color = '" . $this->db->escape($data['input_car_color']) . "',
			status = '" . $this->db->escape($data['sel_status']) . "'" ;
		// dump( $SQLCmd) ;
		$this->db->query( $SQLCmd) ;
	}

	public function editInformation ( $useIdx, $data) {
		$SQLCmd = "UPDATE sys_cars SET
			car_seed = '" . $this->db->escape($data['sel_car_seed']) . "',
			car_no = '". $this->db->escape( $data['input_car_no'])."',
			car_color = '" . $this->db->escape($data['input_car_color']) . "',
			status = '" . $this->db->escape($data['sel_status']) . "'
		 	WHERE idx = '" . (int)$useIdx . "'" ;
		 // dump( $SQLCmd) ;
		$this->db->query( $SQLCmd) ;
	}

	public function getTotalCnt( $data = array()) {
		if ( count( $data['car_seed']) > 0) {
			$SQLCmd = "SELECT COUNT(*) AS total FROM sys_cars" ;
			$SQLCmd .= " WHERE status != 'x'" ;

			if (!empty($data['car_seed'])) {
				$inStr = join( ',', $data['car_seed']) ;

				$SQLCmd .= " AND car_seed in ({$inStr})" ;
			}

			$query = $this->db->query( $SQLCmd) ;

			return $query->row['total'] ;
		} else {
			return 0 ;
		}
	}

	public function getLists( $data = array()) {
		if ( count( $data['car_seed']) > 0) {
			$SQLCmd = "SELECT * FROM sys_cars " ;
			$SQLCmd .= " WHERE status != 'x'" ;

			if (!empty($data['car_seed'])) {
				$inStr = join( ',', $data['car_seed']) ;

				$SQLCmd .= " AND car_seed in ({$inStr})" ;
			}
			$SQLCmd .= " ORDER BY {$data['sort']} {$data['order']}" ;

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$SQLCmd .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}
			$query = $this->db->query( $SQLCmd) ;

			return $query->rows ;
		} else {
			return array() ;
		}
	}

	/**
	 * [getCarNoForList description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2018-05-30
	 */
	public function getCarNoForList( $data = array()) {
		// dump( $data) ;
		$inStr = join( ',', $data) ;
		$SQLCmd = "SELECT * FROM sys_cars WHERE idx IN ({$inStr}) AND status != 'x'" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		return $query->rows ;
	}


	/**
	 * [getCarNoForAax description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2018-02-14
	 */
	public function getCarNoForAjax( $data = array()) {
		$whereStr = "" ;
		if ( isset( $data['car_seed']) && !empty( $data['car_seed'])) {
			$whereStr = "AND car_seed = '{$data['car_seed']}'" ;
		}
		if ( isset( $data['car_no']) && !empty( $data['car_no'])) {
			$whereStr .= "AND car_no LIKE '%{$data['car_no']}%'" ;
		}

		if ( !empty( $whereStr)) {
			$SQLCmd = "SELECT idx, car_no FROM sys_cars WHERE 1=1 {$whereStr} AND status != 'x'" ;
			// dump( $SQLCmd) ;
			$query = $this->db->query( $SQLCmd) ;
			return $query->rows ;
		} else {
			return array() ;
		}
	}

	//�ˬdcar_no���C��
	public function getCarNoColor($whereStr = "") {
		$SQLCmd = "SELECT idx FROM sys_cars WHERE 1=1 {$whereStr}" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		return $query->rows ;
	}

}