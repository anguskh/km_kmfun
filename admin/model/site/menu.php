<?php
class ModelSiteMenu extends Model {

	public function getMenuStructure( ) {
		$menuParent = array() ;
		$menuInfo = array() ;

		$SQLCmd = "SELECT * FROM sys_backend_menu order by seq asc, idx asc" ;
 		$query = $this->db->query( $SQLCmd) ;

 		foreach ($query->rows as $cnt => $row) {
 			$menuParent[ $row['parent']][] = $row['idx'] ;
 			$menuInfo[$row['idx']] = $row ;
 		}

 		return array( $menuParent, $menuInfo) ;
	}


	/**
	 * [getOtherCarIsNull 調整正式機資料 by Angus 2018.06.09]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-06-09
	 */
	public function getOtherCarIsNull() {
		$SQLCmd = "SELECT * FROM tb_order WHERE other_car IS NULL" ;
		$query = $this->db->query( $SQLCmd) ;
		return $query->rows ;
	}

	public function getSchedulingCars ( $order_no = "") {
		$SQLCmd = "SELECT * FROM tb_scheduling WHERE order_no='{$order_no}'" ;
		$query = $this->db->query( $SQLCmd) ;
		return $query->rows ;
	}
	public function getCarInfo( $car_no = "", $car_sn = '') {
		$SQLCmd = "select * from sys_cars where car_no='{$car_no}' and idx='{$car_sn}'" ;
		$query = $this->db->query( $SQLCmd) ;
		// dump( $query->rows) ;
		return $query->rows ;
	}
	public function updateSQL( $order_id = "", $strOtherCar = "") {
		$SQLCmd = "INSERT INTO tb_order_bak SELECT * FROM tb_order WHERE order_id='{$order_id}'" ;
		$this->db->query( $SQLCmd) ;

		$SQLCmd = "update tb_order set other_car='{$strOtherCar}' where order_id='{$order_id}'" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
	}

	/**
	 * [getOrderTypeNone11 description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-06-19
	 */
	public function getOrderTypeNone11(){
		$SQLCmd = "SELECT * FROM tb_order WHERE order_type != 11" ;
		$query = $this->db->query( $SQLCmd) ;
		return $query->rows ;
	}

	public function delScheduling( $orderNoArr = array()) {
		$inStr = join( ',', $orderNoArr) ;

		$SQLCmd = "DELETE FROM tb_scheduling WHERE order_no in ( {$inStr})" ;
		$query = $this->db->query( $SQLCmd) ;
		// dump( count( $query->rows)) ;

		$SQLCmd = "DELETE FROM tb_flight WHERE order_no in ( {$inStr})" ;
		$query = $this->db->query( $SQLCmd) ;
		// dump( count( $query->rows)) ;
	}

	public function delOrderTypeNone11() {
		$SQLCmd = "DELETE FROM tb_order WHERE order_type != 11" ;
		$query = $this->db->query( $SQLCmd) ;
	}



}
