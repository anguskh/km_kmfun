<?php
class ModelSiteScheduling extends Model {

	public function delUseIdx ( $useIdx) {
		$now = date('Y-m-d H:i:s') ;
		$ret = $this->db->query("SELECT order_no FROM tb_scheduling WHERE idx={$useIdx}") ;
		if ( isset($ret->row['order_no'])) {
			$SQLCmd = "INSERT INTO tb_order_bak SELECT * FROM tb_order WHERE agreement_no='{$ret->row['order_no']}'" ;
			$this->db->query( $SQLCmd) ;

			$this->db->query("UPDATE tb_order SET order_status='x', updateUser='{$this->session->data['user_name']}', updateDate='{$now}' WHERE agreement_no='{$ret->row['order_no']}'") ;
		}

		$SQLCmd = "UPDATE tb_scheduling SET status=2, updateUser='{$this->session->data['user_name']}', updateTime='{$now}' WHERE idx={$useIdx}" ;
		// dump( $SQLCmd) ;
		$this->db->query( $SQLCmd) ;

		// $this->cache->delete('information');
	}

	public function getColumns() {
		$columns = array() ;
		$SQLCmd = "DESC sys_news" ;
		$query = $this->db->query( $SQLCmd) ;

		foreach ( $query->rows as $result) {
			$columns[] = $result['Field'];
		}

		return $columns ;
	}

	public function getInformation( $useIdx, $jobId) {
		$SQLCmd = "SELECT * FROM tb_scheduling WHERE car_idx = '" . (int)$useIdx . "' AND idx='{$jobId}'" ;
 		$query = $this->db->query( $SQLCmd) ;
 		// dump( $SQLCmd) ;
 		return $query->row ;
	}

	public function getInformationmemo( $ord_no) {
		$SQLCmd = "SELECT memo FROM tb_scheduling WHERE order_no='{$ord_no}' limit 1" ;
 		$query = $this->db->query( $SQLCmd) ;
 		// dump( $SQLCmd) ;
 		return $query->row ;
	}

	/**
	 * [getOrderTypeForDelete description]
	 * @param   string     $jobId [description]
	 * @param   string     $carIdx     [description]
	 * @return  [type]                 [description]
	 * @Another Angus
	 * @date    2018-01-02
	 */
	public function getOrderTypeForDelete( $jobId = "", $carIdx = "") {
		$SQLCmd = "SELECT order_type, order_no FROM tb_scheduling WHERE idx={$jobId} AND car_idx={$carIdx}" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;

		return $query->row;
	}

	public function getOrderOtherCar( $agreement_no = "") {
		$SQLCmd = "SELECT other_car FROM tb_order WHERE agreement_no='{$agreement_no}'" ;
		$query = $this->db->query( $SQLCmd) ;

		return $query->row;
	}

	/**
	 * [addInformation description]
	 * @param   [type]     $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2017-12-20
	 */
	public function addInformation ( $data) {
		$now = date('Y-m-d H:i:s') ;
		// dump( $this->session) ;
		list($y, $m, $sd) = explode('-', $this->db->escape($data['input_start_date']));
		list($y, $m, $ed) = explode('-', $this->db->escape($data['input_end_date']));

		$strStarDate = $this->db->escape($data['input_start_date']) . " " . $this->db->escape($data['input_start_time']) ;
		$strEndDate = $this->db->escape($data['input_end_date']) . " " . $this->db->escape($data['input_end_time']) ;

		$SQLCmd = "INSERT INTO tb_scheduling SET
			car_seed = '" . $this->db->escape($data['useCarSeed']) . "',
			car_idx = '" . $this->db->escape($data['useCarIdx']) . "', cust_name = '" . $this->db->escape($data['input_cust_name']) . "',
			order_type = '" . $this->db->escape($data['sel_order_type']) . "', order_no = '" . $this->db->escape($data['agreement_no']) . "',
			s_station = '". $this->db->escape($data['s_station']) ."', e_station = '". $this->db->escape($data['e_station']) ."',
			start_date = '" . $strStarDate . "', end_date = '" . $strEndDate . "',
			sday = '{$sd}', eday = '{$ed}',
			cust_tel = '" . $this->db->escape($data['input_cust_tel']) . "', memo = '" . $this->db->escape($data['area_memo']) . "',
			take_name = '" . $this->db->escape($data['input_take_name']) . "', take_mobile = '" . $this->db->escape($data['input_take_mobile']) . "',
			reg_user = '{$this->session->data['user_name']}', reg_date='{$now}' ";
		// dump( $SQLCmd) ;
		$this->db->query( $SQLCmd) ;
	}

	/**
	 * [editInformation description]
	 * @param   [type]     $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2018-01-06
	 */
	public function editInformation ( $data) {
		$now = date('Y-m-d H:i:s') ;
		$resSchedulingIDArr = array() ;

		if ( !empty($data['agreement_no'])) {
			$SQLCmd = "SELECT idx FROM tb_scheduling WHERE order_no='".$this->db->escape( $data['agreement_no'])."'" ;
			$resSchedulingIDArr = $this->db->query( $SQLCmd)->rows ;
			// dump( $resSchedulingIDArr) ;
		}

		list($y, $m, $sd) = explode('-', $this->db->escape($data['input_start_date']));
		list($y, $m, $ed) = explode('-', $this->db->escape($data['input_end_date']));

		$strStarDate = $this->db->escape($data['input_start_date']) . " " . $this->db->escape($data['input_start_time']) ;
		$strEndDate = $this->db->escape($data['input_end_date']) . " " . $this->db->escape($data['input_end_time']) ;


		if ( count( $resSchedulingIDArr) <= 1) {
			$SQLCmd = "UPDATE tb_scheduling SET
				car_seed = '" . $this->db->escape($data['useCarSeed']) . "',
				car_idx = '" . $this->db->escape($data['useCarIdx']) . "', cust_name = '" . $this->db->escape($data['input_cust_name']) . "',
				order_type = '" . $this->db->escape($data['sel_order_type']) . "', order_no = '" . $this->db->escape($data['agreement_no']) . "',
				s_station = '". $this->db->escape($data['s_station']) ."', e_station = '". $this->db->escape($data['e_station']) ."',
				start_date = '" . $strStarDate . "', end_date = '" . $strEndDate . "',
				sday = '{$sd}', eday = '{$ed}',
				cust_tel = '" . $this->db->escape($data['input_cust_tel']) . "', memo = '" . $this->db->escape($data['area_memo']) . "',
				take_name = '" . $this->db->escape($data['input_take_name']) . "', take_mobile = '" . $this->db->escape($data['input_take_mobile']) . "',
				updateUser = '{$this->session->data['user_name']}', updateTime='{$now}'
				where idx = '" . $this->db->escape($data['scheduleIdx']) . "'" ;
			// dump( $SQLCmd) ;
			$this->db->query( $SQLCmd) ;
		} else {
			foreach ($resSchedulingIDArr as $iCnt => $orderId) {
				$SQLCmd = "UPDATE tb_scheduling SET cust_name = '" . $this->db->escape($data['input_cust_name']) . "',
					order_type = '" . $this->db->escape($data['sel_order_type']) . "', order_no = '" . $this->db->escape($data['agreement_no']) . "',
					s_station = '". $this->db->escape($data['s_station']) ."', e_station = '". $this->db->escape($data['e_station']) ."',
					start_date = '" . $strStarDate . "', end_date = '" . $strEndDate . "',
					sday = '{$sd}', eday = '{$ed}',
					cust_tel = '" . $this->db->escape($data['input_cust_tel']) . "', memo = '" . $this->db->escape($data['area_memo']) . "',
					take_name = '" . $this->db->escape($data['input_take_name']) . "', take_mobile = '" . $this->db->escape($data['input_take_mobile']) . "',
					updateUser = '{$this->session->data['user_name']}', updateTime='{$now}'
					where idx = '".$orderId['idx']."'" ;
				$this->db->query( $SQLCmd) ;
			}
		}
	}

	/**
	 * [newOrder 新增訂單]
	 * @param   [type]     $insData [description]
	 * @return  [type]              [description]
	 * @Another Angus
	 * @date    2017-12-14
	 */
	public function newOrder( $insData) {
		$now = date('Y-m-d H:i:s') ;

		$createUser = $this->session->data['user_name'] ;

		$SQLCmd = "INSERT INTO tb_order SET
			agreement_no      ='{$insData['agreement_no']}',
			car_model         ='{$this->db->escape($insData['car_model'])}',
			order_type        ='{$insData['order_type']}',
			minsu_idx         ='{$insData['minsu_idx']}',
			charge_method     ='{$insData['charge_method']}',
			car_sn            ='{$insData['car_sn']}',
			car_no            ='{$insData['car_no']}',
			s_station         ='{$this->db->escape($insData['s_station'])}',
			e_station         ='{$this->db->escape($insData['e_station'])}',
			rent_date_out     ='{$this->db->escape($insData['rent_date_out'])}',
			rent_date_in_plan ='{$this->db->escape($insData['rent_date_in_plan'])}',
			other_car         = '{$insData['other_car']}',
			total_car         = 1,

			rent_user_name    ='{$this->db->escape($insData['rent_user_name'])}',
			rent_user_id      ='{$this->db->escape($insData['rent_user_id'])}',
			rent_user_born    ='{$this->db->escape($insData['rent_user_born'])}',
			rent_user_tel     ='{$this->db->escape($insData['rent_user_tel'])}',
			rent_user_mail    ='{$this->db->escape($insData['rent_user_mail'])}',

			get_user_name     ='{$this->db->escape($insData['get_user_name'])}',
			get_user_id       ='{$this->db->escape($insData['get_user_id'])}',
			get_user_born     ='{$this->db->escape($insData['get_user_born'])}',
			get_user_tel      ='{$this->db->escape($insData['get_user_tel'])}',

			rent_zip1         = '{$this->db->escape($insData['rent_zip1'])}',
			rent_zip2         = '{$this->db->escape($insData['rent_zip2'])}',
			rent_user_address ='{$this->db->escape($insData['rent_user_address'])}',
			rent_company_name ='{$this->db->escape($insData['rent_company_name'])}',
			rent_company_no   ='{$this->db->escape($insData['rent_company_no'])}',
			urgent_man        ='{$this->db->escape($insData['urgent_man'])}',
			urgent_mobile     ='{$this->db->escape($insData['urgent_mobile'])}',

			adult             = '{$insData['adult']}',
			children          = '{$insData['children']}',
			baby              = '{$insData['baby']}',

			baby_chair        ='{$this->db->escape($insData['baby_chair'])}',
			baby_chair1       ='{$this->db->escape($insData['baby_chair1'])}',
			baby_chair2       ='{$this->db->escape($insData['baby_chair2'])}',
			children_chair    ='{$this->db->escape($insData['children_chair'])}',
			gps               ='{$this->db->escape($insData['gps'])}',
			mobile_moto       ='{$this->db->escape($insData['mobile_moto'])}',
			total             ='{$this->db->escape($insData['total'])}',
			createUser='{$this->session->data['user_name']}',

			create_date='{$now}' " ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
	}

	public function updateOrder( $insData) {
		$now = date('Y-m-d H:i:s') ;

		$createUser = $this->session->data['user_name'] ;

		$SQLCmd = "INSERT INTO tb_order_bak SELECT * FROM tb_order WHERE agreement_no='{$this->db->escape($insData['agreement_no'])}'" ;
		$this->db->query( $SQLCmd) ;

		$SQLCmd = "UPDATE tb_order SET
			car_model         ='{$this->db->escape($insData['car_model'])}',
			order_type        ='{$insData['order_type']}',
			minsu_idx         ='{$insData['minsu_idx']}',
			charge_method     ='{$insData['charge_method']}',
			car_sn            ='{$insData['car_sn']}',
			car_no            ='{$insData['car_no']}',
			s_station         ='{$this->db->escape($insData['s_station'])}',
			e_station         ='{$this->db->escape($insData['e_station'])}',
			rent_date_out     ='{$this->db->escape($insData['rent_date_out'])}',
			rent_date_in_plan ='{$this->db->escape($insData['rent_date_in_plan'])}',

			rent_user_name    ='{$this->db->escape($insData['rent_user_name'])}',
			rent_user_id      ='{$this->db->escape($insData['rent_user_id'])}',
			rent_user_born    ='{$this->db->escape($insData['rent_user_born'])}',
			rent_user_tel     ='{$this->db->escape($insData['rent_user_tel'])}',
			rent_user_mail    ='{$this->db->escape($insData['rent_user_mail'])}',

			get_user_name     ='{$this->db->escape($insData['get_user_name'])}',
			get_user_id       ='{$this->db->escape($insData['get_user_id'])}',
			get_user_born     ='{$this->db->escape($insData['get_user_born'])}',
			get_user_tel      ='{$this->db->escape($insData['get_user_tel'])}',

			rent_zip1         = '{$this->db->escape($insData['rent_zip1'])}',
			rent_zip2         = '{$this->db->escape($insData['rent_zip2'])}',
			rent_user_address ='{$this->db->escape($insData['rent_user_address'])}',
			rent_company_name ='{$this->db->escape($insData['rent_company_name'])}',
			rent_company_no   ='{$this->db->escape($insData['rent_company_no'])}',
			urgent_man        ='{$this->db->escape($insData['urgent_man'])}',
			urgent_mobile     ='{$this->db->escape($insData['urgent_mobile'])}',

			adult             = '{$insData['adult']}',
			children          = '{$insData['children']}',
			baby              = '{$insData['baby']}',

			baby_chair        ='{$this->db->escape($insData['baby_chair'])}',
			baby_chair1       ='{$this->db->escape($insData['baby_chair1'])}',
			baby_chair2       ='{$this->db->escape($insData['baby_chair2'])}',
			children_chair    ='{$this->db->escape($insData['children_chair'])}',
			gps               ='{$this->db->escape($insData['gps'])}',
			mobile_moto       ='{$this->db->escape($insData['mobile_moto'])}',
			total             ='{$this->db->escape($insData['total'])}',

			updateDate='{$now}',
			updateUser = '{$this->session->data['user_name']}'
			WHERE agreement_no = '" . $this->db->escape($insData['agreement_no']) . "'" ;
		// dump( $SQLCmd) ;
		// exit() ;
		$query = $this->db->query( $SQLCmd) ;
	}

	/**
	 * [newFlight description]
	 * @param   [type]     $insData [description]
	 * @return  [type]              [description]
	 * @Another Angus
	 * @date    2018-01-26
	 */
	public function newFlight( $insData) {
		$SQLCmd = "INSERT INTO tb_flight SET
			order_no   ='{$insData['agreement_no']}',
			trans_type ='{$insData['trans_type']}',
			airport    ='{$insData['airport']}',
			airline    ='{$insData['airline']}',
			pier       ='{$insData['pier']}',
			flight_no  ='{$insData['flight_no']}',
			s_date     ='{$insData['fs_date']}',
			e_date     ='{$insData['fe_date']}',
			trans_type_rt ='{$insData['trans_type_rt']}',
			airport_rt    ='{$insData['airport_rt']}',
			airline_rt    ='{$insData['airline_rt']}',
			pier_rt       ='{$insData['pier_rt']}',
			flight_no_rt  ='{$insData['flight_no_rt']}',
			s_date_rt     ='{$insData['fs_date_rt']}',
			e_date_rt     ='{$insData['fe_date_rt']}'
		" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
	}

	/**
	 * [updateFlight description]
	 * @param   [type]     $insData [description]
	 * @return  [type]              [description]
	 * @Another Angus
	 * @date    2018-01-26
	 */
	public function updateFlight( $insData, $types = "") {
		$up = "Y";
		//if($types == "pickup"){
			//檢查有沒有存在 沒有的話要用insert
			$rs = $this->getFilght($insData['agreement_no']);
			if(count($rs) == 0){
				//新增
				$up = 'N';
				$this->newFlight($insData);
			}
		//}
		if($up == "Y"){
			$SQLCmd = "UPDATE tb_flight SET
				trans_type ='{$insData['trans_type']}',
				airport    ='{$insData['airport']}',
				airline    ='{$insData['airline']}',
				pier       ='{$insData['pier']}',
				flight_no  ='{$insData['flight_no']}',
				s_date     ='{$insData['fs_date']}',
				e_date     ='{$insData['fe_date']}',
				trans_type_rt ='{$insData['trans_type_rt']}',
				airport_rt    ='{$insData['airport_rt']}',
				airline_rt    ='{$insData['airline_rt']}',
				pier_rt       ='{$insData['pier_rt']}',
				flight_no_rt  ='{$insData['flight_no_rt']}',
				s_date_rt     ='{$insData['fs_date_rt']}',
				e_date_rt     ='{$insData['fe_date_rt']}'
				WHERE order_no ='{$insData['agreement_no']}'
			" ;
			// dump( $SQLCmd) ;
			// exit() ;
			$query = $this->db->query( $SQLCmd) ;
		}
	}

	/**
	 * [getFlight description]
	 * @param   string     $order_no [description]
	 * @return  [type]               [description]
	 * @Another Angus
	 * @date    2018-01-26
	 */
	public function getFlightInfo( $order_no = "") {

		$SQLCmd = "SELECT tf.*, so.opt_name airline_name FROM tb_flight tf
					LEFT JOIN sys_option so ON (so.idx = tf.airline AND so.parent = '125') OR (so.idx = tf.pier AND so.parent = '131')
					WHERE order_no='{$order_no}'" ;
 		$query = $this->db->query( $SQLCmd) ;
 		// dump( $SQLCmd) ;
 		return $query->row ;
	}

	public function getFlightInfoArr( $data = array()) {
		$inStr = '\''. join('\',\'', $data) . '\'' ;
		$SQLCmd = "SELECT tf.*, so.opt_name airline_name FROM tb_flight tf
					LEFT JOIN sys_option so ON (so.idx = tf.airline AND so.parent = '125') OR (so.idx = tf.pier AND so.parent = '131')
					WHERE order_no IN ({$inStr}) AND trans_type != 0" ;
 		$query = $this->db->query( $SQLCmd) ;


		$nodeArr = array() ;
		foreach ($query->rows as $iCnt => $node) {
			$nodeArr[$node['order_no']] = $node ;
		}

 		// dump( $SQLCmd) ;
 		return $nodeArr ;
	}

	/**
	 * [getScheduling description]
	 * @param   [type]     $carSeedArr  [車型流水號]
	 * @param   [array]    $dayRangeArr [查詢日期]
	 * @return  [type]                  [description]
	 * @Another Angus
	 * @date    2017-12-20
	 */
	public function getScheduling( $carSeedArr, $dayRangeArr) {
		// dump( $carSeedArr, $dayRangeArr) ;
		$inStr = join( ',', $carSeedArr) ;
		$timeSheetStr = "('{$dayRangeArr['sDayStr']}' BETWEEN ts.start_date AND ts.end_date OR '{$dayRangeArr['eDayStr']} 23:59' BETWEEN ts.start_date AND ts.end_date OR (ts.start_date > '{$dayRangeArr['sDayStr']}' AND ts.end_date < '{$dayRangeArr['eDayStr']} 23:59') ) " ;

		if ( !empty($inStr)) {
			//增加撈 tb_order的pay_type (2019/08/28 jessie)
			$SQLCmd = "SELECT ts.*, tod.pay_type
				FROM tb_scheduling ts
				left join tb_order tod ON tod.agreement_no = ts.order_no
				WHERE ts.car_seed in ({$inStr})
					AND ts.status=1 AND {$timeSheetStr}
				ORDER BY ts.start_date ASC" ;
			// dump( $SQLCmd) ;
			$query = $this->db->query( $SQLCmd) ;

			return $query->rows;
		} else {
			return array() ;
		}
	}

	public function getSchedulingUseAgreementNo ( $agreement_no = "") {
		if ( !empty($agreement_no)) {
			$SQLCmd = "SELECT idx FROM tb_scheduling WHERE order_no='{$agreement_no}'" ;
			$query = $this->db->query( $SQLCmd) ;
/*
    [num_rows] => 1
    [row] => Array
        (
            [idx] => 1585
        )

    [rows] => Array
        (
            [0] => Array
                (
                    [idx] => 1585
                )

        )
*/
			// return $query->rows ;
			return $query->row['idx'] ;
		} else {
			return false ;
		}

	}

	/**
	 * [checkScheduling 檢查新進來的時間與資料表是否有重覆]
	 * @param   string     $carsn       [車輛流水號]
	 * @param   array      $dayRangeArr [開始/結束日期]
	 * @return  [type]                  [description]
	 * @Another Angus
	 * @date    2017-12-20
	 */
	public function checkScheduling( $carsn = "", $filterArr = array()) {
		// $timeSheetStr = "('{$dayRangeArr['sDayStr']}' BETWEEN start_date AND end_date
		// 		OR '{$dayRangeArr['eDayStr']}' BETWEEN start_date AND end_date
		// 		OR (start_date > '{$dayRangeArr['sDayStr']}' AND end_date < '{$dayRangeArr['eDayStr']}') ) " ;
		$timeSheetStr = "('{$filterArr['sDayStr']}' BETWEEN date_add(start_date, interval +1 minute) AND date_add(end_date, interval -1 minute)
				OR '{$filterArr['eDayStr']}' BETWEEN date_add(start_date, interval +1 minute) AND date_add(end_date, interval -1 minute) )" ;
		if ( $filterArr['scheduleIdx'] != '') {
			$timeSheetStr .= " AND idx != '{$filterArr['scheduleIdx']}'" ;
		}
		$SQLCmd = "SELECT * FROM tb_scheduling  WHERE car_idx = '{$carsn}' AND {$timeSheetStr} AND status = 1" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;

		return $query->rows;
	}

	/**
	 * [checkCustUser 檢查顧客身份証字號 是否存在]
	 * @param   string     $pid [顧客身份証字號]
	 * @return  [type]          [description]
	 * @Another Angus
	 * @date    2017-12-21
	 */
	public function checkCustomerByPid( $pid = "") {
		$SQLCmd = "SELECT user_id FROM oc_customer WHERE upper(user_id)='{$pid}'" ;
		$query = $this->db->query( $SQLCmd) ;

		return $query->rows;
	}

	/**
	 * [insCustomerInfo 新增訂單並新增顧客資料]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2017-12-22
	 */
	public function insCustomerInfo( $data = array()) {
		$now = date('Y-m-d H:i:s') ;
		// 新增客戶檔 oc_customer
		$data['email'] = isset( $data['rent_user_mail']) ? $data['rent_user_mail'] : "" ;
		$SQLCmd = "INSERT INTO oc_customer SET customer_group_id=1, store_id=0, language_id=0, status=1, approved=1,
			user_id='{$data['rent_user_id']}',
			firstname='{$data['rent_user_name']}',
			telephone='{$data['rent_user_tel']}',
			email='{$data['email']}',
			date_added='{$now}'" ;
		$query = $this->db->query( $SQLCmd) ;

		// 取回最後新增的customer_id
		$SQLCmd      = "SELECT LAST_INSERT_ID() customer_id" ;
		$query       = $this->db->query( $SQLCmd) ;
		$customer_id = $query->row['customer_id'] ;
		// $customer_id = $this->db->getLastId(); 原生寫法

		// 存客戶地址資訊
		$postCode = ( !empty( $data['rent_zip2'])) ? $data['rent_zip2'] : $data['rent_zip1'] ;
		$SQLCmd   = "SELECT zip_name FROM sys_zip WHERE zip_code='{$data['rent_zip1']}'" ;
		$query    = $this->db->query( $SQLCmd) ;
		$cityName = $query->row['zip_name'] ;

		$SQLCmd = "INSERT INTO oc_address SET customer_id='{$customer_id}',
			pid='{$data['rent_user_id']}',
			firstname='{$data['rent_user_name']}',
			address_1='{$data['rent_user_address']}',
			postcode='{$postCode}',
			city='$cityName' " ;
		$query    = $this->db->query( $SQLCmd) ;
	}

	public function getTotalCnt() {
		$SQLCmd = "SELECT COUNT(*) AS total FROM sys_news" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;

		return $query->row['total'] ;
	}

	/**
	 * [getSchedulingCarModel 訂單管理]
	 * @param   array      $filter [description]
	 * @return  [type]             [description]
	 * @Another Angus
	 * @date    2018-02-20
	 */
	public function getSchedulingCarModel( $filter = array()) {
		$useCarsArr = explode("|",$filter['useCars']);
		$notInCarIdx = array() ;
		// 開始時間在區間內的車
		$SQLCmd = "SELECT car_idx FROM tb_scheduling WHERE '{$filter['s']}' BETWEEN start_date AND end_date AND status = 1" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		foreach ($query->rows as $key => $tmpArr) {
			if(!in_array($tmpArr['car_idx'],$useCarsArr )){
				$notInCarIdx[] = $tmpArr['car_idx'] ;
			}
		}
		// 結束時間在區間內的車
		$SQLCmd = "SELECT car_idx FROM tb_scheduling WHERE '{$filter['e']}' BETWEEN start_date AND end_date AND status = 1" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		foreach ($query->rows as $key => $tmpArr) {
			if(!in_array($tmpArr['car_idx'],$useCarsArr )){
				$notInCarIdx[] = $tmpArr['car_idx'] ;
			}
		}
		// check 車輛在租車開始期間 是否有被租用
		$SQLCmd = "SELECT car_idx FROM tb_scheduling WHERE start_date BETWEEN '{$filter['s']}' and '{$filter['e']}' AND status  = 1" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		foreach ($query->rows as $key => $tmpArr) {
			if(!in_array($tmpArr['car_idx'],$useCarsArr )){
				$notInCarIdx[] = $tmpArr['car_idx'] ;
			}
		}
		// check 車輛在租車結束期間 是否有被租用
		$SQLCmd = "SELECT car_idx FROM tb_scheduling WHERE end_date BETWEEN '{$filter['s']}' and '{$filter['e']}' AND status  = 1" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		foreach ($query->rows as $key => $tmpArr) {
			if(!in_array($tmpArr['car_idx'],$useCarsArr )){
				$notInCarIdx[] = $tmpArr['car_idx'] ;
			}
		}
		$notInCarIdx = array_unique( $notInCarIdx) ;
		$notInStr = join( ',', $notInCarIdx) ;
		$sqlInStr = ( !empty($notInStr)) ? "AND idx NOT IN ( {$notInStr})" : "" ;

		$carModelArr = explode( "|", $filter['car_model']) ;
		// dump( $carModelArr) ;
		$carModelInStr = join( ",", $carModelArr) ;

		$SQLCmd = "SELECT idx, car_seed FROM sys_car_model WHERE
					idx IN ( SELECT DISTINCT car_seed FROM sys_cars WHERE 1=1 {$sqlInStr} AND status = '1')
					AND car_model in ({$carModelInStr})" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;

		$retArr = array() ;
		foreach ($query->rows as $key => $tmpArr) {
			$retArr[] = $tmpArr ;
		}
		// dump( $retArr) ;
		return $query->rows ;
	}

	/**
	 * [getCarNoForAjax 訂單管理]
	 * @param   array      $filter [description]
	 * @return  [type]             [description]
	 * @Another Angus
	 * @date    2018-02-20
	 */
	public function getCarNoForAjax( $filter = array()) {
		$useCarsArr = array();
		if(isset($filter['useCars'])){
			$useCarsArr = explode("|",$filter['useCars']);
		}

		// dump( $filter) ;
		$notInCarIdx = array() ;
		$notInStr = isset( $filter['car_sn']) ? " AND car_idx NOT IN (".join( ',', $filter['car_sn']).")" : "" ;

		// 開始時間在區間內的車
		$SQLCmd = "SELECT car_idx FROM tb_scheduling WHERE '{$filter['s']}' BETWEEN start_date AND end_date {$notInStr} AND status  = 1" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		foreach ($query->rows as $key => $tmpArr) {
			if(!in_array($tmpArr['car_idx'],$useCarsArr )){
				$notInCarIdx[] = $tmpArr['car_idx'] ;
			}
		}
		// 結束時間在區間內的車
		$SQLCmd = "SELECT car_idx FROM tb_scheduling WHERE '{$filter['e']}' BETWEEN start_date AND end_date {$notInStr} AND status  = 1" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		foreach ($query->rows as $key => $tmpArr) {
			if(!in_array($tmpArr['car_idx'],$useCarsArr )){
				$notInCarIdx[] = $tmpArr['car_idx'] ;
			}
		}
		// check 車輛在租車開始期間 是否有被租用
		$SQLCmd = "SELECT car_idx FROM tb_scheduling WHERE start_date BETWEEN '{$filter['s']}' and '{$filter['e']}' {$notInStr} AND status  = 1" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		foreach ($query->rows as $key => $tmpArr) {
			if(!in_array($tmpArr['car_idx'],$useCarsArr )){
				$notInCarIdx[] = $tmpArr['car_idx'] ;
			}
		}
		// check 車輛在租車結束期間 是否有被租用
		$SQLCmd = "SELECT car_idx FROM tb_scheduling WHERE end_date BETWEEN '{$filter['s']}' and '{$filter['e']}' {$notInStr} AND status  = 1" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		foreach ($query->rows as $key => $tmpArr) {
			if(!in_array($tmpArr['car_idx'],$useCarsArr )){
				$notInCarIdx[] = $tmpArr['car_idx'] ;
			}
		}

		$notInCarIdx = array_unique( $notInCarIdx) ;
		$notInStr = join( ',', $notInCarIdx) ;
		$sqlInStr = ( !empty($notInStr)) ? "AND sys_cars.idx NOT IN ( {$notInStr})" : "" ;

		$carSeedsArr = explode( "|", $filter['car_seed']) ;
		// dump( $carModelArr) ;
		$carSeedsInStr = join( ",", $carSeedsArr) ;


		$SQLCmd = "SELECT sys_cars.idx, sys_cars.car_no, sys_car_model.car_seed,sys_car_model.car_price FROM sys_cars left join sys_car_model on sys_cars.car_seed=sys_car_model.idx
		WHERE sys_cars.car_seed in ({$carSeedsInStr}) {$sqlInStr} AND sys_cars.status = '1'" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;

		$retArr = array() ;
		foreach ($query->rows as $key => $tmpArr) {
			$retArr[] = $tmpArr ;
		}
		// dump( $retArr) ;
		return $query->rows ;
	}

	/**
	 * [getDiscountPercentage 回傳優惠百分比]
	 * @param   [type]     $rentStartDate [租車日期]
	 * @return  [type]                    [description]
	 * @Another Angus
	 * @date    2018-04-07
	 */
	public function getDiscountPercentage ( $rentStartDate = "", $create_date = "") {
		if ( !empty( $create_date)) {
			$create_date = substr($create_date, 0, 10);
			$today = strtotime( $create_date) ;
		} else {
			$today = strtotime( date( 'Y-m-d')) ;
		}

		$discountDay = ( strtotime( date('Y-m-d', strtotime( $rentStartDate . " - 0 day")) ) - $today) / 86400 ;

		if ( $discountDay >= 60) {
			$percent = 0.75 ;
		} else if ( $discountDay < 60 && $discountDay >= 45) {
			$percent = 0.85 ;
		} else if ( $discountDay < 45 && $discountDay >= 30) {
			$percent = 0.9 ;
		} else {
			$percent = 1 ;
		}
		// dump( array( $rentStartDate ,$discountDay, $percent)) ;

		return $percent ;
	}

	public function getFilght( $order_no = "") {
		$SQLCmd = "select * from tb_flight where order_no='{$order_no}'" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		return $query->row;
	}

	public function getSeedName($seed_idx){
		$SQLCmd = "SELECT idx, car_seed FROM sys_car_model WHERE
					idx = {$seed_idx}" ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;

		$car_seed = "";
		foreach ($query->rows as $key => $tmpArr) {
			$car_seed = $tmpArr['car_seed'] ;
		}

		return $car_seed;
	}

	/**
	 * [checkOrderChangeDate description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2021-03-08
	 */
	public function checkOrderChangeDate( $data = []) {
		$filter = array(
			"useCars"   => $data['car_sn'],
			"order_no" => $data['agreement_no'],
			"s"         => $data['rent_date_out_full'],
			"e"         => $data['rent_date_in_plan_full'],
		) ;
		$orderNoArr = $this->searchSchedulingCars( $filter) ;
		if ( count( $orderNoArr) > 0) {
			$msg = join(',', $orderNoArr) ;
			$this->session->data['error_warning'] = "修改時間與訂單:[{$msg}]發生衝突  請查明後再設定";
			return false ;
		} else {
			return true ;
		}
	}

	/**
	 * [searchSchedulingCars description]
	 * @param   [type]     $filter [description]
	 * @return  [type]             [description]
	 * @Another Angus
	 * @date    2021-03-07
	 */
	public function searchSchedulingCars( $filter) {
		$inCarIdx = [] ;
		$orderNoArr = [] ;
		// 開始時間在區間內的車
		$SQLCmd = "SELECT car_idx, order_no FROM tb_scheduling WHERE '{$filter['s']}' BETWEEN start_date AND end_date AND status != 2 AND car_idx={$filter['useCars']} " ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		foreach ($query->rows as $key => $tmpArr) {
			$inCarIdx[] = $tmpArr['car_idx'] ;
			$orderNoArr [] = $tmpArr['order_no'] ;
		}

		// 結束時間在區間內的車
		$SQLCmd = "SELECT car_idx, order_no FROM tb_scheduling WHERE '{$filter['e']}' BETWEEN start_date AND end_date AND status != 2 AND car_idx={$filter['useCars']} " ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		foreach ($query->rows as $key => $tmpArr) {
			$inCarIdx[] = $tmpArr['car_idx'] ;
			$orderNoArr [] = $tmpArr['order_no'] ;
		}
		// check 車輛在租車開始期間 是否有被租用
		$SQLCmd = "SELECT car_idx, order_no FROM tb_scheduling WHERE start_date BETWEEN '{$filter['s']}' and '{$filter['e']}' AND status != 2 AND car_idx={$filter['useCars']} " ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		foreach ($query->rows as $key => $tmpArr) {
			$inCarIdx[] = $tmpArr['car_idx'] ;
			$orderNoArr [] = $tmpArr['order_no'] ;
		}
		// check 車輛在租車結束期間 是否有被租用
		$SQLCmd = "SELECT car_idx, order_no FROM tb_scheduling WHERE end_date BETWEEN '{$filter['s']}' and '{$filter['e']}' AND status != 2 AND car_idx={$filter['useCars']} " ;
		// dump( $SQLCmd) ;
		$query = $this->db->query( $SQLCmd) ;
		foreach ($query->rows as $key => $tmpArr) {
			$inCarIdx[] = $tmpArr['car_idx'] ;
			$orderNoArr [] = $tmpArr['order_no'] ;
		}
		$inCarIdx = array_unique( $inCarIdx) ;
		$orderNoArr = array_unique( $orderNoArr) ;
		if (($key = array_search($filter['order_no'], $orderNoArr)) !== false) {
			unset($orderNoArr[$key]);
		}

		return $orderNoArr ;
	}

}