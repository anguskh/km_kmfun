<?php
class ControllerStartupStartup extends Controller {
	public function index() {
		if ( DB_DEBUG_MOD) {
			$runUrl   = isset( $_REQUEST["route"]) ? $_REQUEST["route"] : "" ;
			$userName = isset( $this->session->data['user_name']) ? $this->session->data['user_name'] : "" ;
			$logFileName = DIR_LOGS."db.log" ;
			$fp = fopen($logFileName , "a" ) ;
			$nowTime = date("Y/m/d H:i:s");
			fwrite($fp,"{$nowTime} | --- ROUTE : {$runUrl}  --- USER : {$userName}\r\n") ;
			fwrite($fp,"{$nowTime} | URL ROUTE : {$_SERVER["REQUEST_URI"]} \r\n") ;
			fclose( $fp) ;
		}

		// Settings
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '0'");

		foreach ($query->rows as $setting) {
			if (!$setting['serialized']) {
				$this->config->set($setting['key'], $setting['value']);
			} else {
				$this->config->set($setting['key'], json_decode($setting['value'], true));
			}
		}

		// Language
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "language` WHERE code = '" . $this->db->escape($this->config->get('config_admin_language')) . "'");

		if ($query->num_rows) {
			$this->config->set('config_language_id', $query->row['language_id']);
		}

		// Language
		$language = new Language($this->config->get('config_admin_language'));
		$language->load($this->config->get('config_admin_language'));
		$this->registry->set('language', $language);

		// Customer -- mark by Angus 2017.03.22
		// traceLog( "run affiliate", __METHOD__, __FILE__) ;
		// $this->registry->set('customer', new Cart\Customer($this->registry));

		// Affiliate -- mark by Angus 2017.03.22
		// $this->registry->set('affiliate', new Cart\Affiliate($this->registry));

		// Currency -- mark by Angus 2017.03.22 幣別
		// $this->registry->set('currency', new Cart\Currency($this->registry));

		// Tax -- mark by Angus 2017.03.22
		// $this->registry->set('tax', new Cart\Tax($this->registry));

		// -- mark by Angus 2017.03.22
		// if ($this->config->get('config_tax_default') == 'shipping') {
		// 	$this->tax->setShippingAddress($this->config->get('config_country_id'), $this->config->get('config_zone_id'));
		// }

		// -- mark by Angus 2017.03.22
		// if ($this->config->get('config_tax_default') == 'payment') {
		// 	$this->tax->setPaymentAddress($this->config->get('config_country_id'), $this->config->get('config_zone_id'));
		// }

		// -- mark by Angus 2017.03.22
		// $this->tax->setStoreAddress($this->config->get('config_country_id'), $this->config->get('config_zone_id'));

		// Weight -- mark by Angus 2017.03.22
		// $this->registry->set('weight', new Cart\Weight($this->registry));

		// Length -- mark by Angus 2017.03.22
		// $this->registry->set('length', new Cart\Length($this->registry));

		// Cart -- mark by Angus 2017.03.22
		// $this->registry->set('cart', new Cart\Cart($this->registry));

		// Encryption -- mark by Angus 2017.03.22
		// $this->registry->set('encryption', new Encryption($this->config->get('config_encryption')));

		// OpenBay Pro -- mark by Angus 2017.03.22
		// $this->registry->set('openbay', new Openbay($this->registry));
	}
}