<?php
class ControllerSaleOrder extends Controller {
	private $error       = array();
	protected $devAdmin = false ; // 開發者

	public function index() {
		$this->load->language('sale/order');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('sale/order');

		$this->getList();
	}

	// rework by Angus 2017.07.08
	/**
	 * [add description]
	 * @Another Angus
	 * @date    2018-02-11
	 */
	public function add() {
		$this->load->language('sale/order');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('sale/order') ;
		$this->load->model('site/scheduling') ;
		$this->load->model('site/cars') ;

		// if ($this->request->server['REQUEST_METHOD'] == 'POST') {
		// 	dump( $this->request->post) ;
		// 	exit() ;
		// }

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			// dump( "新增頁面 add page") ;
			// dump( $this->request->post) ;
			// exit() ;
			$insData = $this->buildingData() ;
			// dump( $insData) ;
			// exit() ;
			// 檢查是否為存在客戶
			$checkInfo = $this->model_site_scheduling->checkCustomerByPid( $insData['rent_user_id']) ;
			if ( empty( $checkInfo) ) {
				$this->model_site_scheduling->insCustomerInfo( $insData) ;
			}
			// 新增訂單/新增scheduling
			$insData['agreement_no'] = $this->model_sale_order->addInformation( $insData) ;
			// 新增航船資訊
			// 2020.04.26 取消判斷訂單為民宿/業者才存
			// if ( $insData['order_type'] == 10) {
			if ( ($insData['order_type'] != 123 && $insData['order_type'] != 124)) {
				// dump($insData);
				$this->model_site_scheduling->newFlight( $insData) ;
			}
			$this->session->data['success'] = $this->language->get('text_success') ;

			$url = '' ;
			// exit() ;
			$this->response->redirect($this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url, true)) ;
		}

		$this->getForm();
	}

	// rework by Angus 2017.07.08
	/**
	 * [edit description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-02-11
	 */
	public function edit() {
		$this->load->language('sale/order');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('sale/order') ;
		$this->load->model('site/scheduling') ;
		$this->load->model('site/cars') ;

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			// dump( "修改頁面 edit page") ;
			// exit() ;
			$insData = $this->buildingData() ;

			if ( $this->model_site_scheduling->checkOrderChangeDate( $insData)) {
				// 沒有衝突
				// 檢查是否為存在客戶
				$checkInfo = $this->model_site_scheduling->checkCustomerByPid( $insData['rent_user_id']) ;
				if ( empty( $checkInfo) ) {
					$this->model_site_scheduling->insCustomerInfo( $insData) ;
				}

				$this->model_sale_order->editInformation( $insData) ; // 修改訂單/修scheduling
				// 修改航船資訊
				if ( $insData['order_type'] == 10) {
					// dump($insData);
					$this->model_site_scheduling->updateFlight( $insData) ;
				}
				$this->session->data['success'] = $this->language->get('text_success') ;

				$url = '' ;

				$this->response->redirect($this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url, true)) ;
			}
		}
		$this->getForm() ;
	}

	// add by Angus 2017.07.08
	public function view() {
		$this->load->language('sale/order');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->getForm();
	}

	public function delete() {
		$this->load->language('sale/order') ;

		$this->document->setTitle($this->language->get('heading_title')) ;

		$this->load->model('sale/order') ;

		// dump( $this->request->get) ;
		// dump( $this->request->post) ;
		if ( (isset($this->request->post['selected']) || isset($this->request->get['idx'])) && $this->validate()) {
			if ( isset($this->request->get['idx'])) {
				$this->model_sale_order->deleteOrder( $this->request->get['idx']);
			} else {
				foreach ($this->request->post['selected'] as $order_id) {
					$this->model_sale_order->deleteOrder($order_id);
				}
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_order_id'])) {
				$url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
			}

			if (isset($this->request->get['filter_customer'])) {
				$url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_order_status'])) {
				$url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
			}

			if (isset($this->request->get['filter_total'])) {
				$url .= '&filter_total=' . $this->request->get['filter_total'];
			}

			if (isset($this->request->get['filter_date_added'])) {
				$url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
			}

			if (isset($this->request->get['filter_date_modified'])) {
				$url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
			}

			$this->response->redirect($this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	// rework by Angus 2017.07.05
	protected function getList() {
		$data = $this->preparation() ;
		// if ( $this->session->data['user_group_id'] == 1 || $this->session->data['user_group_id'] == 10) {
		if ( $this->session->data['user_group_id'] == 1) {
			$this->devAdmin = true ;
			$data['devAdmin'] = $this->devAdmin ;
		}

		// 日曆 include JS add by Angus 2017.11.10
		$this->document->addStyle( "view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css") ;
		$this->document->addScript( "view/javascript/jquery/datetimepicker/moment.js") ;
		$this->document->addScript( "view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js") ;

		$data['button_filter']	= $this->language->get('button_filter') ;
		$data['column_action']	= $this->language->get('column_action') ;
		$data['token']			= $this->session->data['token'] ;

		// 訊息類 --------------------------------------------------------------------------------------------
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		// 取回交車站名稱
		$this->load->model('site/option') ;
		$stations             = $this->model_site_option->getOptionNameArr( '15', true) ;
		$order_types          = $this->model_site_option->getOptionItemsArr('8', true) ;	// 訂單類別
		$data['optStation']   = $stations ;
		$data['optOrderType'] = $order_types ;
		// dump( $stations) ;

		// 取回車種名稱
		$this->load->model('site/car_model') ;
		$car_models  = $this->model_site_car_model->getCarSeedOption() ;

		// 搜尋   -------------------------------------------------------------------------------------------
		$sort	= is_null( $this->getRequest( 'get', 'sort'))	? 'create_date' : $this->getRequest( 'get', 'sort') ;
		$order	= is_null( $this->getRequest( 'get', 'order'))	? 'DESC' : $this->getRequest( 'get', 'order') ;
		$page	= is_null( $this->getRequest( 'get', 'page'))	? 1 : $this->getRequest( 'get', 'page') ;
		$defaultOrderIdByToday = date("Ymd") ;
		if ( isset( $this->request->get['filter'])) {
			$filter_order_id		= $this->getRequest( 'get', 'filter_order_id') ;
		} else if ( isset( $this->request->get['page'])) {
			$filter_order_id		= $this->getRequest( 'get', 'filter_order_id') ;
		} else {
			$filter_order_id		= $this->getRequest( 'get', 'filter_order_id') ;
			$filter_order_id		= !empty( $filter_order_id) ? $filter_order_id : $defaultOrderIdByToday ;
		}

		// dump( $this->request->get) ;
		$filter_order_type		= $this->getRequest( 'get', 'filter_order_type') ;
		$filter_customer		= $this->getRequest( 'get', 'filter_customer') ;
		$filter_order_status	= $this->getRequest( 'get', 'filter_order_status') ;
		$filter_mobile			= $this->getRequest( 'get', 'filter_mobile') ;
		$filter_station			= $this->getRequest( 'get', 'filter_station') ;
		$filter_date_out		= $this->getRequest( 'get', 'filter_date_out') ;
		$filter_date_out2		= $this->getRequest( 'get', 'filter_date_out2') ;	//還車日期
		$filter_date_tout		= $this->getRequest( 'get', 'filter_date_tout') ;
		$filter_date_tout2		= $this->getRequest( 'get', 'filter_date_tout2') ;	//實際還車日期
		$filter_date_modified	= "" ;

		$data['filter_order_id']		= $filter_order_id;
		$data['filter_order_type']		= $filter_order_type;
		$data['filter_customer']		= $filter_customer;
		$data['filter_order_status']	= $filter_order_status;
		$data['filter_mobile']			= $filter_mobile;
		$data['filter_station']			= $filter_station;
		$data['filter_date_out']		= $filter_date_out;
		$data['filter_date_out2']		= $filter_date_out2;	//還車日期
		$data['filter_date_tout']		= $filter_date_tout;
		$data['filter_date_tout2']		= $filter_date_tout2;	//還車日期

		$data['filter_date_modified']	= $filter_date_modified;
		// dump( $this->request->get) ;

		$url = "" ;

		// mark by Angus 2017.11.10 發現是多餘的
		// if (isset($this->request->get['page'])) {
		// 	$url .= '&page=' . $this->request->get['page'];
		// }

		// 查尋條件參數
		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
		}
		if (isset($this->request->get['filter_customer'])) {
			$url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
		}
		if (isset($this->request->get['filter_mobile'])) {
			$url .= '&filter_mobile=' . $this->request->get['filter_mobile'];
		}
		$urlForPage = $url ;

		// 欄位用的排序條件
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		// dump( $this->request->get) ;
		// 欄位用的排序條件
		// order_type			訂單類別
		// agreement_no			訂單編號
		// rent_user_name		領車人名稱
		// rent_user_tel		領車人電話
		// car_model			車型
		// car_no				車號
		// rent_date_out		取車日期
		// rent_date_in_plan	還車日期
		// s_station			取車地點
		// e_station			還車地點
		// create_date			建立日期
		$sortUrlArr = array(
			'order_type'        => $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=order_type' . $url, true),
			'agreement_no'      => $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=agreement_no' . $url, true),
			'checkout'          => $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=checkout' . $url, true),
			'rent_user_name'    => $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=rent_user_name' . $url, true),
			'rent_user_tel'     => $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=rent_user_tel' . $url, true),
			'car_model'         => $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=car_model' . $url, true),
			'car_no'            => $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=car_no' . $url, true),
			'rent_date_out'     => $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=rent_date_out' . $url, true),
			'rent_date_in_plan' => $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=rent_date_in_plan' . $url, true),
			'rent_date_out_true'     => $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=rent_date_out_true' . $url, true),
			'rent_date_in' => $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=rent_date_in' . $url, true),
			's_station'         => $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=s_station' . $url, true),
			'e_station'         => $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=e_station' . $url, true),
			'total'             => $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=total' . $url, true),
			'create_date'       => $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=create_date' . $url, true),
			) ;
		$data['sortUrl'] = $sortUrlArr ;

		// 分頁功能 -------------------------------------------------------------------------------------------
		// 查詢條件陣列
		$filter_data = array(
			'filter_order_id'     => $filter_order_id ,
			'filter_order_type'   => $filter_order_type ,
			'filter_order_status' => $filter_order_status ,
			'filter_customer'     => $filter_customer ,
			'filter_mobile'       => $filter_mobile ,
			'filter_station'      => $filter_station ,
			'filter_date_out'     => $filter_date_out ,
			'filter_date_out2'    => $filter_date_out2 ,
			'filter_date_tout'    => $filter_date_tout ,
			'filter_date_tout2'   => $filter_date_tout2 ,
			'sort'                => $sort,
			'order'               => $order,
			'start'               => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'               => $this->config->get('config_limit_admin')
		);
		// dump( $filter_data) ;

		// 取得現在查詢資料筆數
		$order_total = $this->model_sale_order->getTotalOrders($filter_data);
		$results = $this->model_sale_order->getOrders($filter_data);
		// dump( $stations) ;
		foreach ($results as $ck => $result) {
			(trim( isset( $result['s_station'])) && $result['s_station'] != 0) ? $result['s_station']   = $stations[$result['s_station']] : $result['s_station'] = "" ;
			(trim( isset( $result['e_station'])) && $result['e_station'] != 0) ? $result['e_station']   = $stations[$result['e_station']] : $result['e_station'] = "" ;
			(trim( isset( $result['order_type'])) && $result['order_type'] != 0) ? $result['order_type'] = $order_types[$result['order_type']] : $result['order_type'] = "" ;
			trim( isset( $result['car_model'])) ? $result['car_model']   = $car_models[$result['car_model']] : "" ;
			// $result['view'] = $this->url->link('site/scheduling/view', 'token=' . $this->session->data['token'] . '&idx=' . $result['car_sn'] . '&bookingID=' . $result['sidx'], true) ;
			$result['view'] = $this->url->link('site/scheduling/view', 'token=' . $this->session->data['token'] . '&idx=' . $result['car_sn'] . "&ag=" . $result['agreement_no'], true) ;
			$result['edit'] = $this->url->link('sale/order/edit', 'token=' . $this->session->data['token'] . '&idx=' . $result['order_id'] . $urlForPage, true) ;
			$result['delete'] = $this->url->link('sale/order/delete', 'token=' . $this->session->data['token'] . '&idx=' . $result['order_id'] . $urlForPage, true) ;
			$result['savepdf'] = $this->url->link('sale/order/savepdf', 'token=' . $this->session->data['token'] . '&o1=' . urlencode($result['agreement_no']).'&m1='.urlencode($result['rent_user_tel']). $urlForPage, true) ;

			// 整理列表頁 車號欄位的車數 add by Angus 2018.04.22
			// 時空背景的關係 目前的寫法是應付舊資料 新寫法可參考dashboard/kmfunrent.php中 行130
			if ( trim( $result['other_car']) != '') {
				$oCars = unserialize( $result['other_car']) ;
				$carNoStr = "" ;
				if ( !empty($oCars)) {
					foreach ($oCars as $iCnt => $tmpCarInfo) {
						$carNoStr .= "{$tmpCarInfo['car_no']}, " ;
					}
					$carNoStr = str_replace( $result['car_no'] . ", ", "", $carNoStr) ;
					$result['car_no'] = $result['car_no'] . ", " . $carNoStr ;
					$result['car_no'] = substr( $result['car_no'], 0, strlen( $result['car_no']) -2);
				}
			}
			$data['orders'][] = $result ;
		}
		if ( !isset( $data['orders'])) $data['orders'] = "" ;
		// 定義欄位名稱
		// 2018.02.27 列表新增【訂單類別、取車日期、還車日期、還車地點】，如版面超出時可移除【建立日期】
		$columnNames = array(
				"order_type"         => '訂單類別',
				"agreement_no"       => '訂單編號',
				"checkout"           => '綠界訂單編號',
				"rent_user_name"     => '領車人',
				"rent_user_tel"      => '領車人電話',
				"car_model"          => '車型',
				"car_no"             => '車號',
				"rent_date_out"      => '預計取車日期',
				"rent_date_in_plan"  => '預計還車日期',
				"rent_date_out_true" => '實際取車日期',
				"rent_date_in"       => '實際還車日期',
				"s_station"          => '取車地點',
				"e_station"          => '還車地點',
				"total"              => '費用',
				"create_date"        => '建立日期',
				"createUser"         => '建立人',
				"updateUser"         => '修改人',
			) ;
		$data['columnNames'] = $columnNames ;
		$data['td_colspan'] = count( $columnNames) ;

		$pagination = new Pagination();
		$pagination->total = $order_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . $urlForPage . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($order_total - $this->config->get('config_limit_admin'))) ? $order_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $order_total, ceil($order_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$this->load->model('localisation/order_status');

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		// 準備各位置資訊 ----------------------------------------------------------------------------------------
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		// dump( $data) ;

		$this->response->setOutput($this->load->view('sale/order_list', $data));
	}

	// rework by Angus 2017.07.08
	public function getForm() {
		$data = $this->preparation() ;
		$this->load->model('site/zip') ;
		$this->load->model('site/car_model') ;
		$this->load->model('site/cars') ;
		$this->load->model('site/option') ;
		$this->load->model('site/minshuku') ;
		$this->load->model('sale/order') ;

		// 日曆 include JS add by Angus 2017.11.10
		$this->document->addStyle( "view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css") ;
		$this->document->addScript( "view/javascript/jquery/datetimepicker/moment.js") ;
		$this->document->addScript( "view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js") ;

		$data['token']           = $this->session->data['token'];

		$data['text_loading']    = $this->language->get('text_loading');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_back']     = $this->language->get('button_back');
		$data['button_refresh']  = $this->language->get('button_refresh');

		// dump( $this->request->post) ;
		// form 表格內的錯誤訊息 ----------------------------------------------------------------------------------
		if (isset($this->session->data['error_warning'])) {
			$this->error['warning'] = $this->session->data['error_warning'];
			unset($this->session->data['error_warning']);
		}
		// dump( $this->error) ;
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'] ;
			foreach ( $this->error as $errKey => $tmpStr) {
				if ( $errKey != "warning") $data['error_warning'] .= "<br>{$tmpStr}" ;
			}
		} else {
			$data['error_warning'] = '' ;
		}
		// check orderInfo
		// dump( $this->request->post) ;
		if (isset($this->request->get['idx'])) {
			$this->rowInfo = $this->model_sale_order->getOrder($this->request->get['idx']) ;
			// dump( $this->rowInfo) ;
			$flightInfo = $this->model_site_scheduling->getFlightInfo($this->rowInfo['agreement_no']) ;
			$memoInfo = $this->model_site_scheduling->getInformationmemo($this->rowInfo['agreement_no']) ;

			// dump( $this->retCarNoOption()) ;
			list( $data['ckbCarNoHtml'], $car_seed, $car_total) = $this->retCarNoOption() ;

			$data['car_seed']  = $car_seed ;
			$data['car_seeds'] = $this->model_site_car_model->getCarModelLists( $car_seed) ;
			$data['car_total']  = $car_total ;
			foreach ($data['car_seeds'] as $i => $tmp) {
				$data['car_model'][] = $tmp['car_model'] ;
			}
		} else {
			if ( isset( $this->request->post['selCarModels'])) {
				$data['selCarModels'] =  $this->request->post['selCarModels'] ;
			}else{
				$data['selCarModels'] = "" ;
			}
			if ( isset( $this->request->post['selCarTypes'])) {
				$data['selCarTypes'] =  $this->request->post['selCarTypes'] ;
				$car_seeds           = $this->request->post['selCarTypes'] ;
				$car_seed            = join( '|', $this->request->post['selCarTypes']) ;
			}else{
				$data['selCarTypes'] = "" ;
			}
			if ( isset( $this->request->post['useCars'])) {
				$data['useCars'] =  $this->request->post['useCars'] ;
			}else{
				$data['useCars'] = "" ;
			}
			if ( isset( $this->request->post['total_car'])) {
				$data['total_car'] =  $this->request->post['total_car'] ;
			}else{
				$data['total_car'] = "" ;
			}

			if(isset($car_seed)){
				list( $data['ckbCarNoHtml'], $car_seed, $car_total) = $this->retCarNoOption() ;
				$data['car_seed']  = $car_seed ;
				$data['car_seeds'] = $this->model_site_car_model->getCarModelLists( $car_seed ) ;
				$data['car_total']  = $data['total_car'];
				foreach ($data['car_seeds'] as $i => $tmp) {
					$data['car_model'][] = $tmp['car_model'] ;
				}
			}else{
				$data['ckbCarNoHtml'] = "";
				$data['car_seed']     = array();
				$data['car_seeds']    = array();
				$data['car_model'][]  = array();
				$data['car_total']    =  "";
			}

		}

		if ( isset( $this->request->post['area_memo'])) {
			$data['area_memo'] = $this->request->post['area_memo'] ;
		} else if ( !empty($this->rowInfo)) {
			$data['area_memo'] = isset($memoInfo['memo'])?$memoInfo['memo']:'' ;
		} else {
			$data['area_memo'] = "";
		}

		$data["nowname"] = "";
		$data["userList"] = $this->model_site_option->getUserList('19,22', $this->user->getUserName()) ;
		foreach($data["userList"] as $arr){
			if($arr['username'] == $this->user->getUserName()){
				$data["nowname"] = $arr['lastname'].$arr['firstname'];
			}
		}

		// form 表格內的資料 --------------------------------------------------------------------------------------
		// dump( $this->request->post) ;
		$data['order_id'] = $this->setDataInfo( 'get', '', 'idx') ;

		// 方法一
		if ( isset( $this->request->post['rent_user_name'])) {
			$data['rent_user_name'] = $this->request->post['rent_user_name'] ;
		} else if ( !empty($this->rowInfo)) {
			$data['rent_user_name'] = $this->rowInfo['rent_user_name'] ;
		} else {
			$data['rent_user_name'] = '' ;
		}
		// 方法二
		$data['agreement_no']      = $this->setDataInfo( 'post', '', 'agreement_no') ;
		$data['rent_identity']     = $this->setDataInfo( 'post', '', 'rent_identity') ;
		$data['rent_user_id']      = $this->setDataInfo( 'post', '', 'rent_user_id') ;
		$data['rent_user_born']    = $this->setDataInfo( 'post', '', 'rent_user_born') ;
		$data['rent_user_tel']     = $this->setDataInfo( 'post', '', 'rent_user_tel') ;
		$data['rent_user_mail']    = $this->setDataInfo( 'post', '', 'rent_user_mail') ;

		$data['rent_zip1']         = $this->setDataInfo( 'post', '', 'rent_zip1') ;
		$data['rent_zip2']         = $this->setDataInfo( 'post', '', 'rent_zip2') ;
		// dump( array( $data['rent_zip1'], $data['rent_zip2'])) ;
		$data['rent_user_address'] = $this->setDataInfo( 'post', '', 'rent_user_address') ;
		// 取車人資訊
		$data['get_user_name']     = $this->setDataInfo( 'post', '', 'get_user_name') ;
		$data['get_identity']      = $this->setDataInfo( 'post', '', 'get_identity') ;
		$data['get_user_id']       = $this->setDataInfo( 'post', '', 'get_user_id') ;
		$data['get_user_born']     = $this->setDataInfo( 'post', '', 'get_user_born') ;
		$data['get_user_tel']      = $this->setDataInfo( 'post', '', 'get_user_tel') ;
		// 承租公司/緊急連絡人
		$data['rent_company_name'] = $this->setDataInfo( 'post', '', 'rent_company_name') ;
		$data['rent_company_no']   = $this->setDataInfo( 'post', '', 'rent_company_no') ;
		$data['urgent_man']        = $this->setDataInfo( 'post', '', 'urgent_man') ;
		$data['urgent_mobile']     = $this->setDataInfo( 'post', '', 'urgent_mobile') ;

		$data['order_type']        = $this->setDataInfo( 'post', '', 'order_type') ;
		$data['minsu_idx']         = $this->setDataInfo( 'post', '', 'minsu_idx') ;
		$data['charge_method']     = $this->setDataInfo( 'post', '', 'charge_method') ;
		$data['pay_type']          = $this->setDataInfo( 'post', '', 'pay_type') ;
		$data['minsu_name']        = "" ;

		// 成人 幼兒 嬰兒
		$data['adult']             = $this->setDataInfo( 'post', '', 'adult') ;
		$data['children']          = $this->setDataInfo( 'post', '', 'children') ;
		$data['baby']              = $this->setDataInfo( 'post', '', 'baby') ;

		// 配件資訊
		$data['baby_chair']        = $this->setDataInfo( 'post', '', 'baby_chair') ;
		$data['baby_chair1']       = $this->setDataInfo( 'post', '', 'baby_chair1') ;
		$data['baby_chair2']       = $this->setDataInfo( 'post', '', 'baby_chair2') ;
		$data['children_chair']    = $this->setDataInfo( 'post', '', 'children_chair') ;
		$data['gps']               = $this->setDataInfo( 'post', '', 'gps') ;
		$data['mobile_moto']       = $this->setDataInfo( 'post', '', 'mobile_moto') ;

		$data['spdc']              = $this->setDataInfo( 'post', '', 'spdc') ;  // 2020.04.15 add by Angus
		$data['pickdiff']          = $this->setDataInfo( 'post', '', 'pickdiff') ;  // 2020.04.15 add by Angus

		$data['total']             = $this->setDataInfo( 'post', '', 'total') ; // 2018.06.30 add by Angus

		$data['pickdiff']          = $this->setDataInfo( 'post', '', 'pickdiff') ;  // 2020.04.19 add by Jie
		//損失表
		$data['oil_cost']          = $this->setDataInfo( 'post', '', 'oil_cost') ;
		$data['overTime_cost']     = $this->setDataInfo( 'post', '', 'overTime_cost') ;
		$data['business_lose']     = $this->setDataInfo( 'post', '', 'business_lose') ;
		$data['car_lose']          = $this->setDataInfo( 'post', '', 'car_lose') ;
		$data['other_lose']        = $this->setDataInfo( 'post', '', 'other_lose') ;
		$data['lose_type']         = $this->setDataInfo( 'post', '', 'lose_type') ;
		//車輛總計
		$data['total_car']         = $this->setDataInfo( 'post', '', 'total_car') ;

		// 取得航空公司名稱選項
		$airlineArr = $this->model_site_option->getOptionItemsAllColArr( '125') ;
		$data['airlineArr'] = $airlineArr ;
		$data['airlineArr_rt'] = $airlineArr ;
		$airportArr = $this->model_site_option->getOptionItemsAllColArr( '134') ;
		$data['airportArr'] = $airportArr ;
		$data['airportArr_rt'] = $airportArr ;

		// 取得碼頭出發地名稱選項
		$pierArr = $this->model_site_option->getOptionItemsAllColArr( '131') ;
		$data['pierArr'] = $pierArr ;
		$data['pierArr_rt'] = $pierArr ;

		for($i=0; $i<2; $i++){
			$rt = "";
			if($i==1){
				$rt = "_rt";
			}
			$data['s_date'.$rt] = "" ;
			$data['s_date'.$rt] = "";
			$data['s_hour'.$rt] = "";
			$data['s_minute'.$rt] = "";
			$data['e_date'.$rt] = "" ;
			$data['e_date'.$rt] = "";
			$data['e_hour'.$rt] = "";
			$data['e_minute'.$rt] = "";

			if(isset($flightInfo['trans_type'.$rt])) {
				if ( $flightInfo['trans_type'.$rt] == 1) {
					$data['airlineShow'.$rt] = "inline" ;
					$data['flightnoShow'.$rt] = "inline" ;
					$data['pierShow'.$rt] = "none" ;
				} else if ( $flightInfo['trans_type'.$rt] == 2) {
					$data['airlineShow'.$rt] = "none" ;
					$data['flightnoShow'.$rt] = "none" ;
					$data['pierShow'.$rt] = "inline" ;
				}else {
					$data['airlineShow'.$rt] = "none" ;
					$data['flightnoShow'.$rt] = "none" ;
					$data['pierShow'.$rt] = "none" ;
				}
			} else {
				$data['airlineShow'.$rt] = "none" ;
				$data['flightnoShow'.$rt] = "none" ;
				$data['pierShow'.$rt] = "none" ;
			}

			// 航船資訊
			if ( isset( $this->request->post['trans_type'.$rt])) {
				$data['trans_type'.$rt] = $this->request->post['trans_type'.$rt] ;
			} else if ( isset( $flightInfo['trans_type'.$rt])) {
				$data['trans_type'.$rt] = $flightInfo['trans_type'.$rt] ;
			} else {
				$data['trans_type'.$rt] = "" ;
			}
			// 出發航站
			if ( isset( $this->request->post['airport'.$rt])) {
				$data['airport'.$rt] = $this->request->post['airport'.$rt] ;
			} else if ( isset( $flightInfo['airport'.$rt])) {
				$data['airport'.$rt] = $flightInfo['airport'.$rt] ;
			} else {
				$data['airport'.$rt] = "" ;
			}
			// 航空公司
			if ( isset( $this->request->post['airline'.$rt])) {
				$data['airline'.$rt] = $this->request->post['airline'.$rt] ;
			} else if ( isset( $flightInfo['airline'.$rt])) {
				$data['airline'.$rt] = $flightInfo['airline'.$rt] ;
			} else {
				$data['airline'.$rt] = "" ;
			}
			// 碼頭出發地
			if ( isset( $this->request->post['pier'.$rt])) {
				$data['pier'.$rt] = $this->request->post['pier'.$rt] ;
			} else if ( isset( $flightInfo['pier'.$rt])) {
				$data['pier'.$rt] = $flightInfo['pier'.$rt] ;
			} else {
				$data['pier'.$rt] = "" ;
			}
			// 班機編號
			if ( isset( $this->request->post['flight_no'.$rt])) {
				$data['flight_no'.$rt] = $this->request->post['flight_no'.$rt] ;
			} else if ( isset( $flightInfo['flight_no'.$rt])) {
				$data['flight_no'.$rt] = $flightInfo['flight_no'.$rt] ;
			} else {
				$data['flight_no'.$rt] = "" ;
			}
			if ( isset( $this->request->post['s_date'.$rt])) {
				$data['s_date'.$rt]   = $this->request->post['s_date'.$rt] ;
				$data['s_hour'.$rt]   = $this->request->post['s_hour'.$rt] ;
				$data['s_minute'.$rt] = $this->request->post['s_minute'.$rt] ;
			} else if ( isset( $flightInfo['s_date'.$rt]) && $flightInfo['s_date'] != '0000-00-00 00:00:00') {
				$data['s_date'.$rt] = $flightInfo['s_date'.$rt] ;
				if($flightInfo['s_date'] != "") {
					list($data['s_date'], $data['s_time']) = explode(" ", $flightInfo['s_date']) ;
					if(isset($data['s_time'])){
						list($data['s_hour'], $data['s_minute'], $data['s_sec']) = explode(":", $data['s_time']) ;
					}
				}

			} else {
				$data['s_date'.$rt] = "" ;
				$data['s_date'.$rt] = "";
				$data['s_hour'.$rt] = "";
				$data['s_minute'.$rt] = "";
			}
			if($rt == ""){
				if ( isset( $this->request->post['e_date'])) {
					$data['e_date']   = $this->request->post['e_date'] ;
					$data['e_hour']   = $this->request->post['e_hour'] ;
					$data['e_minute'] = $this->request->post['e_minute'] ;
				} else if ( isset( $flightInfo['e_date']) && $flightInfo['e_date'] != '0000-00-00 00:00:00') {
					$data['e_date'] = $flightInfo['e_date'] ;
					if($flightInfo['e_date'] != "") {
						list($data['e_date'], $data['e_time']) = explode(" ", $flightInfo['e_date']) ;
						if(isset($data['e_time'])){
							list($data['e_hour'], $data['e_minute'], $data['e_sec']) = explode(":", $data['e_time']) ;
						}
					}else{
						$data['e_date'] = "";
						$data['e_hour'] = "";
						$data['e_minute'] = "";
					}
				} else {
					$data['e_date'] = "";
					$data['e_hour'] = "";
					$data['e_minute'] = "";
				}


			}
			if ( isset( $flightInfo['e_date']) && $flightInfo['e_date'] != '0000-00-00 00:00:00') {
				list($data['s_date'.$rt], $data['s_time'.$rt]) = explode(" ", $flightInfo['e_date']) ;
				if(isset($data['s_time'.$rt])){
					list($data['s_hour'.$rt], $data['s_minute'.$rt], $data['s_sec'.$rt]) = explode(":", $data['s_time'.$rt]) ;
				}
			}else{
				$data['s_date'.$rt] = "";
				$data['s_time'.$rt] = "";
				$data['s_minute'.$rt] = "";
				$data['s_sec'.$rt] = "";

			}
		}

		// 分離租還車時間
		if ( isset( $this->request->post['rent_date_out'])) {
			$tmpTime = $this->request->post['rent_date_out'] . " " . $this->request->post['out_time'] ;
		} elseif (!empty( $this->rowInfo['rent_date_out'])) {
			$tmpTime = $this->rowInfo['rent_date_out'] ;
		} else {
			$tmpTime = ' ';
		}
		list( $strDate, $strTime)       = explode(" ", $tmpTime) ;
		$data['rent_date_out']          = $strDate ;
		$data['rent_date_out_time']     = strlen( $strTime) > 5 ? substr( $strTime, 0, strlen( $strTime) - 3) : $strTime ;

		if ( isset( $this->request->post['rent_date_in_plan'])) {
			$tmpTime = $this->request->post['rent_date_in_plan'] . " " . $this->request->post['plan_time'] ;
		} elseif (!empty( $this->rowInfo['rent_date_in_plan'])) {
			$tmpTime = $this->rowInfo['rent_date_in_plan'] ;
		} else {
			$tmpTime = ' ';
		}
		list( $strDate, $strTime)       = explode(" ", $tmpTime) ;
		$data['rent_date_in_plan']      = $strDate ;
		$data['rent_date_in_plan_time'] = strlen( $strTime) > 5 ? substr( $strTime, 0, strlen( $strTime) - 3) : $strTime ;

		// dump( $data) ;
		// 計算使用天數
		if ( !empty($data['rent_date_out']) && !empty($data['rent_date_in_plan'])) {
			$useSecond        = floor( ( strtotime( $data['rent_date_in_plan']. " " .$data['rent_date_in_plan_time']) - strtotime( $data['rent_date_out']. " " .$data['rent_date_out_time']))) ;
			$data['rentDay']  = floor( $useSecond / 86400) ;	// 24hr * 60分 * 60秒
			$data['rentHour'] = ( $useSecond % 86400) / 3600 ;	//
			//未滿1天要算1天 2020/08/27 jie
			if($data['rentDay'] == 0){
				$data['rentDay'] = 1;
				$data['rentHour']  = 0;
			}
		} else {
			$data['rentDay']  = "" ;
			$data['rentHour'] = "" ;
		}

		$data['s_station']              = $this->setDataInfo( 'post', '', 's_station') ;
		$data['e_station']              = $this->setDataInfo( 'post', '', 'e_station') ;

		// 處理地址下拉
		$optZip1Arr = $this->model_site_zip->getZipCode( ) ;
		$optZip2Arr = $this->model_site_zip->getZipCode( $this->setDataInfo( 'post', '', 'rent_zip1')) ;
		$data['optZip1Arr'] = $optZip1Arr ;
		$data['optZip2Arr'] = $optZip2Arr ;
		// dump( $data['optZip2Arr']) ;

		// 整理下拉選項資料
		$data['selRentTime'] = $this->model_site_option->getRentTime() ; // add by Angus 提供時間下拉選項 2018.02.08 改寫
		$order_types         = $this->model_site_option->getOptionItemsArr('8') ;	// 訂單類別
		// dump( $order_types) ;
		/* add by Angus 2018.08.31
		foreach ($order_types as $key => $tmpStr) { // 訂單管理  就不管其他選項了 add by Angus 2018.02.17
			// 調整為 9 網路訂單, 10 民宿, 11 長租, 123 保養車輛, 124 自行鎖車, 129 預約未完成, 130 應收
			if ( $key != 9 and $key != 10 and $key != 11 and $key != 124) unset( $order_types[$key]) ;
		} */
		$data['order_types'] = $order_types ;
		$data['car_models']  = $this->model_site_option->getOptionItemsArr('1') ;	// 車種分類
		$data['stations']    = $this->model_site_option->getOptionItemsArr('15') ;	// 分店名稱

		//取民宿業者名稱
		// if ( $data['order_type'] == 10 || $data['order_type'] == 130 || $data['order_type'] == 146) {
		if ( !empty($data['minsu_idx'])) {
			$retArr = $this->model_site_minshuku->getInformation( $data['minsu_idx']) ;
			$data['minsu_name'] = isset( $retArr['name']) ? $retArr['name'] : "" ;
			$data['minsuShow']  = "" ;
		} else {
			$data['minsuShow']  = 'style="display:none"' ;
		}

		// rent time Array 07:30
		$rentTimeArr = $this->model_site_option->getRentTime() ;
		$data['rentTimeOptArr'] = $rentTimeArr ;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		// dump( $data) ;
		$this->response->setOutput($this->load->view('sale/order_form', $data));
	}

	/** add by Angus 2017.07.08
 	* 前置作業
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'sale/order')) {
			$this->error['warning'] = $this->language->get('error_permission') ;
			return !$this->error;
		}

		// 頁籤 客戶資料
		if ( empty( $this->request->post['order_type'])) {
			$this->error['order_type'] = "訂單類別 未選擇" ;
			$input_order_type = "" ;
		} else {
			$input_order_type = $this->request->post['order_type'] ;
		}


		if ( !isset( $this->error['order_type']) && $input_order_type != '124') {
			if ( empty( $this->request->post['rent_user_name'])) {
				$this->error['rent_user_name'] = "承租人姓名 未填寫" ;
			}
			if ( empty( $this->request->post['rent_user_id'])) {
				$this->error['rent_user_id'] = "承租人 身份證字號/護照號碼 未填寫" ;
			}
			if ( empty( $this->request->post['rent_user_born'])) {
				$this->error['rent_user_born'] = "承租人 出生年月日 未填寫" ;
			}
			if ( empty( $this->request->post['rent_user_tel'])) {
				$this->error['rent_user_tel'] = "承租人 電話/手機 未填寫" ;
			}

			if ( empty( $this->request->post['get_user_name'])) {
				$this->error['get_user_name'] = "取車人姓名 未填寫" ;
			}
			if ( empty( $this->request->post['get_user_id'])) {
				$this->error['get_user_id'] = "取車人 身份證字號/護照號碼 未填寫" ;
			}
			if ( empty( $this->request->post['get_user_born'])) {
				$this->error['get_user_born'] = "取車人 出生年月日 未填寫" ;
			}
			if ( empty( $this->request->post['get_user_tel'])) {
				$this->error['get_user_tel'] = "取車人 電話/手機 未填寫" ;
			}
		}

		// 頁籤 訂單名細
		if ( empty( $this->request->post['pay_type'])) {
			$this->error['pay_type'] = "付款類型 未選擇" ;
		}

		if ( empty( $this->request->post['s_station'])) {
			$this->error['s_station'] = "取車地點 未選擇" ;
		}
		if ( empty( $this->request->post['e_station'])) {
			$this->error['e_station'] = "還車地點 未選擇" ;
		}
		if ( empty( $this->request->post['rent_date_out']) || empty( $this->request->post['out_time'])) {
			$this->error['rent_date_out'] = "預計取車時間 未選擇" ;
		}
		if ( empty( $this->request->post['rent_date_in_plan']) || empty( $this->request->post['plan_time'])) {
			$this->error['rent_date_in_plan'] = "預計還車時間 未選擇" ;
		}
		if ( empty( $this->request->post['useCars'])) {
			$this->error['useCars'] = "車號 未設定" ;
		}

		// 檢查相關資料是否可以存檔

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		return !$this->error;
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'sale/order')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	/**
	* 前置作業
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function preparation() {
		// 標題
		$data['heading_title'] = $this->language->get('heading_title') ;
		// 次標題
		$data['text_list'] = $this->language->get('text_list') ;
		$data['text_form'] = !isset($this->request->get['idx']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_confirm'] = $this->language->get('text_confirm') ;
		$data['text_enabled'] = $this->language->get('text_enabled') ;
		$data['text_disabled'] = $this->language->get('text_disabled') ;
		// 麵包屑
		$url = '';
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
			);
		$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url, 'SSL')
			);

		// 列表頁 按鈕
		$data['url_add'] = $this->url->link('sale/order/add', 'token=' . $this->session->data['token'] . $url, 'SSL') ;
		$data['url_delete'] = $this->url->link('sale/order/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['url_cancel'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url, true);
		if (!isset($this->request->get['idx'])) {
			$data['url_action'] = $this->url->link('sale/order/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['url_action'] = $this->url->link('sale/order/edit', 'token=' . $this->session->data['token'] . '&idx=' . $this->request->get['idx'] . $url, true);
		}

		$data['button_add']      = $this->language->get('text_add');
		$data['button_edit']     = $this->language->get('text_edit');
		$data['button_delete']   = $this->language->get('text_delete');
		$data['button_view']     = $this->language->get('button_view');
		$data['button_save']     = $this->language->get('button_save');
		$data['button_cancel']   = $this->language->get('button_cancel');
		$data['tab_general']     = $this->language->get('tab_general');
		$data['text_no_results'] = $this->language->get('text_no_results');

		// 提示訊息
		$data['error_warning'] = '';
		$data['success'] = '';
		return $data ;
	}

	/**
	 * [buildingData description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-02-13
	 */
	protected function buildingData() {
		$orderInfo = $this->request->post ;
		// 訂單 客戶資訊
		$insData['order_id']               = isset( $orderInfo['order_id']) ? $orderInfo['order_id'] : "" ;
		$insData['agreement_no']           = isset( $orderInfo['order_id']) ? $orderInfo['agreement_no'] : "" ;
		// 承租人資訊
		$insData['rent_user_name']         = isset( $orderInfo['rent_user_name']) ? $orderInfo['rent_user_name'] : "" ;
		$insData['rent_identity']          = isset( $orderInfo['rent_identity']) ? $orderInfo['rent_identity'] : "" ;
		$insData['rent_user_id']           = isset( $orderInfo['rent_user_id']) ? strtoupper($orderInfo['rent_user_id']) : "" ;
		$insData['rent_user_born']         = isset( $orderInfo['rent_user_born']) ? $orderInfo['rent_user_born'] : "" ;
		$insData['rent_user_tel']          = isset( $orderInfo['rent_user_tel']) ? $orderInfo['rent_user_tel'] : "" ;
		$insData['rent_user_mail']         = isset( $orderInfo['rent_user_mail']) ? $orderInfo['rent_user_mail'] : "" ;
		$insData['rent_zip1']              = isset( $orderInfo['rent_zip1']) ? $orderInfo['rent_zip1'] : "" ;
		$insData['rent_zip2']              = isset( $orderInfo['rent_zip2']) ? $orderInfo['rent_zip2'] : "" ;
		$insData['rent_user_address']      = isset( $orderInfo['rent_user_address']) ? $orderInfo['rent_user_address'] : "" ;
		// 領車人資訊
		$insData['get_user_name']          = isset( $orderInfo['get_user_name']) ? $orderInfo['get_user_name'] : "" ;
		$insData['get_identity']           = isset( $orderInfo['get_identity']) ? $orderInfo['get_identity'] : "" ;
		$insData['get_user_id']            = isset( $orderInfo['get_user_id']) ? strtoupper($orderInfo['get_user_id']) : "" ;
		$insData['get_user_born']          = isset( $orderInfo['get_user_born']) ? $orderInfo['get_user_born'] : "" ;
		$insData['get_user_tel']           = isset( $orderInfo['get_user_tel']) ? $orderInfo['get_user_tel'] : "" ;
		// 公司/緊急聯絡人
		$insData['rent_company_name']      = isset( $orderInfo['rent_company_name']) ? $orderInfo['rent_company_name'] : "" ;
		$insData['rent_company_no']        = isset( $orderInfo['rent_company_no']) ? $orderInfo['rent_company_no'] : "" ;
		$insData['urgent_man']             = isset( $orderInfo['urgent_man']) ? $orderInfo['urgent_man'] : "" ;
		$insData['urgent_mobile']          = isset( $orderInfo['urgent_mobile']) ? $orderInfo['urgent_mobile'] : "" ;
		// 訂單 租車資訊
		$insData['order_type']             = isset( $orderInfo['order_type']) ? $orderInfo['order_type'] : "" ;
		$insData['s_station']              = isset( $orderInfo['s_station']) ? $orderInfo['s_station'] : "" ;
		$insData['e_station']              = isset( $orderInfo['e_station']) ? $orderInfo['e_station'] : "" ;
		$insData['total']                  = isset( $orderInfo['total']) ? $orderInfo['total'] : "" ;
		// 取車還車時間
		$insData['rent_date_out']          = $orderInfo['rent_date_out'] ;
		$insData['rent_date_in_plan']      = $orderInfo['rent_date_in_plan'];
		$insData['rent_date_out_full']     = $orderInfo['rent_date_out'] . " " . $orderInfo['out_time'] ;
		$insData['rent_date_in_plan_full'] = $orderInfo['rent_date_in_plan'] . " " . $orderInfo['plan_time'] ;
		// 民宿
		$insData['minsu_idx']              = isset( $orderInfo['minsu_idx']) ? $orderInfo['minsu_idx'] : "" ;
		$insData['charge_method']          = isset( $orderInfo['charge_method']) ? $orderInfo['charge_method'] : "" ;
		$insData['pay_type']               = isset( $orderInfo['pay_type']) ? $orderInfo['pay_type'] : "" ;
		$insData['spdc']                   = isset( $orderInfo['spdc']) ? $orderInfo['spdc'] : "" ;
		$insData['pickdiff']               = isset( $orderInfo['pickdiff']) ? $orderInfo['pickdiff'] : "" ;
		// 成人 幼兒 嬰兒
		$insData['adult']                  = isset( $orderInfo['adult']) ? $orderInfo['adult'] : "" ;
		$insData['children']               = isset( $orderInfo['children']) ? $orderInfo['children'] : "" ;
		$insData['baby']                   = isset( $orderInfo['baby']) ? $orderInfo['baby'] : "" ;
		// 配件資訊
		$insData['baby_chair']             = isset( $orderInfo['baby_chair']) ? $orderInfo['baby_chair'] : "" ;
		$insData['baby_chair1']            = isset( $orderInfo['baby_chair1']) ? $orderInfo['baby_chair1'] : "" ;
		$insData['baby_chair2']            = isset( $orderInfo['baby_chair2']) ? $orderInfo['baby_chair2'] : "" ;
		$insData['children_chair']         = isset( $orderInfo['children_chair']) ? $orderInfo['children_chair'] : "" ;
		$insData['gps']                    = isset( $orderInfo['gps']) ? $orderInfo['gps'] : "" ;
		$insData['mobile_moto']            = isset( $orderInfo['mobile_moto']) ? $orderInfo['mobile_moto'] : "" ;

		//損失表
		$insData['oil_cost']               = isset( $orderInfo['oil_cost']) ? $orderInfo['oil_cost'] : "" ;
		$insData['overTime_cost']          = isset( $orderInfo['overTime_cost']) ? $orderInfo['overTime_cost'] : "" ;
		$insData['business_lose']          = isset( $orderInfo['business_lose']) ? $orderInfo['business_lose'] : "" ;
		$insData['car_lose']               = isset( $orderInfo['car_lose']) ? $orderInfo['car_lose'] : "" ;
		$insData['other_lose']             = isset( $orderInfo['other_lose']) ? $orderInfo['other_lose'] : "" ;
		$insData['lose_type']              = isset( $orderInfo['lose_type']) ? $orderInfo['lose_type'] : "" ;

		// 車型資訊
		$carsArr = $this->model_site_cars->getCarNoForList( $this->request->post['useCars']) ;
		// dump( $carsArr) ;
		$insData['car_model']              = isset( $orderInfo['car_seed']) ? $orderInfo['car_seed'] : "" ;
		$insData['car_model']              = $carsArr[0]['car_seed'] ;
		$insData['car_sn']                 = $carsArr[0]['idx'] ;
		$insData['car_no']                 = $carsArr[0]['car_no'] ;
		$insData['other_car']              = serialize( $carsArr) ; // 其他車輛資訊

		$insData['area_memo']              = isset( $orderInfo['area_memo']) ? $orderInfo['area_memo'] : "" ;

		// 航船資訊
		$insData['trans_type']             = isset( $orderInfo['trans_type']) ? $orderInfo['trans_type'] : "" ;
		$insData['airport']                = isset( $orderInfo['airport']) ? $orderInfo['airport'] : "" ;
		$insData['airline']                = isset( $orderInfo['airline']) ? $orderInfo['airline'] : "" ;
		$insData['pier']                   = isset( $orderInfo['pier']) ? $orderInfo['pier'] : "" ;
		$insData['flight_no']              = isset( $orderInfo['flight_no']) ? $orderInfo['flight_no'] : "" ;
		$insData['fs_date']                = isset( $orderInfo['s_date']) ? "{$orderInfo['s_date']} {$orderInfo['s_hour']}:{$orderInfo['s_minute']}" : "" ;
		$insData['fe_date']                = isset( $orderInfo['e_date']) ? "{$orderInfo['e_date']} {$orderInfo['e_hour']}:{$orderInfo['e_minute']}" : "" ;

		$insData['trans_type_rt']             = isset( $orderInfo['trans_type_rt']) ? $orderInfo['trans_type_rt'] : "" ;
		$insData['airport_rt']                = isset( $orderInfo['airport_rt']) ? $orderInfo['airport_rt'] : "" ;
		$insData['airline_rt']                = isset( $orderInfo['airline_rt']) ? $orderInfo['airline_rt'] : "" ;
		$insData['pier_rt']                   = isset( $orderInfo['pier_rt']) ? $orderInfo['pier_rt'] : "" ;
		$insData['flight_no_rt']              = isset( $orderInfo['flight_no_rt']) ? $orderInfo['flight_no_rt'] : "" ;
		$insData['fs_date_rt']                = isset( $orderInfo['s_date_rt']) ? "{$orderInfo['s_date_rt']} {$orderInfo['s_hour_rt']}:{$orderInfo['s_minute_rt']}" : "" ;
		$insData['fe_date_rt']                = isset( $orderInfo['e_date_rt']) ? "{$orderInfo['e_date_rt']} {$orderInfo['e_hour_rt']}:{$orderInfo['e_minute_rt']}" : "" ;

		$insData['total_car']              = isset( $orderInfo['total_car']) ? $orderInfo['total_car'] : "" ; // 總共幾台車

		return $insData ;
 	}

	// rework by Angus 2017.07.05 自動帶出
	public function autocomplete() {
		$json = array() ;

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		$filter_data = array(
			'filter_name'  => $filter_name,
			'start'        => 0,
			'limit'        => 5
		);

		$this->load->model('sale/order') ;
		$results = $this->model_sale_order->getCustomers($filter_data);

		foreach ($results as $result) {
			$json[] = array(
				"name" => $result['rent_user_name'],
			);
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json') ;
		$this->response->setOutput(json_encode($json)) ;
	}

	// rework by Angus 2017.07.06 自
	// 動帶出 先停用
	public function getOptionList() {
		$json = array();
		if (!empty($this->request->get['filter_name'])) {
			$this->load->model('site/option') ;

			$json = $this->model_site_option->getOptionItemsArr($this->request->get['filter_name']);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	// add by Angus 2017.07.09
	protected function getRequest( $method, $index) {
		switch ( $method) {
			case 'get':
				return isset( $this->request->get[$index]) ? $this->request->get[$index] : null ;
				break ;
			case 'post':
				return isset( $this->request->post[$index]) ? $this->request->post[$index] : null ;
				break ;
			default :
				return "";
				break ;
		}
	}

	/**
	 * [retCarNoOption 取得 牌照號碼 的option選項]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-06-03
	 */
	protected function retCarNoOption( ) {
		$ckbCarNoHtml = "" ;
		$selCars = array() ;

		// 簡潔取法
		// $filter['car_seed'] = join( '|', $this->setDataInfo( 'post', '', 'selCarTypes')) ;
		if ( isset( $this->request->post['selCarTypes'])) {
			$carSeeds = $this->request->post['selCarTypes'] ;
			$filter['car_seed'] = join( '|', $this->request->post['selCarTypes']) ;
		}

		// 另外多動作的取法 ---------------------------------------------------------------------------------------------------
		// 出車時間
		if ( isset($this->request->post['rent_date_out']) && isset($this->request->post['out_time'])) {
			$filter['s'] = $this->request->post['rent_date_out'] . " " . $this->request->post['out_time'] ;
		} else if ( isset( $this->rowInfo['rent_date_out'])) {
			$filter['s'] = $this->rowInfo['rent_date_out'] ;
		}
		// 還車時間
		if ( isset($this->request->post['rent_date_in_plan']) && isset($this->request->post['plan_time'])) {
			$filter['e'] = $this->request->post['rent_date_in_plan'] . " " . $this->request->post['plan_time'] ;
		} else if ( isset( $this->rowInfo['rent_date_in_plan'])) {
			$filter['e'] = $this->rowInfo['rent_date_in_plan'] ;
		}
		// 選用車輛
		$carSeeds = array() ;

		if ( isset( $this->request->post['useCars'])) {
			$selCars = $this->request->post['useCars'] ;
			$carSeeds = $this->request->post['selCarTypes'] ;

			$filter['car_sn'] = $selCars ;
			$filter['car_seed'] = join( '|', $carSeeds) ;
		} else if ( isset( $this->rowInfo['other_car'])) {
			$carsArr = unserialize( $this->rowInfo['other_car']) ;
			foreach ($carsArr as $iCnt => $tmpCar) {
				$selCars[]  = $tmpCar['idx'] ;
				$carSeeds[] = $tmpCar['car_seed'] ;
			}
			$filter['car_sn']   = $selCars ;
			$filter['car_seed'] = join( '|', $carSeeds) ;
		}

		$car_total = 0;
		if ( isset( $filter['s'])) {
			$carNoOption = $this->model_site_scheduling->getCarNoForAjax( $filter) ;
			$arrange = array() ;
			foreach ($carNoOption as $tmp) {
				$arrange[$tmp['car_seed']][] = $tmp;
			}
			$ckbCarNoHtml .= "<ul>" ;

			foreach ($arrange as $seedName => $seeds) {
				$ckbCarNoHtml .= "<li><p class=\"brand-class\">{$seedName}</p>" ;
				foreach ($seeds as $tmp) {
					if(in_array( $tmp['idx'], $selCars)){
						$car_total++;
					}
					$chkedStr = in_array( $tmp['idx'], $selCars) ? "checked" : "" ;
					$labelClassStr = in_array( $tmp['idx'], $selCars) ? "" : "bt-background-clean"  ;
					$ckbCarNoHtml .= "<label name=\"useCar\" class=\"btn btn-success {$labelClassStr}\" >
					<i class=\"fa fa-car\"></i>&nbsp;{$tmp['car_no']}
					<input class=\"hidden\" type=\"checkbox\" name=\"useCars[]\" value=\"{$tmp['idx']}\" p2=\"{$tmp['car_price']}\" {$chkedStr}/>
				</label>" ;
				}
				$ckbCarNoHtml .= "</li>" ;
			}
			$ckbCarNoHtml .= "</ul>" ;


		}

		return array( $ckbCarNoHtml, $carSeeds, $car_total) ;
	}

	/**
	 * [autocompleteForCarNo description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-05-23
	 */
	public function autocompleteForCarNo() {
		$this->load->model('sale/order') ;
		$this->load->model('site/scheduling') ;
		$filter['clean_sn'] = isset( $this->request->get['clean_sn']) ? $this->request->get['clean_sn'] : '' ;

		$selCars = array() ;
		if ( isset( $this->request->get['idx']) && !empty( $this->request->get['idx'])) {
			$orderInfo = $this->model_sale_order->getOrder( $this->request->get['idx']) ;
			// dump( $orderInfo) ;

			$carsArr = unserialize( $orderInfo['other_car']) ;
			foreach ($carsArr as $iCnt => $tmpCar) {
				$selCars[] = $tmpCar['idx'] ;
			}
			$filter['car_sn']   = $selCars ;
		}

		$filter['useCars'] = isset( $this->request->get['useCars']) ? $this->request->get['useCars'] : '' ;
		$filter['car_seed'] = isset( $this->request->get['car_seed']) ? $this->request->get['car_seed'] : '' ;
		$filter['s'] = isset( $this->request->get['s']) ? $this->request->get['s'] : '' ;
		$filter['e'] = isset( $this->request->get['e']) ? $this->request->get['e'] : '' ;

		$msgHtml = "" ;
		$clean_seedName = "";
 		if (isset($this->request->get['car_seed'])) {
			if($filter['clean_sn'] != ""){
				$clean_seedName = $this->model_site_scheduling->getSeedName($filter['clean_sn']) ;
			}
			$results = $this->model_site_scheduling->getCarNoForAjax( $filter) ;
			// dump($results) ;
			$arrange = array() ;
			foreach ($results as $tmp) {
				$arrange[$tmp['car_seed']][] = $tmp;
			}
			$car_arr = explode("|",$filter['useCars']);
			$msgHtml .= "<ul>" ;
			foreach ($arrange as $seedName => $seeds) {
				$msgHtml .= "<li><p class=\"brand-class\">{$seedName}</p>" ;
				foreach ($seeds as $tmp) {
					$chkedStr      = in_array( $tmp['idx'], $selCars) ? "checked" : "" ;
					$labelClassStr = in_array( $tmp['idx'], $selCars) ? "" : "bt-background-clean"  ;
					if($clean_seedName == $seedName){
						$chkedStr      =  "" ;
						$labelClassStr = "bt-background-clean";
					}else{
						$chkedStr      = in_array( $tmp['idx'], $car_arr) ? "checked" : "" ;
						$labelClassStr = in_array( $tmp['idx'], $car_arr) ? "" : "bt-background-clean"  ;
					}
					$msgHtml .= "<label name=\"useCar\" class=\"btn btn-success {$labelClassStr}\" >
					<i class=\"fa fa-car\"></i>&nbsp;{$tmp['car_no']}
					<input class=\"hidden\" type=\"checkbox\" name=\"useCars[]\" value=\"{$tmp['idx']}\" p2=\"{$tmp['car_price']}\" {$chkedStr}/>
				</label>" ;

				}
				$msgHtml .= "</li>" ;
			}
			$msgHtml .= "</ul>" ;
		}

		$msgHtml .= '<script type="text/javascript">

$("label[name=\'useCar\']").click( function () {
	var agreement_no = $(\'input[name="agreement_no"]\').val();
	var rentDay =$(\'#rentDay\').html() ;
	var rentHour =$(\'#rentHour\').html() ;
	rentDay = parseFloat(rentDay);
	rentHour = parseFloat(rentHour);
	if(rentHour>6){
		rentDay++;
	}

	// var classStr = $(this).attr(\'class\') ;
	// console.log( classStr);
	var ckbObj = $(this).find("input[type=\'checkbox\']")
	//console.log( ckbObj.is(\':checked\')) ;
	if ( ckbObj.is(\':checked\')) {
		$(this).removeClass(\'bt-background-clean\') ;
	} else {
		$(this).addClass(\'bt-background-clean\') ;
	}
	//車損金額 未上線
	var business_lose = $(\'input[name="business_lose"]\').val();
	var car_lose = $(\'input[name="car_lose"]\').val();
	var order_type=$(\'select[name="order_type"]\').val();
	//改全部訂單都要即時計算金額 2019/09/25
	//if(order_type == \'140\'){
		var c_num = 0;
		var c_price = 0;
		$(\'input[name^="useCars"]\').each(function() {
			if($(this).parent().attr("class").indexOf("clean")===-1){
				c_price += parseInt($(this).attr("p2"));
				c_num ++;
			}
		});

		c_price = parseInt(c_price);
		//mobile_moto 50元
		//gps 100元
		c_price += parseInt($("#mobile_moto").val())*50;
		//baby_chair1/baby_chair2/children_chair 100元
		c_price += parseInt($("#baby_chair1").val())*100;
		c_price += parseInt($("#baby_chair2").val())*100;
		c_price += parseInt($("#children_chair").val())*100;
		c_price += parseInt($("#gps").val())*100;
		//console.log(c_num+" "+c_price);
		if(business_lose != ""){
			c_price += parseInt(business_lose);
		}
		if(car_lose != ""){
			c_price += parseInt(car_lose);
		}
		var add_price = 0;
		parseInt(add_price);
		if(rentHour >=1 && rentHour <=6){
			rentHour =  Math.ceil(rentHour);
			add_price =  Math.ceil(c_price * 0.1) * rentHour ;
		}
		c_price = rentDay * c_price + add_price; //依天數要倍數計算

		if(rentDay > 1){
			c_price += parseInt($("#mobile_moto").val())*100;
			c_price += parseInt($("#mobile_moto").val())*50*(rentDay-1);
		}else{
			c_price += parseInt($("#mobile_moto").val())*100;
		}

		//檢查SPDC有沒有值
		var spdc = $(\'input[name="spdc"]\').val();
		parseInt(spdc);
		if(spdc == ""){
			spdc = 0;
		}
		c_price -= spdc;
		$("#t_money").html(c_price);
		$("#total").val(c_price);

		if(agreement_no == \'\'){
			//計算牌照數量
			$("#total_car").val(c_num);
		}

	//}
}) ;
		</script>';

		$msgHtml .= '';

		echo $msgHtml ;
	}
	/**
	 * [calculationDays 計算天數]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-05-28
	 */
	public function calculationDays() {
		$json = array() ;
		if ( isset( $this->request->get['s']) && isset( $this->request->get['e']) ) {
			$useSecond = floor( ( strtotime( $this->request->get['e']) - strtotime( $this->request->get['s']))) ;
			$rentDay   = floor( $useSecond / 86400) ;	// 24hr * 60分 * 60秒
			$rentHour  = ( $useSecond % 86400) / 3600 ;	//
			//小時沒有小數點，四捨五入 2020/08.13 jie
			$rentHour  = round($rentHour);

			//未滿24小時算1天 2020/08/27 jie
			if($rentDay == 0){
				$rentDay = 1;
				$rentHour = 0;
			}

			$json = array( $rentDay, $rentHour) ;
		}

		$this->response->addHeader('Content-Type: application/json') ;
		$this->response->setOutput(json_encode($json)) ;
	}

	/**
	 * [forOrderGetCarTypeJs description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-08-11
	 */
	public function forOrderGetCarTypeJs() {
echo <<<HTML
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript"><!--
{
	function getCarTypes() {
		var retStr = "" ;
		$("input[name='selCarTypes[]']").each( function() {
			// console.log( $(this).is(':checked')) ;
			if ( $(this).is(':checked')) {
				retStr += $(this).val() + "|" ;
			}
		});
		// console.log( retStr.length) ;
		if ( retStr.length > 0) {
			retStr = retStr.substring(0, retStr.length-1) ;
		}
		return retStr ;
	}


	$("input[name='selCarTypes[]']").click( function(event) {
		//目前牌照(要排除目前牌照)
		var useCarsStr = getuseCars() ;

		var sDate = $('input[name=\'rent_date_out\']').val() + " " + $('select[name=\'out_time\']').val() ;
		var eDate = $('input[name=\'rent_date_in_plan\']').val() + " " + $('select[name=\'plan_time\']').val() ;
		var order_id = $('input[name=\'order_id\']').val() ;
		// var sDate    = "2018-08-09 07:30" ;
		// var eDate    = "2018-08-11 18:30" ;
		var clean_sn = "";
		var pObj = $(this).parent() ;
		if ($(this).is(':checked')) {
			clean_sn = $(this).val();
			pObj.removeClass('bt-background-clean') ;
		} else {
			pObj.addClass('bt-background-clean') ;
		}
		var carTypeStr = getCarTypes() ;
		//console.log( carTypeStr) ;
		// var ajaxUrl = "?route=sale/order/autocompleteForCarNo&token={$this->session->data['token']}&car_seed="+carTypeStr+"&s="+sDate+"&e="+eDate+"&idx="+order_id ;
		var ajaxUrl = "?route=sale/order/autocompleteForCarNo&token={$this->session->data['token']}&car_seed="+carTypeStr+"&s="+sDate+"&e="+eDate+"&useCars="+useCarsStr+"&idx="+order_id+"&clean_sn="+clean_sn;
		//console.log( ajaxUrl) ;
		if ( carTypeStr != '') {
			$.ajax({
				url: ajaxUrl,
				dataType: 'html',
				success: function(html) {
					//console.log( html);
					$('#showArea').html( html) ;
					//更新訂車數量
					update_carnum();
				},
				error: function(xhr, ajaxOptions, thrownError) {
				//console.log( thrownError);
				//console.log( ajaxOptions);
				//console.log( xhr);
			}
			});
		} else {
			$('#showArea').html( '') ;
			//更新訂車數量
			update_carnum();
		}
	}) ;
}
</script>
HTML
;
	}


//-----------------------------------------------------------------------------------------------------------------------------------------
	public function cleanTestData() {
		echo "清除測試資料";
		$this->load->model('site/menu') ;

		$delArr = $this->model_site_menu->getOrderTypeNone11() ;
		// dump( $delArr) ;
		//
		$delAgreement = array() ;
		$delScheduling = array() ;
		foreach ($delArr as $iCnt => $rows) {
			$delAgreement[] = $rows['agreement_no'] ;
		}
		// dump( $delAgreement) ;
		$this->model_site_menu->delScheduling( $delAgreement) ;
		$this->model_site_menu->delOrderTypeNone11() ;
	}


	public function otherCarIsNull() {
		echo "修正訂單中 other_car欄位為空值的問題";
		$this->load->model('site/menu') ;

		$tmpArr = $this->model_site_menu->getOtherCarIsNull() ;
		// dump( count($tmpArr)) ;
		// dump( $tmpArr) ;
		foreach ($tmpArr as $iCnt => $row) {
			$order_id = $row['order_id'] ;
			$car_no   = $row['car_no'] ;
			$car_sn   = $row['car_sn'] ;
			$carInfo = $this->model_site_menu->getCarInfo( $car_no, $car_sn) ;
			// if ( count( $carInfo) != 1) {
			// 	dump( array($order_id,$car_no,$car_sn,$carInfo)) ;
			// }
			$strOtherCar = serialize( $carInfo) ;
			$this->model_site_menu->updateSQL( $order_id, $strOtherCar) ;
		}
	}

	function savepdf(){
		$now_url = HTTP_CATALOG.'?route=order/orderpdf&o1='.urlencode($this->request->get['o1']).'&m1='.urlencode($this->request->get['m1']);
		$filename = $this->request->get['o1'].'_'.date("YmdHis").".pdf";
		$pdf_file = $filename;
		$FILE_TABLE = DIR_IMAGE.'pdf/';
		$file_FOLDER = date("Ym").'/';	//資料夾依年月分類

		$showname = date("YmdHis");
		if(!file_exists($FILE_TABLE.$file_FOLDER.$filename)){
			if (!is_dir($FILE_TABLE)) {
				@mkdir($FILE_TABLE, 0777);
				@chmod($FILE_TABLE, 0777);
			}
			if (!is_dir($FILE_TABLE.$file_FOLDER)) {
				@mkdir($FILE_TABLE.$file_FOLDER, 0777);
				@chmod($FILE_TABLE.$file_FOLDER, 0777);
			}

			//產出一樣的html檔
			$buildInfo  = array( $now_url, $FILE_TABLE.$file_FOLDER.$filename) ;
			$this->htmltopdf->build_pdf_file($buildInfo);
		}

		header("Content-type:application/vnd.ms-pdf"); //告訴瀏覽器 為下載
		header('Content-Disposition: attachment;filename="'.$showname.'.pdf"');
		header('Cache-Control: max-age=0');
		readfile($FILE_TABLE.$file_FOLDER.$filename);
		exit();


	}

}
