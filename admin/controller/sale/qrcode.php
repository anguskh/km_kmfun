<?php
class Controllersaleqrcode extends Controller {
	private $error = array() ;
	private $carsPageLimit = 100 ;

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	public function __construct( $registry) {
		parent::__construct($registry) ;
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	public function index() {
		$this->load->language('sale/qrcode');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('sale/qrcode');

		$this->getList();
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	public function add() {
		$this->load->language('sale/qrcode') ;

		$this->document->setTitle($this->language->get('heading_title')) ;

		$this->load->model('sale/qrcode') ;

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_sale_qrcode->addInformation($this->request->post) ;

			$this->session->data['success'] = $this->language->get('text_success') ;

			$url = '' ;

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'] ;
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'] ;
			}

			$this->response->redirect($this->url->link('sale/qrcode', 'token=' . $this->session->data['token'] . $url, true)) ;
		}

		$this->getForm() ;
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	public function edit() {
		$this->load->language('sale/qrcode');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('sale/qrcode');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_sale_qrcode->editInformation($this->request->get['idx'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('sale/qrcode', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	public function delete() {
		$this->load->language('sale/qrcode');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('sale/qrcode');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {

			foreach ($this->request->post['selected'] as $useIdx) {
				$this->model_sale_qrcode->delUseIdx($useIdx);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('sale/qrcode', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function getList() {
		$data = $this->preparation() ;

		$page          = !isset( $this->request->get['page'])		? 1 : $this->request->get['page'] ;
		$sort          = !isset( $this->request->get['sort'])		? 'create_date' : $this->request->get['sort'] ;
		//$order         = !isset( $this->request->get['order'])		? 'ASC' : $this->request->get['order'] ;
		//$sel_statusStr = !isset( $this->request->get['sel_status'])	? '' : $this->request->get['sel_status'] ;
		//$sel_seedStr   = !isset( $this->request->get['sel_seed'])	? '' : $this->request->get['sel_seed'] ;
		$url = "" ;
		$data['column_action']   = $this->language->get('column_action');
		$data['text_no_results'] = $this->language->get('text_no_results');

		// 查尋條件參數
		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
		}
		if (isset($this->request->get['filter_customer'])) {
			$url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
		}
		if (isset($this->request->get['filter_mobile'])) {
			$url .= '&filter_mobile=' . $this->request->get['filter_mobile'];
		}
		$urlForPage = $url ;


		// 查詢條件陣列
		$filter_data = array(
			'start'               => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'               => $this->config->get('config_limit_admin')
		);

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}


		// 取得現在查詢資料筆數
		$totalCnt = $this->model_sale_qrcode->getTotalQrcode($filter_data);
		$results = $this->model_sale_qrcode->getQrcodes($filter_data);
		// dump( $stations) ;
		// dump( $results) ;



		$data['results'] = array();
		$totalCnt = (($totalCnt=='')?0:$totalCnt);
		if($totalCnt>0){
			foreach ($results as $ck => $result) {


				// dump( $result) ;
				switch ( $result['status']) {
					case '1':
						$result['status'] = "啟用" ;
						break;
					case '2':
						$result['status'] = "<font color=red>停用</font>" ;
						break;
					default :
						$result['status'] = "" ;
						break ;
				}
				$result['edit'] = $this->url->link('sale/qrcode/edit',
						'token=' . $this->session->data['token'] . '&idx=' . $result['idx'] . $url, true) ;
				
				//檢查檔案在不在 不在的話產生新檔 
				if(!file_exists(DIR_IMAGE.'qrcode/'.$result['qrcode'].'.png')){
					if (!is_dir(DIR_IMAGE . "qrcode/")) {
						@mkdir(DIR_IMAGE . "qrcode/", 0777);
					}
					$this->model_sale_qrcode->getImageSrc($result['qrcode']);
				}
				$result['qrcode'] = '<a href="'.HTTP_CATALOG.'image/qrcode/'.$result['qrcode'].'.png" target="_blank"><img src="'.HTTP_CATALOG.'image/qrcode/'.$result['qrcode'].'.png" height="20"></a>';

				$data['results'][] = $result ;


			}
		}
		// 定義欄位名稱
		// 2018.02.27 列表新增【訂單類別、取車日期、還車日期、還車地點】，如版面超出時可移除【建立日期】
		$columnNames = array(
				"qrcode"			 => 'QRCode',
				"remark"			 => '備註',
				"agreement_no"			 => '訂單編號',
				"flag"			 => '使用狀態',
				"ok_min"			=> '分鐘',
				"start_date"		=> '開始日期',
				"effective_date"	=> '截止日期',
				"status"			=> '狀態',

			) ;
		$data['columnNames'] = $columnNames ;
		$data['td_colspan'] = count( $columnNames) ;

		// 分頁程式區
		$pagination = new Pagination();
		$pagination->total = $totalCnt;
		$pagination->page = $page;
		// $pagination->limit = $this->config->get('config_limit_admin');
		$pagination->limit = $this->carsPageLimit ; // 50
		// dump( $pagination->limit) ;
		$pagination->url = $this->url->link('sale/qrcode', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);
		$data['pagination'] = $pagination->render();
		// 顯示 1 - 20 / 22 (共 2 頁)
		// 顯示 %d - %d / %d (共 %d 頁)
		// dump( $this->language->get('text_pagination')) ;
		// dump( $page) ;
		// dump( $this->config->get('config_limit_admin')) ;

		$data['indexDec'] = sprintf($this->language->get('text_pagination'),
			($totalCnt) ? (($page - 1) * $pagination->limit) + 1 :
			0, ((($page - 1) * $pagination->limit) > ($totalCnt - $pagination->limit)) ?
			$totalCnt : ((($page - 1) * $pagination->limit) + $pagination->limit),
			$totalCnt, ceil($totalCnt / $pagination->limit));


		$data['pagination'] = $pagination->render();
		// 準備各位置資訊 ----------------------------------------------------------------------------------------
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');


		$this->response->setOutput($this->load->view('sale/qrcode_list', $data));
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function getForm() {
		$data = $this->preparation() ;
		// add css 日曆
		$this->document->addStyle( "view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css") ;
		$this->document->addScript( "view/javascript/jquery/datetimepicker/moment.js") ;
		$this->document->addScript( "view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js") ;

		// dump( $this->request->get) ;
		$page                      = !isset( $this->request->get['page'])		? 1 : $this->request->get['page'] ;
		$sort                      = !isset( $this->request->get['sort'])		? 'car_seed' : $this->request->get['sort'] ;
		$url                       = "&sort={$sort}";
		$data['url_cancel']        = $this->url->link('sale/qrcode', 'token=' . $this->session->data['token'] . $url, true);


		$data['entry_title']       = $this->language->get('entry_title') ;
		$data['entry_description'] = $this->language->get('entry_description') ;
		$data['entry_online_date'] = $this->language->get('entry_online_date') ;
		$data['entry_status']      = $this->language->get('entry_status') ;

		if (isset($this->request->get['idx']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$rowInfo = $this->model_sale_qrcode->getInformation($this->request->get['idx']) ;
		}

		// form 表格內的錯誤訊息 ----------------------------------------------------------------------------------
		// $data['error_warning'] = $this->setDataInfo( 'error', '', 'warning') ;
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'] ;
		} else {
			$data['error_warning'] = '' ;
		}

		// 頁面狀態
		if (isset($this->request->post['sel_status'])) {
			$data['sel_status'] = $this->request->post['sel_status'] ;
		} elseif (!empty($rowInfo)) {
			$data['sel_status'] = $rowInfo['status'] ;
		} else {
			$data['sel_status'] = '1' ;
		}

		if (!empty($rowInfo)) {
			$data['input_start_date'] = $rowInfo['start_date'] ;
			$data['input_ok_min'] = $rowInfo['ok_min'] ;
			$data['input_remark'] = $rowInfo['remark'] ;
			$data['input_qrcode'] = $rowInfo['qrcode'] ;
			$data['sel_status'] = $rowInfo['status'] ;
		} else {
			$data['input_start_date'] = date("Y-m-d H:i:s") ;
			$data['input_ok_min'] = 30 ;
		}

		
		// form 表格內的資料 --------------------------------------------------------------------------------------
		$data['header'] = $this->load->controller('common/header') ;
		$data['column_left'] = $this->load->controller('common/column_left') ;
		$data['footer'] = $this->load->controller('common/footer') ;

		$this->response->setOutput($this->load->view('sale/qrcode_form', $data)) ;
     }

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function validateForm() {
		// 檢查是否有權限
		if (!$this->user->hasPermission('modify', 'sale/qrcode')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		// 檢查各項必填欄位 -----------------------------------------------------------------------------------------
		// 備註
		if ( $this->request->post['input_remark'] == '') {
			$this->error['input_remark'] = $this->language->get('error_remrk') ;
		}

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		return !$this->error;
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'sale/qrcode')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('sale/qrcode');

		foreach ($this->request->post['selected'] as $information_id) {
			if ($this->config->get('config_account_id') == $information_id) {
				$this->error['warning'] = $this->language->get('error_account');
			}

			if ($this->config->get('config_checkout_id') == $information_id) {
				$this->error['warning'] = $this->language->get('error_checkout');
			}

			if ($this->config->get('config_affiliate_id') == $information_id) {
				$this->error['warning'] = $this->language->get('error_affiliate');
			}

			if ($this->config->get('config_return_id') == $information_id) {
				$this->error['warning'] = $this->language->get('error_return');
			}

			// $store_total = $this->model_setting_store->getTotalStoresByInformationId($information_id);

			if ($store_total) {
				$this->error['warning'] = sprintf($this->language->get('error_store'), $store_total);
			}
		}

		return !$this->error;
	}


	/**
	* 前置作業
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function preparation() {
		// 標題
		$data['heading_title'] = $this->language->get('heading_title') ;
		// 次標題
		$data['text_list'] = $this->language->get('text_list') ;
		$data['text_form'] = !isset($this->request->get['idx']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_confirm'] = $this->language->get('text_confirm') ;
		$data['text_enabled'] = $this->language->get('text_enabled') ;
		$data['text_disabled'] = $this->language->get('text_disabled') ;
		// 麵包屑
		$url = '';
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
			);
		$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('sale/qrcode', 'token=' . $this->session->data['token'] . $url, 'SSL')
			);
		$data['token'] = $this->session->data['token'] ;

		// 列表頁 按鈕
		$data['url_add'] = $this->url->link('sale/qrcode/add', 'token=' . $this->session->data['token'] . $url, 'SSL') ;
		$data['url_delete'] = $this->url->link('sale/qrcode/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['url_cancel'] = $this->url->link('sale/qrcode', 'token=' . $this->session->data['token'] . $url, true);
		if (!isset($this->request->get['idx'])) {
			$data['url_action'] = $this->url->link('sale/qrcode/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['url_action'] = $this->url->link('sale/qrcode/edit', 'token=' . $this->session->data['token'] . '&idx=' . $this->request->get['idx'] . $url, true);
		}

		$data['button_add'] = $this->language->get('text_add');
		$data['button_edit'] = $this->language->get('text_edit');
		$data['button_delete'] = $this->language->get('text_delete');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['tab_general'] = $this->language->get('tab_general');

		// 提示訊息
		$data['error_warning'] = '';
		$data['success'] = '';
		return $data ;
	}
}