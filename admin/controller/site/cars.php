<?php
class ControllerSiteCars extends Controller {
	private $error = array() ;

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	public function __construct( $registry) {
		parent::__construct($registry) ;
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	public function index() {
		$this->load->language('site/cars') ;
		$this->document->setTitle($this->language->get('heading_title')) ;

		$this->load->model('site/cars') ;
		$this->load->model('site/car_model') ;
		$this->getList() ;
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	public function add() {
		$this->load->language('site/cars') ;

		$this->document->setTitle($this->language->get('heading_title')) ;

		$this->load->model('site/cars') ;

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_site_cars->addInformation($this->request->post) ;

			$this->session->data['success'] = $this->language->get('text_success') ;

			$url = '' ;

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'] ;
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'] ;
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'] ;
			}

			$this->response->redirect($this->url->link('site/cars', 'token=' . $this->session->data['token'] . $url, true)) ;
		}

		$this->getForm() ;
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	public function edit() {
		$this->load->language('site/cars');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('site/cars');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_site_cars->editInformation($this->request->get['idx'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('site/cars', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	public function delete() {
		$this->load->language('site/cars');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('site/cars');
		$this->load->model('site/car_model') ;

		if (isset($this->request->post['selected']) && $this->validateDelete()) {

			foreach ($this->request->post['selected'] as $useIdx) {
				$this->model_site_cars->delUseIdx($useIdx);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('site/cars', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function getList() {
		$data = $this->preparation() ;

		$data['mainCar'] = $this->model_site_car_model->getCarModelArr() ;
		// dump( $data['mainCar']) ;
		$searchCarModelArr = $this->model_site_car_model->getCarSeedOptionSearch() ;
		$data['searchCarModelArr'] = $searchCarModelArr ;
		// dump( $searchCarModelArr) ;

		$page          = !isset( $this->request->get['page'])		? 1 : $this->request->get['page'] ;
		$sort          = !isset( $this->request->get['sort'])		? 'car_seed' : $this->request->get['sort'] ;
		$order         = !isset( $this->request->get['order'])		? 'ASC' : $this->request->get['order'] ;
		$sel_statusStr = !isset( $this->request->get['sel_status'])	? '' : $this->request->get['sel_status'] ;
		$sel_seedStr   = !isset( $this->request->get['sel_seed'])	? '' : $this->request->get['sel_seed'] ;

		// dump( $this->request->get) ;

		// 訊息類 --------------------------------------------------------------------------------------------
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$data['column_action']   = $this->language->get('column_action');
		$data['text_no_results'] = $this->language->get('text_no_results');

		// 設定列表欄位 ---------------------------------------------------------------------------------
		$columnNames = array(
				"car_seed"  => '車型',
				"car_no"    => '車牌/編號',
				"car_color" => '顏色',
				"status"    => '狀態',
			) ;
		$data['columnNames'] = $columnNames ;
		$data['td_colspan']  = count( $columnNames) ;

		// 欄位排序設定
		$data['sort']  = $sort;
		$data['order'] = $order;

		// 欄位用的排序條件
		$url = '';
		$urlGetStr = "sel_status={$sel_statusStr}&sel_seed={$sel_seedStr}" ;

		if ($order == 'ASC') {
			$url .= "&order=DESC&{$urlGetStr}";
		} else {
			$url .= "&order=ASC&{$urlGetStr}";
		}

		$sortUrlArr = array (
			"car_seed"  => $this->url->link('site/cars', 'token=' . $this->session->data['token'] . "&sort=car_seed" . $url, true),
			"car_no"    => $this->url->link('site/cars', 'token=' . $this->session->data['token'] . "&sort=car_no" . $url, true),
			"car_color" => $this->url->link('site/cars', 'token=' . $this->session->data['token'] . "&sort=car_color" . $url, true),
			) ;
		$data['sortUrl'] = $sortUrlArr ;


		// 車型打勾
		if ( isset( $sel_statusStr) && $sel_statusStr != '') {
			$selStatusArr = explode('|', $sel_statusStr) ;
			if ( empty( $selStatusArr[count( $selStatusArr) -1])) array_pop( $selStatusArr) ;
		} else {
			$selStatusArr = array() ;
		}
		$data['selStatusArr'] = $selStatusArr ;

		// 車種全選
		if ( isset( $sel_seedStr) && $sel_seedStr != '') {
			$selSeedArr = explode('|', $sel_seedStr) ;
			if ( empty( $selSeedArr[count( $selSeedArr) -1])) array_pop( $selSeedArr) ;
		} else {
			$selSeedArr = array() ;
		}
		$data['selSeedArr'] = $selSeedArr ;
		// dump( $selSeedArr) ;

		$filter_data = array(
			'sort'     => $sort,
			'order'    => $order,
			"car_seed" => $selStatusArr,
			// 'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			// 'limit' => $this->config->get('config_limit_admin')
		);

		$totalCnt = $this->model_site_cars->getTotalCnt( $filter_data) ;
		$results = $this->model_site_cars->getLists( $filter_data) ;
		// 列表頁的 rows data
		$data['results'] = array();
		$url = "&order={$order}&sort={$sort}&{$urlGetStr}" ;
		foreach ($results as $result) {
			// dump( $result) ;
			switch ( $result['status']) {
				case '1':
					$result['status'] = "啟用" ;
					break;
				case '2':
					$result['status'] = "<font color=red>停用</font>" ;
					break;
				default :
					$result['status'] = "" ;
					break ;
			}
			$result['edit'] = $this->url->link('site/cars/edit',
					'token=' . $this->session->data['token'] . '&idx=' . $result['idx'] . $url, true) ;
			$result['car_seed'] = $searchCarModelArr[1][$result['car_seed']] ;

			$data['results'][] = $result ;
		}

		$pagination = new Pagination();
		$pagination->total = $totalCnt;
		$pagination->page = $page;
		// $pagination->limit = $this->config->get('config_limit_admin');
		$pagination->limit = $totalCnt ;
		$pagination->url = $this->url->link('site/cars', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		// 分頁
		// $data['indexDec'] = $data['indexDec'] = sprintf($this->language->get('text_pagination'),
		// 		($totalCnt) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0,
		// 		((($page - 1) * $this->config->get('config_limit_admin')) > ($totalCnt - $this->config->get('config_limit_admin'))) ? $totalCnt : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')),
		// 		$totalCnt,
		// 		ceil($totalCnt / $this->config->get('config_limit_admin')));
		$data['indexDec'] = $data['indexDec'] = sprintf($this->language->get('text_pagination'),
				($totalCnt) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0,
				$totalCnt,
				$totalCnt,
				1);
		// dump( $data['indexDec']) ;


		// 準備各位置資訊 ----------------------------------------------------------------------------------------
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('site/cars_list', $data));
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function getForm() {
		$data = $this->preparation() ;
		// add css 日曆
		$this->document->addStyle( "view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css") ;
		$this->document->addScript( "view/javascript/jquery/datetimepicker/moment.js") ;
		$this->document->addScript( "view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js") ;

		// dump( $this->request->get) ;
		$page                      = !isset( $this->request->get['page'])		? 1 : $this->request->get['page'] ;
		$sort                      = !isset( $this->request->get['sort'])		? 'car_seed' : $this->request->get['sort'] ;
		$order                     = !isset( $this->request->get['order'])		? 'ASC' : $this->request->get['order'] ;
		$sel_statusStr             = !isset( $this->request->get['sel_status'])	? '' : $this->request->get['sel_status'] ;
		$sel_seedStr               = !isset( $this->request->get['sel_seed'])	? '' : $this->request->get['sel_seed'] ;
		$url                       = "&order={$order}&sort={$sort}&sel_status={$sel_statusStr}&sel_seed={$sel_seedStr}";
		$data['url_cancel']        = $this->url->link('site/cars', 'token=' . $this->session->data['token'] . $url, true);


		$data['entry_title']       = $this->language->get('entry_title') ;
		$data['entry_description'] = $this->language->get('entry_description') ;
		$data['entry_online_date'] = $this->language->get('entry_online_date') ;
		$data['entry_status']      = $this->language->get('entry_status') ;

		if (isset($this->request->get['idx']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$rowInfo = $this->model_site_cars->getInformation($this->request->get['idx']) ;
		}

		// form 表格內的錯誤訊息 ----------------------------------------------------------------------------------
		// $data['error_warning'] = $this->setDataInfo( 'error', '', 'warning') ;
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'] ;
		} else {
			$data['error_warning'] = '' ;
		}

		if (isset($this->error['car_seed'])) {
			$data['error_car_seed'] = $this->error['car_seed'] ;
		} else {
			$data['error_car_seed'] = '' ;
		}

		if (isset($this->error['car_no'])) {
			$data['error_car_no'] = $this->error['car_no'];
		} else {
			$data['error_car_no'] = '' ;
		}

		if (isset($this->error['car_color'])) {
			$data['error_car_color'] = $this->error['car_color'];
		} else {
			$data['error_car_color'] = '' ;
		}

		// form 表格內的資料 --------------------------------------------------------------------------------------
		$this->load->model('site/car_model') ;
		$data['selCarSeedOpt'] = $this->model_site_car_model->getCarSeedOption() ;

		// $data['sel_car_seed']	= $this->setDataInfo( 'post', 'sel', 'car_seed') ;		// 車型
		// $data['sel_car_no']		= $this->setDataInfo( 'post', 'input', 'car_no') ;		// 車號/編號
		// $data['sel_car_color']	= $this->setDataInfo( 'post', 'input', 'car_color') ;	// 車體顏色

		if ( isset( $this->request->post['sel_car_seed'])) {
			$data['sel_car_seed'] = $this->request->post['sel_car_seed'] ;
		} else if ( !empty($rowInfo)) {
			$data['sel_car_seed'] = $rowInfo['car_seed'] ;
		} else {
			$data['sel_car_seed'] = '' ;
		}
		if ( isset( $this->request->post['input_car_no'])) {
			$data['input_car_no'] = $this->request->post['input_car_no'] ;
		} else if ( !empty($rowInfo)) {
			$data['input_car_no'] = $rowInfo['car_no'] ;
		} else {
			$data['input_car_no'] = '' ;
		}
		if ( isset( $this->request->post['input_car_color'])) {
			$data['input_car_color'] = $this->request->post['input_car_color'] ;
		} else if ( !empty($rowInfo)) {
			$data['input_car_color'] = $rowInfo['car_color'] ;
		} else {
			$data['input_car_color'] = '' ;
		}


		// 上線日期
		if (isset($this->request->post['input_online_date'])) {
			$data['input_online_date'] = $this->request->post['input_online_date'] ;
		} elseif (!empty($rowInfo)) {
			$data['input_online_date'] = $rowInfo['online_date'] ;
		} else {
			$data['input_online_date'] = '' ;
		}
		// 頁面狀態
		if (isset($this->request->post['sel_status'])) {
			$data['sel_status'] = $this->request->post['sel_status'] ;
		} elseif (!empty($rowInfo)) {
			$data['sel_status'] = $rowInfo['status'] ;
		} else {
			$data['sel_status'] = '1' ;
		}

		$data['header'] = $this->load->controller('common/header') ;
		$data['column_left'] = $this->load->controller('common/column_left') ;
		$data['footer'] = $this->load->controller('common/footer') ;

		$this->response->setOutput($this->load->view('site/cars_form', $data)) ;
     }

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function validateForm() {
		// 檢查是否有權限
		if (!$this->user->hasPermission('modify', 'site/cars')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		// 檢查各項必填欄位 -----------------------------------------------------------------------------------------
		// 車型
		if ( $this->request->post['sel_car_seed'] == '') {
			$this->error['car_seed'] = $this->language->get('error_car_seed') ;
		}
		// 車牌/編號
		if ( $this->request->post['input_car_no'] == '') {
			$this->error['car_no'] = $this->language->get('error_car_no') ;
		}
		// 顏色
		if ( $this->request->post['input_car_color'] == '') {
			$this->error['car_color'] = $this->language->get('error_car_color') ;
		}

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		return !$this->error;
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'site/cars')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('site/cars');

		foreach ($this->request->post['selected'] as $information_id) {
			if ($this->config->get('config_account_id') == $information_id) {
				$this->error['warning'] = $this->language->get('error_account');
			}

			if ($this->config->get('config_checkout_id') == $information_id) {
				$this->error['warning'] = $this->language->get('error_checkout');
			}

			if ($this->config->get('config_affiliate_id') == $information_id) {
				$this->error['warning'] = $this->language->get('error_affiliate');
			}

			if ($this->config->get('config_return_id') == $information_id) {
				$this->error['warning'] = $this->language->get('error_return');
			}

			// $store_total = $this->model_setting_store->getTotalStoresByInformationId($information_id);

			if ($store_total) {
				$this->error['warning'] = sprintf($this->language->get('error_store'), $store_total);
			}
		}

		return !$this->error;
	}

	/**
	 * [autocompleteForCarNo description]
	 * @return  [type]     [description]
	 * ?route=site/cars/autocompleteForCarNo&token=<?=$token?>&car_seed='+car_seed+'&car_no='+encodeURIComponent(request),
	 * @Another Angus
	 * @date    2018-02-14
	 */
	public function autocompleteForCarNo() {
		$this->load->model('site/cars') ;
		$json = array() ;
		$filter['car_seed'] = isset( $this->request->get['car_seed']) ? $this->request->get['car_seed'] : '' ;
		$filter['car_no'] = isset( $this->request->get['car_no']) ? $this->request->get['car_no'] : '' ;
 		if (isset($this->request->get['car_seed'])) {
			$results = $this->model_site_cars->getCarNoForAjax( $filter) ;
			// dump( $results) ;
			foreach ($results as $result) {
				$json[] = array(
					'code'	=> $result['idx'],
					// 'zip_name'	=> strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
					'name'	=> strip_tags(html_entity_decode($result['car_no'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	/**
	* 前置作業
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function preparation() {
		// 標題
		$data['heading_title'] = $this->language->get('heading_title') ;
		// 次標題
		$data['text_list'] = $this->language->get('text_list') ;
		$data['text_form'] = !isset($this->request->get['idx']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_confirm'] = $this->language->get('text_confirm') ;
		$data['text_enabled'] = $this->language->get('text_enabled') ;
		$data['text_disabled'] = $this->language->get('text_disabled') ;
		// 麵包屑
		$url = '';
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
			);
		$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('site/cars', 'token=' . $this->session->data['token'] . $url, 'SSL')
			);
		$data['token'] = $this->session->data['token'] ;

		// 列表頁 按鈕
		$data['url_add'] = $this->url->link('site/cars/add', 'token=' . $this->session->data['token'] . $url, 'SSL') ;
		$data['url_delete'] = $this->url->link('site/cars/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['url_cancel'] = $this->url->link('site/cars', 'token=' . $this->session->data['token'] . $url, true);
		if (!isset($this->request->get['idx'])) {
			$data['url_action'] = $this->url->link('site/cars/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['url_action'] = $this->url->link('site/cars/edit', 'token=' . $this->session->data['token'] . '&idx=' . $this->request->get['idx'] . $url, true);
		}

		$data['button_add'] = $this->language->get('text_add');
		$data['button_edit'] = $this->language->get('text_edit');
		$data['button_delete'] = $this->language->get('text_delete');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['tab_general'] = $this->language->get('tab_general');

		// 提示訊息
		$data['error_warning'] = '';
		$data['success'] = '';
		return $data ;
	}
}