<?php
class ControllerSiteScheduling extends Controller {
	protected $error      = array() ;
	protected $carInfo    = array() ;
	protected $orderInfo  = array() ; // add by Angus 2018.07.11
	protected $carStation = array() ;
	protected $devAdminn  = "" ; // 開發者

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	public function __construct( $registry) {
		parent::__construct($registry) ;
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	public function index() {
		$this->load->language('site/scheduling') ;
		$this->document->setTitle($this->language->get('heading_title')) ;

		$this->load->model('site/scheduling') ;
		$this->load->model('site/car_model') ;
		$this->load->model('site/option') ;
		$this->load->model('sale/order') ;

		$this->getList() ;
	}

	/**
	 * [add 新增]
	 * @Another Angus
	 * @date    2018-01-06
	 */
	public function add() {
		$this->load->language('site/scheduling') ;

		$this->document->setTitle($this->language->get('heading_title')) ;

		$this->load->model('site/scheduling') ;
		$this->load->model('site/car_model') ;
		$this->load->model('site/option') ;
		$this->load->model('site/zip') ;
		$this->load->model('site/minshuku') ;
		$this->load->model('sale/order') ;

			// dump( $this->request->get) ;
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			// dump( $this->request->post) ;
			// exit() ;

			$insData = $this->buildingData() ;
			// dump( $insData) ;
			// exit() ;
			switch ( $insData['sel_order_type']) {
				case '123':
				case '124':
					$insData['agreement_no'] = "" ;
					$this->model_site_scheduling->addInformation( $insData) ;

					break;

				default:
					// 檢查是否有加入會員
					$checkInfo = $this->model_site_scheduling->checkCustomerByPid( $insData['rent_user_id']) ;
					if ( empty( $checkInfo) ) {
						$this->model_site_scheduling->insCustomerInfo( $insData) ;
					}

					$this->model_site_scheduling->newOrder( $insData) ;
					$this->model_site_scheduling->addInformation( $insData) ;
					if ( $insData['sel_order_type'] == 10) {
						$this->model_site_scheduling->newFlight( $insData) ;
					}

					break;
			}

			// dump( $insData) ;
			// exit();

			$this->session->data['success'] = $this->language->get('text_success') ;
			$url = '' ;

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'] ;
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'] ;
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'] ;
			}

			$this->response->redirect("reload.html") ;
		}
		$this->getForm() ;
	}

	/**
	 * [edit 修改]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-01-06
	 */
	public function edit() {
		$this->load->language('site/scheduling') ;

		$this->document->setTitle($this->language->get('heading_title')) ;

		$this->load->model('site/scheduling') ;
		$this->load->model('site/car_model') ;
		$this->load->model('site/option') ;
		$this->load->model('site/zip') ;
		$this->load->model('sale/order') ;


			// dump( $this->request->get) ;
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			// dump( $this->request->post) ;

			$insData = $this->buildingData() ;

			switch ( $insData['sel_order_type']) {
				case '123' :
				case '124' :
					$insData['agreement_no'] = "" ;
					$this->model_site_scheduling->editInformation( $insData) ;

					break ;


				case '10' :
				case '11' :
					if ( empty( $this->request->post['agreement_no'])) {
						$insData['agreement_no'] = $this->newAgreementNo() ;
						// 檢查是否有加入會員
						$checkInfo = $this->model_site_scheduling->checkCustomerByPid( $insData['rent_user_id']) ;
						if ( empty( $checkInfo) ) {
							$this->model_site_scheduling->insCustomerInfo( $insData) ;
						}
						$this->model_site_scheduling->newOrder( $insData) ;
					} else {

						$this->model_site_scheduling->updateOrder( $insData) ;
					}

					$this->model_site_scheduling->editInformation( $insData) ;

					// 可能會有問題 改單的人 by Angus 2018.01.25
					if ( $insData['sel_order_type'] == 10) {
						// dump($insData);
						//$this->model_site_scheduling->updateFlight( $insData) ;
					}

					break ;

				default:

					$this->model_site_scheduling->updateOrder( $insData) ;
					$this->model_site_scheduling->editInformation( $insData) ;

					break;
			}
			//更新船班等等資訊
			$this->model_site_scheduling->updateFlight( $insData) ;

			$this->session->data['success'] = $this->language->get('text_success') ;
			$url = '' ;

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'] ;
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'] ;
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'] ;
			}
			$this->response->redirect("reload.html") ;
		}
		$this->getForm() ;
	}

	/**
	 * [view description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-02-06
	 */
	public function view() {

		$car_sn       = isset( $this->request->get['idx']) ? trim( $this->request->get['idx']) : "" ; // 從調度表來的檢視
		$scheduleIdx  = isset( $this->request->get['bookingID']) ? trim( $this->request->get['bookingID']) : "" ;
		$bookingDate  = isset( $this->request->get['bookingDate']) ? trim( $this->request->get['bookingDate']) : "" ;

		$agreement_no = isset( $this->request->get['ag']) ? trim( $this->request->get['ag']) : "" ;  // 從訂單管理來的檢視
	// [bookingID]   => 227				-> tb_scheduling.idx 車輛使用調度表 流水號
	// [bookingDate] => 2018-02-09		-> 車輛預約日期
	// [idx]         => 63 				-> 車輛流水號
	// [ag]          => 201807090013	-> 訂單編號

		$this->load->model('site/scheduling') ;
		$this->load->model('site/car_model') ;
		$this->load->model('site/minshuku') ;
		$this->load->model('site/option') ;
		$this->load->model('site/zip') ;
		$this->load->model('sale/order') ;

		// 如果從訂單過來的檢視 需由訂單編號來取得 scheduling 的info
		if ( $scheduleIdx == "" && $agreement_no != "") {
			$getSchedulingIdx = $this->model_site_scheduling->getSchedulingUseAgreementNo( $agreement_no) ;
			$scheduleIdx = $getSchedulingIdx ;
		}

		$this->rowInfo = $this->model_site_scheduling->getInformation( $car_sn, $scheduleIdx) ;
		// dump( $this->rowInfo) ;
		// 取得車子資訊 車種,車型,車號
		$carInfo = $this->model_site_car_model->getCarInfo( $car_sn) ;
		$this->carInfo = $carInfo ;
		// dump( $carInfo) ;
		// 取得訂單選項 訂單類別
		$orderType = $this->model_site_option->getOptionItemsArr( '8') ;
		// dump( $orderType) ;
		// 取得車站點名稱選項
		$carStations = $this->model_site_option->getOptionItemsAllColArr( '15') ;
		foreach ($carStations as $key => $tmpArr) {
			$carStations[$tmpArr['idx']] = $tmpArr ;
		}
		// 訂單資訊 檢查調度表是否有訂單編號
		if ( !empty( $this->rowInfo['order_no'])) {
			$this->orderInfo = $this->model_sale_order->getOrderInfoByAgreement_no( $this->rowInfo['order_no']) ;
			$detailPriceStr  = $this->buildingRentPriceData() ; // 組合租車費用明細
		} else {
			$this->orderInfo['agreement_no'] = "" ;
			$this->orderInfo['total_car']    = 1 ;
			$this->orderInfo['total']        = "" ;
			$this->orderInfo['total2']       = "" ;
			$detailPriceStr                  = "" ;
		}
		// dump( $orderInfo) ;
		$minsuName = "";
		$minsuInfo = "";
		// 取得民宿資料
		if ( !empty( $minsuInfo)) {
			// 10 民宿定單, 130 應收
			if ( $orderInfo['order_type'] == 10 || $orderInfo['order_type'] == 130) {
				$minsuInfo = $this->model_site_minshuku->getInformation( $orderInfo['minsu_idx']) ;
				$minsuName = $minsuInfo['name'] ;
			} else {
				$minsuName = "" ;
			}
		}

		$msgStr   = "<b>車輛行事曆資訊</b><br>" ;
		$msgStr   .= "車種 : {$this->carInfo['car_model_name']}<br>" ;
		$msgStr   .= "車型 : {$this->carInfo['car_seed']}<br>" ;
		$msgStr   .= "訂車數量 : {$this->orderInfo['total_car']}<br><hr>" ;

		$sStation = !empty( $this->rowInfo['s_station']) ? $carStations[$this->rowInfo['s_station']]['opt_name'] : "" ;
		$eStation = !empty( $this->rowInfo['e_station']) ? $carStations[$this->rowInfo['e_station']]['opt_name'] : "" ;
		$sTimeStr = substr( $this->rowInfo['start_date'], 0, strlen( $this->rowInfo['start_date']) - 3) ;
		$eTimeStr = substr( $this->rowInfo['end_date'], 0, strlen( $this->rowInfo['end_date']) - 3) ;
		$msgStr   .= "<b>【租車資訊】</b><br>" ;
		$msgStr   .= "訂單編號 : {$this->orderInfo['agreement_no']}<br>" ;
		$msgStr   .= "訂單類別 : {$orderType[$this->rowInfo['order_type']]}<br>" ;
		$msgStr   .= "民宿(業者) : {$minsuName}<br>" ;
		$msgStr   .= "取車地點 : {$sStation}<br>" ;
		$msgStr   .= "還車地點 : {$eStation}<br>" ;
		$msgStr   .= "預計取車時間 : {$sTimeStr}<br>" ;
		$msgStr   .= "預計還車時間 : {$eTimeStr}<br>" ;
		// $msgStr   .= "租車費用 : {$this->orderInfo['total']} | {$this->orderInfo['total2']}<br>" ;
		$msgStr   .= "租車費用 : {$detailPriceStr}<br>" ;
		$msgStr   .= "備註 : {$this->rowInfo['memo']}<br><hr>" ;

		// 檢查是否有訂單編號
		// 沒有訂單編號的情形有 : 自行鎖車, 車輛保養 || 預約未完成 | 應收
		if ( !empty( $this->orderInfo['agreement_no'])) {
			$zipRetArr = $this->model_site_zip->getZipStr( $this->orderInfo['rent_zip1']) ;

			$addStr = !empty( $zipRetArr) ? $zipRetArr['zip_name'] : "" ;
			if ( !empty( $this->orderInfo['rent_zip2'])) {
				$subZipCode = $this->model_site_zip->getZipStr( $this->orderInfo['rent_zip2']) ;
				$addStr .= " " . $this->orderInfo['rent_zip2'] . " " . $subZipCode['zip_name'] ;
			}
			$addStr .= $this->orderInfo['rent_user_address'] ;

			// 航班 add by Angus 2018.07.21 ---------------------------------------------------------------------------------
			$isAirInfo = $this->model_site_scheduling->getFilght( $this->orderInfo['agreement_no']) ;
			// dump( $isAirInfo) ;
			if ( isset( $isAirInfo['order_no'])) {
				$airportArr = $this->model_site_option->getOptionArrUseParent( array(134, 131)) ; // 航/船班資訊
				// dump( $airportArr) ;
				foreach ($airportArr as $iCnt => $tmpRow) {
					if ( $tmpRow['idx'] == $isAirInfo['airport'] || $tmpRow['idx'] == $isAirInfo['pier']) {
						$airInfoStr = $tmpRow['opt_short_desc'] . " " . $isAirInfo['s_date'] ;
						break ;
					}
				}
			} else {
				$airInfoStr = "自取" ;
			}
			// 航班 add by Angus 2018.07.21 End -----------------------------------------------------------------------------



			$msgStr .= "<b>【客戶資料】</b><br>" ;
			$msgStr .= "承租人姓名 :				{$this->orderInfo['rent_user_name']}<br>" ;
			$msgStr .= "身分證字號／護照號碼 :		{$this->orderInfo['rent_user_id']}<br>" ;
			$msgStr .= "出生年月日 :				{$this->orderInfo['rent_user_born']}<br>" ;
			$msgStr .= "手機 :					{$this->orderInfo['rent_user_tel']}<br>" ;
			$msgStr .= "取車人姓名 :				{$this->orderInfo['get_user_name']}<br>" ;
			$msgStr .= "身分證字號／護照號碼 : 	{$this->orderInfo['get_user_id']}<br>" ;
			$msgStr .= "出生年月日 :				{$this->orderInfo['get_user_born']}<br>" ;
			$msgStr .= "手機 : 					{$this->orderInfo['get_user_tel']}<br>" ;
			$msgStr .= "航班時間 :               {$airInfoStr}<hr><br>" ;
			$msgStr .= "通訊地址 : 				{$addStr}<br>" ;
			$msgStr .= "公司名稱 :				{$this->orderInfo['rent_company_name']}<br>" ;
			$msgStr .= "公司統編 :				{$this->orderInfo['rent_company_no']}<br>" ;
			$msgStr .= "緊急連絡人 : 				{$this->orderInfo['urgent_man']}<br>" ;
			$msgStr .= "緊急連絡人電話 : 			{$this->orderInfo['urgent_mobile']}<br>" ;
		} else {
			echo $msgStr ;
			exit();
		}

		// 配件
		$tmpOther = "" ;
		if($this->orderInfo['baby_chair'] > 0) {
			$tmpOther .= "嬰兒安全座椅 x {$this->orderInfo['baby_chair']}" ;
		}
		if($this->orderInfo['baby_chair1'] > 0) {
			if($tmpOther != "")
				$tmpOther .= "\r\n　　&nbsp;&nbsp;&nbsp;";
			$tmpOther .= "兒童安全座椅 x {$this->orderInfo['baby_chair1']}" ;
		}
		if($this->orderInfo['baby_chair2'] > 0) {
			if($tmpOther != "")
				$tmpOther .= "\r\n　　&nbsp;&nbsp;&nbsp;";
			$tmpOther .= "兒童增高墊 x {$this->orderInfo['baby_chair2']}" ;
		}
		if($this->orderInfo['children_chair'] > 0) {
			if($tmpOther != "")
				$tmpOther .= "\r\n　　&nbsp;&nbsp;&nbsp;";
			$tmpOther .= "嬰兒推車 x {$this->orderInfo['children_chair']}" ;
		}
		if($this->orderInfo['gps'] > 0) {
			if($tmpOther != "")
				$tmpOther .= "\r\n　　&nbsp;&nbsp;&nbsp;";
			$tmpOther .= "GPS x {$this->orderInfo['gps']}" ;
		}
		if($this->orderInfo['mobile_moto'] > 0) {
			if($tmpOther != "")
				$tmpOther .= "\r\n　　&nbsp;&nbsp;&nbsp;";
			$tmpOther .= "機車導航架 x {$this->orderInfo['mobile_moto']}" ;
		}
		if($tmpOther == "")
			$tmpOther .= "無";

		$msgStr .= "<hr>配件 : " . $tmpOther;

		echo $msgStr ;
	}
	/**
	*
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	public function delete() {

		$this->load->language('site/scheduling');

		$this->load->model('site/scheduling') ;
		$this->load->model('site/car_model') ;
		$this->load->model('site/option') ;
		$this->load->model('site/zip') ;
		$this->load->model('site/minshuku') ;
		$this->load->model('sale/order') ;

		// dump( $this->request->get) ;
		if ( !empty($this->request->get['bookingID']) && $this->validateDelete()) {
			$this->model_site_scheduling->delUseIdx( $this->request->get['bookingID']) ;
			$this->session->data['success'] = $this->language->get('text_success') ;
			$this->response->redirect("reload.html") ;
		}
		$this->getForm() ;
	}

	public function renew() {
		$this->response->redirect("reload.html") ;
	}

	/**
	*
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function getList() {
		$data = $this->preparation() ;
		// dump( $this->request) ;
		// dump( $data) ;
		// [REQUEST_URI] => /kmfun/admin/?route=site/scheduling&token=S8YPX3fbEI17sAUQhUUBz0qoIO9XWL1e
		$data['aLink'] = str_replace("/kmfun/nimda/", "", $this->request->server['REQUEST_URI']) ;

		if ( $this->session->data['user_group_id'] == 1) {
			$this->devAdmin = true ;
			$data['devAdmin'] = $this->devAdmin ;
		}

		$this->document->addStyle( "view/stylesheet/default.css?v={$data['cssVer']}") ;
		// add css 日曆
		$this->document->addStyle( "view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css") ;
		$this->document->addScript( "view/javascript/tooltip2.js") ;
		$this->document->addScript( "view/javascript/jquery/datetimepicker/moment.js?".time()) ;
		$this->document->addScript( "view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js") ;

		// dump( $this->request->get) ;
		// 不知道是什麼可以關掉
		// $page = 1 ;
		if (isset($this->request->get['carseed'])) {
			$page = $this->request->get['carseed'];
		} else {
			$page = 1;
		}

		// 訊息類 --------------------------------------------------------------------------------------------
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$url = '';

		$data['column_action'] = $this->language->get('column_action');
		$data['text_no_results'] = $this->language->get('text_no_results');

		// 分頁功能 -------------------------------------------------------------------------------------------
		// 車型選項
		// $carSeedArr = $this->model_site_car_model->getCarSeedOption() ;
		// $data['carSeedArr'] = $carSeedArr ;

		// 車型打勾
		if ( isset( $this->request->get['sel_status']) && $this->request->get['sel_status'] != '') {
			$selStatusArr = explode('|', $this->request->get['sel_status']) ;
			if ( empty( $selStatusArr[count( $selStatusArr) -1])) array_pop( $selStatusArr) ;
		} else {
			$selStatusArr = array() ;
		}
		$data['selStatusArr'] = $selStatusArr ;

		// 車種全選
		if ( isset( $this->request->get['sel_seed']) && $this->request->get['sel_seed'] != '') {
			$selSeedArr = explode('|', $this->request->get['sel_seed']) ;
			if ( empty( $selSeedArr[count( $selSeedArr) -1])) array_pop( $selSeedArr) ;
		} else {
			$selSeedArr = array() ;
		}
		$data['selSeedArr'] = $selSeedArr ;

		$data['mainCar']           = $this->model_site_car_model->getCarModelArr() ;
		$data['searchCarModelArr'] = $this->model_site_car_model->getCarSeedOptionSearch() ;

		// 日曆表 查詢區間 (三天、一週、兩週、一個月、二個月、三個月)
		$cycleIndex = isset( $this->request->get['cycle']) ? intval( $this->request->get['cycle']) : 1 ;
		$cycleIndex = ($cycleIndex == 0) ? 1 : $cycleIndex ;
		$cycleArr = array() ;
		/**
		 * [0] label class name = active
		 * [1] radio selected = checked
		 */
		for ($i=1; $i < 7; $i++) {
			$cycleArr[$i][0] = "" ; // label class name
			$cycleArr[$i][1] = "" ; // radio selected
		}
		$cycleArr[$cycleIndex][0] = "active" ;
		$cycleArr[$cycleIndex][1] = "checked" ;
		$data['cycleArr'] = $cycleArr ;
		// dump( $data['cycleArr']) ;

		$startDay = ( isset( $this->request->get['sdate']) && !empty( $this->request->get['sdate'])) ? $this->request->get['sdate'] : date('Y-m-d') ;
		$dayRangeArr = $this->getViewDayRange( $cycleIndex, $startDay) ;
		// exit() ;
		$data['dayRangeArr'] = $dayRangeArr ;
		// dump( $dayRangeArr) ;
		$tdCalendar = $this->getDateRange( $dayRangeArr) ;
		// dump( $tdCalendar) ;

		// 車輛列表
		$this->carInfo = $this->model_site_car_model->getCarsForCarSeed( $selStatusArr) ;
		$data['rowCars'] = $this->carInfo ;
		// unset( $data['rowCars'][64]) ;
		// unset( $data['rowCars'][65]) ;
		// unset( $data['rowCars'][66]) ;
		// dump( $data['rowCars']) ;

		// 地點列表
		$this->carStation = $this->model_site_option->getOptionNameArr( 15) ;
		// dump( $this->carStation) ;

		// 出發航站位置
		// [135] => 台北
		// [136] => 高雄
		// [137] => 台南
		// [138] => 台中
		// [139] => 嘉義
		$airportRows = $this->model_site_option->getOptionItemsAllColArr( '134') ;
		$fromAirportArr = array() ;
		foreach ($airportRows as $key => $tmpRow) {
			$fromAirportArr[$tmpRow['idx']] = $tmpRow['opt_name'] ;
		}

		// 預約清單
		$reserve = $this->model_site_scheduling->getScheduling( $selStatusArr, $dayRangeArr) ;
		$orderNoArr = array() ;
		foreach ($reserve as $iCnt => $scheduling) {
			if ( !empty( $scheduling['order_no'])) {
				$orderNoArr[] = $scheduling['order_no'] ;
			}
		}

		$orderSimpleInfo = $this->model_sale_order->getOrdersFromScheduling( $orderNoArr) ;
		$flightInfoArr = $this->model_site_scheduling->getFlightInfoArr( $orderNoArr) ;
		// dump( $flightInfoArr) ;

		$iconTipArr = array() ;
		foreach ( $reserve as $iCnt => $node) {

			$flightInfo = isset( $flightInfoArr[$node['order_no']]) ? $flightInfoArr[$node['order_no']] : "" ;
			$node['orderInfo'] = isset( $orderSimpleInfo[$node['order_no']]) ? $orderSimpleInfo[$node['order_no']] : "" ;

			// dump( $flightInfo) ;
			$node['trans_type']       = isset($flightInfo["trans_type"]) ? $flightInfo["trans_type"] : "" ;
			$node['airport']          = isset($flightInfo["airport"]) ? $flightInfo["airport"] : "" ;
			$node['airline_name']     = isset($flightInfo["airline_name"]) ? $flightInfo["airline_name"] : "" ;
			$node['flight_no']        = isset($flightInfo["flight_no"]) ? $flightInfo["flight_no"] : "" ;
			$node['s_date']           = (isset($flightInfo["s_date"]) && ($flightInfo["s_date"] != "0000-00-00 00:00:00")) ? substr($flightInfo["s_date"], 0, -3) : "" ;
			$node['e_date']           = (isset($flightInfo["e_date"]) && ($flightInfo["e_date"] != "0000-00-00 00:00:00")) ? substr($flightInfo["e_date"], 0, -3) : "" ;
			// dump( $node) ;
			$iconTipArr[$node['idx']] = $this->generateTipDesc( $node, $fromAirportArr) ;
		}
		// dump( $iconTipArr) ;
		$data['iconTip'] = $iconTipArr ;

		$hourStart	= 7 ;
		$hourEnd	= 18 ;
		$data['hourArea'] = array( $hourStart, $hourEnd) ;

		foreach( $tdCalendar as $iCal => $tdVal) {
			// echo $tdVal[1]."<br>" ;
			if ( is_array( $reserve)) {
				foreach ($reserve as $node) {
					// dump( $node) ;
					for ( $i = $hourStart ; $i <= $hourEnd ; $i++) {
						$shiftCal = strtotime( "{$tdVal[1]} ". str_pad($i, 2, '0', STR_PAD_LEFT).":00:00") ;
						if ( strtotime( $node['start_date']) <= $shiftCal && strtotime( $node['end_date']) > $shiftCal) {
							// dump( array( "{$node['car_idx']} if 上半場 {$i}:00",
							// 	strtotime( $node['start_date']),
							// 	strtotime( "{$tdVal[1]} ". str_pad($i, 2, '0', STR_PAD_LEFT).":00:00"),
							// 	strtotime( $node['end_date'])
							// )) ;

							$colorStr = $this->checkOrderType( $node['order_type'], $node['pay_type']) ;
							$tdCalendar[$iCal][3][$node['car_idx']][$i][0] = $colorStr ;
							$tdCalendar[$iCal][4][$node['car_idx']][$i][0] = $node['idx'] ;
						} else {
							// dump( array( "{$node['car_idx']} else 上半場 {$i}:00",
							// 	strtotime( $node['start_date']),
							// 	strtotime( "{$tdVal[1]} ". str_pad($i, 2, '0', STR_PAD_LEFT).":00:00"),
							// 	strtotime( $node['end_date'])
							// )) ;
						}
						$shiftCal = strtotime( "{$tdVal[1]} ". str_pad($i, 2, '0', STR_PAD_LEFT).":30:00") ;
						if ( strtotime( $node['start_date']) <= $shiftCal && strtotime( $node['end_date']) > $shiftCal) {
							// dump( array( "{$node['car_idx']} if 下半場 {$i}:30",
							// 	strtotime( $node['start_date']),
							// 	strtotime( "{$tdVal[1]} ". str_pad($i, 2, '0', STR_PAD_LEFT).":30:00"),
							// 	strtotime( $node['end_date'])
							// )) ;
							$colorStr = $this->checkOrderType( $node['order_type'], $node['pay_type']) ;
							$tdCalendar[$iCal][3][$node['car_idx']][$i][1] = $colorStr ;
							$tdCalendar[$iCal][4][$node['car_idx']][$i][1] = $node['idx'] ;
						} else{
							// dump( array( "{$node['car_idx']} else 下半場 {$i}:30",
							// 	strtotime( $node['start_date']),
							// 	strtotime( "{$tdVal[1]} ". str_pad($i, 2, '0', STR_PAD_LEFT).":30:00"),
							// 	strtotime( $node['end_date'])
							// )) ;
						}
					}
				}
			}
		}

		$data['tdCalendar'] = $tdCalendar ;
		// dump( count($data['tdCalendar'])) ;
		// dump( $data['tdCalendar']) ;

		// 分頁資訊 -> 無用
		$data['pagination'] = "" ;
		$data['indexDec'] = "" ;
		// 準備各位置資訊 ----------------------------------------------------------------------------------------
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('site/scheduling_list', $data));
	}

	/**
	*
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function getForm() {
		$data = $this->preparation() ;
		// dump( $this->request->get) ;
		// dump( $this->request->post) ;

		// $second1 = floor( ( strtotime( $rentInfo['endDateYMD']) - strtotime( $rentInfo['startDateYMD']))) ;
		// $rentInfo['rentDay'] 		= floor( $second1 / 86400) ;	// 24hr * 60分 * 60秒
		// $rentInfo['rentHour'] 		= ( $second1 % 86400) / 3600 ;	//

		// form 表格內的錯誤訊息 ----------------------------------------------------------------------------------
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'] ;
			foreach ( $this->error as $errKey => $tmpStr) {
				if ( $errKey != "warning") $data['error_warning'] .= "<br>{$tmpStr}" ;
			}
		} else {
			$data['error_warning'] = '' ;
		}
		// form 租車資訊 表格內的資料 -----------------------------------------------------------------------
		$car_sn      = isset( $this->request->get['idx']) ? trim( $this->request->get['idx']) : "" ;
		$scheduleIdx = isset( $this->request->get['bookingID']) ? trim( $this->request->get['bookingID']) : "" ;
		$bookingDate = isset( $this->request->get['bookingDate']) ? trim( $this->request->get['bookingDate']) : "" ;
		$url = "&bookingID={$scheduleIdx}&bookingDate={$bookingDate}&idx={$car_sn}" ;
		$data['url_submit'] = $this->url->link('site/scheduling/add', 'token=' . $this->session->data['token'] . $url, true) ;
		$data['url_del']   = $this->url->link('site/scheduling/delete', 'token=' . $this->session->data['token'] . $url, true) ;
		$data['url_del']   = str_replace( "&amp;", "&", $data['url_del']) ;
		$data['url_view']  = $this->url->link('site/scheduling/view', 'token=' . $this->session->data['token'] . $url, true) ;
		$data['url_renew'] = $this->url->link('site/scheduling/renew', 'token=' . $this->session->data['token'], true) ;
		$data['url_renew'] = str_replace( "&amp;", "&", $data['url_renew']) ;

		// 取調度表資訊
		$this->rowInfo = $this->model_site_scheduling->getInformation( $car_sn, $scheduleIdx) ;
		// dump( $this->rowInfo) ;
		// 標題 與相關按鈕
		if( !empty( $this->rowInfo)) {
			$data['entry_title'] = "編輯" ;
			$data['entry_submit_button'] = "確認修改" ;
			$data['entry_delete_button'] = "刪除" ;
			$data['entry_view_button'] = "顯示內容" ;
			$data['orderShow'] = "inline" ;


			if ( !empty( $this->rowInfo['order_no'])) {
				$order_no = $this->rowInfo['order_no'] ;
				$orderInfo = $this->model_sale_order->getOrderInfoByAgreement_no( $order_no) ;
				// dump( $orderInfo) ;

				if ( $orderInfo['order_type'] == 10 || $orderInfo['order_type'] == 130) {
					$minsuInfo = $this->model_site_minshuku->getInformation( $orderInfo['minsu_idx']) ;
				} else {
					$minsuInfo = "" ;
				}
				// dump( $minsuInfo) ;
				$flightInfo = $this->model_site_scheduling->getFlightInfo( $order_no) ;
				// dump( $flightInfo) ;
			} else {
				$order_no = "" ;
			}
			// 計算 共計天時
			$useSecond        = floor( ( strtotime( $this->rowInfo['end_date']) - strtotime( $this->rowInfo['start_date']))) ;
			$data['rentDay']  = floor( $useSecond / 86400) ;	// 24hr * 60分 * 60秒
			$data['rentHour'] = ( $useSecond % 86400) / 3600 ;	//

			$data['url_submit'] = $this->url->link('site/scheduling/edit', 'token=' . $this->session->data['token'] . $url, true) ;
		} else {
			$data['entry_title'] = "新增" ;
			$data['entry_submit_button'] = "確認新增" ;
			$data['orderShow'] = "none" ;
			$order_no = "" ;
			// 計算 共計天時
			$data['rentDay']  = 0 ;	// 24hr * 60分 * 60秒
			$data['rentHour'] = 0 ;	//
		}
		// 收費方式
		if ( isset( $this->request->post['pay_type'])) {
			$data['pay_type'] = $this->request->post['pay_type'] ;
		} else if ( isset( $minsuInfo['idx'])) {
			$data['pay_type'] = $orderInfo['pay_type'] ;
		} else {
			$data['pay_type'] = "" ;
		}

		// 取得車子資訊 車種,車型,車號
		$carInfo = $this->model_site_car_model->getCarInfo( $car_sn) ;
		$data['carInfo'] = $carInfo ;
		// dump( $carInfo) ;

		// 取得訂單選項 訂單類別
		$orderType = $this->model_site_option->getOptionItemsArr( '8') ;
		if( empty( $this->rowInfo) || $this->rowInfo['order_type'] != 9) unset( $orderType[9]) ;
		// dump( $orderType) ;
		$data['orderType'] = $orderType ;

		if ( isset( $this->request->post['sel_order_type'])) {
			$data['sel_order_type'] = $this->request->post['sel_order_type'] ;
		} else if ( isset( $this->rowInfo['order_type'])) {
			$data['sel_order_type'] = $this->rowInfo['order_type'] ;
		} else {
			$data['sel_order_type'] = "" ;
		}

		// 處理民宿欄位
		if ( isset( $this->request->post['input_minsu_idx'])) {
			$data['input_minsu_idx'] = $this->request->post['input_minsu_idx'] ;
		} else if ( isset( $minsuInfo['idx'])) {
			$data['input_minsu_idx'] = $minsuInfo['idx'] ;
		} else {
			$data['input_minsu_idx'] = "" ;
		}

		if ( isset( $this->request->post['input_minsu_name'])) {
			$data['input_minsu_name'] = $this->request->post['input_minsu_name'] ;
		} else if ( isset( $minsuInfo['idx'])) {
			$data['input_minsu_name'] = $minsuInfo['name'] ;
		} else {
			$data['input_minsu_name'] = "" ;
		}

		if ( $data['sel_order_type'] == 10 || $data['sel_order_type'] == 130) {
			$data['minsuShow'] = "inline" ;
		} else {
			$data['minsuShow'] = "none" ;
		}

		// 起迄時間 Start -------------------------------------------------------------------------
		if ( isset( $this->request->post['input_start_date'])) {
			$data['input_start_date'] = $this->request->post['input_start_date'] ;
			$data['selStartTime'] = $this->request->post['input_start_time'] ;
		} else if ( !empty($this->rowInfo)) {
			$iStartDateArr = explode(" ", $this->rowInfo['start_date']) ;
			$data['input_start_date'] = $iStartDateArr[0] ;
			$data['selStartTime'] = substr($iStartDateArr[1], 0, 5) ;
		} else {
			list( $sData, $sTime) = explode(' ', $bookingDate) ;
			$data['input_start_date'] = $sData ;
			$data['selStartTime'] = $sTime ;
		}

		// 起迄時間 End ---------------------------------------------------------------------------
		if ( isset( $this->request->post['input_end_date'])) {
			$data['input_end_date'] = $this->request->post['input_end_date'] ;
			$data['selEndTime'] = $this->request->post['input_end_time'] ;
		} else if ( !empty($this->rowInfo)) {
			$iEndDateArr = explode(" ", $this->rowInfo['end_date']) ;
			$data['input_end_date'] = $iEndDateArr[0] ;
			$data['selEndTime'] = substr($iEndDateArr[1], 0, 5) ;
		} else {
			list( $sData, $sTime) = explode(' ', $bookingDate) ;
			$data['input_end_date'] = $sData ;
			$data['selEndTime'] = $sTime ;
		}
		// $data['selRentTime'] = $this->getRentTime() ; // add by Angus 提供時間下拉選項
		$data['selRentTime'] = $this->model_site_option->getRentTime() ; // add by Angus 提供時間下拉選項 2018.02.08 改寫

		// 取得車站點名稱選項
		$carStations = $this->model_site_option->getOptionItemsAllColArr( '15') ;
		$data['carStations'] = $carStations ;

		if ( isset( $this->request->post['sel_station_start'])) {
			$data['sel_s_stattion'] = $this->request->post['sel_station_start'] ;
		} else if ( isset( $this->rowInfo['s_station'])) {
			$data['sel_s_stattion'] = $this->rowInfo['s_station'] ;
		} else {
			$data['sel_s_stattion'] = "" ;
		}
		if ( isset( $this->request->post['sel_station_end'])) {
			$data['sel_e_stattion'] = $this->request->post['sel_station_end'] ;
		} else if ( isset( $this->rowInfo['e_station'])) {
			$data['sel_e_stattion'] = $this->rowInfo['e_station'] ;
		} else {
			$data['sel_e_stattion'] = "" ;
		}

		// 隱藏欄位 車輛調度表流水號
		$data['scheduleIdx'] = !empty( $scheduleIdx) ? $scheduleIdx : "" ;
		$data['agreement_no'] = $order_no ;

		$data['input_order_no'] = isset( $order_no) ? $order_no : "" ;
		$url = isset( $orderInfo['order_id']) ? "&idx={$orderInfo['order_id']}" : "" ;
		$data['orderUrl'] = $this->url->link('sale/order/edit', 'token=' . $this->session->data['token'] . $url, true) ;

		// 租車費用
		$data['totalRentCost'] = isset( $orderInfo['total']) ? $orderInfo['total'] : "" ;

		// form 客戶資料 表格內的資料 -----------------------------------------------------------------------
		$custInfo = array(
			"input_cust_name"     => isset( $orderInfo['rent_user_name']) ? $orderInfo['rent_user_name'] : "",
			"input_cust_id"       => isset( $orderInfo['rent_user_id']) ? $orderInfo['rent_user_id'] : "",
			"input_cust_born"     => isset( $orderInfo['rent_user_born']) ? $orderInfo['rent_user_born'] : "",
			"input_cust_mobile"   => isset( $orderInfo['rent_user_tel']) ? $orderInfo['rent_user_tel'] : "",
			"input_cust_email"    => isset( $orderInfo['rent_user_mail']) ? $orderInfo['rent_user_mail'] : "",
			"input_take_name"     => isset( $orderInfo['get_user_name']) ? $orderInfo['get_user_name'] : "",
			"input_take_id"       => isset( $orderInfo['get_user_id']) ? $orderInfo['get_user_id'] : "",
			"input_take_born"     => isset( $orderInfo['get_user_born']) ? $orderInfo['get_user_born'] : "",
			"input_take_mobile"   => isset( $orderInfo['get_user_tel']) ? $orderInfo['get_user_tel'] : "",
			"input_company"       => isset( $orderInfo['rent_company_name']) ? $orderInfo['rent_company_name'] : "",
			"input_company_no"    => isset( $orderInfo['rent_company_no']) ? $orderInfo['rent_company_no'] : "",
			"input_urgent_man"    => isset( $orderInfo['urgent_man']) ? $orderInfo['urgent_man'] : "",
			"input_urgent_mobile" => isset( $orderInfo['urgent_mobile']) ? $orderInfo['urgent_mobile'] : "",
			"sel_zip1"            => isset( $orderInfo['rent_zip1']) ? $orderInfo['rent_zip1'] : "",
			"sel_zip2"            => isset( $orderInfo['rent_zip2']) ? $orderInfo['rent_zip2'] : "",
			"input_cust_address"  => isset( $orderInfo['rent_user_address']) ? $orderInfo['rent_user_address'] : "",
			"area_memo"           => isset( $this->rowInfo['memo']) ? $this->rowInfo['memo'] : "",
		) ;

		if (isset( $this->request->post['input_cust_name']))
			$custInfo['input_cust_name'] = $this->request->post['input_cust_name'] ;
		if (isset( $this->request->post['input_cust_id']))
			$custInfo['input_cust_id'] = $this->request->post['input_cust_id'] ;
		if (isset( $this->request->post['input_cust_born']))
			$custInfo['input_cust_born'] = $this->request->post['input_cust_born'] ;
		if (isset( $this->request->post['input_cust_mobile']))
			$custInfo['input_cust_mobile'] = $this->request->post['input_cust_mobile'] ;
		if (isset( $this->request->post['input_take_name']))
			$custInfo['input_take_name'] = $this->request->post['input_take_name'] ;
		if (isset( $this->request->post['input_take_id']))
			$custInfo['input_take_id'] = $this->request->post['input_take_id'] ;
		if (isset( $this->request->post['input_take_born']))
			$custInfo['input_take_born'] = $this->request->post['input_take_born'] ;
		if (isset( $this->request->post['input_take_mobile']))
			$custInfo['input_take_mobile'] = $this->request->post['input_take_mobile'] ;
		if (isset( $this->request->post['input_company']))
			$custInfo['input_company'] = $this->request->post['input_company'] ;
		if (isset( $this->request->post['input_company_no']))
			$custInfo['input_company_no'] = $this->request->post['input_company_no'] ;
		if (isset( $this->request->post['input_urgent_man']))
			$custInfo['input_urgent_man'] = $this->request->post['input_urgent_man'] ;
		if (isset( $this->request->post['input_urgent_mobile']))
			$custInfo['input_urgent_mobile'] = $this->request->post['input_urgent_mobile'] ;
		if (isset( $this->request->post['input_rent_zipcode1']))
			$custInfo['sel_zip1'] = $this->request->post['input_rent_zipcode1'] ;
		if (isset( $this->request->post['input_rent_zipcode2']))
			$custInfo['sel_zip2'] = $this->request->post['input_rent_zipcode2'] ;
		if (isset( $this->request->post['input_cust_address']))
			$custInfo['input_cust_address'] = $this->request->post['input_cust_address'] ;
		if (isset( $this->request->post['area_memo']))
			$custInfo['area_memo'] = $this->request->post['area_memo'] ;

		$data['custInfo'] = $custInfo ;
		// 地址選項
		$zipcodeMainArr = $this->model_site_zip->getZipCode() ;
		$data['retZip'] = $zipcodeMainArr ;
		if ( $data['custInfo']['sel_zip1']) {
			$zipcodeSecondArr = $this->model_site_zip->getZipCode( $data['custInfo']['sel_zip1']) ;
		} else {
			$zipcodeSecondArr = array() ;
		}
		$data['retZip2'] = $zipcodeSecondArr ;
		$data['ajaxUrlZipCode'] = $this->url->link('site/scheduling/getSubZip', 'token=' . $this->session->data['token'], true) ;


		// form 配件資訊 表格內的資料 -----------------------------------------------------------------------
		$accessoriesInfo = array(
			"sel_adult"          => isset( $orderInfo['adult']) ? $orderInfo['adult'] : "",
			"sel_children"       => isset( $orderInfo['children']) ? $orderInfo['children'] : "",
			"sel_baby"           => isset( $orderInfo['baby']) ? $orderInfo['baby'] : "",
			"sel_baby_chair"     => isset( $orderInfo['baby_chair']) ? $orderInfo['baby_chair'] : "",
			"sel_baby_chair1"     => isset( $orderInfo['baby_chair1']) ? $orderInfo['baby_chair1'] : "",
			"sel_baby_chair2"     => isset( $orderInfo['baby_chair2']) ? $orderInfo['baby_chair2'] : "",
			"sel_children_chair" => isset( $orderInfo['children_chair']) ? $orderInfo['children_chair'] : "",
			"sel_gps"            => isset( $orderInfo['gps']) ? $orderInfo['gps'] : "",
			"sel_mobile_moto"    => isset( $orderInfo['mobile_moto']) ? $orderInfo['mobile_moto'] : "",
		) ;
		if (isset( $this->request->post['sel_adult']))
			$custInfo['sel_adult'] = $this->request->post['sel_adult'] ;
		if (isset( $this->request->post['sel_children']))
			$custInfo['sel_children'] = $this->request->post['sel_children'] ;
		if (isset( $this->request->post['sel_baby']))
			$custInfo['sel_baby'] = $this->request->post['sel_baby'] ;
		if (isset( $this->request->post['sel_baby_chair']))
			$custInfo['sel_baby_chair'] = $this->request->post['sel_baby_chair'] ;
		if (isset( $this->request->post['sel_baby_chair1']))
			$custInfo['sel_baby_chair1'] = $this->request->post['sel_baby_chair1'] ;
		if (isset( $this->request->post['sel_baby_chair2']))
			$custInfo['sel_baby_chair2'] = $this->request->post['sel_baby_chair2'] ;
		if (isset( $this->request->post['sel_children_chair']))
			$custInfo['sel_children_chair'] = $this->request->post['sel_children_chair'] ;
		if (isset( $this->request->post['sel_gps']))
			$custInfo['sel_gps'] = $this->request->post['sel_gps'] ;
		if (isset( $this->request->post['sel_mobile_moto']))
			$custInfo['sel_mobile_moto'] = $this->request->post['sel_mobile_moto'] ;
		$data['accessoriesInfo'] = $accessoriesInfo ;


		// form 航班資訊 表格內的資料 -----------------------------------------------------------------------
		// 取得航空公司名稱選項
		$airlineArr = $this->model_site_option->getOptionItemsAllColArr( '125') ;
		$data['airlineArr'] = $airlineArr ;
		$fromAirportArr = $this->model_site_option->getOptionItemsAllColArr( '134') ;
		$data['airport'] = $fromAirportArr ;

		for($i=0; $i<2; $i++){
			$rt = "";
			if($i==1){
				$rt = "_rt";
			}
			// 處理航船顯示欄位
			if ( isset( $this->request->post['sel_trans_type'.$rt])) {
				$data['sel_trans_type'.$rt] = $this->request->post['sel_trans_type'.$rt] ;
			} else if ( isset( $flightInfo['trans_type'.$rt])) {
				$data['sel_trans_type'.$rt] = $flightInfo['trans_type'.$rt] ;
			} else {
				$data['sel_trans_type'.$rt] = "" ;
			}

			if ( $data['sel_trans_type'.$rt] == 1) {
				$data['airlineShow'.$rt] = "inline" ;
				$data['flightnoShow'.$rt] = "inline" ;
				$data['pierShow'.$rt] = "none" ;
			} else if ( $data['sel_trans_type'.$rt] == 2) {
				$data['airlineShow'.$rt] = "none" ;
				$data['flightnoShow'.$rt] = "none" ;
				$data['pierShow'.$rt] = "inline" ;
			} else {
				$data['airlineShow'.$rt] = "none" ;
				$data['flightnoShow'.$rt] = "none" ;
				$data['pierShow'.$rt] = "none" ;
			}

			if ( isset( $this->request->post['sel_airline'.$rt])) {
				$data['sel_airline'.$rt] = $this->request->post['sel_airline'.$rt] ;
			} else if ( isset( $flightInfo['airline'.$rt])) {
				$data['sel_airline'.$rt] = $flightInfo['airline'.$rt] ;
			} else {
				$data['sel_airline'.$rt] = "" ;
			}
			if ( isset( $this->request->post['sel_airport'])) {
				$data['sel_airport'.$rt] = $this->request->post['sel_airport'.$rt] ;
			} else if ( isset( $flightInfo['airport'.$rt])) {
				$data['sel_airport'.$rt] = $flightInfo['airport'.$rt] ;
			} else {
				$data['sel_airport'.$rt] = "" ;
			}


			// 取得碼頭出發地名稱選項
			$pierArr = $this->model_site_option->getOptionItemsAllColArr( '131') ;
			$data['pierArr'] = $pierArr ;

			if ( isset( $this->request->post['sel_pier'.$rt])) {
				$data['sel_pier'.$rt] = $this->request->post['sel_pier'.$rt] ;
			} else if ( isset( $flightInfo['pier'.$rt])) {
				$data['sel_pier'.$rt] = $flightInfo['pier'.$rt] ;
			} else {
				$data['sel_pier'.$rt] = "" ;
			}

			if ( isset( $this->request->post['input_flight_no'.$rt])) {
				$data['input_flight_no'.$rt] = $this->request->post['input_flight_no'.$rt] ;
			} else if ( isset( $flightInfo['flight_no'.$rt])) {
				$data['input_flight_no'.$rt] = $flightInfo['flight_no'.$rt] ;
			} else {
				$data['input_flight_no'.$rt] = "" ;
			}

			if ( isset( $flightInfo['s_date'.$rt])) {
				list( $s_date, $s_time) = explode(" ", $flightInfo['s_date'.$rt]) ;
				list( $s_hour, $s_minute, $s_sec) = explode(":", $s_time) ;
			}
			if ( isset( $flightInfo['e_date'])) {
				list( $e_date, $e_time) = explode(" ", $flightInfo['e_date']) ;
				list( $e_hour, $e_minute, $e_sec) = explode(":", $e_time) ;
			}

			// 預定起飛時間
			if ( isset( $this->request->post['input_airline_sdate'.$rt])) {
				$data['input_airline_sdate'.$rt] = $this->request->post['input_airline_sdate'.$rt] ;
			} else if ( isset( $flightInfo['flight_no'.$rt])) {
				$data['input_airline_sdate'.$rt] = isset( $s_date) ? $s_date : "" ;
			} else {
				$data['input_airline_sdate'.$rt] = "" ;
			}
			if ( isset( $this->request->post['sel_airline_shour'.$rt])) {
				$data['sel_airline_shour'.$rt] = $this->request->post['sel_airline_shour'.$rt] ;
			} else if ( isset( $flightInfo['flight_no'.$rt])) {
				$data['sel_airline_shour'.$rt] = isset( $s_hour) ? $s_hour : "" ;
			} else {
				$data['sel_airline_shour'.$rt] = "" ;
			}
			if ( isset( $this->request->post['sel_airline_sminute'.$rt])) {
				$data['sel_airline_sminute'.$rt] = $this->request->post['sel_airline_sminute'.$rt] ;
			} else if ( isset( $flightInfo['flight_no'.$rt])) {
				$data['sel_airline_sminute'.$rt] = isset( $s_minute) ? $s_minute : "" ;
			} else {
				$data['sel_airline_sminute'.$rt] = "" ;
			}

			// 預定到達時間
			if ( isset( $this->request->post['input_airline_edate'.$rt])) {
				$data['input_airline_edate'.$rt] = $this->request->post['input_airline_edate'.$rt] ;
			} else if ( isset( $flightInfo['flight_no'.$rt])) {
				$data['input_airline_edate'.$rt] = isset( $e_date) ? $e_date : "" ;
			} else {
				$data['input_airline_edate'.$rt] = "" ;
			}
			if ( isset( $this->request->post['sel_airline_ehour'.$rt])) {
				$data['sel_airline_ehour'.$rt] = $this->request->post['sel_airline_ehour'.$rt] ;
			} else if ( isset( $flightInfo['flight_no'.$rt])) {
				$data['sel_airline_ehour'.$rt] = isset( $e_hour) ? $e_hour : "" ;
			} else {
				$data['sel_airline_ehour'.$rt] = "" ;
			}
			if ( isset( $this->request->post['sel_airline_eminute'.$rt])) {
				$data['sel_airline_eminute'.$rt] = $this->request->post['sel_airline_eminute'.$rt] ;
			} else if ( isset( $flightInfo['flight_no'.$rt])) {
				$data['sel_airline_eminute'.$rt] = isset( $e_minute) ? $e_minute : "" ;
			} else {
				$data['sel_airline_eminute'.$rt] = "" ;
			}
		}

		$this->response->setOutput($this->load->view('site/scheduling_form', $data)) ;
		// $this->response->setOutput($this->load->view('site/scheduling_form_2018_01_11', $data)) ;
     }

	/**
	*
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function validateForm() {
		// 檢查是否有權限
		if (!$this->user->hasPermission('modify', 'site/scheduling')) {
			$this->error['warning'] = $this->language->get('error_permission') ;
		} else {
			// 檢查booking的時間 ===========================================================================================
			$postInfo = $this->request->post ;
			$car_idx     = $postInfo['useCarIdx'] ;
			$filter_data['sDayStr']     = $postInfo['input_start_date'] . " " . $postInfo['input_start_time'] ;
			$filter_data['eDayStr']     = $postInfo['input_end_date'] . " " . $postInfo['input_end_time'] ;
			$filter_data['scheduleIdx'] = isset( $postInfo['scheduleIdx']) ? $postInfo['scheduleIdx'] : "" ;

			$retChk      = $this->model_site_scheduling->checkScheduling( $car_idx, $filter_data) ;
			if ( $retChk) {
				$this->error['rentDateError'] = "使用日期有誤 請查明後再執行" ;
			}
			// 檢查取還車地點 ================================================================================================
			// 考慮全部都加上取還車地點
			// 增加 qrcode訂車、前台業務訂車 取還車必填
			switch ( $postInfo['sel_order_type']) {
				case '9':  // 網路訂單
				case '10': // 民宿(業者)
				case '11': // 長租
				case '140': // QRCode訂車
				case '146': // 前台業務訂車
				case '147': // 前台QRCode訂車

					if ( empty($postInfo['sel_station_start'])) {
						$this->error['err_station_start'] = "取車地點未選擇" ;
					}
					if ( empty($postInfo['sel_station_end'])) {
						$this->error['err_station_end'] = "還車地點未選擇" ;
					}
					break;

				case '123': // 保養車輛
				case '124': // 自行鎖車
					if ( empty($postInfo['sel_station_start'])) {
						$this->error['err_station_start'] = "取車地點未選擇" ;
					}
					break;
			}
			// 檢查民宿訂單 =================================================================================================
			$this->load->model('site/minshuku') ;
			if ( $postInfo['sel_order_type'] == '10') {
				// 檢查民宿名稱是否存在
				if ( isset( $postInfo['input_minsu_name']) && $postInfo['input_minsu_name'] != '') {
					$filter['name'] = $postInfo['input_minsu_name'] ;
					$retChk = $this->model_site_minshuku->checkMinsuName( $filter) ;
					if ( !$retChk) {
						$this->error['minsuError'] = "民宿資料未建立" ;
					}
				} else {
					$this->error['minsuError'] = "民宿名稱不可以為空白" ;
				}
			}
			// 檢查客戶資料必填內容 =============================================================================================
			if ( $postInfo['sel_order_type'] == '10' || $postInfo['sel_order_type'] == '11') {
				// if ( empty( $postInfo['input_cust_name'])) $this->error['custNameError'] = "承租人姓名未填寫" ;
				if ( empty( $postInfo['input_take_name'])) $this->error['takeNameError'] = "取車人姓名未填寫" ;
				// if ( empty( $postInfo['input_cust_id']))   $this->error['custIdrror']    = "承租人 身分證字號／護照號碼 未填寫" ;
				if ( empty( $postInfo['input_take_id']))   $this->error['takeIdError']   = "取車人 身分證字號／護照號碼 未填寫" ;
			}
			// 檢查客戶資料必填內容 =============================================================================================
			if ( $postInfo['sel_order_type'] == '10') {
				// if ( empty( $postInfo['sel_airline'])) $this->error['airlineError']        = "航空公司未選擇" ;
				// if ( empty( $postInfo['input_flight_no'])) $this->error['flightNoError'] = "班機編號未填寫" ;
				// if ( empty( $postInfo['input_airline_sdate'])) $this->error['airlineSdateError'] = "預定起飛日期未選擇" ;
				// if ( empty( $postInfo['input_airline_edate'])) $this->error['airlineEdateError'] = "預定到達日期未選擇" ;
			}
			// dump( $this->request->post) ;
			// exit() ;
		}

		// dump( $this->error) ;
		// exit() ;
		// dump( $this->request->post) ;
		// 檢查各項必填欄位 -----------------------------------------------------------------------------------------
		// 標題
		// if ((utf8_strlen($this->request->post['input_title']) < 3) || (utf8_strlen($this->request->post['input_title']) > 64)) {
		// 	$this->error['title'] = $this->language->get('error_title');
		// }
		// // 頁面內容
		// if (utf8_strlen($this->request->post['area_description']) < 3) {
		// 	$this->error['description'] = $this->language->get('error_description');
		// }

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		return !$this->error;
	}

	/**
	*
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'site/scheduling')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$ret = $this->model_site_scheduling->getOrderTypeForDelete( $this->request->get['bookingID'], $this->request->get['idx']) ;
		// dump( $ret) ;
		// 2018.04.39 不檢查這裡了
		// if ( $ret['order_type'] == 123 || $ret['order_type'] == 124) {
		// } else if ( $ret['order_type'] == 10 || $ret['order_type'] == 11) {
		// } else {
		// 	$this->error['warning'] = "該班表行程為網路訂單 無法刪除" ;
		// }

		if ( !empty( $ret['order_no'])) {
			$ret2 = $this->model_site_scheduling->getOrderOtherCar( $ret['order_no']) ;
			// dump($ret2) ;
			if ( count( unserialize( $ret2['other_car'])) > 1)
				$this->error['warning'] = "該車輛所屬訂單有多輛車 無法刪除<br>請至訂單管理進行作業" ;
		}

		// foreach ($this->request->post['selected'] as $information_id) {
		// 	if ($this->config->get('config_account_id') == $information_id) {
		// 		$this->error['warning'] = $this->language->get('error_account');
		// 	}

		// 	if ($this->config->get('config_checkout_id') == $information_id) {
		// 		$this->error['warning'] = $this->language->get('error_checkout');
		// 	}

		// 	if ($this->config->get('config_affiliate_id') == $information_id) {
		// 		$this->error['warning'] = $this->language->get('error_affiliate');
		// 	}

		// 	if ($this->config->get('config_return_id') == $information_id) {
		// 		$this->error['warning'] = $this->language->get('error_return');
		// 	}

		// 	$store_total = $this->model_setting_store->getTotalStoresByInformationId($information_id);

		// 	if ($store_total) {
		// 		$this->error['warning'] = sprintf($this->language->get('error_store'), $store_total);
		// 	}
		// }
		return !$this->error;
	}

	/**
	* 前置作業
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function preparation() {
		// 標題
		$data['heading_title'] = $this->language->get('heading_title') ;
		// 次標題
		$data['text_list'] = $this->language->get('text_list') ;
		$data['text_form'] = !isset($this->request->get['idx']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_confirm'] = $this->language->get('text_confirm') ;
		$data['text_enabled'] = $this->language->get('text_enabled') ;
		$data['text_disabled'] = $this->language->get('text_disabled') ;
		// 麵包屑
		$url = '';
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
			);
		$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('site/scheduling', 'token=' . $this->session->data['token'] . $url, 'SSL')
			);
		$data['token'] = $this->session->data['token'] ;

		// 列表頁 按鈕
		$data['url_add'] = $this->url->link('site/scheduling/add', 'token=' . $this->session->data['token'] . $url, 'SSL') ;
		$data['url_delete'] = $this->url->link('site/scheduling/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['url_cancel'] = $this->url->link('site/scheduling', 'token=' . $this->session->data['token'] . $url, true);
		if (!isset($this->request->get['idx'])) {
			$data['url_action'] = $this->url->link('site/scheduling/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['url_action'] = $this->url->link('site/scheduling/edit', 'token=' . $this->session->data['token'] . '&idx=' . $this->request->get['idx'] . $url, true);
		}

		$data['button_add'] = $this->language->get('text_add');
		$data['button_edit'] = $this->language->get('text_edit');
		$data['button_delete'] = $this->language->get('text_delete');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['tab_general'] = $this->language->get('tab_general');

		// 提示訊息
		$data['error_warning'] = '';
		$data['success'] = '';
		$data['cssVer'] = time() ;
		return $data ;
	}

	/**
	 * 組合新增修改的資訊
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-01-06
	 */
	protected function buildingData() {
		$rentInfo = $this->request->post ;
		// dump( $rentInfo) ;

		$insData['agreement_no']      = (isset( $rentInfo['agreement_no']) && !empty($rentInfo['agreement_no'])) ? $rentInfo['agreement_no'] : $this->newAgreementNo() ;
		$insData['scheduleIdx']       = isset( $rentInfo['scheduleIdx']) ? $rentInfo['scheduleIdx'] : "" ;
		$insData['order_type']        = isset( $rentInfo['sel_order_type']) ? $rentInfo['sel_order_type'] : "" ;
		$insData['car_model']         = isset( $rentInfo['useCarSeed']) ? $rentInfo['useCarSeed'] : "" ;
		$insData['car_sn']            = isset( $rentInfo['useCarIdx']) ? $rentInfo['useCarIdx'] : "" ;
		$insData['car_no']            = isset( $rentInfo['useCarNo']) ? $rentInfo['useCarNo'] : "" ;
		$insData['s_station']         = isset( $rentInfo['sel_station_start']) ? $rentInfo['sel_station_start'] : "" ;
		$insData['e_station']         = isset( $rentInfo['sel_station_end']) ? $rentInfo['sel_station_end'] : "" ;
		$insData['rent_date_out']     = $rentInfo['input_start_date'] . " " . $rentInfo['input_start_time'] ;
		$insData['rent_date_in_plan'] = $rentInfo['input_end_date'] . " " . $rentInfo['input_end_time'] ;
		$insData['rent_user_name']    = isset( $rentInfo['input_cust_name']) ? $rentInfo['input_cust_name'] : "" ;
		$insData['rent_user_id']      = isset( $rentInfo['input_cust_id']) ? strtoupper( $rentInfo['input_cust_id']) : "" ;
		$insData['rent_user_born']    = isset( $rentInfo['input_cust_born']) ? $rentInfo['input_cust_born'] : "" ;
		$insData['rent_user_tel']     = isset( $rentInfo['input_cust_mobile']) ? $rentInfo['input_cust_mobile'] : "" ;
		$insData['rent_user_mail']    = isset( $rentInfo['input_cust_email']) ? $rentInfo['input_cust_email'] : "" ;
		$insData['get_user_name']     = isset( $rentInfo['input_take_name']) ? $rentInfo['input_take_name'] : "" ;
		$insData['get_user_id']       = isset( $rentInfo['input_take_id']) ? $rentInfo['input_take_id'] : "" ;
		$insData['get_user_born']     = isset( $rentInfo['input_take_born']) ? $rentInfo['input_take_born'] : "" ;
		$insData['get_user_tel']      = isset( $rentInfo['input_take_mobile']) ? $rentInfo['input_take_mobile'] : "" ;
		$insData['rent_zip1']         = isset( $rentInfo['input_rent_zipcode1']) ? $rentInfo['input_rent_zipcode1'] : "" ;
		$insData['rent_zip2']         = isset( $rentInfo['input_rent_zipcode2']) ? $rentInfo['input_rent_zipcode2'] : "" ;
		$insData['rent_user_address'] = isset( $rentInfo['input_cust_address']) ? $rentInfo['input_cust_address'] : "" ;
		$insData['rent_company_name'] = isset( $rentInfo['input_company']) ? $rentInfo['input_company'] : "" ;
		$insData['rent_company_no']   = isset( $rentInfo['input_company_no']) ? $rentInfo['input_company_no'] : "" ;
		$insData['urgent_man']        = isset( $rentInfo['input_urgent_man']) ? $rentInfo['input_urgent_man'] : "" ;
		$insData['urgent_mobile']     = isset( $rentInfo['input_urgent_mobile']) ? $rentInfo['input_urgent_mobile'] : "" ;
		$insData['minsu_idx']         = isset( $rentInfo['input_minsu_idx']) ? $rentInfo['input_minsu_idx'] : "" ;
		$insData['pay_type']          = isset( $rentInfo['pay_type']) ? $rentInfo['pay_type'] : "" ;

		$insData['adult']             = isset( $rentInfo['sel_adult']) ? $rentInfo['sel_adult'] : "" ;
		$insData['children']          = isset( $rentInfo['sel_children']) ? $rentInfo['sel_children'] : "" ;
		$insData['baby']              = isset( $rentInfo['sel_baby']) ? $rentInfo['sel_baby'] : "" ;

		$insData['baby_chair']        = isset( $rentInfo['sel_baby_chair']) ? $rentInfo['sel_baby_chair'] : "" ;
		$insData['baby_chair1']       = isset( $rentInfo['sel_baby_chair1']) ? $rentInfo['sel_baby_chair1'] : "" ;
		$insData['baby_chair2']       = isset( $rentInfo['sel_baby_chair2']) ? $rentInfo['sel_baby_chair2'] : "" ;
		$insData['children_chair']    = isset( $rentInfo['sel_children_chair']) ? $rentInfo['sel_children_chair'] : "" ;
		$insData['gps']               = isset( $rentInfo['sel_gps']) ? $rentInfo['sel_gps'] : "" ;
		$insData['mobile_moto']       = isset( $rentInfo['sel_mobile_moto']) ? $rentInfo['sel_mobile_moto'] : "" ;
		$insData['total']             = isset( $rentInfo['input_total']) ? $rentInfo['input_total'] : "" ;

		$this->load->model('site/cars') ;
		$carsArr = $this->model_site_cars->getCarNoForList( array( isset( $rentInfo['useCarIdx']) ? $rentInfo['useCarIdx'] : "")) ;
		$insData['other_car']         = serialize( $carsArr) ;
		// scheduling 所需的
		$insData['input_cust_name']   = $insData['rent_user_name'] ;
		$insData['sel_order_type']    = $insData['order_type'] ;

		$insData['input_start_date']  = $rentInfo['input_start_date'] ;
		$insData['input_end_date']    = $rentInfo['input_end_date'] ;
		$insData['input_start_time']  = $rentInfo['input_start_time'] ;
		$insData['input_end_time']    = $rentInfo['input_end_time'] ;

		$insData['useCarSeed']        = $insData['car_model'] ;
		$insData['useCarIdx']         = $insData['car_sn'] ;
		$insData['input_cust_tel']    = $insData['rent_user_tel'] ;
		$insData['input_take_name']   = $insData['get_user_name'] ;
		$insData['input_take_mobile'] = $insData['get_user_tel'] ;
		$insData['area_memo']         = isset( $rentInfo['area_memo']) ? $rentInfo['area_memo'] : "" ;

		// tb_flight 所需的
		$insData['trans_type'] =  isset( $rentInfo['sel_trans_type']) ? $rentInfo['sel_trans_type'] : "" ;
		$insData['airport']    =  isset( $rentInfo['sel_airport']) ? $rentInfo['sel_airport'] : "" ;
		$insData['airline']    =  isset( $rentInfo['sel_airline']) ? $rentInfo['sel_airline'] : "" ;
		$insData['pier']       =  isset( $rentInfo['sel_pier']) ? $rentInfo['sel_pier'] : "" ;
		$insData['flight_no']  =  isset( $rentInfo['input_flight_no']) ? $rentInfo['input_flight_no'] : "" ;

		$insData['fs_date']    =  isset( $rentInfo['input_airline_sdate']) ? "{$rentInfo['input_airline_sdate']} {$rentInfo['sel_airline_shour']}:{$rentInfo['sel_airline_sminute']}" : "" ;
		$insData['fe_date']    =  isset( $rentInfo['input_airline_edate']) ? "{$rentInfo['input_airline_edate']} {$rentInfo['sel_airline_ehour']}:{$rentInfo['sel_airline_eminute']}" : "" ;
		
		$insData['trans_type_rt'] =  isset( $rentInfo['sel_trans_type_rt']) ? $rentInfo['sel_trans_type_rt'] : "" ;
		$insData['airport_rt']    =  isset( $rentInfo['sel_airport_rt']) ? $rentInfo['sel_airport_rt'] : "" ;
		$insData['airline_rt']    =  isset( $rentInfo['sel_airline_rt']) ? $rentInfo['sel_airline_rt'] : "" ;
		$insData['pier_rt']       =  isset( $rentInfo['sel_pier_rt']) ? $rentInfo['sel_pier_rt'] : "" ;
		$insData['flight_no_rt']  =  isset( $rentInfo['input_flight_no_rt']) ? $rentInfo['input_flight_no_rt'] : "" ;

		$insData['fs_date_rt']    =  isset( $rentInfo['input_airline_sdate_rt']) ? "{$rentInfo['input_airline_sdate_rt']} {$rentInfo['sel_airline_shour_rt']}:{$rentInfo['sel_airline_sminute_rt']}" : "" ;

		return $insData ;
	}

	/**
	 * [genTipDesc description]
	 * @param   array      $scheduleNode   [description]
	 * @param   array      $fromAirportArr [description]
	 * @return  [type]                     [description]
	 * @Another Angus
	 * @date    2017-12-31
	 */
	protected function generateTipDesc ( $scheduleNode = array(), $fromAirportArr = array()) {
		// dump( $scheduleNode) ;
		$tmpStr = "" ;
		$sStation = "";

		if($scheduleNode['s_station'] != "0")
			$sStation = $this->carStation[$scheduleNode['s_station']] ;
		$eStation = "" ;
		if($scheduleNode['e_station'] != "0")
			$eStation = $this->carStation[$scheduleNode['e_station']] ;
		$sTime    = substr( $scheduleNode['start_date'], 0, 16) ;
		$eTime    = substr( $scheduleNode['end_date'], 0, 16) ;

		$tmpHeader = "";
		$tmpOther = "";
		if(!isset($scheduleNode['orderInfo'][0]['baby_chair']))
			$scheduleNode['orderInfo'][0]['baby_chair'] = 0;
		if(!isset($scheduleNode['orderInfo'][0]['baby_chair1']))
			$scheduleNode['orderInfo'][0]['baby_chair1'] = 0;
		if(!isset($scheduleNode['orderInfo'][0]['baby_chair2']))
			$scheduleNode['orderInfo'][0]['baby_chair2'] = 0;

		if(!isset($scheduleNode['orderInfo'][0]['children_chair']))
			$scheduleNode['orderInfo'][0]['children_chair'] = 0;
		if(!isset($scheduleNode['orderInfo'][0]['gps']))
			$scheduleNode['orderInfo'][0]['gps'] = 0;
		if(!isset($scheduleNode['orderInfo'][0]['mobile_moto']))
			$scheduleNode['orderInfo'][0]['mobile_moto'] = 0;

		if($scheduleNode['orderInfo'][0]['baby_chair'] >= 1 ||
			$scheduleNode['orderInfo'][0]['baby_chair1'] >= 1 ||
			$scheduleNode['orderInfo'][0]['baby_chair2'] >= 1 ||

			$scheduleNode['orderInfo'][0]['children_chair'] >= 1 ||
			$scheduleNode['orderInfo'][0]['gps'] >= 1 ||
			$scheduleNode['orderInfo'][0]['mobile_moto'] >= 1 ) {

			$tmpHeader = "　　　　配件 : " ;
			if($scheduleNode['orderInfo'][0]['baby_chair'] >= 1) {
				$tmpOther .= "嬰兒安全座椅 x {$scheduleNode['orderInfo'][0]['baby_chair']}" ;
			}
			if($scheduleNode['orderInfo'][0]['baby_chair1'] >= 1) {
				if($tmpOther != "")
					$tmpOther .= "<BR>　　　　　　&nbsp;&nbsp;&nbsp;";
				$tmpOther .= "兒童安全座椅 x {$scheduleNode['orderInfo'][0]['baby_chair1']}" ;
			}
			if($scheduleNode['orderInfo'][0]['baby_chair2'] >= 1) {
				if($tmpOther != "")
					$tmpOther .= "<BR>　　　　　　&nbsp;&nbsp;&nbsp;";
				$tmpOther .= "兒童增高墊 x {$scheduleNode['orderInfo'][0]['baby_chair2']}" ;
			}
			if($scheduleNode['orderInfo'][0]['children_chair'] >= 1) {
				if($tmpOther != "")
					$tmpOther .= "<BR>　　　　　　&nbsp;&nbsp;&nbsp;";
				$tmpOther .= "嬰兒推車 x {$scheduleNode['orderInfo'][0]['children_chair']}" ;
			}
			if($scheduleNode['orderInfo'][0]['gps'] >= 1) {
				if($tmpOther != "")
					$tmpOther .= "<BR>　　　　　　&nbsp;&nbsp;&nbsp;";
				$tmpOther .= "GPS x {$scheduleNode['orderInfo'][0]['gps']}" ;
			}
			if($scheduleNode['orderInfo'][0]['mobile_moto'] >= 1) {
				if($tmpOther != "")
					$tmpOther .= "<BR>　　　　　　&nbsp;&nbsp;&nbsp;";
				$tmpOther .= "機車導航架 x {$scheduleNode['orderInfo'][0]['mobile_moto']}" ;
			}
		}

		switch ( $scheduleNode['order_type']) {
			case '9':		// 官網訂單
				$tmpCarName  = "<br>＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝<br>";
				$tmpCarName .= "　　訂單編號 : {$scheduleNode['order_no']}<br>";
				$tmpStr  = "　　　【訂單類別】官網訂單" . $tmpCarName ;
				$tmpStr .= "　　取車地點 : {$sStation}<br>" ;
				$tmpStr .= "預計取車時間 : {$sTime}<br>" ;
				$tmpStr .= "　　還車地點 : {$eStation}<br>" ;
				$tmpStr .= "預計還車時間 : {$eTime}<br>" ;
				$tmpStr .= "　訂車人姓名 : {$scheduleNode['cust_name']}<br>" ;
				$tmpStr .= "　訂車人電話 : {$scheduleNode['cust_tel']}<br>" ;
				$tmpStr .= "　取車人姓名 : {$scheduleNode['take_name']}<br>" ;
				$tmpStr .= "　取車人電話 : {$scheduleNode['take_mobile']}<br>" ;
				$tmpStr .= $tmpHeader.$tmpOther;
				break;
			case '10':		// 民宿訂單
				$tmpCarName  = "<br>＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝<br>";
				$tmpCarName .= "　　訂單編號 : {$scheduleNode['order_no']}<br>";
				$tmpStr  = "　　　【訂單類別】民宿　". $tmpCarName ;
				$tmpStr .= "　　取車地點 : {$sStation}<br>" ;
				$tmpStr .= "預計取車時間 : {$sTime}<br>" ;
				$tmpStr .= "　　還車地點 : {$eStation}<br>" ;
				$tmpStr .= "預計還車時間 : {$eTime}<br>" ;
				$tmpStr .= "　訂車人姓名 : {$scheduleNode['cust_name']}<br>" ;
				$tmpStr .= "　訂車人電話 : {$scheduleNode['cust_tel']}<br>" ;
				$tmpStr .= "　取車人姓名 : {$scheduleNode['take_name']}<br>" ;
				$tmpStr .= "　取車人電話 : {$scheduleNode['take_mobile']}<br>" ;
				if ($scheduleNode['trans_type'] == '1') {
					// $tmpStr .= "　　航空公司 : {$scheduleNode['airline_name']}<br>" ;
					// $tmpStr .= "　　班機編號 : {$scheduleNode['flight_no']}<br>" ;
					$airportStr = ( !empty($scheduleNode['airport'])) ? $fromAirportArr[$scheduleNode['airport']] : "" ;
					$tmpStr .= "　　出發航站 : {$airportStr}<br>" ;
					$tmpStr .= "預定起飛時間 : {$scheduleNode['s_date']}<br>" ;
				}
				else if ($scheduleNode['trans_type'] == '2') {
					// $tmpStr .= "　碼頭出發地 : {$scheduleNode['airline_name']}<br>" ;
					$tmpStr .= "預定出發時間 : {$scheduleNode['s_date']}<br>" ;
				}
				$tmpStr .= $tmpHeader.$tmpOther;
				break;
			case '11':		// 長租訂單
				$tmpCarName  = "<br>＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝<br>";
				$tmpCarName .= "　　訂單編號 : {$scheduleNode['order_no']}<br>";
				$tmpStr  = "　　　　【訂單類別】長租　". $tmpCarName ;
				$tmpStr .= "　　取車地點 : {$sStation}<br>" ;
				$tmpStr .= "預計取車時間 : {$sTime}<br>" ;
				$tmpStr .= "　　還車地點 : {$eStation}<br>" ;
				$tmpStr .= "預計還車時間 : {$eTime}<br>" ;
				$tmpStr .= "　訂車人姓名 : {$scheduleNode['cust_name']}<br>" ;
				$tmpStr .= "　訂車人電話 : {$scheduleNode['cust_tel']}<br>" ;
				$tmpStr .= "　取車人姓名 : {$scheduleNode['take_name']}<br>" ;
				$tmpStr .= "　取車人電話 : {$scheduleNode['take_mobile']}<br>" ;
				$tmpStr .= $tmpHeader.$tmpOther;
				break;
			case '123':		// 保養車輛
				$tmpCarName  = "<br>＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝<br>";
				$tmpStr  = "　　【訂單類別】保養車輛". $tmpCarName ;
				$tmpStr .= "　　保養地點 : {$sStation}<br>" ;
				$tmpStr .= "保養進廠時間 : {$sTime}<br>" ;
				$tmpStr .= "預計完成時間 : {$eTime}<br>" ;
				$tmpStr .= "建立人 : {$scheduleNode['reg_user']}<br>" ;
				break ;
			case '124':		// 自行鎖車
				$tmpCarName  = "<br>＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝<br>";
				$tmpStr  = "　　【訂單類別】自行鎖車". $tmpCarName ;
				$tmpStr .= "　　鎖車地點 : {$sStation}<br>" ;
				$tmpStr .= "鎖車開始時間 : {$sTime}<br>" ;
				$tmpStr .= "鎖車結束時間 : {$eTime}<br>" ;
				$tmpStr .= "建立人 : {$scheduleNode['reg_user']}<br>" ;
				break;
			case '129':		// 預約未完成
				$tmpCarName  = "<br>＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝<br>";
				$tmpStr  = "　　【訂單類別】預約未完成". $tmpCarName ;
				$tmpStr .= "　　取車地點 : {$sStation}<br>" ;
				$tmpStr .= "預計取車時間 : {$sTime}<br>" ;
				$tmpStr .= "　　還車地點 : {$eStation}<br>" ;
				$tmpStr .= "預計還車時間 : {$eTime}<br>" ;
				$tmpStr .= "　訂車人姓名 : {$scheduleNode['cust_name']}<br>" ;
				$tmpStr .= "　訂車人電話 : {$scheduleNode['cust_tel']}<br>" ;
				$tmpStr .= "　取車人姓名 : {$scheduleNode['take_name']}<br>" ;
				$tmpStr .= "　取車人電話 : {$scheduleNode['take_mobile']}<br>" ;
				$tmpStr .= "　　航空公司 : {$scheduleNode['airline_name']}<br>" ;
				$tmpStr .= "　　班機編號 : {$scheduleNode['flight_no']}<br>" ;
				$tmpStr .= "預定起飛時間 : {$scheduleNode['s_date']}<br>" ;
				break;
			case '130':		// 應收
				$tmpCarName  = "<br>＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝<br>";
				$tmpStr  = "　　　【訂單類別】應收". $tmpCarName ;
				$tmpStr .= "　　取車地點 : {$sStation}<br>" ;
				$tmpStr .= "預計取車時間 : {$sTime}<br>" ;
				$tmpStr .= "　　還車地點 : {$eStation}<br>" ;
				$tmpStr .= "預計還車時間 : {$eTime}<br>" ;
				$tmpStr .= "　訂車人姓名 : {$scheduleNode['cust_name']}<br>" ;
				$tmpStr .= "　訂車人電話 : {$scheduleNode['cust_tel']}<br>" ;
				$tmpStr .= "　取車人姓名 : {$scheduleNode['take_name']}<br>" ;
				$tmpStr .= "　取車人電話 : {$scheduleNode['take_mobile']}<br>" ;
				$tmpStr .= "　　航空公司 : {$scheduleNode['airline_name']}<br>" ;
				$tmpStr .= "　　班機編號 : {$scheduleNode['flight_no']}<br>" ;
				$tmpStr .= "預定起飛時間 : {$scheduleNode['s_date']}<br>" ;
				break;

			case '140':		// qrcode
				$tmpCarName  = "<br>＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝<br>";
				$tmpStr  = "　　　【訂單類別】QRCode". $tmpCarName ;
				$tmpStr .= "　　取車地點 : {$sStation}<br>" ;
				$tmpStr .= "預計取車時間 : {$sTime}<br>" ;
				$tmpStr .= "　　還車地點 : {$eStation}<br>" ;
				$tmpStr .= "預計還車時間 : {$eTime}<br>" ;
				$tmpStr .= "　訂車人姓名 : {$scheduleNode['cust_name']}<br>" ;
				$tmpStr .= "　訂車人電話 : {$scheduleNode['cust_tel']}<br>" ;
				$tmpStr .= "　取車人姓名 : {$scheduleNode['take_name']}<br>" ;
				$tmpStr .= "　取車人電話 : {$scheduleNode['take_mobile']}<br>" ;
				$tmpStr .= "　　航空公司 : {$scheduleNode['airline_name']}<br>" ;
				$tmpStr .= "　　班機編號 : {$scheduleNode['flight_no']}<br>" ;
				$tmpStr .= "預定起飛時間 : {$scheduleNode['s_date']}<br>" ;
				break;
			case '146':		// 官網訂單
				$tmpCarName  = "<br>＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝<br>";
				$tmpCarName .= "　　訂單編號 : {$scheduleNode['order_no']}<br>";
				$tmpStr  = "　　　【訂單類別】業務官網訂單" . $tmpCarName ;
				$tmpStr .= "　　取車地點 : {$sStation}<br>" ;
				$tmpStr .= "預計取車時間 : {$sTime}<br>" ;
				$tmpStr .= "　　還車地點 : {$eStation}<br>" ;
				$tmpStr .= "預計還車時間 : {$eTime}<br>" ;
				$tmpStr .= "　訂車人姓名 : {$scheduleNode['cust_name']}<br>" ;
				$tmpStr .= "　訂車人電話 : {$scheduleNode['cust_tel']}<br>" ;
				$tmpStr .= "　取車人姓名 : {$scheduleNode['take_name']}<br>" ;
				$tmpStr .= "　取車人電話 : {$scheduleNode['take_mobile']}<br>" ;
				$tmpStr .= $tmpHeader.$tmpOther;
				break;

			case '147':		// 前台qrcode訂單
				$tmpCarName  = "<br>＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝<br>";
				$tmpCarName .= "　　訂單編號 : {$scheduleNode['order_no']}<br>";
				$tmpStr  = "　　　【訂單類別】前台qrcode訂單" . $tmpCarName ;
				$tmpStr .= "　　取車地點 : {$sStation}<br>" ;
				$tmpStr .= "預計取車時間 : {$sTime}<br>" ;
				$tmpStr .= "　　還車地點 : {$eStation}<br>" ;
				$tmpStr .= "預計還車時間 : {$eTime}<br>" ;
				$tmpStr .= "　訂車人姓名 : {$scheduleNode['cust_name']}<br>" ;
				$tmpStr .= "　訂車人電話 : {$scheduleNode['cust_tel']}<br>" ;
				$tmpStr .= "　取車人姓名 : {$scheduleNode['take_name']}<br>" ;
				$tmpStr .= "　取車人電話 : {$scheduleNode['take_mobile']}<br>" ;
				$tmpStr .= $tmpHeader.$tmpOther;
				break;

			default:
				# code...
				break;
		}

		// $iconTipStr = "data-toggle=\"tooltip\" data-title=\"{$tmpStr}\"" ;
		$iconTipStr = $tmpStr ;
		// dump( $iconTipStr) ;
		return $iconTipStr ;
	}

	/**
	 * [checkOrderType 列表頁的Icon呈現字串用]
	 * @param   string     $orderType [訂單類型代碼]
	 * @return  [type]                [description]
	 * @Another Angus
	 * @date    2017-12-21
	 */
	protected function checkOrderType( $orderType = "", $pay_type = "") {
		switch ( $orderType) {
			case '123': // 保養車輛
				return "btn-warning" ;
				break;

			case '124': // 自行鎖車
				return "btn-locked" ;
				break;

			case '9': // 官網訂單
				return "btn-danger" ;
				break;

			case '10': // 民宿訂單
				return "btn-success" ;
				break;

			case '11': // 長租訂單
				return "btn-long-term" ;
				break;

			case '129': // 預約未完成
				return "btn-maintenance" ;
				break;

			case '130': // 應收
				return "btn-primary" ;
				break;

			case '140': // qrcode
				if( $pay_type == ""){
					//顯示未完成
					return "btn-maintenance" ;

				}else{
					return "btn-qr" ;
				}
				break;

			case '146': // 前台業務訂車
				return "btn-business" ;
				break;

			case '147': // 前台qrcode訂車
				if( $pay_type == ""){
					//顯示未完成
					return "btn-maintenance" ;
				}else{
					return "btn-qr" ;
				}
				break;

			default:
				return "btn btn-xs" ;
				break;
		}
	}

	/**
	 * [newAgreementNo 取最後一筆訂單]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2017-12-14
	 */
	protected function newAgreementNo() {
		$todayNo = date( 'Ymd') ;
		return $this->model_sale_order->newAgreementNo( $todayNo) ;
	}

	/**
	 * 取得日期區間 輸入前x天 後x天 產出日期
	 * @param  [type]	startDay
	 * @param  [type]	endDay
	 * @return [type]
	 */
	protected function getViewDayRange( $cycleIndex = 1, $startDay = "") {
		// dump( $cycleIndex, $startDay) ;
		$endDayArr = array( 0, 2, 6, 13, 29, 59, 89) ;
		$endDay    = isset( $endDayArr[$cycleIndex]) ? $endDayArr[$cycleIndex] : 5 ;

		$retArr = array() ;
		$d                 = strtotime( $startDay) ;
		$retArr['year']    = date("Y", $d) ;
		$retArr['sMonth']  = date("n", $d) ;
		$retArr['sDayNum'] = date("j", $d) ;
		$retArr['sDayStr'] = date("Y-m-d", $d) ;

		$d                 = strtotime( $startDay . "+{$endDay} Day") ;
		$retArr['eMonth']  = date("n", $d) ;
		$retArr['eDayNum'] = date("j", $d) ;
		$retArr['eDayStr'] = date("Y-m-d", $d) ;

		// dump( $retArr) ;
		return $retArr ;
	}

	/**
	 * [getDateRange 列出日期區間的 所有日期清單]
	 * @param  array $data [description]
	 * @return [type]       [description]
	 */
	protected function getDateRange( $data) {
		$cWeekArr = array("日","一","二","三","四","五","六") ;
		$calendarDescArr = array() ;

		$endDate = new DateTime($data['eDayStr']) ;
		$endDate->modify('+1 day');
		$period = new DatePeriod (
			new DateTime($data['sDayStr']),
			new DateInterval('P1D'),
			$endDate
		) ;

		// dump( $period) ;
		foreach ($period as $date) {
			// echo $date->format('Y-m-d w') . "\n" ;
			$calendarDescArr[] = array(
					$date->format('m/d')."(".$cWeekArr[$date->format('w')].")",
					$date->format('Y-m-d'),
					strtotime( $date->format('Y-m-d'))
				) ;
		}

		return $calendarDescArr ;
	}

	/**
	 * [getSubZip description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2017-12-13
	 */
	public function getSubZip() {
		// dump( $_POST['zipMain']) ;
		$this->load->model('site/zip') ;
		$json = array();
		if (isset($this->request->get['zipMain'])) {
			$results = $this->model_site_zip->getZipCode( $this->request->get['zipMain']) ;

			foreach ($results as $result) {
				$json[] = array(
					'zip_code'	=> $result['zip_code'],
					// 'zip_name'	=> strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
					'zip_name'	=> strip_tags(html_entity_decode($result['zip_name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function getAjaxMinsu( ) {
		// dump( $_POST['minsuName']) ;
		// dump( $_GET) ;
		$this->load->model('site/minshuku') ;
		$json = array() ;
		$filter['name'] = isset( $this->request->get['minsuName']) ? $this->request->get['minsuName'] : '' ;
 		if (isset($this->request->get['minsuName'])) {
			$results = $this->model_site_minshuku->getListFromAjax( $filter) ;
			// dump( $results) ;
			foreach ($results as $result) {
				$json[] = array(
					'code'	=> $result['idx'],
					// 'zip_name'	=> strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
					'name'	=> strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	/**
	 * [getCarModelList 取得有空檔的車型]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-02-20
	 */
	public function getCarModelList() {
		$this->load->model('site/scheduling') ;
		$filter = array(
			"useCars"=> $this->request->get['useCars'],
			"car_model"=> $this->request->get['car_model'],
			"s"=> $this->request->get['s'],
			"e"=> $this->request->get['e'],
		) ;
		$json = $this->model_site_scheduling->getSchedulingCarModel( $filter) ;

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	/**
	 * [buildingRentPriceData 組合租車費用明細]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-07-11
	 */
	protected function buildingRentPriceData() {
		// dump( $this->orderInfo) ;
		$this->orderInfo['total_car'] = count(unserialize($this->orderInfo['other_car'])) ;
		// 早鳥折數
		$discountPercentage = $this->model_site_scheduling->getDiscountPercentage( $this->orderInfo['rent_date_out'], $this->orderInfo['create_date']) ;
		// 使用天數
		$second1 = strtotime( $this->orderInfo['rent_date_in_plan']) - strtotime( $this->orderInfo['rent_date_out']) ;
		$data['rentDay'] 		= floor( $second1 / 86400) ;	// 24hr * 60分 * 60秒
		$data['rentHour'] 		= ( $second1 % 86400) / 3600 ;	//

		// 產生費用
		if ( $data['rentDay'] == 0 ) {
			$data['car_price2'] = $this->carInfo['car_price'] * $discountPercentage * $this->orderInfo['total_car'] ;

		} else if ( $data['rentDay']  >= 1 && $data['rentHour'] <= 6) {
			$data['car_price2'] = ($this->carInfo['car_price'] * $data['rentDay'] * $discountPercentage + $this->carInfo['car_price'] * 0.1 * ceil( $data['rentHour']) ) * $this->orderInfo['total_car'] ;

		} else { //if ( $rentInfo['rentDay']  > 1 && $rentInfo['rentHour'] > 6) {
			$data['car_price2'] = $this->carInfo['car_price'] * ( $data['rentDay'] + 1) * $discountPercentage * $this->orderInfo['total_car'] ;

		}
		$data['car_price2Str'] = number_format( $data['car_price2']) ;

		// 配件使用天數
		$useDay = round( ( strtotime( $this->orderInfo['rent_date_in_plan']) - strtotime( $this->orderInfo['rent_date_out']))/3600/24) + 1 ;
		// bc => 兒童安全座椅〔一歲以下〕, bc1 => 兒童安全座椅〔一歲以上〕, bc2 => 兒童安全座椅〔增高墊〕
		if($this->orderInfo['baby_chair'] > 0 || $this->orderInfo['baby_chair1'] > 0 || $this->orderInfo['baby_chair2'] > 0 ||
			$this->orderInfo['children_chair'] > 0 || $this->orderInfo['gps'] > 0 || $this->orderInfo['mobile_moto'] > 0) {
			$data['car_price3'] = ($this->orderInfo['baby_chair'] * 100 * $useDay) +
									($this->orderInfo['baby_chair1'] * 100 * $useDay) +
									($this->orderInfo['baby_chair2'] * 100 * $useDay) +
									($this->orderInfo['children_chair'] * 100 * $useDay) +
									($this->orderInfo['gps'] * 0 * $useDay) +
									($this->orderInfo['mobile_moto'] * 50 * $useDay) ;
			$data['car_price3Str'] = number_format( $data['car_price3']) ;

		} else {
			$data['car_price3']    = 0 ;
			$data['car_price3Str'] = 0 ;
		}
		$data['totalStr'] = number_format( $data['car_price2'] + $data['car_price3']) ;


		$rentPriceStr = "<br>" ;
		if ( $discountPercentage < 1) {
			$rentPriceStr .= "　　　　　早鳥優惠 ".($discountPercentage * 10)." 折<br>" ;
		}
		$rentPriceStr .= "　　　　　車價（共{$this->orderInfo['total_car']}台，每台借車時間為 {$data['rentDay']}天又{$data['rentHour']}小時），共 {$data['car_price2Str']}元<br>" ;
		$rentPriceStr .= "　　　　　配件合計 {$data['car_price3Str']}元<br>" ;
		$rentPriceStr .= "　　　　　總價（含稅）{$data['totalStr']}元<br>" ;
		// dump( $rentPriceStr) ;
		return $rentPriceStr ;

/*
車價（共1台，每台借車時間為 0天又7.5小時），共 元
配件合計 0元
總價（含稅）400元
*/
		// dump( $data) ;
	}
}