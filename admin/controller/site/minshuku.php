<?php
/*
--- pos ---
金門縣

--- pos1 ---
金湖鎮
金城鎮
金沙鎮
金寧鄉
烈嶼鄉

--- room_type ---
現代式
閩式
Hotel旅舍
洋樓式
古早厝

--- start ---


--- km_type ---
好客民宿
星級旅館
民宿
旅館


*/
class ControllerSiteMinshuku extends Controller {
	protected $error = array() ;

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	public function __construct( $registry) {
		parent::__construct($registry) ;
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	public function index() {
		$this->load->language('site/minshuku') ;
		$this->document->setTitle($this->language->get('heading_title')) ;

		$this->load->model('site/minshuku') ;
		$this->getList() ;
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	public function add() {
		$this->load->language('site/minshuku') ;

		$this->document->setTitle($this->language->get('heading_title')) ;

		$this->load->model('site/minshuku') ;

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_site_minshuku->addInformation($this->request->post) ;

			$this->session->data['success'] = $this->language->get('text_success') ;

			$url = '' ;

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'] ;
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'] ;
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'] ;
			}

			$this->response->redirect($this->url->link('site/minshuku', 'token=' . $this->session->data['token'] . $url, true)) ;
		}

		$this->getForm() ;
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	public function edit() {
		$this->load->language('site/minshuku');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('site/minshuku');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_site_minshuku->editInformation($this->request->get['idx'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('site/minshuku', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	public function delete() {
		$this->load->language('site/minshuku');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('site/minshuku');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {

			foreach ($this->request->post['selected'] as $useIdx) {
				$this->model_site_minshuku->delUseIdx($useIdx);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('site/minshuku', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function getList() {
		$data = $this->preparation() ;

		$this->document->addStyle( "view/stylesheet/default.css") ;

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_pos1'])) {
			$filter_pos1 = $this->request->get['filter_pos1'];
		} else {
			$filter_pos1 = null;
		}

		if (isset($this->request->get['filter_room_type'])) {
			$filter_room_type = $this->request->get['filter_room_type'];
		} else {
			$filter_room_type = null;
		}

		if (isset($this->request->get['filter_address'])) {
			$filter_address = $this->request->get['filter_address'];
		} else {
			$filter_address = null;
		}

		// 訊息類 --------------------------------------------------------------------------------------------
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_pos1'])) {
			$url .= '&filter_pos1=' . urlencode(html_entity_decode($this->request->get['filter_pos1'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_room_type'])) {
			$url .= '&filter_room_type=' . urlencode(html_entity_decode($this->request->get['filter_room_type'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_address'])) {
			$url .= '&filter_address=' . urlencode(html_entity_decode($this->request->get['filter_address'], ENT_QUOTES, 'UTF-8'));
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		$data['sort_name'] = $this->url->link('site/minshuku', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$data['sort_pos'] = $this->url->link('site/minshuku', 'token=' . $this->session->data['token'] . '&sort=pos' . $url, true);
		$data['sort_pos1'] = $this->url->link('site/minshuku', 'token=' . $this->session->data['token'] . '&sort=pos1' . $url, true);
		$data['sort_room_type'] = $this->url->link('site/minshuku', 'token=' . $this->session->data['token'] . '&sort=room_type' . $url, true);
		$data['sort_room_cnt'] = $this->url->link('site/minshuku', 'token=' . $this->session->data['token'] . '&sort=room_cnt' . $url, true);
		$data['sort_price'] = $this->url->link('site/minshuku', 'token=' . $this->session->data['token'] . '&sort=price' . $url, true);
		$data['sort_tel'] = $this->url->link('site/minshuku', 'token=' . $this->session->data['token'] . '&sort=tel' . $url, true);
		$data['sort_address'] = $this->url->link('site/minshuku', 'token=' . $this->session->data['token'] . '&sort=address' . $url, true);

		$data['column_map'] = $this->language->get('column_map');
		$data['column_action'] = $this->language->get('column_action');
		$data['text_no_results'] = $this->language->get('text_no_results');

		$data['token'] = $this->session->data['token'];

		// 分頁功能 -------------------------------------------------------------------------------------------
//		$columnNames = $this->model_site_minshuku->getColumns() ;
		$columnNames = array(
				"name"		=> '民宿',
				"pos"		=> '縣別',
				"pos1"		=> '地區',
				"room_type"	=> '房型',
				"room_cnt"	=> '房間數',
				"price"		=> '價位',
				"tel"		=> '電話',
				"address"	=> '地址',
			) ;


		$data['columnNames'] = $columnNames ;
		$data['td_colspan'] = count( $columnNames) ;

		$filter_data = array(
			'filter_name'		=> $filter_name,
			'filter_pos1'		=> $filter_pos1,
			'filter_room_type'	=> $filter_room_type,
			'filter_address'	=> $filter_address,
			'sort'          	=> $sort,
			'order'         	=> $order,
			'start'         	=> ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'         	=> $this->config->get('config_limit_admin')
		);
		$totalCnt = $this->model_site_minshuku->getTotalCnt($filter_data) ;

		$results = $this->model_site_minshuku->getLists($filter_data) ;

		// 列表頁的 rows data
		$data['results'] = array();
		foreach ($results as $result) {
			// dump( $result) ;
			$result['edit'] = $this->url->link('site/minshuku/edit',
					'token=' . $this->session->data['token'] . '&idx=' . $result['idx'] . $url, true) ;
			// 移除網址最後的 %C2%A0 字串
			$result['google_map'] = str_replace("\xc2\xa0", '', $result['google_map']);
			$data['results'][] = $result ;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_pos1'])) {
			$url .= '&filter_pos1=' . urlencode(html_entity_decode($this->request->get['filter_pos1'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_room_type'])) {
			$url .= '&filter_room_type=' . urlencode(html_entity_decode($this->request->get['filter_room_type'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_address'])) {
			$url .= '&filter_address=' . urlencode(html_entity_decode($this->request->get['filter_address'], ENT_QUOTES, 'UTF-8'));
		}

		$pagination = new Pagination();
		$pagination->total = $totalCnt;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('site/minshuku', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['indexDec'] = sprintf($this->language->get('text_pagination'),
				($totalCnt) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 :
				0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($totalCnt - $this->config->get('config_limit_admin'))) ?
				$totalCnt : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')),
				$totalCnt, ceil($totalCnt / $this->config->get('config_limit_admin')));

		$data['filter_name'] = $filter_name;
		$data['filter_pos1'] = $filter_pos1;
		$data['filter_room_type'] = $filter_room_type;
		$data['filter_address'] = $filter_address;

		// 準備各位置資訊 ----------------------------------------------------------------------------------------
		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('site/minshuku_list', $data));
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function getForm() {
		$data = $this->preparation() ;

		// add css 日曆
		$this->document->addStyle( "view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css") ;
		$this->document->addScript( "view/javascript/jquery/datetimepicker/moment.js") ;
		$this->document->addScript( "view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js") ;
		// add css html編輯器
  		$this->document->addStyle( "view/javascript/summernote/summernote.css") ;
		$this->document->addScript( "view/javascript/summernote/summernote.js") ;
		$this->document->addScript( "view/javascript/summernote/opencart.js") ;


		$data['entry_title']          = $this->language->get('entry_title') ;
		$data['entry_description']    = $this->language->get('entry_description') ;
		$data['entry_online_date']    = $this->language->get('entry_online_date') ;
		$data['entry_status']         = $this->language->get('entry_status') ;
		$data['room_data']	= array(0=>array("name"=>'', "base_price"=>'', "per_cnt"=>'', "extra_per_cnt"=>'', "device"=>'', "thumb"=>'', "image"=>''));
		if (isset($this->request->get['idx']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$this->rowInfo = $this->model_site_minshuku->getInformation($this->request->get['idx']) ;
			$data['room_data']	=  $this->model_site_minshuku->getRoomData($this->request->get['idx']) ;
			// dump( $this->rowInfo) ;
		}

		// form 表格內的錯誤訊息 ----------------------------------------------------------------------------------
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'] ;
		} else {
			$data['error_warning'] = '' ;
		}

		$data['error_name']          = $this->setDataInfo( 'error', '', 'name') ;
		$data['error_pos']           = $this->setDataInfo( 'error', '', 'pos') ;
		$data['error_pos1']          = $this->setDataInfo( 'error', '', 'pos1') ;
		$data['error_room_type']     = $this->setDataInfo( 'error', '', 'room_type') ;
		$data['error_room_cnt']      = $this->setDataInfo( 'error', '', 'room_cnt') ;
		$data['error_price']         = $this->setDataInfo( 'error', '', 'price') ;
		$data['error_tel']           = $this->setDataInfo( 'error', '', 'tel') ;
		$data['error_email']         = $this->setDataInfo( 'error', '', 'email') ;

		// form 表格內的資料 --------------------------------------------------------------------------------------
		$data['input_name']          = $this->setDataInfo( 'post', 'input', 'name') ;
		$data['input_start']         = $this->setDataInfo( 'post', 'input', 'start') ;
		$data['input_pos']           = $this->setDataInfo( 'post', 'input', 'pos') ;
		$data['input_pos1']          = $this->setDataInfo( 'post', 'input', 'pos1') ;
		$data['input_room_type']     = $this->setDataInfo( 'post', 'input', 'room_type') ;
		$data['input_room_cnt']      = $this->setDataInfo( 'post', 'input', 'room_cnt') ;
		$data['input_price']         = $this->setDataInfo( 'post', 'input', 'price') ;
		$data['input_tel']           = $this->setDataInfo( 'post', 'input', 'tel') ;
		$data['input_fax']           = $this->setDataInfo( 'post', 'input', 'fax') ;
		$data['input_email']         = $this->setDataInfo( 'post', 'input', 'email') ;
		$data['input_url']           = $this->setDataInfo( 'post', 'input', 'url') ;
		$data['input_post_id']       = $this->setDataInfo( 'post', 'input', 'post_id') ;
		$data['input_address']       = $this->setDataInfo( 'post', 'input', 'address') ;
		$data['input_km_type']       = $this->setDataInfo( 'post', 'input', 'km_type') ;
		$data['input_reg_no']        = $this->setDataInfo( 'post', 'input', 'reg_no') ;
		$data['input_approved_date'] = $this->setDataInfo( 'post', 'input', 'approved_date') ;
		$data['input_google_map']    = $this->setDataInfo( 'post', 'input', 'google_map') ;
		$data['input_service']       = $this->setDataInfo( 'post', 'input', 'service') ;
		$data['input_device_text']   = $this->setDataInfo( 'post', 'input', 'device_text') ;
		$data['input_service_text']  = $this->setDataInfo( 'post', 'input', 'service_text') ;
		$data['input_rule_text']     = $this->setDataInfo( 'post', 'input', 'rule_text') ;
		$data['input_cancel_text']   = $this->setDataInfo( 'post', 'input', 'cancel_text') ;



		// 上線日期
		if (isset($this->request->post['input_online_date'])) {
			$data['input_online_date'] = $this->request->post['input_online_date'] ;
		} elseif (!empty($rowInfo)) {
			$data['input_online_date'] = $rowInfo['online_date'] ;
		} else {
			$data['input_online_date'] = '' ;
		}
		// 頁面狀態
		if (isset($this->request->post['sel_status'])) {
			$data['sel_status'] = $this->request->post['sel_status'] ;
		} elseif (!empty($rowInfo)) {
			$data['sel_status'] = $rowInfo['status'] ;
		} else {
			$data['sel_status'] = '1' ;
		}

		// 圖片設定
		if ( !empty($this->rowInfo))
			$imageIntroArr = unserialize( $this->rowInfo['imageIntro']) ;
		else {
			$imageIntroArr = array() ;
		}

		$this->load->model('tool/image');
		$data["no_image"] =$this->model_tool_image->resize('no_image.png', 100, 100);
		for ($i=0; $i <8 ; $i++) {
			if (isset($this->request->post['imageIntro'.($i+1)])) {
				$data['imageIntro'.($i+1)] = $this->request->post['imageIntro'.($i+1)];
			} elseif (!empty($this->rowInfo)) {
				$data['imageIntro'.($i+1)] = isset( $imageIntroArr[$i]) ? $imageIntroArr[$i] : "" ;
			} else {
				$data['imageIntro'.($i+1)] = '';
			}
			if (isset($this->request->post['imageIntro'.($i+1)]) && is_file(DIR_IMAGE . $this->request->post['imageIntro'.($i+1)])) {
				$data['thumb'.($i+1)] = $this->model_tool_image->resize($this->request->post['imageIntro'.($i+1)], 100, 100);
			} elseif (!empty($this->rowInfo) && is_file(DIR_IMAGE . $imageIntroArr[$i])) {
				$data['thumb'.($i+1)] = $this->model_tool_image->resize($imageIntroArr[$i], 100, 100);
			} else {
				$data['thumb'.($i+1)] = $this->model_tool_image->resize('no_image.png', 100, 100);
			}
		}

		foreach ($data['room_data'] as $key => $room_row) {

			if (isset($room_row["image"]) && is_file(DIR_IMAGE . $room_row["image"])) {
				$data['room_data'][$key]['thumb'] = $this->model_tool_image->resize($room_row["image"], 100, 100);
			} else {
				$data['room_data'][$key]['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
			}
		}


		$data['header'] = $this->load->controller('common/header') ;
		$data['column_left'] = $this->load->controller('common/column_left') ;
		$data['footer'] = $this->load->controller('common/footer') ;

		$this->response->setOutput($this->load->view('site/minshuku_form', $data)) ;
     }

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function validateForm() {
		// 檢查是否有權限
		if (!$this->user->hasPermission('modify', 'site/minshuku')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		// 檢查各項必填欄位 -----------------------------------------------------------------------------------------
		if ((utf8_strlen($this->request->post['input_name']) == 0)) {
			$this->error['name'] = $this->language->get('error_name');
		}
		if ((utf8_strlen($this->request->post['input_pos']) == 0)) {
			$this->error['pos'] = $this->language->get('error_pos');
		}
		if ((utf8_strlen($this->request->post['input_pos1']) == 0)) {
			$this->error['pos1'] = $this->language->get('error_pos1');
		}
		if ((utf8_strlen($this->request->post['input_room_type']) == 0)) {
			$this->error['room_type'] = $this->language->get('error_room_type');
		}
		if ((utf8_strlen($this->request->post['input_room_cnt']) == 0)) {
			$this->error['room_cnt'] = $this->language->get('error_room_cnt');
		}
		if ((utf8_strlen($this->request->post['input_price']) == 0)) {
			$this->error['price'] = $this->language->get('error_price');
		}
		if ((utf8_strlen($this->request->post['input_tel']) == 0)) {
			$this->error['tel'] = $this->language->get('error_tel');
		}
		if ((utf8_strlen($this->request->post['input_email']) == 0)) {
			$this->error['email'] = $this->language->get('error_email');
		}
		if ((utf8_strlen($this->request->post['input_price']) == 0)) {
			$this->error['price'] = $this->language->get('error_price');
		}

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		return !$this->error;
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'site/minshuku')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('site/minshuku');

		foreach ($this->request->post['selected'] as $information_id) {
			if ($this->config->get('config_account_id') == $information_id) {
				$this->error['warning'] = $this->language->get('error_account');
			}

			if ($this->config->get('config_checkout_id') == $information_id) {
				$this->error['warning'] = $this->language->get('error_checkout');
			}

			if ($this->config->get('config_affiliate_id') == $information_id) {
				$this->error['warning'] = $this->language->get('error_affiliate');
			}

			if ($this->config->get('config_return_id') == $information_id) {
				$this->error['warning'] = $this->language->get('error_return');
			}

			// $store_total = $this->model_setting_store->getTotalStoresByInformationId($information_id);

			// if ($store_total) {
			// 	$this->error['warning'] = sprintf($this->language->get('error_store'), $store_total);
			// }
		}

		return !$this->error;
	}

	/**
	* 前置作業
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function preparation() {
		// 標題
		$data['heading_title'] = $this->language->get('heading_title') ;
		// 次標題
		$data['text_list'] = $this->language->get('text_list') ;
		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_pos1'] = $this->language->get('entry_pos1');
		$data['entry_room_type'] = $this->language->get('entry_room_type');
		$data['entry_address'] = $this->language->get('entry_address');

		$data['text_form'] = !isset($this->request->get['idx']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_confirm'] = $this->language->get('text_confirm') ;
		$data['text_enabled'] = $this->language->get('text_enabled') ;
		$data['text_disabled'] = $this->language->get('text_disabled') ;
		// 麵包屑
		$url = '';
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
			);
		$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('site/minshuku', 'token=' . $this->session->data['token'] . $url, 'SSL')
			);

		// 列表頁 按鈕
		$data['url_add'] = $this->url->link('site/minshuku/add', 'token=' . $this->session->data['token'] . $url, 'SSL') ;
		$data['url_delete'] = $this->url->link('site/minshuku/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['url_cancel'] = $this->url->link('site/minshuku', 'token=' . $this->session->data['token'] . $url, true);
		if (!isset($this->request->get['idx'])) {
			$data['url_action'] = $this->url->link('site/minshuku/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['url_action'] = $this->url->link('site/minshuku/edit', 'token=' . $this->session->data['token'] . '&idx=' . $this->request->get['idx'] . $url, true);
		}

		$data['button_add'] = $this->language->get('text_add');
		$data['button_edit'] = $this->language->get('text_edit');
		$data['button_delete'] = $this->language->get('text_delete');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_filter'] = $this->language->get('button_filter');
		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_image'] = $this->language->get('tab_image');
		$data['tab_room'] = $this->language->get('tab_room');
		$data['tab_other'] = $this->language->get('tab_other');

		// 提示訊息
		$data['error_warning'] = '';
		$data['success'] = '';
		return $data ;
	}
}