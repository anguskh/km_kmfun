<?php
class ControllerSiteCarModel extends Controller {
	private $error		= array() ;
	private $carsPageLimit = 100 ;
	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	public function __construct( $registry) {
		parent::__construct($registry) ;
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	public function index() {
		$this->load->language('site/car_model') ;
		$this->document->setTitle($this->language->get('heading_title')) ;

		$this->load->model('site/car_model') ;
		$this->getList() ;
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	public function add() {
		$this->load->language('site/car_model') ;
		$this->document->setTitle($this->language->get('heading_title')) ;
		$this->load->model('site/car_model') ;
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_site_car_model->addInformation($this->request->post) ;
			$this->session->data['success'] = $this->language->get('text_success') ;
			$url = '' ;
			$this->response->redirect($this->url->link('site/car_model', 'token=' . $this->session->data['token'] . $url, true)) ;
		}
		$this->getForm() ;
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	public function edit() {
		$this->load->language('site/car_model');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('site/car_model');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

			// dump( $this->request->post) ;
			$this->model_site_car_model->editInformation($this->request->get['idx'], $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$url = '';
			$this->response->redirect($this->url->link('site/car_model', 'token=' . $this->session->data['token'] . $url, true));
		}
		$this->getForm();
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	public function delete() {
		dump( $this->request->post) ;
		exit() ;
		$this->load->language('site/car_model');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('site/car_model');
		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $useIdx) {
				// $this->model_site_car_model->delUseIdx($useIdx);
			}
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('site/car_model', 'token=' . $this->session->data['token'] . $url, true));
		}
		$this->getList();
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function getList() {
		$data = $this->preparation() ;

		// if (isset($this->request->get['page'])) {
		// 	$page = $this->request->get['page'];
		// } else {
		// 	$page = 1;
		// }
		$page  = !isset( $this->request->get['page'])	? 1 : $this->request->get['page'] ;
		$sort  = !isset( $this->request->get['sort'])	? 'car_model' : $this->request->get['sort'] ;
		$order = !isset( $this->request->get['order'])	? 'ASC' : $this->request->get['order'] ;


		// 訊息類 --------------------------------------------------------------------------------------------
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';
		$data['column_action']   = $this->language->get('column_action');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['car_model_arr']   = $this->model_site_car_model->getCarModelArr( true) ;

		// 設定列表欄位 ---------------------------------------------------------------------------------
		// $columnNames = $this->model_site_car_model->getColumns() ;
		$columnNames = array(
			"car_model"    => "車種",
			"car_seed"     => "車型",
			"car_year"     => "年份",
			"car_price"    => "單價",
			"displacement" => "排氣量",
			"status"       => "狀態",
			) ;
		$data['columnNames'] = $columnNames ;
		// dump( $data['columnNames']) ;
		// dump( $data['car_model_arr']) ;

		$data['td_colspan'] = count( $columnNames) ;

		// 欄位排序設定
		$data['sort'] = $sort;
		$data['order'] = $order;

		// 欄位用的排序條件
		if ($order == 'ASC') {
			$url .= "&order=DESC&page={$page}" ;
		} else {
			$url .= "&order=ASC&page={$page}" ;
		}

		$sortUrlArr = array (
			"car_model"    => $this->url->link('site/car_model', 'token=' . $this->session->data['token'] . "&sort=car_model" . $url, true),
			"car_seed"     => $this->url->link('site/car_model', 'token=' . $this->session->data['token'] . "&sort=car_seed" . $url, true),
			"car_year"     => $this->url->link('site/car_model', 'token=' . $this->session->data['token'] . "&sort=car_year" . $url, true),
			"car_price"    => $this->url->link('site/car_model', 'token=' . $this->session->data['token'] . "&sort=car_price" . $url, true),
			"displacement" => $this->url->link('site/car_model', 'token=' . $this->session->data['token'] . "&sort=displacement" . $url, true),
			) ;
		$data['sortUrl'] = $sortUrlArr ;

		$filter_data = array (
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			// 'limit' => $this->config->get('config_limit_admin')
			'limit' => $this->carsPageLimit, // 50
			) ;


		$totalCnt = $this->model_site_car_model->getTotalCnt( $filter_data) ;
		// dump( $totalCnt) ;
		$results = $this->model_site_car_model->getLists($filter_data) ;
		// 列表頁的 rows data
		$data['results'] = array();
		foreach ($results as $result) {
			// dump( $result) ;
			switch ( $result['status']) {
				case '1':
					$result['status'] = "啟用" ;
					break;
				case '2':
					$result['status'] = "<font color=red>停用</font>" ;
					break;
				default :
					$result['status'] = "" ;
					break ;
			}
			$result['edit'] = $this->url->link('site/car_model/edit',
					'token=' . $this->session->data['token'] . '&idx=' . $result['idx'] . $url, true) ;
			$data['results'][] = $result ;
		}

		// 分頁功能 -------------------------------------------------------------------------------------------
		// 分頁用的排序條件
		$url = "&sort={$sort}" ;
		if ($order == 'ASC') {
			$url .= '&order=ASC';
		} else {
			$url .= '&order=DESC';
		}
		// 分頁程式區
		$pagination = new Pagination();
		$pagination->total = $totalCnt;
		$pagination->page = $page;
		// $pagination->limit = $this->config->get('config_limit_admin');
		$pagination->limit = $this->carsPageLimit ; // 50
		// dump( $pagination->limit) ;
		$pagination->url = $this->url->link('site/car_model', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);
		$data['pagination'] = $pagination->render();
		// 顯示 1 - 20 / 22 (共 2 頁)
		// 顯示 %d - %d / %d (共 %d 頁)
		// dump( $this->language->get('text_pagination')) ;
		// dump( $page) ;
		// dump( $this->config->get('config_limit_admin')) ;
		$data['indexDec'] = sprintf($this->language->get('text_pagination'),
			($totalCnt) ? (($page - 1) * $pagination->limit) + 1 :
			0, ((($page - 1) * $pagination->limit) > ($totalCnt - $pagination->limit)) ?
			$totalCnt : ((($page - 1) * $pagination->limit) + $pagination->limit),
			$totalCnt, ceil($totalCnt / $pagination->limit));

		// 準備各位置資訊 ----------------------------------------------------------------------------------------
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('site/car_model_list', $data));
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function getForm() {
		$data = $this->preparation() ;

		$data['entry_status']		= $this->language->get('entry_status') ;
		$data['sel_status']			= '' ;

		if (isset($this->request->get['idx']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$this->rowInfo = $this->model_site_car_model->getInformation($this->request->get['idx']) ;
			// dump( $this->rowInfo) ;
		}

		// form 表格內的錯誤訊息 ----------------------------------------------------------------------------------
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'] ;
		} else {
			$data['error_warning'] = '' ;
		}
		foreach ($this->error as $errIndex => $errMsg) {
			$data[$errIndex] = $errMsg ;
		}

		// 選項
		$data['car_model_arr'] = $this->model_site_car_model->getCarModelArr( true) ;
		// form 表格內的資料 --------------------------------------------------------------------------------------
		// 標題
		$data['input_car_seed']         = $this->setDataInfo( 'post', 'input', 'car_seed') ;
		$data['sel_car_model']          = $this->setDataInfo( 'post', 'sel', 'car_model') ;
		$data['input_car_year']         = $this->setDataInfo( 'post', 'input', 'car_year') ;
		$data['input_car_price']        = $this->setDataInfo( 'post', 'input', 'car_price') ;
		$data['input_car_peopele']      = $this->setDataInfo( 'post', 'input', 'car_peopele') ;
		$data['input_car_door']         = $this->setDataInfo( 'post', 'input', 'car_door') ;
		$data['input_gas_class']        = $this->setDataInfo( 'post', 'input', 'gas_class') ;
		$data['input_gas_type']         = $this->setDataInfo( 'post', 'input', 'gas_type') ;
		$data['input_displacement']     = $this->setDataInfo( 'post', 'input', 'displacement') ;
		$data['input_at_mt']            = $this->setDataInfo( 'post', 'input', 'at_mt') ;
		$data['input_back_radar']       = $this->setDataInfo( 'post', 'input', 'back_radar') ;
		$data['input_cd_player']        = $this->setDataInfo( 'post', 'input', 'cd_player') ;
		$data['input_radio']            = $this->setDataInfo( 'post', 'input', 'radio') ;
		$data['input_usb_charge']       = $this->setDataInfo( 'post', 'input', 'usb_charge') ;
		$data['input_gps']              = $this->setDataInfo( 'post', 'input', 'gps') ;
		$data['input_driving_recorder'] = $this->setDataInfo( 'post', 'input', 'driving_recorder') ;
		$data['input_airbag']           = $this->setDataInfo( 'post', 'input', 'airbag') ;
		$data['input_airbag_pos']       = $this->setDataInfo( 'post', 'input', 'airbag_pos') ;
		$data['sel_status']             = $this->setDataInfo( 'post', 'sel', 'status') ;

		if (isset($this->request->post['image'])) {
			$data['image'] = $this->request->post['image'];
		} elseif (!empty($this->rowInfo)) {
			$data['image'] = $this->rowInfo['image'];
		} else {
			$data['image'] = '';
		}

		if ( !empty($this->rowInfo))
			$imageIntroArr = unserialize( $this->rowInfo['imageIntro']) ;
		else {
			$imageIntroArr = array() ;
		}

		if (isset($this->request->post['imageIntro1'])) {
			$data['imageIntro1'] = $this->request->post['imageIntro1'];
		} elseif (!empty($this->rowInfo)) {
			$data['imageIntro1'] = isset( $imageIntroArr[0]) ? $imageIntroArr[0] : "" ;
		} else {
			$data['imageIntro1'] = '';
		}
		if (isset($this->request->post['imageIntro2'])) {
			$data['imageIntro2'] = $this->request->post['imageIntro2'];
		} elseif (!empty($this->rowInfo)) {
			$data['imageIntro2'] = isset( $imageIntroArr[1]) ? $imageIntroArr[1] : "" ;
		} else {
			$data['imageIntro2'] = '';
		}
		if (isset($this->request->post['imageIntro3'])) {
			$data['imageIntro3'] = $this->request->post['imageIntro3'];
		} elseif (!empty($this->rowInfo)) {
			$data['imageIntro3'] = isset( $imageIntroArr[2]) ? $imageIntroArr[2] : "" ;
		} else {
			$data['imageIntro3'] = '';
		}

		$this->load->model('tool/image');
		if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
		} elseif (!empty($this->rowInfo) && is_file(DIR_IMAGE . $this->rowInfo['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($this->rowInfo['image'], 100, 100);
		} else {
			$data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}

		if (isset($this->request->post['imageIntro1']) && is_file(DIR_IMAGE . $this->request->post['imageIntro1'])) {
			$data['thumb1'] = $this->model_tool_image->resize($this->request->post['imageIntro1'], 100, 100);
		} elseif (!empty($this->rowInfo) && is_file(DIR_IMAGE . $imageIntroArr[0])) {
			$data['thumb1'] = $this->model_tool_image->resize($imageIntroArr[0], 100, 100);
		} else {
			$data['thumb1'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}
		if (isset($this->request->post['imageIntro2']) && is_file(DIR_IMAGE . $this->request->post['imageIntro2'])) {
			$data['thumb2'] = $this->model_tool_image->resize($this->request->post['imageIntro2'], 100, 100);
		} elseif (!empty($this->rowInfo) && is_file(DIR_IMAGE . $imageIntroArr[1])) {
			$data['thumb2'] = $this->model_tool_image->resize($imageIntroArr[1], 100, 100);
		} else {
			$data['thumb2'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}
		if (isset($this->request->post['imageIntro3']) && is_file(DIR_IMAGE . $this->request->post['imageIntro3'])) {
			$data['thumb3'] = $this->model_tool_image->resize($this->request->post['imageIntro3'], 100, 100);
		} elseif (!empty($this->rowInfo) && is_file(DIR_IMAGE . $imageIntroArr[2])) {
			$data['thumb3'] = $this->model_tool_image->resize($imageIntroArr[2], 100, 100);
		} else {
			$data['thumb3'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}



		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);


		$data['header'] = $this->load->controller('common/header') ;
		$data['column_left'] = $this->load->controller('common/column_left') ;
		$data['footer'] = $this->load->controller('common/footer') ;
		$this->response->setOutput($this->load->view('site/car_model_form', $data)) ;
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function validateForm() {
		// 檢查是否有權限
		if (!$this->user->hasPermission('modify', 'site/car_model')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		// 檢查各項必填欄位 -----------------------------------------------------------------------------------------
		// 車型
		if ( utf8_strlen( $this->request->post['input_car_seed']) == 0) {
			$this->error['error_car_seed'] = $this->language->get('error_car_seed') ;
		}

		if ( !isset($this->request->get['idx'])) {
			$model = $this->model_site_car_model->getInformationUseSeed( $this->request->post['input_car_seed']) ;
			// dump( $model) ;
			if ( isset( $model['car_seed']) &&
				strtoupper( $model['car_seed']) == strtoupper( $this->request->post['input_car_seed'])) {
				$this->error['error_car_seed'] = $this->language->get('error_car_seed_1') ;
			}
		}

		// 車種
		if ( $this->request->post['sel_car_model'] == '') {
			$this->error['error_car_model'] = $this->language->get('error_car_model') ;
		}
		// 單價
		if ( !is_numeric($this->request->post['input_car_price'])) {
			$this->error['error_car_price'] = $this->language->get('error_car_price') ;
		}

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		return !$this->error;
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/product')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		// todo 檢查是否有車輛 有車輛就不能刪


		return !$this->error;
	}

	/**
	* 前置作業
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function preparation() {
		// 標題
		$data['heading_title'] = $this->language->get('heading_title') ;
		// 次標題
		$data['text_list'] = $this->language->get('text_list') ;
		$data['text_form'] = !isset($this->request->get['idx']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_confirm'] = $this->language->get('text_confirm') ;
		$data['text_enabled'] = $this->language->get('text_enabled') ;
		$data['text_disabled'] = $this->language->get('text_disabled') ;
		// 麵包屑
		$url = '';
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
			);
		$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('site/car_model', 'token=' . $this->session->data['token'] . $url, 'SSL')
			);

		// 列表頁 按鈕
		$data['url_add'] = $this->url->link('site/car_model/add', 'token=' . $this->session->data['token'] . $url, 'SSL') ;
		$data['url_delete'] = $this->url->link('site/car_model/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['url_cancel'] = $this->url->link('site/car_model', 'token=' . $this->session->data['token'] . $url, true);
		if (!isset($this->request->get['idx'])) {
			$data['url_action'] = $this->url->link('site/car_model/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['url_action'] = $this->url->link('site/car_model/edit', 'token=' . $this->session->data['token'] . '&idx=' . $this->request->get['idx'] . $url, true);
		}

		$data['button_add'] = $this->language->get('text_add');
		$data['button_edit'] = $this->language->get('text_edit');
		$data['button_delete'] = $this->language->get('text_delete');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['tab_general'] = $this->language->get('tab_general');

		// 提示訊息
		$data['error_warning'] = '';
		$data['success'] = '';
		return $data ;
	}

	/**
	* 前置作業
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	public function getModelList() {
		$json = array();
		if (!empty($this->request->get['car_model'])) {
			$this->load->model('site/car_model') ;

			$json = $this->model_site_car_model->jsonCarModelList($this->request->get['car_model']);
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

}