<?php
class ControllerSiteNews extends Controller {
	private $error = array();
	/**
     * 新增 / 修改 頁面
     * @param  [type] $method [description]
     * @return [type]         [description]
     */
	public function __construct( $registry) {
		parent::__construct($registry);
	}

	/**
     * 新增 / 修改 頁面
     * @param  [type] $method [description]
     * @return [type]         [description]
     */
	public function index() {
		$this->load->language('site/news');
		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('site/news');
		$this->getList();
	}

	/**
     * 新增 / 修改 頁面
     * @param  [type] $method [description]
     * @return [type]         [description]
     */
	public function add() {
		$this->load->language('site/news') ;

		$this->document->setTitle($this->language->get('heading_title')) ;

		$this->load->model('site/news') ;

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_site_news->addInformation($this->request->post) ;

			$this->session->data['success'] = $this->language->get('text_success') ;

			$url = '' ;

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'] ;
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'] ;
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'] ;
			}

			$this->response->redirect($this->url->link('site/news', 'token=' . $this->session->data['token'] . $url, true)) ;
		}

		$this->getForm() ;
	}

	/**
     * 新增 / 修改 頁面
     * @param  [type] $method [description]
     * @return [type]         [description]
     */
	public function edit() {
		$this->load->language('site/news');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('site/news');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_site_news->editInformation($this->request->get['idx'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('site/news', 'token=' . $this->session->data['token'] . $url, true));
		}

          $this->getForm();
	}

	/**
     * 新增 / 修改 頁面
     * @param  [type] $method [description]
     * @return [type]         [description]
     */
	public function delete() {
        $this->load->language('site/news');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('site/news');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {

			foreach ($this->request->post['selected'] as $useIdx) {
				$this->model_site_news->delUseIdx($useIdx);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('site/news', 'token=' . $this->session->data['token'] . $url, true));
		}

          $this->getList();
     }

	/**
     * 新增 / 修改 頁面
     * @param  [type] $method [description]
     * @return [type]         [description]
     */
	protected function getList() {
		$data = $this->preparation() ;

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		// 訊息類 --------------------------------------------------------------------------------------------
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		$data['column_action'] = $this->language->get('column_action');
		$data['text_no_results'] = $this->language->get('text_no_results');


		// 分頁功能 -------------------------------------------------------------------------------------------

		$columnNames = $this->model_site_news->getColumns() ;
		$columnNames = array(
				"title"			=> $this->language->get('column_title'),
				"status"		=> $this->language->get('column_status'),
				"online_date"	=> $this->language->get('column_online_date'),
				"c_date"		=> $this->language->get('column_c_date'),
			) ;

		$data['columnNames'] = $columnNames ;
		$data['td_colspan'] = count( $columnNames) ;

		$filter_data = array(
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);
		$totalCnt = $this->model_site_news->getTotalCnt() ;

		$results = $this->model_site_news->getLists($filter_data) ;

		// 列表頁的 rows data
		$data['results'] = array();
		foreach ($results as $result) {
			// dump( $result) ;
			$result['edit'] = $this->url->link('site/news/edit',
				'token=' . $this->session->data['token'] . '&idx=' . $result['idx'] . $url, true) ;

			$data['results'][] = $result ;
		}

		$pagination = new Pagination();
		$pagination->total = $totalCnt;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('site/news', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['indexDec'] = sprintf($this->language->get('text_pagination'),
				($totalCnt) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 :
				0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($totalCnt - $this->config->get('config_limit_admin'))) ?
				$totalCnt : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')),
				$totalCnt, ceil($totalCnt / $this->config->get('config_limit_admin')));


		// 準備各位置資訊 ----------------------------------------------------------------------------------------
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('site/news_list', $data));
	}

	/**
     * 新增 / 修改 頁面
     * @param  [type] $method [description]
     * @return [type]         [description]
     */
	protected function getForm() {
		$data = $this->preparation() ;

		// add css 日曆
		$this->document->addStyle( "view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css") ;
		$this->document->addScript( "view/javascript/jquery/datetimepicker/moment.js") ;
		$this->document->addScript( "view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js") ;
		// add css html編輯器
  		$this->document->addStyle( "view/javascript/summernote/summernote.css") ;
		$this->document->addScript( "view/javascript/summernote/summernote.js") ;
		$this->document->addScript( "view/javascript/summernote/opencart.js") ;

		$data['entry_title']		= $this->language->get('entry_title') ;
		$data['entry_description']	= $this->language->get('entry_description') ;
		$data['entry_online_date']	= $this->language->get('entry_online_date') ;
		$data['entry_status']		= $this->language->get('entry_status') ;

		if (isset($this->request->get['idx']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$rowInfo = $this->model_site_news->getInformation($this->request->get['idx']) ;
		}

		// form 表格內的錯誤訊息 ----------------------------------------------------------------------------------
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'] ;
		} else {
			$data['error_warning'] = '' ;
		}

		if (isset($this->error['title'])) {
			$data['error_title'] = $this->error['title'] ;
		} else {
			$data['error_title'] = '' ;
		}

		if (isset($this->error['description'])) {
			$data['error_description'] = $this->error['description'];
		} else {
			$data['error_description'] = '' ;
		}

		if (isset($this->error['online_date'])) {
			$data['error_online_date'] = $this->error['online_date'];
		} else {
			$data['error_online_date'] = '' ;
		}

		// form 表格內的資料 --------------------------------------------------------------------------------------
		// 標題
		if (isset($this->request->post['input_title'])) {
			$data['input_title'] = $this->request->post['input_title'] ;
		} elseif (!empty($rowInfo)) {
			$data['input_title'] = $rowInfo['title'] ;
		} else {
			$data['input_title'] = '' ;
		}
		// 頁面內容
		if (isset($this->request->post['area_description'])) {
			$data['area_description'] = $this->request->post['area_description'] ;
		} elseif (!empty($rowInfo)) {
			$data['area_description'] = $rowInfo['description'] ;
		} else {
			$data['area_description'] = '' ;
		}
		// 上線日期
		if (isset($this->request->post['input_online_date'])) {
			$data['input_online_date'] = $this->request->post['input_online_date'] ;
		} elseif (!empty($rowInfo)) {
			$data['input_online_date'] = $rowInfo['online_date'] ;
		} else {
			$data['input_online_date'] = '' ;
		}
		// 頁面狀態
		if (isset($this->request->post['sel_status'])) {
			$data['sel_status'] = $this->request->post['sel_status'] ;
		} elseif (!empty($rowInfo)) {
			$data['sel_status'] = $rowInfo['status'] ;
		} else {
			$data['sel_status'] = '1' ;
		}

		$data['header'] = $this->load->controller('common/header') ;
		$data['column_left'] = $this->load->controller('common/column_left') ;
		$data['footer'] = $this->load->controller('common/footer') ;

		$this->response->setOutput($this->load->view('site/news_form', $data)) ;
	}

	/**
     * 新增 / 修改 頁面
     * @param  [type] $method [description]
     * @return [type]         [description]
     */
	protected function validateForm() {
		// 檢查是否有權限
		if (!$this->user->hasPermission('modify', 'site/news')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		// 檢查各項必填欄位 -----------------------------------------------------------------------------------------
		// 標題
		if ((utf8_strlen($this->request->post['input_title']) < 3) || (utf8_strlen($this->request->post['input_title']) > 64)) {
			$this->error['title'] = $this->language->get('error_title');
		}
		// 頁面內容
		// dump( trim($this->request->post['area_description'])) ;
		if (utf8_strlen(trim($this->request->post['area_description'])) < 3) {
			$this->error['description'] = $this->language->get('error_description');
		}
		// 上線日期
		// dump( trim($this->request->post['input_online_date'])) ;
		$pattern1 = '/^19|20[0-9]{2}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])\s([0-1]?\d|2[0-3]):([0-5]?\d):([0-5]?\d)/' ;
		$pattern2 = '/^19|20[0-9]{2}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])/' ;
		$pattern3 = '/^0000-00-00\s00:00:00/' ;
		// dump(preg_match( $pattern1, $this->request->post['input_online_date'])) ;
		if ( preg_match( $pattern1, $this->request->post['input_online_date']) ||
			preg_match( $pattern2, $this->request->post['input_online_date']) ||
			preg_match( $pattern3, $this->request->post['input_online_date']) ) {
		} else {
			$this->error['online_date'] = $this->language->get('error_online_date');
		}


		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		return !$this->error;
	}

	/**
     * 新增 / 修改 頁面
     * @param  [type] $method [description]
     * @return [type]         [description]
     */
	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'site/news')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('site/news');

		// foreach ($this->request->post['selected'] as $information_id) {
		// 	if ($this->config->get('config_account_id') == $information_id) {
		// 		$this->error['warning'] = $this->language->get('error_account');
		// 	}

		// 	if ($this->config->get('config_checkout_id') == $information_id) {
		// 		$this->error['warning'] = $this->language->get('error_checkout');
		// 	}

		// 	if ($this->config->get('config_affiliate_id') == $information_id) {
		// 		$this->error['warning'] = $this->language->get('error_affiliate');
		// 	}

		// 	if ($this->config->get('config_return_id') == $information_id) {
		// 		$this->error['warning'] = $this->language->get('error_return');
		// 	}

		// 	$store_total = $this->model_setting_store->getTotalStoresByInformationId($information_id);

		// 	if ($store_total) {
		// 		$this->error['warning'] = sprintf($this->language->get('error_store'), $store_total);
		// 	}
		// }

		return !$this->error;
	}

     /**
     * 前置作業
     * @param  [type] $method [description]
     * @return [type]         [description]
     */
	protected function preparation() {
		// 標題
		$data['heading_title'] = $this->language->get('heading_title') ;
		// 次標題
		$data['text_list'] = $this->language->get('text_list') ;
		$data['text_form'] = !isset($this->request->get['idx']) ? $this->language->get('text_add') : $this->language->get('text_edit');

		$data['text_confirm'] = $this->language->get('text_confirm') ;
		$data['text_enabled'] = $this->language->get('text_enabled') ;
		$data['text_disabled'] = $this->language->get('text_disabled') ;

		// 麵包屑
		$url = '';
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('site/news', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

		// 列表頁 按鈕
		$data['url_add'] = $this->url->link('site/news/add', 'token=' . $this->session->data['token'] . $url, 'SSL') ;
		$data['url_delete'] = $this->url->link('site/news/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['url_cancel'] = $this->url->link('site/news', 'token=' . $this->session->data['token'] . $url, true);
		if (!isset($this->request->get['idx'])) {
			$data['url_action'] = $this->url->link('site/news/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['url_action'] = $this->url->link('site/news/edit', 'token=' . $this->session->data['token'] . '&idx=' . $this->request->get['idx'] . $url, true);
		}


		$data['button_add'] = $this->language->get('text_add');
		$data['button_edit'] = $this->language->get('text_edit');
		$data['button_delete'] = $this->language->get('text_delete');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');

		// 提示訊息
		$data['error_warning'] = '';
		$data['success'] = '';

		return $data ;
     }
}