<?php
class ControllerSiteOption extends Controller {
	private $error = array() ;

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	public function __construct( $registry) {
		parent::__construct($registry) ;
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	public function index() {
		$this->load->language('site/option') ;
		$this->document->setTitle($this->language->get('heading_title')) ;

		$this->load->model('site/option') ;
		$this->getList() ;
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	public function add() {
		$this->load->language('site/option') ;

		$this->document->setTitle($this->language->get('heading_title')) ;

		$this->load->model('site/option') ;

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			foreach ( $this->request->post['optionchild'] as $idx => $optionArr) {
				if ( strpos( $idx, "a") !== false) {
					$optionArr['parent'] = isset( $this->request->get['sel_mainOption']) ? $this->request->get['sel_mainOption'] : '0' ;
					if ( $this->issetOption( $optionArr)) {
						$this->model_site_option->addInformation( $optionArr) ;
					}
				} else {
					$this->model_site_option->editInformation( $idx, $optionArr) ;
				}
			}

			$this->session->data['success'] = $this->language->get('text_success') ;
		}

		$this->getList() ;
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function getList() {
		$data = $this->preparation() ;

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		// 訊息類 --------------------------------------------------------------------------------------------
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$url = '';

		$data['column_action'] = $this->language->get('column_action');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['show_return_button'] = isset( $this->request->get['sel_mainOption']) ? true : "" ; // add by Angus 2017.11.12 確認顯示返回按鈕

		// 分頁功能 -------------------------------------------------------------------------------------------
		$mainOptionArr = $this->model_site_option->getOptionItems() ;
		$data['mainOptionArr'] = $mainOptionArr ;

		$columnNames = array (
				"opt_name"			=> "名稱：",
				"opt_desc"			=> "說明：",
				"opt_short_desc"	=> "簡稱：",
				"opt_seq"			=> "排序：",
				"status"			=> "狀態：",
			) ;
		$data['columnNames'] = $columnNames ;
		$data['td_colspan'] = count( $columnNames) ;

		$filter_data = array(
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$selParentIndex = isset( $this->request->get['sel_mainOption']) ? $this->request->get['sel_mainOption'] : "0" ;
		$data['sel_mainOption'] = $selParentIndex ;

		// 列表頁的 rows data
		$data['results'] = $this->model_site_option->getOptionItems( $selParentIndex) ;

		$this->load->model('tool/image') ;
		foreach ($data['results'] as $iCnt => $tmpRow) {
			if (!empty($tmpRow) && is_file(DIR_IMAGE . $tmpRow['opt_short_desc'])) {
				$tmpRow['thumb'] = $this->model_tool_image->resize($tmpRow['opt_short_desc'], 100, 100);
			} else {
				$tmpRow['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
			}
			if ( $selParentIndex == 0)
				$tmpRow['gotoUrl'] = $this->url->link('site/option', 'token=' . $this->session->data['token'] . "&sel_mainOption={$tmpRow['idx']}" , true); ;
			$data['results'][$iCnt] = $tmpRow ;
		}

		// dump( $data['results']) ;
		$data['pagination'] = '' ;
		$data['indexDec'] = '' ;

		// 準備各位置資訊 ----------------------------------------------------------------------------------------
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('site/option_list', $data));
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function issetOption ( $optionDataArr){
		if ( trim( $optionDataArr['opt_name']) == '' ) {
			return false ;
		}
		if ( trim( $optionDataArr['opt_desc']) == '' ) {
			return false ;
		}
		return true ;
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function validateForm() {
		// 檢查是否有權限
		if (!$this->user->hasPermission('modify', 'site/option')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		// 檢查各項必填欄位 -----------------------------------------------------------------------------------------
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		if ( !isset( $this->request->post['optionchild'])) {
			$this->error['warning'] = "尚未填寫相關資料" ;
		}

		return !$this->error;
	}

	/**
	* 前置作業
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function preparation() {
		// 標題
		$data['heading_title'] = $this->language->get('heading_title') ;
		// 次標題
		$data['text_list'] = $this->language->get('text_list') ;
		$data['text_form'] = !isset($this->request->get['idx']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_confirm'] = $this->language->get('text_confirm') ;
		$data['text_enabled'] = $this->language->get('text_enabled') ;
		$data['text_disabled'] = $this->language->get('text_disabled') ;
		// 麵包屑
		$url = '';
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
			);
		$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('site/option', 'token=' . $this->session->data['token'] . $url, 'SSL')
			);

		// 列表頁 按鈕
		if ( isset( $this->request->get['sel_mainOption'])) {
			$url = "&sel_mainOption=" . $this->request->get['sel_mainOption'] ;
		}

		$data['url_add'] = $this->url->link('site/option/add', 'token=' . $this->session->data['token'] . $url, 'SSL') ;
		$data['url_delete'] = $this->url->link('site/option/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['url_cancel'] = $this->url->link('site/option', 'token=' . $this->session->data['token'] , true);
		if (!isset($this->request->get['idx'])) {
			$data['url_action'] = $this->url->link('site/option/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['url_action'] = $this->url->link('site/option/edit', 'token=' . $this->session->data['token'] . '&idx=' . $this->request->get['idx'] . $url, true);
		}

		$data['button_add'] = $this->language->get('text_add');
		$data['button_edit'] = $this->language->get('text_edit');
		$data['button_delete'] = $this->language->get('text_delete');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['tab_general'] = $this->language->get('tab_general');

		$data['token'] = $this->session->data['token'] ;

		// 提示訊息
		$data['error_warning'] = '';
		$data['success'] = '';
		return $data ;
	}

	/**
	* 前置作業
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	public function getOptionList() {
		$json = array();
		if (!empty($this->request->get['filter_name'])) {
			$this->load->model('site/option') ;

			$json = $this->model_site_option->getOptionItemsArr($this->request->get['filter_name']);
		}



		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

}