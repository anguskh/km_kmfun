<?php
class ControllerSitePickdown extends Controller {
	private $error = array() ;

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	public function __construct( $registry) {
		parent::__construct($registry) ;
	}

	/**
	 * 列表頁
	 */
	public function index() {
		$this->load->language('site/pickdown') ;
		$this->document->setTitle($this->language->get('heading_title')) ;

		$this->load->model('site/pickup') ;
		$this->getList() ;
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	public function add() {
		$this->load->language('site/pickdown') ;

		$this->document->setTitle($this->language->get('heading_title')) ;

		$this->load->model('site/pickdown') ;

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_site_pickdown->addInformation($this->request->post) ;

			$this->session->data['success'] = $this->language->get('text_success') ;

			$url = '' ;

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'] ;
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'] ;
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'] ;
			}

			$this->response->redirect($this->url->link('site/pickdown', 'token=' . $this->session->data['token'] . $url, true)) ;
		}

		$this->getForm() ;
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	public function edit() {
		$this->load->language('site/pickdown') ;
		$this->load->model('site/pickup');
		$this->load->model('sale/order') ;
		$this->load->model('site/cars') ;

		$this->document->setTitle($this->language->get('heading_title')) ;

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			// 整理取回資訊
			$postData = $this->request->post ;
			$postData['e_station'] = isset($postData['e_station'])?$postData['e_station']:'';
				// 整理簽名資訊
			$retSignFileName = $this->buildSignatureFile( $postData) ;
			// dump( $retSignFileName) ;
			unset( $postData['signNameBin2']) ;
			$postData['signature'] = $retSignFileName ;

			$this->load->model('site/option') ;
			$checkItemsArr = $this->getCarCheckItems() ;
			$postData['now_pickup'] = 'N';
			$carConditionInfoArr = $this->arrangeCarConditionInfo( $checkItemsArr, $postData) ;
			// 車輛狀態寫入資料庫
			$this->model_site_pickup->updateInformation($this->request->get['idx'], $postData, $carConditionInfoArr) ;

	// 以上完成
	//-----------------------------------------------------------------------------------------------------------------
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if( isset( $this->session->data['from_url'])) {
				$url = "&s={$this->session->data['from_station_id']}&mod={$this->session->data['from_mode']}" ;
				$linkStr = $this->session->data['from_url'] ;

				unset( $this->session->data['from_url']) ;
				unset( $this->session->data['from_station_id']) ;
				unset( $this->session->data['from_mode']) ;

				$this->response->redirect($this->url->link($linkStr, 'token=' . $this->session->data['token'] . $url, true));
			} else {
				$this->response->redirect($this->url->link('site/pickdown', 'token=' . $this->session->data['token'] . $url, true));
			}
		}

		$this->getForm() ;
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	public function delete() {
		$this->load->language('site/pickdown');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('site/pickdown');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {

			foreach ($this->request->post['selected'] as $useIdx) {
				$this->model_site_pickdown->delUseIdx($useIdx);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('site/pickdown', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function getList() {
		$data = $this->preparation() ;

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		// 日曆 include JS add by Angus 2017.11.10
		$this->document->addStyle( "view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css") ;
		$this->document->addScript( "view/javascript/jquery/datetimepicker/moment.js") ;
		$this->document->addScript( "view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js") ;

		$data['button_filter']	= $this->language->get('button_filter') ;
		$data['token']			= $this->session->data['token'] ;

		// 訊息類 --------------------------------------------------------------------------------------------
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		//
		$url = '';
		$data['now_urls'] =  "pickdown";

		// 搜尋   -------------------------------------------------------------------------------------------
		$sort	= is_null( $this->getRequest( 'get', 'sort'))	? 'create_date' : $this->getRequest( 'get', 'sort') ;
		$order	= is_null( $this->getRequest( 'get', 'order'))	? 'DESC' : $this->getRequest( 'get', 'order') ;
		$page	= is_null( $this->getRequest( 'get', 'page'))	? 1 : $this->getRequest( 'get', 'page') ;
		$defaultOrderIdByToday = date("Ymd") ;
		if ( isset( $this->request->get['filter'])) {
			$filter_order_id		= $this->getRequest( 'get', 'filter_order_id') ;
		} else {
			$tmp_o_id = $this->getRequest( 'get', 'filter_order_id');
			$filter_order_id		= !empty( $tmp_o_id ) ? $this->getRequest( 'get', 'filter_order_id') : $defaultOrderIdByToday ;
		}

		// dump( $this->request->get) ;
		$filter_order_type		= $this->getRequest( 'get', 'filter_order_type') ;
		$filter_customer		= $this->getRequest( 'get', 'filter_customer') ;
		$filter_order_status	= $this->getRequest( 'get', 'filter_order_status') ;
		$filter_mobile			= $this->getRequest( 'get', 'filter_mobile') ;
		$filter_station			= $this->getRequest( 'get', 'filter_station') ;
		$filter_date_out		= $this->getRequest( 'get', 'filter_date_out') ;
		$filter_date_modified	= "" ;

		$data['filter_order_id']		= $filter_order_id;
		$data['filter_order_type']		= $filter_order_type;
		$data['filter_customer']		= $filter_customer;
		$data['filter_order_status']	= $filter_order_status;
		$data['filter_mobile']			= $filter_mobile;
		$data['filter_station']			= $filter_station;
		$data['filter_date_out']		= $filter_date_out;
		$data['filter_date_modified']	= $filter_date_modified;
		// dump( $this->request->get) ;
		// 查尋條件參數
		if (isset($this->request->get['filter_order_id'])) {
			$url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
		}
		if (isset($this->request->get['filter_customer'])) {
			$url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
		}
		if (isset($this->request->get['filter_mobile'])) {
			$url .= '&filter_mobile=' . $this->request->get['filter_mobile'];
		}
		$urlForPage = $url ;

		// 列表頁欄位名稱
		$columnNames             = $this->getListColumnNames() ;
		$data['columnNames']     = $columnNames ;
		$data['td_colspan']      = count( $columnNames) ;
		$data['column_action']   = $this->language->get('column_action');
		$data['text_no_results'] = $this->language->get('text_no_results');

		// 分頁功能 -------------------------------------------------------------------------------------------
		$nowDay		= date('Y-m-d', strtotime('-5 day')) ;
		$plusOneDay	= date('Y-m-d', strtotime('+2 day')) ;

		$filter_data = array(
			'filter_order_id'     => $filter_order_id ,
			'filter_order_type'   => $filter_order_type ,
			'filter_order_status' => $filter_order_status ,
			'filter_customer'     => $filter_customer ,
			'filter_mobile'       => $filter_mobile ,
			'filter_station'      => $filter_station ,
			'filter_date_out'     => $filter_date_out ,
			'dateRange'    => "", // today, today+2
			'order_status' => "1",
			'start'        => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'        => $this->config->get('config_limit_admin')
		);
		$totalCnt = $this->model_site_pickup->getTotalCnt( $filter_data) ;

		$results  = $this->model_site_pickup->getLists( $filter_data) ;

		// 準備列表頁的 rows data
		$this->load->model('site/car_model') ;
		$carModelArr = $this->model_site_car_model->getCarSeedOption() ;
		// dump( $carModelArr) ;

		$data['results'] = array();
		foreach ($results as $result) {
			// dump( $result) ;
			$result['edit'] = $this->url->link('site/pickdown/edit',
					'token=' . $this->session->data['token'] . '&idx=' . $result['order_id'] . $url, true) ;
			$result['car_model'] = $carModelArr[ $result['car_model']] ; // add by Angus 2017.11.12 補上車型名稱
			// add by Angus 2019.09.15 顯示多台車號 start --------------------------------
			$otherCarArr = unserialize($result['other_car']) ;
			if ( count( $otherCarArr) > 1) {
				foreach ($otherCarArr as $cCnt => $cTmp) {
					$carNoTmp[] = $cTmp['car_no'] ;
				}
				$result['car_no'] = join( ', ', $carNoTmp) ;
			}
			// add by Angus 2019.09.15 顯示多台車號 End ----------------------------------

			$data['results'][] = $result ;
		}
		// dump( $data['results']) ;

		$pagination        = new Pagination();
		$pagination->total = $totalCnt;
		$pagination->page  = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url   = $this->url->link('site/pickup', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['indexDec'] = sprintf($this->language->get('text_pagination'),
				($totalCnt) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 :
				0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($totalCnt - $this->config->get('config_limit_admin'))) ?
				$totalCnt : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')),
				$totalCnt, ceil($totalCnt / $this->config->get('config_limit_admin')));

		// 取回交車站名稱
		$this->load->model('site/option') ;
		$stations             = $this->model_site_option->getOptionNameArr( '15', true) ;
		$order_types          = $this->model_site_option->getOptionItemsArr('8') ;	// 訂單類別
		$data['optStation']   = $stations ;
		$data['optOrderType'] = $order_types ;

		// 準備各位置資訊 ----------------------------------------------------------------------------------------
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('site/pickup_list', $data));
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function getForm() {
		$data = $this->preparation() ;
		// add css 汽車檢查表的圖
		$this->document->addStyle( "view/stylesheet/default.css") ;
		$this->document->addStyle( "view/stylesheet/bootstrap-select.css") ;
		$this->document->addScript( "view/javascript/bootstrap-select.js") ;

		// 日曆 include JS add by Angus 2017.11.10
		$this->document->addStyle( "view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css") ;
		$this->document->addScript( "view/javascript/jquery/datetimepicker/moment.js") ;
		$this->document->addScript( "view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js") ;

		$data['entry_title']          = $this->language->get('entry_title') ;
		$data['entry_description']    = $this->language->get('entry_description') ;
		$data['entry_online_date']    = $this->language->get('entry_online_date') ;
		$data['entry_status']         = $this->language->get('entry_status') ;
		$data['token']           = $this->session->data['token'];

		// form 表格內的錯誤訊息 ----------------------------------------------------------------------------------
		// dump( $this->error) ;
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'] ;
		} else {
			$data['error_warning'] = '' ;
		}

		if (isset($this->error['checkIsOrder'])) {
			$data['error_checkIsOrder'] = $this->error['checkIsOrder'] ;
		} else {
			$data['error_checkIsOrder'] = '' ;
		}

		if (isset($this->error['checkIsSign'])) {
			$data['error_noSign'] = $this->error['checkIsSign'];
		} else {
			$data['error_noSign'] = '' ;
		}

		// form 表格內的資料 --------------------------------------------------------------------------------------
		// dump( $this->request->get['idx']) ;
		// dump( $this->request->server['REQUEST_METHOD']) ;
		$this->load->model('site/option') ;
		$this->load->model('site/car_model') ;
		$this->load->model('site/minshuku') ;
		$this->load->model('site/scheduling') ;

		//查業務人員下拉19
		$data["userList"] = $this->model_site_option->getUserList('19,22', $this->user->getUserName()) ;
		$data["nowname"] = "";
		foreach($data["userList"] as $arr){
			if($arr['username'] == $this->user->getUserName()){
				$data["nowname"] = $arr['lastname'].$arr['firstname'];
			}
		}
		if ( isset($this->request->get['idx'])) {
			$rowInfo = $this->model_site_pickup->getInformation($this->request->get['idx']) ;
		}
		$data['rentInfo'] = $rowInfo ;
		$data['check_name'] = $rowInfo['check_name'];
		$data['user_allname'] = $rowInfo['add_name'];
		$data['add_time'] = $rowInfo['add_time'];
		$this->load->model('tool/image');

		$path = 'checkcar/'.$rowInfo['agreement_no'].'/';

		//image
		for($i=1; $i<=4; $i++){
			if (!empty($rowInfo) && is_file(DIR_IMAGE .$path.$rowInfo['image'.$i] )) {
				$data['thumb'.$i] = $this->model_tool_image->resize($path.$rowInfo['image'.$i], 250, 250);
			}else{
				$data['thumb'.$i] = $this->model_tool_image->resize('no_image.png', 250, 250);
			}
		}
		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 250, 250);

		$this->session->data['from_station_id'] = $rowInfo['s_station'] ;
		preg_match("/common\/dashboard/", $this->request->server['HTTP_REFERER'], $output_array);
		// dump( $output_array) ;
		if ( $output_array) {
		// http://localhost/kmfun/admin/?route=common/dashboard&token=XK49R8yxGIYwILLl001nkGc5Im17OuFM
			$this->session->data['from_url']     = $output_array[0] ;
			$this->session->data['from_mode']    = "pickdown" ;

			$url = "&s={$this->session->data['from_station_id']}&mod={$this->session->data['from_mode']}" ;
			$linkStr = $this->session->data['from_url'] ;

			$data['url_cancel'] = $this->url->link($linkStr, 'token=' . $this->session->data['token'] . $url, true) ;
		}

		// 1. 租車資訊
		$order_types         = $this->model_site_option->getOptionItemsArr('8') ;	// 訂單類別
		$data['order_types'] = $order_types ;
		if ( $rowInfo['order_type'] == 10) {	// 民宿業者
			$retArr = $this->model_site_minshuku->getInformation( $rowInfo['minsu_idx']) ;
			$data['minsu_name'] = ((isset($retArr['name']))?$retArr['name']:'') ;
			$data['minsuShow']  = "" ;
		} else {
			$data['minsuShow']  = 'style="display:none"' ;
		}
		$data['stations']    = $this->model_site_option->getOptionItemsArr('15') ;	// 分店名稱
		$data['car_models']  = $this->model_site_option->getOptionItemsArr('1') ;	// 車種分類
		$data['carSeedInfo'] = $this->model_site_car_model->getInformation( $rowInfo['car_model']) ;

		// 2. 配件資訊
		$data['gps']            = !empty( $rowInfo['gps']) ? $rowInfo['gps'] : 0 ;
		$data['baby_chair']     = !empty( $rowInfo['baby']) ? $rowInfo['baby'] : 0 ;
		$data['children_chair'] = !empty( $rowInfo['children']) ? $rowInfo['children'] : 0 ;


		// 4. 航船資訊
		$flightInfo = $this->model_site_scheduling->getFlightInfo($rowInfo['agreement_no']) ;
		// dump( $flightInfo) ;
		$data['flightInfo'] = $flightInfo;
		// 取得航空公司名稱選項
		$airlineArr = $this->model_site_option->getOptionItemsAllColArr( '125') ;
		$data['airlineArr'] = $airlineArr ;
		$data['airlineArr_rt'] = $airlineArr ;
		$airportArr = $this->model_site_option->getOptionItemsAllColArr( '134') ;
		$data['airportArr'] = $airportArr ;
		$data['airportArr_rt'] = $airportArr ;
		// 取得碼頭出發地名稱選項
		$pierArr = $this->model_site_option->getOptionItemsAllColArr( '131') ;
		$data['pierArr'] = $pierArr ;
		$data['pierArr_rt'] = $pierArr ;
		$airlineArr = array();
		$airlineArr_rt = array();
		$pierArr = array();
		$pierArr_rt = array();

		for($i=0; $i<2; $i++){
			$rt = "";
			if($i==1){
				$rt = "_rt";
			}
			if ( count( $flightInfo)) {
				// 取得航空公司名稱選項
				foreach ($airlineArr as $key => $tmpRow) {
					$airlineArr[$tmpRow['idx']] = $tmpRow['opt_desc'] ;
					$airlineArr_rt[$tmpRow['idx']] = $tmpRow['opt_desc'] ;
					unset( $airlineArr[$key]) ;
					unset( $airlineArr_rt[$key]) ;
				}
				// 取得碼頭出發地名稱選項

				foreach ($pierArr as $key => $tmpRow) {
					$pierArr[$tmpRow['idx']] = $tmpRow['opt_desc'] ;
					$pierArr_rt[$tmpRow['idx']] = $tmpRow['opt_desc'] ;
					unset( $pierArr[$key]) ;
					unset( $pierArr_rt[$key]) ;
				}

				if ( $flightInfo['trans_type'.$rt] == 0) {
					$ttArr = array( "", "") ;
				} else {
					$ttArr = ( $flightInfo['trans_type'.$rt] == 1) ? array( "checked", "")  : array( "", "checked") ;
				}

				$data['trans_type'.$rt] = $ttArr ;

				$data['flight_sDate'.$rt] = ( $flightInfo['s_date'.$rt] == "0000-00-00 00:00:00") ? "" : substr($flightInfo['s_date'.$rt], 0, strlen($flightInfo['s_date'.$rt]) -3 ) ;
				$data['flight_eDate'.$rt] = ( $flightInfo['e_date'.$rt] == "0000-00-00 00:00:00") ? "" : substr($flightInfo['e_date'.$rt], 0, strlen($flightInfo['e_date'.$rt]) -3 ) ;

				$data['pierShow'.$rt]     = isset( $pierArr[$flightInfo['pier'.$rt]]) ? $pierArr[$flightInfo['pier'.$rt]] : "" ;
				$data['airlineShow'.$rt]  = (isset( $airlineArr[$flightInfo['airline'.$rt]]) && $flightInfo['airline'.$rt] != 0) ? $airlineArr[$flightInfo['airline']] : "" ;
				$data['flight_no'.$rt]    = $flightInfo['flight_no'.$rt] ;
				$data['pier'.$rt]    = $flightInfo['pier'.$rt];
				$data['airline'.$rt]    = $flightInfo['airline'.$rt];
				$data['airport'.$rt]    = $flightInfo['airport'.$rt];
			} else {
				$data['trans_type'.$rt] = array( "", "") ;

				$data['flight_sDate'.$rt] = "" ;
				$data['flight_eDate'.$rt] = "" ;
				$data['pierShow'.$rt]     = "" ;
				$data['airlineShow'.$rt]  = "" ;
				$data['flight_no'.$rt]    = "" ;
				$data['pier'.$rt]    = "";
				$data['airline'.$rt]    = "";
				$data['airport'.$rt]    = "";

			}

			if(isset($flightInfo['trans_type'.$rt])) {
				if ( $flightInfo['trans_type'.$rt] == 1) {
					$data['airlineShow'.$rt] = "inline" ;
					$data['flightnoShow'.$rt] = "inline" ;
					$data['pierShow'.$rt] = "none" ;
				} else if ( $flightInfo['trans_type'.$rt] == 2) {
					$data['airlineShow'.$rt] = "none" ;
					$data['flightnoShow'.$rt] = "none" ;
					$data['pierShow'.$rt] = "inline" ;
				}else{
					$data['airlineShow'.$rt] = "none" ;
					$data['flightnoShow'.$rt] = "none" ;
					$data['pierShow'.$rt] = "none" ;
				}
			} else {
				$data['airlineShow'.$rt] = "none" ;
				$data['flightnoShow'.$rt] = "none" ;
				$data['pierShow'.$rt] = "none" ;

			}
		}


		// todo 檢查other_car欄位 如非空值則做轉換
		if ( empty( $data['rentInfo']['other_car'])) {
			$data['rentInfo']['other_car'] = array(
				array(
					'idx' => $data['rentInfo']['car_sn'],
					'car_seed' => $data['rentInfo']['car_model'],
					'car_no' => $data['rentInfo']['car_no'],
					'car_color' => '',
					'online_date' => '',
					'status' => '',
				)
			) ;

		} else {
			$data['rentInfo']['other_car'] = unserialize( $data['rentInfo']['other_car']) ;
		}
		// dump( $data['rentInfo']) ;
		//改用訂單管理的呈現方式
		$this->rowInfo = $this->model_sale_order->getOrder($this->request->get['idx']) ;
		$memoInfo = $this->model_site_scheduling->getInformationmemo($this->rowInfo['agreement_no']) ;
		$data['area_memo'] = isset($memoInfo['memo'])?$memoInfo['memo']:'' ;
		// dump( $this->retCarNoOption()) ;
		list( $data['ckbCarNoHtml'], $car_seed, $car_total) = $this->retCarNoOption() ;

		// 分離租還車時間
		if (!empty( $this->rowInfo['rent_date_out'])) {
			$tmpTime = $this->rowInfo['rent_date_out'] ;
		} else {
			$tmpTime = ' ';
		}
		list( $strDate, $strTime)       = explode(" ", $tmpTime) ;
		$data['rent_date_out']          = $strDate ;
		$data['rent_date_out_time']     = strlen( $strTime) > 5 ? substr( $strTime, 0, strlen( $strTime) - 3) : $strTime ;
		if (!empty( $this->rowInfo['rent_date_in_plan'])) {
			$tmpTime = $this->rowInfo['rent_date_in_plan'] ;
		} else {
			$tmpTime = ' ';
		}
		list( $strDate, $strTime)       = explode(" ", $tmpTime) ;
		$data['rent_date_in_plan']      = $strDate ;
		$data['rent_date_in_plan_time'] = strlen( $strTime) > 5 ? substr( $strTime, 0, strlen( $strTime) - 3) : $strTime ;

		// dump( $data) ;
		// 計算使用天數
		if ( !empty($data['rent_date_out']) && !empty($data['rent_date_in_plan'])) {
			$useSecond        = floor( ( strtotime( $data['rent_date_in_plan']. " " .$data['rent_date_in_plan_time']) - strtotime( $data['rent_date_out']. " " .$data['rent_date_out_time']))) ;
			$data['rentDay']  = floor( $useSecond / 86400) ;	// 24hr * 60分 * 60秒
			$data['rentHour'] = ( $useSecond % 86400) / 3600 ;	//
			//未滿1天要算1天 2020/08/27 jie
			if($data['rentDay'] == 0){
				$data['rentDay'] = 1;
				$data['rentHour']  = 0;
			}
		} else {
			$data['rentDay']  = "" ;
			$data['rentHour'] = "" ;
		}

		// rent time Array 07:30
		$rentTimeArr = $this->model_site_option->getRentTime() ;
		$data['rentTimeOptArr'] = $rentTimeArr ;


		// dump( $this->retCarNoOption()) ;
		list( $data['ckbCarNoHtml'], $car_seed, $car_total) = $this->retCarNoOption() ;

		$data['car_seed']  = $car_seed ;
		$data['car_seeds'] = $this->model_site_car_model->getCarModelLists( $car_seed) ;
		$data['car_total']  = $car_total ;

		foreach ($data['car_seeds'] as $i => $tmp) {
			$data['car_model'][] = $tmp['car_model'] ;
		}

		// 取得車輛檢查表狀態
		$this->load->model('site/option') ;
		// 參考 選項管理清單 -> 車輛外觀狀態
		$data['ext_status']		= $this->getCarStatusItems() ;
		// dump( $data['ext_status']) ;
		$data['car_checkItems']	= $this->getCarCheckItems() ;
		// dump( $data['car_checkItems']) ;

		// dump( $data['rentInfo']) ; // agreement_no
		$retArr = $this->getCarConditionInfo( $data['rentInfo']['other_car'], $data['rentInfo']['agreement_no']) ;
		$data['car_checkInfos'] = $retArr[0] ;
		$data['signature'] = $retArr[1] ;
		// dump( $retArr[1]) ;

		$data['header']      = $this->load->controller('common/header') ;
		$data['column_left'] = $this->load->controller('common/column_left') ;
		$data['footer']      = $this->load->controller('common/footer') ;
		$data['now_pickup']  = "N" ;

		$this->response->setOutput($this->load->view('site/pickup_form', $data)) ;
     }

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function validateForm() {
		// 檢查是否有權限
		if (!$this->user->hasPermission('modify', 'site/pickdown')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		// dump( $this->request->get) ;
		// dump( $this->request->post) ;

		$order_id = $this->request->post['order_id'] ;
		$agreement_no = $this->request->post['agreement_no'] ;
		$checkIsOrder =  $this->model_site_pickup->checkIsOrder( $order_id, $agreement_no) ;

		if ( !$checkIsOrder) {
			$this->error['checkIsOrder'] = "訂單不存在，請連絡系統管理員" ;
		}

		// 檢查簽名 車有幾台，簽名就要幾個
		// 暫時不提供該功能 add by Angus 2018.05.12
		// foreach ($this->request->post['signNameBin'] as $iCnt => $signBin) {
		// 	if ( empty( $signBin)) {
		// 		$this->error['checkIsSign'] = "尚未做簽名確認" ;
		// 	}
		// }
		return !$this->error;
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'site/pickdown')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('site/pickdown');

		foreach ($this->request->post['selected'] as $information_id) {
			if ($this->config->get('config_account_id') == $information_id) {
				$this->error['warning'] = $this->language->get('error_account');
			}

			if ($this->config->get('config_checkout_id') == $information_id) {
				$this->error['warning'] = $this->language->get('error_checkout');
			}

			if ($this->config->get('config_affiliate_id') == $information_id) {
				$this->error['warning'] = $this->language->get('error_affiliate');
			}

			if ($this->config->get('config_return_id') == $information_id) {
				$this->error['warning'] = $this->language->get('error_return');
			}

			$store_total = $this->model_setting_store->getTotalStoresByInformationId($information_id);

			if ($store_total) {
				$this->error['warning'] = sprintf($this->language->get('error_store'), $store_total);
			}
		}

		return !$this->error;
	}

	/**
	 * [getListColumnNames description]
	 * @return [array] 列表頁欄位名稱
	 * add by Angus 2017.09.13
	 */
	protected function getListColumnNames () {
		return array(
				"agreement_no"		=> "訂單編號",
				"car_model"			=> "車型",
				"car_no"			=> "車號",
				"rent_date_out" 	=> "取車日期",
				"rent_date_in_plan" => "預計還車日期",
				"rent_user_name"	=> "訂車人",
				"rent_user_tel"		=> "連絡電話",
				"get_user_name"		=> "取車人",
				"get_user_tel"		=> "取車人電話",
			) ;
	}

	/**
	 * [getCarStatusItems 車輛檢查表 車輛各部件狀態名稱]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2017-11-12
	 */
	protected function getCarStatusItems() {
		$results = $this->model_site_option->getOptionItemsAllColArr( 20) ;
		return $results ;
	}

	/**
	 * [getCarCheckItems 車輛檢查表 車輛各部件列表]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2017-11-12
	 */
	protected function getCarCheckItems() {
		$results = $this->model_site_option->getOptionItemsAllColArr( 33) ;

		$Items = array() ;
		foreach ( $results as $i => $result) {
			$Items[$result['opt_desc']][$result['idx']] = $result['opt_name'] ;
		}
		foreach ( $Items as $key => $value) {
			asort( $value) ;
			$Items[ $key] = $value ;
		}

		return $Items ;
	}

	/**
	 * [arrangeCarConditionInfo 整理車輛外觀狀態]
	 * @param   [type]     $checkItemsArr [description]
	 * @param   [type]     $data          [description]
	 * @return  [type]                    [description]
	 * @Another Angus
	 * @date    2017-11-14
	 */
	protected function arrangeCarConditionInfo( $checkItemsArr, $data) {
		$retArr = array() ;
		$car_snArr = $data['car_sn'] ;
		foreach ($car_snArr as $car_sn) {
			foreach ($checkItemsArr as $lv1_key => $lv1_Arr) {
				foreach ($lv1_Arr as $lv2_key => $tmpStr) {
					$dataKeyStr = "{$car_sn}_{$lv2_key}" ;
					if ( isset($data[$dataKeyStr]) && count( $data[$dataKeyStr]) > 1) {
						$indexKey = array_search( 21, $data[$dataKeyStr]) ;
						if ( $indexKey === 0) array_shift( $data[$dataKeyStr]) ;
					}
					$retArr[$car_sn][$lv1_key][$lv2_key] = ((isset($data[$dataKeyStr]))?$data[$dataKeyStr]:'') ;
				}
			}
		}
 		return $retArr ;
	}

	/**
	 * [getCarConditionInfo 還車作業 取回交車時的狀態]
	 * @param   array      $carInfoArr   [車輛清單]
	 * @param   string     $agreement_no [交車時訂單編號]
	 * @return  [type]                   [傳回 交車時車輛狀態]
	 * @Another Angus
	 * @date    2017-11-17
	 */
	protected function getCarConditionInfo( $carInfoArr = array(), $agreement_no = "") {
		$carCheckInfoArr = array() ;
		$signatureArr = array() ;

		foreach ($carInfoArr as $iCnt => $carInfo) {
			$car_sn = $carInfo['idx'] ;
			$infoArr = $this->model_site_pickup->getCarConditionInfoPickup( $car_sn, $agreement_no) ;
			// todo 需要將上次出租的狀態帶出來
			$results = isset( $infoArr['condition_before']) ? json_decode($infoArr['condition_before'], true) : array();

			foreach ($results as $aKey => $tmp) {
				$carCheckInfoArr[$aKey][$car_sn] = $tmp ;
			}
			$inputStr = isset( $infoArr['sign_out']) ? substr( $infoArr['sign_out'], 0, 2) . "/{$infoArr['sign_out']}" : "" ;
			$signatureArr[] = $inputStr ;
		}


		return array( $carCheckInfoArr, $signatureArr) ;
	}

	/**
	 * [buildSignatureFile description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2017-11-24
	 */
	protected function buildSignatureFile( $data = array()) {
		// $guid = $this->guid->getGUID( false) ;

		$retArr = array() ;
		$mainPath = DIR_APPLICATION . "view/image/signature/" ;

		foreach ($data['signNameBin2'] as $iCnt => $tmp) {
			$guid = $this->guid->getGUID( false) ;
			$subPath = substr( $guid, 0, 2) ;
			// 建立目錄
			if ( !file_exists( $mainPath . $subPath)) {
				mkdir( $mainPath . $subPath, 0755) ;
			}

			$imgUrl = str_replace('data:image/png;base64,', '', $tmp) ;
			if( !empty( $imgUrl)) {
				$imgUrl = base64_decode( $imgUrl) ;
				// 寫兩份
				$pathFileName = "{$mainPath}{$subPath}/{$guid}.png" ;
				file_put_contents($pathFileName, $imgUrl) ;
				$pathFileName = "{$mainPath}{$guid}.png" ;
				file_put_contents($pathFileName, $imgUrl) ;
				$retArr[] = "{$guid}.png" ;
			}
		}
		return $retArr ;
	}

	/**
	* 前置作業
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function preparation() {
		// 標題
		$data['heading_title'] = $this->language->get('heading_title') ;
		// 次標題
		$data['text_list'] = $this->language->get('text_list') ;
		$data['text_form'] = !isset($this->request->get['idx']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_confirm'] = $this->language->get('text_confirm') ;
		$data['text_enabled'] = $this->language->get('text_enabled') ;
		$data['text_disabled'] = $this->language->get('text_disabled') ;
		// 麵包屑
		$url = '';
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
			);
		$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('site/pickdown', 'token=' . $this->session->data['token'] . $url, 'SSL')
			);

		// 列表頁 按鈕
		$data['url_add'] = $this->url->link('site/pickdown/add', 'token=' . $this->session->data['token'] . $url, 'SSL') ;
		$data['url_delete'] = $this->url->link('site/pickdown/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['url_cancel'] = $this->url->link('site/pickdown', 'token=' . $this->session->data['token'] . $url, true);
		if (!isset($this->request->get['idx'])) {
			$data['url_action'] = $this->url->link('site/pickdown/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['url_action'] = $this->url->link('site/pickdown/edit', 'token=' . $this->session->data['token'] . '&idx=' . $this->request->get['idx'] . $url, true);
		}

		$data['button_add'] = $this->language->get('text_add');
		$data['button_edit'] = $this->language->get('text_edit');
		$data['button_delete'] = $this->language->get('text_delete');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['tab_general'] = $this->language->get('tab_general');

		// 提示訊息
		$data['error_warning'] = '';
		$data['success'] = '';
		return $data ;
	}
	// add by Angus 2017.07.09
	protected function getRequest( $method, $index) {
		switch ( $method) {
			case 'get':
				return isset( $this->request->get[$index]) ? $this->request->get[$index] : null ;
				break ;
			case 'post':
				return isset( $this->request->post[$index]) ? $this->request->post[$index] : null ;
				break ;
			default :
				return "";
				break ;
		}
	}

	/**
	 * [retCarNoOption 取得 牌照號碼 的option選項]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-06-03
	 */
	protected function retCarNoOption( ) {
		$ckbCarNoHtml = "" ;
		$selCars = array() ;

		// 簡潔取法
		// $filter['car_seed'] = join( '|', $this->setDataInfo( 'post', '', 'selCarTypes')) ;
		if ( isset( $this->request->post['selCarTypes'])) {
			$carSeeds = $this->request->post['selCarTypes'] ;
			$filter['car_seed'] = join( '|', $this->request->post['selCarTypes']) ;
		}

		// 另外多動作的取法 ---------------------------------------------------------------------------------------------------
		// 出車時間
		if ( isset($this->request->post['rent_date_out']) && isset($this->request->post['out_time'])) {
			$filter['s'] = $this->request->post['rent_date_out'] . " " . $this->request->post['out_time'] ;
		} else if ( isset( $this->rowInfo['rent_date_out'])) {
			$filter['s'] = $this->rowInfo['rent_date_out'] ;
		}
		// 還車時間
		if ( isset($this->request->post['rent_date_in_plan']) && isset($this->request->post['plan_time'])) {
			$filter['e'] = $this->request->post['rent_date_in_plan'] . " " . $this->request->post['plan_time'] ;
		} else if ( isset( $this->rowInfo['rent_date_in_plan'])) {
			$filter['e'] = $this->rowInfo['rent_date_in_plan'] ;
		}
		// 選用車輛
		$carSeeds = array() ;

		if ( isset( $this->request->post['useCars'])) {
			$selCars = $this->request->post['useCars'] ;
			$carSeeds = $this->request->post['selCarTypes'] ;

			$filter['car_sn'] = $selCars ;
			$filter['car_seed'] = join( '|', $carSeeds) ;
		} else if ( isset( $this->rowInfo['other_car'])) {
			$carsArr = unserialize( $this->rowInfo['other_car']) ;
			foreach ($carsArr as $iCnt => $tmpCar) {
				$selCars[]  = $tmpCar['idx'] ;
				$carSeeds[] = $tmpCar['car_seed'] ;
			}
			$filter['car_sn']   = $selCars ;
			$filter['car_seed'] = join( '|', $carSeeds) ;
		}

		$car_total = 0;
		if ( isset( $filter['s'])) {
			$carNoOption = $this->model_site_scheduling->getCarNoForAjax( $filter) ;
			$arrange = array() ;
			foreach ($carNoOption as $tmp) {
				$arrange[$tmp['car_seed']][] = $tmp;
			}
			$ckbCarNoHtml .= "<ul>" ;

			foreach ($arrange as $seedName => $seeds) {
				$ckbCarNoHtml .= "<li><p class=\"brand-class\">{$seedName}</p>" ;
				foreach ($seeds as $tmp) {
					if(in_array( $tmp['idx'], $selCars)){
						$car_total++;
					}
					if(in_array( $tmp['idx'], $selCars)){
						$chkedStr = in_array( $tmp['idx'], $selCars) ? "checked" : "" ;
						$labelClassStr = in_array( $tmp['idx'], $selCars) ? "" : "bt-background-clean"  ;
						$ckbCarNoHtml .= "<label name=\"useCar\" class=\"btn btn-success {$labelClassStr}\" >
						<i class=\"fa fa-car\"></i>&nbsp;{$tmp['car_no']}
						<input class=\"hidden\" type=\"checkbox\" name=\"useCars[]\" value=\"{$tmp['idx']}\" p2=\"{$tmp['car_price']}\" {$chkedStr}/>
						</label>" ;
					}
				}
				$ckbCarNoHtml .= "</li>" ;
			}
			$ckbCarNoHtml .= "</ul>" ;


		}

		return array( $ckbCarNoHtml, $carSeeds, $car_total) ;
	}

}