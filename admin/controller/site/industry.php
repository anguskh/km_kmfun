<?php
class ControllerSiteIndustry extends Controller {
	private $error = array() ;

	/**
	 * [__construct description]
	 * @param   [type]     $registry [description]
	 * @Another Angus
	 * @date    2018-04-30
	 */
	public function __construct( $registry) {
		parent::__construct($registry) ;
	}

	/**
	 * [index 列表]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-04-30
	 */
	public function index() {
		$this->load->language('site/industry') ;
		$this->document->setTitle($this->language->get('heading_title')) ;

		$this->load->model('site/industry') ;
		$this->getList() ;
	}

	/**
	 * [add 新增]
	 * @Another Angus
	 * @date    2018-05-01
	 */
	public function add() {
		$this->load->language('site/industry') ;

		$this->document->setTitle($this->language->get('heading_title')) ;

		$this->load->model('site/industry') ;

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_site_industry->addInformation($this->request->post) ;

			$this->session->data['success'] = $this->language->get('text_success') ;

			$url = '' ;

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'] ;
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'] ;
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'] ;
			}

			$this->response->redirect($this->url->link('site/industry', 'token=' . $this->session->data['token'] . $url, true)) ;
		}

		$this->getForm() ;
	}

	/**
	 * [edit description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-05-01
	 */
	public function edit() {
		$this->load->language('site/industry');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('site/industry');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_site_industry->editInformation($this->request->get['idx'], $this->request->post);
			// $this->model_site_industry->addInformation( $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('site/industry', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	public function delete() {
		$this->load->language('site/industry');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('site/industry');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $useIdx) {
				$this->model_site_industry->delUseIdx($useIdx);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('site/industry', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	/**
	 * [getList 列表]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-05-01
	 */
	protected function getList() {
		$data = $this->preparation() ;
		// search area 搜尋類 ----------------------------------------------------------------------------------
		$data['entry_name']     = "業者名稱" ;
		$data['entry_man_name'] = "連絡人姓名" ;
		$data['entry_tel']      = "連絡人電話" ;
		$data['entry_mail']     = "連絡人Mail" ;
		$data['entry_address']  = "連絡人地址" ;

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}
		if (isset($this->request->get['filter_man_name'])) {
			$filter_man_name = $this->request->get['filter_man_name'];
		} else {
			$filter_man_name = null;
		}
		if (isset($this->request->get['filter_tel'])) {
			$filter_tel = $this->request->get['filter_tel'];
		} else {
			$filter_tel = null;
		}
		if (isset($this->request->get['filter_address'])) {
			$filter_address = $this->request->get['filter_address'];
		} else {
			$filter_address = null;
		}

		// order by | page
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'] ;
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'] ;
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'] ;
		} else {
			$page = 1;
		}

		// 訊息類 --------------------------------------------------------------------------------------------
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8')) ;
		}
		if (isset($this->request->get['filter_man_name'])) {
			$url .= '&filter_man_name=' . urlencode(html_entity_decode($this->request->get['filter_man_name'], ENT_QUOTES, 'UTF-8')) ;
		}
		if (isset($this->request->get['filter_tel'])) {
			$url .= '&filter_tel=' . urlencode(html_entity_decode($this->request->get['filter_tel'], ENT_QUOTES, 'UTF-8')) ;
		}
		if (isset($this->request->get['filter_address'])) {
			$url .= '&filter_address=' . urlencode(html_entity_decode($this->request->get['filter_address'], ENT_QUOTES, 'UTF-8')) ;
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		$hrefArr = array() ;
		$hrefArr['name']     = $this->url->link('site/industry', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
		$hrefArr['man_name'] = $this->url->link('site/industry', 'token=' . $this->session->data['token'] . '&sort=man_name' . $url, true);
		$hrefArr['tel']      = $this->url->link('site/industry', 'token=' . $this->session->data['token'] . '&sort=tel' . $url, true);
		$hrefArr['email']    = $this->url->link('site/industry', 'token=' . $this->session->data['token'] . '&sort=email' . $url, true);
		$hrefArr['address']  = $this->url->link('site/industry', 'token=' . $this->session->data['token'] . '&sort=address' . $url, true);
		$data['hrefArr']     = $hrefArr ;

		$data['token']           = $this->session->data['token'];
		$data['filter_name']     = $filter_name ;
		$data['filter_man_name'] = $filter_man_name ;
		$data['filter_tel']      = $filter_tel ;
		$data['filter_address']  = $filter_address ;


		// 分頁功能 -------------------------------------------------------------------------------------------
		$columnNames = $this->model_site_industry->getColumns() ;
		$data['columnNames'] = $columnNames ;
		$data['td_colspan'] = count( $columnNames) ;

		$filter_data = array(
			'filter_name'     => $filter_name ,
			'filter_man_name' => $filter_man_name ,
			'filter_tel'      => $filter_tel ,
			'filter_address'  => $filter_address ,

			'sort'            => $sort,
			'order'           => $order,

			'start'           => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'           => $this->config->get('config_limit_admin')
		);
		$totalCnt = $this->model_site_industry->getTotalCnt( $filter_data) ;

		$results = $this->model_site_industry->getLists( $filter_data) ;

		// 列表頁的 rows data
		$data['results'] = array();
		foreach ($results as $result) {
			// dump( $result) ;
			$result['edit'] = $this->url->link('site/industry/edit',
					'token=' . $this->session->data['token'] . '&idx=' . $result['idx'] . $url, true) ;

			$data['results'][] = $result ;
		}

		if ( strpos(" ".$url, "&order=DESC")) {
			$url = str_replace( "&order=DESC", "&order=ASC", $url) ;
		} else {
			$url = str_replace( "&order=ASC", "&order=DESC", $url) ;
		}

		$pagination = new Pagination();
		$pagination->total = $totalCnt;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		// $pagination->limit = 5 ;
		$pagination->url = $this->url->link('site/industry', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);
		$data['pagination'] = $pagination->render();

		$data['indexDec'] = sprintf($this->language->get('text_pagination'),
				($totalCnt) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 :
				0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($totalCnt - $this->config->get('config_limit_admin'))) ?
				$totalCnt : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')),
				$totalCnt, ceil($totalCnt / $this->config->get('config_limit_admin')));

		$data['sort']            = $sort ;
		$data['order']           = $order ;
		$data['column_action']   = $this->language->get('column_action');
		$data['text_no_results'] = $this->language->get('text_no_results');

		// 準備各位置資訊 ----------------------------------------------------------------------------------------
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('site/industry_list', $data));
	}

	/**
	 * [getForm 表單]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-05-01
	 */
	protected function getForm() {
		$data = $this->preparation() ;

		$data['entry_name']     = "業者名稱" ;
		$data['entry_man_name'] = "連絡人姓名" ;
		$data['entry_tel']      = "連絡人電話" ;
		$data['entry_mail']     = "連絡人Mail" ;
		$data['entry_address']  = "連絡人地址" ;
		$data['entry_status'] = $this->language->get('entry_status') ; // 頁面狀態

		if (isset($this->request->get['idx']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$this->rowInfo = $this->model_site_industry->getInformation($this->request->get['idx']) ;
			$data['input_idx'] = $this->rowInfo['idx'] ;
		}

		// form 表格內的錯誤訊息 ----------------------------------------------------------------------------------
		// add by Angus 2018.05.01
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'] ;
		} else {
			$data['error_warning'] = '' ;
		}
		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}
		if (isset($this->error['man_name'])) {
			$data['error_man_name'] = $this->error['man_name'];
		} else {
			$data['error_man_name'] = '';
		}
		if (isset($this->error['tel'])) {
			$data['error_tel'] = $this->error['tel'];
		} else {
			$data['error_tel'] = '';
		}

		// form 表格內的資料 --------------------------------------------------------------------------------------
		// by Angus 2018.05.01
		$data['input_name']     = $this->setDataInfo( 'post', 'input', 'name') ;
		$data['input_man_name'] = $this->setDataInfo( 'post', 'input', 'man_name') ;
		$data['input_tel']      = $this->setDataInfo( 'post', 'input', 'tel') ;
		$data['input_email']    = $this->setDataInfo( 'post', 'input', 'email') ;
		$data['input_address']  = $this->setDataInfo( 'post', 'input', 'address') ;
		// dump( $data) ;

		// 頁面狀態
		if (isset($this->request->post['sel_status'])) {
			$data['sel_status'] = $this->request->post['sel_status'] ;
		} elseif (!empty($rowInfo)) {
			$data['sel_status'] = $rowInfo['status'] ;
		} else {
			$data['sel_status'] = '1' ;
		}


		$data['header'] = $this->load->controller('common/header') ;
		$data['column_left'] = $this->load->controller('common/column_left') ;
		$data['footer'] = $this->load->controller('common/footer') ;

		$this->response->setOutput($this->load->view('site/industry_form', $data)) ;
     }

    /**
     * [validateForm 資料檢核]
     * @return  [type]     [description]
     * @Another Angus
     * @date    2018-05-01
     */
	protected function validateForm() {
		// 檢查是否有權限
		if (!$this->user->hasPermission('modify', 'site/industry')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		// 檢查各項必填欄位 -----------------------------------------------------------------------------------------
		// 業者名稱
		if ((utf8_strlen($this->request->post['input_name']) < 2) || (utf8_strlen($this->request->post['input_name']) > 64)) {
			$this->error['name'] = $this->language->get('error_name') ;
		}
		// 連絡人姓名
		if (utf8_strlen($this->request->post['input_man_name']) < 2 || (utf8_strlen($this->request->post['input_name']) > 15)) {
			$this->error['man_name'] = $this->language->get('error_man_name') ;
		}
		// 連絡人電話
		if (utf8_strlen($this->request->post['input_tel']) < 2 || (utf8_strlen($this->request->post['input_name']) > 15)) {
			$this->error['tel'] = $this->language->get('error_tel') ;
		}


		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		return !$this->error;
	}

	/**
	* 新增 / 修改 頁面
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'site/industry')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('site/industry');

		return !$this->error;
	}

	/**
	* 前置作業
	* @param  [type] $method [description]
	* @return [type]         [description]
	*/
	protected function preparation() {
		// 標題
		$data['heading_title'] = $this->language->get('heading_title') ;
		// 次標題
		$data['text_list'] = $this->language->get('text_list') ;
		$data['text_form'] = !isset($this->request->get['idx']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_confirm'] = $this->language->get('text_confirm') ;
		$data['text_enabled'] = $this->language->get('text_enabled') ;
		$data['text_disabled'] = $this->language->get('text_disabled') ;
		// 麵包屑
		$url = '';
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
			);
		$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('site/industry', 'token=' . $this->session->data['token'] . $url, 'SSL')
			);

		// 列表頁 按鈕
		$data['url_add'] = $this->url->link('site/industry/add', 'token=' . $this->session->data['token'] . $url, 'SSL') ;
		$data['url_delete'] = $this->url->link('site/industry/delete', 'token=' . $this->session->data['token'] . $url, true);
		$data['url_cancel'] = $this->url->link('site/industry', 'token=' . $this->session->data['token'] . $url, true);
		if (!isset($this->request->get['idx'])) {
			$data['url_action'] = $this->url->link('site/industry/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['url_action'] = $this->url->link('site/industry/edit', 'token=' . $this->session->data['token'] . '&idx=' . $this->request->get['idx'] . $url, true);
		}

		$data['button_add'] = $this->language->get('text_add');
		$data['button_edit'] = $this->language->get('text_edit');
		$data['button_delete'] = $this->language->get('text_delete');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_filter'] = $this->language->get('button_filter');
		$data['tab_general'] = $this->language->get('tab_general');

		// 提示訊息
		$data['error_warning'] = '';
		$data['success'] = '';
		return $data ;
	}
}