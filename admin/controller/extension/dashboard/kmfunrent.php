<?php
class ControllerExtensionDashboardKmfunrent extends Controller {
	private $error = array();

	/**
	 * [dashboard description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-03-03
	 */
	public function dashboard() {
		$this->load->model('sale/order') ;
		$this->load->model('site/option') ;
		$this->load->model('site/car_model') ;
		// dump( $this->request->get) ;

		// 清空從取車/還車存的session
		unset( $this->session->data['from_url']) ;
		unset( $this->session->data['from_station_id']) ;
		unset( $this->session->data['from_mode']) ;

		// 取得車站名稱
		$carStations = $this->model_site_option->getOptionItemsAllColArr( '15') ;
		$kmfunStation       = array() ;
		$station_background = array() ;
		foreach ( $carStations as $tmpRow) {
			$kmfunStation[$tmpRow['idx']] = $tmpRow['opt_name'] ;
		}
		$data['kmfunStation'] = $kmfunStation ;
		// dump( $kmfunStation) ;
		// dashboard 顏色
		$station_background[16][0] = '#AC283C' ;
		$station_background[16][1] = '#EF9A9A' ;
		$station_background[16][2] = '#EF5350' ;
		$station_background[16][3] = '#EF9A9A' ;

		$station_background[17][0] = '#338646' ;
		$station_background[17][1] = '#A5D6A7' ;
		$station_background[17][2] = '#66BB6A' ;
		$station_background[17][3] = '#A5D6A7' ;

		$station_background[18][0] = '#1547B1' ;
		$station_background[18][1] = '#7EBBFC' ;
		$station_background[18][2] = '#2A7CF7' ;
		$station_background[18][3] = '#7EBBFC' ;
		$data['station_background'] = $station_background ;

		$today      = date( 'Y-m-d') ;
		$outCarArr  = $this->model_sale_order->dashboardTodayOrderCnt( 's_station', $today) ;
		$backCarArr = $this->model_sale_order->dashboardTodayOrderCnt( 'e_station', $today) ;

		// dump( array( $outCarArr, $backCarArr)) ;
		$data['todayCnt']    = array( 'out' => @$outCarArr[0], 'back' => @$backCarArr[0]) ;
		$data['tomorrowCnt'] = array( 'out' => @$outCarArr[1], 'back' => @$backCarArr[1]) ;

		$data['text_view'] = $this->language->get('text_view') ;
		$data['percentage'] = 0 ;
		$data['token'] = $this->session->data['token'] ;


		// 快速選單 =================================================================================================================
		$data['colArr'] = $this->getOutColArr() ;

		// 車型明細
		$carModelArr = $this->model_site_car_model->getCarSeedOption() ;

		$cid = !is_null( $this->getRequest('get','cid')) ? $this->getRequest('get','cid') : '' ;
		$stationIdx = !is_null( $this->getRequest('get','s')) ? $this->getRequest('get','s') : 16 ;
		$mod        = !is_null( $this->getRequest('get','mod')) ? $this->getRequest('get','mod') : 'pickup' ;
		$order      = !is_null( $this->getRequest('get','order')) ? $this->getRequest('get','order') : 'asc' ;
		$orderUrl   = ( $order == 'desc') ? 'asc' : 'desc' ;

		$chDay      = !is_null( $this->getRequest('get','chDay')) ? $this->getRequest('get','chDay') : 1 ;
		if ( $chDay == 1) {
			$useDate = date( 'Y-m-d') ;
		} else if ( $chDay == 2){
			$useDate = date('Y-m-d', strtotime(date( 'Y-m-d') . ' + 1 day')) ;
		}
		$linkStr = "common/dashboard" ;
		$url = "&s={$stationIdx}&mod={$mod}&chDay={$chDay}&cid={$cid}" ;

		if ( !isset( $this->request->get['mod']) || $this->request->get['mod'] == 'pickup') {
			$sort       = !is_null( $this->getRequest('get','sort')) ? $this->getRequest('get','sort') : 'rent_date_out' ;
			$filter_data['fStation']  = "s_station" ;
			$filter_data['fDateName'] = 'rent_date_out' ;
			$filter_data['sort']      = $sort ;

			$data['station_status']   = ( $chDay == 1) ? "今日出車" : "明日出車" ;
			$data['tr_color']		  = 0 ;

			$urlStr                           = $url . "&sort=car_model&order={$orderUrl}" ;
			$data['sortUrl']['car_model']     = $this->url->link($linkStr, 'token=' . $this->session->data['token'] . $urlStr, true) ;
			$urlStr                           = $url . "&sort=rent_date_out&order={$orderUrl}" ;
			$data['sortUrl']['rent_date_out'] = $this->url->link($linkStr, 'token=' . $this->session->data['token'] . $urlStr, true) ;
			$data['colArr']                   = $this->getOutColArr() ;

		} else if ( $this->request->get['mod'] == 'pickdown') {
			$sort       = !is_null( $this->getRequest('get','sort')) ? $this->getRequest('get','sort') : 'rent_date_in_plan' ;
			$filter_data['fStation']  = "e_station" ;
			$filter_data['fDateName'] = 'rent_date_in_plan' ;
			$filter_data['sort']      = $sort ;

			$data['station_status']   = ( $chDay == 1) ? "今日還車" : "明日還車";
			$data['tr_color']		  = 2 ;

			$urlStr                           = $url . "&sort=car_model&order={$orderUrl}" ;
			$data['sortUrl']['car_model']     = $this->url->link($linkStr, 'token=' . $this->session->data['token'] . $urlStr, true) ;
			$urlStr                           = $url . "&sort=rent_date_in_plan&order={$orderUrl}" ;
			$data['sortUrl']['rent_date_in_plan'] = $this->url->link($linkStr, 'token=' . $this->session->data['token'] . $urlStr, true) ;
			$data['colArr']                   = $this->getBackColArr() ;
		}
		// dump( $data['sortUrl']) ;
		$filter_data['fStationVal'] = $stationIdx ;
		$filter_data['fToday']      = $useDate ;
		$filter_data['order']       = $order ;

		$data['stationIdx']         = $stationIdx ;
		$data['station_name']       = $kmfunStation[$stationIdx] ;
		$data['sort']               = $sort ;
		$data['order']              = $order ;

		// 訂單類別
		$orderTypeArr = $this->model_site_option->getOptionItemsArr('8') ;

		// dump( $filter_data) ;
		$rentArr = $this->model_sale_order->dashboardTodayOrder( $filter_data) ;
		$now_carmodel = array();
		foreach ($rentArr as $iCnt => $tmpRow) {
			$rentUrl = "" ;
			$rentUrl .= "&idx={$tmpRow['order_id']}" ;
			$now_carmodel[$tmpRow['car_model']] = $carModelArr[$tmpRow['car_model']] ;

			// add by Angus 2019.09.15 顯示多台車號 start --------------------------------
			$otherCarArr = unserialize($tmpRow['other_car']) ;
			if ( count( $otherCarArr) > 1) {
				$carNoTmp = "";
				foreach ($otherCarArr as $cCnt => $cTmp) {
					$carNoTmp[] = $cTmp['car_no'] ;
				}
				$rentArr[$iCnt]['car_no'] = join( ', ', $carNoTmp) ;
			}
			// add by Angus 2019.09.15 顯示多台車號 End ----------------------------------

			$rentArr[$iCnt]['car_model'] = $carModelArr[$tmpRow['car_model']] ;
			$rentArr[$iCnt]['order_type'] = $orderTypeArr[$tmpRow['order_type']] ;
			$rentArr[$iCnt]['href'] = $this->url->link('site/pickup/edit', 'token=' . $this->session->data['token'] . $rentUrl, true) ;
		}
		$data['rentArr'] = $rentArr ;
		$data['now_carmodel'] = $now_carmodel ;
		$data['cid'] = $cid ;
		return $this->load->view('extension/dashboard/kmfunrent', $data) ;
	}

	/**
	 * [ajaxRentTable description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-03-07
	 */
	public function ajaxRentTable() {
		// dump( $this->request->get) ;
		$this->load->model('sale/order') ;
		$this->load->model('site/option') ;
		$this->load->model('site/car_model') ;

		if ( $this->checkAjaxStationData()) {
			$carStations = $this->model_site_option->getOptionItemsAllColArr( '15') ;
			$kmfunStation = array() ;
			foreach ( $carStations as $tmpRow) {
				$kmfunStation[$tmpRow['idx']] = $tmpRow['opt_name'] ;
			}

			// dashboard 顏色
			$station_background[16][0] = 'style="background: #AC283C; color: White;"' ;
			$station_background[16][1] = 'style="background: #ef9a9a;"' ;
			$station_background[16][2] = 'style="background: #EF5350; color: White;"' ;
			$station_background[16][3] = 'style="background: #FFCC80;"' ;

			$station_background[17][0] = 'style="background: #338646; color: White;"' ;
			$station_background[17][1] = 'style="background: #A5D6A7;"' ;
			$station_background[17][2] = 'style="background: #66BB6A; color: White;"' ;
			$station_background[17][3] = 'style="background: #90CAF9;"' ;

			$station_background[18][0] = 'style="background: #1547B1; color: White;"' ;
			$station_background[18][1] = 'style="background: #9FA8DA;"' ;
			$station_background[18][2] = 'style="background: #2A7CF7; color: White;"' ;
			$station_background[18][3] = 'style="background: #CE93D8;"' ;
			$data['station_background'] = $station_background ;

			$cid = !is_null( $this->getRequest('get','cid')) ? $this->getRequest('get','cid') : '' ;
			$stationIdx = !is_null( $this->getRequest('get','station')) ? $this->getRequest('get','station') : 16 ;
			$chDay      = !is_null( $this->getRequest('get','chDay')) ? $this->getRequest('get','chDay') : 1 ;

			if ( $chDay == 1) {
				$useDate = date( 'Y-m-d') ;
			} else if ( $chDay == 2){
				$useDate = date('Y-m-d', strtotime(date( 'Y-m-d') . ' + 1 day')) ;
			}


			$data['station_name'] = $kmfunStation[$stationIdx] ;
			$showMod = $this->request->get['type'] ;
			$linkStr = "common/dashboard" ;

			switch ( $showMod) {
				case 'out': // 出車
					$data['station_status']     = ( $chDay == 1) ? "今日出車" : "明日出車" ;
					$data['colArr']             = $this->getOutColArr() ;
					$data['view_tr_styles']     = $station_background[$this->request->get['station']][0] ;
					$data['sort']               = 'rent_date_out' ;

					$filter_data['fStation']    = "s_station" ;
					$filter_data['fDateName']   = $filter_data['sort'] = "rent_date_out" ;

					$url = "&s={$stationIdx}&mod=pickup&chDay={$chDay}&cid={$cid}" ;

					$urlStr = $url . "&sort=car_model&order=desc" ;
					$data['sortUrl']['car_model'] = $this->url->link($linkStr, 'token=' . $this->session->data['token'] . $urlStr, true) ;
					$urlStr = $url . "&sort=rent_date_out&order=desc" ;
					$data['sortUrl']['rent_date_out'] = $this->url->link($linkStr, 'token=' . $this->session->data['token'] . $urlStr, true) ;
					break;
				case 'back': // 還車
					$data['station_status']     = ( $chDay == 1) ? "今日還車" : "明日還車";
					$data['colArr']             = $this->getBackColArr() ;
					$data['view_tr_styles']     = $station_background[$this->request->get['station']][2] ;
					$data['sort']               = 'rent_date_in_plan' ;

					$filter_data['fStation']    = "e_station" ;
					$filter_data['fDateName']   = $filter_data['sort'] = "rent_date_in_plan" ;
					$url = "&s={$stationIdx}&mod=pickdown" ;

					$urlStr = $url . "&sort=car_model&order=desc&chDay={$chDay}&cid={$cid}" ;
					$data['sortUrl']['car_model'] = $this->url->link($linkStr, 'token=' . $this->session->data['token'] . $urlStr, true) ;
					$urlStr = $url . "&sort=rent_date_out&order=desc" ;
					$data['sortUrl']['rent_date_in_plan'] = $this->url->link($linkStr, 'token=' . $this->session->data['token'] . $urlStr, true) ;
					break;
			}

			$filter_data['fStationVal'] = $this->request->get['station'] ;
			$filter_data['fToday']      = $useDate ;
			$filter_data['order']       = 'asc' ;
			// dump( $filter_data) ;

			// 車型明細
			$carModelArr = $this->model_site_car_model->getCarSeedOption() ;
			// 訂單類別
			$orderTypeArr = $this->model_site_option->getOptionItemsArr('8') ;

			$rentArr = $this->model_sale_order->dashboardTodayOrder( $filter_data) ;
			// dump( $rentArr) ;
			$now_carmodel = array();
			foreach ($rentArr as $iCnt => $tmpRow) {
				$rentUrl = "" ;
				$rentUrl .= "&idx={$tmpRow['order_id']}" ;

				$rentArr[$iCnt]['car_model'] = $carModelArr[$tmpRow['car_model']] ;
				$rentArr[$iCnt]['order_type'] = $orderTypeArr[$tmpRow['order_type']] ;
				$now_carmodel[$tmpRow['car_model']] = $carModelArr[$tmpRow['car_model']] ;

				// add by Angus 2019.09.15 顯示多台車號 start --------------------------------
				$otherCarArr = unserialize($tmpRow['other_car']) ;
				$carNoTmp = "";
				if ( count( $otherCarArr) > 1) {
					foreach ($otherCarArr as $cCnt => $cTmp) {
						$carNoTmp[] = $cTmp['car_no'] ;
					}
					$rentArr[$iCnt]['car_no'] = join( ', ', $carNoTmp) ;
				}
				// add by Angus 2019.09.15 顯示多台車號 End ----------------------------------

				if ( $showMod == "out") {
					$rentArr[$iCnt]['href'] = $this->url->link('site/pickup/edit', 'token=' . $this->session->data['token'] . $rentUrl, true) ;
				} else if ( $showMod = "back") {
					$rentArr[$iCnt]['href'] = $this->url->link('site/pickdown/edit', 'token=' . $this->session->data['token'] . $rentUrl, true) ;
				}

			}

			//print_r($rentArr);
			$data['rentArr'] = $rentArr ;
			$data['now_carmodel'] = $now_carmodel ;
			$data['cid'] = $cid ;

			echo $this->load->view('extension/dashboard/kmfunrent_ajax', $data) ;
		}
	}

	/**
	 * [checkAjaxStationData description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-03-07
	 */
	protected function checkAjaxStationData() {
		$retFlag = true ;
		if( !isset( $this->request->get['station'])) {
			$retFlag = false ;
		}
		if ( empty( $this->request->get['station'])) {
			$retFlag = false ;
		}

		if( !isset( $this->request->get['type'])) {
			$retFlag = false ;
		}
		if ( empty( $this->request->get['type'])) {
			$retFlag = false ;
		}

		if( !isset( $this->request->get['chDay'])) {
			$retFlag = false ;
		}
		if ( empty( $this->request->get['chDay'])) {
			$retFlag = false ;
		}


		return $retFlag ;
	}

	/**
	 * [getOutColArr 取車欄位]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-03-07
	 */
	protected function getOutColArr() {
		return array(
			'agreement_no'  => "訂單編號",
			'car_model'     => "車型",
			'car_no'        => "車號",
			'rent_date_out' => "預計取車日期",
			'get_user_name' => "領車人",
			'get_user_id' 	=> "領車人身份証",
			'get_user_tel'  => "連絡電話",
			'order_type'    => "訂單類別",
		) ;
	}

	/**
	 * [getBackColArr 還車欄位]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2018-03-07
	 */
	protected function getBackColArr() {
		return array(
			'agreement_no'      => "訂單編號",
			'car_model'         => "車型",
			'car_no'            => "車號",
			'rent_date_in_plan' => "預計還車日期",
			'get_user_name'     => "領車人",
			'get_user_id' 	    => "領車人身份証",
			'get_user_tel'      => "連絡電話",
			'order_type'        => "訂單類別",
		) ;
	}

	/**
	 * [getRequest description]
	 * @param   [type]     $method [description]
	 * @param   [type]     $index  [description]
	 * @return  [type]             [description]
	 * @Another Angus
	 * @date    2018-05-14
	 */
	protected function getRequest( $method, $index) {
		switch ( $method) {
			case 'get':
				return isset( $this->request->get[$index]) ? $this->request->get[$index] : null ;
				break;
			case 'post':
				return isset( $this->request->post[$index]) ? $this->request->post[$index] : null ;
				break;
		}
	}

}