<?php
class ControllerCommonLogout extends Controller {
	public function index() {
		$this->user->logout();

		unset($this->session->data['token']);
		unset($this->session->data['user_name']);
		unset($this->session->data['user_group_id']);

		$this->response->redirect($this->url->link('common/login', '', true));
	}
}