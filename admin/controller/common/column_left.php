<?php
class ControllerCommonColumnLeft extends Controller {
	private $menuParent = array() ;
	private $menuInfo = array() ;

	public function index() {
		// 加功能選項需知
		// 1. 需在資料庫中增加功能項 sys_backend_menu 新增一筆資料
		// 2. 使用sadmin / job4!kmfun 角色登入系統 加入群組權限

		// pre( $this->request->get['token'], __METHOD__, __FILE__) ;
		// pre( $this->session->data['token'], __METHOD__, __FILE__) ;
		// pre( $this->session->data, __METHOD__, __FILE__) ;
		/* add by Angus 2017.03.10
			session data
			    [language] => zh-TW
			    [currency] => USD
			    [user_group_id] => 1 -> superAdmin
			    [user_id] => 1
			    [token] => qrFFjtO0vBYeKDYytUSUdSumOuhkiNR0
		*/

		if (isset($this->request->get['token']) && isset($this->session->data['token']) && ($this->request->get['token'] == $this->session->data['token'])) {
			$this->load->language('common/column_left');

			$this->load->model('user/user');

			$this->load->model('tool/image');

			$this->load->model( 'site/menu') ;

			$user_info = $this->model_user_user->getUser($this->user->getId());

			if ($user_info) {
				$data['firstname'] = $user_info['firstname'];
				$data['lastname'] = $user_info['lastname'];

				$data['user_group'] = $user_info['user_group'];

				if (is_file(DIR_IMAGE . $user_info['image'])) {
					$data['image'] = $this->model_tool_image->resize($user_info['image'], 45, 45);
				} else {
					$data['image'] = '';
				}
			} else {
				$data['firstname'] = '';
				$data['lastname'] = '';
				$data['user_group'] = '';
				$data['image'] = '';
			}

			// Create a 3 level menu array
			// Level 2 can not have children
			// 原生opencart 2.3.0.2是寫死的 2017.04.09改成動態生成 add by Angus 2017.04.09
			list( $this->menuParent, $this->menuInfo) = $this->model_site_menu->getMenuStructure() ;
			// dump( $this->user) ;
			// dump( $this->user->getHasPermission()) ;
 			// dump( $this->menuParent) ;
			// dump( $this->menuInfo) ;

			foreach ( $this->menuParent[0] as $i => $mParent) {
				if ( !isset($this->menuParent[$mParent])) {
					// Menu 資訊總覽
					// ========================================================================================================
					if ($this->user->hasPermission('access', $this->menuInfo[$mParent]['fun_path']) ||
							$this->menuInfo[$mParent]['fun_path'] == 'common/dashboard') {
						$data['menus'][] = array(
							'id'       => $this->menuInfo[$mParent]['idx'],
							'icon'	   => $this->menuInfo[$mParent]['fun_icon'],
							'name'	   => $this->language->get( $this->menuInfo[$mParent]['lang_desc']),
							'href'     => $this->url->link( $this->menuInfo[$mParent]['fun_path'], 'token=' . $this->session->data['token'], true),
							'children' => array()
						);
					}
				} else {
					// if ( $mParent == "9") dump( $this->menuParent[$mParent]) ;
					$childrenArr = $this->getChildrenItems( $mParent) ;
					// if ( $mParent == "9") dump( $childrenArr) ;
					if ( is_array( $childrenArr)) {
						$data['menus'][] = array(
							'id'       => $this->menuInfo[$mParent]['idx'],
							'icon'	   => $this->menuInfo[$mParent]['fun_icon'],
							'name'	   => $this->language->get( $this->menuInfo[$mParent]['lang_desc']),
							'href'     => '',
							'children' => $childrenArr
						);
					}
				}
			}
			// dump( $data['menus']) ;


			// Stats
			$data['text_complete_status'] = $this->language->get('text_complete_status');
			$data['text_processing_status'] = $this->language->get('text_processing_status');
			$data['text_other_status'] = $this->language->get('text_other_status');

			$this->load->model('sale/order');

			// mark by Angus 2019.08.25
			// $order_total = $this->model_sale_order->getTotalOrders();
			$order_total = 0 ;

			// mark by Angus 2019.08.25
			// $complete_total = $this->model_sale_order->getTotalOrders(array('filter_order_status' => implode(',', $this->config->get('config_complete_status'))));
			$complete_total = 0 ;

			if ($complete_total) {
				$data['complete_status'] = round(($complete_total / $order_total) * 100);
			} else {
				$data['complete_status'] = 0;
			}

			// mark by Angus 2019.08.25
			// $processing_total = $this->model_sale_order->getTotalOrders(array('filter_order_status' => implode(',', $this->config->get('config_processing_status'))));
			$processing_total = 0 ;

			if ($processing_total) {
				$data['processing_status'] = round(($processing_total / $order_total) * 100);
			} else {
				$data['processing_status'] = 0;
			}

			$this->load->model('localisation/order_status');

			$order_status_data = array();

			$results = $this->model_localisation_order_status->getOrderStatuses();

			foreach ($results as $result) {
				if (!in_array($result['order_status_id'], array_merge($this->config->get('config_complete_status'), $this->config->get('config_processing_status')))) {
					$order_status_data[] = $result['order_status_id'];
				}
			}

			// mark by Angus 2019.08.25
			// $other_total = $this->model_sale_order->getTotalOrders(array('filter_order_status' => implode(',', $order_status_data)));
			$other_total = 0 ;

			if ($other_total) {
				$data['other_status'] = round(($other_total / $order_total) * 100);
			} else {
				$data['other_status'] = 0;
			}
			// 工具 列出所有功能階層
			// pre( $data, __METHOD__, __FILE__) ;
			// foreach ( $data['menus'] as $icnt => $tmpl1) {
			// 	echo "{$tmpl1['name']}\t{$tmpl1['href']}<br>";
			// 	if ( count($tmpl1['children']) > 0) {
			// 		foreach ( $tmpl1['children'] as $iCnt => $tmpl2) {
			// 			echo "&nbsp;&nbsp;&nbsp;&nbsp;{$tmpl2['name']}\t{$tmpl2['href']}<br>";
			// 			if ( count( $tmpl2['children']) > 0) {
			// 				foreach ( $tmpl2['children'] as $iCnt => $tmpl3) {
			// 					echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$tmpl3['name']}\t{$tmpl3['href']}<br>";
			// 					if ( count( $tmpl3['children']) > 0) {
			// 						foreach ( $tmpl3['children'] as $iCnt => $tmpl4) {
			// 							echo "{$tmpl4['name']}\t{$tmpl4['href']}<br>";
			// 						}
			// 					}
			// 				}
			// 			}
			// 		}
			// 	}
			// }

			return $this->load->view('common/column_left', $data);
		}
	}

	private function getChildrenItems ( $mParentIdx) {
		$retArr = [] ;
		// if ( $mParentIdx == "9")  dump( $this->menuParent[$mParentIdx]) ;
		foreach ( $this->menuParent[$mParentIdx] as $i => $menuIdx) {
			// if ( $mParentIdx == "9") dump( array( $menuIdx, $this->menuParent[$menuIdx])) ;
			if ( !isset($this->menuParent[$menuIdx]) ) {
				// if ( $mParentIdx == "9") dump( $this->menuInfo[$menuIdx]['fun_path']) ;
				if ($this->user->hasPermission('access', $this->menuInfo[$menuIdx]['fun_path'])) {
					$retArr[] = array(
						'name'	   => $this->language->get( $this->menuInfo[$menuIdx]['lang_desc']),
						'href'     => $this->url->link( $this->menuInfo[$menuIdx]['fun_path'], 'token=' . $this->session->data['token'], true),
						'children' => array()
					);
				}
			} else {
				$childrenArr = $this->getChildrenItems( $menuIdx) ;
				if ( is_array( $childrenArr)) {
					$retArr[] = array(
						'name'	   => $this->language->get( $this->menuInfo[$menuIdx]['lang_desc']),
						'href'     => '',
						'children' => $childrenArr
					);
				}
			}
		}
		return $retArr ;
	}
}
