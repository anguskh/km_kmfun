<?php
class ControllerDevelopAutoinsert extends Controller {
	private $error = array();
	public $dataType = '';
	public $dataLength = 0;

	public function index() {
		$this->load->language('develop/autoinsert');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('develop/autoinsert');

		$this->getList();
	}

	public function insert() {
		$this->load->language('develop/autoinsert');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('develop/autoinsert');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			// 塞假資料動作
			$insertDatas = $this->insertAction();

			$this->model_develop_autoinsert->insertRandomData($this->request->post['selTable'], $insertDatas);

			$this->session->data['success'] = $this->language->get('text_success');
		}

		$this->getList();
	}

	public function insertAction() {
		$descriptions = $this->model_develop_autoinsert->getDescription($this->request->post['selTable']);

		for ($i = 0; $i < $this->request->post['quantity']; $i++) {
			foreach ($descriptions as $key => $description) {
				preg_match('/([\w]*)[\(]?/', $description['Type'], $match);
				$this->dataType = $match[1];

				$pregLength = preg_match('/\(([\w\W]*)\)/', $description['Type'], $match);

				if ($pregLength) $this->dataLength = $match[1];

				if ($description['Extra'] == 'auto_increment') continue;

				$insertData[$i][$description['Field']] = (
						empty($this->request->post['customize'][$description['Field']])) ?
						$this->getRandomData() : $this->request->post['customize'][$description['Field']];
				// dump( array($this->request->post, $this->dataType, $pregLength)) ;
			}
		}
		return $insertData;
	}

	public function getRandomData() {
		switch ($this->dataType) {
			case 'int':
				$result = intval($this->buildRandomData('integer', $this->dataLength));
				break;
			case 'varchar':
				$result = $this->buildRandomData('string', $this->dataLength);
				break;
			case 'text':
				$result = $this->buildRandomData('string');
				break;
			case 'decimal':
				$result = $this->buildDecimalData($this->dataLength);
				break;
			case 'tinyint':
				$result = mt_rand(0, 9);
				break;
			case 'datetime':
				$result = date( 'Y-m-d H:i:s');
				break;
			case 'date':
				$result = date( 'Y-m-d');
				break;
			default :
				return $result = "" ;
		}

		return $result;
	}

	public function buildRandomData($property, $dataLength = 50) {
		$randomLength = mt_rand(1, $dataLength);
		$output = '';
		for ($i = 0; $i < $randomLength; $i++) {
			$output .= ($property == 'string') ? chr(mt_rand(97, 122)) : mt_rand(0, 9);
			if ( $i%5 == mt_rand(0, 5)) $output .= ' ' ;
		}

		return $output;
	}

	public function buildDecimalData($dataLength) {
		$decimal = explode(',', $dataLength);

		$accuracy = $this->buildRandomData('integer', $decimal[0] - $decimal[1]);
		$scaling = $this->buildRandomData('integer', $decimal[1]);

		return intval($accuracy) . '.' . $scaling;
	}

	protected function getList() {

		$url = '';

		if (isset($this->request->get['tables'])) {
			$url .= '&tables=' . $this->request->get['tables'];
		}

		if (isset($this->request->get['tables'])) {
			$selTables = $this->request->get['tables'];
		} else {
			$selTables = null;
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('develop/autoinsert', 'token=' . $this->session->data['token'], true)
		);

		$data['insert'] = $this->url->link('develop/autoinsert/insert', 'token=' . $this->session->data['token'] . $url, true);

		// $data['tables'] = $this->model_develop_autoinsert->getTables();
		$tmpTables = $this->model_develop_autoinsert->getTables();
		$colName = "Tables_in_".DB_DATABASE ;
		foreach ($tmpTables as $i => $colArr) {
			$data['tables'][] = $colArr[$colName] ;
		}

		$data['nowTable'] = '';
		$data['descriptions'] = array();

		if ($selTables) {
			$data['selTable'] = $selTables;
			$data['descriptions'] = $this->model_develop_autoinsert->getDescription($selTables);
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');

		$data['column_field'] = $this->language->get('column_field');
		$data['column_type'] = $this->language->get('column_type');
		$data['column_key'] = $this->language->get('column_key');
		$data['column_customize'] = $this->language->get('column_customize');


		$data['entry_tables'] = $this->language->get('entry_tables');



		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('develop/autoinsert', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'develop/autoinsert')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!ctype_digit(strval($this->request->post['quantity'])) || $this->request->post['quantity'] < 0) {
			$this->error['warning'] = $this->language->get('error_quantity');
		}

		return !$this->error;
	}
}
