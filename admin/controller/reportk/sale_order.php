<?php
/**
 *select strY, strM, sum(total) total, count(strD) cnt from (
SELECT
	date_format( create_date, '%Y' ) strY,
	date_format( create_date, '%m' ) strM,
	date_format( create_date, '%d' ) strD,
	total
FROM
	tb_order
WHERE
	order_status IS NULL
	AND order_type =9 ) a
	group by strY, strM
	order by strY desc, strM desc
 */
class ControllerReportkSaleOrder extends Controller {
	private $data = array() ;

	public function index() {
		$this->preparation() ;
		$this->load->model('reportk/order');
		$this->load->model('site/option');

		$this->data['colArr'] = $this->getLevel_1_column() ;
		$this->data['orderTypeArr'] = $this->model_site_option->getOptionItemsArr( '8', true) ;

		if (($this->request->server['REQUEST_METHOD'] == 'GET') && $this->validateReport()) {
			$filter['ot'] = isset( $this->request->get['filter_ot']) ? $this->request->get['filter_ot'] : "" ;
			$retArr = $this->model_reportk_order->getLevel_1_report( $filter) ;
			$this->data['reportRows'] = $retArr ;
		} else {
			$this->data['reportRows'] = array() ;
		}

		// 整理查詢條件
		$this->data['filter_ot'] = isset( $filter['ot']) ? $filter['ot'] : "" ;


		// 分頁部份
		$this->data['pagination'] = "" ;
		$this->data['results'] = "" ;
		$this->response->setOutput($this->load->view('reportk/sale_order', $this->data));
	}

	public function nextLevel() {
		$this->preparation() ;
		$this->load->model('reportk/order');
		$this->load->model('site/option');

		$this->data['colArr'] = $this->getLevel_2_column() ;
		$this->data['orderTypeArr'] = $this->model_site_option->getOptionItemsArr( '8', true) ;

		if (($this->request->server['REQUEST_METHOD'] == 'GET') && $this->validateReport()) {
			$filter['ot'] = isset( $this->request->get['filter_ot']) ? $this->request->get['filter_ot'] : "" ;
			$filter['yy'] = isset( $this->request->get['strY']) ? $this->request->get['strY'] : "" ;
			$filter['mm'] = isset( $this->request->get['strM']) ? $this->request->get['strM'] : "" ;

			$retArr = $this->model_reportk_order->getLevel_2_report( $filter) ;
			$this->data['reportRows'] = $retArr ;
		} else {
			$this->data['reportRows'] = array() ;
		}

		// 整理查詢條件
		$this->data['filter_ot'] = isset( $filter['ot']) ? $filter['ot'] : "" ;
		$this->data['filter_ot'] = isset( $filter['ot']) ? $filter['ot'] : "" ;
		$this->data['filter_ot'] = isset( $filter['ot']) ? $filter['ot'] : "" ;

		// 分頁部份
		$this->data['pagination'] = "" ;
		$this->data['results'] = "" ;
		$this->response->setOutput($this->load->view('reportk/sale_order', $this->data));
	}

	/**
	 * [preparation description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-01-10
	 */
	private function preparation() {
		$this->load->language('report/sale_order');

		$this->document->setTitle($this->language->get('heading_title'));
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_list']     = $this->language->get('text_list');
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['token']        = $this->session->data['token'];

		$url = "" ;
		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$this->data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('reportk/sale_order', 'token=' . $this->session->data['token'] . $url, true)
		);

		$this->data['header'] = $this->load->controller('common/header');
		$this->data['column_left'] = $this->load->controller('common/column_left');
		$this->data['footer'] = $this->load->controller('common/footer');

	}

	/**
	 * [getLevel_1_column description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-01-11
	 */
	private function getLevel_1_column() {
		return array(
			"strY"  => "年份",
			"strM"  => "月份",
			"cnt" => "訂單筆數",
			"total" => "合計",
		) ;
	}

	/**
	 * [getLevel_1_column description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-01-12
	 */
	private function getLevel_2_column() {
		return array(
			"strY"  => "年份",
			"strM"  => "月份",
			"strD"  => "日期",
			"cnt" => "訂單筆數",
			"total" => "合計",
		) ;
	}

	/**
	 * [validateReport description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2019-01-11
	 */
	private function validateReport() {
		if ( !isset( $this->request->get['filter_ot'])) return false ;
		if ( $this->request->get['filter_ot'] == '0') return false ;

		return true ;
	}

}