<?php
class ControllerReportkSaleToday extends Controller {
	private $error      = array();
	private $debug      = false ;

	private $column_url      = '' ;
	private $page_url        = '';
	private $func_path       = 'reportk/sale_today' ;
	private $orderTypeArr    = array() ;	// 訂單種類
	private $carModelTypeArr = array() ;	// 車種對照表
	private $minsuNameArr    = array() ;	// 民宿名稱
	private $stationArr      = [] ; 		// 取/還車地點
	private $dashboardItem   = [] ; 		// dashboard 項目
	private $payTypeArr      = [] ; 		// 付費類別
	private $loseTypeArr     = [] ; 		// 回程付費類別
	private $filter_mod      = "" ;


    /**
	 * [index description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2020-02-20
	 */
	public function index() {
		$this->load->language( 'report/sale_main');

		$this->document->setTitle($this->language->get('heading_title_today'));

		$this->load->model( 'reportk/order');

		$this->getList();
	}

	/**
	 * [getList description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2020-02-20
	 */
	protected function getList() {
		ini_set("max_execution_time", 0) ;
		ini_set("memory_limit","2048M");

		$data = $this->init() ;
		// get參數==========================================================================================================
		$data["page"]         = !empty($data["page"]) ? $data["page"] : 1;
		$data["filter_limit"] = !empty($data["filter_limit"]) ? $data["filter_limit"] : $this->config->get('config_limit_admin');

		// 取得排序資訊
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'default';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		$url = $this->column_url;
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		$columnNames = array(
				"agreement_no"      => $this->language->get('column_agreement_no'),
				"order_type"        => $this->language->get('column_order_type'),
				"car_type"          => $this->language->get('column_car_type'),
				"car_model"         => $this->language->get('column_car_model'),
				"car_no"            => $this->language->get('column_car_no'),

				"s_station"         => $this->language->get('column_s_station'),
				"e_station"         => $this->language->get('column_e_station'),

				"total_day"         => $this->language->get('column_total_day'),
				"pay_type"          => $this->language->get('column_pay_type'),			// 2020
				"total"             => $this->language->get('column_total'),
				"plug_cost"         => $this->language->get('column_plug_cost'),		// 2020

				"lose_type"  		=> $this->language->get('column_lose_type'),		// 2020
				"oil_cost"          => $this->language->get('column_oil_cost'),			// 2020
				"overTime_cost"     => $this->language->get('column_overTime_cost'),	// 2020
				"business_lose"     => $this->language->get('column_business_lose'),	// 2020
				"car_lose"          => $this->language->get('column_car_lose'),			// 2020
				"other_lose"        => $this->language->get('column_other_lose'),		// 2020
				"spdc"              => $this->language->get('column_spdc_cost'),		// 2020
				"rent_out_date"     => $this->language->get('column_rent_out_date'),
				"rent_out_time"     => $this->language->get('column_rent_out_time'),
				"rent_in_date"      => $this->language->get('column_rent_in_date'),
				"rent_in_time"      => $this->language->get('column_rent_in_time'),

				"real_out_date"     => $this->language->get('column_real_out_date'),
				"real_out_time"     => $this->language->get('column_real_out_time'),
				"real_in_date"      => $this->language->get('column_real_in_date'),
				"real_in_time"      => $this->language->get('column_real_in_time'),
				"minsu"             => $this->language->get('column_minsu'),
				"memo"              => $this->language->get('column_memo'),

				"rent_user_name"    => $this->language->get('column_rent_user_name'),
				"rent_company_name" => $this->language->get('column_rent_company_name'),
				"rent_company_no"   => $this->language->get('column_rent_company_no'),
				"rent_user_born"    => $this->language->get('column_rent_user_born'),
				"create_man"        => $this->language->get('column_create_man'), // 租車業務
				"return_man"        => $this->language->get('column_return_man'), // 還車業務
		) ;

		$data['columnNames']   = $columnNames ;
		$data['column_action'] = $this->language->get('column_action');
		$data['td_colspan']    = count( $columnNames) + 2 ;

		$data['heading_title']   = $this->language->get('heading_title_today');

		$data['text_list']       = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm']    = $this->language->get('text_confirm');

		$data['button_add']      = $this->language->get('text_add');
		$data['button_edit']     = $this->language->get('text_edit');
		$data['button_delete']   = $this->language->get('text_delete');
		$data['button_filter']   = $this->language->get('button_filter');

		// 報表區間上色
		// 預計時間
		$data['planDate'] = ['rent_out_date','rent_out_time','rent_in_date','rent_in_time'] ;
		// 實際時間
		$data['realDate'] = ['real_out_date','real_out_time','real_in_date','real_in_time'] ;
		// 訂單區
		$data['rentPay'] = ['total_day', 'pay_type', 'total', 'plug_cost'] ;
		// 還車加收區
		$data['retPay'] = ['lose_type','oil_cost','overTime_cost','business_lose','car_lose','other_lose'] ;

		$data['colColor'] = ['planDate'=>'#D5F5E3','realDate'=>'#FAE5D3','rentPay'=>'#FDEDEC','retPay'=>'#F9E79F'] ;

		// modal page 彈跳視窗
		// $data['modal_action'] = $this->url->link($this->func_path . '/edit', 'token=' . $this->session->data['token'] . $url, true);
		// $data['del_action']   = $this->url->link($this->func_path . '/del', 'token=' . $this->session->data['token'] . $url, true);

		// 查詢條件 ------------------------------------------------------------------------------------
		$filter_data = array(
			// 'fds'        => '2020-10-28' ,
			// 'fde'        => '2020-10-28' ,
			'fds'        => isset( $data['filter_fds']) ? $data['filter_fds'] : '' ,
			'fde'        => isset( $data['filter_fde']) ? $data['filter_fde'] : '' ,
			'rdi'        => isset( $data['filter_rdi']) ? $data['filter_rdi'] : '' ,
			'rde'        => isset( $data['filter_rde']) ? $data['filter_rde'] : '' ,
			'station'    => isset( $data['filter_station']) ? $data['filter_station'] : '' ,
			'filter_man' => isset( $data['filter_man']) ? $data['filter_man'] : '' ,

			'sort'  => $sort,
			'order' => $order,

			'start' => ($data["page"] - 1) * $data["filter_limit"],
			'limit' => $data["filter_limit"]
		);
		// dump( $filter_data) ;

		$data['dashboardMaps'] = $this->blockDashboardMaps() ;
		$this->dashboardItem   = $this->makeDashboardItem() ;

		$this->load->model('site/option') ;
		$this->stationArr     = $this->model_site_option->getOptionNameArr('15', true) ;
		$data['stationArr']   = $this->stationArr ;
		$data['userArr']      = $this->model_reportk_order->getUserArr() ;


		// filter_fds 開始時間
		// filter_rdi 實際還車日期
		if ( isset( $data['filter_mod']) && $data['filter_mod'] != "") {
			// 匯出連結
			$url .= "&export=excel" ;
			$data['exportUrl'] = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . $url, true) ;

			// $total           = $this->model_reportk_order->getTotal( $filter_data) ; // 所有筆數
			$total = 0 ;
			$results         = $this->model_reportk_order->getList( $filter_data) ;  // 逐筆資訊
			// $results = [] ;
			// $statisticalArr  = $this->model_reportk_order->getOrderTotal( $filter_data) ;  // 計算各類別筆數及總合。用不到了

			$this->minsuNameArr    = $this->model_reportk_order->getMinsuList() ;
			$this->carModelTypeArr = $this->model_reportk_order->getCarModelType() ;
			$this->orderTypeArr    = $this->model_site_option->getOptionItemsArr('8', true) ;
			$this->payTypeArr	   = $this->getPayType() ;
			$this->loseTypeArr	   = $this->getLoseType() ;

			// dump( $minsuNameArr) ;
			// dump( $carModelTypeArr) ;
			// dump( $results) ;
			// dump( count($results)) ;


			$listRows = [] ;
			foreach ($results as $iCnt => $row) {
				// 取得經手人
                if ( $row['order_status'] != '') {
                    // list( $row['create_man'], $row['return_man']) = $this->model_reportk_order->getConditionName( $row['agreement_no']) ;
                	// list( $row['create_man'], $row['return_man']) = [$row['out_man'], $row['in_man']] ;
                }

				$tmpArr = $row ;


				// 全部訂單
				if ( $this->request->get['filter_mod'] == 'rent_out') {

					$this->dashboardItem['allOrder']['cnt'] ++ ;
					$row['total'] = $row['total'] + $row['pickdiff'] ;
					$this->dashboardItem['allOrder']['total'] += $row['total'] ;
					$row = $this->computing_accessories( $row) ;
					// dump( $row['total']) ;
					// 訂單類別
					// 9	網路訂單
					// 10	業者 T | 民宿(業者)
					// 11	長租
					// #123	保養車輛
					// #124	自行鎖車
					// #129	預約未完成
					// 130	業者 S | 業者應收
					// 140	QRCode訂車
					// 146	前台業務訂車
					// 147	前台qrcode訂車

					switch ($tmpArr['order_type']) {
						case '9':
							$this->dashboardItem['netOrder']['cnt'] ++ ;
							$this->dashboardItem['netOrder']['total'] += $row['total'] ;
							break;

						case '10':
						case '11':
						case '140':
						case '146':
						case '147':
							$this->dashboardItem['cashOrder']['cnt'] ++ ;
							$this->dashboardItem['cashOrder']['total'] += $row['total'] ;
							break;

						case '130':
							$this->dashboardItem['minsuOrder']['cnt'] ++ ;
							$this->dashboardItem['minsuOrder']['total'] += $row['total'] ;
							break;
					}
					// 付款類型 1:信用卡,2:現金,3:匯款,4:應收
					switch ($tmpArr['pay_type']) {
						case '1':
							$this->dashboardItem['credit_card']['cnt'] ++ ;
							$this->dashboardItem['credit_card']['total'] += $row['total'] ;
							break;
						case '2':
							$this->dashboardItem['cash']['cnt'] ++ ;
							$this->dashboardItem['cash']['total'] += $row['total'] ;
							break;
						case '3':
							$this->dashboardItem['remittance']['cnt'] ++ ;
							$this->dashboardItem['remittance']['total'] += $row['total'] ;
							break;
						// case '4':
						// 	$this->dashboardItem['allOrder']['cnt'] ++ ;
						// 	$this->dashboardItem['allOrder']['total'] += $row['total'] ;
						// 	break;
					}
				} else {

				}
				// 配件
				// 兒童安全座椅〔一歲以下〕 baby_chair
				// 兒童安全座椅〔一歲以上〕 baby_chair1
				// 兒童安全座椅〔增高墊〕   baby_chair2
				// 嬰兒推車				 children_chair
				// GPS衛星導航系統		 gps
				// 機車導航架				 mobile_moto
				// if ( $row['baby_chair'] > 0 || $row['baby_chair1'] > 0 || $row['baby_chair2'] > 0 || $row['children_chair'] > 0 ||
				// 		$row['gps'] > 0 || $row['mobile_moto'] > 0) {
				// 	$this->dashboardItem['plug_cost']['cnt'] ++ ;
				// 	$cost = ( $row['baby_chair'] + $row['baby_chair1'] + $row['baby_chair2'] + $row['children_chair'] + $row['gps'] + $row['mobile_moto']) * 100 ;
				// 	$this->dashboardItem['plug_cost']['total'] += $cost ;
				// }
				if ( ($row['oil_cost'] != 0 ||
						$row['overTime_cost'] != 0 ||
						$row['business_lose'] != 0 ||
						$row['car_lose'] != 0 || $row['other_lose'] != 0) && $this->request->get['filter_mod'] == 'rent_in') {
					switch ( $row['lose_type']) {
						case 'value':
						case '5':
							$this->dashboardItem['credit_card']['cnt'] ++ ;

							break;
						case '4':
							$this->dashboardItem['cash']['cnt'] ++ ;
							break;
					}
					$this->dashboardItem['allOrder']['cnt'] ++ ;
				}
				// 油費損失
				if ( $row['oil_cost'] != 0 && $this->request->get['filter_mod'] == 'rent_in') {
					$this->dashboardItem['oil_cost']['cnt'] ++ ;
					$this->dashboardItem['oil_cost']['total'] += $row['oil_cost'] ;
					$this->dashboardItem['allOrder']['total'] += $row['oil_cost'] ;
					switch ( $row['lose_type']) {
						case '5':
							$this->dashboardItem['credit_card']['total'] += $row['oil_cost'] ;
							break;
						case '4':
							$this->dashboardItem['cash']['total'] += $row['oil_cost'] ;
							break;
					}
				} else {
					$tmpArr['oil_cost'] = '';
				}
				// 超時費用
				if ( $row['overTime_cost'] != 0 && $this->request->get['filter_mod'] == 'rent_in') {
					$this->dashboardItem['overTime']['cnt'] ++ ;
					$this->dashboardItem['overTime']['total'] += $row['overTime_cost'] ;
					$this->dashboardItem['allOrder']['total'] += $row['overTime_cost'] ;
					switch ( $row['lose_type']) {
						case '5':
							$this->dashboardItem['credit_card']['total'] += $row['overTime_cost'] ;
							break;
						case '4':
							$this->dashboardItem['cash']['total'] += $row['overTime_cost'] ;
							break;
					}
				} else {
					$tmpArr['overTime_cost'] = '';
				}
				// 營業撌失
				if ( $row['business_lose'] != 0 && $this->request->get['filter_mod'] == 'rent_in') {
					$this->dashboardItem['business_lose']['cnt'] ++ ;
					$this->dashboardItem['business_lose']['total'] += $row['business_lose'] ;
					$this->dashboardItem['allOrder']['total'] += $row['business_lose'] ;
					switch ( $row['lose_type']) {
						case '5':
							$this->dashboardItem['credit_card']['total'] += $row['business_lose'] ;
							break;
						case '4':
							$this->dashboardItem['cash']['total'] += $row['business_lose'] ;
							break;
					}
				} else {
					$tmpArr['business_lose'] = '';
				}
				// 車輛損失
				if ( $row['car_lose'] != 0 && $this->request->get['filter_mod'] == 'rent_in') {
					$this->dashboardItem['car_lose']['cnt'] ++ ;
					$this->dashboardItem['car_lose']['total'] += $row['car_lose'] ;
					$this->dashboardItem['allOrder']['total'] += $row['car_lose'] ;
					switch ( $row['lose_type']) {
						case '5':
							$this->dashboardItem['credit_card']['total'] += $row['car_lose'] ;
							break;
						case '4':
							$this->dashboardItem['cash']['total'] += $row['car_lose'] ;
							break;
					}
				} else {
					$tmpArr['car_lose'] = '';
				}
				// 車輛損失
				if ( $row['other_lose'] != 0 && $this->request->get['filter_mod'] == 'rent_in') {
					$this->dashboardItem['other_cost']['cnt'] ++ ;
					$this->dashboardItem['other_cost']['total'] += $row['other_lose'] ;
					$this->dashboardItem['allOrder']['total'] += $row['other_lose'] ;
					switch ( $row['lose_type']) {
						case '5':
							$this->dashboardItem['credit_card']['total'] += $row['other_lose'] ;
							break;
						case '4':
							$this->dashboardItem['cash']['total'] += $row['other_lose'] ;
							break;
					}
				} else {
					$tmpArr['other_lose'] = '';
				}
				// 業務折抵
				if ( $row['spdc'] != 0 && $this->request->get['filter_mod'] == 'rent_out') {
					$this->dashboardItem['spdc']['cnt'] ++ ;
					$this->dashboardItem['spdc']['total'] += $row['spdc'] ;
					$this->dashboardItem['allOrder']['total'] += $row['spdc'] ;
				} else {
					$tmpArr['spdc'] = '';
				}

				$tmpArr['car_type']      = $this->carModelTypeArr[$tmpArr['car_model']]['opt_name'] ;
				$tmpArr['car_model']     = $this->carModelTypeArr[$row['car_model']]['car_seed'] ;

				// 訂單類別
				$tmpArr['order_type'] 	 = $this->orderTypeArr[$tmpArr['order_type']] ;
				if ($row['order_type'] == 9) {
					$tmpArr['pay_type'] 	 = "網路訂單";
				} else {
					$tmpArr['pay_type'] 	 = !empty($tmpArr['pay_type']) ? $this->payTypeArr[$tmpArr['pay_type']] : "";
				}
				// lose_type
				$tmpArr['lose_type'] 	 = !empty($tmpArr['lose_type']) ? $this->loseTypeArr[$tmpArr['lose_type']] : "";

				$tmpArr['s_station']     = $this->stationArr[$tmpArr['s_station']] ;
				$tmpArr['e_station']     = $this->stationArr[$tmpArr['e_station']] ;

				// 退傭單位
				$tmpArr['minsu']         = isset( $this->minsuNameArr[$tmpArr['minsu_idx']]) ? $this->minsuNameArr[$tmpArr['minsu_idx']] : "" ;
				$tmpArr['income']        = $row['total'] ;
				$tmpArr['total']         = $row['total'] ;

				$tmpArr['rent_out_date'] = !empty($tmpArr['rent_date_out']) ? substr($tmpArr['rent_date_out'], 0, 10) : "" ;
				$tmpArr['rent_out_time'] = !empty($tmpArr['rent_date_out']) ? substr($tmpArr['rent_date_out'], 11, 5) : "" ;
				$tmpArr['rent_in_date']  = !empty($tmpArr['rent_date_in_plan']) ? substr($tmpArr['rent_date_in_plan'], 0, 10) : "" ;
				$tmpArr['rent_in_time']  = !empty($tmpArr['rent_date_in_plan']) ? substr($tmpArr['rent_date_in_plan'], 11, 5) : "" ;

				$tmpArr['real_out_date'] = !empty($tmpArr['rent_date_out_true']) ? substr($tmpArr['rent_date_out_true'], 0, 10) : "" ;
				$tmpArr['real_out_time'] = !empty($tmpArr['rent_date_out_true']) ? substr($tmpArr['rent_date_out_true'], 11, 5) : "" ;
				$tmpArr['real_in_date']  = !empty($tmpArr['rent_date_in']) ? substr($tmpArr['rent_date_in'], 0, 10) : "" ;
				$tmpArr['real_in_time']  = !empty($tmpArr['rent_date_in']) ? substr($tmpArr['rent_date_in'], 11, 5) : "" ;

				$listRows[]    = $tmpArr ;
				/**
				 * 車輛整理
				 */
				if ( $row['total_car'] > 1) {
					$carsInfoArr = unserialize( $row['other_car']) ;
					foreach ($carsInfoArr as $carCnt => $carInfo) {
						// $tmpArr                  = $row ;
						if ( $carInfo['car_no'] == $tmpArr['car_no']) continue ;

						$tmpArr['car_no']        = $carInfo['car_no'] ;
						$tmpArr['car_type']      = $this->carModelTypeArr[$carInfo['car_seed']]['opt_name'] ;
						$tmpArr['car_model']     = $this->carModelTypeArr[$carInfo['car_seed']]['car_seed'] ;
						$tmpArr['minsu']         = isset( $minsuNameArr[$tmpArr['minsu_idx']]) ? $minsuNameArr[$tmpArr['minsu_idx']] : "" ;
						// 訂單類別
						$tmpArr['order_type'] 	 = $this->orderTypeArr[$tmpArr['order_type']] ;
						$tmpArr['pay_type'] 	 = !empty($tmpArr['pay_type']) ? $this->payTypeArr[$tmpArr['pay_type']] : "" ;
						$tmpArr['lose_type'] 	 = !empty($tmpArr['lose_type']) ? $this->loseTypeArr[$tmpArr['lose_type']] : "" ;

						$tmpArr['s_station']     = $this->stationArr[$tmpArr['s_station']] ;
						$tmpArr['e_station']     = $this->stationArr[$tmpArr['e_station']] ;

						$tmpArr['rent_out_date'] = !empty($tmpArr['rent_date_out']) ? substr($tmpArr['rent_date_out'], 0, 10) : "" ;
						$tmpArr['rent_out_time'] = !empty($tmpArr['rent_date_out']) ? substr($tmpArr['rent_date_out'], 11, 5) : "" ;
						$tmpArr['rent_in_date']  = !empty($tmpArr['rent_date_in_plan']) ? substr($tmpArr['rent_date_in_plan'], 0, 10) : "" ;
						$tmpArr['rent_in_time']  = !empty($tmpArr['rent_date_in_plan']) ? substr($tmpArr['rent_date_in_plan'], 11, 5) : "" ;

						$tmpArr['real_out_date'] = !empty($tmpArr['rent_date_out_true']) ? substr($tmpArr['rent_date_out_true'], 0, 10) : "" ;
						$tmpArr['real_out_time'] = !empty($tmpArr['rent_date_out_true']) ? substr($tmpArr['rent_date_out_true'], 11, 5) : "" ;
						$tmpArr['real_in_date']  = !empty($tmpArr['rent_date_in']) ? substr($tmpArr['rent_date_in'], 0, 10) : "" ;
						$tmpArr['real_in_time']  = !empty($tmpArr['rent_date_in']) ? substr($tmpArr['rent_date_in'], 11, 5) : "" ;

						$tmpArr['oil_cost']        = "" ;
						$tmpArr['overTime_cost']   = "" ;
						$tmpArr['business_lose']   = "" ;
						$tmpArr['car_lose']        = "" ;
						$tmpArr['other_lose']      = "" ;

						$tmpArr['members']       = "" ;
						$tmpArr['total']         = "" ;
						$tmpArr['income']        = "" ;
						$tmpArr['spdc']          = "" ;
						// dump( $tmpArr) ;
						$listRows[]              = $tmpArr ;
					}
				}

				/**
				 * 兒童安全座椅〔一歲以下〕
				 */
				if ( $row['baby_chair'] > 0) {
					$listRows[] = $this->makeFittingRow( $tmpArr, "兒童安全座椅〔一歲以下〕", 'baby_chair') ;
				}
				/**
				 * 兒童安全座椅〔一歲以上〕
				 */
				if ( $row['baby_chair1'] > 0) {
					$listRows[] = $this->makeFittingRow( $tmpArr, "兒童安全座椅〔一歲以上〕", 'baby_chair1') ;
				}
				/**
				 * 兒童安全座椅〔增高墊〕
				 */
				if ( $row['baby_chair2'] > 0) {
					$listRows[] = $this->makeFittingRow( $tmpArr, "兒童安全座椅〔增高墊〕", 'baby_chair2') ;
				}
				/**
				 * 嬰兒推車
				 */
				if ( $row['children_chair'] > 0) {
					$listRows[] = $this->makeFittingRow( $tmpArr, "嬰兒推車", 'children_chair') ;
				}
				/**
				 * GPS衛星導航系統
				 */
				// if ( $row['gps'] > 0) {
				// 	$listRows[] = $this->makeFittingRow( $tmpArr, "GPS衛星導航系統", 'gps') ;
				// }
				/**
				 * 機車導航架
				 */
				if ( $row['mobile_moto'] > 0) {
					$listRows[] = $this->makeFittingRow( $tmpArr, "機車導航架", 'mobile_moto') ;
				}
			}

			$data['listRows'] = $listRows ;
			$data['dashboardBlock'] = $this->dashboardItem ;
 		} else {
			$total = 0 ;
			$data['listRows'] = array() ;
			$data['dashboardBlock'] = $this->dashboardItem ;
		}

		$url = $this->page_url;
		$pagination         = new Pagination();
		$pagination->total  = $total;
		$pagination->page   = $data["page"];
		$pagination->limit  = $data["filter_limit"];
		$pagination->url    = $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$data['pagination'] = $pagination->render();
		$data['results']    = sprintf($this->language->get('text_pagination'), ($total) ? (($data["page"] - 1) * $data["filter_limit"]) + 1 : 0, ((($data["page"] - 1) * $data["filter_limit"]) > ($total - $data["filter_limit"])) ? $total : ((($data["page"] - 1) * $data["filter_limit"]) + $data["filter_limit"]), $total, ceil($total / $data["filter_limit"]));

		$data['pagination'] = "" ;
		$data['results'] = "" ; // 顯示 1 - 14 / 14 (共 1 頁)
		$data['sort']  = $sort ;
		$data['order'] = $order ;


		if (isset( $this->request->get['export']) && $this->request->get['export'] == "excel") {
			$this->exportExcel( $data['columnNames'], $data['listRows'], [$data['filter_fds'], $data['filter_fde']] ) ;
		}
		// dump( $data) ;
		// 程式最後====================================================================================
		// ==========================================================================================
		// ==========================================================================================
		// ==========================================================================================
		$this->response->setOutput($this->load->view('reportk/sale_today', $data));
	}

	/**
	 * [init description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2020-02-20
	 */
	protected function init() {
		$this->load->language( 'report/sale_main');
		$this->document->setTitle( $this->language->get('heading_title_today'));
		$this->load->model( 'reportk/order');

		$this->document->addStyle( "view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css") ;
		$this->document->addScript( "view/javascript/jquery/datetimepicker/moment.js") ;
		$this->document->addScript( "view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js") ;

		// message area============================================================================
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		// 麵包屑 Start ------------------------------------------------------------------------------
		$data['breadcrumbs'] = array();
		$url = "" ;
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title_today'),
			'href' => $this->url->link( $this->func_path, 'token=' . $this->session->data['token'] . $url, true)
		);
		// 麵包屑 End ---------------------------------------------------------------------------------

		// 整理列表搜尋條件
		// "filter_fds" => "查詢啟始時間" , // 訂單日期
		// "filter_fde" => "查詢結束時間" , // 訂單日期
		// "filter_spt" => "查詢出車地點" ,
		// "filter_ept" => "查詢還車地點" ,
		// "filter_rdi" => "以還車時間查詢" ,
		// "filter_rde" => "以還車時間查詢" ,
		$filterArr = array (
			"filter_mod"     => "查詢出/還車時間" ,
			"filter_station" => "營業據點" ,
			"filter_man"     => "服務人員" ,

			"sort"           => "排序升降冪條件" ,
			"order"          => "欄位排序" ,
			"page"           => "頁碼" ,
			// "filter_limit" => "分頁數" ,
		) ;
		foreach ($filterArr as $col => $name) {
			$data["{$col}"] = isset($this->request->get["{$col}"]) ? trim($this->request->get["{$col}"]) : "";

			if (isset($this->request->get["{$col}"]) && !empty($this->request->get["{$col}"])) {
				// 欄位
				if ( $col != "order" && $col != "sort") {
					$this->column_url .= "&{$col}=" . $this->request->get["{$col}"] ;
				}
				// 分頁
				if ( $col != "page") {
					$this->page_url .= "&{$col}=" . $this->request->get["{$col}"] ;
				}
			}
		}
		if ( isset( $this->request->get['filter_mod'])) {
			$this->filter_mod = $this->request->get['filter_mod'] ;
			$setDate = $this->debug ? '2020-06-16' : date( 'Y-m-d') ;
			switch ( $this->request->get['filter_mod']) {
				case 'rent_out': // 當日租車
					$data['filter_fds'] = $setDate ;
					$data['filter_fde'] = $setDate ;
					break;
				case 'rent_in': // 當日還車
					$data['filter_rdi'] = $setDate ;
					$data['filter_rde'] = $setDate ;
					break;
			}
		}


		// 程式最後=============================================================
		$data['header']      = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer']      = $this->load->controller('common/footer');


		return $data ;
	}

	/**
	 * [makeFittingRow 建立配件資料]
	 * @param   array      $data         [description]
	 * @param   [type]     $strName      [description]
	 * @return  [type]                   [description]
	 * @Another Angus
	 * @date    2020-03-05
	 */
	protected function makeFittingRow( $data = array(), $strName, $keyName) {

		// $data['order_type'] 	 = $this->orderTypeArr[$data['order_type']] ;

		// $data['s_station']     = $this->stationArr[$data['s_station']] ;
		// $data['e_station']     = $this->stationArr[$data['e_station']] ;

		$data['car_no']        = "" ;
		$data['car_type']      = $strName ;
		$data['car_model']     = "" ;
		$data['minsu']         = "" ;

		$data['rent_out_date'] = !empty($data['rent_date_out']) ? substr($data['rent_date_out'], 0, 10) : "" ;
		$data['rent_out_time'] = !empty($data['rent_date_out']) ? substr($data['rent_date_out'], 11, 5) : "" ;
		$data['rent_in_date']  = !empty($data['rent_date_in_plan']) ? substr($data['rent_date_in_plan'], 0, 10) : "" ;
		$data['rent_in_time']  = !empty($data['rent_date_in_plan']) ? substr($data['rent_date_in_plan'], 11, 5) : "" ;

		$data['real_out_date'] = !empty($data['rent_date_out_true']) ? substr($data['rent_date_out_true'], 0, 10) : "" ;
		$data['real_out_time'] = !empty($data['rent_date_out_true']) ? substr($data['rent_date_out_true'], 11, 5) : "" ;
		$data['real_in_date']  = !empty($data['rent_date_in']) ? substr($data['rent_date_in'], 0, 10) : "" ;
		$data['real_in_time']  = !empty($data['rent_date_in']) ? substr($data['rent_date_in'], 11, 5) : "" ;
		if ( $keyName == "mobile_moto") {
			$data['plug_cost'] = $data['total_day'] * 50 ;
			// if ( $data['total_day'] > 1) {
			// 	$data['plug_cost'] = 100 + ( $data['total_day']-1) * 50 ;
			// } else {
			// 	$data['plug_cost'] = 100 ;
			// }
		} else {
			$data['plug_cost'] = $data['total_day'] * 100 ;
		}
		$data['plug_cost'] = $data['plug_cost'] * $data[$keyName] ;

		$data['lose_type']     = "" ;
		$data['oil_cost']      = "" ;
		$data['overTime_cost'] = "" ;
		$data['business_lose'] = "" ;
		$data['car_lose']      = "" ;
		$data['other_lose']    = "" ;

		$data['members']       = "" ;
		$data['total']         = "" ;
		$data['income']        = "" ;
		$data['spdc']          = "" ;


		if ( $this->request->get['filter_mod'] == 'rent_out') {
			$this->dashboardItem['plug_cost']['cnt'] ++ ;
			$this->dashboardItem['plug_cost']['total'] += $data['plug_cost'] ;

			switch ($data['pay_type']) {
				case '信用卡':
					$this->dashboardItem['credit_card']['cnt'] ++ ;
					$this->dashboardItem['credit_card']['total'] += $data['plug_cost'] ;
					break;
				case '現金':
					$this->dashboardItem['cash']['cnt'] ++ ;
					$this->dashboardItem['cash']['total'] += $data['plug_cost'] ;
					break;
				case '匯款':
					$this->dashboardItem['remittance']['cnt'] ++ ;
					$this->dashboardItem['remittance']['total'] += $data['plug_cost'] ;
					break;
				// case '4':
				// 	$this->dashboardItem['allOrder']['cnt'] ++ ;
				// 	$this->dashboardItem['allOrder']['total'] += $row['total'] ;
				// 	break;
			}
		}
		return $data ;
	}

	/**
	 * [blockDashboardMaps description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2020-05-30
	 */
	protected function blockDashboardMaps() {
		// if ( $this->request->get[''])
		switch ( $this->filter_mod) {
			case 'rent_out': // 當日租車
				return ['attr' => [
								['title'=>'#6951EF', 'body'=>'#A598ED'],
								['title'=>'#DD5D55', 'body'=>'#E39F9D'],
								['title'=>'#609CE9', 'body'=>'#A1C2EB'],
								['title'=>'#98EA68', 'body'=>'#BEECA2'],
							],
					'element' => [
								['allOrder', 'netOrder', 'minsuOrder', 'remittance'],
								['cash', 'credit_card'],
								['insurance', 'plug_cost'],
								['spdc'],
							]
					] ;
				break ;
			case 'rent_in': // 當日還車
				return ['attr' => [
								['title'=>'#6951EF', 'body'=>'#A598ED'],
								['title'=>'#DD5D55', 'body'=>'#E39F9D'],
								['title'=>'#E7B362', 'body'=>'#E9CDA1'],
							],
					'element' => [
								['allOrder', 'netOrder', 'minsuOrder', 'remittance'],
								['cash', 'credit_card'],
								['overTime', 'car_lose', 'business_lose', 'oil_cost', 'other_cost'],
							]
					] ;
				break ;
		}
	}

	/**
	 * [makeDashboardItem description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2020-04-05
	 */
	protected function makeDashboardItem() {
		return array(
			"allOrder"      => array("name" => "全部訂單",	"cnt" => 0, "total" => 0),
			"cashOrder"     => array("name" => "當日收現",	"cnt" => 0, "total" => 0),
			"netOrder"      => array("name" => "網路訂單",	"cnt" => 0, "total" => 0),
			"minsuOrder"    => array("name" => "業者 S",		"cnt" => 0, "total" => 0),

			"cash"          => array("name" => "現金",		"cnt" => 0, "total" => 0),
			"credit_card"   => array("name" => "刷卡",		"cnt" => 0, "total" => 0),
			"remittance"    => array("name" => "匯款",		"cnt" => 0, "total" => 0),

			"insurance"     => array("name" => "保險收入",	"cnt" => 0, "total" => 0),
			"plug_cost"     => array("name" => "配件費用",	"cnt" => 0, "total" => 0),

			"overTime"      => array("name" => "超時費用",	"cnt" => 0, "total" => 0),
			"car_lose"      => array("name" => "車體損失",	"cnt" => 0, "total" => 0),
			"business_lose" => array("name" => "營業損失",	"cnt" => 0, "total" => 0),
			"oil_cost"      => array("name" => "油費",		"cnt" => 0, "total" => 0),
			"other_cost"    => array("name" => "其他損失",	"cnt" => 0, "total" => 0),

			"spdc"          => array("name" => "業務折抵",	"cnt" => 0, "total" => 0),
		) ;
	}

	/**
	 * [getPayType 租車付款方式]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2020-04-08
	 */
	protected function getPayType() {
		return [	'1' => '信用卡',
					'2' => '現金',
					'3' => '匯款',
					'4' => '應收',
						] ;
	}
	/**
	 * [getLoseType 回程收費付款方式]
	 * @return  [type]     [description]
	 * 1應收, 2已收, 3未收, 4現金, 5信用卡
	 * @Another Angus
	 * @date    2020-06-14
	 */
	protected function getLoseType() {
		return [	'1' => '應收',
					'2' => '已收',
					'3' => '未收',
					'4' => '現金',
					'5' => '信用卡',
						] ;
	}

	/**
	 * [exportData description]
	 * @return  [type]     [description]
	 * https://www.cnblogs.com/connect/p/php-export-excel.html
	 * https://malagege.github.io/blog/2019/08/08/PHPExcel-%E4%BD%BF%E7%94%A8%E5%B0%8F%E8%A8%98/
	 * @Another Angus
	 * @date    2020-05-03
	 */
	protected function exportExcel( $columnNames, $data, $range) {
		ini_set("max_execution_time", 0) ;
		ini_set("memory_limit","2048M") ;
		include_once( DIR_SYSTEM . 'library/PHPExcel.php') ;

		$exportDate = date( 'Y-m-d') ;
		$fileName   = "金豐報表" . $exportDate ;
		$fileType   = "xlsx";

		$objPHPExcel = new PHPExcel() ;
		$objPHPExcel->setActiveSheetIndex(0) ;
		$objPHPExcel->getActiveSheet()->setTitle( $range[0].'~'.$range[1]) ;

		// $objPHPExcel = PHPExcel_IOFactory::load( $this->uploadFileName) ;

		list( $colIndex, $lastIndex) = $this->makeExcelIndex( $columnNames) ;
		foreach ($columnNames as $kName => $colName) {
			$objPHPExcel->getActiveSheet()->setCellValue($colIndex[$kName].'2', $colName) ;
		}

		foreach ($data as $iCnt => $row) {
			foreach ($columnNames as $kName => $colName) {
				if ($colIndex[$kName] == 'A') {
					$objPHPExcel->getActiveSheet()->setCellValue( $colIndex[$kName].(3+$iCnt), '\''.$row[$kName], PHPExcel_Cell_DataType::TYPE_STRING2 ) ;
				} else {
					$objPHPExcel->getActiveSheet()->setCellValue( $colIndex[$kName].(3+$iCnt), @$row[$kName], PHPExcel_Cell_DataType::TYPE_STRING2 ) ;
				}

			}
		}

		//凍結窗格
		$objPHPExcel->getActiveSheet()->freezePane('A3') ;

		// 主要欄位設顏色
		$objPHPExcel->getActiveSheet()->getStyle("A2:".$lastIndex."2")->applyFromArray(
			array(
				"fill" => array(
				"type" => PHPExcel_Style_Fill::FILL_SOLID,
				"color"=> array("rgb" => "FFFFAB") ),
			)
		);
		// 用陣列設定 全框線
		$style_array = array(
		    'borders' => array(
		        'allborders' => array(
		            'style' => PHPExcel_Style_Border::BORDER_THIN,
		            'color' => array('rgb' => '000000') ),
		    )
		);
		ob_clean();
        if ($fileType == 'xls') {
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="' . $fileName . '.xls');
            header('Cache-Control: max-age=1');
            $objWriter = new \PHPExcel_Writer_Excel5($objPHPExcel);
            $objWriter->save('php://output');
            exit;
        } elseif ($fileType == 'xlsx') {
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $fileName . '.xlsx');
            header('Cache-Control: max-age=1');
            $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save('php://output');
            exit;
        }
	}

	/**
	 * [makeExcelIndex description]
	 * @param   [type]     $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2020-05-03
	 */
	protected function makeExcelIndex( $data) {
		$i         = 0 ;
		$charIndex = 65 ;
		$colIndex  = [] ;
		$lastIndex = "" ;

		foreach ($data as $key => $value) {
			if ( intval($i / 26) == 0) {
				// echo "index : {$i} | {$value} => ".chr( $charIndex + $i) . "\n" ;
				$colIndex[$key] = $lastIndex = chr( $charIndex + $i) ;
			} else {
				$mod = intval($i / 26) - 1 ;
				$j = $i % 26 ;
				echo "index : {$i} | {$value} => ".chr( $charIndex + $mod).chr( $charIndex + $j). "\n" ;
				$colIndex[$key] = $lastIndex = chr( $charIndex + $mod).chr( $charIndex + $j) ;
			}
			$i++ ;
		}
		return [$colIndex, $lastIndex] ;
	}

	/**
	 * [computing_accessories description]
	 * @param   array      $data [description]
	 * @return  [type]           [description]
	 * @Another Angus
	 * @date    2020-10-29
	 */
	protected function computing_accessories( $data = []) {
		/**
		 * 兒童安全座椅〔一歲以下〕
		 */
		if ( $data['baby_chair'] > 0) {
			$data['total'] = $data['total'] - $data['total_day'] * 100 * $data['baby_chair'] ;
		}
		/**
		 * 兒童安全座椅〔一歲以上〕
		 */
		if ( $data['baby_chair1'] > 0) {
			$data['total'] = $data['total'] - $data['total_day'] * 100 * $data['baby_chair1'] ;
		}
		/**
		 * 兒童安全座椅〔增高墊〕
		 */
		if ( $data['baby_chair2'] > 0) {
			$data['total'] = $data['total'] - $data['total_day'] * 100 * $data['baby_chair2'] ;
		}
		/**
		 * 嬰兒推車
		 */
		if ( $data['children_chair'] > 0) {
			$data['total'] = $data['total'] - $data['total_day'] * 100 * $data['children_chair'] ;
		}
		/**
		 * GPS衛星導航系統
		 */
		// if ( $data['gps'] > 0) {
		// 	$data['total'] = $data['total'] - $data['total_day'] * 100 * $data['gps'] ;
		// }
		/**
		 * 機車導航架
		 */
		if ( $data['mobile_moto'] > 0) {
			$data['total'] = $data['total'] - $data['total_day'] * 50 * $data['mobile_moto'] ;
		}
		// if ( $data['mobile_moto'] > 0) {
		// 	if ( $data['total_day'] > 1) {
		// 		$cost = 100 + ( $data['total_day']-1) * 50 ;
		// 	} else {
		// 		$cost = 100 ;
		// 	}
		// 	$data['total'] = $data['total'] - $cost * $data['mobile_moto'] ;
		// }
		// dump( $data['total']) ;
		return $data ;
	}
}
