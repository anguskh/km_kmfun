var gridViewScroll = null;
window.onload = function () {
    gridViewScroll = new GridViewScroll({
        elementID: "gvMain",
        width: "100%",
        height: 586,
        freezeColumn: true,
        freezeFooter: false,
        freezeColumnCssClass: "GridViewScrollItemFreeze",
        freezeFooterCssClass: "GridViewScrollFooterFreeze",
        freezeHeaderRowCount: 3,
        freezeColumnCount: 3,
        onscroll: function (scrollTop, scrollLeft) {
            console.log(scrollTop + " - " + scrollLeft);
        }
    });
    gridViewScroll.enhance();
}