<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <a href="<?php echo $url_add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-list').submit() : false;"><i class="fa fa-trash-o"></i></button>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <!--===================================== 訊息提示 Start =======================================-->
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <!--======================================= 訊息提示 End =======================================-->
    <div class="panel panel-default">
        <!--================================= 標題 Start ===========================================-->
        <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
        </div>
        <!--================================= 標題 End =============================================-->
      <div class="panel-body">
        <form action="<?php echo $url_delete; ?>" method="post" enctype="multipart/form-data" id="form-list">
          <div class="table-responsive">
<table class="table table-bordered table-hover">
<?php foreach($mainCar as $index => $carModelName) :
	$checkStr = "" ;
	foreach( $selSeedArr as $seled) {
		if ( $seled == $index ) $checkStr =  "checked" ;
	}
?>
	<tr>
		<td nowrap="nowrap"><input type="checkbox" name="carSeed-<?=$index?>" value="<?=$index?>" <?=$checkStr?>></td>
		<td nowrap="nowrap"><?=$carModelName?></td>
		<td>
			<?php foreach( $searchCarModelArr[0][$index] as $carSeedNo) :
					$checkStr = "" ;
					foreach( $selStatusArr as $sel_status) {
						if ( $sel_status == $carSeedNo ) $checkStr =  "checked" ;
					}
			?>
			<input type="checkbox" name="carId-<?=$carSeedNo?>" value="<?=$carSeedNo?>" <?=$checkStr?>/> <?=$searchCarModelArr[1][$carSeedNo]?>&nbsp;&nbsp;
			<?php endforeach ; ?>
		</td>
	</tr>
<?php endforeach ; ?>
</table>
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                  <?php foreach ($columnNames as $colKey => $columnName): ?>
                  <td class="text-left">
                    <a href="<?=$sortUrl[$colKey] ?>" class="<?=( $sort == $colKey) ? strtolower( $order) : '' ?>"><?php echo $columnName; ?></a>
                    </td>
                  <?php endforeach; ?>
                  <td class="text-right"><?php echo $column_action; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($results) { ?>
                <?php foreach ($results as $res):?>
                <tr>
                  <td class="text-center"><?php if (in_array($res['idx'], $selected)) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $res['idx']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $res['idx']; ?>" />
                    <?php } ?></td>
                  <?php foreach ($columnNames as $colKey => $columnName): ?>
                  <td class="text-left"><?php echo $res[$colKey]; ?></td>
                  <?php endforeach; ?>
                  <td class="text-right">
                  <a href="<?php echo $res['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                <?php endforeach; ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="<?=$td_colspan+2?>"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $indexDec; ?></div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$("input[name*='carSeed']").click( function () {
	var $obj = $(this).parent().parent() ;
	$obj.find('input[name*=\'carId\']').prop('checked', this.checked) ;

	var selStatusStr = "" ;
	$("input[name*='carId']:checked").each( function ( index) {
		selStatusStr += $(this).val() + '|' ;
	});

	var selMainStr = "" ;
	$("input[name*='carSeed']:checked").each( function ( index) {
		selMainStr += $(this).val() + '|' ;
	});

	window.location.href = "?route=site/cars&token=<?=$token?>&sel_status=" + selStatusStr + "&sel_seed=" + selMainStr ;
});
// 單獨選擇
$("input[name*='carId']").click( function () {
  var selStatusStr = "" ;
  $("input[type='checkbox']:checked").each( function ( index) {
    selStatusStr += $(this).val() + '|' ;
  });

  window.location.href = "?route=site/cars&token=<?=$token?>&sel_status=" + selStatusStr ;
}) ;
//--></script></div>
<?php echo $footer; ?>