<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<button type="submit" form="form-action" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $url_cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
				<h1><?php echo $heading_title; ?></h1>
				<ul class="breadcrumb">
					<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
					<?php } ?>
				</ul>
			</div>
		</div>
		<div class="container-fluid">
			<!--===================================== 訊息提示 Start =======================================-->
			<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
			<?php } ?>
			<!--===================================== 訊息提示 End =========================================-->
			<div class="panel panel-default">
				<!--================================= 標題 Start ===========================================-->
				<div class="panel-heading">
					<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
				</div>
				<!--================================= 標題 End =============================================-->
				<div class="panel-body">
					<form action="<?php echo $url_action; ?>" method="post" enctype="multipart/form-data" id="form-action" class="form-horizontal">
						<ul class="nav nav-tabs"><!-- 頁籤 -->
						<li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
					</ul>
					<div class="tab-content"><!-- 內容 -->
					<div class="tab-pane active" id="tab-general">
						<div class="tab-content">
<!-- 車型 -->
<div class="form-group required form-inline">
	<label class="col-sm-2 control-label" for="input-car-seed">車型</label>
	<div class="col-sm-2">
		<input type="text" name="input_car_seed" placeholder="車型"
		value="<?php echo isset( $input_car_seed) ? $input_car_seed : ''; ?>"
		id="input-car-seed" class="form-control" />
		<?php if ( isset( $error_car_seed)) { ?>
		<div class="text-danger"><?php echo $error_car_seed; ?></div>
		<?php } ?>
	</div>
</div>
<!-- 車種 -->
<div class="form-group required form-inline">
	<label class="col-sm-2 control-label" for="input-car-model">車種</label>
	<div class="col-sm-10">
		<select name="sel_car_model" id="input-car-model" class="form-control">
			<option value="">--- 請選擇 ---</option>
			<?php foreach( $car_model_arr as $val => $tmpStr) :
					$seledStr = ( $sel_car_model == $val) ? "selected" : "" ;
			?>
				<option value="<?=$val?>" <?=$seledStr?> ><?=$tmpStr?></option>
			<?php endforeach ; ?>
		</select>
		<?php if ( isset( $error_car_model)) { ?>
		<div class="text-danger"><?php echo $error_car_model; ?></div>
		<?php } ?>
	</div>
</div>
<div class="form-group">
	<div class="col-md-3 col-sm-6 col-xs-6" >
		<label class="col-sm-5 control-label">車型圖片：</label>
		<div class="col-sm-7"><a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
			<img src="<?php echo $thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
			<input type="hidden" name="image" value="<?php echo $image; ?>" id="input-image" />
		</div>
	</div>
	<div class="col-md-3 col-sm-6 col-xs-6">
		<label class="col-sm-5 control-label">介紹圖片1：</label>
		<div class="col-sm-7"><a href="" id="thumb-imageIntro1" data-toggle="image" class="img-thumbnail">
			<img src="<?php echo $thumb1; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
			<input type="hidden" name="imageIntro1" value="<?php echo $imageIntro1; ?>" id="input-imageIntro1" />
		</div>
	</div>
	<div class="col-md-3 col-sm-6 col-xs-6">
		<label class="col-sm-5 control-label">介紹圖片2：</label>
		<div class="col-sm-7"><a href="" id="thumb-imageIntro2" data-toggle="image" class="img-thumbnail">
			<img src="<?php echo $thumb2; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
			<input type="hidden" name="imageIntro2" value="<?php echo $imageIntro2; ?>" id="input-imageIntro2" />
		</div>
	</div>
	<div class="col-md-3 col-sm-6 col-xs-6">
		<label class="col-sm-5 control-label">介紹圖片3：</label>
		<div class="col-sm-7"><a href="" id="thumb-imageIntro3" data-toggle="image" class="img-thumbnail">
			<img src="<?php echo $thumb3; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
			<input type="hidden" name="imageIntro3" value="<?php echo $imageIntro3; ?>" id="input-imageIntro3" />
		</div>
	</div>
</div>
<!-- 年份 -->
<div class="form-group">
	<label class="col-sm-2 control-label" for="input-car-year">年份</label>
	<div class="col-sm-10">
		<input type="text" name="input_car_year" placeholder="年份"
		value="<?php echo isset( $input_car_year) ? $input_car_year : ''; ?>"
		id="input-car-year" class="form-control" />
	</div>
</div>
<!-- 單價 -->
<div class="form-group required">
	<label class="col-sm-2 control-label" for="input-car-price">單價</label>
	<div class="col-sm-10">
		<input type="text" name="input_car_price" placeholder="單價"
		value="<?php echo isset( $input_car_price) ? $input_car_price : ''; ?>"
		id="input-car-price" class="form-control" />
		<?php if ( isset( $error_car_price)) { ?>
		<div class="text-danger"><?php echo $error_car_price; ?></div>
		<?php } ?>
	</div>
</div>
<!-- 座位數 -->
<div class="form-group">
	<label class="col-sm-2 control-label" for="input-car-peopele">座位數</label>
	<div class="col-sm-10">
		<input type="text" name="input_car_peopele" placeholder="座位數"
		value="<?php echo isset( $input_car_peopele) ? $input_car_peopele : ''; ?>"
		id="input-car-peopele" class="form-control" />
	</div>
</div>
<!-- 車門數 -->
<div class="form-group">
	<label class="col-sm-2 control-label" for="input-car-door">車門數</label>
	<div class="col-sm-10">
		<input type="text" name="input_car_door" placeholder="車門數"
		value="<?php echo isset( $input_car_door) ? $input_car_door : ''; ?>"
		id="input-car-door" class="form-control" />
	</div>
</div>
<!-- 燃油 -->
<div class="form-group">
	<label class="col-sm-2 control-label" for="input-gas-class">燃油</label>
	<div class="col-sm-10">
		<input type="text" name="input_gas_class" placeholder="燃油"
		value="<?php echo isset( $input_gas_class) ? $input_gas_class : ''; ?>"
		id="input-gas-class" class="form-control" />
	</div>
</div>
<!-- 燃油種類 -->
<div class="form-group">
	<label class="col-sm-2 control-label" for="input-gas-type">燃油種類</label>
	<div class="col-sm-10">
		<input type="text" name="input_gas_type" placeholder="燃油種類"
		value="<?php echo isset( $input_gas_type) ? $input_gas_type : ''; ?>"
		id="input-gas-type" class="form-control" />
	</div>
</div>
<!-- 排氣量 -->
<div class="form-group">
	<label class="col-sm-2 control-label" for="input-displacement">排氣量</label>
	<div class="col-sm-10">
		<input type="text" name="input_displacement" placeholder="排氣量"
		value="<?php echo isset( $input_displacement) ? $input_displacement : ''; ?>"
		id="input-displacement" class="form-control" />
	</div>
</div>
<!-- 變速系統 -->
<div class="form-group">
	<label class="col-sm-2 control-label" for="input-at_mt">變速系統</label>
	<div class="col-sm-10">
		<input type="text" name="input_at_mt" placeholder="變速系統"
		value="<?php echo isset( $input_at_mt) ? $input_at_mt : ''; ?>"
		id="input-at_mt" class="form-control" />
	</div>
</div>
<!-- 倒車雷達 -->
<div class="form-group">
	<label class="col-sm-2 control-label" for="input-back_radar">倒車雷達</label>
	<div class="col-sm-10">
		<input type="text" name="input_back_radar" placeholder="倒車雷達"
		value="<?php echo isset( $input_back_radar) ? $input_back_radar : ''; ?>"
		id="input-back_radar" class="form-control" />
	</div>
</div>
<!-- CD -->
<div class="form-group">
	<label class="col-sm-2 control-label" for="input-cd_player">CD</label>
	<div class="col-sm-10">
		<input type="text" name="input_cd_player" placeholder="CD"
		value="<?php echo isset( $input_cd_player) ? $input_cd_player : ''; ?>"
		id="input-cd_player" class="form-control" />
	</div>
</div>
<!-- MP3 -->
<div class="form-group">
	<label class="col-sm-2 control-label" for="input-mp3">MP3</label>
	<div class="col-sm-10">
		<input type="text" name="input_mp3" placeholder="MP3"
		value="<?php echo isset( $input_mp3) ? $input_mp3 : ''; ?>"
		id="input-mp3" class="form-control" />
	</div>
</div>
<!-- 廣播 -->
<div class="form-group">
	<label class="col-sm-2 control-label" for="input-radio">廣播</label>
	<div class="col-sm-10">
		<input type="text" name="input_radio" placeholder="廣播"
		value="<?php echo isset( $input_radio) ? $input_radio : ''; ?>"
		id="input-radio" class="form-control" />
	</div>
</div>
<!-- USB充電器 -->
<div class="form-group">
	<label class="col-sm-2 control-label" for="input-usb_charge">USB充電器</label>
	<div class="col-sm-10">
		<input type="text" name="input_usb_charge" placeholder="USB充電器"
		value="<?php echo isset( $input_usb_charge) ? $input_usb_charge : ''; ?>"
		id="input-usb_charge" class="form-control" />
	</div>
</div>
<!-- 衛星導航 -->
<div class="form-group">
	<label class="col-sm-2 control-label" for="input-gps">衛星導航</label>
	<div class="col-sm-10">
		<input type="text" name="input_gps" placeholder="衛星導航"
		value="<?php echo isset( $input_gps) ? $input_gps : ''; ?>"
		id="input-gps" class="form-control" />
	</div>
</div>
<!-- 行車紀錄器 -->
<div class="form-group">
	<label class="col-sm-2 control-label" for="input-driving_recorder">行車紀錄器</label>
	<div class="col-sm-10">
		<input type="text" name="input_driving_recorder" placeholder="行車紀錄器"
		value="<?php echo isset( $input_driving_recorder) ? $input_driving_recorder : ''; ?>"
		id="input-driving_recorder" class="form-control" />
	</div>
</div>
<!-- 安全氣囊 -->
<div class="form-group">
	<label class="col-sm-2 control-label" for="input-airbag">安全氣囊</label>
	<div class="col-sm-10">
		<input type="text" name="input_airbag" placeholder="安全氣囊"
		value="<?php echo isset( $input_airbag) ? $input_airbag : ''; ?>"
		id="input-airbag" class="form-control" />
	</div>
</div>
<!-- 安全氣囊位置 -->
<div class="form-group">
	<label class="col-sm-2 control-label" for="input-airbag_pos">安全氣囊位置</label>
	<div class="col-sm-10">
		<input type="text" name="input_airbag_pos" placeholder="安全氣囊位置"
		value="<?php echo isset( $input_airbag_pos) ? $input_airbag_pos : ''; ?>"
		id="input-airbag_pos" class="form-control" />
	</div>
</div>
<!-- 頁面狀態 -->
<div class="form-group">
	<label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
	<div class="col-sm-10">
		<select name="sel_status" id="input-status" class="form-control">
			<?php if ($sel_status==1) { ?>
			<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
			<option value="0"><?php echo $text_disabled; ?></option>
			<?php } else { ?>
			<option value="1"><?php echo $text_enabled; ?></option>
			<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
			<?php } ?>
		</select>
	</div>
</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script></div>
<?php echo $footer; ?>