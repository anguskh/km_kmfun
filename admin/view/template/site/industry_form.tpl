<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-action" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $url_cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
	<!--===================================== 訊息提示 Start =======================================-->
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <!--===================================== 訊息提示 End =========================================-->
    <div class="panel panel-default">
    	<!--================================= 標題 Start ===========================================-->
		<div class="panel-heading">
		<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
		</div>
		<!--================================= 標題 End =============================================-->
        <div class="panel-body">
            <form action="<?php echo $url_action; ?>" method="post" enctype="multipart/form-data" id="form-action" class="form-horizontal">
                <ul class="nav nav-tabs"><!-- 頁籤 -->
                    <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                </ul>
            <div class="tab-content"><!-- 內容 -->
                <div class="tab-pane active" id="tab-general">
                    <div class="tab-content">
<input type="hidden" name="input_idx" value="<?php echo isset( $input_idx) ? $input_idx : ''; ?>">
<!-- 業者名稱 -->
<div class="form-group required">
	<label class="col-sm-2 control-label" for="input-title"><?php echo $entry_name; ?></label>
	<div class="col-sm-10">
		<input type="text" name="input_name" placeholder="<?php echo $entry_name; ?>"
			value="<?php echo isset( $input_name) ? $input_name : ''; ?>"
			id="input-name" class="form-control" />
		<?php if ($error_name != '') { ?>
		<div class="text-danger"><?php echo $error_name; ?></div>
		<?php } ?>
	</div>
</div>
<!-- 連絡人姓名 -->
<div class="form-group required">
	<label class="col-sm-2 control-label" for="input-title"><?php echo $entry_man_name; ?></label>
	<div class="col-sm-10">
		<input type="text" name="input_man_name" placeholder="<?php echo $entry_man_name; ?>"
			value="<?php echo isset( $input_man_name) ? $input_man_name : ''; ?>"
			id="input-man-name" class="form-control" />
		<?php if ($error_man_name != '') { ?>
		<div class="text-danger"><?php echo $error_man_name; ?></div>
		<?php } ?>
	</div>
</div>
<!-- 連絡人電話 -->
<div class="form-group required">
	<label class="col-sm-2 control-label" for="input-title"><?php echo $entry_tel; ?></label>
	<div class="col-sm-10">
		<input type="text" name="input_tel" placeholder="<?php echo $entry_tel; ?>"
			value="<?php echo isset( $input_tel) ? $input_tel : ''; ?>"
			id="input-tel" class="form-control" />
		<?php if ($error_tel != '') { ?>
		<div class="text-danger"><?php echo $error_tel; ?></div>
		<?php } ?>
	</div>
</div>
<!-- 連絡人Mail -->
<div class="form-group ">
	<label class="col-sm-2 control-label" for="input-title"><?php echo $entry_mail; ?></label>
	<div class="col-sm-10">
		<input type="text" name="input_mail" placeholder="<?php echo $entry_mail; ?>"
			value="<?php echo isset( $input_mail) ? $input_mail : ''; ?>"
			id="input-mail" class="form-control" />
	</div>
</div>
<!-- 連絡人地址 -->
<div class="form-group ">
	<label class="col-sm-2 control-label" for="input-title"><?php echo $entry_address; ?></label>
	<div class="col-sm-10">
		<input type="text" name="input_address" placeholder="<?php echo $entry_address; ?>"
			value="<?php echo isset( $input_address) ? $input_address : ''; ?>"
			id="input-address" class="form-control" />
	</div>
</div>
<!-- 頁面狀態 -->
<div class="form-group">
	<label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
	<div class="col-sm-10">
		<select name="sel_status" id="input-status" class="form-control">
			<?php if ($sel_status) { ?>
			<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
			<option value="2"><?php echo $text_disabled; ?></option>
			<?php } else { ?>
			<option value="1"><?php echo $text_enabled; ?></option>
			<option value="2" selected="selected"><?php echo $text_disabled; ?></option>
			<?php } ?>
		</select>
	</div>
</div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
  </div>
  <script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>
  <link href="view/javascript/summernote/summernote.css" rel="stylesheet" />
  <script type="text/javascript" src="view/javascript/summernote/opencart.js"></script>
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script></div>
<?php echo $footer; ?>