<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-action" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $url_cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
	<!--===================================== 訊息提示 Start =======================================-->
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <!--===================================== 訊息提示 End =========================================-->
    <div class="panel panel-default">
    	<!--================================= 標題 Start ===========================================-->
		<div class="panel-heading">
		<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
		</div>
		<!--================================= 標題 End =============================================-->
        <div class="panel-body">
            <form action="<?php echo $url_action; ?>" method="post" enctype="multipart/form-data" id="form-action" class="form-horizontal">
                <ul class="nav nav-tabs"><!-- 頁籤 -->
                    <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                </ul>
            <div class="tab-content"><!-- 內容 -->
                <div class="tab-pane active" id="tab-general">
                    <div class="tab-content">
<!-- 車種 -->
<div class="form-group required">
	<label class="col-sm-2 control-label" for="input-car-seed">車型</label>
	<div class="col-sm-2">
		<select name="sel_car_seed" id="input-car-seed" class="form-control">
			<option value="">--- 請選擇車型 ---</option>
			<?php foreach( $selCarSeedOpt as $val => $tmpStr) :
					$seledStr = ( $sel_car_seed == $val) ? "selected" : "" ;
			?>
				<option value="<?=$val?>" <?=$seledStr?> ><?=$tmpStr?></option>
			<?php endforeach ; ?>
		</select>
		<?php if ( $error_car_seed != '') { ?>
		<div class="text-danger"><?php echo $error_car_seed; ?></div>
		<?php } ?>
	</div>
</div>
<!-- 車號 -->
<div class="form-group required">
	<label class="col-sm-2 control-label" for="input-car-no">車牌/編號</label>
	<div class="col-sm-10">
		<input type="text" name="input_car_no" placeholder="車牌/編號"
			value="<?php echo isset( $input_car_no) ? $input_car_no : ''; ?>"
			id="input-title" class="form-control" />
		<?php if ($error_car_no != '') { ?>
		<div class="text-danger"><?php echo $error_car_no; ?></div>
		<?php } ?>
	</div>
</div>
<!-- 顏色 -->
<div class="form-group required">
	<label class="col-sm-2 control-label" for="input-car-color">顏色</label>
	<div class="col-sm-10">
		<input type="text" name="input_car_color" placeholder="顏色"
			value="<?php echo isset( $input_car_color) ? $input_car_color : ''; ?>"
			id="input-car-color" class="form-control" />
		<?php if ($error_car_color != '') { ?>
		<div class="text-danger"><?php echo $error_car_color; ?></div>
		<?php } ?>
	</div>
</div>

<!-- 上線日期 -->
<div class="form-group">
	<label class="col-sm-2 control-label" for="input-date-added"><?php echo $entry_online_date; ?></label>
		<div class="col-sm-3">
			<div class="input-group date">
			<input type="text" name="input_online_date"
				value="<?php echo $input_online_date; ?>"
				placeholder="<?php echo $entry_online_date; ?>"
				data-date-format="YYYY-MM-DD"
				id="input-online-date" class="form-control" />
			<span class="input-group-btn">
				<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
			</span>
		</div>
	</div>
</div>
<!-- 頁面狀態 -->
<div class="form-group">
	<label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
	<div class="col-sm-10">
		<select name="sel_status" id="input-status" class="form-control">
			<?php if ($sel_status==1) { ?>
			<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
			<option value="2"><?php echo $text_disabled; ?></option>
			<?php } else { ?>
			<option value="1"><?php echo $text_enabled; ?></option>
			<option value="2" selected="selected"><?php echo $text_disabled; ?></option>
			<?php } ?>
		</select>
	</div>
</div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script></div>
<?php echo $footer; ?>