<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<!-- 功能按鈕 預留區 -->
			</div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<!--===================================== 訊息提示 Start =======================================-->
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<?php if ($success) { ?>
		<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<!--======================================= 訊息提示 End =======================================-->
		<div class="panel panel-default">
			<!--================================= 標題 Start ===========================================-->
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
			</div>
			<!--================================= 標題 End =============================================-->
			<div class="panel-body">
				<div class="well">
					<div class="row">
						<div class="col-sm-2">
							<div class="form-group">
								<label class="control-label" for="input-order-id">訂單 ID</label>
								<input type="text" name="filter_order_id" value="<?=$filter_order_id;?>" placeholder="訂單 ID" id="input-order-id" class="form-control" />
							</div>
							<div class="form-group">
								<label class="control-label" for="input-customer">訂車人</label>
								<input type="text" name="filter_customer" value="<?=$filter_customer;?>" placeholder="客戶" id="input-customer" class="form-control" />
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label class="control-label" for="input-order-type">訂單類別</label>
								<select name="filter_order_type" id="input-order-type" class="form-control">
									<option value=""></option>
									<?php foreach ($optOrderType as $optKey => $optName) : ?>
										<option value="<?=$optKey?>" <?=($optKey == $filter_order_type) ? "selected" : "" ?>><?=$optName?></option>
									<?php endforeach ; ?>
								</select>
							</div>
							<div class="form-group">
								<label class="control-label" for="input-order-status">訂單狀態</label>
								<select name="filter_order_status" id="input-order-status" class="form-control">
									<option value="*"></option>
									<option value="success" <?=($filter_order_status == 'success') ? "selected" : "" ?>>成立</option>
									<option value="x" <?=($filter_order_status == 'x') ? "selected" : "" ?> >刪除</option>
								</select>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label class="control-label" for="input-station">取車地點</label>
								<select name="filter_station" id="input-station" class="form-control">
									<option value=""></option>
									<?php foreach ($optStation as $optKey => $optName) : ?>
										<option value="<?=$optKey?>" <?=($optKey == $filter_station) ? "selected" : "" ?>><?=$optName?></option>
									<?php endforeach ; ?>
								</select>
							</div>
							<div class="form-group">
								<label class="control-label" for="input-mobile">取車人電話</label>
								<input type="text" name="filter_mobile" value="<?=$filter_mobile;?>" placeholder="客戶電話" id="input-total" class="form-control" />
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label class="control-label" for="input-date-added">取車日期</label>
								<div class="input-group date">
									<input type="text" name="filter_date_out" value="<?=$filter_date_out?>" placeholder="出車日期" data-date-format="YYYY-MM-DD" id="input-date-out" class="form-control" />
									<span class="input-group-btn">
										<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
									</span></div>
								</div>
								<div class="form-group">
									<label class="control-label" for="input-date-modified">修改日期</label>
									<div class="input-group date">
										<input type="text" name="filter_date_modified" value="" placeholder="修改日期" data-date-format="YYYY-MM-DD" id="input-date-modified" class="form-control" />
										<span class="input-group-btn">
											<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
										</span></div>
									</div>
									<button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-filter"></i> <?php echo $button_filter; ?></button>
								</div>
							</div>
						</div>



				<form action="<?php echo $url_delete; ?>" method="post" enctype="multipart/form-data" id="form-list">
					<div class="table-responsive">
						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
									<?php foreach ($columnNames as $columnName): ?>
									<td class="text-left">
										<a href="" class=""><?php echo $columnName; ?></a>
									</td>
									<?php endforeach; ?>
									<td class="text-right"><?php echo $column_action; ?></td>
								</tr>
							</thead>
							<tbody>
								<?php if ($results) { ?>
								<?php foreach ($results as $res):?>
								<tr>
									<td class="text-center"><?php if (in_array($res['order_id'], $selected)) { ?>
										<input type="checkbox" name="selected[]" value="<?php echo $res['order_id']; ?>" checked="checked" />
										<?php } else { ?>
										<input type="checkbox" name="selected[]" value="<?php echo $res['order_id']; ?>" />
									<?php } ?></td>
									<?php foreach ($columnNames as $cKey => $columnName): ?>
									<td class="text-left" <?php if ($cKey != "car_no") echo "nowrap" ;?>><?php echo $res[$cKey]; ?></td>
									<?php endforeach; ?>
									<td class="text-right">
										<a href="<?php echo $res['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
								</tr>
								<?php endforeach; ?>
								<?php } else { ?>
									<tr>
										<td class="text-center" colspan="<?=$td_colspan+2?>"><?php echo $text_no_results; ?></td>
									</tr>
								<?php } ?>
								</tbody>
							</table>
						</div>
					</form>
					<div class="row">
						<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
						<div class="col-sm-6 text-right"><?php echo $indexDec; ?></div>
					</div>
				</div>
			</div>
		</div>
		<script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
		<link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
		<script>
			$('#button-filter').on('click', function() {
				var url = '?route=site/<?php echo $now_urls?>&token=<?php echo $token; ?>';
					var filter_order_id = $('input[name=\'filter_order_id\']').val();
					if (filter_order_id) {
						url += '&filter_order_id=' + encodeURIComponent(filter_order_id);
					}
					var filter_order_type = $('select[name=\'filter_order_type\']').val();
					if (filter_order_type) {
						url += '&filter_order_type=' + encodeURIComponent(filter_order_type);
					}
					var filter_customer = $('input[name=\'filter_customer\']').val();
					if (filter_customer) {
						url += '&filter_customer=' + encodeURIComponent(filter_customer);
					}
					var filter_order_status = $('select[name=\'filter_order_status\']').val();
					if (filter_order_status != '*') {
						url += '&filter_order_status=' + encodeURIComponent(filter_order_status);
					}
					var filter_mobile = $('input[name=\'filter_mobile\']').val();
					if (filter_mobile) {
						url += '&filter_mobile=' + encodeURIComponent(filter_mobile);
					}
					var filter_station = $('select[name=\'filter_station\']').val();
					if (filter_station) {
						url += '&filter_station=' + encodeURIComponent(filter_station);
					}
					var filter_date_out = $('input[name=\'filter_date_out\']').val();
					if (filter_date_out) {
						url += '&filter_date_out=' + encodeURIComponent(filter_date_out);
					}
					var filter_date_modified = $('input[name=\'filter_date_modified\']').val();
					if (filter_date_modified) {
						url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
					}
				url += "&filter=filter" ;
				console.log( url) ;
				location = url;
			});


		</script>
		<script type="text/javascript"><!--
			$('.date').datetimepicker({
				pickTime: false
			});
	//--></script>
</div>
<?php echo $footer; ?>