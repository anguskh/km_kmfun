<!doctype html>
<html lang="zh-TW">
	<head>
		<meta charset="utf-8">
		<title>車輛行事曆</title>
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<meta name="designer" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

 		<script src="view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript" ></script>
		<script src="view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript" ></script>

		<link href="view/stylesheet/bootstrap.css" type="text/css" rel="stylesheet" />
		<link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />

		<link rel="stylesheet" href="view/stylesheet/default.css">

		<link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
		<script src="view/javascript/jquery/datetimepicker/moment.js" type="text/javascript"></script>
		<script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

		<!-- jquery autocomplete 必備 -->
		<script src="view/javascript/common.js" type="text/javascript"></script>
		<script>
			$(function(){
				var theWindow = $(window);
				var windowW = theWindow.width();
				var windowH = theWindow.height();
				mainFunction();
				$(window).resize(function() {
					mainFunction();
				});
				function mainFunction(){
					var picW = $("#calendar_box .box .tab-content .tableform li:last-child").width();
					$("#calendar_box .box .tab-content .tableform li textarea").width(picW-136);
					if (windowW < 1000) {
						$("#calendar_box .box .tab-content .tableform li textarea").width(picW);

					}
				};
			});
		</script>
	</head>
	<body id="calendar_box">
<div class="container-fluid" >
	<form action="<?php echo $url_submit; ?>" method="post" enctype="multipart/form-data" id="form-list">

<div class="container-fluid" >
	<div class="row title" style="color: #1976d2;">
		<h3 class="text-center"><?=$entry_title?>車輛行事曆<h3>
	</div>
	<div class="box">
		<div class="container">
			<input type="hidden" name="useCarIdx" value="<?=$carInfo['idx']?>">
			<input type="hidden" name="useCarSeed" value="<?=$carInfo['car_seed_idx']?>">
			<input type="hidden" name="useCarNo" value="<?=$carInfo['car_no']?>">
			<input type="hidden" name="scheduleIdx" id="scheduleIdx" value="<?=$scheduleIdx?>">
			<input type="hidden" name="agreement_no" id="agreement_no" value="<?=$agreement_no?>">

			<ul class="vehicle-info">
				<li class="col-md-4"><label for="">車種</label><?=$carInfo['car_model_name']?></li>
				<li class="col-md-4"><label for="">車型</label><?=$carInfo['car_seed']?></li>
				<li class="col-md-4"><label for="">牌照號碼</label><?=$carInfo['car_no']?></li>
			</ul>
			<!--===================================== 訊息提示 Start =======================================-->
			<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
			<?php } ?>
			<?php if ($success) { ?>
			<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
			<?php } ?>
			<!--======================================= 訊息提示 End =======================================-->
			<div class="panel panel-default">
				<div class="panel-body">
					<!-- 頁籤 -->
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab-general" data-toggle="tab"><span class="num">1</span><span class="txt">租車資訊</span></a></li>
						<li><a href="#tab-accessoriesPage"  data-toggle="tab"><span class="num">2</span><span class="txt">配件資訊</span></a></li>
						<li><a href="#tab-orderPage"  data-toggle="tab"><span class="num">3</span><span class="txt">客戶資訊</span></a></li>
						<li><a href="#tab-flightPage" data-toggle="tab"><span class="num">4</span><span class="txt">航船資訊</span></a></li>
					</ul>
					<!-- 頁籤 end -->
				</div><!-- panel-body End -->
				<div class="tab-content"><!-- 內容 -->
					<div class="tab-pane active" id="tab-general"><!-- tab-general 1. 租車資訊 Start -->
						<div class="tablebox">
							<ul class="tableform">
								<li class="col-md-6 col-sm-12">
									<label for="">訂單類別</label>
									<select name="sel_order_type" id="sel-order-type">
										<option value="">請選擇</option>
										<?php foreach( $orderType as $optVal => $optName) : ?>
											<option value="<?=$optVal?>" <?=( $optVal == $sel_order_type) ? "selected" : "" ?>><?=$optName?></option>
										<?php endforeach ; ?>
									</select>
								</li>
								<li class="col-md-6 col-sm-12">
									<div id="orderno" style="display:<?=$orderShow?>">
									<label for="">訂單編號</label>
									<?php if ( !empty($input_order_no)) : ?>
										<b><a href="<?=$orderUrl?>" target="_blank"><?=$input_order_no?></a></b>
									<?php endif ; ?>
									</div>
								</li>
								<li class="col-md-12">
									<!-- <div id="minsu" style="display:<?=$minsuShow?>"> -->
									<div id="minsu">
										<label for="">單位</label>
										<input type="text" name="input_minsu_name" size="100px" value="<?= $input_minsu_name ;?>" placeholder="民宿名稱：" class="form-control minsu">
										<input type="hidden" name="input_minsu_idx" value="<?=$input_minsu_idx?>">
									</div>
								</li>
								<li class="col-md-6 col-sm-12">
									<label for="">取車地點</label>
									<select name="sel_station_start">
										<option value="">請選擇</option>
										<?php foreach ($carStations as $iCnt => $station) : ?>
											<option value="<?=$station['idx']?>" <?=( $station['idx'] == $sel_s_stattion) ? "selected" : "" ?>><?=$station['opt_name']?></option>
										<?php endforeach ; ?>
									</select>
								</li>
								<li class="col-md-6 col-sm-12">
									<label for="">還車地點</label>
									<select name="sel_station_end">
										<option value="">請選擇</option>
										<?php foreach ($carStations as $iCnt => $station) : ?>
											<option value="<?=$station['idx']?>" <?=( $station['idx'] == $sel_e_stattion) ? "selected" : "" ?>><?=$station['opt_name']?></option>
										<?php endforeach ; ?>
									</select>
								</li>
								<li class="col-md-6 col-sm-12">
									<label for="">取車時間</label>
									<div class="input-group date">
										<input type="text" name="input_start_date" value="<?=$input_start_date?>" placeholder="開始日期" data-date-format="YYYY-MM-DD" id="input-start-date" class="form-control ptxt2 rentCost" />
										<select class="select rentCost" name="input_start_time">
										<?php foreach( $selRentTime as $tt) :
											$seledStr = "" ;
											if ( isset( $selStartTime) && $tt == $selStartTime) {
												$seledStr = "selected" ;
											}
										?>
											<option value="<?=$tt?>" <?=$seledStr?>><?=$tt?></option>
										<?php endforeach ; ?>
										</select>
										<span class="input-group-btn">
											<button class="btn btn-default" type="button">
												<i class="fa fa-calendar"></i>
											</button>
										</span>
									</div>
								</li>
								<li class="col-md-6 col-sm-12">
									<div class="input-group date">
										<label for="">還車時間</label>
										<input type="text" name="input_end_date" value="<?=$input_end_date?>" placeholder="結束日期" data-date-format="YYYY-MM-DD" id="input-end-date" class="form-control ptxt2 rentCost" />
										<select class="select rentCost" name="input_end_time">
										<?php foreach( $selRentTime as $tt) :
											$seledStr = "" ;
											if ( isset( $selEndTime) && $tt == $selEndTime) {
												$seledStr = "selected" ;
											}
										?>
											<option value="<?=$tt?>" <?=$seledStr?>><?=$tt?></option>
										<?php endforeach ; ?>
										</select>
										<span class="input-group-btn">
											<button class="btn btn-default" type="button">
												<i class="fa fa-calendar"></i>
											</button>
										</span>
									</div>
								</li>
								<li class="col-md-6 col-sm-12">
									<label for="">租車費用</label>
									<span><input type="text" name="input_total" value="<?=$totalRentCost?>" placeholder="租車費用"/></span>
								</li>
								<li class="col-md-6 col-sm-12">
									<label for="">收費方式</label>
									<span>
										<input type="radio" name="pay_type" value="1" <?php echo ($pay_type == 1) ? "checked" : "" ;?>>信用卡&nbsp;&nbsp;
										<input type="radio" name="pay_type" value="2" <?php echo ($pay_type == 2) ? "checked" : "" ;?>>現金&nbsp;&nbsp;
										<input type="radio" name="pay_type" value="3" <?php echo ($pay_type == 3) ? "checked" : "" ;?>>匯款&nbsp;&nbsp;
										<input type="radio" name="pay_type" value="4" <?php echo ($pay_type == 4) ? "checked" : "" ;?>>應收&nbsp;&nbsp;
									</span>
								</li>
								<li class="col-md-6 col-sm-12">
								</li>
								<li class="col-md-6 col-sm-12">
									共計&nbsp;<span id="haveDay" style="color: black;"><?=$rentDay?></span>&nbsp;天<span id="haveHour" style="color: black;"><?=$rentHour?></span>&nbsp;時
								</li>
								<li class="col-sm-12">
									<label for="">備註</label>
									<textarea name="area_memo" id="" cols="" rows="2" class="notebox"><?=$custInfo['area_memo']?></textarea>
								</li>
							</ul>
						</div>
					</div><!-- tab-general 1. 租車資訊 End -->
					<div class="tab-pane" id="tab-accessoriesPage"><!-- tab-accessoriesPage 2. 配件資訊 Start -->
						<div class="tablebox">
							<ul class="tableform tableform2">
								<?php if ( $carInfo['car_model'] == 7 ) : ?>
									<input type="hidden" name="sel_mobile_moto" value="0">
									<input type="hidden" name="sel_baby_chair" value="0">
									<input type="hidden" name="sel_baby_chair1" value="0">
									<input type="hidden" name="sel_baby_chair2" value="0">
									<input type="hidden" name="children_chair" value="0">
									<input type="hidden" name="sel_gps" value="0">
								<?php elseif ( $carInfo['car_model'] == 2 || $carInfo['car_model'] == 6 ) : ?>
								<li class="col-md-6 col-sm-12">
									<label for="" class="accessories"" id="baby_chair">機車導航架</label>
									<select class="select min rentCost" name="sel_mobile_moto">
										<?php for( $i = 0; $i < 4; $i++) :
											$val = str_pad($i, 1, '0', STR_PAD_LEFT) ;
										?>
										<option value="<?=$val?>" <?=( $val == $accessoriesInfo['sel_mobile_moto']) ? "selected" : "" ?>><?=$val?></option>
										<?php endfor ; ?>
									</select>
									<input type="hidden" id="sel_baby_chair" value="0">
									<input type="hidden" id="sel_baby_chair1" value="0">
									<input type="hidden" id="sel_baby_chair2" value="0">
									<input type="hidden" id="children_chair" value="0">
									<input type="hidden" id="sel_gps" value="0">
								</li>
								<?php else : ?>
								<!--<li class="col-md-6 col-sm-12">
									<input type="hidden" id="sel_mobile_moto" value="0">
									<label for="" class="accessories"" id="baby_chair">兒童椅〔一歲以下〕</label>
									<select class="select min rentCost" name="sel_baby_chair">
										<?php for( $i = 0; $i < 4; $i++) :
											$val = str_pad($i, 1, '0', STR_PAD_LEFT) ;
										?>
										<option value="<?=$val?>" <?=( $val == $accessoriesInfo['sel_baby_chair']) ? "selected" : "" ?>><?=$val?></option>
										<?php endfor ; ?>
									</select>
								</li>-->
								<li class="col-md-6 col-sm-12">
									<label for="" class="accessories"" id="baby_chair1">兒童椅〔一歲以上〕</label>
									<select class="select min rentCost" name="sel_baby_chair1">
										<?php for( $i = 0; $i < 4; $i++) :
											$val = str_pad($i, 1, '0', STR_PAD_LEFT) ;
										?>
										<option value="<?=$val?>" <?=( $val == $accessoriesInfo['sel_baby_chair1']) ? "selected" : "" ?>><?=$val?></option>
										<?php endfor ; ?>
									</select>
								</li>
								<li class="col-md-6 col-sm-12">
									<label for="" class="accessories"" id="baby_chair2">兒童椅〔增高墊〕</label>
									<select class="select min rentCost" name="sel_baby_chair2">
										<?php for( $i = 0; $i < 4; $i++) :
											$val = str_pad($i, 1, '0', STR_PAD_LEFT) ;
										?>
										<option value="<?=$val?>" <?=( $val == $accessoriesInfo['sel_baby_chair2']) ? "selected" : "" ?>><?=$val?></option>
										<?php endfor ; ?>
									</select>
								</li>
								<li class="col-md-6 col-sm-12">
									<label for="" class="accessories"" id="gps">GPS衛星導航系統</label>
									<select class="select min rentCost" name="sel_gps">
										<?php for( $i = 0; $i < 4; $i++) :
											$val = str_pad($i, 1, '0', STR_PAD_LEFT) ;
										?>
										<option value="<?=$val?>" <?=( $val == $accessoriesInfo['sel_gps']) ? "selected" : "" ?>><?=$val?></option>
										<?php endfor ; ?>
									</select>
								</li>
								<li class="col-md-6 col-sm-12">
									<label for="" class="accessories"" id="children_chair">嬰兒推車</label>
									<select class="select min rentCost" name="sel_children_chair">
										<?php for( $i = 0; $i < 4; $i++) :
											$val = str_pad($i, 1, '0', STR_PAD_LEFT) ;
										?>
										<option value="<?=$val?>" <?=( $val == $accessoriesInfo['sel_children_chair']) ? "selected" : "" ?>><?=$val?></option>
										<?php endfor ; ?>
									</select>
								</li>
								<?php endif ; ?>
							</ul>
						</div>
					</div><!-- tab-peripheralPage 2. 配件資訊 End -->
					<div class="tab-pane" id="tab-orderPage"><!-- tab-orderPage 3. 客戶資料 Start -->
						<div class="tablebox">
							<ul class="tableform tableform2">
								<li class="col-md-5 col-sm-12">
									<label for="" class="short">承租人姓名</label>
									<input type="text" name="input_cust_name" value="<?=$custInfo['input_cust_name']?>">
								</li>
								<li class="col-md-5 col-sm-12">
									<label for="">身分證字號／護照號碼</label>
									<input type="text" name="input_cust_id" value="<?=$custInfo['input_cust_id']?>">
								</li>
								<li class="col-md-2 col-sm-12">
									<label for="" id="adult" style="width:60px;">成人</label>
									<select class="select min rentCost" name="sel_adult">
										<?php for( $i = 0; $i < 10; $i++) :
											$val = str_pad($i, 1, '0', STR_PAD_LEFT) ;
										?>
										<option value="<?=$val?>" <?=( $val == $accessoriesInfo['sel_adult']) ? "selected" : "" ?>><?=$val?></option>
										<?php endfor ; ?>
									</select>
								</li>
								<li class="col-md-5 col-sm-12">
									<label for="" class="short">出生年月日</label>
									<div class="input-group date">
										<input type="text" name="input_cust_born" value="<?=$custInfo['input_cust_born']?>" placeholder="出生年月日" data-date-format="YYYY-MM-DD" id="input-cust-born" class="form-control ptxt2" />
										<span class="input-group-btn">
											<button class="btn btn-default" type="button">
												<i class="fa fa-calendar"></i>
											</button>
										</span>
									</div>
								</li>
								<li class="col-md-5 col-sm-12">
									<label for="">手機</label>
									<input type="text" name="input_cust_mobile" value="<?=$custInfo['input_cust_mobile']?>">
								</li>
								<li class="col-md-2 col-sm-12">
									<label for="" id="children" style="width:60px;">幼兒</label>
									<select class="select min rentCost" name="sel_children">
										<?php for( $i = 0; $i < 4; $i++) :
											$val = str_pad($i, 1, '0', STR_PAD_LEFT) ;
										?>
										<option value="<?=$val?>" <?=( $val == $accessoriesInfo['sel_children']) ? "selected" : "" ?>><?=$val?></option>
										<?php endfor ; ?>
									</select>
								</li>
								<li class="col-sm-10">
									<label for="" class="short">承租人E-mail</label>
									<input type="text" class="address" name="input_cust_email" value="<?=$custInfo['input_cust_email']?>">
								</li>
								<li class="col-md-2 col-sm-12">
									<label for="" id="baby" style="width:60px;">嬰兒</label>
									<select class="select min rentCost" name="sel_baby">
										<?php for( $i = 0; $i < 4; $i++) :
											$val = str_pad($i, 1, '0', STR_PAD_LEFT) ;
										?>
										<option value="<?=$val?>" <?=( $val == $accessoriesInfo['sel_baby']) ? "selected" : "" ?>><?=$val?></option>
										<?php endfor ; ?>
									</select>
								</li>
								<li class="col-md-5 col-sm-12">
									<label for="" class="short">取車人姓名</label>
									<input type="text" name="input_take_name" value="<?=$custInfo['input_take_name']?>">
									<input type="checkbox" name="syncrent" /> 同承租人
								</li>
								<li class="col-md-7 col-sm-12">
									<label for="">身分證字號／護照號碼</label>
									<input type="text" name="input_take_id" value="<?=$custInfo['input_take_id']?>">
								</li>
								<li class="col-md-5 col-sm-12">
									<label for="" class="short">出生年月日</label>
									<div class="input-group date">
										<input type="text" name="input_take_born" value="<?=$custInfo['input_take_born']?>" placeholder="出生年月日" data-date-format="YYYY-MM-DD" id="input-take-born" class="form-control ptxt2" />
										<span class="input-group-btn">
											<button class="btn btn-default" type="button">
												<i class="fa fa-calendar"></i>
											</button>
										</span>
									</div>
								</li>
								<li class="col-md-7 col-sm-12">
									<label for="">手機</label>
									<input type="text" name="input_take_mobile" value="<?=$custInfo['input_take_mobile']?>">
								</li>
								<li class="col-sm-12">
									<label for="" class="short">通訊地址</label>
									<select name="input_rent_zipcode1" id="input_rent_zipcode1">
										<option value="">請選擇</option>
										<?php foreach ( $retZip as $iCnt => $tmpOpt) :
												$zipSeled = ( $tmpOpt['zip_code'] == $custInfo['sel_zip1']) ? "selected" : "" ;
										?>
											<option value="<?=$tmpOpt['zip_code']?>" <?=$zipSeled?>><?=$tmpOpt['zip_name']?></option>
										<?php endforeach ;?>
									</select>
									<select name="input_rent_zipcode2" id="input_rent_zipcode2">
										<option value="">請選擇</option>
										<?php
											if( isset( $retZip2)) {
												foreach( $retZip2 as $iCnt => $tmpOpt) {
													$zipSeled = ( $tmpOpt['zip_code'] == $custInfo['sel_zip2']) ? "selected" : "" ;
													echo "<option value=\"{$tmpOpt['zip_code']}\" {$zipSeled}>{$tmpOpt['zip_name']}</option>" ;
												}
											}
										?>
									</select>
									<input type="text" class="address" name="input_cust_address" value="<?=$custInfo['input_cust_address']?>">
								</li>
								<li class="col-md-5 col-sm-12">
									<label for="" class="short">公司名稱</label>
									<input type="text" name="input_company" value="<?=$custInfo['input_company']?>">
								</li>
								<li class="col-md-7 col-sm-12">
									<label for="">公司統編</label>
									<input type="text" name="input_company_no" value="<?=$custInfo['input_company_no']?>">
								</li>
								<li class="col-md-5 col-sm-12">
									<label for="" class="short">緊急連絡人</label>
									<input type="text" name="input_urgent_man" value="<?=$custInfo['input_urgent_man']?>">
								</li>
								<li class="col-md-7 col-sm-12">
									<label for="">緊急連絡人電話</label>
									<input type="text" name="input_urgent_mobile" value="<?=$custInfo['input_urgent_mobile']?>">
								</li>
							</ul>
						</div>
					</div><!-- tab-orderPage 3. 客戶資料 End -->
					<div class="tab-pane" id="tab-flightPage"><!-- tab-flightPage 4. 航班資訊 Start -->
						<div class="tablebox">
							<ul class="tableform tableform2">
								<li class="col-md-12 col-sm-12">
								<?php
									$flightFlag = "";
									$pierFlag = "";
									if($sel_trans_type == 1)
										$flightFlag = "checked";
									else if($sel_trans_type == 2)
										$pierFlag = "checked";
								?>
									<label for="" class="flight"><span class="outbound">去程</span>交通工具</label>
									<input type="radio" name="sel_trans_type" value="1" <?=$flightFlag?>> 飛機　　
									<input type="radio" name="sel_trans_type" value="2" <?=$pierFlag?>> 輪船
								</li>
								<li class="col-md-6 col-sm-12">
									<label for="" class="flight" id="sdate_name">預定起飛時間</label>
									<div class="input-group date">
										<input type="text" class="flight" name="input_airline_sdate" value="<?=$input_airline_sdate?>" placeholder="開始日期" data-date-format="YYYY-MM-DD" id="input-airline-sdate" class="form-control ptxt2" />
										<select class="select" name="sel_airline_shour">
											<?php for( $i = 0; $i < 24; $i++) :
												$val = str_pad($i, 2, '0', STR_PAD_LEFT) ;
											?>
											<option value="<?=$val?>" <?=( $val == $sel_airline_shour) ? "selected" : "" ?>><?=$val?></option>
											<?php endfor ; ?>
										</select> :
										<select class="select min" name="sel_airline_sminute">
											<?php for( $i = 0; $i < 12; $i++) :
												$val = str_pad($i * 5, 2, '0', STR_PAD_LEFT) ;
											?>
											<option value="<?=$val?>" <?=( $val == $sel_airline_sminute) ? "selected" : "" ?>><?=$val?></option>
											<?php endfor ; ?>
										</select>
										<span class="input-group-btn">
											<button class="btn btn-default" type="button">
												<i class="fa fa-calendar"></i>
											</button>
										</span>
									</div>
								</li>
								<li class="col-md-6 col-sm-12">
									<label for="" class="flight">預定到達時間</label>
									<div class="input-group date">
										<input type="text" class="flight" name="input_airline_edate" value="<?=$input_airline_edate?>" placeholder="結束日期" data-date-format="YYYY-MM-DD" id="input-airline-edate" class="form-control ptxt2" />
										<select class="select" name="sel_airline_ehour">
											<?php for( $i = 0; $i < 24; $i++) :
												$val = str_pad($i, 2, '0', STR_PAD_LEFT) ;
											?>
											<option value="<?=$val?>" <?=( $val == $sel_airline_ehour) ? "selected" : "" ?>><?=$val?></option>
											<?php endfor ; ?>
										</select> :
										<select class="select min" name="sel_airline_eminute">
											<?php for( $i = 0; $i < 12; $i++) :
												$val = str_pad($i * 5, 2, '0', STR_PAD_LEFT) ;
											?>
											<option value="<?=$val?>" <?=( $val == $sel_airline_eminute) ? "selected" : "" ?>><?=$val?></option>
											<?php endfor ; ?>
										</select>
										<span class="input-group-btn">
											<button class="btn btn-default" type="button">
												<i class="fa fa-calendar"></i>
											</button>
										</span>
									</div>
								</li>
								<li class="col-md-12 col-sm-12">
									<div id="airport" style="display:<?=$airlineShow?>">
										<label for="" class="flight">出發航站</label>
										<select name="sel_airport">
											<option value="">請選擇</option>
											<?php foreach ($airport as $iCnt => $station) : ?>
												<option value="<?=$station['idx']?>" <?=( $station['idx'] == $sel_airport) ? "selected" : "" ?>><?=$station['opt_name']?></option>
											<?php endforeach ; ?>
										</select>
									</div>
								</li>
								<li class="col-md-6 col-sm-12">
									<div id="airline" style="display:<?=$airlineShow?>">
										<label for="" class="flight">航空公司</label>
										<select name="sel_airline">
											<option value="">請選擇</option>
											<?php foreach ($airlineArr as $iCnt => $station) : ?>
												<option value="<?=$station['idx']?>" <?=( $station['idx'] == $sel_airline) ? "selected" : "" ?>><?=$station['opt_name']?></option>
											<?php endforeach ; ?>
										</select>
									</div>
									<div id="pier" style="display:<?=$pierShow?>">
										<label for="" class="flight">碼頭出發地</label>
										<select name="sel_pier">
											<option value="">請選擇</option>
											<?php foreach ($pierArr as $iCnt => $station) : ?>
												<option value="<?=$station['idx']?>" <?=( $station['idx'] == $sel_pier) ? "selected" : "" ?>><?=$station['opt_name']?></option>
											<?php endforeach ; ?>
										</select>
									</div>
								</li>
								<li class="col-md-6 col-sm-12">
									<div id="flight_no" style="display:<?=$flightnoShow?>">
										<label for="" class="flight">班機編號</label>
										<input type="text" name="input_flight_no" value="<?=$input_flight_no?>">
									</div>
								</li>
							</ul>
							<ul class="tableform tableform2">
								<li class="col-md-12 col-sm-12">
								<?php
									$flightFlag_rt = "";
									$pierFlag_rt = "";
									if($sel_trans_type_rt == 1)
										$flightFlag_rt = "checked";
									else if($sel_trans_type_rt == 2)
										$pierFlag_rt = "checked";
								?>
									<label for="" class="flight"><span class="inbound">回程</span>交通工具</label>
									<input type="radio" name="sel_trans_type_rt" value="1" <?=$flightFlag_rt?>> 飛機　　
									<input type="radio" name="sel_trans_type_rt" value="2" <?=$pierFlag_rt?>> 輪船
								</li>
								<li class="col-md-6 col-sm-12">
									<label for="" class="flight" id="sdate_name_rt">預定起飛時間</label>
									<div class="input-group date">
										<input type="text" class="flight" name="input_airline_sdate_rt" value="<?=$input_airline_sdate_rt?>" placeholder="開始日期" data-date-format="YYYY-MM-DD" id="input-airline-sdate" class="form-control ptxt2" />
										<select class="select" name="sel_airline_shour_rt">
											<?php for( $i = 0; $i < 24; $i++) :
												$val = str_pad($i, 2, '0', STR_PAD_LEFT) ;
											?>
											<option value="<?=$val?>" <?=( $val == $sel_airline_shour_rt) ? "selected" : "" ?>><?=$val?></option>
											<?php endfor ; ?>
										</select> :
										<select class="select min" name="sel_airline_sminute_rt">
											<?php for( $i = 0; $i < 12; $i++) :
												$val = str_pad($i * 5, 2, '0', STR_PAD_LEFT) ;
											?>
											<option value="<?=$val?>" <?=( $val == $sel_airline_sminute_rt) ? "selected" : "" ?>><?=$val?></option>
											<?php endfor ; ?>
										</select>
										<span class="input-group-btn">
											<button class="btn btn-default" type="button">
												<i class="fa fa-calendar"></i>
											</button>
										</span>
									</div>
								</li>
								<!-- 
								<li class="col-md-12 col-sm-12">
									<div id="airport_rt" style="display:<?=$airlineShow_rt?>">
										<label for="" class="flight">出發航站</label>
										<select name="sel_airport_rt">
											<option value="">請選擇</option>
											<?php foreach ($airport as $iCnt => $station) : ?>
												<option value="<?=$station['idx']?>" <?=( $station['idx'] == $sel_airport) ? "selected" : "" ?>><?=$station['opt_name']?></option>
											<?php endforeach ; ?>
										</select>
									</div>
								</li>
								<li class="col-md-6 col-sm-12">
									<div id="airline_rt" style="display:<?=$airlineShow_rt?>">
										<label for="" class="flight">航空公司</label>
										<select name="sel_airline_rt">
											<option value="">請選擇</option>
											<?php foreach ($airlineArr as $iCnt => $station) : ?>
												<option value="<?=$station['idx']?>" <?=( $station['idx'] == $sel_airline_rt) ? "selected" : "" ?>><?=$station['opt_name']?></option>
											<?php endforeach ; ?>
										</select>
									</div>
									<div id="pier_rt" style="display:<?=$pierShow_rt?>">
										<label for="" class="flight">碼頭出發地</label>
										<select name="sel_pier_rt">
											<option value="">請選擇</option>
											<?php foreach ($pierArr as $iCnt => $station) : ?>
												<option value="<?=$station['idx']?>" <?=( $station['idx'] == $sel_pier_rt) ? "selected" : "" ?>><?=$station['opt_name']?></option>
											<?php endforeach ; ?>
										</select>
									</div>
								</li>
								<li class="col-md-6 col-sm-12">
									<div id="flight_no_rt" style="display:<?=$flightnoShow_rt?>">
										<label for="" class="flight">班機編號</label>
										<input type="text" name="input_flight_no_rt" value="<?=$input_flight_no_rt?>">
									</div>
								</li -->
							</ul>
						</div>
					</div><!-- tab-orderPage 4. 航班資訊 End -->
				</div><!-- tab-content End -->
			</div><!-- panel panel-default End -->
		</div><!-- container End -->
	</div><!-- row End -->
</div><!-- container-fluid End -->

		<!-- 按鈕列 start -->
		<div class="button_line">
			<?php if( isset( $entry_delete_button)) : ?>
			<!-- 刪除 -->
			<button id="btn_renew" class="view" >關閉視窗</button>
			<button id="btn_delete" class="delete" ><?=$entry_delete_button?></button>
			<?php endif ;?>
			<!-- 新增 / 修改 -->
			<button id="btn_send" class="send" ><?=$entry_submit_button?></button>
			<?php if( isset( $entry_delete_button)) : ?>
			<button id="btn_view" class="view" ><?=$entry_view_button?></button>
			<?php endif ;?>
		</div>
		<!-- 按鈕列 End -->
	</form>
</div>
<script type="text/javascript"><!--

/**
 * 同租車人資訊
 * @Another Angus
 * @date    2018-02-12
 */
$("input[name='syncrent']").click( function () {
	if ($(this).is(":checked")) {
		$( "input[name$='input_take_name']" ).val( $( "input[name='input_cust_name']").val()) ;
		$( "input[name$='input_take_id']" ).val( $( "input[name='input_cust_id']").val()) ;
		$( "input[name$='input_take_born']" ).val( $( "input[name='input_cust_born']").val()) ;
		$( "input[name$='input_take_mobile']" ).val( $( "input[name='input_cust_mobile']").val()) ;
	} else {
		$( "input[name$='input_take_name']" ).val( "") ;
		$( "input[name$='input_take_id']" ).val( "") ;
		$( "input[name$='input_take_born']" ).val( "") ;
		$( "input[name$='input_take_mobile']" ).val( "") ;
	}
}) ;
$("input[name='input_start_date'],input[name='input_end_date'],input[name='sel_station_start'],[name='sel_station_end'],[name='input_start_time'],[name='input_end_time']").click( function () {
	change_day();
})

function change_day(){
	var d1 = $("input[name='input_start_date']").val() ;
	var d2 = $("input[name='input_end_date']").val() ;
	var t1 = $('select[name=\'input_start_time\'] option:selected').val() ;
	var t2 = $('select[name=\'input_end_time\'] option:selected').val() ;

	var sDate = d1+'T'+t1;
	var eDate = d2+'T'+t2;
	var strMsg = '';

	var oneDayHour = checkOneDay ( sDate, eDate) ;
	var oneDay = 0;
	var oneHour = 0;
	var allDay = 0;
	if(oneDayHour > 0){
		oneDay = oneDayHour/ 24;
		oneDay = parseInt(oneDay);
		Math.floor(oneDay);
		oneHour = oneDayHour%24;
		oneDay = parseInt(oneDay);
	}

	$("#haveDay").html(oneDay);
	$("#haveHour").html(oneHour);

	//計算總金額
	var car_price ="<?php echo $carInfo['car_price']?>";
	car_price = parseInt(car_price);
	var c_price = car_price;
	//baby_chair1/baby_chair2/children_chair 100元
	c_price += parseInt($('select[name=\'sel_baby_chair1\'] option:selected').val())*100;
	c_price += parseInt($('select[name=\'sel_baby_chair2\'] option:selected').val())*100;
	c_price += parseInt($('select[name=\'sel_children_chair\'] option:selected').val())*100;

	if(oneHour>0){
		oneDay++;
	}
	c_price = oneDay * c_price; //依天數要倍數計算
	$("input[name='input_total']").val(c_price);
}

//配件更新要更新價格
$("select[name='sel_baby_chair1'],select[name='sel_baby_chair2'],select[name='sel_children_chair']").change( function () {
	change_day();
}) ;

$('.date').datetimepicker({
	pickTime: false
});
$('.date').datetimepicker().on('dp.change', function (event) {
	change_day();
});

// 郵政區號
$("select[name='input_rent_zipcode1']").change( function () {
	var selVal = $(this).val() ;
	$('#input_rent_zipcode2').find('option:not(:first)').remove() ;
	console.log( selVal) ;
	$.ajax({
		url : '?route=site/scheduling/getSubZip&token=<?=$token?>',
		type : 'get',
		data : {zipMain : selVal},
		dataType : 'json',
		success : function (resp) {
			console.log(resp) ;
			for (var i = 0; i < resp.length; i++) {
				$("#input_rent_zipcode2").append('<option value=' + resp[i].zip_code + '>' + resp[i].zip_code + ' ' + resp[i].zip_name + '</option>') ;
			}
		}
	});
});

// 處理訂單類別 如果是選擇民宿業者 需要把民宿的名稱帶上
$("select[name='sel_order_type']").change( function () {
	var orderType = $(this).val() ;
	console.log('sel_order_type : '+orderType) ;
	if ( orderType == 10 || orderType == 130 || orderType == 140 || orderType == 146) {
		$('#minsu').show() ;
	} else {
		$('#minsu').hide() ;
	}
}) ;

// 處理交通工具 分為 飛機 輪船
$("input[name='sel_trans_type']").change( function () {
	var transType = $(this).val() ;
	console.log('sel_trans_type : '+transType) ;
	if ( transType == 1) {	// 飛機
		$('#airport').show() ;
		$('#airline').show() ;
		$('#flight_no').show() ;
		$('#pier').hide() ;
		$('#sdate_name').text("預定起飛時間");
	} else {				// 輪船
		$('#airport').hide() ;
		$('#airline').hide() ;
		$('#flight_no').hide() ;
		$('#pier').show() ;
		$('#sdate_name').text("預定出發時間");
	}
}) ;


// 處理交通工具 分為 飛機 輪船
$("input[name='sel_trans_type_rt']").change( function () {
	var transType = $(this).val() ;
	console.log('sel_trans_type : '+transType) ;
	if ( transType == 1) {	// 飛機
		$('#airport_rt').show() ;
		$('#airline_rt').show() ;
		$('#flight_no_rt').show() ;
		$('#pier_rt').hide() ;
		$('#sdate_name_rt').text("預定起飛時間");
	} else {				// 輪船
		$('#airport_rt').hide() ;
		$('#airline_rt').hide() ;
		$('#flight_no_rt').hide() ;
		$('#pier_rt').show() ;
		$('#sdate_name_rt').text("預定出發時間");
	}
}) ;


/**
 * [刪除功能]
 * @param   {String}   ) {		var       url [description]
 * @return  {[type]}     [description]
 * @Another Angus
 * @date    2018-01-09
 */
$('#btn_delete').click( function () {
	var url = "<?=$url_del?>" ;
	console.log( url) ;
	console.log( url.replace(/&amp;/g, '&')) ;

	if( confirm( "確定要刪除??")) {
		// location.href = url.replace(/&amp;/g, '&') ;
		location.href = url ;
		return false;
	}
});

/**
 * [description]
 * @param   {[type]}   event) {	}         [description]
 * @return  {[type]}          [description]
 * @Another Angus
 * @date    2018-02-07
 */
$('#btn_view').click(function(event) {
	var url = "<?=$url_view?>" ;
	console.log( url) ;
	window.open(url.replace(/&amp;/g, '&'),'_blank') ;
	return false ;
});

$('#btn_renew').click(function(event) {
	var url = "<?=$url_renew?>" ;
	console.log( url) ;
	console.log( url.replace(/&amp;/g, '&')) ;

	location.href = url ;
	return false;
});


/**
 * [checkDate 檢查日期]
 * @param   {[type]}   sDate [description]
 * @param   {[type]}   eDate [description]
 * @return  {[type]}         [description]
 * @Another Angus
 * @date    2017-12-16
 */
function checkDate( sDate, eDate) {
	var currDate = "" ;
	var txtDate = "" ;
	if ( eDate == undefined) {
		console.log(new Date()) ;
		currDate = Date.parse((new Date()).toDateString()+' '+(new Date()).toTimeString());
		txtDate = Date.parse(sDate);
	} else {
		currDate = Date.parse(sDate);
		txtDate = Date.parse(eDate);
	}
	// if ( currDate == txtDate) return true ;
	console.log('開始日期' + currDate) ;
	console.log('比較日期' + currDate) ;

	if (currDate <= txtDate) {
		return false ;
	} else {
		return true ;
	}
}


$('#btn_send').click( function () {
	var strMsg = "" ;

	// 檢查租車日期
	var d1 = $("input[name='input_start_date']").val() ;
	var d2 = $("input[name='input_end_date']").val() ;
	var t1 = $("select[name='input_start_time']").val() ;
	var t2 = $("select[name='input_end_time']").val() ;

	var sDate = d1+'T'+t1 ;
	var eDate = d2+'T'+t2 ;
	console.log( sDate) ;
	console.log( eDate) ;

	// 定義租車訂單類別
	var sel_order_type = $("select[name='sel_order_type']").val() ;
	console.log( "sel_order_type : " + sel_order_type) ;
	if ( sel_order_type == "") { strMsg += "訂單類別未設定\n" ; }

	if ( sel_order_type == 9 || sel_order_type == 10 || sel_order_type == 11) {
		// 需檢查客戶資料
	}
	// 需檢查取車還車是否必填

	if ( sel_order_type == 9 || sel_order_type == 10 || sel_order_type == 11 || sel_order_type == 140 || sel_order_type == 146 ) {
		if($("select[name='sel_station_start']").val() == ""){
			strMsg += "取車地點未選擇\n" ;
		}
		if($("select[name='sel_station_end']").val() == ""){
			strMsg += "還車地點未選擇\n" ;
		}
	}else if(sel_order_type == 123 || sel_order_type == 124 ){
		if($("select[name='sel_station_start']").val() == ""){
			strMsg += "取車地點未選擇\n" ;
		}
	}
	if ( sDate == eDate) { strMsg += "還車日期不能等於取車日期\n" ; }
	// if ( checkDate( sDate)) { strMsg += "取車日期不能小於今天\n" ; }
	if ( checkDate( sDate, eDate)) { strMsg += "還車日期不能小於取車日期\n" ; }

	if(sel_order_type == 10){
		if($("input[name='input_minsu_name']").val() == ""){
			strMsg += "民宿名稱不可以為空白\n" ;
		}
	}
	if(sel_order_type == 10 || sel_order_type == 11){
		if($("input[name='input_take_name']").val() == ""){
			strMsg += "取車人姓名未填寫\n" ;
		}
		if($("input[name='input_take_id']").val() == ""){
			strMsg += "取車人 身分證字號／護照號碼 未填寫\n" ;
		}
	}


	if ( strMsg != "") {
		alert( strMsg) ;
		return false ;
	}
}) ;


$('input[name=\'input_minsu_name\']').autocomplete({
	'source': function(request, response) {
		// console.log( "request :" + request) ;
		// console.log( "response :" + response) ;
		$.ajax({
			url: '?route=site/scheduling/getAjaxMinsu&token=<?=$token?>&minsuName='+encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				// console.log( "json : " + json) ;
				response($.map(json, function(item) {
					// console.log( "item : " + item) ;
					return {
						label: item['name'],
						value: item['code']
					}
				}));
			}
		});
	},
	'select': function(item) {
		console.log( item) ;
		$('input[name=\'input_minsu_name\']').val(item['label']) ;
		$('input[name=\'input_minsu_idx\']').val(item['value']);
	}
});
$("select[name*='sel_station_start']").change( function () {
	var html = "" ;
	if ( $(this).val() == 16) {
		html = '<option value="08:30">08:30</option><option value="09:00">09:00</option><option value="09:30">09:30</option><option value="10:00">10:00</option><option value="10:30">10:30</option><option value="11:00">11:00</option><option value="11:30">11:30</option><option value="12:00">12:00</option><option value="12:30">12:30</option><option value="13:00">13:00</option><option value="13:30">13:30</option><option value="14:00">14:00</option><option value="14:30">14:30</option><option value="15:00">15:00</option><option value="15:30">15:30</option><option value="16:00">16:00</option><option value="16:30">16:30</option><option value="17:00">17:00</option><option value="17:30">17:30</option><option value="18:00">18:00</option>';
	} else if ( $(this).val() == 17) {
		html = '<option value="08:30">08:30</option><option value="09:00">09:00</option><option value="09:30">09:30</option><option value="10:00">10:00</option><option value="10:30">10:30</option><option value="11:00">11:00</option><option value="11:30">11:30</option><option value="12:00">12:00</option><option value="12:30">12:30</option><option value="13:00">13:00</option><option value="13:30">13:30</option><option value="14:00">14:00</option><option value="14:30">14:30</option><option value="15:00">15:00</option><option value="15:30">15:30</option><option value="16:00">16:00</option><option value="16:30">16:30</option><option value="17:00">17:00</option><option value="17:30">17:30</option>';
	} else if ( $(this).val() == 18) {
		html = '<option value="08:00">08:00</option><option value="08:30">08:30</option><option value="09:00">09:00</option><option value="09:30">09:30</option><option value="10:00">10:00</option><option value="10:30">10:30</option><option value="11:00">11:00</option><option value="11:30">11:30</option><option value="12:00">12:00</option><option value="12:30">12:30</option><option value="13:00">13:00</option><option value="13:30">13:30</option><option value="14:00">14:00</option><option value="14:30">14:30</option><option value="15:00">15:00</option><option value="15:30">15:30</option><option value="16:00">16:00</option><option value="16:30">16:30</option><option value="17:00">17:00</option><option value="17:30">17:30</option><option value="18:00">18:00</option><option value="18:30">18:30</option>';
	} else {
		html = '<option value="08:30">08:30</option><option value="09:00">09:00</option><option value="09:30">09:30</option><option value="10:00">10:00</option><option value="10:30">10:30</option><option value="11:00">11:00</option><option value="11:30">11:30</option><option value="12:00">12:00</option><option value="12:30">12:30</option><option value="13:00">13:00</option><option value="13:30">13:30</option><option value="14:00">14:00</option><option value="14:30">14:30</option><option value="15:00">15:00</option><option value="15:30">15:30</option><option value="16:00">16:00</option><option value="16:30">16:30</option><option value="17:00">17:00</option><option value="17:30">17:30</option><option value="18:00">18:00</option>';
	}
	$('select[name=\'input_start_time\']').html(html);
});

$("select[name*='sel_station_end']").change( function () {
	var html = "" ;
	if ( $(this).val() == 16) {
		html = '<option value="08:30">08:30</option><option value="09:00">09:00</option><option value="09:30">09:30</option><option value="10:00">10:00</option><option value="10:30">10:30</option><option value="11:00">11:00</option><option value="11:30">11:30</option><option value="12:00">12:00</option><option value="12:30">12:30</option><option value="13:00">13:00</option><option value="13:30">13:30</option><option value="14:00">14:00</option><option value="14:30">14:30</option><option value="15:00">15:00</option><option value="15:30">15:30</option><option value="16:00">16:00</option><option value="16:30">16:30</option><option value="17:00">17:00</option><option value="17:30">17:30</option><option value="18:00">18:00</option>';
	} else if ( $(this).val() == 17) {
		html = '<option value="08:30">08:30</option><option value="09:00">09:00</option><option value="09:30">09:30</option><option value="10:00">10:00</option><option value="10:30">10:30</option><option value="11:00">11:00</option><option value="11:30">11:30</option><option value="12:00">12:00</option><option value="12:30">12:30</option><option value="13:00">13:00</option><option value="13:30">13:30</option><option value="14:00">14:00</option><option value="14:30">14:30</option><option value="15:00">15:00</option><option value="15:30">15:30</option><option value="16:00">16:00</option><option value="16:30">16:30</option><option value="17:00">17:00</option><option value="17:30">17:30</option>';
	} else if ( $(this).val() == 18) {
		html = '<option value="08:00">08:00</option><option value="08:30">08:30</option><option value="09:00">09:00</option><option value="09:30">09:30</option><option value="10:00">10:00</option><option value="10:30">10:30</option><option value="11:00">11:00</option><option value="11:30">11:30</option><option value="12:00">12:00</option><option value="12:30">12:30</option><option value="13:00">13:00</option><option value="13:30">13:30</option><option value="14:00">14:00</option><option value="14:30">14:30</option><option value="15:00">15:00</option><option value="15:30">15:30</option><option value="16:00">16:00</option><option value="16:30">16:30</option><option value="17:00">17:00</option><option value="17:30">17:30</option><option value="18:00">18:00</option><option value="18:30">18:30</option>';
	} else {
		html = '<option value="08:30">08:30</option><option value="09:00">09:00</option><option value="09:30">09:30</option><option value="10:00">10:00</option><option value="10:30">10:30</option><option value="11:00">11:00</option><option value="11:30">11:30</option><option value="12:00">12:00</option><option value="12:30">12:30</option><option value="13:00">13:00</option><option value="13:30">13:30</option><option value="14:00">14:00</option><option value="14:30">14:30</option><option value="15:00">15:00</option><option value="15:30">15:30</option><option value="16:00">16:00</option><option value="16:30">16:30</option><option value="17:00">17:00</option><option value="17:30">17:30</option><option value="18:00">18:00</option>';
	}
	$('select[name=\'input_end_time\']').html(html);
});

//檢查選的天數幾天幾小時
function checkOneDay( sDate, eDate) {
	var currDate = "" ;
	var txtDate = "" ;
	if ( eDate == undefined) {
		currDate = Date.parse((new Date()).toDateString()+' '+(new Date()).toTimeString());
		txtDate = Date.parse(sDate);
	} else {
		currDate = Date.parse(sDate);
		txtDate = Date.parse(eDate);
	}
	var ONE_HOUR = 1000 * 60 * 60 ;
	return  (txtDate - currDate) / ONE_HOUR ;
}
--></script>
	</body>
</html>