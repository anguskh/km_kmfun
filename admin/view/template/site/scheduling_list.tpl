<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <!--<a href="<?php echo $url_add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-list').submit() : false;"><i class="fa fa-trash-o"></i></button>-->
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <!--===================================== 訊息提示 Start =======================================-->
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <!--======================================= 訊息提示 End =======================================-->
    <div class="panel panel-default">
        <!--================================= 標題 Start ===========================================-->
        <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
        </div>
        <!--================================= 標題 End =============================================-->
<link type="text/css" href="view/stylesheet/fixedtable.css?v=<?=$cssVer?>" rel="stylesheet" media="screen">
<link rel="stylesheet" type="text/css" href="view/stylesheet/jquery.fancybox.css?v=<?=$cssVer?>">
<script src="view/javascript/jquery.fancybox.js"></script>
<script src="view/javascript/gridviewscroll.js"></script>
<script src="view/javascript/gridviewscroll2.js"></script>

		<div class="panel-body">
<table class="table table-bordered table-hover">
<?php foreach($mainCar as $index => $carModelName) :
	$checkStr = "" ;
	foreach( $selSeedArr as $seled) {
		if ( $seled == $index ) $checkStr =  "checked" ;
	}
?>
	<tr>
		<td nowrap="nowrap"><input type="checkbox" name="carSeed-<?=$index?>" value="<?=$index?>" <?=$checkStr?>></td>
		<td nowrap="nowrap"><?=$carModelName?></td>
		<td>
			<?php foreach( $searchCarModelArr[0][$index] as $carSeedNo) :
					$checkStr = "" ;
					foreach( $selStatusArr as $sel_status) {
						if ( $sel_status == $carSeedNo ) $checkStr =  "checked" ;
					}
			?>
			<input type="checkbox" name="carId-<?=$carSeedNo?>" value="<?=$carSeedNo?>" <?=$checkStr?>/> <?=$searchCarModelArr[1][$carSeedNo]?>&nbsp;&nbsp;
			<?php endforeach ; ?>
		</td>
	</tr>
<?php endforeach ; ?>
</table>
<div class="form-group required" style="overflow: hidden;">
	<a href="<?=$aLink?>" class="btn btn-xs btn-danger">官網</a>
	<a href="<?=$aLink?>" class="btn btn-xs btn-success">民宿</a>
	<a href="<?=$aLink?>" class="btn btn-xs btn-long-term">長租</a>
	<a href="<?=$aLink?>" class="btn btn-xs btn-warning">保養</a>
	<a href="<?=$aLink?>" class="btn btn-xs btn-locked">鎖車</a>
	<a href="<?=$aLink?>" class="btn btn-xs btn-maintenance">未完成</a>
	<a href="<?=$aLink?>" class="btn btn-xs btn-primary">應收</a>
	<a href="<?=$aLink?>" class="btn btn-xs btn-qr">QRCode</a>
	<a href="<?=$aLink?>" class="btn btn-xs btn-business">前台業務訂車</a>
</div>
<div class="form-group required" style="overflow: hidden;">
	<div class="search-date">
		<b>查詢日期區間：自</b>
		<div class="input-group date search_sdate">
			<input type="text" name="search_sdate"
			value="<?=$dayRangeArr['sDayStr']?>"
			placeholder="查詢開始時間"
			data-date-format="YYYY-MM-DD"
			id="search_sdate" class="form-control" />
			<span class="input-group-btn">
				<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
			</span>
		</div>
		<b>至</b>
		<div class="input-group date date-non">
			<input type="text" name="search_edate"
			value="<?=$dayRangeArr['eDayStr']?>"
			placeholder="查詢結束時間"
			data-date-format="YYYY-MM-DD"
			id="search_edate" class="form-control" READONLY/>
			<span class="input-group-btn">
				<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
			</span>
		</div>
	</div>
	<div class="search-date right">
		檢視週期：
		<div class="btn-group" data-toggle="buttons">
			<label class="btn btn-primary <?=$cycleArr[1][0]?>">
				<input type="radio" name="view_cycle" value="1" <?=$cycleArr[1][1]?>> 三天
			</label>
			<label class="btn btn-primary <?=$cycleArr[2][0]?>">
				<input type="radio" name="view_cycle" value="2" <?=$cycleArr[2][1]?>> 一週
			</label>
			<label class="btn btn-primary <?=$cycleArr[3][0]?>">
				<input type="radio" name="view_cycle" value="3" <?=$cycleArr[3][1]?>> 雙週
			</label>
			<label class="btn btn-primary <?=$cycleArr[4][0]?>">
				<input type="radio" name="view_cycle" value="4" <?=$cycleArr[4][1]?>> 一個月
			</label>
			<label class="btn btn-primary <?=$cycleArr[5][0]?>">
				<input type="radio" name="view_cycle" value="5" <?=$cycleArr[5][1]?>> 二個月
			</label>
			<label class="btn btn-primary <?=$cycleArr[6][0]?>">
				<input type="radio" name="view_cycle" value="6" <?=$cycleArr[6][1]?>> 三個月
			</label>
		</div>
	</div>
</div>

<script type="text/javascript"><!--
var clickTime = 0 ;

$('.date').datetimepicker({
	pickTime: false,
});
$('.search_sdate').datetimepicker().change(function(event) {
	console.log("in here :" + clickTime) ;
	var today = new Date() ;
	var todayStr = today.getFullYear() + "-" + getMonth( today) + "-" + getDay( today) ;
	console.log(todayStr) ;
	var selStatusStr = oneCheckBox()
	var selMainStr = allCheckBox() ;
	var cycle = $("input[name='view_cycle']:checked").val() ;

	var sDate = $("input[name='search_sdate']").val() ;
	reUrl( selStatusStr, selMainStr, cycle, sDate) ;
	console.log(selStatusStr+"^"+selMainStr+"^"+cycle+"^"+sDate) ;
}) ;

function getMonth(date) {
  var month = date.getMonth() + 1;
  return month < 10 ? '0' + month : '' + month; // ('' + month) for string result
}

function getDay(date) {
  var day = date.getDate();
  return day < 10 ? '0' + day : '' + day;
}
//--></script>

	<!--車輛調度表 Start -->
	<div class="fixedbox">
		<?php $colspanCnt = ($hourArea[1]-$hourArea[0]+1) * 2 ;?>
		<table cellspacing="0" id="gvMain" style="width:100%;border-collapse:collapse;">
			<tr class="GridViewScrollHeader">
				<td colspan="3">車輛狀態</td>
				<?php foreach( $tdCalendar as $iCal => $tdVal) : ?>
				<td colspan="<?=$colspanCnt?>"><?=$tdVal[0]?></th>
				<?php endforeach ; ?>
			</tr>
			<tr class="GridViewScrollHeader">
				<th scope="col"><div class="fixw">廠牌</div></th>
				<th scope="col"><div class="fixw">車牌號碼</div></th>
				<th scope="col"><div class="fixws">狀態</div></th>
				<?php foreach( $tdCalendar as $iCal => $tdVal) : ?>
					<?php for( $i = $hourArea[0] ; $i <= $hourArea[1] ; $i++) : ?>
					<th colspan="2" scope="col"><?=$i?></th>
					<?php endfor ; ?>
				<?php endforeach ; ?>
			</tr>
			<tr class="GridViewScrollItem">
				<td><div class="fixgrid"></div></td>
				<td><div class="fixgrid"></div></td>
				<td><div class="fixgrid"></div></td>
				<?php foreach( $tdCalendar as $iCal => $tdVal) : ?>
					<?php for( $i = $hourArea[0] ; $i <= $hourArea[1] ; $i++) : ?>
						<td><div class="fixgrid"></div></td>
						<td><div class="fixgrid"></div></td>
					<?php endfor ; ?>
				<?php endforeach ; ?>
			</tr>
			<tr class="GridViewScrollItem">
				<td><div class="fixgrid"></div></td>
				<td><div class="fixgrid"></div></td>
				<td><div class="fixgrid"></div></td>
				<?php foreach( $tdCalendar as $iCal => $tdVal) : ?>
					<?php for( $i = $hourArea[0] ; $i <= $hourArea[1] ; $i++) : ?>
						<td><div class="fixgrid"></div></td>
						<td><div class="fixgrid"></div></td>
					<?php endfor ; ?>
				<?php endforeach ; ?>
			</tr>
			<?php foreach( $rowCars as $car_no => $carInfo) :
				$colspan = 0 ;
				$argsArr = null ;
			?>
			<tr class="GridViewScrollItem">
				<td><div class="fixw"><?=$carInfo[0]?></div></td>
				<td><div class="fixw"><?=$carInfo[1]?><?php echo (isset($devAdmin)) ? "-{$carInfo[2]}" : "" ; ?></div></td>
				<td><div class="fixws">上架</div></td>
				<?php //dump( $tdCalendar) ; ?>
				<?php foreach( $tdCalendar as $iCal => $tdVal) : ?>
					<?php for( $i = $hourArea[0] ; $i <= $hourArea[1] ; $i++) :
						// xx:00
						if ( !isset( $tdVal[4][$car_no][$i][0])) {
							if( $colspan > 0) {
								viewGenerateSpaceTD( $colspan, $argsArr) ;
								$colspan   = 0 ;
							}
							$argsArr = array(
								"tipStr"      => "",
								"bid"         => "",
								"aClassStr"   => "",
								"bookingDate" => $tdVal[1],
								"car_no"      => $car_no,
								"token"       => $token,
								"hours"       => str_pad( $i, 2, '0', STR_PAD_LEFT).":00",
							) ;
							viewGenerateSpaceTD( $colspan, $argsArr) ;
						} else {
							if ( isset( $argsArr) && $argsArr['bid'] != '' && $argsArr['bid'] != $tdVal[4][$car_no][$i][0]) {
								viewGenerateSpaceTD( $colspan, $argsArr) ;
								$colspan = 0 ;
							}
							$colspan ++ ;
							$argsArr = array(
								"tipStr"      => $iconTip[$tdVal[4][$carInfo[2]][$i][0]],
								"bid"         => $tdVal[4][$car_no][$i][0],
								"aClassStr"   => $tdVal[3][$car_no][$i][0],
								"bookingDate" => $tdVal[1],
								"car_no"      => $car_no,
								"token"       => $token,
								"hours"       => str_pad( $i, 2, '0', STR_PAD_LEFT).":00",
							) ;
						}
						// xx:30
						if ( !isset( $tdVal[4][$car_no][$i][1])) {
							if( $colspan > 0) {
								viewGenerateSpaceTD( $colspan, $argsArr) ;
								$colspan   = 0 ;
							}
							$argsArr = array(
								"tipStr"      => "",
								"bid"         => "",
								"aClassStr"   => "",
								"bookingDate" => $tdVal[1],
								"car_no"      => $car_no,
								"token"       => $token,
								"hours"       => str_pad( $i, 2, '0', STR_PAD_LEFT).":30",
							) ;
							viewGenerateSpaceTD( $colspan, $argsArr) ;
						} else {
							if ( isset( $argsArr) && $argsArr['bid'] != '' && $argsArr['bid'] != $tdVal[4][$car_no][$i][1]) {
								viewGenerateSpaceTD( $colspan, $argsArr) ;
								$colspan = 0 ;
							}
							$colspan ++ ;
							$argsArr = array(
								"tipStr"      => $iconTip[$tdVal[4][$carInfo[2]][$i][1]],
								"bid"         => $tdVal[4][$car_no][$i][1],
								"aClassStr"   => $tdVal[3][$car_no][$i][1],
								"bookingDate" => $tdVal[1],
								"car_no"      => $car_no,
								"token"       => $token,
								"hours"       => str_pad( $i, 2, '0', STR_PAD_LEFT).":30",
							) ;
						}
					?>
					<?php endfor ; ?>
				<?php endforeach ; ?>
				<?php
				if ( $colspan > 0) {
					if ( $colspan > 2) $carIcon = "<i class=\"fa fa-car\"></i>" ;
					else $carIcon = "" ;
echo <<<HTML
<td colspan="{$colspan}">
	<a data-fancybox data-src="?route=site/scheduling/add&token={$token}&bookingID={$argsArr['bid']}&bookingDate={$tdVal[1]}&idx={$argsArr['car_no']}" href="javascript:;" class="order btn btn-xs img {$argsArr['aClassStr']}"><i class="fa fa-car"></i></a>
	<i class="img-tooltip" data-toggle="tooltip2" data-placement="top" data-html="true" data-title="{$argsArr['tipStr']}" data-animation="false" data-trigger="manual"/></td>
HTML
;
					$colspan = 0 ;
				}?>
			</tr>
			<?php endforeach ; ?>
		</table>
	</div>
	<!--車輛調度表 End-->

		<div class="row">
			<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
			<div class="col-sm-6 text-right"><?php echo $indexDec; ?></div>
		</div>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
// 檢視週期
$("input[name='view_cycle']").change( function (){
	var cycle = $(this).val() ;
	console.log( "cycle = " + cycle) ;
	var selStatusStr = oneCheckBox() ;
	var selMainStr = allCheckBox() ;
	var sDate = $("input[name='search_sdate']").val() ;
	reUrl( selStatusStr, selMainStr, cycle, sDate) ;
	console.log(selStatusStr+"^"+selMainStr+"^"+cycle+"^"+sDate) ;
});
// 全選checkbox發動
$("input[name*='carSeed']").click( function () {
	var $obj = $(this).parent().parent() ;
	$obj.find('input[name*=\'carId\']').prop('checked', this.checked) ;

	var cycle = $("input[name='view_cycle']:checked").val() ;
	var selStatusStr = oneCheckBox() ;
	var selMainStr = allCheckBox() ;
	var sDate = $("input[name='search_sdate']").val() ;
	reUrl( selStatusStr, selMainStr, cycle, sDate) ;
	console.log(selStatusStr+"^"+selMainStr+"^"+cycle+"^"+sDate) ;
});

// 單一checkbox發動
$("input[name*='carId']").click( function () {
	var cycle = $("input[name='view_cycle']:checked").val() ;
	var selMainStr   = "" ;
	var selStatusStr = oneCheckBox() ;
	var sDate = $("input[name='search_sdate']").val() ;
	reUrl( selStatusStr, selMainStr, cycle, sDate) ;
	console.log(selStatusStr+"^"+selMainStr+"^"+cycle+"^"+sDate) ;
}) ;

// 取所有單一checkbox的值
function oneCheckBox() {
	var retStr = "" ;
	$("input[name*='carId']:checked").each( function ( index) {
		retStr += $(this).val() + '|' ;
	});
	return retStr ;
}
// 取所有全選checkbox的值
function allCheckBox() {
	var retStr = "" ;
	$("input[name*='carSeed']:checked").each( function ( index) {
		retStr += $(this).val() + '|' ;
	});
	return retStr ;
}
// 導頁
function reUrl( selStatusStr= "", selMainStr="",  cycle=1, sdate="" ) {
	window.location.href = "?route=site/scheduling&token=<?=$token?>&sel_status=" + selStatusStr + "&sel_seed=" + selMainStr + "&cycle=" + cycle + "&sdate=" + sdate
}
//--></script>
</div>
<?php echo $footer; ?>

<?php
/**
 * [viewGenerateSpaceTD description]
 * @param   integer    $colspan [description]
 * @param   array      $data    [description]
 * @return  [type]              [description]
 * @Another Angus
 * @date    2018-01-04
 */
function viewGenerateSpaceTD( $colspan = 0, $data = array()) {
	// <i class="fa fa-car"></i>
	if ( $colspan > 0) {
		if ( $colspan > 2) $carIcon = "<i class=\"fa fa-car\"></i>" ;
		else $carIcon = "" ;
echo <<<HTML
<td colspan="{$colspan}">
	<a data-fancybox data-src="?route=site/scheduling/add&token={$data['token']}&bookingID={$data['bid']}&bookingDate={$data['bookingDate']}&idx={$data['car_no']}" href="javascript:;" class="order btn btn-xs img {$data['aClassStr']}">{$carIcon}</a>
	<i class="img-tooltip" data-toggle="tooltip2" data-placement="top" data-html="true" title="{$data['tipStr']}" data-animation="false" data-trigger="manual"/></td>

HTML
;
	} else {
echo <<<HTML
<td><a data-fancybox data-src="?route=site/scheduling/add&token={$data['token']}&bookingID=&bookingDate={$data['bookingDate']} {$data['hours']}&idx={$data['car_no']}" href="javascript:;" class="space"></a></td>

HTML
;
	}
}
?>