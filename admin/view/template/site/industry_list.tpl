<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <a href="<?php echo $url_add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-list').submit() : false;"><i class="fa fa-trash-o"></i></button>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <!--===================================== 訊息提示 Start =======================================-->
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <!--======================================= 訊息提示 End =======================================-->
    <div class="panel panel-default">
        <!--================================= 標題 Start ===========================================-->
        <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
        </div>
        <!--================================= 標題 End =============================================-->
      <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-3">
              <div class="form-group">
                <label class="control-label"><?php echo $entry_name; ?></label>
                <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo $entry_name; ?>" class="form-control" />
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <label class="control-label"><?php echo $entry_man_name; ?></label>
                <input type="text" name="filter_man_name" value="<?php echo $filter_man_name; ?>" placeholder="<?php echo $entry_man_name; ?>" class="form-control" />
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <label class="control-label"><?php echo $entry_tel; ?></label>
                <input type="text" name="filter_tel" value="<?php echo $filter_tel; ?>" placeholder="<?php echo $entry_tel; ?>" class="form-control" />
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <label class="control-label"><?php echo $entry_address; ?></label>
                <input type="text" name="filter_address" value="<?php echo $filter_address; ?>" placeholder="<?php echo $entry_address; ?>" class="form-control" />
              </div>
              <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-filter"></i> <?php echo $button_filter; ?></button>
            </div>
          </div>
        </div>
        <form action="<?php echo $url_delete; ?>" method="post" enctype="multipart/form-data" id="form-list">
<div class="form-group required" style="overflow: hidden;">
  <a class="btn btn-xs btn-danger">己停用</a>
</div>

          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                  <?php foreach ($columnNames as $colKey => $columnName):
                      $orderStr = ( $sort == $colKey) ? strtolower($order) : "" ;
                      $hrefStr = $hrefArr[$colKey] ;
                  ?>
                  <td class="text-left">
                    <a href="<?=$hrefStr?>" class="<?=$orderStr?>"><?php echo $columnName; ?></a>
                    </td>
                  <?php endforeach; ?>
                  <td class="text-right"><?php echo $column_action; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($results) { ?>
                <?php foreach ($results as $res):
                    $disabledItem = ($res['status'] == '2') ? "class='deleteitem'" : "" ;
                ?>
                <tr <?=$disabledItem?>>
                  <td class="text-center"><?php if (in_array($res['idx'], $selected)) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $res['idx']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $res['idx']; ?>" />
                    <?php } ?></td>
                  <?php foreach ($columnNames as $colKey => $columnName): ?>
                  <td class="text-left"><?php echo $res[$colKey]; ?></td>
                  <?php endforeach; ?>
                  <td class="text-right">
                  <a href="<?php echo $res['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                <?php endforeach; ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="<?=$td_colspan+2?>"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $indexDec; ?></div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('#button-filter').on('click', function() {
  url = '?route=site/industry&token=<?php echo $token; ?>';

  var filter_name = $('input[name=\'filter_name\']').val();

  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
  }

  var filter_man_name = $('input[name=\'filter_man_name\']').val();

  if (filter_man_name) {
    url += '&filter_man_name=' + encodeURIComponent(filter_man_name);
  }

  var filter_tel = $('input[name=\'filter_tel\']').val();

  if (filter_tel) {
    url += '&filter_tel=' + encodeURIComponent(filter_tel);
  }

  var filter_address = $('input[name=\'filter_address\']').val();

  if (filter_address) {
    url += '&filter_address=' + encodeURIComponent(filter_address);
  }

  location = url;
});
//--></script></div>
<?php echo $footer; ?>