<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-list').submit() : false;" >
			<!-- onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-marketing').submit() : false;" -->
			<i class="fa fa-save"></i>
		</button>
		<?php if ( $show_return_button) : ?>
		<a href="<?php echo $url_cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
		<?php endif ; ?>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <!--===================================== 訊息提示 Start =======================================-->
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <!--======================================= 訊息提示 End =======================================-->
    <div class="panel panel-default">
        <!--================================= 標題 Start ===========================================-->
        <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
        </div>
        <!--================================= 標題 End =============================================-->
      <div class="panel-body">
<div class="row">
	<div class="col-sm-4">
		<div class="form-group">
			<label class="control-label" for="input-name">項目選擇</label>
			<select name="sel_mainOption" id="sel-mainOption" class="form-control">
				<option value="0">--- 目前是主要選項 ---</option>
				<?php foreach( $mainOptionArr as $i => $optArr) : ?>
				<option value="<?=$optArr['idx']?>" <?=( $sel_mainOption == $optArr['idx'] ) ? "selected" : "" ; ?>><?=$optArr['idx']?> <?=$optArr['opt_desc']?> - <?=($optArr['status'] == '1') ? "啟用" : "停用" ; ?></option>
				<?php endforeach ; ?>
			</select>
		</div>
	</div>
</div>
<?php //dump($sel_mainOption) ; ?>
        <form action="<?php echo $url_add; ?>" method="post" enctype="multipart/form-data" id="form-list">
          <div class="table-responsive">

            <table class="table table-bordered table-hover" id="attribute">
              <thead>
                <tr>
                    <td class="text-left"><a href="" class="">代碼</a></td>
<?php foreach ($columnNames as $columnName): ?>
                  	<td class="text-left"><a href="" class=""><?php echo $columnName; ?></a></td>
<?php endforeach; ?>
                  <td class="text-right"></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($results) { ?>
<?php foreach ($results as $i => $res): ?>
                <tr>
                  <td class="text-right"><?=$res['idx']?></td>
	<?php foreach ($columnNames as $colKey => $columnName): ?>
		<?php if ( $colKey == "status") : ?>
					<td class="text-left">
						<select name="optionchild[<?=$res['idx']?>][<?=$colKey?>]" id="sel-option-<?=$res['idx']?>" class="form-control">
							<option value="1" <?=( $res['status'] == "1") ? "selected" : "" ?>>啟用</option>
							<option value="2" <?=( $res['status'] == "2") ? "selected" : "" ?>>停用</option>
						</select>
					</td>
		<?php else : ?>
					<td class="text-left">
						<input name="optionchild[<?=$res['idx']?>][<?=$colKey?>]" value="<?=$res[$colKey]?>" placeholder="<?=$columnName?>" class="form-control" autocomplete="off" type="text" required>
					</td>
		<?php endif ; ?>
	<?php endforeach; ?>

<!--<td class="text-left">
	<a href="" id="thumb-optionchild[<?=$res['idx']?>][image]" data-toggle="image" class="img-thumbnail">
	<img src="<?=$res['thumb']; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
	<input type="hidden" name="optionchild[<?=$res['idx']?>][image]" value="<?=$res['opt_short_desc']?>" id="input-optionchild[<?=$res['idx']?>][image]" />
</td>-->
<?php if ($sel_mainOption == 0) : ?>
	<td class="text-right">
		<a href="<?=$res['gotoUrl']?>" data-toggle="tooltip" title="前往 <?php echo $res['opt_desc'] ; ?>" class="btn btn-success"><i class="fa fa-arrow-right"></i> 前住</a>
	</td>
<?php else : ?>
	<td class="text-right"></td>
<?php endif ; ?>
                </tr>
<?php endforeach; ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="<?=$td_colspan+2?>"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
				<tfoot>
					<tr>
						<td colspan="<?=$td_colspan+1?>"></td>
						<td class="text-left"><button type="button" onclick="addAttribute();" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="新增選項"><i class="fa fa-plus-circle"></i></button></td>
					</tr>
				</tfoot>

            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $indexDec; ?></div>
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript"><!--
$('#sel-mainOption').change(function(){
	var selIndex = $( "#sel-mainOption option:selected" ).val();
	window.location.href = "?route=site/option&token=<?=$token?>&sel_mainOption=" + selIndex ;
});

function addAttribute() {
	var attribute_row = "a" + Math.floor( ( Math.random() * 10000) + 1000) ;
	html  = '<tr id="attribute-row' + attribute_row + '">';
  html += '<td class="text-left">&nbsp;</td>';
  html += '<td class="text-left">';
	html += '<input name="optionchild[' + attribute_row + '][opt_name]" value="" placeholder="名稱：" class="form-control" autocomplete="off" type="text" required>';
	html += '</td>';
	html += '<td class="text-left">';
	html += '<input name="optionchild[' + attribute_row + '][opt_desc]" value="" placeholder="說明：" class="form-control" autocomplete="off" type="text" required>';
	html += '</td>';
	html += '<td class="text-left">';
	html += '<input name="optionchild[' + attribute_row + '][opt_short_desc]" value="" placeholder="簡稱：" class="form-control" autocomplete="off" type="text">';
	html += '</td>';
	html += '<td class="text-right">';
	html += '<input name="optionchild[' + attribute_row + '][opt_seq]" value="" placeholder="排序：" class="form-control" autocomplete="off" type="text">';
	html += '</td>';
	html += '<td>';
	html += '<select name="optionchild[' + attribute_row + '][status]" id="input-status" class="form-control">';
	html += '<option value="1">啟用</option>';
	html += '<option value="2">停用</option>';
	html += '</select>';
	html += '</td>';
	html += '<td class="text-left"><button type="button" onclick="$(\'#attribute-row' + attribute_row + '\').remove();" data-toggle="tooltip" title="" class="btn btn-danger" data-original-title="移除"><i class="fa fa-minus-circle"></i></button></td>';
	html += '</tr>';

	$('#attribute tbody').append(html);


	attribute_row++;
}
//--></script></div>
<?php echo $footer; ?>