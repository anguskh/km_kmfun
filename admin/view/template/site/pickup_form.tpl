<?php echo $header; ?><?php echo $column_left; ?>
<?php
	//檢查car_model
	$mo_flag = "N";	//預設汽車
	foreach($car_model as $arr){
		if($arr == '2' || $arr == '7'){
			$mo_flag = 'Y';
		}

	}
?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<button type="submit" form="form-action" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $url_cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
				<h1><?php echo $heading_title; ?></h1>
				<ul class="breadcrumb">
					<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
					<?php } ?>
				</ul>
			</div>
		</div>
		<div class="container-fluid">
			<!--===================================== 訊息提示 Start =======================================-->
			<?php if ($error_warning) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
			<?php } ?>
			<?php if ($error_checkIsOrder) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_checkIsOrder; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
			<?php } ?>
			<?php if ($error_noSign) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_noSign; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
			<?php } ?>
			<!--===================================== 訊息提示 End =========================================-->
			<div class="panel panel-default">
				<!--================================= 標題 Start ===========================================-->
				<div class="panel-heading">
					<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
				</div>
				<!--================================= 標題 End =============================================-->
				<div class="panel-body">
					<form id="pick_save" action="<?php echo $url_action; ?>" method="post" enctype="multipart/form-data"
						id="form-action" class="form-horizontal">
						<input type="hidden" value="<?php echo $rentInfo['total'];?>" placeholder="更新後的金額" id="new_total" class="form-control" />

						<ul class="nav nav-tabs"><!-- 頁籤 -->
					<?php
						//echo (($order_type=='140')?'<li class="last">$ <span id="t_money">'.$total.'</span></a></li>':'');
						//改全部訂單都要即時計算金額 2019/09/25
						echo '<li class="last" style="display:none;"><span style="background: #8fbb6c; font-size: 14px; color:#fff; padding: 0 10px; border-radius: 20px"><b>共計</b> <span id="rentDay1">'.$rentDay.'</span> <b>天</b> <span id="rentHour1">'.$rentHour.'</span> <b>時</b></span>　$ <span id="t_money">'.($rentInfo['total']+$rentInfo['pickdiff']).'</span></a>－$ <span id="now_money">'.$rentInfo['total'].'</span></a>＝$ <span id="t_pickdiff">'.$rentInfo['pickdiff'].'</span></a></li>';
					?>

							<li class=" active"><a href="#tab-cart" data-toggle="tab">1. 租車資訊</a></li>
							<li class="">		<a href="#tab-customer" data-toggle="tab">2. 客戶資訊</a></li>
							<li class="">		<a href="#tab-accessories" data-toggle="tab">3.交車資訊與收費</a></li>
<?php
							$pick_nest_id = "";
							if(isset($now_pickup) && $now_pickup == 'Y'){
								$pick_nest_id = "tab-DeliveryCar";
								$pick_up_id = "tab-DeliveryCar";
								$car_nest_id = "tab-bigContract";

?>
								<li class="">		<a href="#tab-DeliveryCar" data-toggle="tab">4.注意事項與合約說明</a></li>
								<li class="">		<a href="#tab-checkCar" data-toggle="tab">5.取車作業</a></li>

<?php
							}else{
								$pick_nest_id = "tab-checkCar";
								$pick_up_id = "tab-accessories";
								$car_nest_id = "tab-ExceptionalLoss";

?>
								<li class="">		<a href="#tab-checkCar" data-toggle="tab">4. 還車作業</a></li>
								<!-- li class="">		<a href="#tab-ExceptionalLoss" data-toggle="tab">6. 加收費用</a></li -->

<?php
							}
?>
						<?php foreach ($rentInfo['other_car'] as $iCnt => $carInfo) :
						$activeStr = ( $iCnt == 0) ?  "active" : "" ;
						?>
						<!-- <li class="" ><a href="#tab-<?=$carInfo['idx']?>" data-toggle="tab"><?=$carInfo['car_no']?></a></li> -->
						<?php endforeach ; ?>
						</ul><!-- 頁籤 end -->
						<div class="tab-content"><!-- 內容 -->
						<div class="tab-pane active" id="tab-cart"><!-- tab-cart 1. 租車資訊 start -->
							<div class="row">
								<div class="col-sm-12 text-right" style="float:right;">
								  <button type="button" id="button-cart" class="btn btn-primary">
								  	<i class="fa fa-arrow-right"></i> 下一步 </button>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="sel-order-type">訂單類別</label>
								<div class="col-sm-10">
									<input type="text" value="<?=$order_types[$rentInfo['order_type']]?>" class="form-control"/>
									<INPUT TYPE="hidden" NAME="order_type" value="<?=$rentInfo['order_type'];?>">
								</div>
							</div>
							<div class="form-group" <?=$minsuShow?>>
								<label class="col-sm-2 control-label" >單位</label>
								<div class="col-sm-10">
									<input type="text" value="<?=$minsu_name?>"  class="form-control"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="s-station">取車地點</label>
								<div class="col-sm-10">
								<select name="s_station" id="s-station" class="form-control">
									<option value=""> --- 請選擇 --- </option>
								<?php foreach ($stations as $mid => $optionName) { ?>
									<?php if($rentInfo['s_station'] == $mid) : ?>
										<option value="<?php echo $mid; ?>" selected><?php echo $optionName; ?></option>
									<?php else : ?>
										<option value="<?php echo $mid; ?>"><?php echo $optionName; ?></option>
									<?php endif ; ?>
								<?php } ?>
								</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="rent-date-out">預計取車時間</label>
								<div class="col-sm-10">
									<div class="input-group date">
										<input type="text" name="rent_date_out" value="<?=$rent_date_out?>" placeholder="取車時間"
											data-date-format="YYYY-MM-DD" id="rent-date-out" class="form-control" />
										<span class="input-group-btn">
											<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
										</span>
										<select name="out_time" class="form-control">
												<option value="">請選擇</option>
												<?php foreach ($rentTimeOptArr as $optItem) :
													$seledStr = ( $optItem == $rent_date_out_time) ? "selected" : "" ;
												?>
												<option value="<?=$optItem?>" <?=$seledStr?>><?=$optItem?></option>
											<?php endforeach ; ?>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="e-station">還車地點</label>
								<div class="col-sm-10">
									<select name="e_station" id="e-station" class="form-control">
										<option value=""> --- 請選擇 --- </option>
									<?php foreach ($stations as $mid => $optionName) { ?>
										<?php if( $rentInfo['e_station'] == $mid) : ?>
											<option value="<?php echo $mid; ?>" selected><?php echo $optionName; ?></option>
										<?php else : ?>
											<option value="<?php echo $mid; ?>"><?php echo $optionName; ?></option>
										<?php endif ; ?>
									<?php } ?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="rent-date-in-plan">還車時間</label>
								<div class="col-sm-10">
									<div class="input-group date">
										<input type="text" name="rent_date_in_plan" value="<?=$rent_date_in_plan?>" placeholder="還車時間"
											data-date-format="YYYY-MM-DD" id="rent-date-in-plan" class="form-control" />
										<span class="input-group-btn">
											<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
										</span>
										<select name="plan_time" class="form-control">
												<option value="">請選擇</option>
												<?php foreach ($rentTimeOptArr as $optItem) :
													$seledStr = ( $optItem == $rent_date_in_plan_time) ? "selected" : "" ;
												?>
												<option value="<?=$optItem?>" <?=$seledStr?>><?=$optItem?></option>
											<?php endforeach ; ?>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="sel-car-model">車種</label>
								<div class="col-sm-10">
									<?php foreach ($car_models as $mid => $optionName) { ?>
										<?php $labelClassStr = in_array( $mid, $car_model) ? "" : "bt-background-clean" ; ?>
										<?php $chkedStr      = in_array( $mid, $car_model) ? "checked" : "" ; ?>
									<label name="carModels" class="btn btn-success <?=$labelClassStr?>" >
										<i class="fa fa-car"></i>&nbsp;<?=$optionName?>
										<input class="hidden" type="checkbox" name="selCarModels[]" value="<?=$mid?>" <?=$chkedStr?> />
									</label>&nbsp&nbsp
									<?php } ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="sel-car-seed">車型</label>
								<div class="col-sm-10" id="showCarType">

									<?php foreach ($car_seeds as $mid => $optionName) { ?>
										<?php $labelClassStr = in_array( $optionName['idx'], $car_seed) ? "" : "bt-background-clean" ; ?>
										<?php $chkedStr      = in_array( $optionName['idx'], $car_seed) ? "checked" : "" ; ?>
									<label name="carModels" class="btn btn-success <?=$labelClassStr?>" >
										<i class="fa fa-car"></i>&nbsp;<?=$optionName['car_seed']?>
										<input class="hidden" type="checkbox" name="selCarTypes[]" value="<?=$optionName['idx']?>" <?=$chkedStr?> />
									</label>&nbsp&nbsp
									<?php } ?>
								</div>
								<span id="showJs"></span>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">租車費用預計金額</label>
								<div class="col-sm-10">
									<table class="estimate-price">
										<tr><th>$<span id="d_price">0</span>/天</th><th>$<span id="h_price">0</span>/時</th><th>小計</th></tr>
										<tr><td><span id="rentDay"><?=$rentDay?></span>天</td><td><span id="rentHour"><?=$rentHour?></span>小時</td><td rowspan="2">$<span id="all_price">0</span></td></tr>
										<tr><td>$<span id="ad_price">0</span></td><td>$<span id="ah_price">0</span><b id="h_remark"></b></td></tr>
									</table>
								</div>
							</div>

						</div><!-- tab-content 1. 租車資訊 end -->
						<div class="tab-pane" id="tab-accessories"><!-- tab-accessories 2. 配件資訊 start -->
							<div class="row">
								<div class="col-sm-6 text-left" style="float:left;">
									<button type="button" onclick="$('a[href=\'#tab-customer\']').tab('show');" class="btn btn-default"><i class="fa fa-arrow-left"></i> 上一步 </button>
								</div>
								<div class="col-sm-6 text-right" style="float:right;">
									<button type="button" id="button-cart2" class="btn btn-primary"><i class="fa fa-arrow-right"></i> 下一步 </button>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="baby_chair1">兒童安全座椅〔一歲以上〕 (金額100)</label>
								<div class="col-sm-10">
									<input type="text" name="baby_chair1" id="baby_chair1" value="<?php echo $rentInfo['baby_chair1'];?>" placeholder="兒童安全座椅〔一歲以上〕" id="input-baby_chair1" class="form-control" maxlength="1">
									<!--
									<select name="baby_chair1" id="baby_chair1" class="form-control">
										<?php for( $i = 0; $i < 4; $i++) :
											$val = str_pad($i, 1, '0', STR_PAD_LEFT) ;
										?>
										<option value="<?=$i?>" <?=( $i == $rentInfo['baby_chair1'] ) ? "selected" : "" ?>><?=$i?></option>
										<?php endfor ; ?>
									</select> -->
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="baby_chair2">兒童安全座椅〔增高墊〕(金額100)</label>
								<div class="col-sm-10">
									<input type="text" name="baby_chair2" id="baby_chair2" value="<?php echo $rentInfo['baby_chair2'];?>" placeholder="兒童安全座椅〔增高墊〕" id="input-baby_chair2" class="form-control" maxlength="1">
									<!-- select name="baby_chair2" id="baby_chair2" class="form-control">
										<?php for( $i = 0; $i < 4; $i++) :
											$val = str_pad($i, 1, '0', STR_PAD_LEFT) ;
										?>
										<option value="<?=$i?>" <?=( $i == $rentInfo['baby_chair2'] ) ? "selected" : "" ?>><?=$i?></option>
										<?php endfor ; ?>
									</select -->
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="children_chair">嬰兒推車 (金額100)</label>
								<div class="col-sm-10">
									<input type="text" name="children_chair" id="children_chair" value="<?php echo $rentInfo['children_chair'];?>" placeholder="嬰兒推車" id="input-children_chair" class="form-control" maxlength="1">

									<!-- select name="children_chair" id="children_chair" class="form-control">
										<?php for( $i = 0; $i < 4; $i++) :
											$val = str_pad($i, 1, '0', STR_PAD_LEFT) ;
										?>
										<option value="<?=$i?>" <?=( $i == $rentInfo['children_chair'] ) ? "selected" : "" ?>><?=$i?></option>
										<?php endfor ; ?>
									</select -->
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="gps">GPS衛星導航系統 (金額0)</label>
								<div class="col-sm-10">
									<input type="text" name="gps" id="gps" value="<?php echo $rentInfo['gps'];?>" placeholder="GPS衛星導航系統" id="input-gps" class="form-control" maxlength="1">
									<!-- select name="gps" id="gps" class="form-control">
										<?php for( $i = 0; $i < 4; $i++) :
											$val = str_pad($i, 1, '0', STR_PAD_LEFT) ;
										?>
										<option value="<?=$i?>" <?=( $i == $rentInfo['gps'] ) ? "selected" : "" ?>><?=$i?></option>
										<?php endfor ; ?>
									</select -->
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="mobile_moto">機車導航架(金額50)</label>
								<div class="col-sm-10">
									<input type="text" name="mobile_moto" id="mobile_moto" value="<?php echo $rentInfo['mobile_moto'];?>" placeholder="GPS衛星導航系統" id="input-gps" class="form-control" maxlength="1">

									<!-- select name="mobile_moto" id="mobile_moto" class="form-control">
										<?php for( $i = 0; $i < 4; $i++) :
											$val = str_pad($i, 1, '0', STR_PAD_LEFT) ;
										?>
										<option value="<?=$i?>" <?=( $i == $rentInfo['mobile_moto'] ) ? "selected" : "" ?>><?=$i?></option>
										<?php endfor ; ?>
									</select -->
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="mobile_moto"><font style="color:red">異動差額</font></label>
								<div class="col-sm-10">
									<input type="text" name="pickdiff" id="pickdiff" value="<?=$rentInfo['pickdiff']?>" class="form-control"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="umbrella">雨傘 (金額0) </label>
								<div class="col-sm-10">
									<input type="text" name="umbrella" id="umbrella" value="<?=(($rentInfo['umbrella']=='')?'2':$rentInfo['umbrella'])?>" class="form-control" maxlength="1"/>

								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="driving_recorder">行車記錄器 (金額0) </label>
								<div class="col-sm-10">
									<input type="text" name="driving_recorder" id="driving_recorder" value="<?=(($rentInfo['driving_recorder']=="" && $mo_flag=="N")?1:$rentInfo['driving_recorder'])?>" class="form-control"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="mobile_car">汽車手機架 (金額0) </label>
								<div class="col-sm-10">
									<input type="text" name="mobile_car" id="mobile_car" value="<?=(($rentInfo['mobile_car']=="" && $mo_flag=="N")?1:$rentInfo['mobile_car'])?>" class="form-control" maxlength="1"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="helmet">安全帽  (金額0) </label>
								<div class="col-sm-10">
									<input type="text" name="helmet" id="helmet" value="<?=(($rentInfo['helmet']=="" && $mo_flag=="Y")?2:$rentInfo['helmet'])?>" class="form-control" maxlength="1"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="raincoat">雨衣  (金額0) </label>
								<div class="col-sm-10">
									<input type="text" name="raincoat" id="raincoat" value="<?=(($rentInfo['raincoat']=="" && $mo_flag=="Y")?2:$rentInfo['raincoat'])?>" class="form-control" maxlength="1"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="car_safety">汽車保險費用 </label>
								<div class="col-sm-10">
									<input type="text" name="car_safety" id="car_safety" value="<?=$rentInfo['car_safety']?>" class="form-control"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="moto_safety">機車保險費用 </label>
								<div class="col-sm-10">
									<input type="text" name="moto_safety" id="moto_safety" value="<?=$rentInfo['moto_safety']?>" class="form-control"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="car_ps">租車費用 </label>
								<div class="col-sm-10">
									<input type="text" id="car_ps" value="" class="form-control" disabled/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="plug_total">配件費用 </label>
								<div class="col-sm-10">
									<input type="text" id="plug_total" name="plug_total" value="<?=$rentInfo['plug_total']?>" class="form-control" disabled/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">SPDC</label>
								<div class="col-sm-10">
									<input type="text" name="spdc" value="<?=$rentInfo['spdc']?>" placeholder="SPDC" class="form-control" disabled />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">合計</label>
								<div class="col-sm-10">
									<input type="text" id="tt_money" value="0" placeholder="SPDC" class="form-control" disabled />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">付款類型</label>
								<div class="col-sm-10">
									<input type="radio" name="pay_type" value="1" <?php echo ($rentInfo['pay_type'] == 1) ? "checked" : "" ;?>>信用卡&nbsp;&nbsp;
									<input type="radio" name="pay_type" value="2" <?php echo ($rentInfo['pay_type'] == 2) ? "checked" : "" ;?>>現金&nbsp;&nbsp;
									<input type="radio" name="pay_type" value="3" <?php echo ($rentInfo['pay_type'] == 3) ? "checked" : "" ;?>>匯款&nbsp;&nbsp;
									<input type="radio" name="pay_type" value="4" <?php echo ($rentInfo['pay_type'] == 4) ? "checked" : "" ;?>>應收&nbsp;&nbsp;
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">MEMO</label>
								<div class="col-sm-9">
									<input type="text" name="remark" id="remark" class="form-control">
								</div>
								<div class="col-sm-1" style="padding: 7px 0; text-align: left;">
									<a onclick="ins_reamrk('remark');" class="add-btn" href="javascript:void();">＋</a>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">備註</label>
								<div class="col-sm-10">
									<textarea id="area_memo" name="area_memo" cols="100" rows="5" disabled><?php echo (($area_memo=="")?'輸入你想要寫的內容...':$area_memo);?></textarea>
								</div>
							</div>
						</div><!-- tab-content 2. 配件資訊 end -->
						<div class="tab-pane" id="tab-customer"><!-- tab-customer 3. 客戶資訊 start -->
							<div class="row">
								<div class="col-sm-6 text-left" style="float:left;">
									<button type="button" onclick="$('a[href=\'#tab-cart\']').tab('show');" class="btn btn-default"><i class="fa fa-arrow-left"></i> 上一步 </button>
								</div>
								<div class="col-sm-6 text-right" style="float:right;">
									<button type="button" id="button-cart" onclick="$('a[href=\'#tab-accessories\']').tab('show');" class="btn btn-primary"><i class="fa fa-arrow-right"></i> 下一步 </button>
								</div>
							</div>
							<!-- 訂單編號 -->
							<input type="hidden" name="now_pickup" value="<?=$now_pickup?>">
							<input type="hidden" name="order_id" value="<?=$rentInfo['order_id']?>">
							<input type="hidden" name="agreement_no" value="<?=$rentInfo['agreement_no']?>" class="form-control" />
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-title">訂單編號</label>
								<div class="col-sm-10">
									<input type="text" value="<?=$rentInfo['agreement_no']?>" class="form-control" disabled />
								</div>
							</div>
							<!-- 承租人姓名 -->
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-title">承租人姓名</label>
								<div class="col-sm-10">
									<input type="text" name="rent_user_name" value="<?=$rentInfo['rent_user_name']?>" class="form-control" />
								</div>
							</div>
							<!-- 身份證字號 / 護照號碼 -->
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-title">身份證字號 / 護照號碼</label>
								<div class="col-sm-10">
									<input type="text" name="rent_user_id" value="<?=$rentInfo['rent_user_id']?>" class="form-control" />
								</div>
							</div>
							<!-- 出生年月日 -->
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-title">出生年月日</label>
								<div class="col-sm-10">
									<input type="text" name="rent_user_born" value="<?=$rentInfo['rent_user_born']?>" class="form-control" />
								</div>
							</div>
							<!-- 電話 / 手機 -->
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-title">電話 / 手機</label>
								<div class="col-sm-10">
									<input type="text" name="rent_user_tel" value="<?=$rentInfo['rent_user_tel']?>" class="form-control" />
								</div>
							</div>
							<!-- 聯絡人地址 -->
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-title">聯絡人地址</label>
								<div class="col-sm-10">
									<input type="text" name="rent_user_address" value="<?=$rentInfo['rent_user_address']?>" class="form-control" />
								</div>
							</div>
							<!-- 取車人 姓名 -->
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-title">取車人 姓名</label>
								<div class="col-sm-10">
									<input type="text" name="get_user_name" value="<?=$rentInfo['get_user_name']?>" class="form-control" />
								</div>
							</div>
							<!-- 取車人 身份證 -->
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-title">取車人 身份證</label>
								<div class="col-sm-10">
									<input type="text" name="get_user_id" value="<?=$rentInfo['get_user_id']?>" class="form-control" />
								</div>
							</div>
							<!-- 取車人 生日 -->
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-title">取車人 生日</label>
								<div class="col-sm-10">
									<input type="text" name="get_user_born" value="<?=$rentInfo['get_user_born']?>" class="form-control" />
								</div>
							</div>
							<!-- 取車人 電話 -->
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-title">取車人 電話</label>
								<div class="col-sm-10">
									<input type="text" name="get_user_tel" value="<?=$rentInfo['get_user_tel']?>" class="form-control" />
								</div>
							</div>
							<!-- 公司名稱 -->
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-title">公司名稱</label>
								<div class="col-sm-10">
									<input type="text" name="rent_company_name" value="<?=$rentInfo['rent_company_name']?>" class="form-control" />
								</div>
							</div>
							<!-- 公司統編 -->
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-title">公司統編</label>
								<div class="col-sm-10">
									<input type="text" name="rent_company_no" value="<?=$rentInfo['rent_company_no']?>" class="form-control" />
								</div>
							</div>
							<!-- 緊急連絡人 -->
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-title">緊急連絡人</label>
								<div class="col-sm-10">
									<input type="text" name="urgent_man" value="<?=$rentInfo['urgent_man']?>" class="form-control" />
								</div>
							</div>
							<!-- 緊急連絡人電話 -->
							<div class="form-group">
								<label class="col-sm-2 control-label" for="input-title">緊急連絡人電話</label>
								<div class="col-sm-10">
									<input type="text" name="urgent_mobile" value="<?=$rentInfo['urgent_mobile']?>" class="form-control" />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">MEMO</label>
								<div class="col-sm-9">
									<input type="text" name="remark3" id="remark3" class="form-control">
								</div>
								<div class="col-sm-1" style="padding: 7px 0; text-align: left;">
									<a onclick="ins_reamrk('remark3');" class="add-btn" href="javascript:void();">＋</a>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">備註</label>
								<div class="col-sm-10">
									<textarea id="area_memo2" name="area_memo2" cols="100" rows="5" disabled><?php echo (($area_memo=="")?'輸入你想要寫的內容...':$area_memo);?></textarea>
								</div>
							</div>

							<div class="form-group" style="padding:15px 0 0 0;">
								<div class="col-sm-2" style="text-align: right;"><span style="padding: 0 5px; border-radius: 3px; color: #fff; background: #1978ab;">去程</span></div>
							</div>
							<div class="form-group" style="border-top: 0; padding: 15px 0 0 0;">
								<label class="col-sm-2 control-label" for="trans_type">交通工具</label>
								<div class="col-sm-10">
									<input type="radio" name="trans_type" value="1" <?=$trans_type[0]?>> 飛機　　
									<input type="radio" name="trans_type" value="2" <?=$trans_type[1]?>> 輪船
								</div>
							</div>
							<div class="form-group" style="border-top: 0; padding: 15px 0 0 0;">
								<label class="col-sm-2 control-label" for="sdate_name">預定起飛時間</label>
								<div class="col-sm-10">
<?php
									$s_date = "";
									$s_hour = "";
									$s_minute = "";
									if($flight_sDate != ""){
										$s_date = substr($flight_sDate,0,10);
										$s_hour = substr($flight_sDate,11,2);
										$s_minute = substr($flight_sDate,14,2);
									}
?>
									<div class="date">
										<input type="text" name="s_date" value="<?=$s_date?>" placeholder="預定起飛時間"
											data-date-format="YYYY-MM-DD" id="s_date" class="flight_date" />
										<span class="input-group-btn" style="float: left; width: auto; margin: 0 10px 0 0;">
											<button type="button" class="btn btn-default flight_date_btn"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
									<select name="s_hour" class="flight_date_select">
											<option value="">請選擇</option>
											<?php for( $i = 0; $i < 24; $i++) :
											$val = str_pad($i, 2, '0', STR_PAD_LEFT) ;
										?>
										<option value="<?=$val?>" <?=( $val == $s_hour) ? "selected" : "" ?>><?=$val?></option>
										<?php endfor ; ?>
									</select> :
									<select name="s_minute" class="flight_date_select">
											<option value="">請選擇</option>
											<?php for( $i = 0; $i < 12; $i++) :
											$val = str_pad($i * 5, 2, '0', STR_PAD_LEFT) ;
										?>
										<option value="<?=$val?>" <?=( $val == $s_minute) ? "selected" : "" ?>><?=$val?></option>
										<?php endfor ; ?>
									</select>
								</div>
							</div>
							<div class="form-group"  style="border-top: 0; padding: 15px 0 0 0;">
								<label class="col-sm-2 control-label" for="e_date">預定到達時間</label>
								<div class="col-sm-10">
<?php
									$e_date = "";
									$e_hour = "";
									$e_minute = "";
									if($flight_sDate != ""){
										$e_date = substr($flight_eDate,0,10);
										$e_hour = substr($flight_eDate,11,2);
										$e_minute = substr($flight_eDate,14,2);
									}
?>
									<div class="date">
										<input type="text" name="e_date" value="<?=$e_date?>" placeholder="預定到達時間"
											data-date-format="YYYY-MM-DD" id="e_date" class="flight_date" />
										<span class="input-group-btn" style="float: left; width: auto; margin: 0 10px 0 0;">
											<button type="button" class="btn btn-default flight_date_btn"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
									<select name="e_hour" class="flight_date_select">
											<option value="">請選擇</option>
											<?php for( $i = 0; $i < 24; $i++) :
											$val = str_pad($i, 2, '0', STR_PAD_LEFT) ;
										?>
										<option value="<?=$val?>" <?=( $val == $e_hour) ? "selected" : "" ?>><?=$val?></option>
										<?php endfor ; ?>
									</select> :
									<select name="e_minute" class="flight_date_select">
											<option value="">請選擇</option>
											<?php for( $i = 0; $i < 12; $i++) :
											$val = str_pad($i * 5, 2, '0', STR_PAD_LEFT) ;
										?>
										<option value="<?=$val?>" <?=( $val == $e_minute) ? "selected" : "" ?>><?=$val?></option>
										<?php endfor ; ?>
									</select>
								</div>
							</div>
							<div class="form-group" style="border-top: 0; padding: 15px 0 0 0;">
								<div id="airport" style="padding:15px 0;display:<?=$airlineShow?>">
									<label class="col-sm-2 control-label" for="airport">出發航站</label>
									<div class="col-sm-10">
										<select name="airport" id="airport" class="form-control">
											<option value=""> --- 請選擇 --- </option>
											<?php foreach ($airportArr as $iCnt => $station) : ?>
											<option value="<?=$station['idx']?>" <?=( $station['idx'] == $airport) ? "selected" : "" ?>><?=$station['opt_name']?></option>
											<?php endforeach ; ?>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group" style="border-top: 0; padding: 0;">
								<div id="airline" style="padding: 15px 0;display:<?=$airlineShow?>">
									<label class="col-sm-2 control-label" for="airline">航空公司</label>
									<div class="col-sm-10">
										<select name="airline" id="airline" class="form-control">
											<option value=""> --- 請選擇 --- </option>
											<?php foreach ($airlineArr as $iCnt => $station) : ?>
											<option value="<?=$station['idx']?>" <?=( $station['idx'] == $airline) ? "selected" : "" ?>><?=$station['opt_name']?></option>
											<?php endforeach ; ?>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group" style="border-top: 0; padding: 0;">
								<div id="pier" style="padding: 15px 0;display:<?=$pierShow?> ">
									<label class="col-sm-2 control-label" for="pier">碼頭出發地</label>
									<div class="col-sm-10">
										<select name="pier" id="pier" class="form-control">
											<option value=""> --- 請選擇 --- </option>
											<?php foreach ($pierArr as $iCnt => $station) : ?>
											<option value="<?=$station['idx']?>" <?=( $station['idx'] == $pier) ? "selected" : "" ?>><?=$station['opt_name']?></option>
											<?php endforeach ; ?>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group" style="border-top: 0; padding: 0 0 15px 0;">
								<div id="flight_no" style="padding: 15px 0;display:<?=$flightnoShow?>">
									<label class="col-sm-2 control-label" for="flight_no">班機編號</label>
									<div class="col-sm-10">
										<input type="text" name="flight_no" value="<?=$flight_no?>" class="form-control" placeholder="班機編號" />
									</div>
								</div>
							</div>
							<div class="form-group" style="padding:15px 0 0 0;">
								<div class="col-sm-2" style="text-align: right;"><span style="padding: 0 5px; border-radius: 3px; color: #fff; background: #990000;">回程</span></div>
							</div>
							<div class="form-group" style="border-top:0; padding: 15px 0 0 0;">
								<label class="col-sm-2 control-label" for="trans_type_rt">交通工具</label>
								<div class="col-sm-10">
									<input type="radio" name="trans_type_rt" value="1" <?=$trans_type_rt[0]?>> 飛機　　
									<input type="radio" name="trans_type_rt" value="2" <?=$trans_type_rt[1]?>> 輪船
								</div>
							</div>
							<div class="form-group" style="border-top:0; padding: 15px 0 0 0;">
								<label class="col-sm-2 control-label" for="sdate_name">預定起飛時間</label>
								<div class="col-sm-10">
<?php
									$s_date_rt = "";
									$s_hour_rt = "";
									$s_minute_rt = "";
									if($flight_sDate_rt != ""){
										$s_date_rt = substr($flight_sDate_rt,0,10);
										$s_hour_rt = substr($flight_sDate_rt,11,2);
										$s_minute_rt = substr($flight_sDate_rt,14,2);
									}

?>
									<div class="date">
										<input type="text" name="s_date_rt" value="<?=$s_date_rt?>" placeholder="起飛時間"
											data-date-format="YYYY-MM-DD" id="s_date_rt" class="flight_date" />
										<span class="input-group-btn" style="float: left; width: auto; margin: 0 10px 0 0;">
											<button type="button" class="btn btn-default flight_date_btn"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
									<select name="s_hour_rt" class="flight_date_select">
											<option value="">請選擇</option>
											<?php for( $i = 0; $i < 24; $i++) :
											$val = str_pad($i, 2, '0', STR_PAD_LEFT) ;
										?>
										<option value="<?=$val?>" <?=( $val == $s_hour_rt) ? "selected" : "" ?>><?=$val?></option>
										<?php endfor ; ?>
									</select> :
									<select name="s_minute_rt" class="flight_date_select">
											<option value="">請選擇</option>
											<?php for( $i = 0; $i < 12; $i++) :
											$val = str_pad($i * 5, 2, '0', STR_PAD_LEFT) ;
										?>
										<option value="<?=$val?>" <?=( $val == $s_minute_rt) ? "selected" : "" ?>><?=$val?></option>
										<?php endfor ; ?>
									</select>
								</div>
							</div>
							<!--
							<div class="form-group" style="border-top:0; padding:15px 0 0 0;">
								<div id="airport_rt" style="display:<?=$airlineShow_rt?>">
									<label class="col-sm-2 control-label" for="airport_rt">出發航站</label>
									<div class="col-sm-10">
										<select name="airport_rt" id="airport_rt" class="form-control">
											<option value=""> --- 請選擇 --- </option>
											<?php foreach ($airportArr_rt as $iCnt => $station) : ?>
											<option value="<?=$station['idx']?>" <?=( $station['idx'] == $airport) ? "selected" : "" ?>><?=$station['opt_name']?></option>
											<?php endforeach ; ?>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group" style="border-top: 0; padding: 0;">
								<div id="airline_rt" style="padding: 15px 0;display:<?=$airlineShow_rt?> ">
									<label class="col-sm-2 control-label" for="airline_rt">航空公司</label>
									<div class="col-sm-10">
										<select name="airline_rt" id="airline_rt" class="form-control">
											<option value=""> --- 請選擇 --- </option>
											<?php foreach ($airlineArr_rt as $iCnt => $station) : ?>
											<option value="<?=$station['idx']?>" <?=( $station['idx'] == $airline) ? "selected" : "" ?>><?=$station['opt_name']?></option>
											<?php endforeach ; ?>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group" style="border-top: 0; padding: 0;">
								<div id="pier_rt" style="display:<?=$pierShow_rt?>">
									<label class="col-sm-2 control-label" for="pier_rt">碼頭出發地</label>
									<div class="col-sm-10">
										<select name="pier_rt" id="pier_rt" class="form-control">
											<option value=""> --- 請選擇 --- </option>
											<?php foreach ($pierArr_rt as $iCnt => $station) : ?>
											<option value="<?=$station['idx']?>" <?=( $station['idx'] == $pier_rt) ? "selected" : "" ?>><?=$station['opt_name']?></option>
											<?php endforeach ; ?>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group" style="border-top: 0; padding: 0;">
								<div id="flight_no_rt" style="padding: 15px 0 15px 0;display:<?=$flightnoShow_rt?> ">
									<label class="col-sm-2 control-label" for="flight_no_rt">班機編號</label>
									<div class="col-sm-10">
										<input type="text" name="flight_no_rt" value="<?=$flight_no_rt?>" placeholder="班機編號" id="flight_no_rt" class="form-control" />
									</div>
								</div>
							</div -->
						</div><!-- tab-content 3. 客戶資料 end -->
						<div class="tab-pane" id="tab-flight"><!-- tab-flight 4. 航船資訊 start -->
						</div><!-- tab-content 4. 航船資訊 end -->
						<div class="tab-pane" id="tab-DeliveryCar"><!-- tab-DeliveryCar 5. 交車程序 start -->
							<div class="row">
								<div class="col-sm-6 text-left" style="float:left;">
									<button type="button" onclick="$('a[href=\'#tab-accessories\']').tab('show');" class="btn btn-default"><i class="fa fa-arrow-left"></i> 上一步 </button>
								</div>
								<div class="col-sm-6 text-right" style="float:right;">
									<button type="button" id="button-DeliveryCar" class="btn btn-primary"><i class="fa fa-arrow-right"></i> 下一步 </button>
								</div>
							</div>
							<!-- 開始 -->
							<div id="qaContent">
								<ul class="col-md-12 accordionPart">
								<li>
									<div class="qa_title"><span>a</span> 營業時間 <b>我了解<INPUT TYPE="hidden" id="check_ok_1"></b></div>
									<div class="qa_content">
                                        <p>金城門市：08:30-18:30<br>機場門市：08:00-18:30<br>金湖門市：08:30-17:30<br><span class="highlight">超出營業時間需收取超時費用300元</span></p>
										<p onclick="change_checkmark(1);">
											<span id="checkmark_1_0"><img src="view/image/checkmark_dark_1.png" alt=""></span>
											<span id="checkmark_1_1" style="display:none;"><img src="view/image/checkmark_1.png" alt=""></span>
										</p>
                                    </div>
								</li>
								<li>
									<div class="qa_title"><span>b</span> 禁煙禁檳榔 <b>我了解<INPUT TYPE="hidden" id="check_ok_2"></b></div>
									<div class="qa_content">
										<p>租賃車輛嚴禁油煙、嚼食檳榔..等相關行為，如因造成車內異味或污損將收取 <span class="highlight">新台幣500元以上不等</span> 之清潔或是更換費用</p>
										<p onclick="change_checkmark(2);">
											<span id="checkmark_2_0"><img src="view/image/checkmark_dark_1.png" alt=""></span>
											<span id="checkmark_2_1" style="display:none;"><img src="view/image/checkmark_1.png" alt=""></span>
										</p>
									</div>
								</li>
								<li>
									<div class="qa_title"><span>c</span> 太武山 <b>我了解<INPUT TYPE="hidden" id="check_ok_3"></b></div>
									<div class="qa_content">
										<p>太武山為管制區域，汽機車均不得進入，違者裁罰。</p>
										<p onclick="change_checkmark(3);">
											<span id="checkmark_3_0"><img src="view/image/checkmark_dark_1.png" alt=""></span>
											<span id="checkmark_3_1" style="display:none;"><img src="view/image/checkmark_1.png" alt=""></span>
										</p>
									</div>
								</li>
								<li>
									<div class="qa_title"><span>d</span> 行李接送時間(限機車用戶) <b>我了解<INPUT TYPE="hidden" id="check_ok_4"></b></div>
									<div class="qa_content">
										<p>送行李時間於每日17:30前完成登記，接行李為每日09:30後航班(至營業時間結束)<br>
										<span class="highlight">( 行李放置妥當後無需等待服務人員，航班異動請提前告知，當日變更如無法承接敬請見諒 )</span></p>
										<p onclick="change_checkmark(4);">
											<span id="checkmark_4_0"><img src="view/image/checkmark_dark_1.png" alt=""></span>
											<span id="checkmark_4_1" style="display:none;"><img src="view/image/checkmark_1.png" alt=""></span>
										</p>
									</div>
								</li>
								<li>
									<div class="qa_title"><span>e</span> 油費注意事項 <b>我了解<INPUT TYPE="hidden" id="check_ok_5"></b></div>
									<div class="qa_content">
										<p><span class="highlight">*滿油租車滿油還車</span></p>
										<p onclick="change_checkmark(5);">
											<span id="checkmark_5_0"><img src="view/image/checkmark_dark_1.png" alt=""></span>
											<span id="checkmark_5_1" style="display:none;"><img src="view/image/checkmark_1.png" alt=""></span>
										</p>
									</div>
								</li>
								</ul>
							<div id="qaContent2">
								<div class="col-md-12 accordionPart">
									<div class="qa_title">
										<h3>租賃事宜</h3>
										<h4>小客車租賃契約</h4>
										<p>出租人（以下簡稱甲方）依本約正面及背面所載全部（包括印刷及手寫）之條款及條件，經簽約前與承租人（以下簡稱乙方）逐條說明契約內容審閱完成，將正面所指定之客車隨車附件租與乙方使用，雙方同意訂立本契約書。</p>
										<p>
											<span>第一條</span><br>
											小客車及隨車附件之所有權歸屬甲方，本合約僅係將該小客車及隨車附件租與乙方使用，乙方並不取得其他任何權利。在租賃期間內，乙方並非甲方任何目的之代理人，有關小客車之任何零件或附件之修護或更換，需經甲方事前核准。乙方於租賃期間內應依本合約給付甲方全部之租金與相關費用。如 乙方為兩人或以上時。應負連帶給付責任。甲方不另收取保證金或擔保品。
										</p>
										<p>
											<span>第二條</span><br>
											乙方及甲方已一併檢驗小客車，乙方同意該租用之小客車、安全配備與隨車附件齊全，具有良好之性能及狀況，並同意負擔於租賃期間內所消耗之燃料，並保證以合法銷售之無鉛汽油為限，如有違反本約定致租賃車輛故障者，應負擔損害賠償責任。乙方還車時，必需以出車時給付的油量基準計算， 剩餘之汽油不另退還。
										</p>
										<p>
											<span>第三條</span><br>
											本車輛每日行駛里程不得逾四百公里，逾四百公里，每一公里加收二元累計，但每日加收金額不得逾當日租金之半數。乙方應依約定時間交還車輛，還車時間逾期按車輛收費標準計算（不足一小時以一小時計算），逾期六小時以上者，以一日之租金計算收費。但因車輛本身機件故障，致乙方不能依約定時間交還車輛者，不在此限。提前還車時間每滿一日以上者，得請求退還每滿一日部分租金，但乙 方享有天數折扣者，甲方得依實際使用日數重算租金，再退還餘額。
										</p>

									</div>
									<div class="qa_content">
										<p>
											<span>第四條</span><br>
											租賃期間乙方應自行駕駛，非經甲方事先同意並登記於本合約不得交由他人駕駛，亦不得擅交無駕照之他駕駛、從事汽車運輸業行為或充作教練車等用途；並不得載送違禁品、危險品、不潔或易於污損車輛之物品及不適宜隨車之動物類。違反前兩項約定，甲方得終止租賃契約，並即時收回車輛， 如另有損害，並得向乙方請求賠償。
										</p>
										<p>
											<span>第五條</span><br>
											租賃期間乙方應隨身攜帶駕駛執照、強制汽車責任保險證、汽車出租單及行車執照以供稽查人員查驗，其間所生之停車費、過路通行費等費用，概由乙方自行負擔。前項因違規所生之處罰案件，有關罰鍰應由乙方負責繳清，如由甲方代為繳納者，乙方應負責償還；有關牌照被扣部份，自牌照被扣之 日起至公路監理機關通知得領回日止之租金，由乙方負擔。
										</p>
										<p>
											<span>第六條</span><br>
											該小客車不得使用於下列情況：<br>
											為收取明示或默示之報酬而攬載乘客或貨物。<br>
											用以推動或拖曳任何車輛或物體。<br>
											供競賽或其他試驗性目的。<br>
											為任何違反中華民國法令或其他違法目的之使用。<br>
											服用麻醉或迷幻藥物或酒醉駕車。<br>
											准許或指使任何無合法駕駛執照之人使用。<br>
											以詐欺或委虛僞陳述之方法向甲方取得小客車。<br>
											嚴禁超載。
										</p>
										<p>
											<span>第七條</span><br>
											本車輛發生擦撞、毀損、翻覆、失竊或其他肇事等意外事故時，乙方應立即報案並通知甲方以原廠修護處理。如因可歸責於乙方之事由所生之拖車費、修理費及第十條後段規定車輛修理期間之租金，應由乙方負責。<br><br>
											立即通知甲方及警察機關，且於24小時內提出包括圖解之詳細報告，並填寫甲方出具之意外事故報告表交與甲方。 金豐租車行服務專線：（082）371888，作相關後續處理。<br>
											取得意外之人及其他證人之姓名與地址。<br>
											未徵得保險公司同意前，不得私下與肇事對方達成和解。<br>
											警方處理完畢，若車輛行駛有危險之虞，請勿 將該小客車棄置於無人看管之處所。<br>
											若發現小客車有機件異常或中途拋錨，應立即告知甲方或至耆定之保修廠作緊急修護，甲方同意乙方等待車輛修護之時間得以延長補足，或免收租金，但因此造成之不便，時間等衍生之損失，均不得要求甲方賠償。公里內或一小時內，機件發生故障者，乙方應立即通知甲方處理，並得要求換車，如甲方無車供應，乙方得要求解約，雙方均不得要求任何一方賠償。但一般破胎或爆胎者由乙方負責自行修復。<br>
											如乙方怠於前項通知或違反本條（一）～（四）之規定，致喪失保險索賠權益時，所有損害均由乙方負擔。汽車之毀損非經甲方之許可擅自在外修護者，甲方得請求以原廠估價予以重修，修理費由乙方負擔。
										</p>
										<p>
											<span>第八條</span><br>
											乙方應盡善良管理人注意義務保管及維護本車輛，禁止出賣、設質、抵押、讓與擔保、典當車輛等行為。
										</p>
										<p>
											<span>第九條</span><br>
											該小客車甲方已經投保強制險，乙方依本合約之規定使用小客車時，造成第三人死亡，身體傷害之最 高理賠總額，每一事故新台幣 150 萬元。
										</p>
										<p>
											<span>第十條</span><br>
											因可歸責於乙方之事由致本車輛損壞或失竊者，需照市價賠償及支付實際修復金額。
										</p>
										<p>
											<span>第十一條</span><br>
											甲方應擔保租賃期間內本車輛合於約定使用狀態，如違反，雙方得依物之瑕庇擔保或債務不履行等相關法律規定辦理。
										</p>
										<p>
											<span>第十二條</span><br>
											因本契約發生訴訟時，甲、乙雙方同意以金門地方法院為第一審管轄法院，但不得排除消費者保護法第四十七條及民事訴訟法第四百三十六條之九 規定之小額訴訟管轄法院之適用。
										</p>
										<p>
											<span>第十三條</span><br>
											乙方欲續租本車輛時，應在甲方營業時間（08： 30 ～ 18：30）內事先聯繫並取得甲方之同意，始 為有效。若乙方告知還車日期，未準時歸還，甲方得以強制將車輛取回。
										</p>
										<p>
											<span>第十四條</span><br>
											乙本契約如有未訂事宜，依相關法、習慣及誠信原則公平解決之。
										</p>
										<p>
											<span>第十五條</span><br>
											甲方對乙方留存之個人資料依電腦負有保密義務，非經乙方書面同意，甲方不得對外揭露或為契約目的範圍外之利用，並應處理個人資料保護法之規定保護個人資料。
										</p>
										<p>
											<span>第十六條</span><br>
											太武山為管制區，汽機車均不得進入，違者裁罰。
										</p>
										<p>
											<span>第十七條</span><br>
											車輛發生事故或失竊時請立刻報警處理及通知出租人。<br>
											租金包含強制險。<br>
											上述保險不包括因自然災害造成之損傷及車內配備遺失或損壞之賠償責任，承租人有保管之責，若有損竊事實發生，相關賠償概由承租人負擔。<br>
											承租人使用期間所有停車費及交通罰鍰概由承租人負擔。<br>
											隨車證件：行照、強制保卡。
										</p>
										<div class="notice-box">
											<h4>取車需知</h4>
											(1)計費方式：本公司計費以 24 小時為一日，逾時 6 小時以一日計算，逾時依車型10%加收費用。<br>
											(2)保險方式：本公司所有租車均加保汽車強制險，如有事故顧客需賠償本公司維修之費用及本公司之營業損失。<br>
											(3)油料部份：本公司所有之租車所有油料部份皆需由顧客本身負擔。加滿給車，加滿還車。<br>
											(4)還車須知：請各位顧客於前往機場前30～60分鐘前與本公司服務人員聯絡，告知還車時間與地點。
										</div>
										<div class="notice-box">
											<h4>消退費規定</h4>
											<span>承租人應負責任及義務</span>
											承租人因租車期間衍生之停車費及罰單費用概由承租人自行負擔。<br>
											若因承租人行為導致承租車輛車牌吊扣，承租人須負擔本公司營業損失。<br>

											<span>取消訂單手續費收取說明</span>
											承租人未依約定時間取車，則依下表向承租人酌收營業損失費用。若因自然災害(例如：颱風、地震、洪水等)直接或間接影響租車行程，則可全額退費。

											<span>取消交易須酌收營業損失</span>
											於預定取車日當日或不通知取消，不退費。<br>
											於預定取車日前 1 日內取消(不含取車當日)，退 20%。<br>
											於預定取車日前 2-3 日前取消(不含取車當日)，退 30%。<br>
											於預定取車日前 4-6 日前取消(不含取車當日)，退 40%。<br>
											於預定取車日前 7-9 日前取消(不含取車當日)，退 50%。<br>
											於預定取車日 10 日前(含第 10 日)取消(不含取車當日)，退 100%。(但仍收取100元人工作業處理費用)
										</div>
									</div>
								</div>
								<label class="control-label" for="trans_type" style="color:red;">我己閱讀合約了</label>
								<input type="checkbox" name="ckboxConfirm2" value='yes' class="checkinfo">同意（我已詳細閱讀<a href="?route=information/page&token=A2DDDBF2-82AA-C541-F603-64B8ED6CE4D7" target="_blank">租賃契約</a>，並接受下述內容）
							</div>

							</div>
							<!-- 結束 -->
						</div><!-- tab-content 5. 交車程序 end -->
						<div class="tab-pane" id="tab-ExceptionalLoss"><!-- tab-ExceptionalLoss 5 損失賠償 start -->
							<div class="row">
								<div class="col-sm-6 text-left" style="float:left;">
									<button type="button" onclick="$('a[href=\'#tab-checkCar\']').tab('show');" class="btn btn-default"><i class="fa fa-arrow-left"></i> 上一步 </button>
								</div>

							</div>
						</div><!-- tab-ExceptionalLoss 5. 損失賠償 end -->
						<div class="tab-pane" id="tab-bigContract"><!-- tab-bigContract 6. 大合約 start -->
							<div class="row">
								<div class="col-sm-12 text-left" style="float:left;">
									<button type="button" onclick="$('a[href=\'#tab-checkCar\']').tab('show');" class="btn btn-default"><i class="fa fa-arrow-left"></i> 上一步 </button>
								</div>
							</div>
						</div><!-- tab-content 6. 大合約 end -->
						<div class="tab-pane" id="tab-checkCar"><!-- tab-checkCar 7. 車輛檢核 start -->
							<div class="row">
								<div class="col-sm-12 text-left" style="float:left;">
									<button type="button" onclick="$('a[href=\'#<?php echo $pick_up_id;?>\']').tab('show');" class="btn btn-default"><i class="fa fa-arrow-left"></i> 上一步 </button>
								</div>
								<!-- div class="col-sm-6 text-right" style="float:right;">
									<button type="button" id="button-checkCar" class="btn btn-primary"><i class="fa fa-arrow-right"></i> 下一步 </button>
								</div -->

							</div>
							<div class="form-group">
								<input type="hidden" name="car_total" value="<?php echo $car_total;?>" placeholder="訂車數量" id="car_total"/>
								<label class="col-sm-2 control-label">牌照號碼</label>
								<div class="col-sm-10" id="showArea">
									<?=$ckbCarNoHtml?>
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label" for="rent_miles_out"><font style="color:red;">出車里程數</font></label>
								<div class="col-sm-10">
									<input type="text" name="rent_miles_out" value="<?php echo $rentInfo['rent_miles_out']?>" class="form-control" placeholder="出車里程數" <?php echo ((isset($now_pickup) && $now_pickup == 'Y')?'':'disabled')?>/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="rent_miles_in"><font style="color:red;">還車里程數</font></label>
								<div class="col-sm-10">
									<input type="text" name="rent_miles_in" value="<?php echo $rentInfo['rent_miles_in']?>" class="form-control" placeholder="還車里程數" <?php echo ((isset($now_pickup) && $now_pickup == 'Y')?'disabled':'')?>/>
								</div>
							</div>

							<!-- div class="form-group">
<?php
								/*
								$img_arr = array("1"=>"左前","2"=>"右前","3"=>"左後","4"=>"右後");
								$thumb_arr = array("1"=>$thumb1,"2"=>$thumb2,"3"=>$thumb3,"4"=>$thumb4);
								foreach($img_arr as $key=>$val){
									echo '<div class="form-group">
									<label class="col-sm-2 control-label" for="add_name">'.$val.'</label>
									<div class="col-sm-10">
										'.((isset($now_pickup) && $now_pickup == 'Y')
											?'<label class="btn">
												<input id="image'.$key.'" style="display:none;" type="file"  onchange="readURL(this,'.$key.')" accept="image/*" name="image'.$key.'" >

												<img src="'.$thumb_arr[$key].'" id="img_'.$key.'" />
											</label>'
											:'<img src="'.$thumb_arr[$key].'" alt="" title="" data-placeholder="'.$placeholder.'" />').'
									</div>
								</div>';

								}*/
?>
							</div -->
							<div class="form-group">
								<label class="col-sm-2 control-label" for="rent_miles_in">車輛外觀檢查</label>
								<div class="col-sm-10">
									<div class="row">
										<img class="col-sm-6" src="view/image/fuel.jpg" alt="">
										<img class="col-sm-6" src="view/image/oil.jpg" alt="">
									</div>
									<br>
									<p class="checkyes"><input type="checkbox" name="ckboxCrim" value="yes"> 已確認</p>
									汽車回程車輛未滿油按左表收取油費<br>
									機車未滿油加收200元作業費用及油費（機車因不用回廠整理，故收取較高人員移動作業費用，請見諒）
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="add_name">錄影人員</label>
								<div class="col-sm-10">
									<select class="form-control" id="add_name" name="add_name">
<?php
									foreach($userList as $k=>$arr){
										$selected = "";
										if((isset($now_pickup) && $now_pickup == 'Y') && $user_allname == $arr['lastname'].$arr['firstname']){
											$selected = "selected";
										}else if($user_allname == ""){
											if($nowname == $arr['lastname'].$arr['firstname']){
												$selected = "selected";
											}
										}else{
											if($user_allname == $arr['user_id']){
											$selected = "selected";
											}
										}
										echo '<option value="'.$arr['user_id'].'" '.$selected.'>'.$arr['lastname'].''.$arr['firstname'].'</option>';
									}
?>
									</select>


								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="add_time">錄影時間</label>
								<div class="col-sm-10">
									<input type="datetime-local" name="add_time" value="<?php echo $add_time;?>" class="form-control" placeholder="錄影時間" <?php echo ((isset($now_pickup) && $now_pickup == 'Y')?'':'disabled')?>/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="check_name">驗車人員</label>
								<div class="col-sm-10">

									<select class="form-control" id="check_name" name="check_name">
<?php

									foreach($userList as $k=>$arr){
										$selected = "";
										$check_name = isset($check_name)?$check_name:"";
										if((isset($now_pickup) && $now_pickup == 'Y') && $check_name == $arr['lastname'].$arr['firstname']){
											$selected = "selected";
										}else if($check_name==""){
											if($nowname == $arr['lastname'].$arr['firstname']){
												$selected = "selected";
											}
										}else{
											if($check_name == $arr['user_id']){
											$selected = "selected";
											}
										}
										echo '<option value="'.$arr['user_id'].'" '.$selected.'>'.$arr['lastname'].''.$arr['firstname'].'</option>';
									}
?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">MEMO</label>
								<div class="col-sm-9">
									<input type="text" name="remark2" id="remark2" class="form-control">
								</div>
								<div class="col-sm-1" style="padding: 7px 0; text-align: left;">
									<a onclick="ins_reamrk('remark2');" class="add-btn" href="javascript:void();">＋</a>
								</div>

							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">備註</label>
								<div class="col-sm-10">
									<textarea id="area_memo3" name="area_memo3" cols="100" rows="5" disabled><?php echo (($area_memo=="")?'輸入你想要寫的內容...':$area_memo);?></textarea>
								</div>
							</div>

							<div class="part" id="sign_div" style="background-color:#FFD382;padding:10px;margin-bottom:5px;display:none;">
							<?php if ( isset( $signature)) : ?>
								<div>
									<img src="view/image/signature/<?=$signature[0]?>" alt="簽名">
								</div>
							<?php endif ; ?>
								<div>
									確認簽名
									<button type="button" data-toggle="tooltip" title="清除" class="btn btn-primary signatureClear">清除</button>
									<button type="button" data-toggle="tooltip" title="確認" class="btn btn-primary signatureSave">簽名完成請確認</button>
								</div>
								<input type="hidden" class="signNameBin" id="signNameBin" name="signNameBin[]">
								<canvas class="signatureArea" style="position: relative; margin: 0; padding: 0; border: 1px solid #c4caac;"></canvas>
							</div>

<?
							if(!isset($now_pickup) || $now_pickup == "N"){

?>
							<!-- 還車作業 -->
							<div class="form-group">
								<label class="col-sm-2 control-label" for="flight_no">營業損失</label>
								<div class="col-sm-10">
									<input type="text" name="business_lose" value="<?=$rentInfo['business_lose']?>" class="form-control" placeholder="營業損失"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="flight_no">車體損失</label>
								<div class="col-sm-10">
									<input type="text" name="car_lose" value="<?=$rentInfo['car_lose']?>" class="form-control" placeholder="車體損失"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="overTime_cost">逾時費用</label>
								<div class="col-sm-10">
									<input type="text" name="overTime_cost" value="<?php echo (($rentInfo['overTime_cost']=="")?0:$rentInfo['overTime_cost']);?>" class="form-control" placeholder="逾時費用"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="other_lose">其他加收</label>
								<div class="col-sm-10">
									<input type="text" name="other_lose" value="<?php echo (($rentInfo['other_lose']=="")?0:$rentInfo['other_lose']);?>" class="form-control" placeholder="其他加收"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="oil_cost">油費</label>
								<div class="col-sm-10">
									<input type="text" name="oil_cost" value="<?php echo (($rentInfo['oil_cost']=="")?0:$rentInfo['oil_cost']);?>" class="form-control" placeholder="油費"/>
								</div>
							</div>
							<!-- div class="form-group">
								<label class="col-sm-2 control-label">備註</label>
								<div class="col-sm-10">
									<textarea id="area_memo1" cols="100" rows="5" disabled><?php echo (($area_memo=="")?'輸入你想要寫的內容...':$area_memo);?></textarea>
								</div>
							</div -->
							<div class="form-group">
								<label class="col-sm-2 control-label" for="lose_type">費用支付方式</label>
								<div class="col-sm-10">
									<input type="radio" name="lose_type" value="4" <?php echo (($rentInfo['lose_type']==4)?'checked':'')?>> 現金
									<input type="radio" name="lose_type" value="5" <?php echo (($rentInfo['lose_type']==5)?'checked':'')?>> 信用卡
								</div>
							</div>
							<div class="part" id="signin_div" style="background-color:#FFD382;padding:10px;margin-bottom:5px;display:none;">
							<?php if ( isset( $signature) && isset($signature[$iCnt])) : ?>
								<!-- div class="row">
									　<img src="view/image/signature/<?=$signature[$iCnt]?>" alt="簽名" >
								</div -->
							<?php endif ; ?>
								<div class="row" style="margin: 20px 0;padding: 15px;border: 1px solid #eee;background: #f4f4f4;">
								確認簽名
									<button type="button" data-toggle="tooltip" title="清除" class="btn btn-primary signatureClear2">清除</button>
									<button type="button" data-toggle="tooltip" title="確認" class="btn btn-primary signatureSave2">簽名完成請確認</button>
								</div>
								<input type="hidden" class="signNameBin2" id="signNameBin2" name="signNameBin2[]">
								<canvas class="signatureArea2" style="position: relative; margin: 0; padding: 0; border: 1px solid #c4caac;"></canvas>
							</div>


<?
							}
?>
							<!-- div class="form-group">
								<label class="col-sm-2 control-label" for="mobile_moto"><font style="color:red">異動差額</font></label>
								<div class="col-sm-10">
									<input type="text" name="pickdiff_1" id="pickdiff_1" value="<?=$rentInfo['pickdiff']?>" class="form-control"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">備註</label>
								<div class="col-sm-10">
									<textarea name="area_memo" id="area_memo" cols="100" rows="5"><?php echo (($area_memo=="")?'輸入你想要寫的內容...':$area_memo);?></textarea>
								</div>
							</div -->


						</div>

						<?php foreach ($rentInfo['other_car'] as $iCnt => $carInfo) :
						$activeStr = ( $iCnt == 0) ?  "active" : "" ;
						?>
						<div class="tab-pane" id="tab-<?=$carInfo['idx']?>">
							<div class="tab-content"><!-- tab-content 3. 車輛狀況 start -->
								<input type="hidden" name="car_sn[]" value="<?=$carInfo['idx']?>">
								<input type="hidden" name="car_no[]" value="<?=$carInfo['car_no']?>">
								<div class="condition">
									<div class="part">
										<div class="leftbox">
											<div class="zone zone-1">
												<p class="blockpart">正面</p>
												<p class="pimg2">
													<span class="num num-1">1</span>
													<span class="num num-2">2</span>
													<span class="num num-3">3</span>
													<span class="num num-4">4</span>
													<span class="num num-5">5</span>
													<span class="num num-6">6</span>
													<span class="bar num-6"></span>
													<span class="num num-7">7</span>
													<span class="bar num-7"></span>
													<span class="num num-8">8</span>
													<span class="bar num-8"></span>
													<span class="num num-9">9</span>
													<span class="bar num-9"></span>
												</p>
												<div class="condition-item">
													<?php foreach ($car_checkItems['front'] as $labelID => $labelName) : ?>
													<p>
														<label for=""><?=$labelName?></label>
														<select class="selectpicker" multiple name="<?=$carInfo['idx']?>_<?=$labelID?>[]">
															<?php foreach ($ext_status as $i => $optionDesc) :
																$optSelStr = in_array( $optionDesc['idx'], $car_checkInfos['front'][$carInfo['idx']][$labelID]) ? "selected" : "" ;
															?>
															<option data-content="<span class='label <?=$optionDesc['opt_desc']?>'><b class='<?=$optionDesc['opt_short_desc']?>'></b><?=$optionDesc['opt_name']?></span>" value="<?=$optionDesc['idx']?>" <?=$optSelStr?>><?=$optionDesc['opt_name']?></option>
															<?php endforeach ; ?>
														</select>
													</p>
													<?php endforeach ; ?>
												</div>
											</div><!--zone-1 end-->
											<div class="zone zone-3">
												<p class="blockpart">駕駛側</p>
												<p class="pimg2">
													<span class="num num-16">16</span>
													<span class="num num-17">17</span>
													<span class="num num-18">18</span>
													<span class="num num-19">19</span>
													<span class="num num-20">20</span>
													<span class="num num-21">21</span>
													<span class="num num-t1">T1</span>
													<span class="bar num-t1"></span>
													<span class="num num-t2">T2</span>
													<span class="bar num-t2"></span>
												</p>
												<div class="condition-item">
													<?php foreach ($car_checkItems['left'] as $labelID => $labelName) : ?>
													<p>
														<label for=""><?=$labelName?></label>
														<select class="selectpicker" multiple name="<?=$carInfo['idx']?>_<?=$labelID?>[]">
															<?php foreach ($ext_status as $i => $optionDesc) :
																$optSelStr = in_array( $optionDesc['idx'], $car_checkInfos['left'][$carInfo['idx']][$labelID]) ? "selected" : "" ;
															?>
															<option data-content="<span class='label <?=$optionDesc['opt_desc']?>'><b class='<?=$optionDesc['opt_short_desc']?>'></b><?=$optionDesc['opt_name']?></span>" value="<?=$optionDesc['idx']?>" <?=$optSelStr?>><?=$optionDesc['opt_name']?></option>
															<?php endforeach ; ?>
														</select>
													</p>
													<?php endforeach ; ?>
												</div>
											</div><!--zone-3 end-->
										</div>
										<div class="rightbox">
											<div class="zone zone-2">
												<p class="blockpart">背面</p>
												<p class="pimg2">
													<span class="num num-10">10</span>
													<span class="num num-11">11</span>
													<span class="num num-12">12</span>
													<span class="num num-13">13</span>
													<span class="num num-14">14</span>
													<span class="bar num-14"></span>
													<span class="num num-15">15</span>
													<span class="bar num-15"></span>
												</p>
												<div class="condition-item">
													<?php foreach ($car_checkItems['back'] as $labelID => $labelName) : ?>
													<p>
														<label for=""><?=$labelName?></label>
														<select class="selectpicker" multiple name="<?=$carInfo['idx']?>_<?=$labelID?>[]">
															<?php foreach ($ext_status as $i => $optionDesc) :
																$optSelStr = in_array( $optionDesc['idx'], $car_checkInfos['back'][$carInfo['idx']][$labelID]) ? "selected" : "" ;
															?>
															<option data-content="<span class='label <?=$optionDesc['opt_desc']?>'><b class='<?=$optionDesc['opt_short_desc']?>'></b><?=$optionDesc['opt_name']?></span>" value="<?=$optionDesc['idx']?>" <?=$optSelStr?>><?=$optionDesc['opt_name']?></option>
															<?php endforeach ; ?>
														</select>
													</p>
													<?php endforeach ; ?>
													<p></p>
													<p></p>
													<p></p>
												</div>
											</div><!--zone-2 end-->
											<div class="zone zone-4">
												<p class="blockpart">副駕駛側</p>
												<p class="pimg2">
													<span class="num num-22">22</span>
													<span class="num num-23">23</span>
													<span class="num num-24">24</span>
													<span class="num num-25">25</span>
													<span class="num num-26">26</span>
													<span class="num num-27">27</span>
													<span class="num num-t3">T3</span>
													<span class="bar num-t3"></span>
													<span class="num num-t4">T4</span>
													<span class="bar num-t4"></span>
												</p>
												<div class="condition-item">
													<?php foreach ($car_checkItems['right'] as $labelID => $labelName) : ?>
													<p>
														<label for=""><?=$labelName?></label>
														<select class="selectpicker" multiple name="<?=$carInfo['idx']?>_<?=$labelID?>[]">
															<?php foreach ($ext_status as $i => $optionDesc) :
																$optSelStr = in_array( $optionDesc['idx'], $car_checkInfos['right'][$carInfo['idx']][$labelID]) ? "selected" : "" ;
															?>
															<option data-content="<span class='label <?=$optionDesc['opt_desc']?>'><b class='<?=$optionDesc['opt_short_desc']?>'></b><?=$optionDesc['opt_name']?></span>" value="<?=$optionDesc['idx']?>" <?=$optSelStr?>><?=$optionDesc['opt_name']?></option>
															<?php endforeach ; ?>
														</select>
													</p>
													<?php endforeach ; ?>
												</div>
											</div><!--zone-4 end-->
										</div>
									</div>
												<!--<div class="button_line">
														<button class="send" type="button">確認新增</button>
														<button class="delete" type="button">刪除</button>
												</div>-->
								</div>
							</div><!-- tab-content 3. 車輛狀況 end -->
						</div>
						<?php endforeach ; ?>
						</div>
					</form>
				</div>
			</div>
		</div>

<script type="text/javascript"><!--
// 打勾功能
$(document). ready(function(){
	var now_pickup = '<?php echo $now_pickup;?>';
	$('button[type="submit"]').attr("disabled",true);

	//取車作業預設才要把存檔disabled
	if(now_pickup == 'Y'){
		$('button[type="submit"]').attr("disabled",true);
		$('input[name="ckboxConfirm2"]').click(function(){
			//改檢查check_box是否打開
			clickCnt = 0;
			for(i=1; i<=5; i++){
				if($('#check_ok_'+i).val() == 1){
					clickCnt++;
				}
			}

			var pick_flag = '<?php echo $now_pickup;?>';
			var flag = 'Y';
			if(pick_flag == 'Y'){
				//if($('input[name="rent_miles_out"]').val() == '' && $('input[name="rent_miles_out"]').val() == 0){
				//	flag = 'N';
					//alert('請填寫出車里程數');
				//}
			}else{
				//if($('input[name="rent_miles_in"]').val() == '' && $('input[name="rent_miles_in"]').val() == 0){
				//	flag = 'N';
				//	//alert('請填寫還車里程數');
				//}
			}

			if(flag == 'Y'){
				//先檢查未讀資訊是否已讀
				if(clickCnt != 5){
					alert("您未閱讀完所有資訊") ;
					$('a[href=\'#tab-DeliveryCar\']').tab('show');
					$(this).prop("checked", false);
				}/*else if($('#signNameBin').val() == ''){
					//檢查簽名是否存檔了
					alert("您尚未簽名") ;
					$('body, html').animate({scrollTop: $('#sign_div').offset().top+'px'}, 800);
					$(this).prop("checked", false);
				}*/else{
					//跳到下一貢
					$('a[href=\'#tab-checkCar\']').tab('show');

					//if($(this).prop("checked") == true){
						//顯示簽名
						//$("#sign_div").css("display",'block');
						//$('.agreed').fadeIn(300).delay(2500).fadeOut(300);
						//$('button[type="submit"]').attr("disabled",false);
					//}
				}
			}else{
				$('a[href=\'#tab-checkCar\']').tab('show');
				$(this).prop("checked", false);
			}
			//console.log('test');
		});

		$('input[name="ckboxCrim"]').click(function(){
			$('button[type="submit"]').attr("disabled",true);
			clickCnt = 0;
			for(i=1; i<=5; i++){
				if($('#check_ok_'+i).val() == 1){
					clickCnt++;
				}
			}

			var pick_flag = '<?php echo $now_pickup;?>';
			var flag = 'Y';
			if(pick_flag == 'Y'){
				if($('input[name="rent_miles_out"]').val() == '' && $('input[name="rent_miles_out"]').val() == 0){
					flag = 'N';
					alert('請填寫出車里程數');
				}

				var c_num = 0;
				$('input[name^="useCars"]').each(function() {
					if($(this).parent().attr("class").indexOf("clean")===-1){
						c_num ++;
					}
				});

				if(c_num == 0){
					flag = 'N';
					alert('請選擇牌照號碼');
				}

			}else{
				//if($('input[name="rent_miles_in"]').val() == '' && $('input[name="rent_miles_in"]').val() == 0){
				//	flag = 'N';
				//	//alert('請填寫還車里程數');
				//}
			}

			if(flag == 'Y'){
				//先檢查未讀資訊是否已讀
				if(clickCnt != 5){
					alert("您未閱讀完所有資訊") ;
					$('a[href=\'#tab-DeliveryCar\']').tab('show');
					$(this).prop("checked", false);
				}/*else if($('#signNameBin').val() == ''){
					//檢查簽名是否存檔了
					alert("您尚未簽名") ;
					$('body, html').animate({scrollTop: $('#sign_div').offset().top+'px'}, 800);
					$(this).prop("checked", false);
				}*/else{
					if($(this).prop("checked") == true){
						//顯示簽名
						$("#sign_div").css("display",'block');
						//$('.agreed').fadeIn(300).delay(2500).fadeOut(300);
						//$('button[type="submit"]').attr("disabled",false);
					}
				}
			}else{
				$('a[href=\'#tab-checkCar\']').tab('show');
				$(this).prop("checked", false);
			}
		});


	}else{
		//把前三個頁籤鎖起來
		$('#tab-cart :input').prop('disabled',true) ;
		$('#tab-customer :input').prop('disabled',true) ;
		$('#tab-accessories :input').prop('disabled',true) ;
		$('input[name="useCars[]"]').prop("disabled",true);
		$('select[name="add_name"]').prop("disabled",true);
		$('select[name="check_name"]').prop("disabled",true);

		//檢查 費用支付方式 有資訊才能顯示簽名
		$('input[name="ckboxCrim"]').click(function(){
			var pick_flag = '<?php echo $now_pickup;?>';
			var flag = 'Y';
			if($('input[name="rent_miles_in"]').val() == '' || $('input[name="rent_miles_in"]').val() == 0){
				flag = 'N';
				alert('請填寫還車里程數');
				$(this).prop("checked", false);
			}else{
				var lose_type = $('input:radio[name="lose_type"]:checked').val();
				 if(parseInt($('input[name="rent_miles_in"]').val()) < parseInt($('input[name="rent_miles_out"]').val())){
					//檢查還車里程數 < 出車里程數 才可以過
					flag = 'N';
					alert('還車里程數不可小於出車里程數');
					$(this).prop("checked", false);

				}else{
					//檢查費用資訊
					if(parseInt($('input[name="business_lose"]').val()) > 0 || parseInt($('input[name="car_lose"]').val()) > 0 || parseInt($('input[name="overTime_cost"]').val()) > 0 || parseInt($('input[name="other_lose"]').val()) > 0 || parseInt($('input[name="oil_cost"]').val()) > 0 ){
						if(lose_type){
							$("#signin_div").css("display",'block');
						}else{
							flag = 'N';
							alert('費用支付方式未選擇');
							$(this).prop("checked", false);
						}
					}else{
						$("#signin_div").css("display",'block');
					}
				}
			}
		});


	}
});
//--></script></div>
<div class="agreed">
	<span class="helper"></span>
	<div>
		<div class="circle">
			<div class="check-signal"></div>
		</div>
	</div>
</div>

<!-- script type="text/javascript" src="view/javascript/canvasSignature.js"></script-->
<script>
var clickCnt = 0;
// add by Angus
$('#button-DeliveryCar').on('click', function() {
	clickCnt = 0;
	for(i=1; i<=5; i++){
		if($('#check_ok_'+i).val() == 1){
			clickCnt++;
		}
	}

	if ( clickCnt == 5 && $('input[name="ckboxConfirm2"]').prop("checked")) {
		$('a[href=\'#tab-checkCar\']').tab('show');
	} else {
		alert("您未閱讀完所有資訊") ;
	}
});
//檢查出還車里程數 必填
$('#button-checkCar').on('click', function() {
	var pick_flag = '<?php echo $now_pickup;?>';
	var flag = 'Y';
	if(pick_flag == 'Y'){
		if($('input[name="rent_miles_out"]').val() == '' || $('input[name="rent_miles_out"]').val() == 0){
			flag = 'N';
			alert('請填寫出車里程數');
		}
	}else{
		if($('input[name="rent_miles_in"]').val() == '' || $('input[name="rent_miles_in"]').val() == 0){
			flag = 'N';
			alert('請填寫還車里程數');
		}
	}

	if(flag == 'Y'){
		$('a[href=\'#<?php echo $car_nest_id;?>\']').tab('show');
	}

});
$(function(){
	$('#qaContent2 div.accordionPart div.qa_title').click(function(){
		$(this).toggleClass('qa_title_on');
		$(this).next('div.qa_content').slideToggle();
		var colorStr = $(this).parent().css( "background-color") ;
		if ( colorStr == "rgb(255, 255, 255)" ) {
			//clickCnt ++ ;
		}
		$(this).parent().css( "background-color", "#edf9ff" );
	}).siblings('div.qa_content').hide();
});

// 更換頁籤 方法一
$('#button-cart2').on('click', function() {
	//檢查付款類型是否必填
	var pay_type = $('input:radio[name="pay_type"]:checked').val();
	if(pay_type){
		$('a[href=\'#<?=$pick_nest_id?>\']').tab('show');
	}else{
		alert("付款類型必填");
		return false;
	}
});
$('#button-cart').on('click', function() {
	//換頁簽前要先檢查車牌是否數量一致
	//getuseCars
	var flag = 'Y';
	var c_num = 0;
	car_total = $("#car_total").val();
	if(car_total != 0){
		//檢查編輯時原先選取數量與車量是否一致
		$('input[name^="useCars"]').each(function() {
			if($(this).parent().attr("class").indexOf("clean")===-1){
				c_num ++;
			}
		});

		if(c_num != car_total){
			flag = 'N';
		}
	}
	$('a[href=\'#tab-customer\']').tab('show');
	if(flag == 'Y'){
		//console.log('tab-accessories show');
		//$('a[href=\'#tab-accessories\']').tab('show');
	}else{
		//alert('所選牌照號碼數量與訂車數量不一致！');
	}
});

	// 簽名區 Constructor
	$('.signatureArea').each( function (index) {
		// console.log( this ) ;
		signatureCapture( this) ;
	}) ;

	$('.signatureClear').click( function () {
		// console.log( this ) ;
		// console.log( $(this).parent().parent().find('.signatureArea')) ;
		var canvas = $(this).parent().parent().find('.signatureArea').get()[0] ;
		console.log( canvas);
		mainConfig( canvas) ;
	}) ;

	$('.signatureSave').click( function () {
		var canvas = $(this).parent().parent().find('.signatureArea').get()[0] ;
		var dataURL = canvas.toDataURL("image/png") ;
		console.log( dataURL) ;
		$(this).parent().parent().find('.signNameBin').val( dataURL) ;
		$(this).parent().parent().find('.signatureClear').attr('disabled','disabled') ;
		$(this).parent().parent().find('.signatureSave').attr('disabled','disabled') ;
		//一併存檔
		//$('.agreed').fadeIn(300).delay(2500).fadeOut(300);
		$('button[type="submit"]').attr("disabled",false);
		$("#area_memo").attr('disabled',false) ;
		$('#pick_save').submit();
	}) ;

	// 簽名區 Constructor
	$('.signatureArea2').each( function (index) {
		// console.log( this ) ;
		signatureCapture( this) ;
	}) ;

	$('.signatureClear2').click( function () {
		// console.log( this ) ;
		// console.log( $(this).parent().parent().find('.signatureArea')) ;
		var canvas = $(this).parent().parent().find('.signatureArea2').get()[0] ;
		console.log( canvas);
		mainConfig( canvas) ;
	}) ;

	$('.signatureSave2').click( function () {
		var canvas = $(this).parent().parent().find('.signatureArea2').get()[0] ;
		var dataURL = canvas.toDataURL("image/png") ;
		console.log( dataURL) ;
		$(this).parent().parent().find('.signNameBin2').val( dataURL) ;
		$(this).parent().parent().find('.signatureClear2').attr('disabled','disabled') ;
		$(this).parent().parent().find('.signatureSave2').attr('disabled','disabled') ;

		var pick_flag = '<?php echo $now_pickup;?>';
		var flag = 'Y';
		if(pick_flag == 'Y'){
			if($('input[name="rent_miles_out"]').val() == '' || $('input[name="rent_miles_out"]').val() == 0){
				flag = 'N';
				alert('請填寫出車里程數');
			}
			var c_num = 0;
			$('input[name^="useCars"]').each(function() {
				if($(this).parent().attr("class").indexOf("clean")===-1){
					c_num ++;
				}
			});

			if(c_num == 0){
				flag = 'N';
				alert('請選擇牌照號碼');
			}


		}else{
			if($('input[name="rent_miles_in"]').val() == '' || $('input[name="rent_miles_in"]').val() == 0){
				flag = 'N';
				alert('請填寫還車里程數');
			}

			var lose_type = $('input:radio[name="lose_type"]:checked').val();
			//檢查費用資訊
			if(parseInt($('input[name="business_lose"]').val()) > 0 || parseInt($('input[name="car_lose"]').val()) > 0 || parseInt($('input[name="overTime_cost"]').val()) > 0 || parseInt($('input[name="other_lose"]').val()) > 0 || parseInt($('input[name="oil_cost"]').val()) > 0){
				if(lose_type){

				}else{
					flag = 'N';
					alert('費用支付方式未選擇');
				}
			}
		}

		if(flag == 'Y'){
			$('button[type="submit"]').attr("disabled",false);
			$("input[name='selCarModels[]']").prop('disabled',false) ;
			$("input[name='selCarTypes[]']").prop('disabled',false) ;
			$("input[name='useCars[]']").prop('disabled',false) ;
			$("input[name='order_id']").prop('disabled',false) ;
			$("input[name='agreement_no']").prop('disabled',false) ;
			$("#area_memo").attr('disabled',false) ;

			$('#tab-cart :input').prop('disabled',false) ;
			$('#tab-customer :input').prop('disabled',false) ;
			$('#tab-accessories :input').prop('disabled',false) ;
			$('input[name="useCars[]"]').prop("disabled",false);
			$('select[name="add_name"]').prop("disabled",false);
			$('select[name="check_name"]').prop("disabled",false);

			$('#pick_save').submit();
		}else{
			$('a[href=\'#tab-checkCar\']').tab('show');
			$(this).parent().parent().find('.signatureClear2').prop('disabled',false) ;
			$(this).parent().parent().find('.signatureSave2').prop('disabled',false) ;

		}

	}) ;


function mainConfig( canvas) {
	var context = canvas.getContext("2d");
	// 清除
	context.clearRect(0, 0, canvas.width, canvas.height);
	// 重新config
	context.fillStyle = "#fff";
	context.strokeStyle = "#444";
	context.lineWidth = 1.2;
	context.lineCap = "round";

	context.fillRect(0, 0, canvas.width, canvas.height);

	context.fillStyle = "#3a87ad";
	context.strokeStyle = "#3a87ad";
	context.lineWidth = 4;
	//context.moveTo(20,125);
	//context.lineTo(454,125);
	context.stroke();

	context.fillStyle = "#fff";
	context.strokeStyle = "#444";
}

function signatureCapture( canvas) {
	// var canvas = document.getElementById("newSignature");
	var context = canvas.getContext("2d");

	if (!context) {
		throw new Error("Failed to get canvas' 2d context");
	}

	canvas.width = 474 ;
	canvas.height = 180 ;

	context.fillStyle = "#fff";
	context.strokeStyle = "#444";
	context.lineWidth = 1.2;
	context.lineCap = "round";

	context.fillRect(0, 0, canvas.width, canvas.height);

	context.fillStyle = "#3a87ad";
	context.strokeStyle = "#3a87ad";
	context.lineWidth = 4;
	//context.moveTo(20,125);
	//context.lineTo(454,125);
	context.stroke();

	context.fillStyle = "#fff";
	context.strokeStyle = "#444";


	var disableSave = true;
	var pixels = [];
	var cpixels = [];
	var xyLast = {};
	var xyAddLast = {};
	var calculate = false;
	//functions
	{
		function remove_event_listeners() {
			canvas.removeEventListener('mousemove', on_mousemove, false);
			canvas.removeEventListener('mouseup', on_mouseup, false);
			canvas.removeEventListener('touchmove', on_mousemove, false);
			canvas.removeEventListener('touchend', on_mouseup, false);

			document.body.removeEventListener('mouseup', on_mouseup, false);
			document.body.removeEventListener('touchend', on_mouseup, false);
		}

		function get_board_coords(e) {
			var x, y;

			if (e.changedTouches && e.changedTouches[0]) {
				var offsety = canvas.offsetTop || 0;
				var offsetx = canvas.offsetLeft || 0;

				x = e.changedTouches[0].pageX - offsetx;
				y = e.changedTouches[0].pageY - offsety;
			} else if (e.layerX || 0 == e.layerX) {
				x = e.layerX;
				y = e.layerY;
			} else if (e.offsetX || 0 == e.offsetX) {
				x = e.offsetX;
				y = e.offsetY;
			}

			return {
				x : x,
				y : y
			};
		};

		function on_mousedown(e) {
			e.preventDefault();
			e.stopPropagation();

			canvas.addEventListener('mousemove', on_mousemove, false);
			canvas.addEventListener('mouseup', on_mouseup, false);
			canvas.addEventListener('touchmove', on_mousemove, false);
			canvas.addEventListener('touchend', on_mouseup, false);

			document.body.addEventListener('mouseup', on_mouseup, false);
			document.body.addEventListener('touchend', on_mouseup, false);

			empty = false;
			var xy = get_board_coords(e);
			context.beginPath();
			pixels.push('moveStart');
			context.moveTo(xy.x, xy.y);
			pixels.push(xy.x, xy.y);
			xyLast = xy;
		};

		function on_mousemove(e, finish) {
			e.preventDefault();
			e.stopPropagation();

			var xy = get_board_coords(e);
			var xyAdd = {
				x : (xyLast.x + xy.x) / 2,
				y : (xyLast.y + xy.y) / 2
			};

			if (calculate) {
				var xLast = (xyAddLast.x + xyLast.x + xyAdd.x) / 3;
				var yLast = (xyAddLast.y + xyLast.y + xyAdd.y) / 3;
				pixels.push(xLast, yLast);
			} else {
				calculate = true;
			}

			context.quadraticCurveTo(xyLast.x, xyLast.y, xyAdd.x, xyAdd.y);
			pixels.push(xyAdd.x, xyAdd.y);
			context.stroke();
			context.beginPath();
			context.moveTo(xyAdd.x, xyAdd.y);
			xyAddLast = xyAdd;
			xyLast = xy;

		};

		function on_mouseup(e) {
			remove_event_listeners();
			disableSave = false;
			context.stroke();
			pixels.push('e');
			calculate = false;
		};

	}//end

	canvas.addEventListener('mousedown', on_mousedown, false);
	canvas.addEventListener('touchstart', on_mousedown, false);
}

function readURL(input, key){
  if(input.files && input.files[0]){
    var reader = new FileReader();
    reader.onload = function (e) {
       $("#img_"+key).attr("src", e.target.result);
       $("#img_"+key).attr("width", 100);
       $("#img_"+key).attr("height", 100);
    }
    reader.readAsDataURL(input.files[0]);
  }

}
$("#car_sn").change(function(){
	$("#car_no").val($(this).find('option:selected').text());
});

// datetimepicker setting
$('.date').datetimepicker({
	pickTime: false
});
$('.date').datetimepicker().on('dp.change', function (event) {
	change_day();
});


// add by Angus 2018.08.09
function reLoadCarTypeJs() {
	//console.log( 'in function reLoadCarTypeJs ') ;
	//console.log( '?route=sale/order/forOrderGetCarTypeJs&token=<?php echo $token; ?>');
	$.ajax({
		url : '?route=sale/order/forOrderGetCarTypeJs&token=<?php echo $token; ?>',
		dataType: 'html',
		success: function(html) {
			//console.log(html);
			$('#showJs').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
}



/*$("label[name='useCar']").click( function () {
	// var classStr = $(this).attr('class') ;
	// console.log( classStr);
	var now_pickup = '<?php echo $now_pickup;?>';
	if(now_pickup == 'Y'){

		var ckbObj = $(this).find("input[type='checkbox']")
		//console.log( ckbObj.is(':checked')) ;
		if ( ckbObj.is(':checked')) {
			$(this).removeClass('bt-background-clean') ;
		} else {
			$(this).addClass('bt-background-clean') ;
		}
	}
}) ;*/

// 處理交通工具 分為 飛機 輪船
$("input[name='trans_type']").change( function () {
	var transType = $(this).val() ;
	console.log('trans_type : '+transType) ;
	if ( transType == 1) {	// 飛機
		$('#airport').show() ;
		$('#airline').show() ;
		$('#flight_no').show() ;
		$('#pier').hide() ;
		$('#sdate_name').text("預定起飛時間");
	} else {				// 輪船
		$('#airport').hide() ;
		$('#airline').hide() ;
		$('#flight_no').hide() ;
		$('#pier').show() ;
		$('#sdate_name').text("預定出發時間");
	}
}) ;

// 處理交通工具 分為 飛機 輪船
$("input[name='trans_type_rt']").change( function () {
	var transType = $(this).val() ;
	console.log('trans_type_rt : '+transType) ;
	if ( transType == 1) {	// 飛機
		$('#airport_rt').show() ;
		$('#airline_rt').show() ;
		$('#flight_no_rt').show() ;
		$('#pier_rt').hide() ;
		$('#sdate_name_rt').text("預定起飛時間");
	} else {				// 輪船
		$('#airport_rt').hide() ;
		$('#airline_rt').hide() ;
		$('#flight_no_rt').hide() ;
		$('#pier_rt').show() ;
		$('#sdate_name_rt').text("預定出發時間");
	}
}) ;

function change_checkmark(field){
	var check_ok = $('#check_ok_'+field).prop('checked');
	$('#check_ok_'+field).val('1');
	$('#checkmark_'+field+'_0').css('display','none');
	$('#checkmark_'+field+'_1').css('display','none');
	$('#checkmark_'+field+'_1').css('display','block');
	//if(check_ok){
	//	$('#checkmark_'+field+'_1').css('display','block');
	//}else{
	//	$('#checkmark_'+field+'_0').css('display','block');
	//}
}
</script>

<script>
// add by Angus 2018.08.08
function getCarModels() {
	var retStr = "" ;
	$("input[name='selCarModels[]']").each( function() {
		// console.log( $(this).is(':checked')) ;
		if ( $(this).is(':checked')) {
			retStr += $(this).val() + "|" ;
		}
	});
	// console.log( retStr.length) ;
	if ( retStr.length > 0) {
		retStr = retStr.substring(0, retStr.length-1) ;
	}
	return retStr ;
}

//Jessie 2019/12/02
function getCarTypes() {
	var retStr = "" ;
	$("input[name='selCarTypes[]']").each( function() {
		// console.log( $(this).is(':checked')) ;
		if ( $(this).is(':checked')) {
			retStr += $(this).val() + "|" ;
		}
	});
	// console.log( retStr.length) ;
	if ( retStr.length > 0) {
		retStr = retStr.substring(0, retStr.length-1) ;
	}
	return retStr ;
}

function getuseCars() {
	var retStr = "" ;
	$("input[name='useCars[]']").each( function() {
		// console.log( $(this).is(':checked')) ;
		if ( $(this).is(':checked')) {
			retStr += $(this).val() + "|" ;
		}
	});
	// console.log( retStr.length) ;
	if ( retStr.length > 0) {
		retStr = retStr.substring(0, retStr.length-1) ;
	}
	return retStr ;
}

// add by Angus 2018.08.09
function reLoadCarTypeJs() {
	//console.log( 'in function reLoadCarTypeJs ') ;
	//console.log( '?route=sale/order/forOrderGetCarTypeJs&token=<?php echo $token; ?>');
	$.ajax({
		url : '?route=sale/order/forOrderGetCarTypeJs&token=<?php echo $token; ?>',
		dataType: 'html',
		success: function(html) {
			//console.log(html);
			$('#showJs').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
}
function isTimeSeted() {
	// console.log( $('input[name=\'rent_date_out\']').val());
	// console.log( $('select[name=\'out_time\']').val());
	// console.log( $('input[name=\'rent_date_in_plan\']').val());
	// console.log( $('select[name=\'plan_time\']').val());
	var msg = "" ;
	if ( $('input[name=\'rent_date_out\']').val() == "" || $('select[name=\'out_time\']').val() == "") {
		msg += "預計取車時間 未設定\n" ;
	}
	if ( $('input[name=\'rent_date_in_plan\']').val() == "" || $('select[name=\'plan_time\']').val() == "") {
		msg += "預計還車時間 未設定\n" ;
	}
	return msg ;
}


// add by Angus 2018.08.08
$("input[name='selCarModels[]']").click( function(event) {
	var pObj = $(this).parent() ;
	// 當勾選成立 檢查是否有填日期 沒有填日期就取消勾選
	if ($(this).is(':checked')) {
		if ( msg = isTimeSeted()) {
			alert(msg) ;
			$(this).prop('checked', false) ;
		}
	}

	if ($(this).is(':checked')) {
		pObj.removeClass('bt-background-clean') ;
	} else {
		pObj.addClass('bt-background-clean') ;
	}

	var carModelStr = getCarModels() ;
	//console.log( "選擇車種 : " + carModelStr) ;
	//目前車型
	var CarTypesStr = getCarTypes() ;
	var cartype_arr = CarTypesStr.split('|');
	//目前牌照(要排除目前牌照)
	var useCarsStr = getuseCars() ;
	if ( carModelStr != '') {
		var sDate = $('input[name=\'rent_date_out\']').val() + " " + $('select[name=\'out_time\']').val() ;
		var eDate = $('input[name=\'rent_date_in_plan\']').val() + " " + $('select[name=\'plan_time\']').val() ;
		// var sDate = "2018-08-09 07:30" ;
		// var eDate = "2018-08-11 18:30" ;
		var ajaxUrl = '?route=site/scheduling/getCarModelList&token=<?php echo $token; ?>&car_model=' + carModelStr + '&s=' + sDate + '&e=' + eDate+'&useCars='+useCarsStr;
		//console.log( ajaxUrl);
		$.ajax({
			url : ajaxUrl,
			dataType: 'json',
			success: function(json) {
				var html = "" ;
				for (i = 0; i < json.length; i++) {
					if(cartype_arr.indexOf(json[i]['idx']) !== -1){
						html += "<label name=\"carTypes\" class=\"btn btn-success\"><i class=\"fa fa-car\"></i>&nbsp;"+json[i]['car_seed']+"<input class=\"hidden\" type=\"checkbox\" name=\"selCarTypes[]\" value=\""+json[i]['idx']+"\" checked /></label>&nbsp&nbsp" ;
					}else{
						html += "<label name=\"carTypes\" class=\"btn btn-success bt-background-clean\"><i class=\"fa fa-car\"></i>&nbsp;"+json[i]['car_seed']+"<input class=\"hidden\" type=\"checkbox\" name=\"selCarTypes[]\" value=\""+json[i]['idx']+"\" /></label>&nbsp&nbsp" ;
					}
				}
				if(html == ""){
					$('#showCarType').html('<span style="color:red;display: inline-block;padding-top: 9px;font-weight: bold;">所選日期已無車輛</span>');
				}else{
					$('#showCarType').html(html);
				}
				//$('#showArea').html('') ;
				reLoadCarTypeJs() ;
				//更新訂車數量
				update_carnum();
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
		$.ajax({
			url : '?route=sale/order/calculationDays&token=<?php echo $token; ?>' + '&s=' + sDate + '&e=' + eDate,
			dataType: 'json',
			success: function(json) {
				//console.log( json) ;
				$('#rentDay').html( json[0]) ;
				$('#rentHour').html( json[1]) ;
				$('#rentDay1').html( json[0]) ;
				$('#rentHour1').html( json[1]) ;
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	} else {
		$('#showCarType').html('') ;
		$('#showArea').html('') ;
		//更新訂車數量
		update_carnum();

	}
});

// add by Angus 2018.08.11
function getCarTypes() {
	var retStr = "" ;
	$("input[name='selCarTypes[]']").each( function() {
		// console.log( $(this).is(':checked')) ;
		if ( $(this).is(':checked')) {
			retStr += $(this).val() + "|" ;
		}
	});
	// console.log( retStr.length) ;
	if ( retStr.length > 0) {
		retStr = retStr.substring(0, retStr.length-1) ;
	}
	return retStr ;
}

// add by Angus 2018.08.11
$("input[name='selCarTypes[]']").click( function(event) {
	//目前牌照(要排除目前牌照)
	var useCarsStr = getuseCars() ;

	var sDate = $('input[name=\'rent_date_out\']').val() + " " + $('select[name=\'out_time\']').val() ;
	var eDate = $('input[name=\'rent_date_in_plan\']').val() + " " + $('select[name=\'plan_time\']').val() ;
	var order_id = $('input[name=\'order_id\']').val() ;
	//var sDate    = "2018-08-09 07:30" ;
	//var eDate    = "2018-08-11 18:30" ;
	var pObj = $(this).parent() ;
	var clean_sn = "";
	if ($(this).is(':checked')) {
		clean_sn = $(this).val();
		pObj.removeClass('bt-background-clean') ;
	} else {
		pObj.addClass('bt-background-clean') ;
	}
	var carTypeStr = getCarTypes() ;
	//console.log( carTypeStr) ;
	// var ajaxUrl = "?route=sale/order/autocompleteForCarNo&token={$this->session->data['token']}&car_seed="+carTypeStr+"&s="+sDate+"&e="+eDate+"&idx="+order_id ;
	var ajaxUrl = "?route=sale/order/autocompleteForCarNo&token=<?php echo $token; ?>&car_seed="+carTypeStr+"&s="+sDate+"&e="+eDate+"&useCars="+useCarsStr+"&idx="+order_id+"&clean_sn="+clean_sn ;
	//console.log( ajaxUrl) ;
	if ( carTypeStr != '') {
		$.ajax({
			url: ajaxUrl,
			dataType: 'html',
			success: function(html) {
				//console.log( html);
				$('#showArea').html( html) ;
				//更新訂車數量
				update_carnum();

				//再宣告一次？
			},
			error: function(xhr, ajaxOptions, thrownError) {
			//console.log( thrownError);
			//console.log( ajaxOptions);
			//console.log( xhr);
		}
		});
	} else {
		$('#showArea').html( '') ;
		//更新訂車數量
		update_carnum();

	}
}) ;
$("input[name='rent_date_out'],input[name='rent_date_in_plan'],input[name='sel_station_start'],[name='sel_station_end'],[name='out_time'],[name='plan_time']").click( function () {
	change_day();
})
function change_day(){
	var d1 = $("input[name='rent_date_out']").val() ;
	var d2 = $("input[name='rent_date_in_plan']").val() ;
	var t1 = $('select[name=\'out_time\'] option:selected').val() ;
	var t2 = $('select[name=\'plan_time\'] option:selected').val() ;

	var sDate = d1+'T'+t1;
	var eDate = d2+'T'+t2;
	var strMsg = '';

	var oneDayHour = checkOneDay ( sDate, eDate) ;
	var oneDay = 0;
	var oneHour = 0;
	var allDay = 0;
	if(oneDayHour > 0){
		oneDay = oneDayHour/ 24;
		oneDay = parseInt(oneDay);
		Math.floor(oneDay);
		oneHour = oneDayHour%24;
	}
	parseInt(oneHour);
	oneHour = Math.round(oneHour);

	//未滿1天要算1天
	if(parseInt(oneDayHour) < 24){
		console.log(oneDayHour);
		oneDay = 1;
		oneHour = 0;
	}
	$("#rentDay").html(oneDay);
	$("#rentHour").html(oneHour);
	$("#rentDay1").html(oneDay);
	$("#rentHour1").html(oneHour);

	//檢查車種是否已選 已選要更新一下機型 2019/12/24
	if(oneDay>0 || oneHour>0){
		check_model_car();
	}
	change_price();
}
function check_model_car(){
	//目前車型
	var CarTypesStr = getCarTypes() ;
	var cartype_arr = CarTypesStr.split('|');

	var carModelStr = getCarModels() ;
	//console.log( "選擇車種 : " + carModelStr) ;
	var useCarsStr = getuseCars() ;
	if ( carModelStr != '') {
		var sDate = $('input[name=\'rent_date_out\']').val() + " " + $('select[name=\'out_time\']').val() ;
		var eDate = $('input[name=\'rent_date_in_plan\']').val() + " " + $('select[name=\'plan_time\']').val() ;
		// var sDate = "2018-08-09 07:30" ;
		// var eDate = "2018-08-11 18:30" ;
		var ajaxUrl = '?route=site/scheduling/getCarModelList&token=<?php echo $token; ?>&car_model=' + carModelStr + '&s=' + sDate + '&e=' + eDate+'&useCars='+useCarsStr;
		//console.log( ajaxUrl);
		$.ajax({
			url : ajaxUrl,
			dataType: 'json',
			success: function(json) {
				var html = "" ;
				for (i = 0; i < json.length; i++) {
					if(cartype_arr.indexOf(json[i]['idx']) !== -1){
						html += "<label name=\"carTypes\" class=\"btn btn-success\"><i class=\"fa fa-car\"></i>&nbsp;"+json[i]['car_seed']+"<input class=\"hidden\" type=\"checkbox\" name=\"selCarTypes[]\" value=\""+json[i]['idx']+"\" checked /></label>&nbsp&nbsp" ;
					}else{
						html += "<label name=\"carTypes\" class=\"btn btn-success bt-background-clean\"><i class=\"fa fa-car\"></i>&nbsp;"+json[i]['car_seed']+"<input class=\"hidden\" type=\"checkbox\" name=\"selCarTypes[]\" value=\""+json[i]['idx']+"\" /></label>&nbsp&nbsp" ;
					}
				}
				if(html == ""){
					$('#showCarType').html('<span style="color:red;display: inline-block;padding-top: 9px;font-weight: bold;">所選日期已無車輛</span>');
				}else{
					$('#showCarType').html(html);
				}
				//$('#showArea').html('') ;
				reLoadCarTypeJs() ;
				//更新訂車數量
				update_carnum();
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
		$.ajax({
			url : '?route=sale/order/calculationDays&token=<?php echo $token; ?>' + '&s=' + sDate + '&e=' + eDate,
			dataType: 'json',
			success: function(json) {
				//console.log( json) ;
				$('#rentDay').html( json[0]) ;
				$('#rentHour').html( json[1]) ;
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	} else {
		$('#showCarType').html('') ;
		$('#showArea').html('') ;
		//更新訂車數量
		update_carnum();
	}
}
//檢查選的天數幾天幾小時
function checkOneDay( sDate, eDate) {
	var currDate = "" ;
	var txtDate = "" ;
	if ( eDate == undefined) {
		currDate = Date.parse((new Date()).toDateString()+' '+(new Date()).toTimeString());
		txtDate = Date.parse(sDate);
	} else {
		currDate = Date.parse(sDate);
		txtDate = Date.parse(eDate);
	}
	var ONE_HOUR = 1000 * 60 * 60 ;
	return  (txtDate - currDate) / ONE_HOUR ;
}

$("label[name='useCar']").click( function () {
	var agreement_no = $('input[name="agreement_no"]').val();
	var rentDay =$('#rentDay').html() ;
	var rentHour =$('#rentHour').html() ;
	rentDay = parseFloat(rentDay);
	rentHour = parseFloat(rentHour);
	if(rentHour>0){
		rentDay++;
	}

	// var classStr = $(this).attr('class') ;
	// console.log( classStr);
	var ckbObj = $(this).find("input[type='checkbox']")
	//console.log( ckbObj.is(':checked')) ;
	if ( ckbObj.is(':checked')) {
		$(this).removeClass('bt-background-clean') ;
	} else {
		$(this).addClass('bt-background-clean') ;
	}
	//車損金額 未上線
	var business_lose = $('input[name="business_lose"]').val();
	var car_lose = $('input[name="car_lose"]').val();

	var order_type=$('select[name="order_type"]').val();
	//改全部訂單都要即時計算金額 2019/09/25
	//if(order_type == '140'){
		var c_num = 0;
		var c_price = 0;
		$('input[name^="useCars"]').each(function() {
			if($(this).parent().attr("class").indexOf("clean")===-1){
				c_price += parseInt($(this).attr("p2"));
				c_num ++;
			}
		});

		c_price = parseInt(c_price);
		//mobile_moto 50元
		//c_price += parseInt($("#mobile_moto").val())*50;
		//baby_chair1/baby_chair2/children_chair 100元
		c_price += parseInt($("#baby_chair1").val())*100;
		c_price += parseInt($("#baby_chair2").val())*100;
		c_price += parseInt($("#children_chair").val())*100;
		//console.log(c_num+" "+c_price);
		if(business_lose != "" && business_lose != undefined){
			c_price += parseInt(business_lose);
		}
		if(car_lose != "" && business_lose != undefined){
			c_price += parseInt(car_lose);
		}

		console.log(rentDay);
		c_price = c_price*rentDay;

		//car_safety 汽車保險費用、moto_safety機車保險費用
		if(parseInt($('input[name="car_safety"]').val())>0){
			c_price += parseInt($('input[name="car_safety"]').val());
		}
		if(parseInt($('input[name="moto_safety"]').val())>0){
			c_price += parseInt($('input[name="moto_safety"]').val());
		}

		//檢查SPDC有沒有值
		var spdc = $('input[name="spdc"]').val();
		parseInt(spdc);
		if(spdc == ""){
			spdc = 0;
		}
		c_price -= spdc;
		$("#t_money").html(c_price);
		$("#tt_money").val(c_price);
		$("#new_total").val(c_price);
		$("#t_pickdiff").html(c_price - parseInt($("#now_money").html()));
		$("#pickdiff").val(c_price - parseInt($("#now_money").html()));
		$("#pickdiff_1").val(c_price - parseInt($("#now_money").html()));

		if(agreement_no == ''){
			//計算牌照數量
			//$("#total_car").val(c_num);
		}
		change_price();

	//}
}) ;

function update_carnum(){
	var agreement_no = $('input[name="agreement_no"]').val();
	var c_num = 0;
	$('input[name^="useCars"]').each(function() {
		if($(this).parent().attr("class").indexOf("clean")===-1){
			c_num ++;
		}
	});
	console.log('c_num='+c_num);

	if(agreement_no == ''){
		//計算牌照數量
		//$("#total_car").val(c_num);
	}
}

//配件更新要更新價格
$("input[name='mobile_moto'],input[name='baby_chair1'],input[name='baby_chair2'],input[name='children_chair'],input[name='gps']").change( function () {
	//change_price();
}) ;
$("input[name='business_lose'],input[name='car_lose']").blur( function () {
	//change_price();
}) ;
$("#area_memo1").blur( function () {
	$("#area_memo").val($("#area_memo1").val());
}) ;
$("#area_memo").blur( function () {
	$("#area_memo1").val($("#area_memo").val());
}) ;

$("input[name='adult'],input[name='children'],input[name='baby'],input[name='baby_chair1'],input[name='baby_chair2'],input[name='children_chair'],input[name='mobile_moto'],input[name='umbrella'],input[name='driving_recorder'],input[name='mobile_car'],input[name='helmet'],input[name='raincoat'],input[name='car_safety'],input[name='moto_safety'],input[name='gps'],input[name='business_lose'],input[name='car_lose']").blur( function (value) {
	if(this.value==""){
		this.value= 0;
	}
	this.value=this.value.replace(/\D/g,"0");
	change_price();

}) ;


function change_price(){
	var agreement_no = $('input[name="agreement_no"]').val();
	var rentDay     = parseFloat( $('#rentDay').html()) ;
	var rentDay4Acc = parseFloat( $('#rentDay').html()) ;
	var rentHour    = parseFloat( $('#rentHour').html()) ;

	if(rentHour>6){
		rentDay++;
	}
	var plug_total = 0;
	parseFloat(plug_total);

	var business_lose = $('input[name="business_lose"]').val();
	var car_lose = $('input[name="car_lose"]').val();
	var order_type=$('select[name="order_type"]').val();
	//if(order_type == '140'){
		var c_num = 0;
		var c_price = 0;
		$('input[name^="useCars"]').each(function() {
			if($(this).parent().attr("class").indexOf("clean")===-1){
				c_price += parseInt($(this).attr("p2"));
				c_num ++;
			}
		});
		c_price = parseInt(c_price);
		//mobile_moto 50元
		//c_price += parseInt($("#mobile_moto").val())*50;
		//baby_chair1/baby_chair2/children_chair 100元
		// c_price += parseInt($("#baby_chair1").val())*100;
		// c_price += parseInt($("#baby_chair2").val())*100;
		// c_price += parseInt($("#children_chair").val())*100;
		// c_price += parseInt($("#gps").val())*100;
		// c_price += parseInt($("#mobile_moto").val())*50;

		//plug_total += parseInt($("#mobile_moto").val())*50;
		plug_total += parseInt($("#baby_chair1").val())*100;
		plug_total += parseInt($("#baby_chair2").val())*100;
		plug_total += parseInt($("#children_chair").val())*100;
		// plug_total += parseInt($("#gps").val())*100;
		plug_total += parseInt($("#mobile_moto").val())*50;

		var add_price = 0;
		var add_price2 = 0;
		parseInt(add_price);
		if(rentHour >=1 && rentHour <=6){
			rentHour =  Math.ceil(rentHour);
			add_price =  Math.ceil(c_price * 0.1) * rentHour ;
			add_price2 =  Math.ceil(plug_total * 0.1) * rentHour ;
		}
		c_price = rentDay * c_price + add_price; //依天數要倍數計算
		plug_total = ( rentHour > 0) ? plug_total * ( rentDay4Acc + 1) : plug_total * rentDay4Acc ;

		c_price += plug_total ;
		if(business_lose != undefined){
			c_price += parseInt(business_lose);
		}
		if(car_lose != undefined){
			c_price += parseInt(car_lose);
		}

		// if(rentDay > 1){
		// 	c_price += parseInt($("#mobile_moto").val())*100;
		// 	c_price += parseInt($("#mobile_moto").val())*50*(rentDay-1);

		// 	plug_total += parseInt($("#mobile_moto").val())*100;
		// 	plug_total += parseInt($("#mobile_moto").val())*50*(rentDay-1);
		// }else{
		// 	c_price += parseInt($("#mobile_moto").val())*100;
		// 	plug_total += parseInt($("#mobile_moto").val())*100;
		// }

		//car_safety 汽車保險費用、moto_safety機車保險費用
		if(parseInt($('input[name="car_safety"]').val())>0){
			c_price += parseInt($('input[name="car_safety"]').val());
		}
		if(parseInt($('input[name="moto_safety"]').val())>0){
			c_price += parseInt($('input[name="moto_safety"]').val());
		}

		//檢查SPDC有沒有值
		var spdc = $('input[name="spdc"]').val();
		parseInt(spdc);
		if(spdc == ""){
			spdc = 0;
		}
		c_price -= spdc;
		$("#t_money").html(c_price);
		$("#tt_money").val(c_price);
		$("#new_total").val(c_price);
		$("#t_pickdiff").html(c_price - parseInt($("#now_money").html()));
		$("#pickdiff").val(c_price - parseInt($("#now_money").html()));
		$("#pickdiff_1").val(c_price - parseInt($("#now_money").html()));
		$("#plug_total").val(plug_total);
		if(agreement_no == ''){
			//計算牌照數量
			//$("#total_car").val(c_num);
		}
	//}
	change_total_money();
}

$(document).ready(function() {
	//change_total_money();
	change_price();
})

function change_total_money(){
	$('#h_remark').html("") ;
	var d_price = 0;
	var h_price = 0;
	var ad_price = 0;
	var ah_price = 0;
	var all_price = 0;

	parseInt(d_price);
	parseInt(h_price);
	parseInt(ad_price);
	parseInt(ah_price);
	parseInt(all_price);

	var rentDay =$('#rentDay').html() ;
	var rentHour =$('#rentHour').html() ;
	rentDay = parseFloat(rentDay);
	rentHour = parseFloat(rentHour);

	$('input[name^="useCars"]').each(function() {
		if($(this).parent().attr("class").indexOf("clean")===-1){
			d_price += parseInt($(this).attr("p2"));
		}
	});
	h_price = d_price/10;

	if(rentDay>0){
		ad_price = d_price*rentDay;
	}
	if(rentHour>6){
		ah_price = d_price*1;
		$('#h_remark').html("(己超出6小時進位)") ;

	}else{
		ah_price = h_price*rentHour;
	}
	all_price = ad_price + ah_price;


	$("#d_price").html(d_price) ;
	$("#h_price").html(h_price) ;
	$("#ad_price").html(ad_price) ;
	$("#ah_price").html(ah_price) ;
	$("#all_price").html(all_price) ;

	$("#car_ps").val(all_price);

}

function ins_reamrk(field){
	var user_allname = '<?=$nowname?>';

	var remark = $("#"+field).val();
	var area_memo = $("#area_memo").val();
	if(remark != ""){
		remark = remark+' '+user_allname;
		if(area_memo == "輸入你想要寫的內容..."){
			$("#area_memo").val(remark);
			$("#area_memo1").val(remark);
			$("#area_memo2").val(remark);
			$("#area_memo3").val(remark);
		}else{
			$("#area_memo").val(area_memo+"\r\n"+remark);
			$("#area_memo1").val(area_memo+"\r\n"+remark);
			$("#area_memo2").val(area_memo+"\r\n"+remark);
			$("#area_memo3").val(area_memo+"\r\n"+remark);
		}
	}
}
</script>

<style type="text/css">
	.bt-background-clean {
		color: #000000 ;
		background-color: #FFFFFF ;
	}
</style>


<?php echo $footer; ?>