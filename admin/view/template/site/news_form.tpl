<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-action" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $url_cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
	<!--===================================== 訊息提示 Start =======================================-->
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <!--===================================== 訊息提示 End =========================================-->
    <div class="panel panel-default">
    	<!--================================= 標題 Start ===========================================-->
		<div class="panel-heading">
		<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
		</div>
		<!--================================= 標題 End =============================================-->
        <div class="panel-body">
            <form action="<?php echo $url_action; ?>" method="post" enctype="multipart/form-data" id="form-action" class="form-horizontal">
                <ul class="nav nav-tabs"><!-- 頁籤 -->
                    <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                </ul>
            <div class="tab-content"><!-- 內容 -->
                <div class="tab-pane active" id="tab-general">
                    <div class="tab-content">
<!-- 標題 -->
<div class="form-group required">
	<label class="col-sm-2 control-label" for="input-title"><?php echo $entry_title; ?></label>
	<div class="col-sm-10">
		<input type="text" name="input_title" placeholder="<?php echo $entry_title; ?>"
			value="<?php echo isset( $input_title) ? $input_title : ''; ?>"
			id="input-title" class="form-control" />
		<?php if ($error_title != '') { ?>
		<div class="text-danger"><?php echo $error_title; ?></div>
		<?php } ?>
	</div>
</div>
<!-- 頁面內容 -->
<div class="form-group required">
	<label class="col-sm-2 control-label" for="input-description"><?php echo $entry_description; ?></label>
	<div class="col-sm-10">
		<textarea name="area_description" placeholder="<?php echo $entry_description; ?>"
			id="input-description" class="form-control summernote">
			<?php echo isset( $area_description) ? $area_description : ''; ?>
		</textarea>
		<?php if ($error_description != '') { ?>
		<div class="text-danger"><?php echo $error_description; ?></div>
		<?php } ?>
	</div>
</div>
<!-- 上線日期 -->
<div class="form-group required">
	<label class="col-sm-2 control-label" for="input-date-added"><?php echo $entry_online_date; ?></label>
	<div class="col-sm-3">
		<div class="input-group date">
		<input type="text" name="input_online_date"
			value="<?php echo $input_online_date; ?>"
			placeholder="<?php echo $entry_online_date; ?>"
			data-date-format="YYYY-MM-DD"
			id="input-online-date" class="form-control" />
		<span class="input-group-btn">
			<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
		</span>
		</div>
		<?php if ($error_online_date != '') { ?>
		<div class="text-danger"><?php echo $error_online_date; ?></div>
		<?php } ?>
	</div>
</div>
<!-- 頁面狀態 -->
<div class="form-group">
	<label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
	<div class="col-sm-10">
		<select name="sel_status" id="input-status" class="form-control">
			<?php if ($sel_status) { ?>
			<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
			<option value="0"><?php echo $text_disabled; ?></option>
			<?php } else { ?>
			<option value="1"><?php echo $text_enabled; ?></option>
			<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
			<?php } ?>
		</select>
	</div>
</div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script></div>
<?php echo $footer; ?>