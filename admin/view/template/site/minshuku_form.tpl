<?php echo $header; ?>
<style type="text/css">
.div_copy .control-label{
	padding-top: 0px;
}
</style>
<?php echo $column_left; ?>

<div id="content">
<div class="page-header">
<div class="container-fluid">
	<div class="pull-right">
		<button type="submit" form="form-action" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
		<a href="<?php echo $url_cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
		<h1><?php echo $heading_title; ?></h1>
		<ul class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
			<?php } ?>
		</ul>
	</div>
</div>
<div class="container-fluid">
<!--===================================== 訊息提示 Start =======================================-->
<?php if ($error_warning) { ?>
<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
	<button type="button" class="close" data-dismiss="alert">&times;</button>
</div>
<?php } ?>
<!--===================================== 訊息提示 End =========================================-->
<div class="panel panel-default">
<!--================================= 標題 Start ===========================================-->
<div class="panel-heading">
	<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
</div>
<!--================================= 標題 End =============================================-->
<div class="panel-body">
	<form action="<?php echo $url_action; ?>" method="post" enctype="multipart/form-data" id="form-action" class="form-horizontal">
		<ul class="nav nav-tabs"><!-- 頁籤 -->
			<li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
			<li><a href="#tab-image" data-toggle="tab"><?php echo $tab_image; ?></a></li>
			<li><a href="#tab-room" data-toggle="tab"><?php echo $tab_room; ?></a></li>
			<li><a href="#tab-other" data-toggle="tab"><?php echo $tab_other; ?></a></li>
		</ul>
		<div class="tab-content"><!-- 內容 -->
			<div class="tab-pane active" id="tab-general">
				<div class="tab-content">
					<!-- 民宿名稱 -->
					<div class="form-group required">
						<label class="col-sm-2 control-label" for="input-name">民宿名稱</label>
						<div class="col-sm-10">
							<input type="text" name="input_name" placeholder="民宿名稱"
							value="<?php echo isset( $input_name) ? $input_name : ''; ?>"
							id="input-name" class="form-control" />
							<?php if ($error_name != '') { ?>
							<div class="text-danger"><?php echo $error_name; ?></div>
							<?php } ?>
						</div>
					</div>
					<!-- 星等 -->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-start">星等</label>
						<div class="col-sm-10">
							<input type="text" name="input_start" placeholder="星等"
							value="<?php echo isset( $input_start) ? $input_start : ''; ?>"
							id="input-start" class="form-control" />
						</div>
					</div>
					<!-- 縣別 -->
					<div class="form-group required">
						<label class="col-sm-2 control-label" for="input-pos">縣別</label>
						<div class="col-sm-10">
							<input type="text" name="input_pos" placeholder="縣別"
							value="<?php echo isset( $input_pos) ? $input_pos : ''; ?>"
							id="input-pos" class="form-control" />
							<?php if ($error_pos != '') { ?>
							<div class="text-danger"><?php echo $error_pos; ?></div>
							<?php } ?>
						</div>
					</div>
					<!-- 地區 -->
					<div class="form-group required">
						<label class="col-sm-2 control-label" for="input-pos1">地區</label>
						<div class="col-sm-10">
							<input type="text" name="input_pos1" placeholder="地區"
							value="<?php echo isset( $input_pos1) ? $input_pos1 : ''; ?>"
							id="input-pos1" class="form-control" />
							<?php if ($error_pos1 != '') { ?>
							<div class="text-danger"><?php echo $error_pos1; ?></div>
							<?php } ?>
						</div>
					</div>
					<!-- 房型 -->
					<div class="form-group required">
						<label class="col-sm-2 control-label" for="input-room-type">房型</label>
						<div class="col-sm-10">
							<input type="text" name="input_room_type" placeholder="房型"
							value="<?php echo isset( $input_room_type) ? $input_room_type : ''; ?>"
							id="input-room-type" class="form-control" />
							<?php if ($error_room_type != '') { ?>
							<div class="text-danger"><?php echo $error_room_type; ?></div>
							<?php } ?>
						</div>
					</div>
					<!-- 房間數 -->
					<div class="form-group required">
						<label class="col-sm-2 control-label" for="input-room-cnt">房間數</label>
						<div class="col-sm-10">
							<input type="text" name="input_room_cnt" placeholder="房間數"
							value="<?php echo isset( $input_room_cnt) ? $input_room_cnt : ''; ?>"
							id="input-room-cnt" class="form-control" />
							<?php if ($error_room_cnt != '') { ?>
							<div class="text-danger"><?php echo $error_room_cnt; ?></div>
							<?php } ?>
						</div>
					</div>
					<!-- 價位 -->
					<div class="form-group required">
						<label class="col-sm-2 control-label" for="input-price">價位</label>
						<div class="col-sm-10">
							<input type="text" name="input_price" placeholder="價位"
							value="<?php echo isset( $input_price) ? $input_price : ''; ?>"
							id="input-price" class="form-control" />
							<?php if ($error_price != '') { ?>
							<div class="text-danger"><?php echo $error_price; ?></div>
							<?php } ?>
						</div>
					</div>
					<!-- 電話 -->
					<div class="form-group required">
						<label class="col-sm-2 control-label" for="input-tel">電話</label>
						<div class="col-sm-10">
							<input type="text" name="input_tel" placeholder="電話"
							value="<?php echo isset( $input_tel) ? $input_tel : ''; ?>"
							id="input-tel" class="form-control" />
							<?php if ($error_tel != '') { ?>
							<div class="text-danger"><?php echo $error_tel; ?></div>
							<?php } ?>
						</div>
					</div>
					<!-- 傳真 -->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-fax">傳真</label>
						<div class="col-sm-10">
							<input type="text" name="input_fax" placeholder="傳真"
							value="<?php echo isset( $input_fax) ? $input_fax : ''; ?>"
							id="input-fax" class="form-control" />
						</div>
					</div>
					<!-- email -->
					<div class="form-group required">
						<label class="col-sm-2 control-label" for="input-email">電子郵件</label>
						<div class="col-sm-10">
							<input type="text" name="input_email" placeholder="電子郵件"
							value="<?php echo isset( $input_email) ? $input_email : ''; ?>"
							id="input-email" class="form-control" />
							<?php if ($error_email != '') { ?>
							<div class="text-danger"><?php echo $error_email; ?></div>
							<?php } ?>
						</div>
					</div>
					<!-- 網址 -->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-url">網址</label>
						<div class="col-sm-10">
							<input type="text" name="input_url" placeholder="網址"
							value="<?php echo isset( $input_url) ? $input_url : ''; ?>"
							id="input-url" class="form-control" />
						</div>
					</div>
					<!-- google_map -->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-google-map">地圖位置</label>
						<div class="col-sm-10">
							<input type="text" name="input_google_map" placeholder="地圖位置"
							value="<?php echo isset( $input_google_map) ? $input_google_map : ''; ?>"
							id="input-google-map" class="form-control" />
						</div>
					</div>
					<!-- 郵遞區號 -->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-post-id">郵遞區號</label>
						<div class="col-sm-10">
							<input type="text" name="input_post_id" placeholder="郵遞區號"
							value="<?php echo isset( $input_post_id) ? $input_post_id : ''; ?>"
							id="input-post-id" class="form-control" />
						</div>
					</div>
					<!-- 地址 -->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-address">地址</label>
						<div class="col-sm-10">
							<input type="text" name="input_address" placeholder="地址"
							value="<?php echo isset( $input_address) ? $input_address : ''; ?>"
							id="input-address" class="form-control" />
						</div>
					</div>
					<!-- 金豐評等 -->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-km-type">金豐評等</label>
						<div class="col-sm-10">
							<input type="text" name="input_km_type" placeholder="金豐評等"
							value="<?php echo isset( $input_km_type) ? $input_km_type : ''; ?>"
							id="input-fm-type" class="form-control" />
						</div>
					</div>
					<!-- 立案字號 -->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-reg-no">立案字號</label>
						<div class="col-sm-10">
							<input type="text" name="input_reg_no" placeholder="立案字號"
							value="<?php echo isset( $input_reg_no) ? $input_reg_no : ''; ?>"
							id="input-reg-no" class="form-control" />
						</div>
					</div>
					<!-- 立案日期 -->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-approved-date">立案日期</label>
						<div class="col-sm-10">
							<input type="text" name="input_approved_date" placeholder="立案日期"
							value="<?php echo isset( $input_approved_date) ? $input_approved_date : ''; ?>"
							id="input-approved-date" class="form-control" />
						</div>
					</div>
					<!-- 上線日期 -->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-date-added"><?php echo $entry_online_date; ?></label>
						<div class="col-sm-3">
							<div class="input-group date">
								<input type="text" name="input_online_date"
								value="<?php echo $input_online_date; ?>"
								placeholder="<?php echo $entry_online_date; ?>"
								data-date-format="YYYY-MM-DD"
								id="input-online-date" class="form-control" />
								<span class="input-group-btn">
									<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
								</span>
							</div>
						</div>
					</div>
					<!-- 頁面狀態 -->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
						<div class="col-sm-10">
							<select name="sel_status" id="input-status" class="form-control">
								<?php if ($sel_status) { ?>
								<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
								<option value="0"><?php echo $text_disabled; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_enabled; ?></option>
								<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<!-- 服務項目 -->
					<div class="form-group">
					<?php
						$service = explode(";", $input_service);
						$children="";
						$pet="";
						$no_smoking="";
						$eng="";
						$breakfast="";
						$wifi="";
						$parking="";
						$washing="";
						$bike="";
						foreach ($service as $key => $value) {
							$$value='checked';
						}
					?>
						<label class="col-sm-2 control-label">服務項目</label>
						<div class="col-sm-10" style="padding-top: 10px">
							<input type="checkbox" name="service[]" value="children" <?=$children?>>歡迎孩子&nbsp;&nbsp;
							<input type="checkbox" name="service[]" value="pet" <?=$pet?>>歡迎寵物&nbsp;&nbsp;
							<input type="checkbox" name="service[]" value="no_smoking" <?=$no_smoking?>>禁止吸菸&nbsp;&nbsp;
							<input type="checkbox" name="service[]" value="eng" <?=$eng?>>英語服務&nbsp;&nbsp;
							<input type="checkbox" name="service[]" value="breakfast" <?=$breakfast?>>提供早餐&nbsp;&nbsp;
							<input type="checkbox" name="service[]" value="wifi" <?=$wifi?>>提供WIFI&nbsp;&nbsp;
							<input type="checkbox" name="service[]" value="parking" <?=$parking?>>提供停車&nbsp;&nbsp;
							<input type="checkbox" name="service[]" value="washing" <?=$washing?>>洗衣機&nbsp;&nbsp;
							<input type="checkbox" name="service[]" value="bike" <?=$bike?>>單車
						</div>
					</div>
				</div>
			</div>
			<!-- 圖片設定 -->
			<div class="tab-pane" id="tab-image">
				<div class="tab-content">
					<!-- 圖片上傳 -->
					<div class="form-group">
						<div class="col-md-3 col-sm-6 col-xs-6" >
							<label class="col-sm-5 control-label">圖片1：</label>
							<div class="col-sm-7"><a href="" id="thumb-imageIntro1" data-toggle="image" class="img-thumbnail">
								<img src="<?php echo $thumb1; ?>" alt="" title="" data-placeholder="" /></a>
								<input type="hidden" name="imageIntro1" value="<?php echo $imageIntro1; ?>" id="input-imageIntro1" />
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6">
							<label class="col-sm-5 control-label">圖片2：</label>
							<div class="col-sm-7"><a href="" id="thumb-imageIntro2" data-toggle="image" class="img-thumbnail">
								<img src="<?php echo $thumb2; ?>" alt="" title="" data-placeholder="" /></a>
								<input type="hidden" name="imageIntro2" value="<?php echo $imageIntro2; ?>" id="input-imageIntro2" />
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6">
							<label class="col-sm-5 control-label">圖片3：</label>
							<div class="col-sm-7"><a href="" id="thumb-imageIntro3" data-toggle="image" class="img-thumbnail">
								<img src="<?php echo $thumb3; ?>" alt="" title="" data-placeholder="" /></a>
								<input type="hidden" name="imageIntro3" value="<?php echo $imageIntro3; ?>" id="input-imageIntro3" />
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6">
							<label class="col-sm-5 control-label">圖片4：</label>
							<div class="col-sm-7"><a href="" id="thumb-imageIntro4" data-toggle="image" class="img-thumbnail">
								<img src="<?php echo $thumb4; ?>" alt="" title="" data-placeholder="" /></a>
								<input type="hidden" name="imageIntro4" value="<?php echo $imageIntro4; ?>" id="input-imageIntro4" />
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6">
							<label class="col-sm-5 control-label">圖片5：</label>
							<div class="col-sm-7"><a href="" id="thumb-imageIntro5" data-toggle="image" class="img-thumbnail">
								<img src="<?php echo $thumb5; ?>" alt="" title="" data-placeholder="" /></a>
								<input type="hidden" name="imageIntro5" value="<?php echo $imageIntro5; ?>" id="input-imageIntro5" />
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6">
							<label class="col-sm-5 control-label">圖片6：</label>
							<div class="col-sm-7"><a href="" id="thumb-imageIntro6" data-toggle="image" class="img-thumbnail">
								<img src="<?php echo $thumb6; ?>" alt="" title="" data-placeholder="" /></a>
								<input type="hidden" name="imageIntro6" value="<?php echo $imageIntro6; ?>" id="input-imageIntro6" />
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6">
							<label class="col-sm-5 control-label">圖片7：</label>
							<div class="col-sm-7"><a href="" id="thumb-imageIntro7" data-toggle="image" class="img-thumbnail">
								<img src="<?php echo $thumb7; ?>" alt="" title="" data-placeholder="" /></a>
								<input type="hidden" name="imageIntro7" value="<?php echo $imageIntro7; ?>" id="input-imageIntro7" />
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-6">
							<label class="col-sm-5 control-label">圖片8：</label>
							<div class="col-sm-7"><a href="" id="thumb-imageIntro8" data-toggle="image" class="img-thumbnail">
								<img src="<?php echo $thumb8; ?>" alt="" title="" data-placeholder="" /></a>
								<input type="hidden" name="imageIntro8" value="<?php echo $imageIntro8; ?>" id="input-imageIntro8" />
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- 房型 -->
			<div class="tab-pane" id="tab-room">
				<div class="tab-content" id="div_room">
					<div class="col-sm-12" style="margin-bottom: 15px">
						<button type="button" id="btn_plus" class="btn btn-success">＋</button>
					</div>
					<!-- 房型資料 -->
				<?php
					foreach ($room_data as $key => $room_row) {
				?>
					<div class="form-group div_copy" hidden_sn="<?=$key?>">
						<div class="col-sm-8">
							<label class="col-sm-2 control-label">
								<button type="button" class="btn btn-danger btn_del" style="float: left;">－</button>
								房型名稱
							</label>
							<div class="col-sm-4">
								<input type="text" placeholder="房型名稱" name="room[name][]" class="form-control" value="<?=$room_row['name']?>" />
							</div>

							<label class="col-sm-2 control-label">每晚最低價</label>
							<div class="col-sm-4">
								<input type="text" name="room[base_price][]" placeholder="每晚最低價" value="<?=$room_row['base_price']?>" class="form-control" />
							</div>

							<label class="col-sm-2 control-label" style="padding-top: 20px">人數</label>
							<div class="col-sm-2" style="padding-top: 10px">
								<input type="text" name="room[per_cnt][]" placeholder="人數" value="<?=$room_row['per_cnt']?>" class="form-control"  value=""/>
							</div>

							<label class="col-sm-1 control-label" style="padding-top: 10px">可加</br>人數</label>
							<div class="col-sm-1" style="padding-top: 18px">
								<select name="room[extra_per_cnt][]">
								<?php
								for ($i=0; $i <11 ; $i++) { 
									$selected = "";
									if ($room_row['extra_per_cnt'] == $i) {
										$selected = "selected=true";
									}
								?>
									<option value="<?=$i?>" <?=$selected?> ><?=$i?>人</option>
								<?php }?>
								</select>
							</div>

							<label class="col-sm-2 control-label" style="padding-top: 20px">設備</label>
							<div class="col-sm-3" style="padding-top: 18px">
								<?php
									$device = explode(";", $room_row['device']);
									$wifi="";
									$bath="";
									foreach ($device as $a => $value) {
										$$value='checked';
									}
								?>
								<input type="checkbox" class="checkbox_bath" name="room[bath][<?=$key?>]" value="1" <?=$bath?> >獨立衛浴&nbsp;&nbsp;
								<input type="checkbox" class="checkbox_wifi" name="room[wifi][<?=$key?>]" value="1" <?=$wifi?> >免費WIFI
							</div>
						</div>
						<div class="col-sm-2">
							<a id="room_img_<?=$key?>" href="" data-toggle="image" class="img-thumbnail">
							<img class="room_img" src="<?=$room_row['thumb']?>" alt="" title="" data-placeholder="" /></a>
							<input type="hidden" name="room[image][]" class="input_room_image" value="<?=$room_row['image']?>" id="input_room_image_<?=$key?>" />
						</div>
					</div>
				<?php
					}
				?>
				<input id="hidden_sn" type="hidden" name="hidden_sn" value="<?=$key?>">
				</div>
			</div>
			<!-- 其他 -->
			<div class="tab-pane" id="tab-other">
				<div class="tab-content">
					<!-- 旅宿設備 -->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="device_text">旅宿設備</label>
						<div class="col-sm-10">
							<textarea name="input_device_text" id="device_text"  class="form-control" placeholder="旅宿設備..." style="height: 140px"><?php echo isset( $input_device_text) ? $input_device_text : ''; ?></textarea>
						</div>
					</div>
					<!-- 住房規則 -->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="rule_text">入住時間與住房規則</label>
						<div class="col-sm-10">
							<textarea name="input_rule_text" id="rule_text"  class="form-control" placeholder="住房規則..." style="height: 140px"><?php echo isset( $input_rule_text) ? $input_rule_text : ''; ?></textarea>
						</div>
					</div>
					<!--旅宿服務 -->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="service_text">旅宿服務</label>
						<div class="col-sm-10">
							<textarea name="input_service_text" id="service_text"  class="form-control" placeholder="旅宿服務..." style="height: 140px"><?php echo isset( $input_service_text) ? $input_service_text : ''; ?></textarea>
						</div>
					</div>
					<!--退訂政策 -->
					<div class="form-group">
						<label class="col-sm-2 control-label" for="cancel_text">退訂政策</label>
						<div class="col-sm-10">
							<textarea name="input_cancel_text" id="cancel_text"  class="form-control" placeholder="退訂政策..." style="height: 140px"><?php echo isset( $input_cancel_text) ? $input_cancel_text : ''; ?></textarea>
						</div>
					</div>
				</div>
			</div>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
$('.date').datetimepicker({
	pickTime: false
});
$("#btn_plus").click(function(event) {
	var div_copy = $(".div_copy").last().clone();
	$(this).parents("#div_room").append(div_copy);
	set_id($(".div_copy").last());
	room_clear($(".div_copy").last());
});

$("#div_room").delegate('.btn_del', 'click', function(event) {
	var div_cnt = $(".div_copy").size();
	if (div_cnt > 1) {
		$(this).parents(".div_copy").remove();
	}else{
		room_clear($(this).parents(".div_copy"));
	}
});

function room_clear(obj) {
	obj.find('input').not(':checkbox').val('')
	obj.find(':checkbox').removeAttr('checked');
	obj.find('select').val("0");
	obj.find('.room_img').prop('src', '<?=$no_image?>');
}

function set_id(obj){
	var hidden_sn = parseInt(obj.attr("hidden_sn"));
	var new_sn = hidden_sn+1;
	obj.attr('hidden_sn', new_sn);
	obj.find('.img-thumbnail').prop('id', 'room_img_'+new_sn);
	obj.find('.input_room_image').prop('id', 'input_room_image_'+new_sn);
	obj.find('.checkbox_bath').prop('name', 'room[bath]['+new_sn+']');
	obj.find('.checkbox_wifi').prop('name', 'room[wifi]['+new_sn+']');
	$("#hidden_sn").val = new_sn;
}
</script>


</div>
<?php echo $footer; ?>