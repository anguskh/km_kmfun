<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-3">
              <div class="form-group">
                <label class="control-label" for="input-tables"><?php echo $entry_tables; ?></label>
                <select id="tables" name="selTable" id="input-customer-group" class="form-control">
                  <option value="*">--請選擇--</option>.
                  <?php foreach ($tables as $tableName) { ?>
                  <? $tableName == $selTable ? $selStr = "selected" : $selStr = "" ; ?>
                  <option value="<?=$tableName?>" <?=$selStr?>><?=$tableName?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="col-sm-3">
            </div>
          </div>
        </div>
        <form action="<?php echo $insert; ?>" method="post" enctype="multipart/form-data" id="form-autoinsert">
		  <input type="hidden" name="selTable" value="<?php echo $selTable; ?>" />
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td class="text-left" style="width:25%"><?php echo $column_field; ?></td>
                  <td class="text-left" style="width:25%"><?php echo $column_type; ?></a></td>
                  <td class="text-center" style="width:5%"><?php echo $column_key; ?></td>
				          <td class="text-right" style="width:20%"><?php echo $column_customize; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($descriptions) { ?>
                <?php foreach ($descriptions as $description) { ?>
                <tr>
                  <td class="text-left"><?php echo $description['Field']; ?></td>
                  <td class="text-left"><?php echo $description['Type']; ?></td>
                  <td class="text-center"><?php echo $description['Key']; ?></td>
				  <td class="text-right">
					<?php if ($description['Extra'] != 'auto_increment'): ?>
					<input type="text" name="customize[<?php echo $description['Field']; ?>]" />
					<?php else: ?>
					<?php echo $description['Extra']; ?>
					<?php endif; ?>
				  </td>
                </tr>
                <?php } ?>
				<tr>
				  <td class="text-right" colspan="4">新增 <input type="text" name="quantity" /> 筆資料 <button type="button" data-toggle="tooltip" title="" class="btn btn-primary" onclick="$('#form-autoinsert').submit();" data-original-title="送出"><i class="fa fa-paper-plane"></i></button></td>
				</tr>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="4"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('#tables').on('change', function() {
	url = 'index.php?route=develop/autoinsert&token=<?php echo $token; ?>';

	var tables = $('select[name=\'selTable\']').val();

	if (tables != '*') {
		url += '&tables=' + encodeURIComponent(tables);
	}

	location = url;
});
//--></script>
<?php echo $footer; ?>
