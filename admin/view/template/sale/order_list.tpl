<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<a href="<?php echo $url_add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
				<button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('確定嗎？') ? $('#form-delete').submit() : false;"><i class="fa fa-trash-o"></i></button>
			</div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<?php if ($success) { ?>
		<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
			</div>
			<div class="panel-body">
				<div class="well">
					<div class="row">
						<div class="col-sm-2">
							<div class="form-group">
								<label class="control-label" for="input-order-id">訂單 ID</label>
								<input type="text" name="filter_order_id" value="<?=$filter_order_id;?>" placeholder="訂單 ID" id="input-order-id" class="form-control" />
							</div>
							<div class="form-group">
								<label class="control-label" for="input-customer">領車人</label>
								<input type="text" name="filter_customer" value="<?=$filter_customer;?>" placeholder="客戶" id="input-customer" class="form-control" />
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label class="control-label" for="input-order-type">訂單類別</label>
								<select name="filter_order_type" id="input-order-type" class="form-control">
									<option value=""></option>
									<?php foreach ($optOrderType as $optKey => $optName) : ?>
										<option value="<?=$optKey?>" <?=($optKey == $filter_order_type) ? "selected" : "" ?>><?=$optName?></option>
									<?php endforeach ; ?>
								</select>
							</div>
							<div class="form-group">
								<label class="control-label" for="input-order-status">訂單狀態</label>
								<select name="filter_order_status" id="input-order-status" class="form-control">
									<option value="*"></option>
									<option value="success" <?=($filter_order_status == 'success') ? "selected" : "" ?>>成立</option>
									<option value="x" <?=($filter_order_status == 'x') ? "selected" : "" ?> >刪除</option>
								</select>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label class="control-label" for="input-station">取車地點</label>
								<select name="filter_station" id="input-station" class="form-control">
									<option value=""></option>
									<?php foreach ($optStation as $optKey => $optName) : ?>
										<option value="<?=$optKey?>" <?=($optKey == $filter_station) ? "selected" : "" ?>><?=$optName?></option>
									<?php endforeach ; ?>
								</select>
							</div>
							<div class="form-group">
								<label class="control-label" for="input-mobile">領車人電話</label>
								<input type="text" name="filter_mobile" value="<?=$filter_mobile;?>" placeholder="客戶電話" id="input-total" class="form-control" />
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label class="control-label" for="input-date-added">預計取車日期</label>
								<div class="input-group date">
									<input type="text" name="filter_date_out" value="<?=$filter_date_out?>" placeholder="出車日期" data-date-format="YYYY-MM-DD" id="input-date-out" class="form-control" />
									<span class="input-group-btn">
										<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
									</span>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label" for="input-date-modified">預計修改日期</label>
								<div class="input-group date">
									<input type="text" name="filter_date_modified" value="" placeholder="修改日期" data-date-format="YYYY-MM-DD" id="input-date-modified" class="form-control" />
									<span class="input-group-btn">
										<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
									</span>
								</div>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label class="control-label" for="input-date-added">實際取車日期</label>
								<div class="input-group date">
									<input type="text" name="filter_date_tout" value="<?=$filter_date_tout?>" placeholder="實際取車日期" data-date-format="YYYY-MM-DD" id="input-date-tout" class="form-control" />
									<span class="input-group-btn">
										<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
									</span>
								</div>
							</div>
						</div>						<div class="col-sm-2">
							<div class="form-group">
								<label class="control-label" for="input-date-added">實際還車日期</label>
								<div class="input-group date">
									<input type="text" name="filter_date_tout2" value="<?=$filter_date_tout2?>" placeholder="實際還車日期" data-date-format="YYYY-MM-DD" id="input-date-tout2" class="form-control" />
									<span class="input-group-btn">
										<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
									</span>
								</div>
							</div>
							<button style="margin:36px 0 0 0" type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-filter"></i> <?php echo $button_filter; ?></button>
						</div>
					</div>
				</div>

						<form method="post" action="<?=$url_delete?>" enctype="multipart/form-data" id="form-delete">
<div class="form-group required" style="overflow: hidden;">
	<a class="btn btn-xs order-st__messageitem">訂單簡訊已發送</a>
	<a class="btn btn-xs order-st__deleteitem">訂單已刪除 or 網路訂單未成單</a>
	<a class="btn btn-xs order-st__qrnopayitem">QRCode訂車未付款</a>
</div>
							<div class="table-responsive">
								<table class="table table-bordered table-hover order-list">
									<thead>
										<tr class="trtd">
											<!-- class="text-right"
											class="text-left" -->
						<td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
						<td class="text-right"><?php echo $column_action; ?></td>

						<?php foreach ($columnNames as $colKey => $columnName) : ?>
							<td class="text-left"><a href="<?=$sortUrl[$colKey] ?>" class="<?=( $sort == $colKey) ? strtolower( $order) : '' ?>"><?php echo $columnName; ?></a></td>
						<?php endforeach ; ?>
										</tr>
									</thead>
									<tbody>
										<?php if ($orders) { ?>
											<?php foreach ($orders as $order) { ?>
											<?php
												$smsColor = !empty( $order['batchID']) ? 'style="background-color: #e6ffe6;"' : "" ;
												$deleteitem = "" ;
												if($order['order_type'] == 'QRCode訂車' && $order['pay_type'] == '' ) $deleteitem = "class='qrnopayitem'";
												if($order['order_status'] == 'x') $deleteitem = "class='deleteitem'";
											?>
											<tr <?=$deleteitem?> class="trtd">
												<td class="text-center">
													<input type="checkbox" name="selected[]" value="<?php echo $order['order_id']; ?>" />
												</td>
												<td class="text-left" <?=$smsColor?> nowrap>
													<a href="<?php echo $order['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary">
														<i class="fa fa-pencil"></i></a>
													<a href="<?php echo $order['view']; ?>" target="_blank" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="btn btn-success">
														<i class="fa fa-file-text-o"></i></a>
													<?php if ( $order['order_status'] != 'x') : ?>
													<a href="<?php echo $order['delete']; ?>" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger">
														<i class="fa fa-trash-o"></i></a>
													<?php endif ; ?>
													<a target="_blank" href="<?php echo $order['savepdf']; ?>" data-toggle="tooltip" title="合約書" class="btn btn-primary">
														<i class="fa fa-file-pdf-o"></i></a>


												</td>

												<?php foreach( $columnNames as $colKey => $columnName) :
													$nowrapStr = ( $colKey == "order_type" || $colKey == "s_station" || $colKey == "e_station"  || $colKey == "rent_user_name") ? "nowrap" : "" ;
													$car_noClassStr = ( $colKey == "car_no" ) ? "license" : "" ;
													$oidStr = ( $colKey == "order_type" && isset($devAdmin)) ? "-" . $order['order_id'] : "" ;
												?>
												<td class="text-left <?=$car_noClassStr?>" <?=$nowrapStr?>><?=$order[$colKey]?><?=$oidStr?></td>
												<?php endforeach ; ?>
											</tr>
											<?php } ?>
										<?php } else { ?>
											<tr>
												<td class="text-center" colspan="<?=$td_colspan+2?>"><?php echo $text_no_results; ?></td>
											</tr>
										<?php } ?>
										</tbody>
									</table>
								</div>
							</form>
							<div class="row">
								<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
								<div class="col-sm-6 text-right"><?php echo $results; ?></div>
							</div>
						</div>
					</div>
				</div>
<style type="text/css">
	.trtd td {
		white-space: nowrap;
	}
</style>

<script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	var url = '?route=sale/order&token=<?php echo $token; ?>';
		var filter_order_id = $('input[name=\'filter_order_id\']').val();
		if (filter_order_id) {
			url += '&filter_order_id=' + encodeURIComponent(filter_order_id);
		}
		var filter_order_type = $('select[name=\'filter_order_type\']').val();
		if (filter_order_type) {
			url += '&filter_order_type=' + encodeURIComponent(filter_order_type);
		}
		var filter_customer = $('input[name=\'filter_customer\']').val();
		if (filter_customer) {
			url += '&filter_customer=' + encodeURIComponent(filter_customer);
		}
		var filter_order_status = $('select[name=\'filter_order_status\']').val();
		if (filter_order_status != '*') {
			url += '&filter_order_status=' + encodeURIComponent(filter_order_status);
		}
		var filter_mobile = $('input[name=\'filter_mobile\']').val();
		if (filter_mobile) {
			url += '&filter_mobile=' + encodeURIComponent(filter_mobile);
		}
		var filter_station = $('select[name=\'filter_station\']').val();
		if (filter_station) {
			url += '&filter_station=' + encodeURIComponent(filter_station);
		}
		var filter_date_out = $('input[name=\'filter_date_out\']').val();
		if (filter_date_out) {
			url += '&filter_date_out=' + encodeURIComponent(filter_date_out);
		}
		var filter_date_out2 = $('input[name=\'filter_date_out2\']').val();
		if (filter_date_out2) {
			url += '&filter_date_out2=' + encodeURIComponent(filter_date_out2);
		}
		var filter_date_tout = $('input[name=\'filter_date_tout\']').val();
		if (filter_date_tout) {
			url += '&filter_date_tout=' + encodeURIComponent(filter_date_tout);
		}
		var filter_date_tout2 = $('input[name=\'filter_date_tout2\']').val();
		if (filter_date_tout2) {
			url += '&filter_date_tout2=' + encodeURIComponent(filter_date_tout2);
		}
		var filter_date_modified = $('input[name=\'filter_date_modified\']').val();
		if (filter_date_modified) {
			url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
		}
	url += "&filter=filter" ;
	console.log( url) ;
	location = url;
});
//--></script>
<script type="text/javascript"><!--
//url: '?route=customer/customer/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
// 客戶名稱自動帶入
$('input[name=\'filter_customer\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: '?route=sale/order/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['name']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_customer\']').val(item['label']);
	}
});
//--></script>
	<script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
	<link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
	<script type="text/javascript"><!--
	$('.date').datetimepicker({
		pickTime: false
	});
//--></script></div>
<?php echo $footer; ?>