<?php
	$filename = "pdf_".$rentInfo['agreement_no'].".html";
?>
<!doctype html>
<html lang="zh-TW">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>
		<title>Website name</title>
	</head>
	<body>
		<div style="width: 900px; overflow: hidden; box-sizing: border-box; font-family: '微軟正黑體'; font-size: 12px;">
			<table style="margin: 0 0 10px 0; border-spacing: 0;">
				<tr>
					<td style="width: 33%;"></td>
					<td style="width: 34%; text-align: center;">
						<p style="margin: 0; padding: 0; font-size: 14.4px; font-weight: bold; letter-spacing: 2px;">Agreement</p>
						<p style="margin: 0; padding: 0; font-size: 36px; font-weight: bold; line-height: 36px;">汽車出租單</p>
						<p style="margin: 0; padding: 0; font-size: 19.2px; font-weight: bold;">□代駕　□短程接送</p>
					</td>
					<td style="width: 23%;">
						<p style="margin: 0; padding: 0; font-size: 14.4px; font-weight: bold;">出租合約書單號<br>Agreement No.</p>
						<p style="margin: 0; padding: 0; font-size: 36px; font-weight: bold; color: red;"><?=$rentInfo['agreement_no']?></p>
					</td>
					<td style="width: 10%; margin: 0; padding: 5px; vertical-align: top; text-align: center; font-weight: bold; border: 1px solid #000;">
						同行人數<br><?=($rentInfo['adult']+$rentInfo['baby']+$rentInfo['children']);?>
					</td>
				</tr>
			</table>
			<div style="width: 100%; overflow: hidden;">
				<div style="float: left; width: 20px; text-align: center; word-break: break-all;">
					第<br>一<br>聯<br><i style="display:block; font-style: normal; transform:rotate(90deg);">（</i>藍<i style="display:block; font-style: normal; transform:rotate(90deg);">）</i>：<br>公<br>司<br>存<br>查<br>聯<br>　<br>　<br>第<br>二<br>聯<i style="display:block; font-style: normal; transform:rotate(90deg);">（</i>綠<i style="display:block; font-style: normal; transform:rotate(90deg);">）</i>：<br>會<br>計<br>存<br>查<br>聯<br>　<br>　第<br>三<br>聯<i style="display:block; font-style: normal; transform:rotate(90deg);">（</i>白<i style="display:block; font-style: normal; transform:rotate(90deg);">）</i>：<br>客<br>戶<br>存<br>查<br>聯<br>　<br>　第<br>四<br>聯<i style="display:block; font-style: normal; transform:rotate(90deg);">（</i>黃<i style="display:block; font-style: normal; transform:rotate(90deg);">）</i>：<br>預<br>收<br>款<br>項<br>聯
				</div>
				<div style="float: left; width:880px;">
					<table style="width: 100%; border: 2px solid #000; border-spacing: 0;">
						<tr>
							<td style="padding: 0;">
								<table style="width: 100%; border-spacing: 0;">
									<tr>
										<td style="width: 33%; height: 48px; padding: 3px; font-weight: bold; color: red; vertical-align: top; border: 1px solid #000; border-top: 0; border-left: 0;">車牌號碼<i style="padding: 0 0 0 3px; font-style: normal;">Vehicle Plate No.</i><p style="color:black;"><?=$rentInfo['car_no']?></p></td>
										<td style="width: 34%; height: 48px; padding: 3px; font-weight: bold; vertical-align: top; border: 1px solid #000; border-top: 0; border-left: 0; border-right: 0;">廠牌/車型/顏色<i style="padding: 0 0 0 3px; font-style: normal;">Brand/Model/Color</i><p><?=$carSeedInfo['car_seed']?>/<?=$carInfo['car_color'];?></p></td>
										<td style="width: 33%; height: 48px; padding: 3px; font-weight: bold; vertical-align: top; border: 1px solid #000; border-top: 0; border-right: 0;">里程數(出/回)<i style="padding: 0 0 0 3px; font-style: normal;">Miles Out & In</i></td>
									</tr>
								</table>
								<table style="width: 100%; border-spacing: 0;">
									<tr>
										<td style="width: 25%; height: 48px; padding: 3px; font-weight: bold; color: red; vertical-align: top; border: 1px solid #000; border-top: 0; border-left: 0;">承租人姓名<i style="padding: 0 0 0 3px; font-style: normal;">Renter</i><p style="color:black;"><?=$rentInfo['rent_user_name']?></p></td>
										<td style="width: 30%; height: 48px; padding: 3px; font-weight: bold; color: red; vertical-align: top; border: 1px solid #000; border-top: 0; border-left: 0;">身分證字號/護照號碼ID<i style="padding: 0 0 0 3px; font-style: normal;">ID/Password No.</i><p style="color:black;"><?=$rentInfo['rent_user_id']?></p></td>
										<td style="width: 20%; height: 48px; padding: 3px; font-weight: bold; color: red; vertical-align: top; border: 1px solid #000; border-top: 0; border-left: 0; border-right: 0;">出生年月日<i style="padding: 0 0 0 3px; font-style: normal;">Date of Birth</i><p style="color:black;"><?=$rentInfo['rent_user_born']?></p></td>
										<td style="width: 25%; height: 48px; padding: 3px; font-weight: bold; color: red; vertical-align: top; border: 1px solid #000; border-top: 0; border-right: 0;">手機<i style="padding: 0 0 0 3px; font-style: normal;">Mobile</i><p style="color:black;"><?=$rentInfo['rent_user_tel']?></p></td>
									</tr>
								</table>
								<table style="width: 100%; border-spacing: 0;">
									<tr>
										<td style="width: 50%; height: 48px; padding: 3px; font-weight: bold; color: red; vertical-align: top; border: 1px solid #000; border-top: 0; border-left: 0; border-right: 0;">聯絡地址<i style="padding: 0 0 0 3px; font-style: normal;">Address</i><p style="color:black;"><?=$rentInfo['rent_user_address']?></p></td>
										<td style="width: 50%; height: 48px; padding: 3px; font-weight: bold; vertical-align: top; border: 1px solid #000; border-top: 0; border-right: 0;">
											公司名稱與統編<i style="padding: 0 0 0 3px; font-style: normal;">Company＆VAT No.</i><br><?=$rentInfo['rent_company_name'];?><br>
											<div style="margin:0; padding: 0; overflow: hidden;">
												<p style="float: right; margin: 0; padding: 0; overflow: hidden;"><span style="float: left; display: block; width: 24px; height: 24px; border: 1px solid #000; border-right: 0;"><?echo (substr($rentInfo['rent_company_no'],0,1))?></span><span style="float: left; display: block; width: 24px; height: 24px; border: 1px solid #000; border-right: 0;"><?echo (substr($rentInfo['rent_company_no'],1,1))?></span><span style="float: left; display: block; width: 24px; height: 24px; border: 1px solid #000; border-right: 0;"><?echo (substr($rentInfo['rent_company_no'],2,1))?></span><span style="float: left; display: block; width: 24px; height: 24px; border: 1px solid #000; border-right: 0;"><?echo (substr($rentInfo['rent_company_no'],3,1))?></span><span style="float: left; display: block; width: 24px; height: 24px; border: 1px solid #000; border-right: 0;"><?echo (substr($rentInfo['rent_company_no'],4,1))?></span><span style="float: left; display: block; width: 24px; height: 24px; border: 1px solid #000; border-right: 0;"><?echo (substr($rentInfo['rent_company_no'],5,1))?></span><span style="float: left; display: block; width: 24px; height: 24px; border: 1px solid #000; border-right: 0;"><?echo (substr($rentInfo['rent_company_no'],6,1))?></span><span style="float: left; display: block; width: 24px; height: 24px; border: 1px solid #000;"><?echo (substr($rentInfo['rent_company_no'],7,1))?></span></p>
											</div>
										</td>
									</tr>
								</table>
								<table style="width: 100%; border-spacing: 0;">
									<tr>
										<td style="width: 50%; height: 48px; padding: 3px; font-weight: bold; border: 1px solid #000; vertical-align: top; border: 1px solid #000; border-top: 0; border-left: 0; border-right: 0;">緊急連絡人/電話<i style="padding: 0 0 0 3px; font-style: normal;">Emergency Contact/Tel</i><p><?=$rentInfo['urgent_man']?>/<?=$rentInfo['urgent_mobile']?></p></td>
										<td style="width: 50%; width: 50%; height: 48px; padding: 3px; font-weight: bold; color: red; vertical-align: top; border: 1px solid #000; border-top: 0; border-right: 0;">共同承租人/身分證字號/電話<i style="padding: 0 0 0 3px; font-style: normal;">Co-Renter/ID No./Tel</i></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style="padding: 0; border: 2px solid red;">
								<table style="width: 100%; border-spacing: 0;">
									<tr>
										<td colspan="2" style="font-size: 14px; font-weight: bold; text-align: center; border-bottom: 1px solid #000;">出車/還車資料<i style="padding: 0 0 0 3px; font-style: normal;">Vechicle Check Out/In Information</i></td>
									</tr>
									<tr>
										<td style="width: 50%; padding: 3px; font-size: 8px;">
											<i style="font-style: normal; font-weight: bold;">注意事項</i><i style="padding: 0 0 0 3px; font-style: normal; font-weight: bold;">Point For Attention</i><br>
											1.車輛發生事故或失竊時請立刻報警處理（110）及通知本公司（082-371888）。<br>
											2.<i style="font-style: normal; color: red;">本公司車輛均有強制險，如有事故顧客需賠償本公司維修之費用及本公司之營業損失。</i><br>
											3.上述保險不包括因自然災害造成之損傷及車内配備遺失或損壞之賠償責任，承租人有保管之責，若有損竊事實發生，相關賠償概由承租人負擔。<br>
											4.承租人使用期間所有<i style="font-style: normal; color: red;">停車費及交通罰緩概由承租人負擔。</i><br>
											5.隨車證件：行照，強制保卡。<br>
											6.本人已於簽署前經合理期間審閱確認且<i style="font-style: normal; color: red; text-decoration: underline;">同意合約畫第一頁及第二頁全數條款及條件</i><i style="font-style: normal; color: red;">，並了解保險未含零配件之損竊，若有損竊發生承租人願負損害賠償之責。</i>若以信用卡付款時以下簽名即表示授權出租人就有關租金費，以承租人名義處理信用卡收款單以供付款。<br>
											7.承租人同意出租人基於租貨車輛需求，消費者/客戶管理，統計分析與研究分析同意本行蒐集，處理及利用其個人資料個護法第二條第一項之個人資料（如本合約書内容）。<br>
											<i style="font-style: normal; color: red;">取車需知：</i><br>
											<i style="font-style: normal; color: red;">計費方式：</i>本公司計費以<i style="font-style: normal; color: red;">24小時為一日，*逾時6小時</i>以一日計算，每逾1小時加收本公司訂定日租金10％，不足1小時以1小時計算：取車時依顧客預計還車時間計費；<i style="font-style: normal; color: red;">如需延長時間需於合約結束前3小時與本公司聯繫並取得同意</i>，無法續約情形發生時，請按合約結束時間前還車，<i style="font-style: normal; color: red;">因未準時還車影響其他顧客用車權益除收取超時租金外，將額外酌收該車日租金3倍之賠償金額。（*逾時為超出「日」之定義非指與本公司協議之時間）</i><br>
											<i style="font-style: normal; color: red;">油料部份：</i>本公司出租汽車之油料部份皆為滿油（油料指示針在滿油線以上），承租人需於還車需補滿油料還回，請參閱背頁油費計算公式計價。<br>
											提前還車：如旅客因個人因素提前還車依合約時間起算未滿一天不予退費。<br>
											還車須知：承租人交車時必需確認還車地點，如需變更得於前一日自行來電告知，如未按1每台車計畫地點辦理還車手續，本公司將酌收300元行政作業費用，敬請見諒！
										</td>
										<td style="width: 50%; padding: 3px; border-left: 1px dashed #000;">
											
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style="padding: 0; border-top: 1px solid #000; border-bottom: 1px solid #000;">
								<table style="width: 100%; border-spacing: 0;">
									<tr>
										<td style="width: 42%; height: 48px; padding: 3px; font-weight: bold; color: red; vertical-align: top; border-right: 1px solid #000;">
											出租日期與時間<i style="padding: 0 0 0 3px; font-style: normal;">Check OutTime & Date</i><br>
											<i style="font-size: 16px; font-style: normal; line-height: 32px; color: #000;">
											
											西元
											<u> <?echo substr($rentInfo['rent_date_out'],0,4)?> </u>年
											<u> <?echo substr($rentInfo['rent_date_out'],5,2)?> </u>月
											<u> <?echo substr($rentInfo['rent_date_out'],8,2)?> </u>日
											<u> <?echo substr($rentInfo['rent_date_out'],11,2)?> </u>時
											<u> <?echo substr($rentInfo['rent_date_out'],14,2)?> </u>分
											</i><i style="font-size: 10px; font-style: normal; line-height: 32px; color: #000;">(Y/M/D/TIME)</i>
										</td>
										<td style="width: 29%; height: 48px; padding: 3px; font-weight: bold; vertical-align: top; border-right: 1px solid #000;">
											出車業務簽名<i style="padding: 0 0 0 3px; font-style: normal;">Check Out Staff Signature</i>
										</td>
										<td style="width: 29%; height: 48px; padding: 3px; font-weight: bold; vertical-align: top;">
											承租人出車簽名<i style="padding: 0 0 0 3px; font-style: normal;">Check Out Renter Signature</i>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style="padding: 0; border-bottom: 1px solid #000;">
								<table style="width: 100%; border-spacing: 0;">
									<tr>
										<td style="width: 30%; padding: 3px 3px 3px 30px; vertical-align: top; font-weight: bold; border-right: 1px solid #000;">
											<i style="font-size: 16px; font-style: normal; color: red;">預計</i><i style="font-size: 16px; font-style: normal;">還車日期時間/地點</i>
											<br>
											<i style="font-style: normal">Est.Return date & Time / Location</i><br>
											<font style="font-size: 16px; font-style: normal;"><?=$rentInfo['rent_date_in_plan']?></font><br>
											<p style="font-size: 16px; line-height: 24px;">□ 航 班 / □ 船 班 時 間 為<br>
											___月___日___時___分；<br>
											<i style="font-size: 16px; font-style: normal; color: red;">預計還車地點：</i>    <?echo (($rentInfo['e_station']=='16')?'■':'□');?>金城店；<br>
											□山外店；<?echo (($rentInfo['e_station']=='18')?'■':'□');?>機場店；□碼頭；<br>
											□其他________</p>
										</td>
										<td style="width: 70%; padding: 3px; vertical-align: top;">
											<table style="width: 90%; margin: 0 auto 10px auto; border-spacing: 0;">
												<tr>
													<td style="width: 20%; font-size: 14px; font-weight: bold;">多車同單登記表</td>
													<td style="width: 80%;"><i style="font-style: normal; color: red;">*</i>共同承租人於簽署前經合理期間審閱確認且<i style="font-style: normal; color: red; text-decoration: underline;">同意合約書第一頁及第二頁全數條款及條件，租車時間同上述承租車。</i></td>
												</tr>
											</table>
											<table style="width: 100%; border: 1px solid #000; border-radius: 5px; border-spacing: 0;">
												<tr>
													<th style="width: 6%; height: 12px; text-align: center; color: red; font-size: 10px;  border-right: 1px solid #000; border-bottom: 1px solid #000;">次單號</th>
													<th style="width: 14%; height: 12px; text-align: center; color: red;  border-right: 1px solid #000; border-bottom: 1px solid #000;">車號</th>
													<th style="width: 20%; height: 12px; text-align: center; color: red;  border-right: 1px solid #000; border-bottom: 1px solid #000;">身分證字號</th>
													<th style="width: 20%; height: 12px; text-align: center; color: red;  border-right: 1px solid #000; border-bottom: 1px solid #000;">共同承租人簽名</th>
													<th style="width: 22%; height: 12px; text-align: center; color: red;  border-right: 1px solid #000; border-bottom: 1px solid #000;">還車時間</th>
													<th style="width: 18%; height: 12px; text-align: center; color: red;  border-bottom: 1px solid #000;">小計</th>
												</tr>
												<tr>
													<td style="height: 36px; text-align: center; border-right: 1px solid #000; border-bottom: 1px solid #000;" rowspan="2">單號<br>-01</td>
													<td style="text-align: center;border-right: 1px solid #000; border-bottom: 1px solid #000;" rowspan="2"><?=$rentInfo['car_no']?></td>
													<td style="text-align: center;border-right: 1px solid #000; border-bottom: 1px solid #000;" rowspan="2"><?=$rentInfo['rent_user_id']?></td>
													<td style="text-align: center;border-right: 1px solid #000; border-bottom: 1px solid #000;" rowspan="2"></td>
													<td style="text-align: center;border-right: 1px solid #000; border-bottom: 1px solid #000;">　年　月　日　時　分</td>
													<td style="text-align: center;border-bottom: 1px solid #000;"></td>
												</tr>
												<tr>
													<td style="border-bottom: 1px solid #000;" colspan="2">里程數出    km/回    Km</td>
												</tr>
												<tr>
													<td style="height: 36px; text-align: center;border-right: 1px solid #000; border-bottom: 1px solid #000;" rowspan="2">單號<br>-02</td>
													<td style="text-align: center;border-right: 1px solid #000; border-bottom: 1px solid #000;" rowspan="2"></td>
													<td style="text-align: center;border-right: 1px solid #000; border-bottom: 1px solid #000;" rowspan="2"></td>
													<td style="text-align: center;border-right: 1px solid #000; border-bottom: 1px solid #000;" rowspan="2"></td>
													<td style="text-align: center;border-right: 1px solid #000; border-bottom: 1px solid #000;">　年　月　日　時　分</td>
													<td style="text-align: center;border-bottom: 1px solid #000;"></td>
												</tr>
												<tr>
													<td style="border-bottom: 1px solid #000;" colspan="2">里程數出    km/回    Km</td>
												</tr>
												<tr>
													<td style="height: 36px; text-align: center;border-right: 1px solid #000;" rowspan="2">單號<br>-03</td>
													<td style="text-align: center;border-right: 1px solid #000;" rowspan="2"></td>
													<td style="text-align: center;border-right: 1px solid #000;" rowspan="2"></td>
													<td style="text-align: center;border-right: 1px solid #000;" rowspan="2"></td>
													<td style="text-align: center;border-right: 1px solid #000; border-bottom: 1px solid #000;">　年　月　日　時　分</td>
													<td style="text-align: center;border-bottom: 1px solid #000;"></td>
												</tr>
												<tr>
													<td colspan="2">里程數出    km/回    Km</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style="padding: 0; border-top: 1px solid #000; border-bottom: 1px solid #000;">
								<table style="width: 100%; border-spacing: 0;">
									<tr>
										<td style="width: 42%; height: 48px; padding: 3px; font-weight: bold; color: red; vertical-align: top; border-right: 1px solid #000;">
											實際還車日期時間/地點<i style="padding: 0 0 0 3px; font-style: normal;">Est.Return date & Time / Location</i><br>
											<i style="font-size: 16px; font-style: normal; line-height: 32px; color: #000;">西元________年____月____日____時____分</i><i style="font-size: 10px; font-style: normal; line-height: 32px; color: #000;">(Y/M/D/TIME)</i>
										</td>
										<td style="width: 29%; height: 48px; padding: 3px; font-weight: bold; vertical-align: top; border-right: 1px solid #000;">
											還車業務簽名<i style="font-style: normal;">Check In Staff Signature</i>
										</td>
										<td style="width: 29%; height: 48px; padding: 3px; font-weight: bold; vertical-align: top;">
											承租人還車簽名<i style="font-style: normal;">Check Out Renter Signature</i>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td style="padding: 0;">
								<table style="position: relative; width: 100%; border-spacing: 0;">
									<tr>
										<td style="position: absolute; right: 0; top: 0; width: 32.2%; height: 142px; padding: 3px; border: 1px solid #000; border-top: 0; border-right: 0; background: #fff;">專案核銷碼/備註</td>
										<td style="width: 3%; font-weight: bold; font-size: 16px; text-align: center; border-right: 1px solid #000;">收<br>款<br>明<br>細<br><i style="font-style: normal; color: red;">　<br>禁<br>止<br>修<br>改<br>價<br>格</i></td>
										<td style="width: 97%; padding: 0; border-bottom: 0;">
											<div style="padding: 3px; border-bottom: 1px solid #000;">
												行前預收款： □匯款；□刷卡；□官網(信/超/A/____)；專案________________(____時)；<br>
												□第三方支付NTD/RMB(____)；其他_________　　<i style="font-weight: bold; font-style: normal; color: red;">使用時間：<u> <?=$rentInfo['rentDay'];?> </u>天<u> <?=$rentInfo['rentHour'];?> 時</i>
												<table style="width: 100%; border: 1px solid #000; border-spacing: 0;">
													<tr>
														<th style="width: 8%; text-align: center; border-right: 1px solid #000; border-bottom: 1px solid #000;">租車費用</th>
														<th style="width: 18%; text-align: center; border-right: 1px solid #000; border-bottom: 1px solid #000;">兒童安全椅<?=$rentInfo['baby_chair1'];?>座<?=$rentInfo['useDay'];?>天</th>
														<th style="width: 15%; text-align: center; border-right: 1px solid #000; border-bottom: 1px solid #000;">兒童推車<?=$rentInfo['children_chair'];?>台<?=$rentInfo['useDay'];?>天</th>
														<th style="width: 12%; text-align: center; border-right: 1px solid #000; border-bottom: 1px solid #000;">其他________</th>
														<th style="width: 13%; text-align: center; border: 3px solid red;border-bottom: 1px solid #000;">預收小計</th>
														<th style="width: 34%;"></th>
													</tr>
													<tr>
														<td style="height: 48px; border-right: 1px solid #000;"><?=$rentInfo['car_price2'];?></td>
														<td style="border-right: 1px solid #000;"><?=$rentInfo['baby_chair1_price'];?></td>
														<td style="border-right: 1px solid #000;"><?=$rentInfo['children_chair_price'];?></td>
														<td style="border-right: 1px solid #000;"></td>
														<td style="border: 3px solid red; border-top: 0;"><?=$rentInfo['totalStr'];?></td>
														<td></td>
													</tr>
												</table>
											</div>
											<div style="padding: 3px; border-bottom: 1px solid #000;">
												現場改款：□現金；□刷卡；□官網(信/超/A/____)；專案________________(____時)；<br>
												□第三方支付NTD/RMB(____)；其他_________　　<i style="font-weight: bold; font-style: normal; color: red;">使用時間：____天____時</i>
												<table style="width: 100%; border: 1px solid #000; border-spacing: 0;">
													<tr>
														<th style="width: 8%; padding: 3px; border-right: 1px solid #000; border-bottom: 1px solid #000;">租車費用</th>
														<th style="width: 18%; padding: 3px; border-right: 1px solid #000; border-bottom: 1px solid #000;">兒童安全椅__座__天</th>
														<th style="width: 15%; padding: 3px; border-right: 1px solid #000; border-bottom: 1px solid #000;">兒童推車__台__天</th>
														<th style="width: 12%; padding: 3px; border-right: 1px solid #000; border-bottom: 1px solid #000;">其他________</th>
														<th style="width: 13%; padding: 3px; border: 3px solid red; border-bottom: 1px solid #000; border-right: 1px solid #000;">預收小計</th>
														<th style="width: 17%; padding: 3px; color: red; border: 3px solid red; border-left: 0; border-bottom: 1px solid #000;">收款人員簽名</th>
														<th style="width: 17%; padding: 3px; color: red; border-bottom: 1px solid #000;">業者/業務</th>
													</tr>
													<tr>
														<td style="height: 48px; border-right: 1px solid #000;"></td>
														<td style="border-right: 1px solid #000;"></td>
														<td style="border-right: 1px solid #000;"></td>
														<td style="border-right: 1px solid #000;"></td>
														<td style="border: 3px solid red; border-top: 0; border-right: 1px solid #000;"></td>
														<td style="border: 3px solid red; border-top: 0; border-left: 0;"></td>
														<td></td>
													</tr>
												</table>
											</div>
											<div style="padding: 3px;">
												加收款項：□現金；□刷卡；□第三方支付 NTD/RMB(____)；其他_________
												<table style="width: 100%; border: 1px solid #000; border-spacing: 0;">
													<tr>
														<th style="width: 9%; border-right: 1px solid #000; border-bottom: 1px solid #000;">逾時</th>
														<th style="width: 9%; border-right: 1px solid #000; border-bottom: 1px solid #000;">車輛損壞賠償</th>
														<th style="width: 9%; border-right: 1px solid #000; border-bottom: 1px solid #000;">營業損失</th>
														<th style="width: 9%; border-right: 1px solid #000; border-bottom: 1px solid #000;">車輛清潔費</th>
														<th style="width: 9%; border-right: 1px solid #000; border-bottom: 1px solid #000;">救援費</th>
														<th style="width: 9%; border-right: 1px solid #000; border-bottom: 1px solid #000;">油費</th>
														<td style="width: 9%; border-right: 1px solid #000; border-bottom: 1px solid #000;">行李接送費</th>
														<th style="width: 9%; border-right: 1px solid #000; border-bottom: 1px solid #000;">其他______</th>
														<th style="width: 9%; border-right: 1px solid #000; border-bottom: 1px solid #000;">加收小計</th>
														<th style="width: 19%; border-bottom: 1px solid #000;">收款人員簽名</th>
													</tr>
													<tr>
														<td style="height: 48px; border-right: 1px solid #000;"></td>
														<td style="border-right: 1px solid #000;"></td>
														<td style="border-right: 1px solid #000;"></td>
														<td style="border-right: 1px solid #000;"></td>
														<td style="border-right: 1px solid #000;"></td>
														<td style="border-right: 1px solid #000;"></td>
														<td style="border-right: 1px solid #000;"></td>
														<td style="border-right: 1px solid #000;"></td>
														<td style="border-right: 1px solid #000;"></td>
														<td></td>
													</tr>
												</table>
											</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>	
				</div>
			</div>
			<table style="margin: 10px 0 0 0;">
				<tr>
					<td style="width: 20%; padding: 0 5% 0 20px;">
						
					</td>
					<td style="width: 80%;">
						出租行號：金豐租車行　統一編號：41147204　　E-Mail：kmfun999@gmail.com　　傳真：082-372333　　<i style="font-style: normal; color: red;">http://kmfun.tw</i><br>
						營登地址：金門縣金湖鎮太湖路二段207號1樓　　金城總店082-371888　　金湖店082-330222　　機場服務檯082-335333
					</td>
				</tr>
			</table>
		</div>
		<style>
		#footer {
			position: fixed;
			width: 100%;
			bottom: 0;
			z-index: 1;
			/* background: #dcdcdc; */
			text-align: center;
			margin-left: -10px;
		}
		</style>
		<div id="footer"><input id="btn_savepdf" onclick="" type="button" value="轉存pdf"></div>
		<script>
			/*$("#btn_savepdf").click(function(){
				var body_html = $( "body" ).html();
				var ajaxUrl = "?route=sale/order/savepdf&token=<?php echo $token;?>";
				$.ajax({
					type:'post',
					url: ajaxUrl,
					data: { body_html:body_html,
							filename:'<?php echo $filename;?>'},
					success: function(json) {
						alert(body_html);
						console.log(json);
					}
				});
			});*/
		</script>
	</body>	
</html>
