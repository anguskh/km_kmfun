<?php echo $header; ?><?php echo $column_left; ?>
<?php
if($order_type != '140'){
	unset($order_types[140]);
}
?>
<div id="content">
<div class="page-header">
	<div class="container-fluid">
		<div class="pull-right">
			<button type="button" form="form-product" id="save_submit" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
			<a href="<?php echo $url_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></a>
		</div>
		<h1><?php echo $heading_title; ?></h1>
		<ul class="breadcrumb">
			<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
			<?php } ?>
		</ul>
	</div>
</div>
<div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
		</div>
		<div class="panel-body">
			<form action="<?php echo $url_action; ?>" method="post" enctype="multipart/form-data" id="form-product" class="form-horizontal">
				<ul id="order" class="nav nav-tabs">
					<li class=" active"><a href="#tab-cart" data-toggle="tab">1. 租車資訊</a></li>
					<li class=""><a href="#tab-accessories" data-toggle="tab">2. 配件資訊</a></li>
					<li class=""><a href="#tab-customer" data-toggle="tab">3. 客戶資訊</a></li>
					<li class=""><a href="#tab-flight" data-toggle="tab">4. 航船資訊</a></li>
					<li class=""><a href="#tab-ExceptionalLoss" data-toggle="tab">5. 加收費用</a></li>
					<?php
						//echo (($order_type=='140')?'<li class="last">$ <span id="t_money">'.$total.'</span></a></li>':'');
						//改全部訂單都要即時計算金額 2019/09/25



						echo '<li class="last"><span style="background: #8fbb6c; font-size: 14px; color:#fff; padding: 0 10px; border-radius: 20px"><b>共計</b> <span id="rentDay1">'.$rentDay.'</span> <b>天</b> <span id="rentHour1">'.$rentHour.'</span> <b>時</b></span>　 $ <span id="t_money">'.$total.'</span></a></li>';
					?>
					<input type="hidden" name="total" id="total" value="<?=$total?>">
				</ul>
				<div class="tab-content">
					<!-- 租車資訊 Start -->
					<div class="tab-pane active" id="tab-cart">
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-order-type">訂單類別</label>
							<div class="col-sm-10">
								<select name="order_type" id="order-type" class="form-control">
									<option value=""> --- 請選擇 --- </option>
								<?php foreach ($order_types as $mid => $optionName) { ?>
									<?php if( $order_type == $mid) : ?>
										<option value="<?php echo $mid; ?>" selected><?php echo $optionName; ?></option>
									<?php else : ?>
										<option value="<?php echo $mid; ?>"><?php echo $optionName; ?></option>
									<?php endif ; ?>
								<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group" id="minsu_area" >
							<label class="col-sm-2 control-label">單位</label>
							<div class="col-sm-10">
								<input type="text" name="minsu_name" value="<?=$minsu_name?>" id="car-no" placeholder="" class="form-control" />
								<input type="hidden" name="minsu_idx" value="<?=$minsu_idx?>" id="car-no" placeholder="" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="s-station">取車地點</label>
							<div class="col-sm-10">
								<select name="s_station" id="s-station" class="form-control">
									<option value=""> --- 請選擇 --- </option>
								<?php foreach ($stations as $mid => $optionName) { ?>
									<?php if( $s_station == $mid) : ?>
										<option value="<?php echo $mid; ?>" selected><?php echo $optionName; ?></option>
									<?php else : ?>
										<option value="<?php echo $mid; ?>"><?php echo $optionName; ?></option>
									<?php endif ; ?>
								<?php } ?>
								</select>
								<!--<input type="text" name="rent_pos" value=""
									id="input-rent-pos" placeholder="金湖店" class="form-control" />-->
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="rent-date-out">取車時間</label>
							<div class="col-sm-10">
								<div class="input-group date">
									<input type="text" name="rent_date_out" value="<?=$rent_date_out?>" placeholder="取車時間"
										data-date-format="YYYY-MM-DD" id="rent-date-out" class="form-control" />
									<span class="input-group-btn">
										<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
									</span>
									<select name="out_time" class="form-control">
											<option value="">請選擇</option>
											<?php foreach ($rentTimeOptArr as $optItem) :
												$seledStr = ( $optItem == $rent_date_out_time) ? "selected" : "" ;
											?>
											<option value="<?=$optItem?>" <?=$seledStr?>><?=$optItem?></option>
										<?php endforeach ; ?>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="e-station">還車地點</label>
							<div class="col-sm-10">
								<select name="e_station" id="e-station" class="form-control">
									<option value=""> --- 請選擇 --- </option>
								<?php foreach ($stations as $mid => $optionName) { ?>
									<?php if( $e_station == $mid) : ?>
										<option value="<?php echo $mid; ?>" selected><?php echo $optionName; ?></option>
									<?php else : ?>
										<option value="<?php echo $mid; ?>"><?php echo $optionName; ?></option>
									<?php endif ; ?>
								<?php } ?>
								</select>
								<!--<input type="text" name="return_pos" value=""
									id="input-return-pos" placeholder="金湖店" class="form-control" />-->
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="rent-date-in-plan">還車時間</label>
							<div class="col-sm-10">
								<div class="input-group date">
									<input type="text" name="rent_date_in_plan" value="<?=$rent_date_in_plan?>" placeholder="還車時間"
										data-date-format="YYYY-MM-DD" id="rent-date-in-plan" class="form-control" />
									<span class="input-group-btn">
										<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
									</span>
									<select name="plan_time" class="form-control">
											<option value="">請選擇</option>
											<?php foreach ($rentTimeOptArr as $optItem) :
												$seledStr = ( $optItem == $rent_date_in_plan_time) ? "selected" : "" ;
											?>
											<option value="<?=$optItem?>" <?=$seledStr?>><?=$optItem?></option>
										<?php endforeach ; ?>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-car-model">車種</label>
							<div class="col-sm-10">
								<?php foreach ($car_models as $mid => $optionName) { ?>
									<?php $labelClassStr = in_array( $mid, $car_model) ? "" : "bt-background-clean" ; ?>
									<?php $chkedStr      = in_array( $mid, $car_model) ? "checked" : "" ; ?>
								<label name="carModels" class="btn btn-success <?=$labelClassStr?>" >
									<i class="fa fa-car"></i>&nbsp;<?=$optionName?>
									<input class="hidden" type="checkbox" name="selCarModels[]" value="<?=$mid?>" <?=$chkedStr?> />
								</label>&nbsp&nbsp
								<?php } ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="sel-car-seed">車型</label>
							<div class="col-sm-10" id="showCarType">

								<?php foreach ($car_seeds as $mid => $optionName) { ?>
									<?php $labelClassStr = in_array( $optionName['idx'], $car_seed) ? "" : "bt-background-clean" ; ?>
									<?php $chkedStr      = in_array( $optionName['idx'], $car_seed) ? "checked" : "" ; ?>
								<label name="carModels" class="btn btn-success <?=$labelClassStr?>" >
									<i class="fa fa-car"></i>&nbsp;<?=$optionName['car_seed']?>
									<input class="hidden" type="checkbox" name="selCarTypes[]" value="<?=$optionName['idx']?>" <?=$chkedStr?> />
								</label>&nbsp&nbsp
								<?php } ?>
							</div>
							<span id="showJs"></span>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">牌照號碼</label>
							<div class="col-sm-10" id="showArea">
								<?=$ckbCarNoHtml?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">訂車數量</label>
							<div class="col-sm-10">
								<input type="text" name="total_car" value="<?php echo $total_car;?>" placeholder="訂車數量" id="total_car" class="form-control" disabled/>
							</div>
						</div>
						<!--
						<div class="form-group" <?php echo (($order_type=='140')?'style="display:none"':'');?>>
							<label class="col-sm-2 control-label">收費方式</label>
							<div class="col-sm-10">
								<input type="radio" name="charge_method" value="1" <?php echo ($charge_method == 1) ? "checked" : "" ;?>>未收款&nbsp;&nbsp;
								<input type="radio" name="charge_method" value="2" <?php echo ($charge_method == 2) ? "checked" : "" ;?>>應收&nbsp;&nbsp;
								<input type="radio" name="charge_method" value="3" <?php echo ($charge_method == 3) ? "checked" : "" ;?>>已收

							</div>
						</div>
						-->
						<div class="form-group">
							<label class="col-sm-2 control-label">付款類型</label>
							<div class="col-sm-10">
								<input type="radio" name="pay_type" value="1" <?php echo ($pay_type == 1) ? "checked" : "" ;?>>信用卡&nbsp;&nbsp;
								<input type="radio" name="pay_type" value="2" <?php echo ($pay_type == 2) ? "checked" : "" ;?>>現金&nbsp;&nbsp;
								<input type="radio" name="pay_type" value="3" <?php echo ($pay_type == 3) ? "checked" : "" ;?>>匯款&nbsp;&nbsp;
								<input type="radio" name="pay_type" value="4" <?php echo ($pay_type == 4) ? "checked" : "" ;?>>應收&nbsp;&nbsp;
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">SPDC</label>
							<div class="col-sm-10">
								<input type="text" name="spdc" value="<?=$spdc?>" placeholder="SPDC" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">異動差額</label>
							<div class="col-sm-10">
								<input type="text" name="pickdiff" value="<?=$pickdiff?>" placeholder="異動差額" class="form-control" disabled/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">MEMO</label>
							<div class="col-sm-9">
								<input type="text" name="remark1" id="remark1" class="form-control">
							</div>
							<div class="col-sm-1" style="padding: 7px 0; text-align: left;">
								<a onclick="ins_reamrk('remark1');" class="add-btn" href="javascript:void();">＋</a>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">備註</label>
							<div class="col-sm-10">
								<textarea name="area_memo" id="area_memo" cols="100" rows="5" disabled><?php echo (($area_memo=="")?'輸入你想要寫的內容...':$area_memo);?></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-12 text-right"><b>共計</b> <span id="rentDay"><?=$rentDay?></span> <b>天</b> <span id="rentHour"><?=$rentHour?></span> <b>時</b></label>
						</div>
					</div>
					<!-- 租車資訊 End -->
					<!-- 配件資訊 Start -->
					<div class="tab-pane" id="tab-accessories">
<?php

						$adult = (($adult=="")?0:$adult);
						$children = (($children=="")?0:$children);
						$baby = (($baby=="")?0:$baby);
						$baby_chair1 = (($baby_chair1=="")?0:$baby_chair1);
						$baby_chair2 = (($baby_chair2=="")?0:$baby_chair2);
						$children_chair = (($children_chair=="")?0:$children_chair);
						$gps = (($gps=="")?0:$gps);
						$mobile_moto = (($mobile_moto=="")?0:$mobile_moto);
?>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="mobile_moto">機車導航架(金額50)</label>
							<div class="col-sm-10">
								<input type="text" name="mobile_moto" id="mobile_moto" value="<?php echo $mobile_moto;?>" placeholder="機車導航架" id="input-mobile_moto" class="form-control" maxlength="1">
								<!--
								<select name="mobile_moto" id="mobile_moto" class="form-control">
									<?php for( $i = 0; $i < 4; $i++) :
										$val = str_pad($i, 1, '0', STR_PAD_LEFT) ;
									?>
									<option value="<?=$i?>" <?=( $i == $mobile_moto ) ? "selected" : "" ?>><?=$i?></option>
									<?php endfor ; ?>
								</select -->
							</div>
						</div>
						<!--<div class="form-group">
							<label class="col-sm-2 control-label" for="baby_chair">兒童安全座椅〔一歲以下〕</label>
							<div class="col-sm-10">
								<select name="baby_chair" id="baby_chair" class="form-control">
									<?php for( $i = 0; $i < 4; $i++) :
										$val = str_pad($i, 1, '0', STR_PAD_LEFT) ;
									?>
									<option value="<?=$i?>" <?=( $i == $baby_chair ) ? "selected" : "" ?>><?=$i?></option>
									<?php endfor ; ?>
								</select>
							</div>
						</div>-->
						<div class="form-group">
							<label class="col-sm-2 control-label" for="baby_chair1">兒童安全座椅〔一歲以上〕</label>
							<div class="col-sm-10">
								<input type="text" name="baby_chair1" id="baby_chair1" value="<?php echo $baby_chair1;?>" placeholder="兒童安全座椅〔一歲以上〕" id="input-baby_chair1" class="form-control" maxlength="1">
								<!--
								<select name="baby_chair1" id="baby_chair1" class="form-control">
									<?php for( $i = 0; $i < 4; $i++) :
										$val = str_pad($i, 1, '0', STR_PAD_LEFT) ;
									?>
									<option value="<?=$i?>" <?=( $i == $baby_chair1 ) ? "selected" : "" ?>><?=$i?></option>
									<?php endfor ; ?>
								</select -->
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="baby_chair2">兒童安全座椅〔增高墊〕</label>
							<div class="col-sm-10">
								<input type="text" name="baby_chair2" id="baby_chair2" value="<?php echo $baby_chair2;?>" placeholder="兒童安全座椅〔增高墊〕" id="input-baby_chair2" class="form-control" maxlength="1">
								<!--
								<select name="baby_chair2" id="baby_chair2" class="form-control">
									<?php for( $i = 0; $i < 4; $i++) :
										$val = str_pad($i, 1, '0', STR_PAD_LEFT) ;
									?>
									<option value="<?=$i?>" <?=( $i == $baby_chair2 ) ? "selected" : "" ?>><?=$i?></option>
									<?php endfor ; ?>
								</select -->
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="children_chair">嬰兒推車</label>
							<div class="col-sm-10">
								<input type="text" name="children_chair" id="children_chair" value="<?php echo $children_chair;?>" placeholder="嬰兒推車" id="input-children_chair" class="form-control" maxlength="1">
								<!--
								<select name="children_chair" id="children_chair" class="form-control">
									<?php for( $i = 0; $i < 4; $i++) :
										$val = str_pad($i, 1, '0', STR_PAD_LEFT) ;
									?>
									<option value="<?=$i?>" <?=( $i == $children_chair ) ? "selected" : "" ?>><?=$i?></option>
									<?php endfor ; ?>
								</select -->
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="gps">GPS衛星導航系統</label>
							<div class="col-sm-10">
								<input type="text" name="gps" id="gps" value="<?php echo $gps;?>" placeholder="GPS衛星導航系統" id="input-gps" class="form-control" maxlength="1">
								<!--
								<select name="gps" id="gps" class="form-control">
									<?php for( $i = 0; $i < 4; $i++) :
										$val = str_pad($i, 1, '0', STR_PAD_LEFT) ;
									?>
									<option value="<?=$i?>" <?=( $i == $gps ) ? "selected" : "" ?>><?=$i?></option>
									<?php endfor ; ?>
								</select -->
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">MEMO</label>
							<div class="col-sm-9">
								<input type="text" name="remark2" id="remark2" class="form-control">
							</div>
							<div class="col-sm-1" style="padding: 7px 0; text-align: left;">
								<a onclick="ins_reamrk('remark2');" class="add-btn" href="javascript:void();">＋</a>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">備註</label>
							<div class="col-sm-10">
								<textarea name="area_memo2" id="area_memo2" cols="100" rows="5" disabled><?php echo (($area_memo=="")?'輸入你想要寫的內容...':$area_memo);?></textarea>
							</div>
						</div>

					</div>
					<!-- 配件資訊 End -->
					<!-- 客戶資訊 Start -->
					<div class="tab-pane" id="tab-customer">
						<!-- div class="form-group">
							<label class="col-sm-2 control-label" for="input-customer">客戶</label>
							<div class="col-sm-10">
								<input type="text" name="customer" value="" placeholder="客戶" id="input-customer" class="form-control" />
								<input type="hidden" name="customer_id" value="" />
							</div>
						</div -->
						<div class="form-group">
							<label class="col-sm-2 control-label" for="rent-user-name">訂單編號</label>
							<div class="col-sm-10">
								<?=$agreement_no?>
								<input type="hidden" name="agreement_no" value="<?=$agreement_no?>" />
								<input type="hidden" name="order_id" value="<?=$order_id?>" />
							</div>
						</div>
						<div class="form-group required">
							<label class="col-sm-2 control-label" for="rent-user-name">承租人姓名</label>
							<div class="col-sm-10">
								<input type="text" name="rent_user_name" value="<?=$rent_user_name?>" placeholder="承租人姓名" id="rent-user-name" class="form-control"  />
							</div>
						</div>
						<div class="form-group required">
							<label class="col-sm-2 control-label" for="rent-user-id">身份證字號 / 護照號碼</label>
							<div class="col-sm-10">
								<input type="text" name="rent_user_id" value="<?=$rent_user_id?>" placeholder="承租人身分證字號 / 護照號碼" id="rent-user-id" class="form-control" />
								<input type="radio" name="rent_identity" value="1" <?=(($rent_identity==1)?'checked':'')?>>國內身份證
								<input type="radio" name="rent_identity" value="3" <?=(($rent_identity==3)?'checked':'')?>>其他
							</div>
						</div>
						<div class="form-group required">
							<label class="col-sm-2 control-label" for="rent-user-born">出生年月日</label>
							<div class="col-sm-10">
								<input type="text" name="rent_user_born" value="<?=$rent_user_born?>" placeholder="填寫格式 : 1999/01/01" id="rent-user-born" class="form-control" />
							</div>
						</div>
						<div class="form-group required">
							<label class="col-sm-2 control-label" for="rent-user-tel">電話 / 手機</label>
							<div class="col-sm-10">
								<input type="text" name="rent_user_tel" value="<?=$rent_user_tel?>" placeholder="電話 / 手機" id="rent-user-tel" class="form-control" />
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-2 control-label" for="rent-user-tel">承租人Email</label>
							<div class="col-sm-10">
								<input type="text" name="rent_user_mail" value="<?=$rent_user_mail?>" placeholder="電子郵件" id="rent-user-tel" class="form-control" />
							</div>
						</div>
						<div class="form-group required">
							<label class="col-sm-2 control-label" for="rent-company-name">取車人姓名</label>
							<div class="col-sm-10">
								<input type="text" name="get_user_name" value="<?=$get_user_name?>" placeholder="取車人姓名" id="rent-user-tel" class="form-control" />
								<input type="checkbox" name="syncrent" class="form-control" /> 同承租人資料
							</div>
						</div>
						<div class="form-group required">
							<label class="col-sm-2 control-label" for="rent-company-name">身份證字號 / 護照號碼</label>
							<div class="col-sm-10">
								<input type="text" name="get_user_id" value="<?=$get_user_id?>" placeholder="取車人身份證字號 / 護照號碼" id="rent-user-tel" class="form-control" />
								<input type="radio" name="get_identity" value="1" <?=(($get_identity==1)?'checked':'')?>>國內身份證
								<input type="radio" name="get_identity" value="3" <?=(($get_identity==3)?'checked':'')?>>其他
							</div>
						</div>
						<div class="form-group required">
							<label class="col-sm-2 control-label" for="rent-company-name">出生年月日</label>
							<div class="col-sm-10">
								<input type="text" name="get_user_born" value="<?=$get_user_born?>" placeholder="填寫格式 : 1999/01/01" id="rent-user-tel" class="form-control" />
							</div>
						</div>
						<div class="form-group required">
							<label class="col-sm-2 control-label" for="rent-company-name">電話 / 手機</label>
							<div class="col-sm-10">
								<input type="text" name="get_user_tel" value="<?=$get_user_tel?>" placeholder="電話 / 手機" id="rent-user-tel" class="form-control" />
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-2 control-label" for="input-customer-address">聯絡人地址</label>
							<div class="col-sm-10">
								<select name="rent_zip1" class="form-control">
									<option value="">請選擇</option>
								<?php foreach ($optZip1Arr as $i => $optArr) :
									$seledStr = ( $rent_zip1 == $optArr['zip_code']) ? "selected" : "" ;
								?>
									<option value="<?=$optArr['zip_code']?>" <?=$seledStr?>><?=$optArr['zip_name']?></option>
								<?php endforeach ; ?>
								</select>
								<select name="rent_zip2" id="rent_zip2" class="form-control">
									<option value="">請選擇</option>
								<?php foreach ($optZip2Arr as $i => $optArr) :
									$seledStr = ( $rent_zip2 == $optArr['zip_code']) ? "selected" : "" ;
								?>
									<option value="<?=$optArr['zip_code']?>" <?=$seledStr?>><?=$optArr['zip_code']?> <?=$optArr['zip_name']?></option>
								<?php endforeach ; ?>
								</select>
								<input type="text" name="rent_user_address" value="<?=$rent_user_address?>" placeholder="聯絡人地址" id="input-customer-address" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="rent-company-name">公司名稱</label>
							<div class="col-sm-10">
								<input type="text" name="rent_company_name" value="<?=$rent_company_name?>" placeholder="公司名稱" id="rent-company-name" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="rent-company-no">公司統編</label>
							<div class="col-sm-10">
								<input type="text" name="rent_company_no" value="<?=$rent_company_no?>" placeholder="公司統編" id="rent-company-no" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="urgent-man">緊急連絡人</label>
							<div class="col-sm-10">
								<input type="text" name="urgent_man" value="<?=$urgent_man?>" placeholder="緊急連絡人" id="urgent-man" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="urgent-mobile">緊急連絡人電話</label>
							<div class="col-sm-10">
								<input type="text" name="urgent_mobile" value="<?=$urgent_mobile?>" placeholder="緊急連絡人電話" id="urgent-mobile" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="adult">成人</label>
							<div class="col-sm-10">
								<input type="text" name="adult" value="<?php echo $adult;?>" placeholder="成人" id="input-adult" class="form-control" maxlength="1">
								<!--
								<select name="adult" id="adult" class="form-control">
									<?php for( $i = 0; $i < 10; $i++) :
										$val = str_pad($i, 1, '0', STR_PAD_LEFT) ;
									?>
									<option value="<?=$i?>" <?=( $i == $adult ) ? "selected" : "" ?>><?=$i?></option>
									<?php endfor ; ?>
								</select -->
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="children">幼兒</label>
							<div class="col-sm-10">
								<input type="text" name="children" value="<?php echo $children;?>" placeholder="幼兒" id="input-children" class="form-control" maxlength="1">
								<!--
								<select name="children" id="children" class="form-control">
									<?php for( $i = 0; $i < 4; $i++) :
										$val = str_pad($i, 1, '0', STR_PAD_LEFT) ;
									?>
									<option value="<?=$i?>" <?=( $i == $children ) ? "selected" : "" ?>><?=$i?></option>
									<?php endfor ; ?>
								</select -->
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="baby">嬰兒</label>
							<div class="col-sm-10">
								<input type="text" name="baby" value="<?php echo $baby;?>" placeholder="嬰兒" id="input-baby" class="form-control" maxlength="1">
								<!--
								<select name="baby" id="baby" class="form-control">
									<?php for( $i = 0; $i < 4; $i++) :
										$val = str_pad($i, 1, '0', STR_PAD_LEFT) ;
									?>
									<option value="<?=$i?>" <?=( $i == $baby ) ? "selected" : "" ?>><?=$i?></option>
									<?php endfor ; ?>
								</select -->
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">MEMO</label>
							<div class="col-sm-9">
								<input type="text" name="remark3" id="remark3" class="form-control">
							</div>
							<div class="col-sm-1" style="padding: 7px 0; text-align: left;">
								<a onclick="ins_reamrk('remark3');" class="add-btn" href="javascript:void();">＋</a>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">備註</label>
							<div class="col-sm-10">
								<textarea name="area_memo3" id="area_memo3" cols="100" rows="5" disabled><?php echo (($area_memo=="")?'輸入你想要寫的內容...':$area_memo);?></textarea>
							</div>
						</div>

						<!-- 下一步 按鈕 -->
						<!--<div class="text-right">
							<button type="button" id="button-customer" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><i class="fa fa-arrow-right"></i> <?php echo $button_continue; ?></button>
						</div>-->
					</div>
					<!-- 客戶資訊 End -->
					<!-- 航船資訊 Start -->
					<div class="tab-pane" id="tab-flight">
						<div class="form-group" style="padding:0;">
							<div class="col-sm-2" style="text-align: right;"><span style="padding: 0 5px; border-radius: 3px; color: #fff; background: #1978ab;">去程</span></div>
						</div>
						<div class="form-group" style="border-top: 0; padding: 15px 0 0 0;">
							<?php
								$flightFlag = "";
								$pierFlag = "";
								if($trans_type == 1)
									$flightFlag = "checked";
								else if($trans_type == 2)
									$pierFlag = "checked";
							?>
							<label class="col-sm-2 control-label" for="trans_type">交通工具</label>
							<div class="col-sm-10">
								<input type="radio" name="trans_type" value="1" <?=$flightFlag?>> 飛機　　
								<input type="radio" name="trans_type" value="2" <?=$pierFlag?>> 輪船
							</div>
						</div>
						<div class="form-group" style="border-top: 0; padding: 15px 0 0 0;">
							<label class="col-sm-2 control-label" for="sdate_name">預定起飛時間</label>
							<div class="col-sm-10">
								<div class="date">
									<input type="text" name="s_date" value="<?=$s_date?>" placeholder="起飛時間"
										data-date-format="YYYY-MM-DD" id="s_date" class="flight_date" />
									<span class="input-group-btn" style="float: left; width: auto; margin: 0 10px 0 0;">
										<button type="button" class="btn btn-default flight_date_btn"><i class="fa fa-calendar"></i></button>
									</span>
								</div>
								<select name="s_hour" class="flight_date_select">
										<option value="">請選擇</option>
										<?php for( $i = 0; $i < 24; $i++) :
										$val = str_pad($i, 2, '0', STR_PAD_LEFT) ;
									?>
									<option value="<?=$val?>" <?=( $val == $s_hour) ? "selected" : "" ?>><?=$val?></option>
									<?php endfor ; ?>
								</select> :
								<select name="s_minute" class="flight_date_select">
										<option value="">請選擇</option>
										<?php for( $i = 0; $i < 12; $i++) :
										$val = str_pad($i * 5, 2, '0', STR_PAD_LEFT) ;
									?>
									<option value="<?=$val?>" <?=( $val == $s_minute) ? "selected" : "" ?>><?=$val?></option>
									<?php endfor ; ?>
								</select>
							</div>
						</div>
						<div class="form-group"  style="border-top: 0; padding: 15px 0 0 0;">
							<label class="col-sm-2 control-label" for="e_date">預定到達時間</label>
							<div class="col-sm-10">
								<div class="date">
									<input type="text" name="e_date" value="<?=$e_date?>" placeholder="到達時間"
										data-date-format="YYYY-MM-DD" id="e_date" class="flight_date" />
									<span class="input-group-btn" style="float: left; width: auto; margin: 0 10px 0 0;">
										<button type="button" class="btn btn-default flight_date_btn"><i class="fa fa-calendar"></i></button>
									</span>
								</div>
								<select name="e_hour" class="flight_date_select">
										<option value="">請選擇</option>
										<?php for( $i = 0; $i < 24; $i++) :
										$val = str_pad($i, 2, '0', STR_PAD_LEFT) ;
									?>
									<option value="<?=$val?>" <?=( $val == $e_hour) ? "selected" : "" ?>><?=$val?></option>
									<?php endfor ; ?>
								</select> :
								<select name="e_minute" class="flight_date_select">
										<option value="">請選擇</option>
										<?php for( $i = 0; $i < 12; $i++) :
										$val = str_pad($i * 5, 2, '0', STR_PAD_LEFT) ;
									?>
									<option value="<?=$val?>" <?=( $val == $e_minute) ? "selected" : "" ?>><?=$val?></option>
									<?php endfor ; ?>
								</select>
							</div>
						</div>
						<div class="form-group" style="border-top: 0; padding: 15px 0 0 0;">
							<div id="airport" style="padding:15px 0;display:<?=$airlineShow?>">
								<label class="col-sm-2 control-label" for="airport">出發航站</label>
								<div class="col-sm-10">
									<select name="airport" id="airport" class="form-control">
										<option value=""> --- 請選擇 --- </option>
										<?php foreach ($airportArr as $iCnt => $station) : ?>
										<option value="<?=$station['idx']?>" <?=( $station['idx'] == $airport) ? "selected" : "" ?>><?=$station['opt_name']?></option>
										<?php endforeach ; ?>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group" style="border-top: 0; padding: 0;">
							<div id="airline" style="padding: 15px 0;display:<?=$airlineShow?>">
								<label class="col-sm-2 control-label" for="airline">航空公司</label>
								<div class="col-sm-10">
									<select name="airline" id="airline" class="form-control">
										<option value=""> --- 請選擇 --- </option>
										<?php foreach ($airlineArr as $iCnt => $station) : ?>
										<option value="<?=$station['idx']?>" <?=( $station['idx'] == $airline) ? "selected" : "" ?>><?=$station['opt_name']?></option>
										<?php endforeach ; ?>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group" style="border-top: 0; padding: 0;">
							<div id="pier" style="padding: 15px 0;display:<?=$pierShow?> ">
								<label class="col-sm-2 control-label" for="pier">碼頭出發地</label>
								<div class="col-sm-10">
									<select name="pier" id="pier" class="form-control">
										<option value=""> --- 請選擇 --- </option>
										<?php foreach ($pierArr as $iCnt => $station) : ?>
										<option value="<?=$station['idx']?>" <?=( $station['idx'] == $pier) ? "selected" : "" ?>><?=$station['opt_name']?></option>
										<?php endforeach ; ?>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group" style="border-top: 0; padding: 0 0 15px 0;">
							<div id="flight_no" style="padding: 15px 0;display:<?=$flightnoShow?>">
								<label class="col-sm-2 control-label" for="flight_no">班機編號</label>
								<div class="col-sm-10">
									<input type="text" name="flight_no" value="<?=$flight_no?>" placeholder="班機編號" id="flight_no" class="form-control" />
								</div>
							</div>
						</div>
						<div class="form-group" style="padding:15px 0 0 0;">
							<div class="col-sm-2" style="text-align: right;"><span style="padding: 0 5px; border-radius: 3px; color: #fff; background: #990000;">回程</span></div>
						</div>
						<div class="form-group" style="border-top:0; padding: 15px 0 0 0;">
							<?php
								$flightFlag_rt = "";
								$pierFlag_rt = "";
								if($trans_type_rt == 1)
									$flightFlag_rt = "checked";
								else if($trans_type_rt == 2)
									$pierFlag_rt = "checked";
							?>
							<label class="col-sm-2 control-label" for="trans_type_rt">交通工具</label>
							<div class="col-sm-10">
								<input type="radio" name="trans_type_rt" value="1" <?=$flightFlag_rt?>> 飛機　　
								<input type="radio" name="trans_type_rt" value="2" <?=$pierFlag_rt?>> 輪船
							</div>
						</div>
						<div class="form-group" style="border-top:0; padding: 15px 0 0 0;">
							<label class="col-sm-2 control-label" for="sdate_name">預定起飛時間</label>
							<div class="col-sm-10">
								<div class="date">
									<input type="text" name="s_date_rt" value="<?=$s_date_rt?>" placeholder="起飛時間"
										data-date-format="YYYY-MM-DD" id="s_date_rt" class="flight_date" />
									<span class="input-group-btn" style="float: left; width: auto; margin: 0 10px 0 0;">
										<button type="button" class="btn btn-default flight_date_btn"><i class="fa fa-calendar"></i></button>
									</span>
								</div>
								<select name="s_hour_rt" class="flight_date_select">
										<option value="">請選擇</option>
										<?php for( $i = 0; $i < 24; $i++) :
										$val = str_pad($i, 2, '0', STR_PAD_LEFT) ;
									?>
									<option value="<?=$val?>" <?=( $val == $s_hour_rt) ? "selected" : "" ?>><?=$val?></option>
									<?php endfor ; ?>
								</select> :
								<select name="s_minute_rt" class="flight_date_select">
										<option value="">請選擇</option>
										<?php for( $i = 0; $i < 12; $i++) :
										$val = str_pad($i * 5, 2, '0', STR_PAD_LEFT) ;
									?>
									<option value="<?=$val?>" <?=( $val == $s_minute_rt) ? "selected" : "" ?>><?=$val?></option>
									<?php endfor ; ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">MEMO</label>
							<div class="col-sm-9">
								<input type="text" name="remark4" id="remark4" class="form-control">
							</div>
							<div class="col-sm-1" style="padding: 7px 0; text-align: left;">
								<a onclick="ins_reamrk('remark4');" class="add-btn" href="javascript:void();">＋</a>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">備註</label>
							<div class="col-sm-10">
								<textarea name="area_memo4" id="area_memo4" cols="100" rows="5" disabled><?php echo (($area_memo=="")?'輸入你想要寫的內容...':$area_memo);?></textarea>
							</div>
						</div>

						<!--
						<div class="form-group" style="border-top:0; padding:15px 0 0 0;">
							<div id="airport_rt" style="display:<?=$airlineShow_rt?>">
								<label class="col-sm-2 control-label" for="airport_rt">出發航站</label>
								<div class="col-sm-10">
									<select name="airport_rt" id="airport_rt" class="form-control">
										<option value=""> --- 請選擇 --- </option>
										<?php foreach ($airportArr_rt as $iCnt => $station) : ?>
										<option value="<?=$station['idx']?>" <?=( $station['idx'] == $airport) ? "selected" : "" ?>><?=$station['opt_name']?></option>
										<?php endforeach ; ?>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group" style="border-top: 0; padding: 0;">
							<div id="airline_rt" style="padding: 15px 0;display:<?=$airlineShow_rt?> ">
								<label class="col-sm-2 control-label" for="airline_rt">航空公司</label>
								<div class="col-sm-10">
									<select name="airline_rt" id="airline_rt" class="form-control">
										<option value=""> --- 請選擇 --- </option>
										<?php foreach ($airlineArr_rt as $iCnt => $station) : ?>
										<option value="<?=$station['idx']?>" <?=( $station['idx'] == $airline) ? "selected" : "" ?>><?=$station['opt_name']?></option>
										<?php endforeach ; ?>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group" style="border-top: 0; padding: 0;">
							<div id="pier_rt" style="display:<?=$pierShow_rt?>">
								<label class="col-sm-2 control-label" for="pier_rt">碼頭出發地</label>
								<div class="col-sm-10">
									<select name="pier_rt" id="pier_rt" class="form-control">
										<option value=""> --- 請選擇 --- </option>
										<?php foreach ($pierArr_rt as $iCnt => $station) : ?>
										<option value="<?=$station['idx']?>" <?=( $station['idx'] == $pier_rt) ? "selected" : "" ?>><?=$station['opt_name']?></option>
										<?php endforeach ; ?>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group" style="border-top: 0; padding: 0;">
							<div id="flight_no_rt" style="padding: 15px 0 15px 0;display:<?=$flightnoShow_rt?> ">
								<label class="col-sm-2 control-label" for="flight_no_rt">班機編號</label>
								<div class="col-sm-10">
									<input type="text" name="flight_no_rt" value="<?=$flight_no_rt?>" placeholder="班機編號" id="flight_no_rt" class="form-control" />
								</div>
							</div>
						</div> -->

					</div>
					<!-- 航船資訊 End -->
					<div class="tab-pane" id="tab-ExceptionalLoss"><!-- tab-ExceptionalLoss 5 損失賠償 start -->
						<div class="form-group">
							<label class="col-sm-2 control-label" for="flight_no">油費</label>
							<div class="col-sm-10">
								<input type="text" name="oil_cost" value="<?=$oil_cost?>" class="form-control" placeholder="油費"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="flight_no">逾時費用</label>
							<div class="col-sm-10">
								<input type="text" name="overTime_cost" value="<?=$overTime_cost?>" class="form-control" placeholder="逾時費用"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="flight_no">營業損失</label>
							<div class="col-sm-10">
								<input type="text" name="business_lose" value="<?=$business_lose?>" class="form-control" placeholder="營業損失"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="flight_no">車體損失</label>
							<div class="col-sm-10">
								<input type="text" name="car_lose" value="<?=$car_lose?>" class="form-control" placeholder="車體損失"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="flight_no">其他加收</label>
							<div class="col-sm-10">
								<input type="text" name="other_lose" value="<?=$other_lose?>" class="form-control" placeholder="其他損失"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="lose_type">費用支付方式</label>
							<div class="col-sm-10">
								<input type="radio" name="lose_type" value="1" <?php echo (($lose_type==1)?'checked':'')?>> 應收
								<input type="radio" name="lose_type" value="2" <?php echo (($lose_type==2)?'checked':'')?>> 已收
								<input type="radio" name="lose_type" value="3" <?php echo (($lose_type==3)?'checked':'')?>> 未收
								<input type="radio" name="lose_type" value="4" <?php echo (($lose_type==4)?'checked':'')?>> 現金
								<input type="radio" name="lose_type" value="5" <?php echo (($lose_type==5)?'checked':'')?>> 信用卡
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">MEMO</label>
							<div class="col-sm-9">
								<input type="text" name="remark5" id="remark5" class="form-control">
							</div>
							<div class="col-sm-1" style="padding: 7px 0; text-align: left;">
								<a onclick="ins_reamrk('remark5');" class="add-btn" href="javascript:void();">＋</a>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">備註</label>
							<div class="col-sm-10">
								<textarea name="area_memo5" id="area_memo5" cols="100" rows="5" disabled><?php echo (($area_memo=="")?'輸入你想要寫的內容...':$area_memo);?></textarea>
							</div>
						</div>

					</div><!-- tab-ExceptionalLoss 5. 損失賠償 end -->
				</div>
			</form>
		</div>
	</div>
</div>
<style type="text/css">
	.bt-background-clean {
		color: #000000 ;
		background-color: #FFFFFF ;
	}
</style>

<script type="text/javascript"><!--
// Disable the tabs
// $('#order a[data-toggle=\'tab\']').on('click', function(e) {
// 	return false;
// });
	$(document).ready(function() {
		var error_warning = '<?php echo $error_warning;?>';
		if(error_warning != ''){
			check_model_car();
		}
	});

// add by Angus 2018.08.08
function isTimeSeted() {
	// console.log( $('input[name=\'rent_date_out\']').val());
	// console.log( $('select[name=\'out_time\']').val());
	// console.log( $('input[name=\'rent_date_in_plan\']').val());
	// console.log( $('select[name=\'plan_time\']').val());
	var msg = "" ;
	if ( $('input[name=\'rent_date_out\']').val() == "" || $('select[name=\'out_time\']').val() == "") {
		msg += "預計取車時間 未設定\n" ;
	}
	if ( $('input[name=\'rent_date_in_plan\']').val() == "" || $('select[name=\'plan_time\']').val() == "") {
		msg += "預計還車時間 未設定\n" ;
	}
	return msg ;
}

// add by Angus 2018.08.08
function getCarModels() {
	var retStr = "" ;
	$("input[name='selCarModels[]']").each( function() {
		// console.log( $(this).is(':checked')) ;
		if ( $(this).is(':checked')) {
			retStr += $(this).val() + "|" ;
		}
	});
	// console.log( retStr.length) ;
	if ( retStr.length > 0) {
		retStr = retStr.substring(0, retStr.length-1) ;
	}
	return retStr ;
}

//Jessie 2019/12/02
function getCarTypes() {
	var retStr = "" ;
	$("input[name='selCarTypes[]']").each( function() {
		// console.log( $(this).is(':checked')) ;
		if ( $(this).is(':checked')) {
			retStr += $(this).val() + "|" ;
		}
	});
	// console.log( retStr.length) ;
	if ( retStr.length > 0) {
		retStr = retStr.substring(0, retStr.length-1) ;
	}
	return retStr ;
}

function getuseCars() {
	var retStr = "" ;
	$("input[name='useCars[]']").each( function() {
		// console.log( $(this).is(':checked')) ;
		if ( $(this).is(':checked')) {
			retStr += $(this).val() + "|" ;
		}
	});
	// console.log( retStr.length) ;
	if ( retStr.length > 0) {
		retStr = retStr.substring(0, retStr.length-1) ;
	}
	return retStr ;
}

// add by Angus 2018.08.09
function reLoadCarTypeJs() {
	//console.log( 'in function reLoadCarTypeJs ') ;
	//console.log( '?route=sale/order/forOrderGetCarTypeJs&token=<?php echo $token; ?>');
	$.ajax({
		url : '?route=sale/order/forOrderGetCarTypeJs&token=<?php echo $token; ?>',
		dataType: 'html',
		success: function(html) {
			//console.log(html);
			$('#showJs').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
}

// add by Angus 2018.08.08
$("input[name='selCarModels[]']").click( function(event) {
	var pObj = $(this).parent() ;
	// 當勾選成立 檢查是否有填日期 沒有填日期就取消勾選
	if ($(this).is(':checked')) {
		if ( msg = isTimeSeted()) {
			alert(msg) ;
			$(this).prop('checked', false) ;
		}
	}

	if ($(this).is(':checked')) {
		pObj.removeClass('bt-background-clean') ;
	} else {
		pObj.addClass('bt-background-clean') ;
	}

	var carModelStr = getCarModels() ;
	//console.log( "選擇車種 : " + carModelStr) ;
	//目前車型
	var CarTypesStr = getCarTypes() ;
	var cartype_arr = CarTypesStr.split('|');
	//目前牌照(要排除目前牌照)
	var useCarsStr = getuseCars() ;
	if ( carModelStr != '') {
		var sDate = $('input[name=\'rent_date_out\']').val() + " " + $('select[name=\'out_time\']').val() ;
		var eDate = $('input[name=\'rent_date_in_plan\']').val() + " " + $('select[name=\'plan_time\']').val() ;
		// var sDate = "2018-08-09 07:30" ;
		// var eDate = "2018-08-11 18:30" ;
		var ajaxUrl = '?route=site/scheduling/getCarModelList&token=<?php echo $token; ?>&car_model=' + carModelStr + '&s=' + sDate + '&e=' + eDate+'&useCars='+useCarsStr;
		//console.log( ajaxUrl);
		$.ajax({
			url : ajaxUrl,
			dataType: 'json',
			success: function(json) {
				var html = "" ;
				for (i = 0; i < json.length; i++) {
					if(cartype_arr.indexOf(json[i]['idx']) !== -1){
						html += "<label name=\"carTypes\" class=\"btn btn-success\"><i class=\"fa fa-car\"></i>&nbsp;"+json[i]['car_seed']+"<input class=\"hidden\" type=\"checkbox\" name=\"selCarTypes[]\" value=\""+json[i]['idx']+"\" checked /></label>&nbsp&nbsp" ;
					}else{
						html += "<label name=\"carTypes\" class=\"btn btn-success bt-background-clean\"><i class=\"fa fa-car\"></i>&nbsp;"+json[i]['car_seed']+"<input class=\"hidden\" type=\"checkbox\" name=\"selCarTypes[]\" value=\""+json[i]['idx']+"\" /></label>&nbsp&nbsp" ;
					}
				}
				if(html == ""){
					$('#showCarType').html('<span style="color:red;display: inline-block;padding-top: 9px;font-weight: bold;">所選日期已無車輛</span>');
				}else{
					$('#showCarType').html(html);
				}
				//$('#showArea').html('') ;
				reLoadCarTypeJs() ;
				//更新訂車數量
				update_carnum();
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
		$.ajax({
			url : '?route=sale/order/calculationDays&token=<?php echo $token; ?>' + '&s=' + sDate + '&e=' + eDate,
			dataType: 'json',
			success: function(json) {
				//console.log( json) ;
				$('#rentDay').html( json[0]) ;
				$('#rentHour').html( json[1]) ;
				$('#rentDay1').html( json[0]) ;
				$('#rentHour1').html( json[1]) ;
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	} else {
		$('#showCarType').html('') ;
		$('#showArea').html('') ;
		//更新訂車數量
		update_carnum();

	}
});

// add by Angus 2018.08.11
function getCarTypes() {
	var retStr = "" ;
	$("input[name='selCarTypes[]']").each( function() {
		// console.log( $(this).is(':checked')) ;
		if ( $(this).is(':checked')) {
			retStr += $(this).val() + "|" ;
		}
	});
	// console.log( retStr.length) ;
	if ( retStr.length > 0) {
		retStr = retStr.substring(0, retStr.length-1) ;
	}
	return retStr ;
}

// add by Angus 2018.08.11
$("input[name='selCarTypes[]']").click( function(event) {
	//目前牌照(要排除目前牌照)
	var useCarsStr = getuseCars() ;

	var sDate = $('input[name=\'rent_date_out\']').val() + " " + $('select[name=\'out_time\']').val() ;
	var eDate = $('input[name=\'rent_date_in_plan\']').val() + " " + $('select[name=\'plan_time\']').val() ;
	var order_id = $('input[name=\'order_id\']').val() ;
	//var sDate    = "2018-08-09 07:30" ;
	//var eDate    = "2018-08-11 18:30" ;
	var pObj = $(this).parent() ;
	var clean_sn = "";
	if ($(this).is(':checked')) {
		clean_sn = $(this).val();
		pObj.removeClass('bt-background-clean') ;
	} else {
		pObj.addClass('bt-background-clean') ;
	}
	var carTypeStr = getCarTypes() ;
	//console.log( carTypeStr) ;
	// var ajaxUrl = "?route=sale/order/autocompleteForCarNo&token={$this->session->data['token']}&car_seed="+carTypeStr+"&s="+sDate+"&e="+eDate+"&idx="+order_id ;
	var ajaxUrl = "?route=sale/order/autocompleteForCarNo&token=<?php echo $token; ?>&car_seed="+carTypeStr+"&s="+sDate+"&e="+eDate+"&useCars="+useCarsStr+"&idx="+order_id+"&clean_sn="+clean_sn ;
	//console.log( ajaxUrl) ;
	if ( carTypeStr != '') {
		$.ajax({
			url: ajaxUrl,
			dataType: 'html',
			success: function(html) {
				//console.log( html);
				$('#showArea').html( html) ;
				//更新訂車數量
				update_carnum();

				//再宣告一次？
			},
			error: function(xhr, ajaxOptions, thrownError) {
			//console.log( thrownError);
			//console.log( ajaxOptions);
			//console.log( xhr);
		}
		});
	} else {
		$('#showArea').html( '') ;
		//更新訂車數量
		update_carnum();

	}
}) ;
$("input[name='rent_date_out'],input[name='rent_date_in_plan'],[name='out_time'],[name='plan_time']").click( function () {
	change_day();
}) ;
$("[name='out_time'], [name='plan_time']").change(function(event) {
	change_day();
});

function change_day(){
	// console.log('func change_day');
	var d1 = $("input[name='rent_date_out']").val() ;
	var d2 = $("input[name='rent_date_in_plan']").val() ;
	var t1 = $('select[name=\'out_time\'] option:selected').val() ;
	var t2 = $('select[name=\'plan_time\'] option:selected').val() ;

	var sDate = d1+'T'+t1;
	var eDate = d2+'T'+t2;
	var strMsg = '';

	var oneDayHour = checkOneDay ( sDate, eDate) ;
	// console.log( ['func change_day', 'checkOneDay', oneDayHour]);
	var oneDay = 0;
	var oneHour = 0;
	var allDay = 0;
	if(oneDayHour > 0){
		oneDay = oneDayHour/ 24;
		oneDay = parseInt(oneDay);
		Math.floor(oneDay);
		oneHour = oneDayHour%24;
	}
	parseInt(oneHour);
	oneHour = Math.round(oneHour);  // 4捨五入

	//未滿1天要算1天
	if(oneDay == 0){
		oneDay = 1;
		oneHour = 0;
	}

	if ( !isNaN( oneDayHour)) {
		$("#rentDay").html(oneDay);
		$("#rentHour").html(oneHour);
		$("#rentDay1").html(oneDay);
		$("#rentHour1").html(oneHour);
	}

	//檢查車種是否已選 已選要更新一下機型 2019/12/24
	if(oneDay>0 || oneHour>0){
		check_model_car();
	}
	change_price();
}
function check_model_car(){
	//目前車型
	var CarTypesStr = getCarTypes() ;
	var cartype_arr = CarTypesStr.split('|');

	var carModelStr = getCarModels() ;
	//console.log( "選擇車種 : " + carModelStr) ;
	var useCarsStr = getuseCars() ;
	if ( carModelStr != '') {
		var sDate = $('input[name=\'rent_date_out\']').val() + " " + $('select[name=\'out_time\']').val() ;
		var eDate = $('input[name=\'rent_date_in_plan\']').val() + " " + $('select[name=\'plan_time\']').val() ;
		// var sDate = "2018-08-09 07:30" ;
		// var eDate = "2018-08-11 18:30" ;
		var ajaxUrl = '?route=site/scheduling/getCarModelList&token=<?php echo $token; ?>&car_model=' + carModelStr + '&s=' + sDate + '&e=' + eDate+'&useCars='+useCarsStr;
		//console.log( ajaxUrl);
		$.ajax({
			url : ajaxUrl,
			dataType: 'json',
			success: function(json) {
				var html = "" ;
				for (i = 0; i < json.length; i++) {
					if(cartype_arr.indexOf(json[i]['idx']) !== -1){
						html += "<label name=\"carTypes\" class=\"btn btn-success\"><i class=\"fa fa-car\"></i>&nbsp;"+json[i]['car_seed']+"<input class=\"hidden\" type=\"checkbox\" name=\"selCarTypes[]\" value=\""+json[i]['idx']+"\" checked /></label>&nbsp&nbsp" ;
					}else{
						html += "<label name=\"carTypes\" class=\"btn btn-success bt-background-clean\"><i class=\"fa fa-car\"></i>&nbsp;"+json[i]['car_seed']+"<input class=\"hidden\" type=\"checkbox\" name=\"selCarTypes[]\" value=\""+json[i]['idx']+"\" /></label>&nbsp&nbsp" ;
					}
				}
				if(html == ""){
					$('#showCarType').html('<span style="color:red;display: inline-block;padding-top: 9px;font-weight: bold;">所選日期已無車輛</span>');
				}else{
					$('#showCarType').html(html);
				}
				//$('#showArea').html('') ;
				reLoadCarTypeJs() ;
				//更新訂車數量
				update_carnum();
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
		$.ajax({
			url : '?route=sale/order/calculationDays&token=<?php echo $token; ?>' + '&s=' + sDate + '&e=' + eDate,
			dataType: 'json',
			success: function(json) {
				//console.log( json) ;
				$('#rentDay').html( json[0]) ;
				$('#rentHour').html( json[1]) ;
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	} else {
		$('#showCarType').html('') ;
		$('#showArea').html('') ;
		//更新訂車數量
		update_carnum();
	}
}
//檢查選的天數幾天幾小時
function checkOneDay( sDate, eDate) {
	// console.log( ['checkOneDay', sDate, eDate]);
	var currDate = "" ;
	var txtDate = "" ;
	if ( eDate == undefined) {
		// console.log( [ 'checkOneDay', 'eDate == undefined']);
		currDate = Date.parse((new Date()).toDateString()+' '+(new Date()).toTimeString());
		txtDate = Date.parse(sDate);
	} else {
		// console.log( [ 'checkOneDay', 'eDate != undefined']);
		currDate = Date.parse(sDate);
		txtDate = Date.parse(eDate);
	}
	// console.log( ['checkOneDay', currDate, txtDate]);
	var ONE_HOUR = 1000 * 60 * 60 ; // 毫秒 x 60個秒 x 60個分
	return  (txtDate - currDate) / ONE_HOUR ;
}

$("label[name='useCar']").click( function () {
	console.log("label[name='useCar'].click");
	var agreement_no = $('input[name="agreement_no"]').val();
	var rentDay =$('#rentDay').html() ;
	var rentHour =$('#rentHour').html() ;
	rentDay = parseFloat(rentDay);
	rentHour = parseFloat(rentHour);
	if(rentHour>0){
		rentDay++;
	}

	// var classStr = $(this).attr('class') ;
	// console.log( classStr);
	var ckbObj = $(this).find("input[type='checkbox']")
	//console.log( ckbObj.is(':checked')) ;
	if ( ckbObj.is(':checked')) {
		$(this).removeClass('bt-background-clean') ;
	} else {
		$(this).addClass('bt-background-clean') ;
	}
	//車損金額 未上線
	var business_lose = $('input[name="business_lose"]').val();
	var car_lose = $('input[name="car_lose"]').val();

	var order_type=$('select[name="order_type"]').val();
	//改全部訂單都要即時計算金額 2019/09/25
	//if(order_type == '140'){
		var c_num = 0;
		var c_price = 0;
		$('input[name^="useCars"]').each(function() {
			if($(this).parent().attr("class").indexOf("clean")===-1){
				c_price += parseInt($(this).attr("p2"));
				c_num ++;
			}
		});

		c_price = parseInt(c_price);
		//mobile_moto 50元
		//baby_chair1/baby_chair2/children_chair 100元
		//gps 100元
		c_price += parseInt($("#baby_chair1").val())*100;
		c_price += parseInt($("#baby_chair2").val())*100;
		c_price += parseInt($("#children_chair").val())*100;
		// c_price += parseInt($("#gps").val())*100; 不加了
		//console.log(c_num+" "+c_price);
		if(business_lose != ""){
			c_price += parseInt(business_lose);
		}
		if(car_lose != ""){
			c_price += parseInt(car_lose);
		}
		c_price = c_price*rentDay;

		if(rentDay > 1){
			c_price += parseInt($("#mobile_moto").val())*100;
			c_price += parseInt($("#mobile_moto").val())*50*(rentDay-1);
		}else{
			c_price += parseInt($("#mobile_moto").val())*100;
		}


		//檢查SPDC有沒有值
		var spdc = $('input[name="spdc"]').val();
		parseInt(spdc);
		if(spdc == ""){
			spdc = 0;
		}
		c_price -= spdc;

		$("#t_money").html(c_price);
		$("#total").val(c_price);
		if(agreement_no == ''){
			//計算牌照數量
			$("#total_car").val(c_num);
		}


	//}
}) ;

function update_carnum(){
	var agreement_no = $('input[name="agreement_no"]').val();
	var c_num = 0;
	$('input[name^="useCars"]').each(function() {
		if($(this).parent().attr("class").indexOf("clean")===-1){
			c_num ++;
		}
	});
	// console.log('c_num='+c_num);

	if(agreement_no == ''){
		//計算牌照數量
		$("#total_car").val(c_num);
	}
}

//配件更新要更新價格
$("input[name='mobile_moto'],input[name='baby_chair1'],input[name='baby_chair2'],input[name='children_chair'],input[name='gps']").change( function () {
	change_price();
}) ;
$("input[name='spdc'],input[name='business_lose'],input[name='car_lose']").blur( function () {
	change_price();
}) ;

$("input[name='adult'],input[name='children'],input[name='baby'],input[name='baby_chair1'],input[name='baby_chair2'],input[name='children_chair'],input[name='gps'],input[name='mobile_moto']").keyup( function (value) {
	if(this.value==""){
		this.value= 0;
	}

	this.value=this.value.replace(/\D/g,"0")
}) ;


function change_price(){
	// console.log( 'func change_price');
	var agreement_no = $('input[name="agreement_no"]').val();
	var rentDay 	= parseFloat( $('#rentDay').html()) ;
	var rentDay4Acc = parseFloat( $('#rentDay').html()) ;
	var rentHour 	= parseFloat( $('#rentHour').html()) ;

	if(rentHour>6){
		rentDay++;
	}

	var business_lose	= $('input[name="business_lose"]').val();
	var car_lose		= $('input[name="car_lose"]').val();
	var order_types 	= $('select[name="order_type"]').val();
	//if(order_type == '140'){
		var c_num = 0;
		var c_price = 0;
		$('input[name^="useCars"]').each(function() {
			if($(this).parent().attr("class").indexOf("clean")===-1){
				c_price += parseInt($(this).attr("p2"));
				c_num ++;
			}
		});
		c_price = parseInt(c_price);
		//mobile_moto 50元
		//gps 100元
		//baby_chair1/baby_chair2/children_chair 100元
		// c_price += parseInt($("#baby_chair1").val())*100;
		// c_price += parseInt($("#baby_chair2").val())*100;
		// c_price += parseInt($("#children_chair").val())*100;
		// c_price += parseInt($("#gps").val())*100;
		var plug_total = 0 ; // 配件費用
		plug_total += parseInt($("#baby_chair1").val())*100;
		plug_total += parseInt($("#baby_chair2").val())*100;
		plug_total += parseInt($("#children_chair").val())*100;
		// plug_total += parseInt($("#gps").val())*100;
		plug_total += parseInt($("#mobile_moto").val())*50;

		if ( rentHour > 0) {
			plug_total = plug_total * (rentDay4Acc+1) ;
		} else {
			plug_total = plug_total * rentDay4Acc ;
		}

		var add_price = 0;
		parseInt(add_price);
		if(rentHour >=1 && rentHour <=6){
			rentHour =  Math.ceil(rentHour);
			add_price =  Math.ceil(c_price * 0.1) * rentHour ;
		}
		c_price = rentDay * c_price + add_price; //依天數要倍數計算
		c_price += plug_total ; // 配件加收部份

		// 加收費用部份
		if(business_lose != ""){
			c_price += parseInt(business_lose);
		}
		if(car_lose != ""){
			c_price += parseInt(car_lose);
		}

		// if(rentDay > 1){
		// 	c_price += parseInt($("#mobile_moto").val())*100;
		// 	c_price += parseInt($("#mobile_moto").val())*50*(rentDay-1);
		// }else{
		// 	c_price += parseInt($("#mobile_moto").val())*100;
		// }


		//檢查SPDC有沒有值
		var spdc = $('input[name="spdc"]').val();
		parseInt(spdc);
		if(spdc == ""){
			spdc = 0;
		}
		c_price -= spdc;

		$("#t_money").html(c_price);
		$("#total").val(c_price);

		if(agreement_no == ''){
			//計算牌照數量
			$("#total_car").val(c_num);
		}
	//}
}

// 處理訂單類別 如果是選擇民宿業者 需要把民宿的名稱帶上
$("select[name='order_type']").change( function () {
	var orderType = $(this).val() ;
	// console.log('order_type : '+orderType) ;
	if ( orderType == 10 || orderType == 130) {
		$('#minsu_area').show() ;
	} else {
		$('#minsu_area').show() ;
	}
}) ;

$('input[name=\'minsu_name\']').autocomplete({
	'source': function(request, response) {
		// console.log( "request :" + request) ;
		// console.log( "response :" + response) ;
		$.ajax({
			url: '?route=site/scheduling/getAjaxMinsu&token=<?=$token?>&minsuName='+encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				// console.log( "json : " + json) ;
				response($.map(json, function(item) {
					// console.log( "item : " + item) ;
					return {
						label: item['name'],
						value: item['code']
					}
				}));
			}
		});
	},
	'select': function(item) {
		// console.log( item) ;
		$('input[name=\'minsu_name\']').val(item['label']) ;
		$('input[name=\'minsu_idx\']').val(item['value']);
	}
});

// 處理交通工具 分為 飛機 輪船
$("input[name='trans_type']").change( function () {
	var transType = $(this).val() ;
	// console.log('trans_type : '+transType) ;
	if ( transType == 1) {	// 飛機
		$('#airport').show() ;
		$('#airline').show() ;
		$('#flight_no').show() ;
		$('#pier').hide() ;
		$('#sdate_name').text("預定起飛時間");
	} else {				// 輪船
		$('#airport').hide() ;
		$('#airline').hide() ;
		$('#flight_no').hide() ;
		$('#pier').show() ;
		$('#sdate_name').text("預定出發時間");
	}
}) ;

// 處理交通工具 分為 飛機 輪船
$("input[name='trans_type_rt']").change( function () {
	var transType = $(this).val() ;
	// console.log('trans_type_rt : '+transType) ;
	if ( transType == 1) {	// 飛機
		$('#airport_rt').show() ;
		$('#airline_rt').show() ;
		$('#flight_no_rt').show() ;
		$('#pier_rt').hide() ;
		$('#sdate_name_rt').text("預定起飛時間");
	} else {				// 輪船
		$('#airport_rt').hide() ;
		$('#airline_rt').hide() ;
		$('#flight_no_rt').hide() ;
		$('#pier_rt').show() ;
		$('#sdate_name_rt').text("預定出發時間");
	}
}) ;

/**
 * 同租車人資訊
 * @Another Angus
 * @date    2018-02-12
 */
$("input[name='syncrent']").click( function () {
	if ($(this).is(":checked")) {
		$( "input[name='get_user_name']" ).val( $( "input[name='rent_user_name']").val()) ;
		$( "input[name='get_user_id']" ).val( $( "input[name='rent_user_id']").val()) ;
		$( "input[name='get_user_born']" ).val( $( "input[name='rent_user_born']").val()) ;
		$( "input[name='get_user_tel']" ).val( $( "input[name='rent_user_tel']").val()) ;
	} else {
		$( "input[name='get_user_name']" ).val( "") ;
		$( "input[name='get_user_id']" ).val( "") ;
		$( "input[name='get_user_born']" ).val( "") ;
		$( "input[name='get_user_tel']" ).val( "") ;
	}
}) ;

// 郵政區號
$("select[name='rent_zip1']").change( function () {
	var selVal = $(this).val() ;
	$('#rent_zip2').find('option:not(:first)').remove() ;
	console.log( selVal) ;
	$.ajax({
		url : '?route=site/scheduling/getSubZip&token=<?=$token?>',
		type : 'get',
		data : {zipMain : selVal},
		dataType : 'json',
		success : function (resp) {
			console.log(resp) ;
			for (var i = 0; i < resp.length; i++) {
				$("#rent_zip2").append('<option value=' + resp[i].zip_code + '>' + resp[i].zip_code + ' ' + resp[i].zip_name + '</option>') ;
			}
		}
	});
});

// datetimepicker setting
$('.date').datetimepicker({
	pickTime: false
});

$('.date').datetimepicker().on('dp.change', function (event) {
	change_day();
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});

$('#save_submit').click(function(){
	var order_type   = '<?php echo $order_type;?>';
	var car_total    = $("#total_car").val();
	var agreement_no = $('input[name="agreement_no"]').val();

	var c_num = 0;
	var flag = 'Y';
	if(car_total != 0){
		//檢查編輯時原先選取數量與車量是否一致
		$('input[name^="useCars"]').each(function() {
			if($(this).parent().attr("class").indexOf("clean")===-1){
				c_num ++;
			}
		});

		if(c_num != car_total && agreement_no != ""){
			alert('所選牌照號碼數量與訂車數量不一致！');
			flag = 'N';
			return false;
		}
	}

	if(order_type == '140'){
		//如果付款類型有選擇時，把收費方式改成已收
		var pay_type = $('input:radio[name="pay_type"]:checked').val();
		if(pay_type != undefined){
			$('input:radio[name="charge_method"][value=3]').attr('checked', 'checked');
		}
	}
	var pay_type = $('input:radio[name="pay_type"]:checked').val();
	if(pay_type == undefined){
		flag= 'N';
		alert("付款類型必填！");
		return false;
	}

	if(flag == 'Y'){
		$("#area_memo").attr("disabled", false);
		$("#save_submit").attr("type","submit");
	}


})

$("select[name*='s_station']").change( function () {
	var html = "" ;
	if ( $(this).val() == 16) {
		html = '<option value="">請選擇</option><option value="08:30">08:30</option><option value="09:00">09:00</option><option value="09:30">09:30</option><option value="10:00">10:00</option><option value="10:30">10:30</option><option value="11:00">11:00</option><option value="11:30">11:30</option><option value="12:00">12:00</option><option value="12:30">12:30</option><option value="13:00">13:00</option><option value="13:30">13:30</option><option value="14:00">14:00</option><option value="14:30">14:30</option><option value="15:00">15:00</option><option value="15:30">15:30</option><option value="16:00">16:00</option><option value="16:30">16:30</option><option value="17:00">17:00</option><option value="17:30">17:30</option><option value="18:00">18:00</option>';
	} else if ( $(this).val() == 17) {
		html = '<option value="">請選擇</option><option value="08:30">08:30</option><option value="09:00">09:00</option><option value="09:30">09:30</option><option value="10:00">10:00</option><option value="10:30">10:30</option><option value="11:00">11:00</option><option value="11:30">11:30</option><option value="12:00">12:00</option><option value="12:30">12:30</option><option value="13:00">13:00</option><option value="13:30">13:30</option><option value="14:00">14:00</option><option value="14:30">14:30</option><option value="15:00">15:00</option><option value="15:30">15:30</option><option value="16:00">16:00</option><option value="16:30">16:30</option><option value="17:00">17:00</option><option value="17:30">17:30</option>';
	} else if ( $(this).val() == 18) {
		html = '<option value="">請選擇</option><option value="08:00">08:00</option><option value="08:30">08:30</option><option value="09:00">09:00</option><option value="09:30">09:30</option><option value="10:00">10:00</option><option value="10:30">10:30</option><option value="11:00">11:00</option><option value="11:30">11:30</option><option value="12:00">12:00</option><option value="12:30">12:30</option><option value="13:00">13:00</option><option value="13:30">13:30</option><option value="14:00">14:00</option><option value="14:30">14:30</option><option value="15:00">15:00</option><option value="15:30">15:30</option><option value="16:00">16:00</option><option value="16:30">16:30</option><option value="17:00">17:00</option><option value="17:30">17:30</option><option value="18:00">18:00</option><option value="18:30">18:30</option>';
	} else {
		html = '<option value="">請選擇</option><option value="08:30">08:30</option><option value="09:00">09:00</option><option value="09:30">09:30</option><option value="10:00">10:00</option><option value="10:30">10:30</option><option value="11:00">11:00</option><option value="11:30">11:30</option><option value="12:00">12:00</option><option value="12:30">12:30</option><option value="13:00">13:00</option><option value="13:30">13:30</option><option value="14:00">14:00</option><option value="14:30">14:30</option><option value="15:00">15:00</option><option value="15:30">15:30</option><option value="16:00">16:00</option><option value="16:30">16:30</option><option value="17:00">17:00</option><option value="17:30">17:30</option><option value="18:00">18:00</option>';
	}
	$('select[name=\'out_time\']').html(html);
});

$("select[name*='e_station']").change( function () {
	var html = "" ;
	if ( $(this).val() == 16) {
		html = '<option value="">請選擇</option><option value="08:30">08:30</option><option value="09:00">09:00</option><option value="09:30">09:30</option><option value="10:00">10:00</option><option value="10:30">10:30</option><option value="11:00">11:00</option><option value="11:30">11:30</option><option value="12:00">12:00</option><option value="12:30">12:30</option><option value="13:00">13:00</option><option value="13:30">13:30</option><option value="14:00">14:00</option><option value="14:30">14:30</option><option value="15:00">15:00</option><option value="15:30">15:30</option><option value="16:00">16:00</option><option value="16:30">16:30</option><option value="17:00">17:00</option><option value="17:30">17:30</option><option value="18:00">18:00</option>';
	} else if ( $(this).val() == 17) {
		html = '<option value="">請選擇</option><option value="08:30">08:30</option><option value="09:00">09:00</option><option value="09:30">09:30</option><option value="10:00">10:00</option><option value="10:30">10:30</option><option value="11:00">11:00</option><option value="11:30">11:30</option><option value="12:00">12:00</option><option value="12:30">12:30</option><option value="13:00">13:00</option><option value="13:30">13:30</option><option value="14:00">14:00</option><option value="14:30">14:30</option><option value="15:00">15:00</option><option value="15:30">15:30</option><option value="16:00">16:00</option><option value="16:30">16:30</option><option value="17:00">17:00</option><option value="17:30">17:30</option>';
	} else if ( $(this).val() == 18) {
		html = '<option value="">請選擇</option><option value="08:00">08:00</option><option value="08:30">08:30</option><option value="09:00">09:00</option><option value="09:30">09:30</option><option value="10:00">10:00</option><option value="10:30">10:30</option><option value="11:00">11:00</option><option value="11:30">11:30</option><option value="12:00">12:00</option><option value="12:30">12:30</option><option value="13:00">13:00</option><option value="13:30">13:30</option><option value="14:00">14:00</option><option value="14:30">14:30</option><option value="15:00">15:00</option><option value="15:30">15:30</option><option value="16:00">16:00</option><option value="16:30">16:30</option><option value="17:00">17:00</option><option value="17:30">17:30</option><option value="18:00">18:00</option><option value="18:30">18:30</option>';
	} else {
		html = '<option value="">請選擇</option><option value="08:30">08:30</option><option value="09:00">09:00</option><option value="09:30">09:30</option><option value="10:00">10:00</option><option value="10:30">10:30</option><option value="11:00">11:00</option><option value="11:30">11:30</option><option value="12:00">12:00</option><option value="12:30">12:30</option><option value="13:00">13:00</option><option value="13:30">13:30</option><option value="14:00">14:00</option><option value="14:30">14:30</option><option value="15:00">15:00</option><option value="15:30">15:30</option><option value="16:00">16:00</option><option value="16:30">16:30</option><option value="17:00">17:00</option><option value="17:30">17:30</option><option value="18:00">18:00</option>';
	}
	$('select[name=\'plan_time\']').html(html);
});
function ins_reamrk(field){
	var user_allname = '<?=$nowname?>';

	var remark = $("#"+field).val();
	var area_memo = $("#area_memo").val();
	if(remark != ""){
		remark = remark+' '+user_allname;
		if(area_memo == "輸入你想要寫的內容..."){
			$("#area_memo").val(remark);
			$("#area_memo1").val(remark);
			$("#area_memo2").val(remark);
			$("#area_memo3").val(remark);
			$("#area_memo4").val(remark);
			$("#area_memo5").val(remark);
		}else{
			$("#area_memo").val(area_memo+"\r\n"+remark);
			$("#area_memo1").val(area_memo+"\r\n"+remark);
			$("#area_memo2").val(area_memo+"\r\n"+remark);
			$("#area_memo3").val(area_memo+"\r\n"+remark);
			$("#area_memo4").val(area_memo+"\r\n"+remark);
			$("#area_memo5").val(area_memo+"\r\n"+remark);
		}
	}
}

</script></div>
<?php echo $footer; ?>