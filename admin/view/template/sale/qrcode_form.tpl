<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-action" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $url_cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
	<!--===================================== 訊息提示 Start =======================================-->
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <!--===================================== 訊息提示 End =========================================-->
    <div class="panel panel-default">
    	<!--================================= 標題 Start ===========================================-->
		<div class="panel-heading">
		<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
		</div>
		<!--================================= 標題 End =============================================-->
        <div class="panel-body">
            <form action="<?php echo $url_action; ?>" method="post" enctype="multipart/form-data" id="form-action" class="form-horizontal">
                <ul class="nav nav-tabs"><!-- 頁籤 -->
                    <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                </ul>
            <div class="tab-content"><!-- 內容 -->
                <div class="tab-pane active" id="tab-general">
                    <div class="tab-content">
<!-- qrcode -->
<div class="form-group required">
	<label class="col-sm-2 control-label" for="input-car-no">QRCode</label>
	<div class="col-sm-10">
		<input type="text" name="input_qrcode" placeholder="系统自動生成"
			value="<?php echo isset( $input_qrcode) ? $input_qrcode : ''; ?>"
			id="input-title" class="form-control" readonly/>
	</div>
</div>
<!-- 備註 -->
<div class="form-group required">
	<label class="col-sm-2 control-label" for="input-car-color">備註</label>
	<div class="col-sm-10">
		<input type="text" name="input_remark" placeholder="備註"
			value="<?php echo isset( $input_remark) ? $input_remark : ''; ?>"
			id="input-car-color" class="form-control" />
	</div>
</div>

<!-- 開始日期 -->
<div class="form-group">
	<label class="col-sm-2 control-label" for="input-date-added">開始日期</label>
		<div class="col-sm-3">
			<div class="input-group date">
			<input type="text" name="input_start_date"
				value="<?php echo $input_start_date; ?>"
				placeholder="<?php echo $input_start_date; ?>" class="form-control" readonly/>
		</div>
	</div>
</div>
<!-- 有效時間(分) -->
<div class="form-group">
	<label class="col-sm-2 control-label" for="input-date-added">有效時間(分)</label>
		<div class="col-sm-3">
			<div class="input-group date">
			<input type="text" name="input_ok_min"
				value="<?php echo $input_ok_min; ?>"
				placeholder="<?php echo $input_ok_min; ?>" class="form-control"/>
		</div>
	</div>
</div>
<!-- 頁面狀態 -->
<div class="form-group">
	<label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
	<div class="col-sm-10">
		<select name="sel_status" id="input-status" class="form-control">
			<?php if ($sel_status==1) { ?>
			<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
			<option value="2"><?php echo $text_disabled; ?></option>
			<?php } else { ?>
			<option value="1"><?php echo $text_enabled; ?></option>
			<option value="2" selected="selected"><?php echo $text_disabled; ?></option>
			<?php } ?>
		</select>
	</div>
</div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});
//--></script></div>
<?php echo $footer; ?>