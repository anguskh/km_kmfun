<?php foreach ($kmfunStation as $idx => $stationName) : ?>
<div data-toggle="tab" role="tab" aria-controls="nav-menu-<?=$idx?>" aria-selected="true" class="col-lg-2 col-md-12 col-sm-12 nav-scroller-item">
	<div class="tile" station="<?=$idx?>" type="out" style="position: relative;">
		<div class="tile-heading" style="background: <?=$station_background[$idx][0]?>;"><?=$stationName?>
			<span class="pull-right">出車</span></div>
			<div class="tile-body" style="background: <?=$station_background[$idx][1]?>;">
				<i class="fa fa-car"></i>
				<ul class="car_out">
					<li class="order" station="<?=$idx?>" type="out" day='1'>
						<span class="amount"><?=isset($todayCnt['out'][$idx]) ? $todayCnt['out'][$idx] : 0 ;?></span>
						<span>今日</span>
					</li>
					<li class="order" station="<?=$idx?>" type="out" day='2'>
						<span class="amount"><?=isset($tomorrowCnt['out'][$idx]) ? $tomorrowCnt['out'][$idx] : 0 ;?></span>
						<span>明日</span>
					</li>
				</ul>
			</div>
		<div class="tile-footer order" station="<?=$idx?>" type="out" day='1' style="background: <?=$station_background[$idx][1]?>;"><?php echo $text_view; ?></div>
	</div>
</div>
<div class="col-lg-2 col-md-12 col-sm-12">
	<div class="tile" station="<?=$idx?>" type="back" style="position: relative;">
		<div class="tile-heading" style="background: <?=$station_background[$idx][2]?>;"><?=$stationName?>
			<span class="pull-right">還車</span></div>
			<div class="tile-body" style="background: <?=$station_background[$idx][3]?>;">
				<i class="fa fa-car"></i>
				<ul class="car_out">
					<li class="order" station="<?=$idx?>" type="back" day='1'>
						<span class="amount"><?=isset($todayCnt['back'][$idx]) ? $todayCnt['back'][$idx] : 0 ;?></span>
						<span>今日</span>
					</li>
					<li class="order" station="<?=$idx?>" type="back" day='2'>
						<span class="amount"><?=isset($tomorrowCnt['back'][$idx]) ? $tomorrowCnt['back'][$idx] : 0 ;?></span>
						<span>明日</span>
					</li>
				</ul>
			</div>
		<div class="tile-footer order" station="<?=$idx?>" type="back" day='1' style="background: <?=$station_background[$idx][3]?>;"><?php echo $text_view; ?></div>
	</div>
</div>
<?php endforeach ; ?>

</div>
<div class="row" style="margin: 0; padding: 0;">

<div class="col-lg-12 col-md-12 col-sm-12">
	<div class="table-responsive ajax-area">
		<div class="dashboard-title"><b style="color: #ef5350;"><?=$station_name?></b> - <?=$station_status?></div>
		<table class="table table-bordered table-hover" >
			<thead>
<?php
			$select_carmodel = "";
			if(count($now_carmodel)>0){
				//$select_carmodel .= '<select class="car-type">';
				foreach($now_carmodel as $key=>$arr){
					$select_carmodel .= '<option value="'.$arr.'" '.(($cid==$arr)?'selected':'').'>'.$arr.'</option>';
				}
				//$select_carmodel .= '</select>';
			}
?>

				<tr style="background: <?=$station_background[$stationIdx][$tr_color]?>; color: White;">
					<td width="10px">#</td>
				<?php foreach ($colArr as $key => $colName) : ?>
					<?php if (isset( $sortUrl[$key])) : ?>
					<?php
						if($key == "car_model"){
							if($select_carmodel  != ""){
								$select_carmodel = '<select class="car-type" onchange="change_ajx(this.value,\''.$sortUrl[$key] .'\');"><option value="">全部</option>'.$select_carmodel.'</select>';
							}
							
					?>
						<td class="text-left"><a href="<?=$sortUrl[$key] ?>" class="<?=( $sort == $key) ? 'desc' : 'asc' ?>" style="color:white;"><?=$colName?></a><?=$select_carmodel;?></td>

					<?php
						}else{
					?>
							<td class="text-left"><a href="<?=$sortUrl[$key] ?>" class="<?=( $sort == $key) ? 'desc' : 'asc' ?>" style="color:white;"><?=$colName?></a></td>
					<?php
						}
					
					?>
					<?php else : ?>
					<td class="text-left"><?=$colName?></td>
					<?php endif ; ?>
				<?php endforeach ; ?>
					<td class="text-center">管理</td>
				</tr>
			</thead>
			<tbody>
			
				<?php foreach ($rentArr as $iCnt => $row) : ?>
				<tr <?php echo (($cid!='' && $cid!=$row['car_model'])?'style="display:none;"':'');?>>
					<td><?=$iCnt+1?></td>
					<?php foreach ($colArr as $key => $colName) : ?>
					<td <?php if ($key != "car_no") echo "nowrap" ;?>><?=$row[$key]?></td>
					<?php endforeach ; ?>
					<td class="text-center">
						<a href="<?=$row['href']?>" data-toggle="tooltip" title="編輯功能" class="btn btn-primary">
							<i class="fa fa-pencil"></i>
						</a>
					</td>
				</tr>
				<?php endforeach ; ?>
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript"><!--
// $("a[name='order']").click( function () {
$(".order").hover(function() {
	$(this).css('cursor','pointer');
});
$(".order").click( function () {
	// alert( "hello") ;
	var station = $(this).attr('station') ;
	var type    = $(this).attr('type') ;
	var chDay   = $(this).attr('day') ;
	console.log( "attribute : " + station + ", "+ type) ;

	$.ajax({
		url: '?route=extension/dashboard/kmfunrent/ajaxRentTable&token=<?=$token?>&station='+station+'&type='+type+'&chDay='+chDay,
		dataType: 'html',
		success: function(html) {
			$('.ajax-area').empty() ;
			$('.ajax-area').append( html) ;
			console.log( "ajax results :" + html) ;
		}
	});

	return false ;
});
//--></script>
	<script>
		function change_ajx(now_sn, now_url){
			location.href = now_url+'&cid='+encodeURIComponent(now_sn);
		}
	</script>