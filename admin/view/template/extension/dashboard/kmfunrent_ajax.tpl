	<div class="dashboard-title"><b style="color: #ef5350;"><?=$station_name?></b> - <?=$station_status?></div>
	<table class="table table-bordered table-hover">
		<thead>
<?php
			$select_carmodel = "";
			if(count($now_carmodel)>0){
				//$select_carmodel .= '<select class="car-type">';
				foreach($now_carmodel as $key=>$arr){
					$select_carmodel .= '<option value="'.$arr.'" '.(($cid==$arr)?'selected':'').'>'.$arr.'</option>';
				}
				//$select_carmodel .= '</select>';
			}
?>
			<tr <?=$view_tr_styles?>>
				<td width="10px">#</td>
			<?php foreach ($colArr as $key => $colName) : ?>
				<?php if (isset( $sortUrl[$key])) : ?>
				<?php
					if($key == "car_model"){
						if($select_carmodel  != ""){
							$select_carmodel = '<select class="car-type" onchange="change_ajx(this.value,\''.$sortUrl[$key] .'\');"><option value="">全部</option>'.$select_carmodel.'</select>';
						}
						
				?>
					<td class="text-left"><a href="<?=$sortUrl[$key] ?>" class="<?=( $sort == $key) ? 'desc' : 'asc' ?>" style="color:white;"><?=$colName?></a><?=$select_carmodel;?></td>

				<?php
					}else{
				?>
						<td class="text-left"><a href="<?=$sortUrl[$key] ?>" class="<?=( $sort == $key) ? 'desc' : 'asc' ?>" style="color:white;"><?=$colName?></a></td>
				<?php
					}
				
				?>
				
				<?php else : ?>
				<td class="text-left"><?=$colName?></td>
				<?php endif ; ?>
			<?php endforeach ; ?>
				<td class="text-center">管理</td>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($rentArr as $iCnt => $row) : ?>
			<tr <?php echo (($cid!='' && $cid!=$row['car_model'])?'style="display:none;"':'');?>>
				<td><?=$iCnt+1?></td>
				<?php foreach ($colArr as $key => $colName) : ?>
				<td <?php if ($key != "car_no") echo "nowrap" ;?>><?=$row[$key]?></td>
				<?php endforeach ; ?>
				<td class="text-center">
					<a href="<?=$row['href']?>" data-toggle="tooltip" title="編輯功能" class="btn btn-primary">
						<i class="fa fa-pencil"></i>
					</a>
				</td>
			</tr>
			<?php endforeach ; ?>
		</tbody>
	</table>
	<script>
		function change_ajx(now_sn, now_url){
			location.href = now_url+'&cid='+encodeURIComponent(now_sn);
		}
	</script>