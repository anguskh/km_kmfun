<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <div class="well">
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="input-date-start">訂單類別</label>
                <select name="filter_ordertype" id="input-ordertype" class="form-control">
                  <option value="0">請選擇</option>
                  <?php foreach ($orderTypeArr as $oId => $option) { ?>
                  <?php if ( isset($filter_ot) && $oId == $filter_ot) $selStr = "selected" ;
                        else $selStr = "" ;
                  ?>
                  <option value="<?php echo $oId; ?>" <?=$selStr?>><?php echo $option; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="col-sm-6">
              <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-filter"></i> <?php echo $button_filter; ?></button>
            </div>
          </div>
        </div>
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
              <?php foreach ($colArr as $key => $colName) : ?>
                <td class="text-left"><?php echo $colName; ?></td>
              <?php endforeach ; ?>
              <!--
                <td class="text-left"></td>
                <td class="text-right"></td>
              -->
              </tr>
            </thead>
            <tbody>
              <?php foreach ($reportRows as $row) : ?>
              <tr class="nextLevel" year="<?=$row['strY']?>" month="<?=$row['strM']?>" type="<?=$filter_ot?>" style="cursor: pointer">
                <?php foreach ($colArr as $key => $colName) : ?>
                <td class="text-right"><?php echo $row[$key]; ?></td>
                <?php endforeach ; ?>
              </tr>
              <?php endforeach ; ?>
            </tbody>
          </table>
        </div>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('.nextLevel').click( function () {
  url = '?route=reportk/sale_order/nextLevel&token=<?php echo $token; ?>';
  var strY      = $(this).attr('year') ;
  var strM      = $(this).attr('month') ;
  var filter_ot = $(this).attr('type') ;

  url += '&filter_ot=' + encodeURIComponent(filter_ot) + '&strY=' + strY + '&strM=' + strM;
  console.log( url) ;
  location = url;
});
$('.nextLevel').hover( function () {

});

$('#button-filter').on('click', function() {
  url = '?route=reportk/sale_order&token=<?php echo $token; ?>';

  var filter_ordertype = $('select[name=\'filter_ordertype\']').val();

  if (filter_ordertype) {
    url += '&filter_ot=' + encodeURIComponent(filter_ordertype);
  }

  location = url;
});
//--></script>
  <script type="text/javascript"><!--
//--></script></div>
<?php echo $footer; ?>