<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $text_list; ?></h3>
			</div>
			<div class="panel-body">
				<div class="well">
					<div class="row">
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label" for="input-date-start">查詢啟始時間</label>
								<div class="input-group date">
									<input type="text" name="filter_date_start" value="<?php echo @$filter_date_start; ?>" placeholder="啟始時間" data-date-format="YYYY-MM-DD" id="input-date-start" class="form-control" />
									<span class="input-group-btn">
										<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
									</span></div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label" for="input-date-end">查詢結束時間</label>
								<div class="input-group date">
									<input type="text" name="filter_date_end" value="<?php echo @$filter_date_end; ?>" placeholder="結束時間" data-date-format="YYYY-MM-DD" id="input-date-end" class="form-control" />
									<span class="input-group-btn">
										<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
									</span></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3">
							<button type="button" id="button-filter" class="btn btn-primary pull-left"><i class="fa fa-filter"></i> <?php echo $button_filter; ?></button>
						</div>
					</div>
				</div>
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
								<?php foreach ($colArr as $key => $colName) : ?>
								<td class="text-left"><?php echo $colName; ?></td>
								<?php endforeach ; ?>
								<!--
								<td class="text-left"></td>
								<td class="text-right"></td>
								-->
							</tr>
						</thead>
						<tbody>
							<?php foreach ($reportRows as $row) : ?>
							<tr class="nextLevel" year="<?=$row['strY']?>" month="<?=$row['strM']?>" type="<?=$filter_ot?>" style="cursor: pointer">
								<?php foreach ($colArr as $key => $colName) : ?>
								<td class="text-right"><?php echo $row[$key]; ?></td>
								<?php endforeach ; ?>
							</tr>
							<?php endforeach ; ?>
						</tbody>
					</table>
				</div>
				<div class="row">
					<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
					<div class="col-sm-6 text-right"><?php echo $results; ?></div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript"><!--
	$('.date').datetimepicker({
		pickTime: false
	});
//--></script></div>
<?php echo $footer; ?>