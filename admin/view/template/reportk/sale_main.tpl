<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $text_list; ?></h3>
			</div>
			<div class="panel-body">
				<div class="well">
					<div class="row">
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label" for="input-date-start">實際出車日期(起)</label>
								<div class="input-group date">
									<input type="text" name="filter_date_start" value="<?php echo @$filter_fds; ?>" placeholder="啟始時間" data-date-format="YYYY-MM-DD" id="input-date-start" class="form-control" />
									<span class="input-group-btn">
										<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
									</span></div>
							</div>
							<div class="form-group">
								<label class="control-label" for="input-date-end">實際出車日期(止)</label>
								<div class="input-group date">
									<input type="text" name="filter_date_end" value="<?php echo @$filter_fde; ?>" placeholder="結束時間" data-date-format="YYYY-MM-DD" id="input-date-end" class="form-control" />
									<span class="input-group-btn">
										<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
									</span></div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label" for="input-rent-date-in">實際還車日期(起)</label>
								<div class="input-group date">
									<input type="text" name="filter_rdi" value="<?php echo @$filter_rdi; ?>" placeholder="以還車時間查詢" data-date-format="YYYY-MM-DD" id="input-real-date-out" class="form-control" />
									<span class="input-group-btn">
										<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
									</span></div>
							</div>
							<div class="form-group">
								<label class="control-label" for="input-rent-date-in">實際還車日期(止)</label>
								<div class="input-group date">
									<input type="text" name="filter_rde" value="<?php echo @$filter_rde; ?>" placeholder="以還車時間查詢" data-date-format="YYYY-MM-DD" id="input-real-date-in" class="form-control" />
									<span class="input-group-btn">
										<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
									</span></div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">出車地點</label>
								<select name="filter_spt" id="" class="form-control">
									<option value="">出車地點</option>
									<?php foreach ($stationArr as $key => $name) : ?>
										<?php if ($key == $filter_spt)  {
											$sel = "selected" ;
										} else {
											$sel = "" ;
										}
										?>
									<option value="<?=$key?>" <?=$sel?>><?=$name?></option>
									<?php endforeach ; ?>
								</select>
							</div>
							<div class="form-group">
								<label class="control-label">還車地點</label>
								<select name="filter_ept" id="" class="form-control">
									<option value="">還車地點</option>
									<?php foreach ($stationArr as $key => $name) : ?>
										<?php if ($key == $filter_ept)  {
											$sel = "selected" ;
										} else {
											$sel = "" ;
										}
										?>
									<option value="<?=$key?>" <?=$sel?>><?=$name?></option>
									<?php endforeach ; ?>
								</select>
							</div>
						</div>
						<div class="col-sm-3 text-right searchBtn">
							<button type="button" id="button-filter" class="btn btn-primary"><i class="fa fa-filter" style="font-size: 1.5em;"></i> <?php echo $button_filter; ?></button>&nbsp;&nbsp;
							<button type="button" id="button-export" class="btn btn-primary"><i class="fa fa-file-excel-o" style="font-size: 1.5em;"></i> 匯出報表</button>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3">
						</div>
					</div>
				</div>
				<?php if ( $dashboardBlock['allOrder']['cnt'] != 0) : ?>
				<?php foreach ($dashboardMaps['element'] as $iCnt => $elements) : ?>
				<div class="row">
					<?php foreach ($elements as $nd2 => $ele) : ?>
					<div class="col-lg-2 col-md-6 col-xs-12" style="padding-left: 10px; padding-right: 0px;">
						<div class="tile mainorder" style="position: relative;">
							<div class="tile-heading" style="background: <?=$dashboardMaps['attr'][$iCnt]['title']?>;">
								<?=$dashboardBlock[$ele]['name']?><span class="pull-right"></span>
							</div>
							<div class="tile-body" style="background: <?=$dashboardMaps['attr'][$iCnt]['body']?>;">
								<ul class="car_out">
									<!--
									<li class="order" >
										<span class="amount"><?=number_format($dashboardBlock[$ele]['cnt'])?></span>
										<span>筆數</span>
									</li>
									-->
									<li class="order" >
										<span class="amount"><?=number_format($dashboardBlock[$ele]['total'])?></span>
										<span>金額</span>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<?php endforeach ; ?>
				</div>
				<?php endforeach ; ?>
				<?php endif ;?><!--if ( isset( $statisticalArr)) :-->
				<div class="row">
					<div class="col-sm-6 text-left" style="padding-bottom: 3px;"><?php echo $pagination; ?>
						<?php if ( isset( $exportUrl)) : ?>
						<a href="<?=$exportUrl?>" id="export" class="btn btn-primary" target="_blank"><i class="fa fa-file-excel-o" style="font-size: 1.5em;"></i> 匯出報表</a>
						<?php endif ; ?>
					</div>
					<div class="col-sm-6 text-right" ><?php echo $results; ?>
					</div>
				</div>
				<div class="table-responsive ">
					<table class="table table-bordered table-hover order-list">
						<thead>
							<tr>
								<?php foreach ($columnNames as $key => $colName) :
									$colColorStr = "" ;
									if (in_array( $key, $planDate))
										$colColorStr = "background-color: ".$colColor['planDate'].";" ;
									if (in_array( $key, $realDate))
										$colColorStr = "background-color: ".$colColor['realDate'].";" ;
									if (in_array( $key, $retPay))
										$colColorStr = "background-color: ".$colColor['retPay'].";" ;
									if (in_array( $key, $rentPay))
										$colColorStr = "background-color: ".$colColor['rentPay'].";" ;
								?>
								<td class="text-left" style="white-space: nowrap; <?=$colColorStr?>"><?php echo $colName; ?></td>
								<?php endforeach ; ?>
							</tr>
						</thead>
						<tbody>
							<?php if ( $listRows) : ?>
							<?php foreach ($listRows as $iCnt => $row) : ?>
							<tr class="nextLevel trtd" >
								<?php foreach ($columnNames as $key => $colName) :
									$colColorStr = "" ;
									if (in_array( $key, $planDate))
										$colColorStr = "background-color: ".$colColor['planDate'].";" ;
									if (in_array( $key, $realDate))
										$colColorStr = "background-color: ".$colColor['realDate'].";" ;
									if (in_array( $key, $retPay))
										$colColorStr = "background-color: ".$colColor['retPay'].";" ;
									if (in_array( $key, $rentPay))
										$colColorStr = "background-color: ".$colColor['rentPay'].";" ;
								?>
								<td class="text-right" style="<?=$colColorStr?>"><?php echo @$row[$key]; ?></td>
								<?php endforeach ; ?>
							</tr>
							<?php endforeach ; ?>
							<?php endif ; ?>
						</tbody>
					</table>
				</div>
				<div class="row">
					<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
					<div class="col-sm-6 text-right"><?php echo $results; ?></div>
				</div>
			</div>
		</div>
	</div>
	<style type="text/css">
		.trtd td {
			white-space: nowrap;
		}
		.searchBtn{
			padding-top: 36px;
		}
		@media (max-width: 768px) {
			.searchBtn{ padding-top: 0px; }

		}
	</style>
	<script type="text/javascript"><!--
	$('.date').datetimepicker({
		pickTime: false
	});
	$('#button-filter').on('click', function() {
		var msg = "" ;
		url = '?route=reportk/sale_main&token=' + getURLVar('token') ;
		var fds = $('input[name=\'filter_date_start\']').val();
		var fde = $('input[name=\'filter_date_end\']').val();
		var fsp = $('select[name=\'filter_spt\']').val();
		var fep = $('select[name=\'filter_ept\']').val();

		var rdi = $('input[name=\'filter_rdi\']').val();
		var rde = $('input[name=\'filter_rde\']').val();
		console.log(rdi) ;
		if ( fds != '' && rdi != '') {
			msg = "查詢時間只能選擇出車或還車" ;
		}
		if ( msg != '') {
			alert( msg) ;
			return ;
		}

		if ( rdi) {
			url += '&filter_rdi=' + encodeURIComponent(rdi);
		}
		if ( rde) {
			url += '&filter_rde=' + encodeURIComponent(rde);
		}
		if (fds) {
			url += '&filter_fds=' + encodeURIComponent(fds);
		}
		if (fde) {
			url += '&filter_fde=' + encodeURIComponent(fde);
		}

		if (fsp) {
			url += '&filter_spt=' + encodeURIComponent(fsp);
		}
		if (fep) {
			url += '&filter_ept=' + encodeURIComponent(fep);
		}
		console.log( url) ;
		location = url;
	});
	$('#button-export').on('click', function() {
		url = '?route=reportk/sale_main&token=' + getURLVar('token') ;
		var fds = $('input[name=\'filter_date_start\']').val();
		var fde = $('input[name=\'filter_date_end\']').val();
		var fsp = $('select[name=\'filter_spt\']').val();
		var fep = $('select[name=\'filter_ept\']').val();

		var rdi = $('input[name=\'filter_rdi\']').val();
		var rde = $('input[name=\'filter_rde\']').val();

		if ( rdi) {
			url += '&filter_rdi=' + encodeURIComponent(rdi);
		}
		if ( rde) {
			url += '&filter_rde=' + encodeURIComponent(rde);
		}
		if (fds) {
			url += '&filter_fds=' + encodeURIComponent(fds);
		}
		if (fde) {
			url += '&filter_fde=' + encodeURIComponent(fde);
		}

		if (fsp) {
			url += '&filter_spt=' + encodeURIComponent(fsp);
		}
		if (fep) {
			url += '&filter_ept=' + encodeURIComponent(fep);
		}

		console.log( url) ;
		window.open(url+"&export=excel", '_blank');
		// location = url;
	});


//--></script></div>
<?php echo $footer; ?>