<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-bar-chart"></i> <?php echo $text_list; ?></h3>
			</div>
			<div class="panel-body">
				<div class="well">
					<div class="row">
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label" for="input-date-start">出/還車</label>
								<select name="filter_mod" id="" class="form-control">
									<option value="">請選擇</option>
									<option value="rent_out" <?=($filter_mod=='rent_out') ? 'selected' : '';?>>當日租車</option>
									<option value="rent_in"  <?=($filter_mod=='rent_in')  ? 'selected' : '';?>>當日還車</option>
								</select>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label">營業據點</label>
								<select name="filter_station" id="" class="form-control">
									<option value="">請選擇</option>
									<?php foreach ($stationArr as $key => $name) : ?>
										<?php if ($key == $filter_station)  {
											$sel = "selected" ;
										} else {
											$sel = "" ;
										}
										?>
									<option value="<?=$key?>" <?=$sel?>><?=$name?></option>
									<?php endforeach ; ?>
								</select>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label" for="input-date-start">服務人員</label>
								<select name="filter_man" id="" class="form-control">
									<option value="">請選擇</option>
									<?php foreach ($userArr as $key => $name) : ?>
										<?php if ($key == $filter_man) {
											$sel = "selected" ;
										} else {
											$sel = "" ;
										}
										?>
									<option value="<?=$key?>" <?=$sel?>><?=$name?></option>
									<?php endforeach ; ?>
								</select>
							</div>
						</div>
						<div class="col-sm-3 text-right searchBtn">
							<button type="button" id="button-filter" class="btn btn-primary"><i class="fa fa-filter" style="font-size: 1.5em;"></i> <?php echo $button_filter; ?></button>&nbsp;&nbsp;
							<button type="button" id="button-export" class="btn btn-primary"><i class="fa fa-file-excel-o" style="font-size: 1.5em;"></i> 匯出報表</button>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3">
						</div>
					</div>
				</div>
				<?php if ( $dashboardBlock['allOrder']['cnt'] != 0) : ?>
				<?php foreach ($dashboardMaps['element'] as $iCnt => $elements) : ?>
				<div class="row">
					<?php foreach ($elements as $nd2 => $ele) : ?>
					<div class="col-lg-2 col-md-6 col-xs-12" style="padding-left: 10px; padding-right: 0px;">
						<div class="tile mainorder" style="position: relative;">
							<div class="tile-heading" style="background: <?=$dashboardMaps['attr'][$iCnt]['title']?>;">
								<?=$dashboardBlock[$ele]['name']?><span class="pull-right"></span>
							</div>
							<div class="tile-body" style="background: <?=$dashboardMaps['attr'][$iCnt]['body']?>;">
								<ul class="car_out">
									<!--
									<li class="order" >
										<span class="amount"><?=number_format($dashboardBlock[$ele]['cnt'])?></span>
										<span>筆數</span>
									</li>
									-->
									<li class="order" >
										<span class="amount"><?=number_format($dashboardBlock[$ele]['total'])?></span>
										<span>金額</span>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<?php endforeach ; ?>
				</div>
				<?php endforeach ; ?>
				<?php endif ;?><!--if ( isset( $statisticalArr)) :-->
				<div class="row">
					<div class="col-sm-6 text-left" style="padding-bottom: 3px;"><?php echo $pagination; ?>
						<?php if ( isset( $exportUrl)) : ?>
						<a href="<?=$exportUrl?>" id="export" class="btn btn-primary" target="_blank"><i class="fa fa-file-excel-o" style="font-size: 1.5em;"></i> 匯出報表</a>
						<?php endif ; ?>
					</div>
					<div class="col-sm-6 text-right" ><?php echo $results; ?>
					</div>
				</div>
				<div class="table-responsive ">
					<table class="table table-bordered table-hover order-list">
						<thead>
							<tr>
								<?php foreach ($columnNames as $key => $colName) :
									$colColorStr = "" ;
									if (in_array( $key, $planDate))
										$colColorStr = "background-color: ".$colColor['planDate'].";" ;
									if (in_array( $key, $realDate))
										$colColorStr = "background-color: ".$colColor['realDate'].";" ;
									if (in_array( $key, $retPay))
										$colColorStr = "background-color: ".$colColor['retPay'].";" ;
									if (in_array( $key, $rentPay))
										$colColorStr = "background-color: ".$colColor['rentPay'].";" ;
								?>
								<td class="text-left" style="white-space: nowrap; <?=$colColorStr?>"><?php echo $colName; ?></td>
								<?php endforeach ; ?>
							</tr>
						</thead>
						<tbody>
							<?php if ( $listRows) : ?>
							<?php foreach ($listRows as $iCnt => $row) : ?>
							<tr class="nextLevel trtd" >
								<?php foreach ($columnNames as $key => $colName) :
									$colColorStr = "" ;
									if (in_array( $key, $planDate))
										$colColorStr = "background-color: ".$colColor['planDate'].";" ;
									if (in_array( $key, $realDate))
										$colColorStr = "background-color: ".$colColor['realDate'].";" ;
									if (in_array( $key, $retPay))
										$colColorStr = "background-color: ".$colColor['retPay'].";" ;
									if (in_array( $key, $rentPay))
										$colColorStr = "background-color: ".$colColor['rentPay'].";" ;
								?>
								<td class="text-right" style="<?=$colColorStr?>"><?php echo @$row[$key]; ?></td>
								<?php endforeach ; ?>
							</tr>
							<?php endforeach ; ?>
							<?php endif ; ?>
						</tbody>
					</table>
				</div>
				<div class="row">
					<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
					<div class="col-sm-6 text-right"><?php echo $results; ?></div>
				</div>
			</div>
		</div>
	</div>
	<style type="text/css">
		.trtd td {
			white-space: nowrap;
		}
		.searchBtn{
			padding-top: 36px;
		}
		@media (max-width: 768px) {
			.searchBtn{ padding-top: 0px; }

		}
	</style>
	<script type="text/javascript"><!--
	$('#button-filter').on('click', function() {
		var msg = "" ;
		url = '?route=reportk/sale_today&token=' + getURLVar('token') ;
		var fmod     = $('select[name=\'filter_mod\']').val();
		var fstation = $('select[name=\'filter_station\']').val();
		var fman     = $('select[name=\'filter_man\']').val();

		if ( fmod == '') {
			msg += "查詢出/還車時間 未定義\n" ;
		}
		if ( fstation == '') {
			msg += "營業據點 未定義\n" ;
		}

		if (fmod) {
			url += '&filter_mod=' + encodeURIComponent(fmod);
		}
		if (fstation) {
			url += '&filter_station=' + encodeURIComponent(fstation);
		}
		if (fstation) {
			url += '&filter_man=' + encodeURIComponent(fman);
		}
		console.log( url) ;
		location = url;
	});
	$('#button-export').on('click', function() {
		url = '?route=reportk/sale_main&token=' + getURLVar('token') ;
		var msg = "" ;
		url = '?route=reportk/sale_today&token=' + getURLVar('token') ;
		var fmod     = $('select[name=\'filter_mod\']').val();
		var fstation = $('select[name=\'filter_station\']').val();
		var fman     = $('select[name=\'filter_man\']').val();

		if ( fmod == '') {
			msg += "查詢出/還車時間 未定義\n" ;
		}
		if ( fstation == '') {
			msg += "營業據點 未定義\n" ;
		}

		if (fmod) {
			url += '&filter_mod=' + encodeURIComponent(fmod);
		}
		if (fstation) {
			url += '&filter_station=' + encodeURIComponent(fstation);
		}
		if (fstation) {
			url += '&filter_man=' + encodeURIComponent(fman);
		}

		console.log( url) ;
		window.open(url+"&export=excel", '_blank');
	});


//--></script></div>
<?php echo $footer; ?>