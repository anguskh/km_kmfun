<?php
abstract class Controller {
	protected	$registry;
	protected	$rowInfo	= '' ;

	public function __construct($registry) {
		$this->registry = $registry;
	}

	public function __get($key) {
		return $this->registry->get($key);
	}

	public function __set($key, $value) {
		$this->registry->set($key, $value);
	}

	/**
	* 設定資料 add by Angus 2017.04.27
	* @param  [type] $modIndex : post, get, error
	* @param  [type] $inputObjName : input, sel, area
	* @param  [type] $msgIndex	[]
	* @return [type] [description]
	*/
	protected function setDataInfo( $modIndex, $inputObjName, $msgIndex) {
		// dump( array($modIndex, $inputObjName, $msgIndex, $this->error) );
		switch ( $modIndex) {
			case 'post':
				if ( empty( $inputObjName)) {
					$index = "{$msgIndex}" ;
				} else {
					$index = "{$inputObjName}_{$msgIndex}" ;
				}

				if (isset( $this->request->post[$index])) {
					return $this->request->post[$index] ;
				} elseif ( $this->rowInfo) {
					return $this->rowInfo[$msgIndex] ;
				} else {
					return '' ;
				}
				break;
			case 'get':
				if (isset( $this->request->get[$msgIndex])) {
					return $this->request->get[$msgIndex] ;
				} elseif ( $this->rowInfo) {
					return $this->rowInfo[$msgIndex] ;
				} else {
					return '' ;
				}
				break;
			case 'error':
				if (isset( $this->error[$msgIndex])) {
					return $this->error[$msgIndex] ;
				} else {
					return '' ;
				}
				break;
		}
	}
}