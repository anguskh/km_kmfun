<?php
// Site
$_['site_base']         = HTTP_SERVER;
$_['site_ssl']          = HTTPS_SERVER;

// Database
$_['db_autostart']      = true;
$_['db_type']           = DB_DRIVER; // mpdo, mssql, mysql, mysqli or postgre
$_['db_hostname']       = DB_HOSTNAME;
$_['db_username']       = DB_USERNAME;
$_['db_password']       = DB_PASSWORD;
$_['db_database']       = DB_DATABASE;
$_['db_port']           = DB_PORT;

// Session
$_['session_autostart'] = true;

// Actions
$_['action_pre_action'] = array(
	'startup/startup',
	'startup/error',
	'startup/event',
	'startup/sass',
	'startup/login',
	'startup/permission'
);

// Actions
$_['action_default'] = 'common/dashboard';

// Action Events
$_['action_event'] = array(
    'view/*/before'                           => 'event/theme',
	'controller/extension/analytics/*/before' => 'event/compatibility/controller',
	'controller/extension/captcha/*/before'   => 'event/compatibility/controller',
	'controller/extension/feed/*/before'      => 'event/compatibility/controller',
	'controller/extension/fraud/*/before'     => 'event/compatibility/controller',
	'controller/extension/module/*/before'    => 'event/compatibility/controller',
	'controller/extension/payment/*/before'   => 'event/compatibility/controller',
	'controller/extension/recurring/*/before' => 'event/compatibility/controller',
	'controller/extension/shipping/*/before'  => 'event/compatibility/controller',
	'controller/extension/theme/*/before'     => 'event/compatibility/controller',
	'controller/extension/total/*/before'     => 'event/compatibility/controller',
	'view/analytics/*/before'                 => 'event/compatibility/view',
	'view/captcha/*/before'                   => 'event/compatibility/view',
	'view/feed/*/before'                      => 'event/compatibility/view',
	'view/fraud/*/before'                     => 'event/compatibility/view',
	'view/module/*/before'                    => 'event/compatibility/view',
	'view/payment/*/before'                   => 'event/compatibility/view',
	'view/recurring/*/before'                 => 'event/compatibility/view',
	'view/shipping/*/before'                  => 'event/compatibility/view',
	'view/theme/*/before'                     => 'event/compatibility/view',
	'view/total/*/before'                     => 'event/compatibility/view',
	'language/extension/analytics/*/before'   => 'event/compatibility/language',
	'language/extension/captcha/*/before'     => 'event/compatibility/language',
	'language/extension/feed/*/before'        => 'event/compatibility/language',
	'language/extension/fraud/*/before'       => 'event/compatibility/language',
	'language/extension/module/*/before'      => 'event/compatibility/language',
	'language/extension/payment/*/before'     => 'event/compatibility/language',
	'language/extension/recurring/*/before'   => 'event/compatibility/language',
	'language/extension/shipping/*/before'    => 'event/compatibility/language',
	'language/extension/theme/*/before'       => 'event/compatibility/language',
	'language/extension/total/*/before'       => 'event/compatibility/language'
);

/**
 * add by Angus 2015.06.18 2017.02.28
 * pre( $user_group_info, __METHOD__, __FILE__) ;
 */
function pre($val, $className="", $fileName="", $toLogFile = FALSE, $Parameters = "")
{
	if (DEBUG_MOD) {
		if ( $toLogFile) {
			$tmpStr = trim($fileName)		? "file Name : {$fileName}\n"		: "" ;
			$tmpStr .= trim($className)		? "class Name : {$className}\n"		: "" ;
			$tmpStr .= trim($Parameters)	? "Parameters : {$Parameters}\n"	: "" ;
			$tmpStr .= print_r( $val, TRUE);
			save_log( $tmpStr) ;
		} else {
			echo '<pre>';
			echo trim($fileName)	? "file Name : {$fileName}\n"		: "" ;
			echo trim($className)	? "class Name : {$className}\n"		: "" ;
			echo trim($Parameters)	? "Parameters : {$Parameters}\n"	: "" ;
			print_r( $val);
			// var_dump( $val);
			echo '</pre><p>';
		}
	}
}

/**
 * Outputs the given variables with formatting and location. Huge props
 * out to Phil Sturgeon for this one (http://philsturgeon.co.uk/blog/2010/09/power-dump-php-applications).
 * To use, pass in any number of variables as arguments.
 *
 * @return void
 */
function dump() {
	list($callee) = debug_backtrace();
	$arguments = func_get_args();
	$total_arguments = count($arguments);

	echo '<fieldset style="background:#fefefe !important; border:2px red solid; padding:5px">' . PHP_EOL .
		'<legend style="background:lightgrey; padding:5px;">' . $callee['file'] . ' @ line: ' . $callee['line'] . '</legend>' . PHP_EOL .
		'<pre>';

    $i = 0;
    foreach ($arguments as $argument) {
		echo '<br/><strong>Debug #' . (++$i) . ' of ' . $total_arguments . '</strong>: ';

		if ( (is_array($argument) || is_object($argument)) && count($argument)) {
			print_r($argument);
		} else {
			var_dump($argument);
		}
	}

	echo '</pre>' . PHP_EOL .
		'</fieldset>' . PHP_EOL;
}


function traceLog( $msgStr, $className="", $fileName="") {
	if ( DB_DEBUG_MOD) {
		$logFileName = "/Applications/MAMP/htdocs/kmfun/system/storage/logs/db.log" ;
		$fp = fopen($logFileName , "a" ) ;
		$nowTime = date("Y/m/d H:i:s");
		fwrite($fp,"{$nowTime} | {$msgStr} \r\n") ;
		fclose( $fp) ;
	}

}