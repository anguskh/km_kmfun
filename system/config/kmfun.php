<?php // 前台定義 不可刪除
// Site
$_['site_base']        = HTTP_SERVER;
$_['site_ssl']         = HTTPS_SERVER;

// Url
$_['url_autostart']    = false;

// Database
$_['db_autostart']     = true;
$_['db_type']          = DB_DRIVER; // mpdo, mssql, mysql, mysqli or postgre
$_['db_hostname']      = DB_HOSTNAME;
$_['db_username']      = DB_USERNAME;
$_['db_password']      = DB_PASSWORD;
$_['db_database']      = DB_DATABASE;
$_['db_port']          = DB_PORT;

// Session
$_['session_autostart'] = false;

// Autoload Libraries
$_['library_autoload'] = array(
	// 'openbay'
	'SMSHttp'
);

// Actions
$_['action_pre_action'] = array(
	'startup/session',
	'startup/startup',
	'startup/error',
	// 'startup/event',
	'startup/maintenance',
	'startup/seo_url'
);

// Action Events
// $_['action_event'] = array(
// 	'view/*/before'                         => 'event/theme',

// 	'model/extension/analytics/*/before'    => 'event/compatibility/beforeModel',
// 	'model/extension/captcha/*/before'      => 'event/compatibility/beforeModel',
// 	'model/extension/credit_card/*/before'  => 'event/compatibility/beforeModel',
// 	'model/extension/feed/*/before'         => 'event/compatibility/beforeModel',
// 	'model/extension/fraud/*/before'        => 'event/compatibility/beforeModel',
// 	'model/extension/module/*/before'       => 'event/compatibility/beforeModel',
// 	'model/extension/payment/*/before'      => 'event/compatibility/beforeModel',
// 	'model/extension/recurring/*/before'    => 'event/compatibility/beforeModel',
// 	'model/extension/shipping/*/before'     => 'event/compatibility/beforeModel',
// 	'model/extension/theme/*/before'        => 'event/compatibility/beforeModel',
// 	'model/extension/total/*/before'        => 'event/compatibility/beforeModel',

// 	'model/analytics/*/after'               => 'event/compatibility/afterModel',
// 	'model/captcha/*/after'                 => 'event/compatibility/afterModel',
// 	'model/credit_card/*/after'             => 'event/compatibility/afterModel',
// 	'model/feed/*/after'                    => 'event/compatibility/afterModel',
// 	'model/fraud/*/after'                   => 'event/compatibility/afterModel',
// 	'model/module/*/after'                  => 'event/compatibility/afterModel',
// 	'model/payment/*/after'                 => 'event/compatibility/afterModel',
// 	'model/recurring/*/after'               => 'event/compatibility/afterModel',
// 	'model/shipping/*/after'                => 'event/compatibility/afterModel',
// 	'model/theme/*/after'                   => 'event/compatibility/afterModel',
// 	'model/total/*/after'                   => 'event/compatibility/afterModel',

// 	//'language/extension/*/before'         => 'event/translation',
// 	'language/extension/analytics/*/before' => 'event/compatibility/language',
// 	'language/extension/captcha/*/before'   => 'event/compatibility/language',
// 	'language/extension/feed/*/before'      => 'event/compatibility/language',
// 	'language/extension/fraud/*/before'     => 'event/compatibility/language',
// 	'language/extension/module/*/before'    => 'event/compatibility/language',
// 	'language/extension/payment/*/before'   => 'event/compatibility/language',
// 	'language/extension/recurring/*/before' => 'event/compatibility/language',
// 	'language/extension/shipping/*/before'  => 'event/compatibility/language',
// 	'language/extension/theme/*/before'     => 'event/compatibility/language',
// 	'language/extension/total/*/before'     => 'event/compatibility/language'

// 	//'controller/*/before'                 => 'event/debug/before',
// 	//'controller/*/after'                  => 'event/debug/after'
// );


/**
 * Outputs the given variables with formatting and location. Huge props
 * out to Phil Sturgeon for this one (http://philsturgeon.co.uk/blog/2010/09/power-dump-php-applications).
 * To use, pass in any number of variables as arguments.
 *
 * @return void
 */
function dump() {
	list($callee) = debug_backtrace();
	$arguments = func_get_args();
	$total_arguments = count($arguments);

	echo '<fieldset style="background:#fefefe !important; border:2px red solid; padding:5px">' . PHP_EOL .
		'<legend style="background:lightgrey; padding:5px;">' . $callee['file'] . ' @ line: ' . $callee['line'] . '</legend>' . PHP_EOL .
		'<pre>';

    $i = 0;
    foreach ($arguments as $argument) {
		echo '<br/><strong>Debug #' . (++$i) . ' of ' . $total_arguments . '</strong>: ';

		if ( (is_array($argument) || is_object($argument)) && count($argument)) {
			print_r($argument);
		} else {
			var_dump($argument);
		}
	}

	echo '</pre>' . PHP_EOL .
		'</fieldset>' . PHP_EOL;
}

function traceLog( $msgStr, $className="", $fileName="") {
	if ( DB_DEBUG_MOD) {
		$logFileName = "/Applications/MAMP/htdocs/kmfun/system/storage/logs/msg.log" ;
		$fp = fopen($logFileName , "a" ) ;
		$nowTime = date("Y/m/d H:i:s");
		fwrite($fp,"{$nowTime} | {$msgStr} \r\n") ;
		fclose( $fp) ;
	}

}
