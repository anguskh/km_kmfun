<?php
/**
 *  add by Angus 2018.05.09
 *  提供 金豐 -> 綠界 returnUrl 更新訂單狀態
 */
include_once( 'inc/common_inc.php') ;

$logFileName = isset( $_GET['website_type']) ? $_GET['website_type'] : 'none' ;

save_log( $_GET, $logFileName) ;
save_log( $_POST, $logFileName) ;
$ecRetArr = isset( $_POST) ? $_POST : "" ;
// $ecRetArr = array(
// 	"MerchantID"           => "2000132",
// 	"MerchantTradeNo"      => "180509153859181",
// 	"PaymentDate"          => "2018/05/09 15:39:47",
// 	"PaymentType"          => "Credit_CreditCard",
// 	"PaymentTypeChargeFee" => "1",
// 	"RtnCode"              => "1",
// 	"RtnMsg"               => "交易成功",
// 	"SimulatePaid"         => "0",
// 	"StoreID"              => "",
// 	"TradeAmt"             => "1800",
// 	"TradeDate"            => "2018/05/09 15:38:59",
// 	"TradeNo"              => "1805091538592283",
// 	"CheckMacValue"        => "0E1CD40D196FE3558B2A966A1832A99995DF51FCCF3BCD3A25AD4606203A87DA",
// ) ;
$websiteType = isset( $_GET['website_type']) ? $_GET['website_type'] : "" ;
$orderId = isset( $_GET['orderID']) ? $_GET['orderID'] : "" ;
if ( !empty( $websiteType) && isset( $ecRetArr['MerchantTradeNo'])) {
	switch ( $websiteType) {
		case 'dev':
			$dbObj = new database( 'localhost', 'kmfun', 'kmfun123', 'kmfun_dev', '3306') ;
			break;
		case 'prod':
			$dbObj = new database( 'localhost', 'kmfun', 'job4!kmfun', 'kmfun', '3306') ;
			break;

		default:
			echo "非作業程序1" ;
			save_log( "error1 非作業程序 錯誤的進入點 : {$websiteType}", $logFileName) ;
			save_log( $_SERVER, $logFileName) ;
			exit() ;
			break;
	}

	// 保留綠界回傳資料
	$now = date('Y-m-d H:i:s') ;
	$SQLCmd = "INSERT INTO tb_ecpay_return SET
			orderID              = '{$orderId}',
			MerchantID           = '{$ecRetArr['MerchantID']}',
			MerchantTradeNo      = '{$ecRetArr['MerchantTradeNo']}',
			StoreID              = '{$ecRetArr['StoreID']}',
			RtnCode              = '{$ecRetArr['RtnCode']}',
			RtnMsg               = '{$ecRetArr['RtnMsg']}',
			TradeNo              = '{$ecRetArr['TradeNo']}',
			TradeAmt             = '{$ecRetArr['TradeAmt']}',
			PaymentDate          = '{$ecRetArr['PaymentDate']}',
			PaymentType          = '{$ecRetArr['PaymentType']}',
			PaymentTypeChargeFee = '{$ecRetArr['PaymentTypeChargeFee']}',
			TradeDate            = '{$ecRetArr['TradeDate']}',
			SimulatePaid         = '{$ecRetArr['SimulatePaid']}',
			CheckMacValue        = '{$ecRetArr['CheckMacValue']}',
			create_date='{$now}'
	" ;
	save_log( $SQLCmd, $logFileName) ;
	$dbObj->insertDB( $SQLCmd) ;

	// 更新訂單狀態
	if ( $ecRetArr['RtnCode'] == 1) {
		$SQLCmd = "UPDATE tb_order SET checkout='{$ecRetArr['TradeNo']}' WHERE order_id='{$orderId}'" ;
		save_log( $SQLCmd, $logFileName) ;
		$dbObj->updateDB( $SQLCmd) ;
		if ( $ecRetArr['RtnMsg'] != "paid") {
			// sendLineNotify( "綠界回傳交易成功 : {$ecRetArr['RtnCode']} | {$ecRetArr['MerchantTradeNo']} | {$ecRetArr['RtnMsg']}") ;
		}
	} else {
		sendLineNotify( "綠界回傳{$ecRetArr['RtnMsg']} : {$ecRetArr['RtnCode']} | {$ecRetArr['MerchantTradeNo']}") ;
	}
	// echo "{$ecRetArr['RtnCode']}|{$ecRetArr['RtnMsg']}" ;
	save_log( "{$ecRetArr['RtnCode']}| msg : {$ecRetArr['RtnMsg']}", $logFileName) ;
	echo "{$ecRetArr['RtnCode']}|OK" ;


} else {
	echo "非作業程序2" ;
	save_log( "error2 非作業程序2", $logFileName) ;
	save_log( $_SERVER, $logFileName) ;
}




/**
 * [sendLineNotify 綠界回傳通知]
 * @param   string     $msgStr [description]
 * @return  [type]             [description]
 * @Another Angus
 * @date    2019-09-24
 */
function sendLineNotify ( $msgStr = "") {
	/**
	 * [$notifyToken Line notify 設定值]
	 * @var string
	 */
	$notifyToken = "6d68rctKSJnz5IasZb9jXmWuJOBFVoOgBCuOIdigl3x" ; // 主機狀態通知
	$notifyUrl   = 'https://notify-api.line.me/api/notify -H "Authorization: Bearer %s" -d "message=%s"' ;
	$msgStr = "\n[Func] ecpayReturn.php\n" . $msgStr ;
	$notifyUrlStr = sprintf( $notifyUrl, $notifyToken, $msgStr) ;
	exec( "curl {$notifyUrlStr}") ;
}